Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class clmActivitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C330225") AndAlso (ViewState("MainMnu_code") <> "C330230")) Then 'C330225
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    ' txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")
                    BindReportType()
                    If (ViewState("MainMnu_code") = "C330225") Then
                        BindReportPrintedFor()
                    Else
                        BindReportPrintedFor()
                    End If
                    If (ViewState("MainMnu_code") = "C330230") Then
                        trDateRange.Visible = False
                        trReportType.Visible = False
                    Else
                        trDateRange.Visible = True
                        trReportType.Visible = True
                    End If

                    GetAllGrade()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Public Sub SetupReportType()
        If radViewedUsers.Checked Then
            lblFromDate.Text = "From Date"
            tdToDate_1.Visible = True

            tdToDate_3.Visible = True

        ElseIf radSummaryUsers.Checked OrElse radPendingUsers.Checked Then
            lblFromDate.Text = "As on Date"
            tdToDate_1.Visible = False

            tdToDate_3.Visible = False
        End If
        If txtfromdate.Text = "" Then
            txtfromdate.Text = Format(Date.Now, OASISConstants.DateFormat)
        End If
        If txttodate.Text = "" Then
            txttodate.Text = Format(Date.Now, OASISConstants.DateFormat)
        End If
    End Sub

    Sub BindReportPrintedFor(Optional ByVal bFinal As Boolean = False)
        ddlReportPrintedFor.DataSource = ReportFunctions.GetReportPrintedFor_ALL(ddlReportType.SelectedValue, bFinal)
        ddlReportPrintedFor.DataTextField = "RPF_DESCR"
        ddlReportPrintedFor.DataValueField = "RPF_ID"
        ddlReportPrintedFor.DataBind()
    End Sub

    Sub BindReportType()
        ddlReportType.DataSource = ReportFunctions.GetReportType(Session("sBsuid"), ddlAca_Year.SelectedItem.Value)
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindReportType()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        'grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        'grdStudent.DataBind()
    End Sub

    Sub GetAllGrade()
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        ddlGrade.DataSource = ReportFunctions.GetGradeWithReportType(ddlReportType.SelectedValue, ddlReportPrintedFor.SelectedValue, ddlAca_Year.SelectedValue, Session("sBSUID"), Session("EmployeeID"), bSuperUsr)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        GetSectionForGrade()
    End Sub

    Sub GetSectionForGrade()
        'If ddlGrade.SelectedValue = "ALL" Then
        '    ddlSection.DataSource = Nothing
        '    ddlSection.DataBind()
        '    ddlSection.Items.Add(New ListItem("--", 0))
        '    ddlSection.Items.FindByText("--").Selected = True
        'Else
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        Dim strGrd() = ddlGrade.SelectedValue.Split("_")
        h_GRD_IDs.Value = strGrd(0)
        ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, strGrd(0), Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            Select Case ViewState("MainMnu_code")
                Case "C330225"
                    If radViewedUsers.Checked Then
                        ReportViewerList(1)
                    ElseIf radPendingUsers.Checked Then
                        ReportViewerList(2)
                    ElseIf radSummaryUsers.Checked Then
                        ReportViewerList(3)
                    End If
                Case "C330230"
                    DMHS_LowPerformersReport()
            End Select

        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub

    Private Sub DMHS_LowPerformersReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("ACADEMIC_YEAR", ddlAca_Year.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptDMHS_LowPerformers.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub

    Private Sub ReportViewerList(ByVal ReportType As Integer)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STDATE", txtfromdate.Text)
        param.Add("@ENDDATE", txttodate.Text)
        param.Add("@GRD_ID", ddlGrade.SelectedValue)
        param.Add("@ReportType", ReportType)
        If ddlSection.SelectedItem.Text = "ALL" Then
            param.Add("@SCT_ID", DBNull.Value)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedValue)
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If ReportType = 1 Then
                .reportPath = Server.MapPath("../Rpt/rptReportViewedUsers.rpt")
            ElseIf ReportType = 2 Then
                '.reportPath = Server.MapPath("../Rpt/rptReportViewedUsers.rpt")
                .reportPath = Server.MapPath("../Rpt/rptReportViewer_PendingUsers.rpt")
            ElseIf ReportType = 3 Then
                .reportPath = Server.MapPath("../Rpt/rptReportViewer_Summary.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub

    Public Function GetReportType() As String
        Dim strGRD_ID As String = ddlGrade.SelectedValue
        Dim strACD_ID As String = ddlAca_Year.SelectedValue
        Dim strBSSU_ID As String = Session("sBSUID")
        Dim strRSM_ID As String = ddlReportType.SelectedValue
        Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportPrintedFor()
        GetAllGrade()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetSectionForGrade()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        'code added by dhanya
        BindReportType()
        If (ViewState("MainMnu_code") = "C400005") Then
            BindReportPrintedFor(True)
        Else
            BindReportPrintedFor()
        End If
        GetAllGrade()
        GetSectionForGrade()
    End Sub

    Protected Sub radViewedUsers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radViewedUsers.CheckedChanged, radPendingUsers.CheckedChanged, radSummaryUsers.CheckedChanged
        SetupReportType()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
