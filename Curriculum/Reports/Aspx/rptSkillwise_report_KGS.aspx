﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSkillwise_report_KGS.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptSkillwise_report_KGS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Skillwise Report KGS"></asp:Label>
            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Report Printed For</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlReportPrintedFor" runat="server">
                            </asp:DropDownList></td>

                        <td align="left" valign="middle"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle"><span class="field-label">Section</span>
                        </td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                        <td align="left"><span class="field-label">Criteria</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCriteria" runat="server" AutoPostBack="true">
                                <asp:ListItem Text="Percentage Above" Value=">=" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Percentage Below" Value="<="></asp:ListItem>
                                <asp:ListItem Text="Percentage Between" Value="between"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Mark From</span></td>
                        <td>
                            <asp:TextBox ID="txtVal1" runat="server" Text="70">
                            </asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblAnd" runat="server" Text=" Mark To " CssClass="field-label" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtVal2" runat="server" Text="100" Visible="false">
                            </asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" style="text-align: center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                OnClick="btnGenerateReport_Click" />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                                Text="Download Report in Pdf" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfbDownload" runat="server" />
                <asp:HiddenField ID="hfReportType" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfReportFormat" runat="server"></asp:HiddenField>

                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>
