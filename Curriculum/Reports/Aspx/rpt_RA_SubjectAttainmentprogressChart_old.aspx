﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rpt_RA_SubjectAttainmentprogressChart_old.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_SubjectAttainmentprogressChart" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

     <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table class ="matters" align="center" border="1" bordercolor="#1b80b6" cellpadding="4" cellspacing="0" >
        <tr>
            <td valign="middle" class="subheader_img" colspan="3">
                <asp:Label id="lblHeader" runat="server" Text="Progress Report"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                Academic Year</td>
            <td align="center">
                :</td>
            <td align="left">
                <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" 
                   >
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                Report Card</td>
            <td align="center" >
                :</td>
            <td align="left">
                <asp:DropDownList id="ddlReportCard" runat="server" 
                    Width="331px" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                Schedule</td>
            <td align="center">
                :</td>
            <td align="left">
                            <asp:CheckBoxList id="ddlPrintedFor" runat="server" 
                    BorderStyle="Solid" BorderWidth="1px"
                                Height="127px" RepeatLayout="Flow" style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                                vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                                text-align: left" Width="239px">
                            </asp:CheckBoxList></td>
        </tr>
        <tr id ="20" >
            <td align="left" valign="middle">
                Grade</td>
            <td align="center" valign="middle">
                :</td>
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True" 
                    Width="114px">
                </asp:DropDownList>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign ="top">
                Subject</td>
            <td align="center" valign ="top">
                :</td>
            <td align="left"  valign ="top">
                <asp:DropDownList ID="ddlSubject" runat="server" 
                    AutoPostBack="True"  Width="214px" >
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button" 
                    Text="Generate Report" OnClick="btnGenerateReport_Click" Width="126px" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" 
                    Width="95px" /></td>
        </tr>
    </table>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="hfbFinalReport" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_STU_IDs" runat="server"/>
    <asp:HiddenField  id="h_SBG_IDs" runat="server"/>
</asp:Content>

