Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_Reports_Aspx_rptMarkEntryList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C970050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                    If Session("CurrSuperUser") = "Y" Then
                        lblGrp.Text = "Select"
                    Else
                        lblGrp.Text = "Select Group"
                        rdGroup.Visible = False
                        rdSection.Visible = False
                    End If
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                    BindTerm()

                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGrade.Items.Insert(0, li)


                    'ddlSubject = currFns.PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    'ddlSubject = PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlSubject.Items.Add(li)

                    'ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString)
                    ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGroup.Items.Insert(0, li)



                    ddlActivity = currFns.PopulateActivityMaster(ddlActivity, Session("sbsuid"))
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlActivity.Items.Insert(0, li)


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

    Sub SelectSearches(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("accSearch_m") Is Nothing Then
            If Not ddlAcademicYear.Items.FindByValue(Session("accSearch_m")) Is Nothing Then
                ddlAcademicYear.ClearSelection()
                ddlAcademicYear.Items.FindByValue(Session("accSearch_m")).Selected = True
                ddlAcademicYear_SelectedIndexChanged(sender, e)
            End If
        End If
        If Not Session("trmSearch_m") Is Nothing Then
            If Not ddlTerm.Items.FindByValue(Session("trmSearch_m")) Is Nothing Then
                ddlTerm.ClearSelection()
                ddlTerm.Items.FindByValue(Session("trmSearch_m")).Selected = True
            End If
        End If
        If Not Session("grdSearch_m") Is Nothing Then
            If Not ddlGrade.Items.FindByValue(Session("grdSearch_m")) Is Nothing Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(Session("grdSearch_m")).Selected = True
                ddlGrade_SelectedIndexChanged(sender, e)
            End If
        End If
        If Not Session("sbgSearch_m") Is Nothing Then
            If Not ddlSubject.Items.FindByValue(Session("sbgSearch_m")) Is Nothing Then
                ddlSubject.ClearSelection()
                ddlSubject.Items.FindByValue(Session("sbgSearch_m")).Selected = True
                ddlSubject_SelectedIndexChanged(sender, e)
            End If
        End If
        If Not Session("grpSearch_m") Is Nothing Then
            If Not ddlGroup.Items.FindByValue(Session("grpSearch_m")) Is Nothing Then
                ddlGroup.ClearSelection()
                ddlGroup.Items.FindByValue(Session("grpSearch_m")).Selected = True
            End If
        End If

        If Not Session("asmSearch_m") Is Nothing Then
            If Not ddlActivity.Items.FindByValue(Session("asmSearch_m")) Is Nothing Then
                ddlActivity.ClearSelection()
                ddlActivity.Items.FindByValue(Session("asmSearch_m")).Selected = True
            End If
        End If
    End Sub
    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlTerm.Items.Insert(0, li)
        li = New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlTerm.Items.Add(li)
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Protected Sub lblMarks_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblActivity As Label
            Dim lblSubject As Label
            Dim lblCasId As Label
            Dim lblGroup As Label
            Dim lblDate As Label
            Dim lblMentered As Label
            Dim lblGradeSlabId As Label
            Dim lblMinMarks As Label
            Dim lblMaxMarks As Label
            Dim lblEntry As Label

            With sender
                lblActivity = TryCast(.FindControl("lblActivity"), Label)
                lblSubject = TryCast(.FindControl("lblSubject"), Label)
                lblCasId = TryCast(.FindControl("lblCasId"), Label)
                lblGroup = TryCast(.FindControl("lblGroup"), Label)
                lblDate = TryCast(.FindControl("lblDate"), Label)
                lblMentered = TryCast(.FindControl("lblMentered"), Label)
                lblGradeSlabId = TryCast(.FindControl("lblGradeSlabId"), Label)
                lblMinMarks = TryCast(.FindControl("lblMinMarks"), Label)
                lblMaxMarks = TryCast(.FindControl("lblMaxMarks"), Label)
                lblEntry = TryCast(.FindControl("lblEntry"), Label)
            End With
            Dim url As String
            url = String.Format("~\Curriculum\clmMarkEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                               & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                               & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                               & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                               & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                               & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                               & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                               & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text) _
                               & "&maxmarks=" + Encr_decrData.Encrypt(lblMaxMarks.Text) _
                               & "&entry=" + Encr_decrData.Encrypt(lblEntry.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Protected Sub lblAttendance_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            ViewState("datamode") = Encr_decrData.Encrypt("edit")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblActivity As Label
            Dim lblSubject As Label
            Dim lblCasId As Label
            Dim lblGroup As Label
            Dim lblDate As Label
            Dim lblMentered As Label
            Dim lblGradeSlabId As Label
            Dim lblMinMarks As Label
            Dim lblMaxMarks As Label
            Dim lblEntry As Label

            With sender
                lblActivity = TryCast(.FindControl("lblActivity"), Label)
                lblSubject = TryCast(.FindControl("lblSubject"), Label)
                lblCasId = TryCast(.FindControl("lblCasId"), Label)
                lblGroup = TryCast(.FindControl("lblGroup"), Label)
                lblDate = TryCast(.FindControl("lblDate"), Label)
                lblMentered = TryCast(.FindControl("lblMentered"), Label)
                lblGradeSlabId = TryCast(.FindControl("lblGradeSlabId"), Label)
                lblMinMarks = TryCast(.FindControl("lblMinMarks"), Label)
                lblMaxMarks = TryCast(.FindControl("lblMaxMarks"), Label)
                lblEntry = TryCast(.FindControl("lblEntry"), Label)
            End With

            Dim markurl As String
            markurl = String.Format("~\Curriculum\clmMarkEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                               & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                               & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                               & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                               & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                               & "&menterd=" + Encr_decrData.Encrypt(lblMentered.Text) _
                               & "&gradeslab=" + Encr_decrData.Encrypt(lblGradeSlabId.Text) _
                               & "&minmarks=" + Encr_decrData.Encrypt(lblMinMarks.Text) _
                               & "&maxmarks=" + Encr_decrData.Encrypt(lblMaxMarks.Text) _
                               & "&entry=" + Encr_decrData.Encrypt(lblEntry.Text), ViewState("MainMnu_code"), ViewState("datamode"))



            Dim url As String
            url = String.Format("~\Curriculum\clmATTEntry_M.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&casid=" + Encr_decrData.Encrypt(lblCasId.Text) _
                               & "&activity=" + Encr_decrData.Encrypt(lblActivity.Text) _
                               & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                               & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                               & "&date=" + Encr_decrData.Encrypt(lblDate.Text) _
                               & "&markurl=" + Encr_decrData.Encrypt(markurl), ViewState("MainMnu_code"), ViewState("datamode"))



            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
  

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function getReportQuery() As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim strFilter As String = ""

        If rdGroup.Checked = True Then
            If Session("CurrSuperUser") = "Y" Then
                If ddlActivity.SelectedValue = "" Then

                    str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                                 & " CAS_TIME,CAS_DESC,SBG_DESCR,SGR_DESCR,isnull(CAS_MARKENTERED,'No')" _
                                 & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                                 & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                                 & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                                 & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK," _
                                 & " ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE,SBG_GRD_ID,STM_DESCR,STM_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                                 & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                                 & " INNER JOIN GROUPS_M AS C ON A.CAS_SGR_ID=C.SGR_ID" _
                                 & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                                 & " INNER JOIN OASIS..STREAM_M AS Q ON B.SBG_STM_ID=Q.STM_ID" _
                                 & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
                Else
                    str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                                & " CAS_TIME,CAS_DESC,SBG_DESCR,SGR_DESCR,isnull(CAS_MARKENTERED,'No')" _
                                & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                                & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                                & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                                & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK,ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE,SBG_GRD_ID,STM_DESCR,STM_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                                & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                                & " INNER JOIN GROUPS_M AS C ON A.CAS_SGR_ID=C.SGR_ID" _
                                & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                                & " INNER JOIN ACT.ACTIVITY_M AS F ON E.CAD_CAM_ID=F.CAM_ID" _
                                & " INNER JOIN OASIS..STREAM_M AS Q ON B.SBG_STM_ID=Q.STM_ID" _
                                & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND CAM_ID=" + ddlActivity.SelectedValue.ToString
                End If
            Else
                If ddlActivity.SelectedValue = "" Then
                    str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                                 & " CAS_TIME,CAS_DESC,SBG_DESCR,SGR_DESCR,isnull(CAS_MARKENTERED,'No')" _
                                 & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                                 & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                                 & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                                 & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK,ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE,SBG_GRD_ID,STM_DESCR,STM_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                                 & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                                 & " INNER JOIN GROUPS_M AS C ON A.CAS_SGR_ID=C.SGR_ID" _
                                 & " INNER JOIN GROUPS_TEACHER_S AS D ON D.SGS_SGR_ID=C.SGR_ID" _
                                 & " INNER JOIN OASIS..STREAM_M AS Q ON B.SBG_STM_ID=Q.STM_ID" _
                                 & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
                Else
                    str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                                & " CAS_TIME,CAS_DESC,SBG_DESCR,SGR_DESCR,isnull(CAS_MARKENTERED,'No')" _
                                & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                                & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                                & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                                & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK,ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE,SBG_GRD_ID,STM_DESCR,STM_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                                & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                                & " INNER JOIN GROUPS_M AS C ON A.CAS_SGR_ID=C.SGR_ID" _
                                & " INNER JOIN GROUPS_TEACHER_S AS D ON D.SGS_SGR_ID=C.SGR_ID" _
                                & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                                & " INNER JOIN ACT.ACTIVITY_M AS F ON E.CAD_CAM_ID=F.CAM_ID" _
                                & " INNER JOIN OASIS..STREAM_M AS Q ON B.SBG_STM_ID=Q.STM_ID" _
                                & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                                & " AND CAM_ID=" + ddlActivity.SelectedValue.ToString
                End If
            End If


        Else


                 If ddlActivity.SelectedValue = "" Then

                str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                             & " CAS_TIME,CAS_DESC,SBG_DESCR,SCT_DESCR AS SGR_DESCR,isnull(CAS_MARKENTERED,'No')" _
                             & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                             & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                             & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                             & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK," _
                             & " ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE,SBG_GRD_ID,STM_DESCR,STM_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                             & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                             & " INNER JOIN oasis..GRADE_BSU_M AS F ON B.SBG_GRD_ID=F.GRM_GRD_ID AND B.SBG_STM_ID=F.GRM_STM_ID" _
                             & " INNER JOIN oasis..SECTION_M AS C ON F.GRM_ID=C.SCT_GRM_ID" _
                             & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                             & " INNER JOIN OASIS..STREAM_M AS Q ON B.SBG_STM_ID=Q.STM_ID" _
                             & " WHERE SCT_DESCR<>'TEMP' AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
            Else
                str_query = "SELECT DISTINCT CAS_ID,CAS_GSM_SLAB_ID,CAS_SGR_ID,CAS_SBG_ID,CAS_DATE," _
                            & " CAS_TIME,CAS_DESC,SBG_DESCR,SCT_DESCR AS SGR_DESCR,isnull(CAS_MARKENTERED,'No')" _
                            & " AS MARKS,CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN 'Yes' ELSE 'No' END AS ATT ," _
                            & " CASE CAS_bATT_MANDATORY WHEN 'TRUE' THEN CASE CAS_bATT_ENTERED WHEN 'TRUE' THEN " _
                            & " 'TRUE' ELSE 'FALSE' END ELSE 'TRUE' END AS bMARKS,CAS_TYPE_LEVEL,ISNULL(CAS_GSM_SLAB_ID,0) AS CAS_GSM_SLAB_ID," _
                            & " ISNULL(CAS_MIN_MARK,0) AS CAS_MIN_MARK,ISNULL(CAS_MAX_MARK,100) AS CAS_MAX_MARK,ISNULL(SBG_ENTRYTYPE,'Mark') AS SBG_ENTRYTYPE,SBG_GRD_ID,STM_DESCR,STM_ID FROM ACT.ACTIVITY_SCHEDULE AS A INNER JOIN" _
                            & " SUBJECTS_GRADE_S AS B ON A.CAS_SBG_ID=B.SBG_ID" _
                            & " INNER JOIN oasis..GRADE_BSU_M AS F ON B.SBG_GRD_ID=F.GRM_GRD_ID AND B.SBG_STM_ID=F.GRM_STM_ID" _
                            & " INNER JOIN oasis..SECTION_M AS C ON F.GRM_ID=C.SCT_GRM_ID" _
                            & " INNER JOIN ACT.ACTIVITY_D AS E ON A.CAS_CAD_ID=E.CAD_ID" _
                            & " INNER JOIN ACT.ACTIVITY_M AS F ON E.CAD_CAM_ID=F.CAM_ID" _
                            & " INNER JOIN OASIS..STREAM_M AS Q ON B.SBG_STM_ID=Q.STM_ID" _
                            & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                            & " AND SCT_DESCR<>'TEMP' AND CAM_ID=" + ddlActivity.SelectedValue.ToString
            End If
           



        End If
        If (ddlGrade.SelectedValue <> "") Then

            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            If rdGroup.Checked = True Then
                str_query += " AND SGR_GRD_ID='" + grade(0) + "'"
                str_query += " AND SBG_STM_ID=" + grade(1)
            Else
                str_query += " AND GRM_GRD_ID='" + grade(0) + "'"
                str_query += " AND GRM_STM_ID=" + grade(1)
            End If
            'str_query += " AND SGR_GRD_ID='" & ddlGrade.SelectedValue & "'"
        End If

            If ddlTerm.SelectedValue <> "" Then
                str_query += " AND CAD_TRM_ID=" + ddlTerm.SelectedValue.ToString
            End If

            If ddlGroup.SelectedValue <> "" Then
                If rdGroup.Checked = True Then
                    str_query += " AND SGR_ID=" + ddlGroup.SelectedValue
                Else
                    str_query += " AND SCT_ID=" + ddlGroup.SelectedValue.ToString
                End If
            End If



            If ddlSubject.SelectedValue <> "" Then
                str_query += " AND SBG_ID=" + ddlSubject.SelectedValue
            End If


            If ddlMarks.Text <> "ALL" Then
                strFilter += "  AND ISNULL(CAS_MARKENTERED,'NO')='" + ddlMarks.SelectedValue + "'"
            End If

            str_query += strFilter


        str_query += " ORDER BY SBG_GRD_ID,STM_DESCR,SBG_DESCR "
            Return str_query
    End Function
    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@SEARCHQUERY", getReportQuery)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("academicyear", ddlAcademicYear.SelectedItem.Text)
        If rdGroup.Checked = True Then
            param.Add("grHeader", "Group")
        Else
            param.Add("grHeader", "Section")
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptMarkEntryList.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateGroups(ByVal ddlGroup As DropDownList, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")

        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID=" + acd_id

        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID=" + sbg_id
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + grd_id + "'"
        End If
        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        Return ddlGroup
    End Function


    Public Function PopulateSubject(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""

        Dim str_sql As String = ""
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
        '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
        '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                      & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition = " AND SBG_GRD_ID='" & Grdid & "'"
        End If
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_sql = "Select distinct * from (SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & acdid & "'" _
                          & " " & strCondition & " "
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
        Else

            str_sql = " Select distinct * from (SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & acdid & "'"
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
            'If v_GradeID <> "" Then
            '    str_sql += "AND SBG_GRD_ID IN ('" & v_GradeID & "') ORDER BY GRM_DISPLAY "
            'End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        'ddl.DataTextField = "grm_display"
        'ddl.DataValueField = "grm_grd_id"
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function
    Public Function PopulateGroup(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "", Optional ByVal Subjid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
        '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
        '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                      & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition += " AND SGR_GRD_ID='" & Grdid & "'"
        End If
        If Subjid <> "ALL" And Subjid <> "" Then
            strCondition += " AND SGR_SBG_ID='" & Subjid & "'"
        End If
        'If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
        '    strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "'"
        '    str_sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER " _
        '            & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
        '            & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID INNER JOIN GROUPS_TEACHER_S ON SGR_ID= SGS_SGR_ID " _
        '            & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & acdid & "' " & strCondition & " AND VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "' " _
        '            & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        'Else

        '    str_sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER " _
        '            & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
        '            & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID " _
        '            & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & acdid & "' AND VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "' " _
        '            & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
        'End If
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL  AND SGR_ACD_ID='" + acdid + "'"
            'If vSBM_IDs <> "" Then
            'str_query_header += "WHERE SGR_SBG_ID IN ('" & vSBM_IDs.Replace("___", "','") & "') "
            'str_query_header += ""
            'Else
            'str_query_header += " WHERE 1=1 "
            'End If
            'str_sql = str_query_header.Split("||")(0)
            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + acdid + "'"
            'AND SGR_GRD_ID='" & strGRD_IDs & "' AND SGR_SBG_ID=" & vSBM_IDs & " ORDER BY SGR_ID "
            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        'ddl.DataTextField = "grm_display"
        'ddl.DataValueField = "grm_grd_id"
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function
    Private Sub PopulateSection()
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        If ddlGrade.SelectedValue <> "" Then
            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_DESCR<>'TEMP' AND SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                     & " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) + ") AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                     & " ORDER BY SCT_DESCR "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlGroup.DataSource = ds
            ddlGroup.DataTextField = "SCT_DESCR"
            ddlGroup.DataValueField = "SCT_ID"
            ddlGroup.DataBind()
        End If
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGroup.Items.Insert(0, li)
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            BindTerm()

            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGrade.Items.Insert(0, li)


            'ddlSubject = currFns.PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            'ddlSubject = PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlSubject.Items.Add(li)

            'ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString)
            If rdGroup.Checked = True Then
                ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
                li = New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlGroup.Items.Insert(0, li)
            Else
                PopulateSection()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            Dim li As ListItem
            If ddlGrade.SelectedValue <> "" Then
                Dim grade As String()
                grade = ddlGrade.SelectedValue.Split("|")
                If Session("CurrSuperUser") = "Y" Then
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

                Else
                    ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                End If
                li = New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlSubject.Items.Insert(0, li)

                If rdGroup.Checked = True Then
                    If Session("CurrSuperUser") = "Y" Then
                        ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0))
                    Else
                        ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0))
                    End If
                
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGroup.Items.Insert(0, li)
                Else
                    PopulateSection()
                End If
            Else
                ddlSubject.Items.Clear()
                li = New ListItem
                li.Text = "ALL"
                li.Value = ""
                ddlSubject.Items.Add(li)
                If rdGroup.Checked = True Then
                    If Session("CurrSuperUser") = "Y" Then
                        ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString)
                    End If

                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGroup.Items.Insert(0, li)
                Else
                    PopulateSection()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        Try
            Dim li As ListItem
            Dim grade() As String = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
                ddlGroup = PopulateGroups(ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Else
                ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            End If
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGroup.Items.Insert(0, li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub




    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub rdGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGroup.CheckedChanged
        If rdGroup.Checked = True Then
            Dim li As New ListItem
            ddlGroup = PopulateGroup(ddlGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGroup.Items.Insert(0, li)

        End If
    End Sub

    Protected Sub rdSection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdSection.CheckedChanged
        If rdSection.Checked = True Then
            PopulateSection()
        End If
    End Sub
End Class
