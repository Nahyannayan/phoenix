﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTargetTrackerGraph.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptTargetTrackerGraph" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
          var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
          if (GRD_IDs == '') {
              alert('Please select atleast one Grade')
              return false;
          }
          var oWnd = radopen(url, "pop_form");
          <%--result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs.split('|')[0] + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "", sFeatures)
          if (result != '' && result != undefined) {
              document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }--%>
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
           
            var arg = args.get_argument();
          
            if (arg) {                               
                //NameandCode = arg.Student.split('||');
                alert(arg.NameCode);
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = arg.NameCode;//NameandCode[0];
            }

        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <script language="javascript" type="text/javascript">


        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);

            }
        }


        function OnTreeClick1(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }


        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }


        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;
            if (parentNodeTable) {
                var checkUncheckSwitch;
                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }
                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) {
                    //check if the child node is an element node
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

        function UnCheckAll() {
            var oTree = document.getElementById("<%=tvReport.ClientID%>");
    childChkBoxes = oTree.getElementsByTagName("input");
    var childChkBoxCount = childChkBoxes.length;
    for (var i = 0; i < childChkBoxCount; i++) {
        childChkBoxes[i].checked = false;
    }

    return true;
}


function printTable() {

    var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
    disp_setting += "scrollbars=yes,width=1280, height=600, left=100, top=25";
    var content_vlue = document.getElementById('tblChart').innerHTML;
    var docprint = window.open("", "", disp_setting);
    docprint.document.open();
    docprint.document.write('<html><head>');
    docprint.document.write('<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />');
    docprint.document.write('</head><body onLoad="self.print();self.close();" style="margin-top:0px;margin-left:0px;margin-right:0px;" ><center>');
    docprint.document.write(content_vlue);
    docprint.document.write('</center></body></html>');
    docprint.document.close();
    docprint.focus();
}

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Target Tracker Graph
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblClm" runat="server" align="center" cellpadding="0" cellspacing="0"  width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>                                   
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%" ><span class="field-label">Grade</span></td>                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Select Section</span></td>                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%" ><span class="field-label">Select Report Schedule</span></td>                                    
                                    <td align="left" width="30%" >
                                        <div class="checkbox-list">
                                        <asp:TreeView ID="tvReport" runat="server" 
                                            ExpandDepth="1" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                                            PopulateNodesFromClient="False" ShowCheckBoxes="All" 
                                            >
                                            <ParentNodeStyle Font-Bold="False" />
                                            <HoverNodeStyle Font-Underline="True" />
                                            <SelectedNodeStyle BackColor="White" 
                                                />
                                            <NodeStyle 
                                                />
                                        </asp:TreeView></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Subject</span></td>                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true" >
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%" ><span class="field-label">Group</span></td>                                    
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlGroup" AutoPostBack="true" runat="server"
                                            >
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Student ID</span></td>                                    
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStudentID" runat="server" > </asp:TextBox></td>
                                    <td align="left" width="20%" ><span class="field-label">Student Name</span></td>                                    
                                    <td align="left" width="30%" >
                                        <asp:TextBox ID="txtStudentName" runat="server" > </asp:TextBox></td>
                                   
                                </tr>
                                <tr>
                                     <td colspan="4" align="center">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"  />
                                    </td>
                                </tr>
                                <tr id="trChart" runat="server">
                                    <td colspan="4" align="center">
                                        <table id="tblChart" runat="server" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <%-- <asp:GridView ID="gvStudent" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                                        AllowPaging="True" AllowSorting="True" PageSize="1"  BackImageUrl="~/Images/bg-menu-main.png">
                                                        <PagerSettings Mode="NextPrevious" NextPageText="Next Student" PreviousPageText="Previous Student"
                                                            Position="Top" PageButtonCount="100" />--%>
                                                    <asp:GridView ID="gvStudent" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                                        AllowPaging="True" AllowSorting="True" PageSize="1">
                                                        <PagerSettings Mode="NextPrevious" NextPageText="Next Student" PreviousPageText="Previous Student"
                                                            Position="Top" PageButtonCount="100" />
                                                        <RowStyle CssClass="griditem" />
                                                        <PagerStyle
                                                            HorizontalAlign="Right" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <Columns>
                                                            <asp:TemplateField Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuID" runat="server" Text='<%# Eval("Stu_ID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuNo" runat="server" Text='<%# Eval("Stu_No") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuPhotoPath" Visible="false" runat="server" Text='<%# Eval("STU_PHOTOPATH") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuName" runat="server" Text='<%# Eval("Stu_Name") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>

                                                </td>
                                                <td align="center">
                                                    <asp:Image ID="imgStud" runat="server"  ImageUrl="~/Images/Photos/no_image.gif"
                                                         Visible="False" /></td>
                                                

                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Chart ID="Chart1" runat="server" Palette="BrightPastel"  ImageLocation="~/Images/bgbig.gif " width="800px" Height="600px">
                                                        <Legends>
                                                            <asp:Legend IsTextAutoFit="False" Name="Legend1" BackColor="Transparent" ></asp:Legend>
                                                        </Legends>
                                                        <BorderSkin SkinStyle="Emboss"></BorderSkin>
                                                        <Series>
                                                            <asp:Series Name="S1"></asp:Series>
                                                            <asp:Series Name="S2"></asp:Series>
                                                        </Series>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                                            </asp:ChartArea>
                                                            <asp:ChartArea Name="ChartArea2" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                                            </asp:ChartArea>
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </td>
                                            </tr>
                                        </table>
                                              <asp:Button ID="btnPrint" runat="server" Text="Save as Image" CssClass="button" TabIndex="4"
                                                         />
                                               
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfPageindex" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>

</asp:Content>

