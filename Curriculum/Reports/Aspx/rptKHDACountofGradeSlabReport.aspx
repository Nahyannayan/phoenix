﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptKHDACountofGradeSlabReport.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptKHDACountofGradeSlabReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


<script language="javascript" type="text/javascript">
    function fnSelectAll2(master_box) {
        var curr_elem;
        var checkbox_checked_status;
        for (var i = 0; i < document.forms[0].elements.length; i++) {
            curr_elem = document.forms[0].elements[i];
            if (curr_elem.id.substring(0, 28) == 'ctl00_cphMasterpage_cblGrade') {
                curr_elem.checked = master_box.checked;
            }
        }
        // master_box.checked=!master_box.checked;
    }
    function fnSelectAll1(master_box) {
        var curr_elem;
        var checkbox_checked_status;
        for (var i = 0; i < document.forms[0].elements.length; i++) {
            curr_elem = document.forms[0].elements[i];
            if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_cblSubject') {
                curr_elem.checked = master_box.checked;
            }
        }
        // master_box.checked=!master_box.checked;
    }
</script>
   <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Analysis Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
 <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" >
                 
               <tr >
              
                <td width="100%" align="left" class="title" >
                  <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
              </tr>
             
           <tr>
            <td align="center"    valign="top">
                
                <table id="tblClm" runat="server" align="center"  cellpadding="5" cellspacing="5"   style="width: 100%">
                    
                      <tr>
                        <td align="left" >
                            <span class="field-label">Select Academic Year</span></td>
                       
                        <td align="left"  >
                             <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>               
                      </td>
                       </tr>   
                    <tr>
                        <td align="left"  >
                            <span class="field-label">Grade</span></td>
                        
                        <td align="left"  >
                             <div class="checkbox-list">
                        <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll2(this);" runat="server"
                    Text="Select All" AutoPostBack="True" CssClass="field-label"/><br />
                        <asp:CheckBoxList id="cblGrade" runat="server" AutoPostBack="True"  
                               RepeatLayout="Flow"   >
                            </asp:CheckBoxList></div></td>
                       
                   
                    <%--<tr>
                        <td align="left"  style="width: 184px">
                            Select Section</td>
                        <td >
                            :</td>
                        <td align="left"  >
                            <asp:DropDownList ID="ddlSection" AutoPostBack="true" runat="server" Width="107px">
                            </asp:DropDownList></td>
                    </tr>--%>
                    
                        <td align="left"  >
                            <span class="field-label">Select Subject</span></td>
                       
                        <td align="left"  >
                            <div class="checkbox-list">
                         <asp:CheckBox ID="chkSubject" onclick="javascript:fnSelectAll1(this);" runat="server"
                    Text="Select All" AutoPostBack="True"  CssClass="field-label"/><br />
                           <asp:CheckBoxList id="cblSubject" runat="server"  
                                  RepeatLayout="Flow"  >
                            </asp:CheckBoxList></div></td>
                    </tr>
                    
                    <tr>
                        <td align="left"  >
                            <span class="field-label">Report Schedule</span></td>
                        
                        <td align="left"  >
                            <asp:DropDownList ID="ddlCurSchedule" AutoPostBack="true" runat="server" Width="183px">
                            </asp:DropDownList></td>
                   
                    <%--<tr>
                        <td align="left"  style="width: 184px" >
                            Previous Report Schedule</td>
                        <td >
                            :</td>
                        <td align="left"  >
                            <asp:DropDownList ID="ddlPrevSchedule" AutoPostBack="true" runat="server" Width="183px">
                            </asp:DropDownList></td>
                    </tr>--%>
                  
                        <td align="left"  >
                            <span class="field-label">Select Header</span></td>
                       
                        <td align="left"  >
                            <asp:DropDownList ID="ddlHeader" AutoPostBack="true" runat="server" Width="183px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td  align="center" colspan="4">
                            <asp:Button id="btnGenerateReport" runat="server" CssClass="button" 
                                Text="Generate Report" ValidationGroup="groupM1" />
                                <asp:Button id="btnGeneratePDF" runat="server" CssClass="button" 
                                Text="Download PDF" ValidationGroup="groupM1" />
                                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                                </td>
                    </tr>
                    
                    </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server"
                        type="hidden" value="=" />
                        
                        <input id="hfbDownload" runat="server"
                        type="hidden" value="=" />
               </td></tr>
           
    
</table>
                </div>
            </div>
       </div>
    <asp:HiddenField id="h_STU_IDs" runat="server">
    </asp:HiddenField>
     
 <asp:HiddenField id="h_MnuCode" runat="server">
    </asp:HiddenField>

</asp:Content>
