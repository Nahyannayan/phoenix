Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_Reports_Aspx_rptSubjectPerformanceByTeacher
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C280090") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                PopulateGrade(lstGrade, ddlAcademicYear.SelectedValue.ToString)
                BindSubject()
                BindTeacher()

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function PopulateGrade(ByVal ddl As CheckBoxList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid


        str_query += " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function


    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim str_query As String = "SELECT DISTINCT SBM_DESCR SBG_DESCR,SBG_SBM_ID FROM SUBJECT_M INNER JOIN SUBJECTS_GRADE_S ON SUBJECTS_GRADE_S.SBG_SBM_ID = SUBJECT_M.SBM_ID WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If hfGrade.Value <> "" Then
            str_query += " AND SBG_GRD_ID+'|'+convert(varchar(100),SBG_STM_ID) IN(" + hfGrade.Value + ")"
        End If

        str_query += " ORDER BY SBM_DESCR"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub


    Sub BindTeacher()
        lstTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT DISTINCT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') AS EMP_NAME," _
                                & " EMP_FNAME, EMP_MNAME, EMP_LNAME, EMP_ID" _
                                & " FROM EMPLOYEE_M AS A INNER JOIN " _
                                & " GROUPS_TEACHER_S AS B ON A.EMP_ID=B.SGS_EMP_ID AND SGS_TODATE IS NULL" _
                                & " INNER JOIN GROUPS_M AS C ON B.SGS_SGR_ID=C.SGR_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.SGR_SBG_ID=D.SBG_ID" _
                                & " WHERE SGR_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " AND SBG_SBM_ID='" + ddlSubject.SelectedValue.ToString + "'" _
                                & " AND EMP_STATUS=1"

        If hfGrade.Value <> "" Then
            str_query += " AND SBG_GRD_ID+'|'+convert(varchar(100),SBG_STM_ID) IN(" + hfGrade.Value + ")"
        End If


        Dim str_query1 As String = "SELECT DISTINCT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') AS EMP_NAME," _
                               & " EMP_FNAME, EMP_MNAME, EMP_LNAME, EMP_ID" _
                               & " FROM EMPLOYEE_M AS A INNER JOIN " _
                               & " GROUPS_TEACHER_S AS B ON A.EMP_ID=B.SGS_EMP_ID AND SGS_TODATE IS NULL" _
                               & " INNER JOIN GROUPS_M AS C ON B.SGS_SGR_ID=C.SGR_ID" _
                               & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.SGR_SBG_ID=D.SBG_ID" _
                               & " INNER JOIN SUBJECTS_GRADE_S AS E ON D.SBG_PARENT_ID=E.SBG_ID" _
                               & " WHERE SGR_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                               & " AND E.SBG_SBM_ID='" + ddlSubject.SelectedValue.ToString + "'" _
                               & " AND EMP_STATUS=1"

        If hfGrade.Value <> "" Then
            str_query1 += " AND D.SBG_GRD_ID+'|'+convert(varchar(100),D.SBG_STM_ID) IN(" + hfGrade.Value + ")"
        End If


        str_query = "SELECT * FROM (" + str_query + " UNION ALL " + str_query1 + ")PP"

        str_query += " ORDER BY EMP_FNAME,EMP_LNAME,EMP_MNAME"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstTeacher.DataSource = ds
        lstTeacher.DataTextField = "emp_name"
        lstTeacher.DataValueField = "emp_id"
        lstTeacher.DataBind()
    End Sub

    Sub CallReport()

        Dim empIDs As String = ""
        Dim empTempIDs As String = ""
        Dim i As Integer



        For i = 0 To lstTeacher.Items.Count - 1
            If lstTeacher.Items(i).Selected = True Then
                If empIDs <> "" Then
                    empIDs += "|"
                End If
                empIDs += lstTeacher.Items(i).Value
            End If
            If empTempIDs <> "" Then
                empTempIDs += "|"
            End If
            empTempIDs += lstTeacher.Items(i).Value
        Next

        If empIDs = "" Then
            empIDs = empTempIDs
        End If

        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@SBM_ID", ddlSubject.SelectedValue.ToString)
        param.Add("@EMP_IDS", empIDs)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("SUBJECT", ddlSubject.SelectedItem.Text)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("@GRD_IDS", hfGrade.Value.Replace("'", ""))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_SUBJECT_TEACHER_COMPARISON.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub GenerateSubjectListByTeacherReport()

        Dim empXML As String = ""
        Dim empTempXML As String = ""
        Dim i As Integer
        For i = 0 To lstTeacher.Items.Count - 1
            If lstTeacher.Items(i).Selected = True Then
                empXML += lstTeacher.Items(i).Value & "|"
            End If
            empTempXML += lstTeacher.Items(i).Value + "|"
        Next
        If empXML = "" Then
            empXML = empTempXML
        End If
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@EMP_ID", empXML)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptSubjectListByTeacher.rpt")
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub
#End Region


    Protected Sub chkTeacher_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTeacher.CheckedChanged
        If chkTeacher.Checked = True Then
            For i As Integer = 0 To lstTeacher.Items.Count - 1
                lstTeacher.Items(i).Selected = True
            Next
        Else
            lstTeacher.ClearSelection()
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade(lstGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSubject()
        BindTeacher()

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
          CallReport()


    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindTeacher()
    End Sub

    Protected Sub lstGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGrade.SelectedIndexChanged
        Dim i As Integer
        Dim str As String = ""

        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += "'" + lstGrade.Items(i).Value + "'"
            End If
        Next

        hfGrade.Value = str

        BindSubject()
        BindTeacher()
    End Sub

    Protected Sub chkGrade_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGrade.CheckedChanged
        Dim i As Integer
        Dim str As String = ""

        For i = 0 To lstGrade.Items.Count - 1
            If chkGrade.Checked = True Then
                If str <> "" Then
                    str += ","
                End If
                str += "'" + lstGrade.Items(i).Value + "'"
                lstGrade.Items(i).Selected = True
            Else
                lstGrade.Items(i).Selected = False
            End If
        Next

        hfGrade.Value = str

        BindTeacher()
        BindSubject()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
