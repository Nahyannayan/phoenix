﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rpt_RA_SummativeResultAnalysisBySection.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_SummativeResultAnalysisBySection" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Summative Result Analysis By Section"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table class="BlueTable" id="tblrule" runat="server" width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Report Card</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="matters"><span class="field-label">Report Schedule</span></td>
                        <td align="left" valign="middle" class="matters">
                            <asp:DropDownList ID="ddlPrintedFor" runat="server">
                            </asp:DropDownList><br />
                            <asp:CheckBox ID="chkOverAll" runat="server" Text="Overall Grade" CssClass="field-label"></asp:CheckBox>
                        </td>
                        <td align="left" valign="middle" class="matters"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle" class="matters">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trSubject" runat="server">
                        <td align="left" valign="middle" class="matters"><span class="field-label">Subject</span></td>
                        <td align="left" valign="middle" class="matters">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                            </asp:DropDownList>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

