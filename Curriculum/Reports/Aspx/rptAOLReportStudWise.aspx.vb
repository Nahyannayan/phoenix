Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class clmActivitySchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C304005") AndAlso (ViewState("MainMnu_code") <> "StudentProfile")) _
                Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")
                    GetAllGrade()
                    BindActivity()
                    BindSubActivity()
                    BindSubjects(ddlGrade.SelectedValue)
                    BindSubjectGroup()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub
    Sub BindSubActivity()
        ddlSubActivity.DataSource = GetActivityDetails(ddlAca_Year.SelectedValue, ddlActivity.SelectedValue)
        ddlSubActivity.DataTextField = "CAD_DESC"
        ddlSubActivity.DataValueField = "CAD_ID"
        ddlSubActivity.DataBind()
    End Sub

    Public Function GetActivityDetails(ByVal vACD_ID As String, Optional ByVal vCAM_ID As String = "") As DataSet
        Dim str_sql As String = String.Empty
        str_sql = "SELECT CAD_ID, CAD_DESC FROM ACT.ACTIVITY_D " & _
        " WHERE CAD_ACD_ID= '" & vACD_ID & "' "
        If vCAM_ID <> "" Then
            str_sql += " AND CAD_CAM_ID = " & vCAM_ID
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Public Function GetActivity() As DataSet
        Dim str_sql As String = String.Empty
        str_sql = "SELECT CAM_ID, CAM_DESC FROM ACT.ACTIVITY_M WHERE CAM_BSU_ID = '" & Session("sBSUID") & "'"
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Sub BindActivity()
        ddlActivity.DataSource = GetActivity()
        ddlActivity.DataTextField = "CAM_DESC"
        ddlActivity.DataValueField = "CAM_ID"
        ddlActivity.DataBind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindActivity()
        BindSubActivity()
    End Sub

    Protected Sub imgSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSubject.Click
        GridBindsubject(h_SBG_IDs.Value)
    End Sub

    Private Sub GridBindsubject(ByVal vSBG_IDs As String)
        grdSubject.DataSource = ReportFunctions.GetGradeSelectedSubjects(vSBG_IDs)
        grdSubject.DataBind()
    End Sub

    Protected Sub grdSubject_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdSubject.PageIndex = e.NewPageIndex
        GridBindsubject(h_SBG_IDs.Value)
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        If vSTU_IDs = "" Then
            grdStudent.DataSource = Nothing
            grdStudent.DataBind()
        Else
            grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
            grdStudent.DataBind()
        End If
    End Sub

    Sub GetAllGrade()
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        ddlGrade.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBSUID"), ddlAca_Year.SelectedValue)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()        
    End Sub

    'Sub GetSectionForGrade()
    '    'If ddlGrade.SelectedValue = "ALL" Then
    '    '    ddlSection.DataSource = Nothing
    '    '    ddlSection.DataBind()
    '    '    ddlSection.Items.Add(New ListItem("--", 0))
    '    '    ddlSection.Items.FindByText("--").Selected = True
    '    'Else
    '    Dim bSuperUsr As Boolean = False
    '    If Session("CurrSuperUser") = "Y" Then
    '        bSuperUsr = True
    '    End If
    '    ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
    '    ddlSection.DataTextField = "SCT_DESCR"
    '    ddlSection.DataValueField = "SCT_ID"
    '    ddlSection.DataBind()
    '    If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
    '        ddlSection.Items.Add(New ListItem("ALL", "ALL"))
    '        ddlSection.Items.FindByText("ALL").Selected = True
    '    End If
    'End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If h_STU_IDs.Value = "" Then
                'Dim str_sec As String = GetSelectedSection(ddlSection.SelectedValue)
                'h_STU_IDs.Value = GetAllStudentsInSection(Session("sBSU_ID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, str_sec)
                h_STU_IDs.Value = GetAllStudentsInGroup(Session("sBSU_ID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, ddlSubjectGroup.SelectedValue)
            End If
            Select Case ViewState("MainMnu_code")
                Case "C304005"
                    GenerateAOLREPORT()
            End Select
        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub

    Private Sub GenerateAOLREPORT()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@CAM_ID", ddlActivity.SelectedValue)
        param.Add("@CAD_ID", ddlSubActivity.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
        param.Add("RPT_CAPTION", ddlSubActivity.SelectedItem.Text)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptAOL_REPORT_CBSE_03.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Private Function GetSelectedSection(ByVal sec_id As String) As String
        Dim str_sec_ids As String = String.Empty
        Dim comma As String = String.Empty
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If

        If sec_id = "ALL" Then
            Dim ds As DataSet = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0) Is Nothing) AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If str_sec_ids <> "" Then
                        str_sec_ids += ","
                    End If
                    str_sec_ids += dr("SCT_ID").ToString
                Next
            End If
            Return str_sec_ids
        Else
            Return sec_id
        End If
    End Function

    Private Function GetAllStudentsInSection(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSCT_ID As String) As String
        Dim str_sql As String = String.Empty
        'str_sql = "SELECT  STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        '" STUDENT_M  " & _
        '" WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
        '"' AND STU_BSU_ID = '" & Session("sbsuid") & "'" & _
        '" AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,1,'') "
        str_sql = "SELECT  ISNULL(STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
       " STUDENT_M INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID " & _
       " WHERE STP_ACD_ID ='" & vACD_ID & "' AND STP_GRD_ID = '" & vGRD_ID & _
       "' AND STP_BSU_ID = '" & Session("sbsuid") & "'" & _
       " AND STP_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,0,''),0) "

        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Private Function GetAllStudentsInGroup(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSGR_ID As String) As String
        Dim str_sql As String = String.Empty
        str_sql = "SELECT  ISNULL(STUFF((SELECT  CAST(ISNULL(SSD_STU_ID,'') as varchar) + '|' FROM " & _
       " GROUPS_M INNER JOIN STUDENT_GROUPS_S ON GROUPS_M.SGR_ACD_ID = STUDENT_GROUPS_S.SSD_ACD_ID AND " & _
       " GROUPS_M.SGR_GRD_ID = STUDENT_GROUPS_S.SSD_GRD_ID AND GROUPS_M.SGR_SBM_ID = STUDENT_GROUPS_S.SSD_SBM_ID " & _
       " AND GROUPS_M.SGR_SBG_ID = STUDENT_GROUPS_S.SSD_SBG_ID AND GROUPS_M.SGR_ID = STUDENT_GROUPS_S.SSD_SGR_ID " & _
       " WHERE SGR_ACD_ID ='" & vACD_ID & "' AND SGR_GRD_ID = '" & vGRD_ID & _
       "' AND SGR_BSU_ID = '" & Session("sbsuid") & "'" & _
       " AND SGR_ID in (" & vSGR_ID & ") for xml path('')),1,0,''),0) "

        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Public Function GetReportType() As String
        Dim strGRD_ID As String = ddlGrade.SelectedValue
        Dim strACD_ID As String = ddlAca_Year.SelectedValue
        Dim strBSSU_ID As String = Session("sBSUID")
        Dim strRSM_ID As String = ddlActivity.SelectedValue
        Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
        Dim arrList As ArrayList = New ArrayList(h_STU_IDs.Value.Split("___"))
        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function

    Private Function GETStud_photoPath(ByVal STU_ID As String) As String
        STU_ID = CStr(CInt(STU_ID))
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
        Dim RESULT As String
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            RESULT = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return RESULT

    End Function

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlActivity.SelectedIndexChanged
        BindSubActivity()
        GetAllGrade()
        BindSubjects(ddlGrade.SelectedValue)
        BindSubjectGroup()

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        h_SBG_IDs.Value = "0"
        GridBindsubject(h_SBG_IDs.Value)
          'h_STU_IDs.Value = "0"
        GridBindStudents(h_STU_IDs.Value)
        BindSubjects(ddlGrade.SelectedValue)
        BindSubjectGroup()
    End Sub

    Sub BindSubjectGroup()
        ddlSubjectGroup.DataSource = ACTIVITYSCHEDULE.GetSubjectGroups(Session("sBSUID"), ddlAca_Year.SelectedValue _
                                                                       , ddlGrade.SelectedValue, ddlSubject.SelectedValue, _
                                                                       Session("CurrSuperUser"), Session("EmployeeId"))
        ddlSubjectGroup.DataTextField = "SGR_DESCR"
        ddlSubjectGroup.DataValueField = "SGR_ID"
        ddlSubjectGroup.DataBind()
        ddlSubjectGroup.Items.Add("ALL")
        ddlSubjectGroup.Items.FindByValue("ALL").Selected = True
    End Sub

    Sub BindSubjects(ByVal vGRD_ID As String)
        ddlSubject.DataSource = ActivityFunctions.GetSUBJECT_ACD_YR(Session("sBSUID"), ddlAca_Year.SelectedValue, vGRD_ID)
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        'code added by dhanya
        BindActivity()
        BindSubActivity()
        GetAllGrade()
        GridBindsubject(h_SBG_IDs.Value)
        'h_STU_IDs.Value = "0"
        GridBindStudents(h_STU_IDs.Value)
        BindSubjects(ddlGrade.SelectedValue)
        BindSubjectGroup()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindSubjectGroup()
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
