﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptADECReport.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptADECReport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            ADEC Report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center"
                                cellpadding="7" cellspacing="0" width="100%">
                                <tr id="trAcd1" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" >
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                    </td>
                                </tr>


                            </table>

                        </td>
                    </tr>
                </table>
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

