﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSMARTTargetAnalysis.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptSMARTTargetAnalysis" title="Untitled Page" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
             <asp:Label id="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
 <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table  id="tblrule" runat="server" align="center"  cellpadding="4" cellspacing="0" style="width: 100%" >
                    
                    <tr>
                        <td align="left" >
                            <span class="field-label">Academic Year</span></td>
                        
                        <td align="left"  >
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    
                        <td align="left" >
                            <span class="field-label">Report Card</span></td>
                        
                        <td align="left"  >
                            <asp:DropDownList id="ddlReportCard" runat="server" AutoPostBack="True"  Width="303px">
                            </asp:DropDownList></td>
                    </tr>
        <tr>
            <td align="left" valign="middle" width="20%"  >
                <span class="field-label">Report Schedule</span></td>
            
            <td align="left" valign="middle" >
                <asp:DropDownList id="ddlPrintedFor" runat="server"  Width="197px">
                </asp:DropDownList>
                 </td>
        </tr>
        <tr>
            <td align="left" valign="middle"  >
                           <span class="field-label"> Grade</span></td>
            
            <td align="left" valign="middle"  >
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True" Width="172px">
                </asp:DropDownList></td>
            <td >
                <span class="field-label">Section</span></td>
           
            <td >
                <asp:DropDownList id="ddlSection" runat="server" AutoPostBack="True" 
                    Width="100px">
                </asp:DropDownList>
            </td>
        </tr>
                    <tr id ="trSubject" runat="server" >
                        <td align="left" valign="middle" >
                            <span class="field-label">Subject</span></td>
                        
                        <td align="left" valign="middle" >
                            <asp:DropDownList id="ddlSubject" runat="server" AutoPostBack="True"  Width="201px">
                            </asp:DropDownList>&nbsp;
                        </td>
                        
                         <td align="left" valign="middle" >
                            <span class="field-label">Status</span></td>
                        
                        <td align="left" valign="middle" >
                            <asp:DropDownList id="ddlStatus" runat="server" AutoPostBack="True"  
                                Width="100px">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem Value="above">Above Target</asp:ListItem>
                                <asp:ListItem Value="below">Below Target</asp:ListItem>
                                <asp:ListItem Value="on">On Target</asp:ListItem>
                            </asp:DropDownList>&nbsp;
                        </td>
                    </tr>
        <tr>
            <td align="left" colspan="6" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button" H
                   Text="Generate Report" ValidationGroup="groupM1" Width="205px" />&nbsp;<asp:Button 
                    ID="btnDownload" runat="server" CssClass="button" 
                                TabIndex="7" Text="Download Report in PDF" ValidationGroup="groupM1" 
                                Width="206px" />
                        </td>
        </tr>
                </table>
    
   </div>
            </div>
            </div>
 <asp:HiddenField id="hfbDownload" runat="server"></asp:HiddenField>

<CR:CrystalReportSource id="rs" runat="server" CacheDuration="1">
                              </CR:CrystalReportSource>
</asp:Content>

