<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSubjectProgressAnalysis.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptSubjectProgressAnalysis" Title="Untitled Page" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        table td{
            vertical-align :top !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Subject Progress Analysis
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" valign="top" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="bottom" colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="top">
                            <table align="center"  cellpadding="5"
                                cellspacing="0" width="100%" valign="top">                                
                                <tr>
                                    <td valign="top">
                                        <table  cellpadding="5" width="100%" style="border-collapse: collapse" valign="top">
                                            <tr >
                                                <td align="center" class="title-bg-small" colspan="2" valign="middle">                                                    
                                           Previous Reportcard</td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" >
                                                    <asp:Label ID="lblStu" runat="server" CssClass="field-label" Text="Academic Year"></asp:Label>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlAcademicYear1" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" >
                                                    <asp:Label ID="Label1" runat="server" CssClass="field-label" Text="Report Card" ></asp:Label>
                                                </td>
                                                
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlReportCard1" AutoPostBack="true" runat="server" >
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" >
                                                    <asp:Label ID="Label2" runat="server" CssClass="field-label" Text="Report Schedule" ></asp:Label>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlReportSchedule1" AutoPostBack="true" runat="server" >
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" > <span class="field-label">Report Header</span></td>
                                                
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlHeader1" colspan="2" AutoPostBack="true" runat="server" >
                                                    </asp:DropDownList></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <table  cellpadding="5" width="100%" style="border-collapse: collapse" valign="top">
                                            <tr >
                                                <td align="center" colspan="2" class="title-bg-small" valign="middle">                                                    
                                            Current Reportcard</td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%" >
                                                    <asp:Label ID="Label4" runat="server" CssClass="field-label" Text="Academic Year" ></asp:Label>
                                                </td>
                                             
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlAcademicYear2" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" >
                                                    <asp:Label ID="Label5" runat="server" CssClass="field-label" Text="Report Card" ></asp:Label>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlReportCard2" AutoPostBack="true" runat="server" >
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" >
                                                    <asp:Label ID="Label6" runat="server" CssClass="field-label" Text="Report Schedule" ></asp:Label>
                                                </td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlReportSchedule2" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" > <span class="field-label">Report Header</span></td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlHeader2" AutoPostBack="true" runat="server" >
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" > <span class="field-label">Grade</span></td>
                                               
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server" >
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%"  > <span class="field-label">Subject</span></td>
                                              
                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" >
                                                    </asp:DropDownList></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="center" colspan="2" >
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="groupM1"
                                            TabIndex="7"  />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download Report in PDF" ValidationGroup="groupM1"
                                TabIndex="7" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

