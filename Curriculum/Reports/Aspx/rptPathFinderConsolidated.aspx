<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPathFinderConsolidated.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptPathFinderConsolidated" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Pathfinder Ranklist
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

