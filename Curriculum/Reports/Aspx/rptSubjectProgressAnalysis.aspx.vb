Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_Reports_Aspx_rptSubjectProgressAnalysis
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400370") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear1 = studClass.PopulateAcademicYear(ddlAcademicYear1, Session("clm"), Session("sbsuid"))
                    ddlAcademicYear2 = studClass.PopulateAcademicYear(ddlAcademicYear2, Session("clm"), Session("sbsuid"))
                    BindReportCard(ddlReportCard1, ddlAcademicYear1.SelectedValue.ToString)
                    BindReportCard(ddlReportCard2, ddlAcademicYear2.SelectedValue.ToString)
                    BindReportSchedule(ddlReportSchedule1, ddlReportCard1.SelectedValue.ToString)
                    BindReportSchedule(ddlReportSchedule2, ddlReportCard2.SelectedValue.ToString)
                    BindHeader(ddlHeader1, ddlReportCard1.SelectedValue.ToString)
                    BindHeader(ddlHeader2, ddlReportCard2.SelectedValue.ToString)
                    BindGrade()
                    BindSubject()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportCard(ByVal ddlReportCard As DropDownList, ByVal acd_id As String)
        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + acd_id _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindReportSchedule(ByVal ddlReportSchedule As DropDownList, ByVal rsm_id As String)
        ddlReportSchedule.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + rsm_id _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportSchedule.DataSource = ds
        ddlReportSchedule.DataTextField = "RPF_DESCR"
        ddlReportSchedule.DataValueField = "RPF_ID"
        ddlReportSchedule.DataBind()
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear2.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard2.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub


    Sub BindHeader(ByVal ddlHeader As DropDownList, ByVal rsm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + rsm_id _
                                & " AND RSD_BALLSUBJECTS='TRUE' AND RSD_CSSClASS<>'TEXTBOXMULTI'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlHeader.DataSource = ds
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_ID"
        ddlHeader.DataBind()
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_DESCR,SBG_ID FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID=" + ddlAcademicYear2.SelectedValue.ToString _
                                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub CallReport()
        Dim param As New Hashtable
        Dim rptClass As New rptClass

        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID1", ddlAcademicYear1.SelectedValue.ToString)
        param.Add("@ACD_ID2", ddlAcademicYear2.SelectedValue.ToString)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        param.Add("@SBG_DESCR", ddlSubject.SelectedItem.Text)
        param.Add("@RSD_ID1", ddlHeader1.SelectedValue.ToString)
        param.Add("@RSD_ID2", ddlHeader2.SelectedValue.ToString)
        param.Add("@RPF_ID1", ddlReportSchedule1.SelectedValue.ToString)
        param.Add("@RPF_ID2", ddlReportSchedule2.SelectedValue.ToString)
        param.Add("@TYPE", "")
        param.Add("accYear", ddlAcademicYear2.SelectedItem.Text)
        param.Add("grm", ddlGrade.SelectedItem.Text)
        param.Add("rpf1", ddlReportSchedule1.SelectedItem.Text)
        param.Add("rpf2", ddlReportSchedule2.SelectedItem.Text)
        rptClass.reportPath = Server.MapPath("../Rpt/SEF Reports/rptSubjectProgressAnalysis.rpt")

        rptClass.crDatabase = "oasis_curriculum"
        rptClass.reportParameters = param
        Session("rptClass") = rptClass


        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region

    Protected Sub ddlAcademicYear1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear1.SelectedIndexChanged
        BindReportCard(ddlReportCard1, ddlAcademicYear1.SelectedValue.ToString)
        BindReportSchedule(ddlReportSchedule1, ddlReportCard1.SelectedValue.ToString)
        BindHeader(ddlHeader1, ddlReportCard1.SelectedValue.ToString)
    End Sub

    Protected Sub ddlAcademicYear2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear2.SelectedIndexChanged
        BindReportCard(ddlReportCard2, ddlAcademicYear2.SelectedValue.ToString)
        BindReportSchedule(ddlReportSchedule2, ddlReportCard2.SelectedValue.ToString)
        BindHeader(ddlHeader2, ddlReportCard2.SelectedValue.ToString)
        BindGrade()
        BindSubject()
    End Sub

    Protected Sub ddlReportCard1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard1.SelectedIndexChanged
        BindReportSchedule(ddlReportSchedule1, ddlReportCard1.SelectedValue.ToString)
        BindHeader(ddlHeader1, ddlReportCard1.SelectedValue.ToString)
    End Sub

    Protected Sub ddlReportCard2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard2.SelectedIndexChanged
        BindReportSchedule(ddlReportSchedule2, ddlReportCard2.SelectedValue.ToString)
        BindHeader(ddlHeader2, ddlReportCard2.SelectedValue.ToString)
        BindGrade()
        BindSubject()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
    End Sub

    
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub
End Class
