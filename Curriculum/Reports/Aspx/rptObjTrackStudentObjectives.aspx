﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptObjTrackStudentObjectives.aspx.vb" Inherits="Curriculum_rptObjTrackStudentObjectives" Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script>
        function change_ddl_state(ddlThis, ddlObj) {


            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;


                if (currentid.indexOf(ddlObj) != -1) {

                    if (ddlThis.selectedIndex != 0) {

                        document.forms[0].elements[i].selectedIndex = ddlThis.selectedIndex - 1;

                    }


                }
            }
        }

        //
        function GetClientId(strid) {
            var count = document.forms[0].length;
            var i = 0;
            var eleName;
            for (i = 0; i < count; i++) {
                eleName = document.forms[0].elements[i].id;
                pos = eleName.indexOf(strid);
                if (pos >= 0) break;
            }
            return eleName;
        }

        function autoWidth(mySelect) {
            var maxlength = 0;

            //   var mySelect=document.getElementById(GetClientId(ddl));


            for (var i = 0; i < mySelect.options.length; i++) {
                if (mySelect[i].text.length > maxlength) {
                    maxlength = mySelect[i].text.length;
                }
            }
            mySelect.style.width = maxlength * 5;
        }

        function setWidth(mySelect) {
            //var mySelect=document.getElementById(GetClientId(ddl));
            mySelect.style.width = 35;
        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Student Objectives"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"    ></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">

                            <asp:Panel ID="pnlSearch" runat="server">
                                <table id="Table2" runat="server" width="100%">
                                    <tr>
                                        <td class="matters" align="left"><span class="field-label">Academic Year</span></td>
                                        <td class="matters" align="left">
                                            <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                            </asp:DropDownList></td>
                                        <td class="matters" align="left"><span class="field-label">Grade</span></td>
                                        <td class="matters" align="left">
                                            <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td class="matters" align="left"><span class="field-label">Subject</span></td>
                                        <td class="matters" align="left">
                                            <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server"  >
                                            </asp:DropDownList></td>
                                        <td class="matters" align="left"><span class="field-label">Group</span></td>
                                        <td class="matters" align="left">
                                            <asp:DropDownList ID="ddlGroup" runat="server"
                                                >
                                            </asp:DropDownList></td>
                                    </tr>


                                    <tr>
                                        <td class="matters" align="left"><span class="field-label">Level</span></td>
                                        <td class="matters" align="left">
                                            <asp:DropDownList ID="ddlLevel"
                                                runat="server">
                                            </asp:DropDownList></td>
                                        <td class="matters" colspan="2">
                                            <table>
                                                <tr>
                                                    <td class="matters">
                                                        <asp:RadioButton ID="rdAll" runat="server" Checked="True" GroupName="g1" CssClass="field-label"
                                                            Text="All" />
                                                    </td>
                                                    <td class="matters">
                                                        <asp:RadioButton ID="rdObj1" runat="server" GroupName="g1" CssClass="field-label"
                                                            Text="Objectives 1 to 25" />
                                                    </td>
                                                    <td class="matters" colspan="2">
                                                        <asp:RadioButton ID="rdObj2" runat="server" GroupName="g1" CssClass="field-label"
                                                            Text="Objectives 26 to 50" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="matters" align="left"><span class="field-label">Objective Category</span></td>

                                        <td class="matters" align="left"  >
                                            <asp:DropDownList ID="ddlCategory" runat="server"  >
                                            </asp:DropDownList></td>
                                        <td>   <asp:CheckBox ID="chkTranspose" runat="server" Text="Transpose" CssClass="field-label" /></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">Student ID</span></td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtStudentID" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left" class="matters"><span class="field-label">Student Name</span></td>
                                        <td align="left" class="matters">
                                            <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center" class="matters" colspan="4">
                                         
                                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                                TabIndex="7" Text="Generate Report" ValidationGroup="groupM1" />
                                            <asp:Button ID="btnDownload" runat="server" CssClass="button"
                                                TabIndex="7" Text="DownLoad Report In PDF" ValidationGroup="groupM1"
                                                />
                                            <asp:Button ID="btnDownloadExcel" runat="server" CssClass="button"
                                                TabIndex="7" Text="DownLoad Report In Excel" ValidationGroup="groupM1"
                                                />
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>

                        </td>
                    </tr>



                    <tr id="trGrid" runat="server">
                        <td align="left">
                            <table style="border-collapse: collapse">



                                <tr id="trSave" runat="server">
                                    <td align="left">
                                        <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>


                                        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                                        </CR:CrystalReportSource>


                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

