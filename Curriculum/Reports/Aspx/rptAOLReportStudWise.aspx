<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAOLReportStudWise.aspx.vb" Inherits="clmActivitySchedule" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
   <script language="javascript" type="text/javascript">
   
       function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var SGR_IDs =document.getElementById('<%=ddlSubjectGroup.ClientID %>').value;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var ACD_ID =document.getElementById('<%=ddlAca_Year.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_ID + "&SGR_IDs=" + SGR_IDs, "RadWindow1");
            
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=txtStudIDs.ClientID()%>").value = NameandCode[1];
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');

                //document.getElementById('<%=h_SBG_IDs.ClientID %>').value = result;//NameandCode[0];
            }
        }
        

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    
    
      function GetSTUDENTS()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var SGR_IDs =document.getElementById('<%=ddlSubjectGroup.ClientID %>').value;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var ACD_ID =document.getElementById('<%=ddlAca_Year.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_ID + "&SGR_IDs=" + SGR_IDs ,"", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
         }
         
      function GetSUBJECT()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var ACD_ID =document.getElementById('<%=ddlAca_Year.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select Grade')
                return false;
            }
          //result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_ID ,"", sFeatures)            
            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs + "&ACD_ID=" + ACD_ID ,"", "RadWindow2");
            <%--if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_SBG_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }--%>
        }
                 
   </script>

   <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          
        </Windows>
       <Windows>
            <telerik:RadWindow ID="RadWindow2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label id="lblHeader" runat="server" Text="Progress Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label> <asp:Button ID="btnCheck" style="display:none;"  runat="server" />
    <table align="center" width="100%" >
        
        <tr>
            <td align="left">
                <span class="field-label">Academic Year</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left">
                <span class="field-label">Activity</span> </td>
           
            <td align="left">
                <asp:DropDownList id="ddlActivity" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>            
            <td align="left">
                <span class="field-label">Sub Activity</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlSubActivity" runat="server">
                </asp:DropDownList></td>
            <td align="left" valign="middle">
                <span class="field-label">Grade</span></td>
            
            <td align="left" valign="middle" colspan="4">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
       
        <tr id ="20" >
            <td align="left" valign="middle">
                <span class="field-label">Subject</span></td>
           
            <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true" >
                            </asp:DropDownList>
            </td>
            <td align="left" valign="middle" >
               <span class="field-label"> Group</span></td>
           
            <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlSubjectGroup" runat="server">
                            </asp:DropDownList>
                                        </td>
        </tr>
        <tr id="trSubject" runat="server" visible ="false">
            <td align="left" valign ="top">
                <span class="field-label">Subject</span></td>
            
            <td align="left"  valign ="top">
                            <asp:TextBox id="txtSubject" runat="server"></asp:TextBox>
                            <asp:ImageButton id="imgSubject" runat="server" 
                    ImageUrl="~/Images/cal.gif" OnClientClick="GetSUBJECT();" 
                    OnClick="imgSubject_Click" style="width: 16px">
                            </asp:ImageButton>
                </td>
                <td colspan="2" align="left">
                            <asp:GridView id="grdSubject" CssClass="table table-bordered table-row" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                PageSize="5" Width="100%" OnPageIndexChanging="grdSubject_PageIndexChanging">
                                <columns>
<asp:TemplateField HeaderText="GRADE"><ItemTemplate>
<asp:Label id="lblSUBJ_ID" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="SBG_DESCR" HeaderText="Subject Name"></asp:BoundField>
</columns>
                                <headerstyle cssclass="gridheader_new" />
                            </asp:GridView></td>
        </tr>
        <tr>
            <td align="left" valign="top" >
                <span class="field-label">Student</span></td>
           
            <td align="left">
                <asp:TextBox id="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged" ></asp:TextBox>
                <asp:ImageButton id="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click">
                </asp:ImageButton>
                </td>
            <td colspan="2" align="left">
                <asp:GridView id="grdStudent" CssClass="table table-bordered table-row" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    PageSize="5" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                    <columns>
<asp:TemplateField HeaderText="Stud. No"><ItemTemplate>
<asp:Label id="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
</columns>
                    <headerstyle cssclass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="4" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="hfbFinalReport" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_STU_IDs" runat="server"/>
    <asp:HiddenField  id="h_SBG_IDs" runat="server"/>

                </div>
            </div>
        </div>

</asp:Content>

