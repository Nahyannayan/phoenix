﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCBSECommendation01_04.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptCBSECommendation01_04" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Primary Commandation Report Card "></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%" ><span  class="field-label">Academic Year</span></td>
                       
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                          <td align="left" width="20%" valign="middle"  ><span  class="field-label">Grade</span></td>
                       
                        <td align="left" width="30%" valign="middle"  >
                            <asp:DropDownList ID="ddlGrade" runat="server" >
                            </asp:DropDownList></td>                                                
                    </tr>
                   
                 
                    <tr>
                        <td align="left" width="20%" ><span  class="field-label">Report Card</span></td>
                       
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                         <td align="left" width="20%" valign="middle"  ><span  class="field-label">Report Schedule</span></td>
                       
                        <td align="left" width="30%" valign="middle"  >
                            <asp:DropDownList ID="ddlPrintedFor" runat="server" >
                            </asp:DropDownList></td>
                      
                      
                    </tr>
                  
                    <tr>
                         <td align="left"  width="20%" valign="middle"  ><span  class="field-label">Certification</span></td>
                      
                        <td align="left" valign="middle" width="30%"  >
                            <asp:DropDownList ID="ddlCertificate" runat="server" >
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>GOLD</asp:ListItem>
                                <asp:ListItem>SILVER</asp:ListItem>
                                <asp:ListItem>BRONZE</asp:ListItem>
                            </asp:DropDownList></td>
                        <td colspan="2">

                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                Text="Generate Report" ValidationGroup="groupM1"  /></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>


</asp:Content>

