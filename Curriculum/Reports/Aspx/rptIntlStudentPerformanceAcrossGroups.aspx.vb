Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports CURRICULUM
Partial Class Curriculum_Reports_Aspx_rptIntlStudentPerformanceAcrossGroups
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400081") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindReportCard()
                BindHeader()
                BindPrintedFor()
                PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                ViewState("display") = "textbox"

                If Session("CurrSuperUser") = "Y" Then
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
                    ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                Else
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                End If
                lstGroup = PopulateGroup(lstGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

                tbData.Rows(0).Visible = False
                lblA2.Visible = False
                txtG2.Visible = False
            End If
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindHeader()
        BindPrintedFor()
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)



        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        lstGroup = PopulateGroup(lstGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)


        BindDefaults()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged



        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        lstGroup = PopulateGroup(lstGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)


        BindDefaults()
    End Sub

#Region "Private Methods"


    Function getGroups() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstGroup.Items.Count - 1
            If lstGroup.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += lstGroup.Items(i).Value
            End If
        Next
        Return str
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
        '                         & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
        '                         & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
        '                         & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
        '                         & " WHERE SBG_ACD_ID='" + acd_id + "' AND SGS_EMP_ID='" + emp_id + "'" _
        '                         & " AND SGS_TODATE IS NULL"

        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                & " WHERE SBG_ACD_ID='" + acd_id + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID='" + acd_id + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID='" + grade(1) + "'"

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " AND (ISNULL(RSD_bANALYSIS,0)=1 OR ISNULL(RSD_BPERFORMANCE_INDICATOR,0)=1)" _
                                & " AND RSD_CSSCLASS='textboxsmall' ORDER BY RSD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        rdHeader.DataSource = ds
        rdHeader.DataTextField = "RSD_HEADER"
        rdHeader.DataValueField = "RSD_ID"
        rdHeader.DataBind()

        If rdHeader.Items.Count > 0 Then
            rdHeader.Items(0).Selected = True
        End If
    End Sub


    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportSchedule.DataSource = ds
        ddlReportSchedule.DataTextField = "RPF_DESCR"
        ddlReportSchedule.DataValueField = "RPF_ID"
        ddlReportSchedule.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
        '                      & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
        '                      & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
        '                      & " where grm_acd_id=" + acdid _
        '                      & " order by grd_displayorder"

        Dim str_query As String

        'If Session("CurrSuperUser") = "Y" Then
        str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                    & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                    & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                    & " inner join oasis..stream_m as p on a.grm_stm_id=p.stm_id " _
                    & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                    & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                    & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                    & " ORDER BY RSG_DISPLAYORDER"
        'ElseIf studClass.isEmpTeacher(Session("EmployeeId")) = True Then

        '    str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                 & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
        '                 & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
        '                 & " inner join oasis..stream_m as p on a.grm_stm_id=p.stm_id " _
        '                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
        '                 & " INNER JOIN GROUPS_M AS D ON A.GRM_GRD_ID=D.SGR_GRD_ID AND A.GRM_ACD_ID=D.SGR_GRD_ID" _
        '                 & " INNER JOIN GROUPS_TEACHER_S AS E ON D.SGR_ID=E.SGS_SGR_ID AND SGS_EMP_ID='" + Session("EMPLOYEEID") + "'" _
        '                 & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
        '                 & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
        '                 & " ORDER BY RSG_DISPLAYORDER"

        'Else
        '    str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                 & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
        '                 & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
        '                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
        '                 & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
        '                 & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
        '                 & " ORDER BY RSG_DISPLAYORDER"
        'End If




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindDefaults()


        If ddlSubject.SelectedValue.ToString <> "0" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT DISTINCT RGP_GRADE,RGP_VALUE FROM RPT.REPORT_GRADEMAPPING " _
                                   & " WHERE RGP_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                   & " AND RGP_SBG_ID=" + ddlSubject.SelectedValue.ToString _
                                   & " ORDER BY RGP_VALUE"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlG1.DataSource = ds
            ddlG1.DataTextField = "RGP_GRADE"
            ddlG1.DataValueField = "RGP_VALUE"
            ddlG1.DataBind()

            ddlG2.DataSource = ds
            ddlG2.DataTextField = "RGP_GRADE"
            ddlG2.DataValueField = "RGP_VALUE"
            ddlG2.DataBind()
            If ddlG1.Items.Count = 0 Then
                ViewState("display") = "textbox"
            Else
                ViewState("display") = "dropdown"
            End If
        Else
            ViewState("display") = "textbox"
        End If


        If ViewState("display") = "textbox" Then
            tbData.Rows(0).Visible = False
            tbData.Rows(1).Visible = True
            If rdBetween.Checked = True Then
                lblA2.Visible = True
                txtG2.Visible = True
            Else
                lblA2.Visible = False
                txtG2.Visible = False
            End If
        Else
            tbData.Rows(0).Visible = True
            tbData.Rows(1).Visible = False
            If rdBetween.Checked = True Then
                lblA1.Visible = True
                ddlG2.Visible = True
            Else
                lblA1.Visible = False
                ddlG2.Visible = False
            End If
        End If


    End Sub

    Function GetSubjectReportQuery() As String
        Dim str As String

        Dim value1 As String
        Dim value2 As String
        Dim text1 As String
        Dim text2 As String
        Dim valuetype As String

        If ViewState("display") = "textbox" Then

            If IsNumeric(txtG1.Text) = True Then
                valuetype = "number"
                value1 = txtG1.Text
                value2 = txtG2.Text
            Else
                value1 = GetValue(txtG1.Text)
                value2 = GetValue(txtG2.Text)
                valuetype = "string"
            End If

            text1 = txtG1.Text
            text2 = txtG2.Text
        Else
            value1 = ddlG1.SelectedValue.ToString
            value2 = ddlG2.SelectedValue.ToString

            text1 = ddlG1.SelectedItem.Text
            text2 = ddlG2.SelectedItem.Text

            valuetype = "string"
        End If

        If valuetype = "string" Then
            If Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,RST_COMMENTS,SGR_DESCR, " _
                    & " EMP_NAME=(SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                    & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                    & " WHERE(SGS_SGR_ID = B.RST_SGR_ID)" _
                    & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''))" _
                    & " FROM VW_STUDENT_M AS A INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.STU_ID=B.RST_STU_ID" _
                    & " INNER JOIN VW_SECTION_M AS G ON A.STU_SCT_ID=G.SCT_ID" _
                    & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                    & " INNER JOIN RPT.REPORT_SETUP_M AS D ON C.RSD_RSM_ID=D.RSM_ID" _
                    & " INNER JOIN RPT.REPORT_GRADEMAPPING AS E ON D.RSM_ID=E.RGP_RSM_ID" _
                    & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                    & " AND RST_SBG_ID=RGP_SBG_ID AND RST_COMMENTS=RGP_GRADE" _
                    & " INNER JOIN GROUPS_M AS T ON B.RST_SGR_ID=T.SGR_ID" _
                    & " WHERE 1=1"
            Else
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,RST_COMMENTS,SGR_DESCR, " _
               & " EMP_NAME=(SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
               & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
               & " WHERE(SGS_SGR_ID = B.RST_SGR_ID)" _
               & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''))" _
               & " FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B ON A.STU_ID=B.RST_STU_ID" _
               & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
               & " INNER JOIN RPT.REPORT_SETUP_M AS D ON C.RSD_RSM_ID=D.RSM_ID" _
               & " INNER JOIN RPT.REPORT_GRADEMAPPING AS E ON D.RSM_ID=E.RGP_RSM_ID" _
               & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
               & " AND RST_SBG_ID=RGP_SBG_ID AND RST_COMMENTS=RGP_GRADE" _
               & " INNER JOIN GROUPS_M AS T ON B.RST_SGR_ID=T.SGR_ID" _
               & " WHERE 1=1"
            End If

            If rdAbove.Checked = True Then
                str += " AND RGP_VALUE>'" + value1 + "'"
                ViewState("criteria") = "Score above " + text1
            ElseIf rdBelow.Checked = True Then
                str += " AND RGP_VALUE<'" + value1 + "'"
                ViewState("criteria") = "Score below " + text1
            Else
                str += " AND RGP_VALUE BETWEEN '" + value1 + "' AND '" + value2 + "'"
                ViewState("criteria") = "Score between " + text1 + " and " + text2
            End If

        Else
            If Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,RST_COMMENTS,SGR_DESCR, " _
                      & " EMP_NAME=(SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                      & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                      & " WHERE(SGS_SGR_ID = B.RST_SGR_ID)" _
                      & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''))" _
                      & " FROM VW_STUDENT_M AS A INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.STU_ID=B.RST_STU_ID" _
                      & " INNER JOIN VW_SECTION_M AS G ON A.STU_SCT_ID=G.SCT_ID" _
                      & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND ISNULL(RSD_bPERFORMANCE_INDICATOR,'FALSE')='TRUE'" _
                      & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                      & " INNER JOIN GROUPS_M AS T ON B.RST_SGR_ID=T.SGR_ID" _
                      & " WHERE 1=1"
            Else

                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,RST_COMMENTS,SGR_DESCR, " _
                   & " EMP_NAME=(SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                   & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                   & " WHERE(SGS_SGR_ID = B.RST_SGR_ID)" _
                   & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''))" _
                   & " FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B ON A.STU_ID=B.RST_STU_ID" _
                   & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND ISNULL(RSD_bPERFORMANCE_INDICATOR,'FALSE')='TRUE'" _
                   & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                   & " INNER JOIN GROUPS_M AS T ON B.RST_SGR_ID=T.SGR_ID" _
                   & " WHERE 1=1"

            End If
            If rdAbove.Checked = True Then
                str += " AND RST_COMMENTS>'" + value1 + "'"
                ViewState("criteria") = "Score above " + text1
            ElseIf rdBelow.Checked = True Then
                str += " AND RST_COMMENTS<'" + value1 + "'"
                ViewState("criteria") = "Score below " + text1
            Else
                str += " AND RST_COMMENTS BETWEEN '" + value1 + "' AND '" + value2 + "'"
                ViewState("criteria") = "Score between " + text1 + " and " + text2
            End If
        End If

        Dim grade() As String = ddlGrade.SelectedValue.Split("|")
        str += " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        str += " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_STM_ID=" + grade(1)
        str += " AND RST_RPF_ID=" + ddlReportSchedule.SelectedValue.ToString





        str += " AND SBG_ID=" + ddlSubject.SelectedValue.ToString


        If hfGroups.Value <> "" Then
            str += " AND SGR_ID IN (" + hfGroups.Value + ")"
        End If



        str += " ORDER BY SBG_DESCR,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Return str
    End Function


    Function GetValue(ByVal grade As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(MAX(RGP_VALUE),0) FROM RPT.REPORT_GRADEMAPPING WHERE " _
                                 & " RGP_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "' AND RGP_GRADE='" + grade + "'"

        If ddlSubject.SelectedValue.ToString <> "0" Then
            str_query += " AND RGP_SBG_ID=" + ddlSubject.SelectedValue.ToString
        End If

        Dim value As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return value
    End Function

    Public Function PopulateGroup(ByVal ddl As CheckBoxList, ByVal acdid As String, Optional ByVal Grdid As String = "", Optional ByVal Subjid As String = "")
        ddl.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""

        Dim grade As String() = Grdid.Split("|")
        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition += " AND SGR_GRD_ID='" & grade(0) & "'"
        End If
        If Subjid <> "ALL" And Subjid <> "" Then
            strCondition += " AND SGR_SBG_ID='" & Subjid & "'"
        End If

        'If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
        '    strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
        '    str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
        '      " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
        '      " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL AND SGR_ACD_ID='" + acdid + "'"

        '    str_sql += strCondition
        'Else
        str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                  & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + acdid + "'"

        str_sql += strCondition & " ORDER BY SGR_DESCR"
        'End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds

        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function


    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)

        param.Add("@EXEQUERY", GetSubjectReportQuery())

        param.Add("criteria", ViewState("criteria"))
        param.Add("reportcard", ddlReportCard.SelectedItem.Text)
        param.Add("schedule", ddlReportSchedule.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            .reportPath = Server.MapPath("../Rpt/rptINTLStudentPerformanceAcrossGroups.rpt")

        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

#End Region

    Protected Sub rdAbove_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAbove.CheckedChanged
        If ViewState("display") = "textbox" Then
            tbData.Rows(0).Visible = False
            lblA2.Visible = False
            txtG2.Visible = False
        Else
            tbData.Rows(1).Visible = False
            lblA1.Visible = False
            ddlG2.Visible = False
        End If
    End Sub

    Protected Sub rdBelow_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdBelow.CheckedChanged
        If ViewState("display") = "textbox" Then
            tbData.Rows(0).Visible = False
            tbData.Rows(1).Visible = True
            lblA2.Visible = False
            txtG2.Visible = False
        Else
            tbData.Rows(1).Visible = False
            tbData.Rows(0).Visible = True
            lblA1.Visible = False
            ddlG2.Visible = False
        End If
    End Sub

    Protected Sub rdBetween_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdBetween.CheckedChanged
        If ViewState("display") = "textbox" Then
            tbData.Rows(0).Visible = False
            tbData.Rows(1).Visible = True
            lblA2.Visible = True
            txtG2.Visible = True
        Else
            tbData.Rows(1).Visible = False
            tbData.Rows(0).Visible = True
            lblA1.Visible = True
            ddlG2.Visible = True
        End If
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        ' hfSubjects.Value = getSubjects()
        lstGroup = PopulateGroup(lstGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

        BindDefaults()
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindHeader()
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If


        lstGroup = PopulateGroup(lstGroup, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString, ddlSubject.SelectedValue.ToString)

        BindDefaults()
    End Sub

    Protected Sub btnGraph_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGraph.Click
        CallReport()
    End Sub

    Protected Sub lstGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGroup.SelectedIndexChanged
        hfGroups.Value = getGroups()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
