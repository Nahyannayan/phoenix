<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptChangeStreamOptionHistory.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptChangeStreamOptionHistory" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">




        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }



        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" Style="text-align: left" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                Style="text-align: center"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="matters">
                            <table width="100%">


                                <tr>

                                    <td align="left" class="matters" width="20%"><span class="field-label"> Select Academic Year</span> </td>
                                    <td class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>


                                </tr>


                                <tr>
                                    <td colspan="4">

                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="No Records Found" HeaderStyle-Height="30"
                                                        PageSize="20" Width="100%">
                                                        <RowStyle CssClass="griditem" />
                                                        <Columns>


                                                            <asp:TemplateField HeaderText="Available">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                    Select
                                                                    <br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />


                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>

                                                                </ItemTemplate>

                                                                <HeaderStyle Wrap="False"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Stud No.">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblh1" runat="server" CssClass="gridheader_text" Text="Stud. No"></asp:Label><br />
                                                                    <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox></td>
                                                         <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Student Name">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Grade">
                                                                <HeaderTemplate>
                                                                    Grade<br />
                                                                    <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged">
                                                                    </asp:DropDownList>

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Section">
                                                                <HeaderTemplate>
                                                                    Section<br />
                                                                    <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
                                                                    </asp:DropDownList>

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle CssClass="Green" />
                                                        <HeaderStyle CssClass="gridheader_pop" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>


                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="matters" valign="bottom" colspan="4" align="center"> <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                            Text="Generate Report" ValidationGroup="groupM1" OnClick="btnGenerateReport_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">&nbsp;<asp:HiddenField ID="hfSBM_ID" runat="server" />
                        </td>

                    </tr>
                </table>

                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>

