<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptReportCardSchedules.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptReportCardSchedules" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function openWin() {
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
          if (GRD_IDs == '') {
              alert('Please select atleast one Grade')
              return false;
          }

          var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs, "RadWindow1");

      }

      function OnClientClose(oWnd, args) {
          //get the transferred arguments
          var arg = args.get_argument();
          if (arg) {
              NameandCode = arg.NameCode.split('||');
              document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[1];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Marks By ExamType"></asp:Label>
            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <asp:Label ID="lblType" runat="server" Text="Report Type"  CssClass="field-label" ></asp:Label></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr runat="server" id="trRpf">
                        <td align="left" valign="middle"><span class="field-label">Report Printed For</span></td>
                        <td align="left" valign="middle"  >
                            <asp:DropDownList ID="ddlReportPrintedFor" runat="server" OnSelectedIndexChanged="ddlReportPrintedFor_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle"><span class="field-label">Grade</span></td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged1">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle"><span class="field-label">Section</span></td>
                        <td align="left" colspan="1" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr  >
                        <td align="left" valign="middle"><span class="field-label">Subject</span></td>
                        <td align="left" valign="middle"  >
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                            </asp:DropDownList>&nbsp;
                        </td>
                    </tr>
                    <tr runat="server" id="trStudent">
                        <td align="left" valign="top"><span class="field-label">Student</span></td>
                        <td align="left"  >
                            <asp:TextBox ID="txtStudIDs" runat="server"   OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click"></asp:ImageButton>
                            </td>
                        <td colspan="2">
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                PageSize="5" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button"  Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="h_SUBJID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_SCTIDs" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>

