<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptLessonPlanSetupTeacher.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptLessonPlanSetupTeacher
"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        function fnSelectAll_Group(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 28) == 'ctl00_cphMasterpage_lstGroup') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Topic(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 28) == 'ctl00_cphMasterpage_lstTopic') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            LESSON PLANNER
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr >

                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center"  valign="top">

                            <table id="tblClm" runat="server" align="center" width="100%" cellpadding="5" cellspacing="0" >

                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Select Academic Year</span></td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"  ><span class="field-label">Grade</span></td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" Width="183px">
                                        </asp:DropDownList></td>
                                </tr>
                               
                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Select Subject</span>
                                    </td>
                                   
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" Width="183px">
                                        </asp:DropDownList></td>

                                </tr>


                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Select Group</span></td>
                                  
                                    <td align="left" >
                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:fnSelectAll_Group(this);" Text="Select All"  CssClass="field-label"/> <br />
                                                  <div class="checkbox-list">  <asp:CheckBoxList ID="lstGroup" runat="server" >
                                                    </asp:CheckBoxList> </div>
                                    </td>
                                    <td align="left" width="20%" ><span class="field-label">Select Topic</span></td>
                                   
                                    <td align="left" >
                                       <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:fnSelectAll_Topic(this);" Text="Select All" CssClass="field-label"/> <br />
                                                  <div class="checkbox-list">  <asp:CheckBoxList ID="lstTopic" runat="server" >
                                                    </asp:CheckBoxList></div>
                                    </td>

                                </tr>                              

                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button" 
                                            Text="Download Report in PDF" ValidationGroup="groupM1" /></td>
                                </tr>

                            </table>
                            <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>


                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>


                </table>

            </div>
        </div>
    </div>
</asp:Content>

