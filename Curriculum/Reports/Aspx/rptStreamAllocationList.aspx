<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStreamAllocationList.aspx.vb" Inherits="Curriculum_Reports_Aspx_rptStreamAllocationList" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


    
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label id="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table id="tblrule" runat="server" align="center" width="100%" >
                   
        <tr>
            <td align="left" width="25%">
                <span class="field-label">Stream</span></td>
            
            <td align="left" width="25%">
                <asp:DropDownList id="ddlStream" runat="server" AutoPostBack="True" Width="172px">
                </asp:DropDownList></td>
            <td width="25%"></td>
            <td width="25%"></td>
        </tr>
        <tr id="Tr1" runat="server">
            <td align="left">
                <span class="field-label">Option</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlOption" runat="server" AutoPostBack="True"  Width="173px">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button"
                   Text="Generate Report" ValidationGroup="groupM1" />&nbsp;</td>
        </tr>
                </table>
            
                </div>
            </div>
         </div>


</asp:Content>

