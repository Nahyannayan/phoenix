﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports GemBox.Spreadsheet
Imports System.IO
Imports Telerik.Web.UI

Partial Class Curriculum_Reports_Aspx_rptAssessmentsByTeacher
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim currFns As New currFunctions
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")

                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If

                    If Session("CurrSuperUser") = "Y" Then
                        lstGroup = PopulateGroups(lstGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
                    Else
                        lstGroup = PopulateGroupsByTeacher(Session("EmployeeID"), lstGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
                    End If
                    BindTerm()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGenerateReport)
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If
            If Session("CurrSuperUser") = "Y" Then
                lstGroup = PopulateGroups(lstGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Else
                lstGroup = PopulateGroupsByTeacher(Session("EmployeeID"), lstGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            End If
            BindTerm()
            BindAssessment()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If
            If Session("CurrSuperUser") = "Y" Then
                lstGroup = PopulateGroups(lstGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Else
                lstGroup = PopulateGroupsByTeacher(Session("EmployeeID"), lstGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            End If
            BindAssessment()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        Try
            Dim li As ListItem
            Dim grade() As String = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
                lstGroup = PopulateGroups(lstGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            Else
                lstGroup = PopulateGroupsByTeacher(Session("EmployeeID"), lstGroup, ddlAcademicYear.SelectedValue.ToString, grade(0), ddlSubject.SelectedValue.ToString)
            End If
            BindAssessment()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Sub ExportExcel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim i As Integer

        Dim str_query As String
        Dim ds As DataSet

        Dim cadIds As String = ""
        For i = 0 To lstPrintedfor.Items.Count - 1
            If lstPrintedfor.Items(i).Selected = True Then
                If cadIds <> "" Then
                    cadIds += "|"
                End If
                cadIds += lstPrintedfor.Items(i).Value
            End If
        Next

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        Dim ws As ExcelWorksheet
        For i = 0 To lstGroup.Items.Count - 1
            If lstGroup.Items(i).Selected = True Then
                str_query = "EXEC [ACT].[rptTEACHER_ASSESSMENTS] " _
                         & "@ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                         & "@SBG_ID=" + ddlSubject.SelectedValue.ToString + "," _
                         & "@SGR_ID=" + lstGroup.Items(i).Value + "," _
                         & "@CAD_IDS='" + cadIds + "'," _
                         & "@RPF_ID=" + IIf(rdAssessment.Checked = True, "0", ddlPrintedFor.SelectedValue.ToString) + "," _
                         & "@TYPE='" + IIf(rdAssessment.Checked = True, "ASSESSMENT", "REPORTCARD") + "'"

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables.Count > 0 Then
                    Dim title As String = String.Empty
                    If lstGroup.Items(i).Text.Length > 30 Then
                        title = lstGroup.Items(i).Text.Substring(0, 30)
                    Else
                        title = lstGroup.Items(i).Text
                    End If
                    ws = ef.Worksheets.Add(title)
                    ws.InsertDataTable(ds.Tables(0), New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    '  ws.HeadersFooters.AlignWithMargins = True
                End If
            End If
        Next
        'Dim i As Integer
        'For i = 2 To dt.Columns.Count - 1
        '    ws.Columns(i).Style.NumberFormat = "#,##0.00"
        'Next
        If ef.Worksheets.Count > 0 Then
            ef.Save(tempFileName)

            'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(tempFileName)
            'HttpContext.Current.Response.End()

            Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

            System.IO.File.Delete(tempFileName)
        End If
    End Sub

    Sub ExportExcelWithAssessment()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim i As Integer

        Dim str_query As String
        Dim ds As DataSet

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        Dim ws As ExcelWorksheet
        For i = 0 To lstGroup.Items.Count - 1
            If lstGroup.Items(i).Selected = True Then
                str_query = "EXEC [ACT].[getTEACHERGRADEBOOK_WITHASSESSMENTS] " _
                         & "@ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                         & "@GRD_ID='" + grade(0) + "'," _
                         & "@EMP_ID=0," _
                         & "@SBG_ID=" + ddlSubject.SelectedValue.ToString + "," _
                         & "@SGR_ID=" + lstGroup.Items(i).Value + "," _
                         & "@TRM_ID=" + ddlTerm.SelectedValue.ToString + "," _
                         & "@TYPE='EXCEL'"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                Dim title2 As String = String.Empty
                If lstGroup.Items(i).Text.Length > 30 Then
                    title2 = lstGroup.Items(i).Text.Substring(0, 30)
                Else
                    title2 = lstGroup.Items(i).Text
                End If
                If ds.Tables.Count > 0 Then
                    ws = ef.Worksheets.Add(title2)
                    ws.InsertDataTable(ds.Tables(0), New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    ' ws.HeadersFooters.AlignWithMargins = True
                End If
            End If
        Next





        'Dim i As Integer
        'For i = 2 To dt.Columns.Count - 1
        '    ws.Columns(i).Style.NumberFormat = "#,##0.00"
        'Next
        If ef.Worksheets.Count > 0 Then
            ef.Save(tempFileName)



            'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(tempFileName)
            'HttpContext.Current.Response.End()

            Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

            System.IO.File.Delete(tempFileName)
        End If
    End Sub


    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_DESCRIPTION,TRM_ID FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + acdid + "' order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID='" + acd_id + "' AND SGS_EMP_ID='" + emp_id + "'" _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID='" + acd_id + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID='" + grade(1) + "'"

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateGroups(ByVal lstGroup As RadComboBox, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")

        lstGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " WHERE SGR_ACD_ID='" + acd_id + "'"

        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID='" + sbg_id + "'"
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + grd_id + "'"
        End If
        str_query += " ORDER BY SGR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGroup.DataSource = ds
        lstGroup.DataTextField = "SGR_DESCR"
        lstGroup.DataValueField = "SGR_ID"
        lstGroup.DataBind()
        Return lstGroup
    End Function

    Public Function PopulateSubject(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""

        Dim str_sql As String = ""
        'Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
        '                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
        '                     & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
        '                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
        '                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                      & "  grm_acd_id=" + acdid + " order by grd_displayorder"

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition = " AND SBG_GRD_ID='" & Grdid & "'"
        End If
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_sql = "Select distinct * from (SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & acdid & "'" _
                          & " " & strCondition & " "
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
        Else

            str_sql = " Select distinct * from (SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & acdid & "'"
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
            'If v_GradeID <> "" Then
            '    str_sql += "AND SBG_GRD_ID IN ('" & v_GradeID & "') ORDER BY GRM_DISPLAY "
            'End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        'ddl.DataTextField = "grm_display"
        'ddl.DataValueField = "grm_grd_id"
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Return ddl
    End Function
    Function PopulateGroupsByTeacher(ByVal emp_id As String, ByVal ddlGroup As RadComboBox, ByVal acd_id As String, Optional ByVal grd_id As String = "", Optional ByVal sbg_id As String = "")
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID " _
                                & " WHERE SGR_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                & " AND SGS_TODATE IS NULL"
        If sbg_id <> "" Then
            str_query += " AND SGR_SBG_ID=" + sbg_id
        End If

        If grd_id <> "" Then
            str_query += " AND SGR_GRD_ID='" + grd_id + "'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        Return ddlGroup
    End Function
    Sub BindAssessment()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        If rdAssessment.Checked = True Then
            str_query = "SELECT DISTINCT CAD_DESC,CAD_ID FROM ACT.ACTIVITY_D " _
                       & " INNER JOIN ACT.ACTIVITY_SCHEDULE ON CAS_CAD_ID=CAD_ID" _
                       & " WHERE  CAD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND CAS_SBG_ID=" + ddlSubject.SelectedValue.ToString
            If ddlTerm.SelectedValue.ToString <> "0" Then
                str_query += " AND CAD_TRM_ID=" + ddlTerm.SelectedValue.ToString
            End If
        Else
            str_query = "SELECT DISTINCT RPF_DESCR AS CAD_DESC,RPF_ID AS CAD_ID FROM RPT.REPORT_SETUP_M AS A" _
                        & " INNER JOIN RPT.REPORT_PRINTEDFOR_M AS B ON A.RSM_ID=B.RPF_RSM_ID" _
                                         & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.RSM_ID=C.RSG_RSM_ID" _
                                         & " WHERE  RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                         & " AND RSG_GRD_ID='" + grade(0) + "'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If rdReport.Checked = True Then
            ddlPrintedFor.DataSource = ds
            ddlPrintedFor.DataTextField = "CAD_DESC"
            ddlPrintedFor.DataValueField = "CAD_ID"
            ddlPrintedFor.DataBind()
            ddlPrintedFor.Visible = True
            lstPrintedfor.Visible = False
        Else
            lstPrintedfor.DataSource = ds
            lstPrintedfor.DataTextField = "CAD_DESC"
            lstPrintedfor.DataValueField = "CAD_ID"
            lstPrintedfor.DataBind()
            ddlPrintedFor.Visible = False
            lstPrintedfor.Visible = True
        End If
    End Sub


#End Region

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ExportExcel()
    End Sub


    'test
    Protected Sub rdAssessment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAssessment.CheckedChanged
        If rdAssessment.Checked = True Then
            BindAssessment()
        End If
    End Sub

    Protected Sub rdReport_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdReport.CheckedChanged
        If rdReport.Checked = True Then
            BindAssessment()
        End If
    End Sub
End Class
