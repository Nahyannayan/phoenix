Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_Reports_Aspx_rptGradeWiseGroups
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C970020") And (ViewState("MainMnu_code") <> "T000160")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindStream()
                BindGrade()
                BindDataSet()
                GridBind()


            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        Else
            studClass.SetChk(gvSubjects, Session("liUserList"))
        End If

       
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER " _
                                  & " FROM GRADE_BSU_M AS A INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID" _
                                  & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND " _
                                  & " GRM_STM_ID=" + ddlStream.SelectedValue.ToString + " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRD_ID"
        lstGrade.DataBind()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvSubjects.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvSubjects.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub BindDataSet()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,SBG_DESCR,SBG_PARENTS,GRM_DISPLAY,GRM_GRD_ID as GRD_ID FROM SUBJECTS_GRADE_S AS A " _
                                  & " INNER JOIN GRADE_BSU_M AS B ON A.SBG_GRD_ID=B.GRM_GRD_ID AND A.SBG_ACD_ID=B.GRM_ACD_ID AND A.SBG_STM_ID=B.GRM_STM_ID " _
                                  & "  WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_STM_ID=" + ddlStream.SelectedValue.ToString _
                                  & " ORDER BY SBG_DESCR,SBG_PARENTS"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Session("dsGradeSubjects") = ds
    End Sub
    Sub BindStream()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "SELECT DISTINCT STM_ID,STM_DESCR FROM STREAM_M AS A INNER JOIN" _
                                & " GRADE_BSU_M AS B ON A.STM_ID=B.GRM_STM_ID WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & "  ORDER BY STM_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()
        If Not ddlStream.Items.FindByValue("1") Is Nothing Then
            ddlStream.Items.FindByValue("1").Selected = True
        End If

    End Sub
    Sub GridBind()
        Dim dv As DataView
        Dim ds As DataSet = Session("dsGradeSubjects")
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim subjectSearch As String = ""
        Dim shortSearch As String = ""

        Dim txtSearch As New TextBox
        Dim ddlgvlang As New DropDownList
        Dim selectedLang As String = ""

        dv = ds.tables(0).defaultview
        dv.RowFilter = ""
        If gvSubjects.Rows.Count > 0 Then


            txtSearch = gvSubjects.HeaderRow.FindControl("txtSubject")
            strSidsearch = h_selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter = GetSearchString("SBG_DESCR", txtSearch.Text, strSearch)
            End If
            subjectSearch = txtSearch.Text


            txtSearch = gvSubjects.HeaderRow.FindControl("txtParent")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += " AND "
                End If
                strFilter += GetSearchString("SBG_PARENTS", txtSearch.Text, strSearch)
            End If
            shortSearch = txtSearch.Text


        End If

        Dim grdids As String = ""
        Dim i As Integer
        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If grdids <> "" Then
                    grdids += ","
                End If
                grdids += "'" + lstGrade.Items(i).Value + "'"
            End If
        Next

        If grdids <> "" Then
            If strFilter <> "" Then
                strFilter += " and "
            End If
            strFilter += " grd_id in (" + grdids + ")"
        End If

        If strFilter <> "" Then
            dv.RowFilter = strFilter
        End If

        gvSubjects.DataSource = dv
        gvSubjects.DataBind()
       

        txtSearch = gvSubjects.HeaderRow.FindControl("txtSubject")
        txtSearch.Text = subjectSearch

        txtSearch = gvSubjects.HeaderRow.FindControl("txtParent")
        txtSearch.Text = shortSearch

    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = +field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub CallReport()
        Dim grdXML As String = ""
        Dim grdTempXML As String
        Dim subXML As String = ""
        Dim subTempXML As String = ""
        Dim strXml As String = ""
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim chkAll As CheckBox
        Dim lblSbgId As Label
        Dim lblGrm As Label
        Dim sType As String

        'IF NO GRADE IS SELECTED THE QUERY INCLUDES ALL GRADES
        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                grdXML += "<ID><GRD_ID>" + lstGrade.Items(i).Value + "</GRD_ID></ID>"
            End If
            grdTempXML += "<ID><GRD_ID>" + lstGrade.Items(i).Value + "</GRD_ID></ID>"
        Next
        If grdXML = "" Then
            grdXML = grdTempXML
        End If

           chkAll = gvSubjects.HeaderRow.FindControl("chkAll")
        If chkAll.Checked = False Then
            For i = 0 To gvSubjects.Rows.Count - 1
                chkSelect = gvSubjects.Rows(i).FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblSbgId = gvSubjects.Rows(i).FindControl("lblSbgId")
                    subXML += "<ID><SBG_ID>" + lblSbgId.Text + "</SBG_ID></ID>"
                End If
            Next
        End If
        If subXML <> "" Then
            strXml = "<IDS>" + subXML + "</IDS>"
            sType = "Sub"
        Else
            strXml = "<IDS>" + grdXML + "</IDS>"
            sType = "Grade"
        End If

        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@STM_ID", ddlStream.SelectedValue.ToString)
        param.Add("@STR_XML", strXml)
        param.Add("@TYPE", sType)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("stream", ddlStream.SelectedItem.Text)
        param.Add("acyear", ddlAcademicYear.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptGradeWiseGroups.rpt")
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

#End Region
    Protected Sub gvSubjects_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubjects.PageIndexChanging
        gvSubjects.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub btnSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnParent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindStream()
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        BindStream()
    End Sub

    Protected Sub lstGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGrade.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub
End Class
