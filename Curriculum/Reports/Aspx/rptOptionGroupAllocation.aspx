<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptOptionGroupAllocation.aspx.vb" Inherits="rptOptionGroupAllocation" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 
    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table class ="matters" align="center" border="1" bordercolor="#1b80b6" cellpadding="4" cellspacing="0" width="50%">
                    <tr>
                        <td valign="middle" class="subheader_img" colspan="6">
                            <asp:Label id="lblHeader" runat="server" Text="Option Group Allocation-"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Academic Year</td>
                        <td align="center">
                            :</td>
                        <td align="left" colspan="4">
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Grade</td>
                        <td align="center">
                            :</td>
                        <td align="left">
                            <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left">
                            Section </td>
                        <td align="center">
                            :</td>
                        <td align="left">
                            <asp:DropDownList id="ddlSection" runat="server" >
                            </asp:DropDownList></td>
                    </tr>
        <tr>
            <td align="left" colspan="6" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" SkinID="ButtonNormal" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                <asp:Button id="btnCancel" runat="server" SkinID="ButtonNormal" Text="Cancel" /></td>
        </tr>
                </table>
    </asp:Content>

