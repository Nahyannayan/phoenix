<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptMonthlyProgressReportStudWise.aspx.vb" Inherits="clmActivitySchedule"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
    <script language="javascript" type="text/javascript">

        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
        var ACD_IDs = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
        var gender = document.getElementById('<%=ddlGender.ClientID %>').value;
        var sen = document.getElementById('<%=chkSen.ClientID %>').checked;
        if (GRD_IDs == '') {
            alert('Please select atleast one Grade')
            return false;
        }
        var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs + "&gender=" + gender + "&sen=" + sen, "RadWindow1");

    }

        function OnClientClose(oWnd, args) {
          
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
          
            NameandCode = arg.NameCode.split('||');
            document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=txtStudIDs.ClientID()%>").value = NameandCode[1];
             __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
          var ACD_IDs = document.getElementById('<%=ddlAca_Year.ClientID %>').value;
          var gender = document.getElementById('<%=ddlGender.ClientID %>').value;
          var sen = document.getElementById('<%=chkSen.ClientID %>').checked;
          if (GRD_IDs == '') {
              alert('Please select atleast one Grade')
              return false;
          }
          result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs + "&gender=" + gender + "&sen=" + sen, "", sFeatures)
          if (result != '' && result != undefined) {
              document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Progress Report"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Button ID="btnCheck" Style="display: none;" runat="server" />

                <table  align="center" width="100%" style="border-collapse: collapse"
                    cellpadding="4" cellspacing="0">

                    <tr>
                        <td align="left"><span class="field-label">Academic Year</span></td>

                        <td align="left" >
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td colspan="2"></td>                        
                    </tr>
                
                    <tr>
                        <td align="left"><span class="field-label">Report Type</span></td>

                        <td align="left" >
                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged"
                                >
                            </asp:DropDownList></td>
                        <td align="left"><span class="field-label">Report Printed For</span></td>

                        <td align="left" >
                            <asp:DropDownList ID="ddlReportPrintedFor" runat="server" >
                            </asp:DropDownList></td>
                       
                    </tr>
                    <tr id="20">
                         <td align="left" valign="middle"><span class="field-label">Grade</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="middle"><span class="field-label">Section</span>
                        </td>

                        <td align="left"  valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                        
                    </tr>
                    <tr>
                        <td align="left" valign="middle" ><span class="field-label">Boys/Girls</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGender" runat="server">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem Value="M">BOYS</asp:ListItem>
                                <asp:ListItem Value="F">GIRLS</asp:ListItem>
                            </asp:DropDownList></td>
                        <td >
                            <asp:CheckBox ID="chkSen" runat="server" Text="SEN Students" CssClass="field-label"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="field-label">Student</span></td>

                        <td align="left" >
                            <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged" ></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;"
                                OnClick="imgStudent_Click"></asp:ImageButton>
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"  CssClass="table table-bordered table-row"
                                PageSize="5"  OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                OnClick="btnGenerateReport_Click" />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                               Text="Download Report in Pdf" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                <CR:CrystalReportSource ID="rs1" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfReportType" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfReportFormat" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>

</asp:Content>
