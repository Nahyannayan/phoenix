<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAOL_FA_RA_SkillAcrossAssessment.aspx.vb" Inherits="rptFormativeConsolidated" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 
   <script language="javascript" type="text/javascript">
   
   function openWin() {
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
          
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var GRD = GRD_IDs.split('|');
            var ACD_ID =document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
                                                
            var oWnd = radopen("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD[0] + "&ACD_ID=" + ACD_ID + "&SCT_IDs=" + SCT_IDs, "RadWindow1");
            
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.Students.split('|');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=btnCheck.ClientID %>").click();
            }
        }
        

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
   
   
            function GetSTUDENTS()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 600px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
          
            var SCT_IDs =document.getElementById('<%=ddlSection.ClientID %>').value;
            var GRD_IDs =document.getElementById('<%=ddlGrade.ClientID %>').value;
            var GRD = GRD_IDs.split('|');
            var ACD_ID =document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD[0] + "&ACD_ID=" + ACD_ID + "&SCT_IDs=" + SCT_IDs ,"", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
         }
    
                 
   </script>
   <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
          
        </Windows>
    </telerik:RadWindowManager>
    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <asp:Button ID="btnCheck" style="display:none;"  runat="server" />
    <table class ="matters" align="center" border="1" bordercolor="#1b80b6" cellpadding="4" cellspacing="0" >
        <tr>
            <td valign="middle" class="subheader_img" colspan="3">
                <asp:Label id="lblHeader" runat="server" Text="Progress Report"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                Academic Year</td>
            <td align="center">
                :</td>
            <td align="left">
                <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" 
                   >
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                Report Card</td>
            <td align="center" >
                :</td>
            <td align="left">
                <asp:DropDownList id="ddlReportCard" runat="server" 
                    Width="331px" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr valign="top">
            <td align="left">
                Schedule</td>
            <td align="center">
                :</td>
            <td align="left">
                            <asp:CheckBoxList id="ddlPrintedFor" runat="server" 
                    BorderStyle="Solid" BorderWidth="1px"
                                 RepeatLayout="Flow" style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                                vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                                text-align: left" Width="239px" AutoPostBack="True">
                            </asp:CheckBoxList></td>
        </tr>
        <tr valign="top">
            <td align="left">
                Activties</td>
            <td align="center">
                :</td>
            <td align="left">
                            <asp:CheckBoxList id="ddlActivities" runat="server" 
                    BorderStyle="Solid" BorderWidth="1px"
                                 RepeatLayout="Flow" style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid;
                                vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;
                                text-align: left" Width="239px">
                            </asp:CheckBoxList></td>
        </tr>
        <tr id ="20" >
            <td align="left" valign="middle">
                Grade</td>
            <td align="center" valign="middle">
                :</td>
            <td align="left" valign="middle">
                <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True" 
                    >
                </asp:DropDownList>&nbsp;
                Section :
                <asp:DropDownList ID="ddlSection" runat="server" 
                    AutoPostBack="True" >
                </asp:DropDownList>
            </td>
        </tr>
      
        <tr id="trstud1" runat="server">
            <td align="left" valign="top" >
                Student</td>
            <td align="center" valign="top" >
                :</td>
            <td align="left">
                <asp:TextBox id="txtStudIDs" runat="server" Width="330px"></asp:TextBox>
                <asp:ImageButton id="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click">
                </asp:ImageButton>
                <asp:GridView id="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    PageSize="5" Width="353px" OnPageIndexChanging="grdStudent_PageIndexChanging">
                    <columns>
<asp:TemplateField HeaderText="Stud. No"><ItemTemplate>
<asp:Label id="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
</columns>
                    <headerstyle cssclass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="text-align: left">
                Compare
                <asp:RadioButton ID="radOnlyAOL" runat="server" GroupName="TYPE" 
                    Text="Only AOL" />
                <asp:RadioButton ID="radOnlyFA" runat="server" GroupName="TYPE" 
                    Text="Only FA" />
                <asp:RadioButton ID="radAll" runat="server" Checked="True" GroupName="TYPE" 
                    Text="All" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="text-align: center">
                <asp:Button id="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="hfbFinalReport" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_STU_IDs" runat="server"/>
    <asp:HiddenField  id="h_SBG_IDs" runat="server"/>
</asp:Content>

