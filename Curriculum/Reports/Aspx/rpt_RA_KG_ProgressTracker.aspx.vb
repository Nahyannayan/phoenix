﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports Telerik.Web.UI
Imports InfosoftGlobal
Partial Class Curriculum_Reports_Aspx_rpt_RA_KG_ProgressTracker
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lblHeader.Text = Encr_decrData.Decrypt(Request.QueryString("strtext").Replace(" ", "+"))
            hfRPF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rpf_id").Replace(" ", "+"))
            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            hfSbgIds.Value = Encr_decrData.Decrypt(Request.QueryString("sbgid").Replace(" ", "+"))
            hfSCT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+"))
            hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
            hfRSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
            GridBind()
        End If
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        '  Dim str_query As String = "RPT.KG_PROGRESS_TRACKER 965,2559,4226,'KG1','0','29421|29422|29423|29424'"
        Dim str_query As String = "RPT.KG_PROGRESS_TRACKER " + hfACD_ID.Value + "," _
                                  & hfRSM_ID.Value + "," _
                                  & hfRPF_ID.Value + "," _
                                  & "'" + hfGRD_ID.Value + "'," _
                                  & "'" + hfSCT_ID.Value + " '," _
                                  & "'" + hfSbgIds.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        rgProgress.DataSource = ds
        rgProgress.DataBind()

        ' rgProgress.ClientSettings.AllowDragToGroup = False
        PoplateOverallChart(ds)
        PopulateOtherCharts(ds)

        If Session("SBSUID") <> "133006" Then
            rgProgress.Columns(1).Visible = False
        End If
    End Sub

    Sub PoplateOverallChart(ds As DataSet)
        Dim dv As DataView = ds.Tables(0).DefaultView
        dv.RowFilter = "SUBJECT='OVERALL'"

        Dim sChart As New StringBuilder
        Dim chartLabel As String


        Dim i As Integer
        Dim columns As Integer = dv.Table.Columns.Count
        Dim totalStudents As Double = CDbl(dv.Table.Rows(0).Item(columns - 1))
        If totalStudents = 0 Then totalStudents = 1


        sChart.AppendLine("<chart chartLeftMargin='0' caption='OVERALL' showBorder='1' borderColor='#000000' borderThickness='1'  baseFontSize='8' captionPadding='0'  chartRightMargin='0' chartTopMargin='0' chartBottomMargin='10'  palette='3' showLegend='1'  showPercentValues='1'>")
        For i = 3 To dv.Table.Columns.Count - 2
            If dv.Table.Rows(0).Item(i) <> "0" Then
                If Session("Sbsuid") = "133006" Then
                    If dv.Table.Columns(i).ColumnName = "EEA" Then
                        chartLabel = "Always Exceeds"
                    ElseIf dv.Table.Columns(i).ColumnName = "EE" Then
                        chartLabel = "Mostly Exceeds Expectation"
                    ElseIf dv.Table.Columns(i).ColumnName = "MEA" Then
                        chartLabel = "Always Meets Expectation"
                    ElseIf dv.Table.Columns(i).ColumnName = "MEM" Then
                        chartLabel = "Mostly Meets Expectation"
                    ElseIf dv.Table.Columns(i).ColumnName = "WT" Then
                        chartLabel = "Working Towards Expectation"
                    End If
                Else
                    If dv.Table.Columns(i).ColumnName = "EE" Then
                        chartLabel = "EXCEEDS EXPECTATIONS"
                    ElseIf dv.Table.Columns(i).ColumnName = "MEA" Then
                        chartLabel = "MEETS EXPECTATIONS ALWAYS"
                    ElseIf dv.Table.Columns(i).ColumnName = "MEM" Then
                        chartLabel = "MEETS EXPECTATIONS MOSTLY"
                    ElseIf dv.Table.Columns(i).ColumnName = "WT" Then
                        chartLabel = "WORKING TOWARDS EXPECTATIONS"
                    End If
                End If

                sChart.AppendLine("<set isSliced='1' showLabel='0' toolText='" + chartLabel + "' label='" + chartLabel + "' value='" + (Math.Round(dv.Table.Rows(0).Item(i) * 100 / totalStudents, 2)).ToString + "' />")
            End If

        Next
        sChart.AppendLine("</chart>")
        ltOverall.Text = FusionCharts.RenderChart("FusionCharts/Pie3D.swf", "", sChart.ToString, "chart1000000", "250", "200", False, False)



    End Sub

    Sub PopulateOtherCharts(ds As DataSet)
        Dim dv As DataView = ds.Tables(0).DefaultView
        dv.RowFilter = "SUBJECT<>'OVERALL'"
        Dim pallete As String


        Dim chartLabel As String
        Dim i, j As Integer
        Dim columns As Integer = dv.Table.Columns.Count
        Dim totalStudents As Double = CDbl(dv.Table.Rows(0).Item(columns - 1))
        Dim chartHeader As String
        If totalStudents = 0 Then totalStudents = 1
        ltOther.Text = "<table><tr>"
        Dim skillCount As Integer = 0
        For j = 0 To dv.Table.Rows.Count - 1
            If dv.Table.Rows(j).Item(1).ToString <> "OVERALL" Then
                skillCount += 1
                If skillCount > 4 Then
                    skillCount = 0
                    ltOther.Text += "</tr><tr>"
                End If
                If dv.Table.Rows(j).Item(2).ToString = "OVERALL" Then
                    skillCount = 0
                    chartHeader = dv.Table.Rows(j).Item(1).ToString
                    If j <> 0 Then
                        ltOther.Text += "</tr><tr style='border-top:1pt solid black !important;'>"
                    End If
                    pallete = "bgColor='#eef0e7'"
                Else
                    chartHeader = dv.Table.Rows(j).Item(2).ToString
                    pallete = ""
                End If
                Dim sChart As New StringBuilder
                sChart.AppendLine("<chart " + pallete + " chartLeftMargin='0' caption='" + chartHeader + "' showBorder='1' borderColor='#000000' borderThickness='1'  baseFontSize='8' captionPadding='0'  chartRightMargin='0' chartTopMargin='0' chartBottomMargin='10'  palette='3' showLegend='1'  showPercentValues='1'>")
                For i = 3 To dv.Table.Columns.Count - 2

                    With dv.Table.Rows(j)
                        If .Item(i) <> "0" Then
                            If Session("sbsuid") = "133006" Then
                                If dv.Table.Columns(i).ColumnName = "EEA" Then
                                    chartLabel = "Always Exceeds"
                                ElseIf dv.Table.Columns(i).ColumnName = "EE" Then
                                    chartLabel = "Mostly Exceeds Expectation"
                                ElseIf dv.Table.Columns(i).ColumnName = "MEA" Then
                                    chartLabel = "Always Meets Expectation"
                                ElseIf dv.Table.Columns(i).ColumnName = "MEM" Then
                                    chartLabel = "Mostly Meets Expectation"
                                ElseIf dv.Table.Columns(i).ColumnName = "WT" Then
                                    chartLabel = "Working Towards Expectation"
                                End If
                            Else
                                If dv.Table.Columns(i).ColumnName = "EE" Then
                                    chartLabel = "EXCEEDS EXPECTATIONS"
                                ElseIf dv.Table.Columns(i).ColumnName = "MEA" Then
                                    chartLabel = "MEETS EXPECTATIONS ALWAYS"
                                ElseIf dv.Table.Columns(i).ColumnName = "MEM" Then
                                    chartLabel = "MEETS EXPECTATIONS MOSTLY"
                                ElseIf dv.Table.Columns(i).ColumnName = "WT" Then
                                    chartLabel = "WORKING TOWARDS EXPECTATIONS"
                                End If
                            End If
                            sChart.AppendLine("<set isSliced='1' showLabel='0' toolText='" + chartLabel + "' label='" + chartLabel + "' value='" + (Math.Round(.Item(i) * 100 / totalStudents, 2)).ToString + "' />")
                        End If
                    End With
                Next
                sChart.AppendLine("</chart>")
                ltOther.Text += "<td>"
                ltOther.Text += FusionCharts.RenderChart("FusionCharts/Pie3D.swf", "", sChart.ToString, "chart" + ((j + 1) * i).ToString, "250", "200", False, False)
                ltOther.Text += "</td>"
            End If
        Next
        ltOther.Text += "</tr></table>"
    End Sub

    Protected Sub rgProgress_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles rgProgress.ItemDataBound
        If e.Item.ItemType = GridItemType.Header Then
            Dim header As GridHeaderItem = e.Item
            If Session("SBSUID") = "133006" Then

                header("EE").Text = "Mostly Exceeds Expectation"
                header("MEA").Text = "Always Meets Expectation"
                header("MEM").Text = "Mostly Meets Expectation"
                header("WT").Text = "Working Towards Expectation"
                header("TOTAL").Text = "Total"
            End If
        End If
    End Sub

    Protected Sub rgProgress_PreRender(sender As Object, e As EventArgs) Handles rgProgress.PreRender
        Dim gC As GridColumn
        For Each gC In rgProgress.Columns
            If gC.UniqueName = "SKILLS" Then
                gC.ItemStyle.Width = 150
                gC.HeaderStyle.Width = 160
            Else
                gC.ItemStyle.Width = 90
                gC.HeaderStyle.Width = 90
            End If

        Next
    End Sub
End Class
