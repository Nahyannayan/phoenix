Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports CURRICULUM
Imports Telerik.Web.UI

Partial Class Curriculum_Reports_Aspx_rptIntlStudentPerformanceBySubject
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400041" And ViewState("MainMnu_code") <> "C400051" And ViewState("MainMnu_code") <> "C400375" And ViewState("MainMnu_code") <> "C400380") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindReportCard()
                BindHeader()
                BindPrintedFor()
                PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                BindSection()
                ViewState("display") = "textbox"

                If ViewState("MainMnu_code") = "C400041" Or ViewState("MainMnu_code") = "C400380" Then
                    trsubject.Visible = True
                    trstudent.Visible = False

                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If
                    btnGraph.Visible = True
                Else
                    trstudent.Visible = True
                    trsubject.Visible = False
                End If

                tbData.Rows(0).Visible = False
                lblA2.Visible = False
                txtG2.Visible = False
            End If
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindHeader()
        BindPrintedFor()
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSection()

        If ViewState("MainMnu_code") = "C400041" Or ViewState("MainMnu_code") = "C400380" Then
            trsubject.Visible = True
            trstudent.Visible = False

            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If
        Else
            trstudent.Visible = True
            trsubject.Visible = False
        End If


        BindDefaults()
        ' chkAll.Checked = Fals  e
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()

        If ViewState("MainMnu_code") = "C400041" Or ViewState("MainMnu_code") = "C400380" Then

            trsubject.Visible = True
            trstudent.Visible = False


            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If
        Else
            trstudent.Visible = True
            trsubject.Visible = False
        End If



        BindDefaults()
        '   chkAll.Checked = False
    End Sub


    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

#Region "Private Methods"


    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                 & " AND (ISNULL(RSD_bANALYSIS,0)=1 OR ISNULL(RSD_BPERFORMANCE_INDICATOR,0)=1)" _
                                 & " AND RSD_CSSCLASS='textboxsmall' ORDER BY RSD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        rdHeader.DataSource = ds
        rdHeader.DataTextField = "RSD_HEADER"
        rdHeader.DataValueField = "RSD_ID"
        rdHeader.DataBind()

        If rdHeader.Items.Count > 0 Then
            rdHeader.Items(0).Selected = True
        End If
    End Sub

    'Function getSubjects() As String
    '    Dim i As Integer
    '    Dim str As String = ""
    '    For i = 0 To ddlSubject.Items.Count - 1
    '        If ddlSubject.Items(i).Selected = True Then
    '            If str <> "" Then
    '                str += ","
    '            End If
    '            str += ddlSubject.Items(i).Value
    '        End If
    '    Next
    '    Return str
    'End Function

    Function getSubjects() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = ddlSubject.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += ","
                End If
            Next
            str = str.TrimEnd(",")
        Else
            If ddlSubject.SelectedIndex > 0 Then
                str = ddlSubject.SelectedItem.Value
            End If
        End If
        Return str
    End Function

    Sub CheckAll(ByVal chkUncheck As Boolean)
        Dim i As Integer
        Select Case Session("sbsuid")
            Case "125005", "125011", "125012", "125002"
                For i = 0 To ddlSubject.Items.Count - 1
                    If chkUncheck = True Then
                        If ddlSubject.Items(i).Text.ToUpper <> "ARABIC" Then
                            ddlSubject.Items(i).Selected = True
                        End If
                    Else
                        ddlSubject.Items(i).Selected = False
                    End If
                Next
        End Select

    End Sub
    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As RadComboBox, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
        '                         & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
        '                         & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
        '                         & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
        '                         & " WHERE SBG_ACD_ID='" + acd_id + "' AND SGS_EMP_ID='" + emp_id + "'" _
        '                         & " AND SGS_TODATE IS NULL"

        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID='" + acd_id + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        hfSubjects.Value = ""

        Return ddlSubject
    End Function


    Function PopulateSubjects(ByVal ddlSubject As RadComboBox, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID='" + acd_id + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID='" + grade(1) + "'"

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        hfSubjects.Value = ""

        Return ddlSubject
    End Function

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportSchedule.DataSource = ds
        ddlReportSchedule.DataTextField = "RPF_DESCR"
        ddlReportSchedule.DataValueField = "RPF_ID"
        ddlReportSchedule.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        'Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
        '                      & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
        '                      & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
        '                      & " where grm_acd_id=" + acdid _
        '                      & " order by grd_displayorder"

        Dim str_query As String

        'If Session("CurrSuperUser") = "Y" Then
        str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                    & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                    & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                    & " inner join oasis..stream_m as p on a.grm_stm_id=p.stm_id " _
                    & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                    & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                    & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                    & " ORDER BY RSG_DISPLAYORDER"
        'ElseIf studClass.isEmpTeacher(Session("EmployeeId")) = True Then

        '    str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                 & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
        '                 & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
        '                 & " inner join oasis..stream_m as p on a.grm_stm_id=p.stm_id " _
        '                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
        '                 & " INNER JOIN GROUPS_M AS D ON A.GRM_GRD_ID=D.SGR_GRD_ID AND A.GRM_ACD_ID=D.SGR_GRD_ID" _
        '                 & " INNER JOIN GROUPS_TEACHER_S AS E ON D.SGR_ID=E.SGS_SGR_ID AND SGS_EMP_ID='" + Session("EMPLOYEEID") + "'" _
        '                 & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
        '                 & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
        '                 & " ORDER BY RSG_DISPLAYORDER"

        'Else
        '    str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
        '                 & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
        '                 & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
        '                 & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
        '                 & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
        '                 & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
        '                 & " ORDER BY RSG_DISPLAYORDER"
        'End If




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function



    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindDefaults()


        If hfSubjects.Value <> "" Then
            ddlG1.Items.Clear()
            ddlG2.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT DISTINCT RGP_GRADE,RGP_VALUE FROM RPT.REPORT_GRADEMAPPING " _
                                   & " WHERE RGP_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                   & " AND RGP_SBG_ID IN(" + hfSubjects.Value + ")" _
                                   & " ORDER BY RGP_VALUE"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlG1.DataSource = ds
            ddlG1.DataTextField = "RGP_GRADE"
            ddlG1.DataValueField = "RGP_VALUE"
            ddlG1.DataBind()

            ddlG2.DataSource = ds
            ddlG2.DataTextField = "RGP_GRADE"
            ddlG2.DataValueField = "RGP_VALUE"
            ddlG2.DataBind()
            If ddlG1.Items.Count = 0 Then
                ViewState("display") = "textbox"
            Else
                ViewState("display") = "dropdown"
            End If
        Else
            ViewState("display") = "textbox"
        End If


        If ViewState("display") = "textbox" Then
            tbData.Rows(0).Visible = False
            tbData.Rows(1).Visible = True
            If rdBetween.Checked = True Then
                lblA2.Visible = True
                txtG2.Visible = True
            Else
                lblA2.Visible = False
                txtG2.Visible = False
            End If
        Else
            tbData.Rows(0).Visible = True
            tbData.Rows(1).Visible = False
            If rdBetween.Checked = True Then
                lblA1.Visible = True
                ddlG2.Visible = True
            Else
                lblA1.Visible = False
                ddlG2.Visible = False
            End If
        End If


    End Sub

    Function GetSubjectReportQuery() As String
        Dim str As String

        Dim value1 As String
        Dim value2 As String
        Dim text1 As String
        Dim text2 As String
        Dim valuetype As String

        If ViewState("display") = "textbox" Then

            If IsNumeric(txtG1.Text) = True Then
                valuetype = "number"
                value1 = txtG1.Text
                value2 = txtG2.Text
            Else
                value1 = GetValue(txtG1.Text)
                value2 = GetValue(txtG2.Text)
                valuetype = "string"
            End If

            text1 = txtG1.Text
            text2 = txtG2.Text
        Else
            value1 = ddlG1.SelectedValue.ToString
            value2 = ddlG2.SelectedValue.ToString

            text1 = ddlG1.SelectedItem.Text
            text2 = ddlG2.SelectedItem.Text

            valuetype = "string"
        End If

        If valuetype = "string" Then

            If Session("current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS,SGR_DESCR, " _
                    & " EMP_NAME=(SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                    & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                    & " WHERE(SGS_SGR_ID = B.RST_SGR_ID)" _
                    & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''))" _
                    & " FROM VW_STUDENT_M AS A INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.STU_ID=B.RST_STU_ID" _
                    & " INNER JOIN VW_SECTION_M AS GA ON A.STU_SCT_ID=GA.SCT_ID" _
                    & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                    & " INNER JOIN RPT.REPORT_SETUP_M AS D ON C.RSD_RSM_ID=D.RSM_ID" _
                    & " INNER JOIN RPT.REPORT_GRADEMAPPING AS E ON D.RSM_ID=E.RGP_RSM_ID" _
                    & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                    & " AND RST_SBG_ID=RGP_SBG_ID AND ISNULL(RST_COMMENTS,RST_GRADING)=RGP_GRADE" _
                    & " INNER JOIN STUDENT_GROUPS_S AS P ON B.RST_SBG_ID=P.SSD_SBG_ID AND B.RST_STU_ID=P.SSD_STU_ID" _
                    & " INNER JOIN GROUPS_M AS T ON P.SSD_SGR_ID=T.SGR_ID" _
                    & " WHERE 1=1"
            Else

                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS,SGR_DESCR, " _
                 & " EMP_NAME=(SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                 & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                 & " WHERE(SGS_SGR_ID = B.RST_SGR_ID)" _
                 & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''))" _
                 & " FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B ON A.STU_ID=B.RST_STU_ID" _
                 & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                 & " INNER JOIN RPT.REPORT_SETUP_M AS D ON C.RSD_RSM_ID=D.RSM_ID" _
                 & " INNER JOIN RPT.REPORT_GRADEMAPPING AS E ON D.RSM_ID=E.RGP_RSM_ID" _
                 & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                 & " AND RST_SBG_ID=RGP_SBG_ID AND ISNULL(RST_COMMENTS,RST_GRADING)=RGP_GRADE" _
                 & " INNER JOIN STUDENT_GROUPS_S AS P ON B.RST_SBG_ID=P.SSD_SBG_ID AND B.RST_STU_ID=P.SSD_STU_ID" _
                 & " INNER JOIN GROUPS_M AS T ON P.SSD_SGR_ID=T.SGR_ID" _
                 & " AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " WHERE 1=1"

            End If

            If rdAbove.Checked = True Then
                str += " AND RGP_VALUE>'" + value1 + "'"
                ViewState("criteria") = "Score above " + text1
            ElseIf rdBelow.Checked = True Then
                str += " AND RGP_VALUE<'" + value1 + "'"
                ViewState("criteria") = "Score below " + text1
            Else
                str += " AND RGP_VALUE BETWEEN '" + value1 + "' AND '" + value2 + "'"
                ViewState("criteria") = "Score between " + text1 + " and " + text2
            End If

        Else

            If Session("current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS,SGR_DESCR, " _
                      & " EMP_NAME=(SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                      & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                      & " WHERE(SGS_SGR_ID = B.RST_SGR_ID)" _
                      & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''))" _
                      & " FROM VW_STUDENT_M AS A INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.STU_ID=B.RST_STU_ID" _
                      & " INNER JOIN VW_SECTION_M AS G ON A.STU_SCT_ID=G.SCT_ID" _
                      & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                      & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                      & " INNER JOIN STUDENT_GROUPS_S AS G ON B.RST_SBG_ID=G.SSD_SBG_ID AND B.RST_STU_ID=G.SSD_STU_ID" _
                      & " INNER JOIN GROUPS_M AS T ON G.SSD_SGR_ID=T.SGR_ID" _
                      & " WHERE 1=1"
            Else

                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS,SGR_DESCR, " _
                    & " EMP_NAME=(SELECT STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                    & " FROM VW_EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                    & " WHERE(SGS_SGR_ID = B.RST_SGR_ID)" _
                    & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''))" _
                    & " FROM VW_STUDENT_PREVYEARS AS A INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B ON A.STU_ID=B.RST_STU_ID" _
                    & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                    & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                    & " INNER JOIN STUDENT_GROUPS_S AS G ON B.RST_SBG_ID=G.SSD_SBG_ID AND B.RST_STU_ID=G.SSD_STU_ID" _
                    & " INNER JOIN GROUPS_M AS T ON G.SSD_SGR_ID=T.SGR_ID" _
                    & " AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                    & " WHERE 1=1"

            End If
            If rdAbove.Checked = True Then
                str += " AND ISNULL(RST_COMMENTS,RST_GRADING)>'" + value1 + "'"
                ViewState("criteria") = "Score above " + text1
            ElseIf rdBelow.Checked = True Then
                str += " AND ISNULL(RST_COMMENTS,RST_GRADING)<'" + value1 + "'"
                ViewState("criteria") = "Score below " + text1
            Else
                str += " AND ISNULL(RST_COMMENTS,RST_GRADING) BETWEEN '" + value1 + "' AND '" + value2 + "'"
                ViewState("criteria") = "Score between " + text1 + " and " + text2
            End If
        End If

        Dim grade() As String = ddlGrade.SelectedValue.Split("|")
        str += " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        str += " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_STM_ID=" + grade(1)
        str += " AND RST_RPF_ID=" + ddlReportSchedule.SelectedValue.ToString

        If ddlSection.SelectedValue.ToString <> "0" Then
            str += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        If hfSubjects.Value <> "" Then
            str += " AND SBG_ID IN(" + hfSubjects.Value + ")"
        End If



        str += " ORDER BY SBG_DESCR,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Return str
    End Function



    Function GetStudentReportQuery() As String
        Dim str As String

        Dim value1 As String
        Dim value2 As String

        Dim valuetype As String


        Dim text1 As String
        Dim text2 As String


        If ViewState("display") = "textbox" Then

            If IsNumeric(txtG1.Text) = True Then
                valuetype = "number"
                value1 = txtG1.Text
                value2 = txtG2.Text
            Else
                value1 = GetValue(txtG1.Text)
                value2 = GetValue(txtG2.Text)
                valuetype = "string"
            End If

            text1 = txtG1.Text
            text2 = txtG2.Text
        Else
            value1 = ddlG1.SelectedValue.ToString
            value2 = ddlG1.SelectedValue.ToString
            valuetype = "string"

            text1 = ddlG1.SelectedItem.Text
            text2 = ddlG2.SelectedItem.Text
        End If

        If valuetype = "string" Then
            If Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS " _
                    & " FROM VW_STUDENT_M AS A INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.STU_ID=B.RST_STU_ID" _
                    & " INNER JOIN VW_SECTION_M AS G ON A.STU_SCT_ID=G.SCT_ID" _
                    & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                    & " INNER JOIN RPT.REPORT_SETUP_M AS D ON C.RSD_RSM_ID=D.RSM_ID" _
                    & " INNER JOIN RPT.REPORT_GRADEMAPPING AS E ON D.RSM_ID=E.RGP_RSM_ID" _
                    & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                    & " INNER JOIN STUDENT_GROUPS_S AS H ON A.STU_ID=H.SSD_STU_ID AND F.SBG_ID=H.SSD_SBG_ID" _
                    & " AND RST_SBG_ID=RGP_SBG_ID AND ISNULL(RST_COMMENTS,RST_GRADING)=RGP_GRADE" _
                    & " WHERE 1=1"
            Else
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,A.SCT_DESCR,SBG_DESCR,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS " _
                & " FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B ON A.STU_ID=B.RST_STU_ID" _
                & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                & " INNER JOIN RPT.REPORT_SETUP_M AS D ON C.RSD_RSM_ID=D.RSM_ID" _
                & " INNER JOIN RPT.REPORT_GRADEMAPPING AS E ON D.RSM_ID=E.RGP_RSM_ID" _
                & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                & " INNER JOIN STUDENT_GROUPS_S AS H ON A.STU_ID=H.SSD_STU_ID AND F.SBG_ID=H.SSD_SBG_ID" _
                & " AND RST_SBG_ID=RGP_SBG_ID AND ISNULL(RST_COMMENTS,RST_GRADING)=RGP_GRADE" _
                & " AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                & " WHERE 1=1"
            End If

            If rdAbove.Checked = True Then
                str += " AND RGP_VALUE>'" + value1 + "'"
                ViewState("criteria") = "Score above " + text1
            ElseIf rdBelow.Checked = True Then
                str += " AND RGP_VALUE<'" + value1 + "'"
                ViewState("criteria") = "Score below " + text1
            Else
                str += " AND RGP_VALUE BETWEEN '" + value1 + "' AND '" + value2 + "'"
                ViewState("criteria") = "Score between " + text1 + " and " + text2
            End If

        Else
            If Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS " _
                      & " FROM VW_STUDENT_M AS A INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.STU_ID=B.RST_STU_ID" _
                      & " INNER JOIN VW_SECTION_M AS G ON A.STU_SCT_ID=G.SCT_ID" _
                      & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                      & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                      & " INNER JOIN STUDENT_GROUPS_S AS H ON A.STU_ID=H.SSD_STU_ID AND F.SBG_ID=H.SSD_SBG_ID"

            Else
                str = "SELECT STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,SBG_DESCR,ISNULL(RST_COMMENTS,RST_GRADING) RST_COMMENTS " _
                    & " FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B ON A.STU_ID=B.RST_STU_ID" _
                    & " INNER JOIN VW_SECTION_M AS G ON A.STU_SCT_ID=G.SCT_ID" _
                    & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_ID=" + rdHeader.SelectedValue.ToString _
                    & " INNER JOIN SUBJECTS_GRADE_S AS F ON B.RST_SBG_ID=F.SBG_ID" _
                    & " INNER JOIN STUDENT_GROUPS_S AS H ON A.STU_ID=H.SSD_STU_ID AND F.SBG_ID=H.SSD_SBG_ID"
            End If
            If rdAbove.Checked = True Then
                str += " AND ISNULL(RST_COMMENTS,RST_GRADING)>'" + value1 + "'"
            ElseIf rdBelow.Checked = True Then
                str += " AND ISNULL(RST_COMMENTS,RST_GRADING)<'" + value1 + "'"
            Else
                str += " AND ISNULL(RST_COMMENTS,RST_GRADING) BETWEEN '" + value1 + "' AND '" + value2 + "'"
            End If

            str += " AND STU_ACd_ID=" + ddlAcademicYear.SelectedValue.ToString
        End If

        Dim grade() As String = ddlGrade.SelectedValue.Split("|")
        str += " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        str += " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_STM_ID=" + grade(1)
        str += " AND RST_RPF_ID=" + ddlReportSchedule.SelectedValue.ToString
        If h_STU_IDs.Value <> "" Then
            str += " AND STU_ID IN(" + h_STU_IDs.Value.Replace("___", ",") + ")"
        End If
        If ddlSection.SelectedValue.ToString <> "0" Then
            str += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        str += " ORDER BY SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME,SBG_DESCR"
        Return str
    End Function

    Function GetValue(ByVal grade As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(MAX(RGP_VALUE),0) FROM RPT.REPORT_GRADEMAPPING WHERE " _
                                 & " RGP_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "' AND RGP_GRADE='" + grade + "'"

        If hfSubjects.Value <> "" Then
            str_query += " AND RGP_SBG_ID IN(" + hfSubjects.Value + ")"
        End If

        Dim value As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return value
    End Function
    Sub CallReport(Optional ByVal strType As String = "report")
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)
        If ViewState("MainMnu_code") = "C400041" Then
            param.Add("@EXEQUERY", GetSubjectReportQuery())
        Else
            param.Add("@EXEQUERY", GetStudentReportQuery())
        End If
        param.Add("criteria", IIf(ViewState("criteria") = Nothing, "", ViewState("criteria")))
        param.Add("reportcard", ddlReportCard.SelectedItem.Text)
        param.Add("schedule", ddlReportSchedule.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            'If ViewState("MainMnu_code") = "C3000190" Then
            '    .reportPath = Server.MapPath("../Rpt/rptStudGroupList.rpt")
            'Else
            If ViewState("MainMnu_code") = "C400041" Or ViewState("MainMnu_code") = "C400380" Then
                If strType = "report" Then
                    .reportPath = Server.MapPath("../Rpt/rptINTLStudentPerformanceBySubject.rpt")
                ElseIf strType = "student" Then
                    .reportPath = Server.MapPath("../Rpt/rptINTLStudentPerformanceBySubjectAcrossGrade.rpt")
                Else
                    param.Add("section", ddlSection.SelectedItem.Text)
                    .reportPath = Server.MapPath("../Rpt/rptINTLStudentPerformanceBySubject_Graph.rpt")
                End If
            Else
                .reportPath = Server.MapPath("../Rpt/rptINTLSubjectPerformanceByStudent.rpt")
            End If
            'End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

#End Region

    Protected Sub rdAbove_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAbove.CheckedChanged
        If ViewState("display") = "textbox" Then
            tbData.Rows(0).Visible = False
            lblA2.Visible = False
            txtG2.Visible = False
        Else
            tbData.Rows(1).Visible = False
            lblA1.Visible = False
            ddlG2.Visible = False
        End If
    End Sub

    Protected Sub rdBelow_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdBelow.CheckedChanged
        If ViewState("display") = "textbox" Then
            tbData.Rows(0).Visible = False
            tbData.Rows(1).Visible = True
            lblA2.Visible = False
            txtG2.Visible = False
        Else
            tbData.Rows(1).Visible = False
            tbData.Rows(0).Visible = True
            lblA1.Visible = False
            ddlG2.Visible = False
        End If
    End Sub

    Protected Sub rdBetween_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdBetween.CheckedChanged
        If ViewState("display") = "textbox" Then
            tbData.Rows(0).Visible = False
            tbData.Rows(1).Visible = True
            lblA2.Visible = True
            txtG2.Visible = True
        Else
            tbData.Rows(1).Visible = False
            tbData.Rows(0).Visible = True
            lblA1.Visible = True
            ddlG2.Visible = True
        End If
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        hfSubjects.Value = getSubjects()
        BindDefaults()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindHeader()
        BindPrintedFor()
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSection()

        If ViewState("MainMnu_code") = "C400041" Or ViewState("MainMnu_code") = "C400380" Then

            trsubject.Visible = True
            trstudent.Visible = False

            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If
        Else
            trsubject.Visible = False
            trstudent.Visible = True
        End If

        'Dim li As New ListItem
        'li.Text = "All"
        'li.Value = "0"
        'ddlSubject.Items.Insert(0, li)
        BindDefaults()
    End Sub

    Protected Sub btnGraph_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGraph.Click
        CallReport("graph")
    End Sub

    'Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
    '    'CheckAll(chkAll.Checked)
    '    hfSubjects.Value = getSubjects()
    '    BindDefaults()
    'End Sub

    Protected Sub btnByStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnByStudent.Click
        CallReport("student")
    End Sub

    'Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
    '    If h_STU_IDs.Value <> "" Then
    '        grdStudent.Visible = True
    '        GridBindStudents(h_STU_IDs.Value)
    '    Else
    '        grdStudent.Visible = False
    '    End If

    'End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
