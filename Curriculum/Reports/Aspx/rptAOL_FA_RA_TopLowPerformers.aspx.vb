Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM

Partial Class rptFormativeConsolidated
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "C290020") AndAlso _
                                     (ViewState("MainMnu_code") <> "C290025") AndAlso (ViewState("MainMnu_code") <> "StudentProfile")) _
                Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    BindReportCard()
                    BindPrintedFor()
                    PopulateActivity()
                    BindGrade()
                    GetSectionForGrade()
                    PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
                    ddlSection.Enabled = chkGradeWise.Checked

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY,GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " INNER JOIN OASIS..STREAM_M AS D ON A.GRM_STM_ID=D.STM_ID " _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            Select Case ViewState("MainMnu_code")
                Case "C290020"
                    TopLowPerformers()
                Case "C290025"
                    TopLowPerformers()
            End Select

        Catch ex As Exception
            lblerror.Text = ex.Message
        End Try

    End Sub

    Private Sub TopLowPerformers()
        Dim param As New Hashtable
        Dim strRpf As String = ""
        Dim strSgr As String = ""
        Dim i As Integer
        For i = 0 To ddlPrintedFor.Items.Count - 1
            If ddlPrintedFor.Items(i).Selected = True Then
                If strRpf <> "" Then
                    strRpf += "|"
                End If
                strRpf += ddlPrintedFor.Items(i).Value.ToString
            End If
        Next

        Dim strActivities As String = String.Empty

        For i = 0 To ddlActivities.Items.Count - 1
            If ddlActivities.Items(i).Selected = True Then
                If strActivities <> "" Then
                    strActivities += "|"
                End If
                strActivities += ddlActivities.Items(i).Value.ToString
            End If
        Next

        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)

        'param.Add("@IMG_BSU_ID", Session("SBSUID"))
        'param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RPF_ID", strRpf)
        param.Add("@COUNT", CInt(txtCount.Text))
        param.Add("@CAD_ID", strActivities)
        Select Case ViewState("MainMnu_code")
            Case "C290020"
                param.Add("@bTOP_PERFORMER", True)
                param.Add("REPORT_CAPTION", "TOP " & txtCount.Text & " ACHIEVERS")
            Case "C290025"
                param.Add("@bTOP_PERFORMER", False)
                param.Add("REPORT_CAPTION", "TOP " & txtCount.Text & " LOW PERFORMERS")
        End Select
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@SBG_ID", ddlSubject.SelectedValue.ToString)
        If chkOnlyFA.Checked Then
            param.Add("@TYPE", "2")
        Else
            param.Add("@TYPE", "1")
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rpt_RA_AOL_TOP_LOW_PERFORMERS_SUBJECTWISE.rpt")
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

    Function getTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT TRM_DESCRIPTION,TRM_STARTDATE,TRM_ENDDATE FROM VW_TRM_M AS A " _
                            & " INNER JOIN RPT.REPORT_RULE_M AS B ON A.TRM_ID=B.RRM_TRM_ID" _
                            & " WHERE RRM_SBG_ID=" + ddlSubject.SelectedValue.ToString _
                            & " AND RRM_RPF_ID=" + ddlPrintedFor.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim strTerm As String = ""

        With ds.Tables(0).Rows(0)
            strTerm = .Item(0) + ": " + Format(.Item(1), "dd MMMM yyyy") + " TO " + Format(.Item(2), "dd MMMM yyyy")
        End With
        Return strTerm.ToUpper
    End Function

    Public Function GetReportType() As String
        Dim strGRD_ID As String = ddlGrade.SelectedValue
        Dim strACD_ID As String = ddlAcademicYear.SelectedValue
        Dim strBSSU_ID As String = Session("sBSUID")
        Dim strRSM_ID As String = ddlReportCard.SelectedValue
        Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
        Dim arrList As ArrayList = New ArrayList(h_STU_IDs.Value.Split("___"))
        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function

    Private Function GETStud_photoPath(ByVal STU_ID As String) As String
        STU_ID = CStr(CInt(STU_ID))
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
        Dim RESULT As String
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            RESULT = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return RESULT

    End Function

    Sub BindReportCard()
        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSM_DESCR LIKE '%FORMATIVE%'" _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        ddlPrintedFor.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " AND RPF_DESCR LIKE '%FORMATIVE%'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Private Sub PopulateActivity()
        ddlActivities.DataSource = GetActivityDetails(ddlAcademicYear.SelectedValue)
        ddlActivities.DataTextField = "CAD_DESC"
        ddlActivities.DataValueField = "CAD_ID"
        ddlActivities.DataBind()
    End Sub

    Public Function GetActivityDetails(ByVal vACD_ID As String) As DataSet
        Dim str_sql As String = String.Empty
        Dim vRPF_ID As String = GetSelectedIDs(ddlPrintedFor)

        If vRPF_ID = "" Then
            Return Nothing
        End If

        str_sql = "SELECT DISTINCT ACT.ACTIVITY_D.CAD_ID, ACT.ACTIVITY_D.CAD_DESC " & _
        " FROM RPT.REPORT_SCHEDULE_D INNER JOIN " & _
        " RPT.REPORT_RULE_M ON RPT.REPORT_SCHEDULE_D.RRD_RRM_ID = RPT.REPORT_RULE_M.RRM_ID " & _
        " INNER JOIN ACT.ACTIVITY_D ON RPT.REPORT_SCHEDULE_D.RRD_CAD_ID = ACT.ACTIVITY_D.CAD_ID " & _
        " WHERE RRM_RPF_ID IN (" & vRPF_ID & ") "

        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)


    End Function

    Public Function GetSelectedIDs(ByVal vList As DropDownList) As String
        Return vList.SelectedValue
        'Dim strLists As String = String.Empty
        'Dim comma As String = String.Empty
        'For Each vListVal As ListItem In vList.Items
        '    If vListVal.Selected Then
        '        strLists += comma + "'" & vListVal.Value & "'"
        '        comma = ","
        '    End If
        'Next
        'Return strLists
    End Function

    Public Function PopulateSubject(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal Grdid As String = "")
        ddl.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""

        Dim str_sql As String = ""

        Dim grade As String() = Grdid.Split("|")

        If Grdid <> "ALL" And Grdid <> "" Then
            strCondition = " AND SBG_GRD_ID='" & grade(0) & "' AND SBG_STM_ID=" + grade(1)
        End If
        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_sql = "Select distinct * from (SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & acdid & "'" _
                          & " " & strCondition & " "
            str_sql += strCondition & " )A ORDER BY A.DESCR1"
        Else

            str_sql = " Select distinct * from (SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & acdid & "'"
            str_sql += strCondition & " )A ORDER BY A.DESCR1"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddl.DataSource = ds
        ddl.DataTextField = "DESCR2"
        ddl.DataValueField = "ID"
        ddl.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddl.Items.Insert(0, li)
        Return ddl
    End Function

    Sub GetSectionForGrade()
        'If ddlGrade.SelectedValue = "ALL" Then
        '    ddlSection.DataSource = Nothing
        '    ddlSection.DataBind()
        '    ddlSection.Items.Add(New ListItem("--", 0))
        '    ddlSection.Items.FindByText("--").Selected = True
        'Else
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If
        Dim grd As String = ddlGrade.SelectedValue.Split("|")(0)
        ddlSection.DataSource = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAcademicYear.SelectedValue, grd, Session("EmployeeID"), bSuperUsr)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
        GetSectionForGrade()
    End Sub

     Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindGrade()
        PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindGrade()
        PopulateSubject(ddlSubject, ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedValue.ToString)
    End Sub

    Protected Sub ddlPrintedFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrintedFor.SelectedIndexChanged
        PopulateActivity()
    End Sub

    Protected Sub chkGradeWise_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGradeWise.CheckedChanged
        ddlSection.Enabled = chkGradeWise.Checked
    End Sub
End Class
