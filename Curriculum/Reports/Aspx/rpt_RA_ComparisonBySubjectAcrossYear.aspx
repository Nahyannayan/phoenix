﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rpt_RA_ComparisonBySubjectAcrossYear.aspx.vb" Inherits="Curriculum_Reports_Aspx_rpt_RA_ComparisonBySubjectAcrossYear"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            <asp:Label ID="Label1" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" 
                    cellpadding="4" cellspacing="0" width="100%" >
                   
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="middle" ><span class="field-label">Grade</span></td>
                        
                        <td align="left" valign="middle" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                            </asp:DropDownList></td>
                    </tr>
                   
                    <tr>
                        <td align="left" width="20%" valign="middle"  ><span class="field-label">Subject</span></td>
                       
                        <td align="left" valign="middle"  >
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="middle"  ><span class="field-label">Report</span></td>
                      
                        <td align="left" valign="middle"  >
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="ddlPrintedFor" runat="server" >
                        </asp:CheckBoxList></div></td>
                    </tr>
                   
                    <tr id="trFilter" runat="server">
                        <td align="left" width="20%" ><span class="field-label">Filter By</span></td>
                       
                        <td align="left"   >
                            <asp:DropDownList ID="ddlType" runat="server" >
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                                <asp:ListItem>EXCLUDING SEN</asp:ListItem>
                            </asp:DropDownList></td>
                        <td colspan="2"> 

                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                Text="Generate Report" ValidationGroup="groupM1" />
                            </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
