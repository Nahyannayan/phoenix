﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubjectAssessmentReport_AverageComparison.aspx.vb" Inherits="Curriculum_Reports_Aspx_clmSubjectAssessmentReport_AverageComparison" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
        
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        function fnSelectAll2(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 37) == 'ctl00_cphMasterpage_cblReportSchedule') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }
    </script>

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 465px">
        <tr>
            <td style="height: 1px">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" Style="text-align: left" Width="342px" />
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" valign="bottom" style="height: 1px">
                &nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="358px" Style="text-align: center" Height="24px"></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" style="font-weight: bold;" valign="top">
                <asp:Panel ID="panel1" runat="server" DefaultButton="btnGetData">
                    <table align="center" border="1" class="BlueTableView" bordercolor="#1b80b6" cellpadding="5"
                        cellspacing="0" style="width: 824px" id="tb1" runat="server">
                        <tr class="subheader_img">
                            <td align="left" colspan="7" valign="middle">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                    Subject Assessment Details</span></font></td>
                        </tr>
                        <tr>
                            <td align="left" class="matters" style="width: 241px">
                                Select Academic Year</td>
                            <td class="matters" style="width: 46px">
                                :</td>
                            <td align="left" class="matters">
                                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" 
                                    style="height: 22px">
                                </asp:DropDownList>&nbsp;</td>
                            <td align="left" class="matters" colspan="1">
                                Select Report Card</td>
                            <td align="left" class="matters" colspan="1" style="width: 12px">
                                :</td>
                            <td align="left" class="matters" colspan="3">
                                <asp:DropDownList ID="ddlReportCard" runat="server" Width="281px" AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" class="matters" style="width: 241px">
                                Select Grade</td>
                            <td class="matters" style="width: 46px">
                                :</td>
                            <td align="left" class="matters" style="width: 347px">
                                <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" Width="117px">
                                </asp:DropDownList></td>
                            <td align="left" class="matters" style="width: 241px">
                                Select Section</td>
                            <td class="matters" style="width: 46px">
                                :</td>
                            <td align="left" class="matters" style="width: 347px">
                                <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" Width="117px">
                                </asp:DropDownList></td>
                            
                        </tr>
                        <tr>
                            <td align="left" class="matters" style="width: 241px">
                                Report Schedule</td>
                            <td class="matters" style="width: 46px">
                                :</td>
                            <td align="left" class="matters" style="width: 347px">
                                
                                 <br />
                            <asp:DropDownList ID="ddlRepSchedule" runat="server" Width="232px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="matters"  style="width: 347px">
                                Select Subject</td>
                            <td align="left" class="matters" >
                                :</td>
                            <td align="left" class="matters" >
                                <asp:DropDownList ID="ddlSubject" runat="server" Width="232px" AutoPostBack="True">
                                </asp:DropDownList></td>
                            
                        </tr>
                        <tr>
                        <td align="center" class="matters" colspan="6">
                                <asp:Button ID="btnGetData" runat="server" CssClass="button" TabIndex="7" Text="Generate Report"
                                    ValidationGroup="groupM1" Width="109px" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnGenGraph" runat="server" CssClass="button" TabIndex="7" Text="Generate Graph"
                                ValidationGroup="groupM1" Width="109px" /> &nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnDownReport" runat="server" CssClass="button" TabIndex="7" Text="Download Report"
                                ValidationGroup="groupM1" Width="109px" /> &nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnDownGraoh" runat="server" CssClass="button" TabIndex="7" Text="Download Graph"
                                ValidationGroup="groupM1" Width="109px" />
                                    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        </table>
        <asp:HiddenField ID="hfgetData" runat="server" />
        <asp:HiddenField ID="hfDownloadReport" runat="server" />
        <asp:HiddenField ID="hfDownloadGraph" runat="server" />
</asp:Content>
