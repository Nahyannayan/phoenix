<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmGradeSlabDetailsView.aspx.vb" Inherits="clmGradeSlabMasterView"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Grade Slab Details
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" border="0" cellpadding="0" cellspacing="0"
                    width="100%">
                    <tr valign="top">
                        <td align="right" colspan="2" style="text-align: left">&nbsp;<asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><input id="h_selected_menu_1"
                                runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table id="tbl_test" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                   
                    <tr>
                        <td align="center" valign="top" class="matters" colspan="9">
                            <asp:GridView ID="gvGradeSlab" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" CssClass="table table-bordered table-row"
                                Width="100%" AllowPaging="True" PageSize="30" OnRowCreated="gvGradeSlab_RowCreated">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="SLAB_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGSM_SLAB_ID" runat="server" Text='<%# Bind("GSM_SLAB_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderTemplate>
                                            Grade Slab Description &nbsp;
                                                                <asp:TextBox ID="txtCounterDescr" runat="server" style="min-width:60% !important"></asp:TextBox>

                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("GSM_DESC") %>' Visible="False"></asp:Label>
                                            <asp:LinkButton ID="lnkAmount" runat="server" Text='<%# Bind("GSM_DESC") %>' CausesValidation="False"
                                                OnClientClick="return false;"></asp:LinkButton><ajaxToolkit:PopupControlExtender
                                                    ID="PopupControlExtender1" runat="server" DynamicContextKey='<%# Eval("GSM_SLAB_ID") %>'
                                                    DynamicControlID="Panel1" DynamicServiceMethod="GetDynamicContent" PopupControlID="Panel1"
                                                    Position="Bottom" TargetControlID="lnkAmount">
                                                </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total_Mark">
                                        <HeaderTemplate>
                                           Total Marks&nbsp;
                                                            
                                                            <asp:TextBox ID="txtUser" runat="server" ></asp:TextBox>
                                                            <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                                ImageAlign="Middle"></asp:ImageButton>

                                                       
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("GSM_TOT_MARK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

           

                <asp:Panel ID="Panel1" runat="server" CssClass="panel-cover">
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>
