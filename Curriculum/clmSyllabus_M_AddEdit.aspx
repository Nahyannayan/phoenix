<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSyllabus_M_AddEdit.aspx.vb" Inherits="Curriculum_clmSyllabus_M_AddEdit" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Syllabus Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%" >
                    <tr id="Tr1" runat="server">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table  width="100%">
                    <tr>
                        <td align="left" width="20%" ><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True"  OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" ><span class="field-label">Term</span></td>
                        <td align="left" width="30%"><asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" class="title-bg">
                            <asp:Label ID="Label1" runat="server" Text="Details..">
                            </asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Grade</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" ><span class="field-label">Subject</span></td>
                        <td align="left" >
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Syllabus Name</span><asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                        <td align="left">
                            <asp:TextBox ID="txtSyllabus" runat="server"  TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSyllabus"
                                Display="Dynamic" ErrorMessage="Enter Syllabus Name" ForeColor="White" Height="11px"
                                ValidationGroup="SUBERROR"></asp:RequiredFieldValidator></td>
                         <td align="left" ></td>
                          <td align="left" ></td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4" >
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" OnClick="btnDetAdd_Click" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <br />
                <table Width="100%" >
                    <tr id="tr_Deatails" runat="server">
                        <td align="center"  colspan="4" valign="top" >
                            <asp:GridView ID="gvSyllabus" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" EmptyDataText="No Data Found"  Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="GUID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="ACDID">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblACDID" runat="server" Text='<%# Bind("AcdId") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AcdYear">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAcdYear" runat="server" Text='<%# bind("AcdYear") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TrmID">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTrmID" runat="server" Text='<%# bind("TrmId") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Term">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTerm" runat="server" Text='<%# bind("Term") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GrdID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrdID" runat="server" Text='<%# Bind("GrdId") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("Grade") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SubjID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubjId" runat="server" Text='<%# bind("SubjId") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Subject">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubject" runat="server" Text='<%# bind("Subject") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Syllabus">
                                        <ItemStyle></ItemStyle>

                                        <HeaderStyle></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="T5lblview" runat="server" Text='<%# Eval("tempview") %>' Width="100%" ></asp:Label>
                                            <asp:Panel ID="T5Panel1" runat="server" Width="100%" >
                                                <%#Eval("Syllabus")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server" AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true" CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T5lblview" ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T5Panel1" TextLabelID="T5lblview"></ajaxToolkit:CollapsiblePanelExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SylId">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSylId" runat="server" Text='<%# Bind("SylId") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Topics">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkView" OnClick="lnkView_Click" runat="server" >SetTopics</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server"  OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" OnClick="lnkDelete_Click" runat="server" >Delete</asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="lnkDelete" ConfirmText="Do you want to delete this Syllabus?"></ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td  colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button"
                                Text="Add" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" OnClick="btnDelete_Click" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="MAINERROR" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_SYL_ID" runat="server" />
                <asp:HiddenField ID="h_GRD_ID" runat="server" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Do you want to delete this Syllabus?"
                    TargetControlID="btnDelete">
                </ajaxToolkit:ConfirmButtonExtender>
                &nbsp; &nbsp;
    &nbsp; &nbsp;&nbsp;
            </div>
        </div>
    </div>
</asp:Content>

