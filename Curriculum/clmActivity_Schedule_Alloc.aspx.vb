Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports CURRICULUM
Partial Class Students_studGrade_Att
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "edit"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_ACTIVITY_SCHEDULE And ViewState("MainMnu_code") <> "C312015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'ViewState("menu_rights") = menu_rights

                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Stu_ACT_LIST()
                    gridbind()
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                    btnCancel.Text = "Back"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub
    
    Sub Stu_ACT_LIST()

        Using readerACT_SCHEDULE As SqlDataReader = ACTIVITYMASTER.GetACT_SCHEDULE(ViewState("viewid"))
            While readerACT_SCHEDULE.Read


                ViewState("ACD_ID") = Convert.ToString(readerACT_SCHEDULE("ACD_ID"))
                ViewState("CAS_SGR_ID") = Convert.ToString(readerACT_SCHEDULE("CAS_SGR_ID"))
                ViewState("CAS_SBG_ID") = Convert.ToString(readerACT_SCHEDULE("CAS_SBG_ID"))
                ltAcd_Year.Text = Convert.ToString(readerACT_SCHEDULE("ACY_DESC"))
                ltActivity.Text = Convert.ToString(readerACT_SCHEDULE("CAD_DESC"))
                ltGrade.Text = Convert.ToString(readerACT_SCHEDULE("GRD_DESC"))
                ltGroup.Text = Convert.ToString(readerACT_SCHEDULE("SGR_DESC"))
                ltTerm.Text = Convert.ToString(readerACT_SCHEDULE("TRM_DESC"))
                ltLevel.Text = Convert.ToString(readerACT_SCHEDULE("TYPE_LEVEL"))
                ltSub.Text = Convert.ToString(readerACT_SCHEDULE("SBG_DESC"))
            End While
        End Using
    End Sub


    
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            Dim CAS_ID As String = ViewState("viewid")
            Dim ds As New DataSet

            str_Sql = "  SELECT DISTINCT * FROM ("

            str_Sql += "SELECT     STUDENT_GROUPS_S.SSD_STU_ID AS STU_ID, VW_STUDENT_M.STU_NO, ISNULL(VW_STUDENT_M.STU_FIRSTNAME, '') " & _
                     " + ' ' + ISNULL(VW_STUDENT_M.STU_MIDNAME, '') + ' ' + ISNULL(VW_STUDENT_M.STU_LASTNAME, '') AS STUNAME, " & _
                      " ACT.ACTIVITY_SCHEDULE.CAS_ID, ACT.STUDENT_ACTIVITY.STA_CAS_ID, CASE WHEN ACT.STUDENT_ACTIVITY.STA_STU_ID IS NULL " & _
                      " THEN '0' ELSE '1' END AS AVAIL,VW_GRADE_BSU_M.GRM_DISPLAY +' & ' +  vw_SECTION_M.SCT_DESCR  as GRD_SEC" & _
                      " FROM STUDENT_GROUPS_S LEFT OUTER JOIN  ACT.ACTIVITY_SCHEDULE ON STUDENT_GROUPS_S.SSD_SGR_ID = ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID AND " & _
                      " STUDENT_GROUPS_S.SSD_SBG_ID = ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID INNER JOIN " & _
                      " VW_STUDENT_M ON STUDENT_GROUPS_S.SSD_STU_ID = VW_STUDENT_M.STU_ID AND " & _
            " STUDENT_GROUPS_S.SSD_ACD_ID = VW_STUDENT_M.STU_ACD_ID And " & _
                      " STUDENT_GROUPS_S.SSD_GRD_ID = VW_STUDENT_M.STU_GRD_ID INNER JOIN " & _
                      " VW_GRADE_BSU_M ON VW_STUDENT_M.STU_GRM_ID = VW_GRADE_BSU_M.GRM_ID AND " & _
                      " VW_STUDENT_M.STU_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID LEFT OUTER JOIN " & _
                      " vw_SECTION_M ON VW_STUDENT_M.STU_SCT_ID = vw_SECTION_M.SCT_ID AND " & _
                      " VW_STUDENT_M.STU_ACD_ID = vw_SECTION_M.SCT_ACD_ID LEFT OUTER Join " & _
                      " ACT.STUDENT_ACTIVITY ON ACT.ACTIVITY_SCHEDULE.CAS_ID = ACT.STUDENT_ACTIVITY.STA_CAS_ID AND " & _
            " STUDENT_GROUPS_S.SSD_STU_ID = ACT.STUDENT_ACTIVITY.STA_STU_ID And " & _
            " STUDENT_GROUPS_S.SSD_SBG_ID = ACT.STUDENT_ACTIVITY.STA_SBG_ID And " & _
            " STUDENT_GROUPS_S.SSD_SGR_ID = ACT.STUDENT_ACTIVITY.STA_SGR_ID And " & _
            " STUDENT_GROUPS_S.SSD_ACD_ID = ACT.STUDENT_ACTIVITY.STA_ACD_ID " & _
            " WHERE (ISNULL(VW_STUDENT_M.STU_LEAVEDATE, getdate())>= getdate()) " & _
            " AND (VW_STUDENT_M.STU_CURRSTATUS <> 'CN') AND " & _
            " (ACT.ACTIVITY_SCHEDULE.CAS_ID ='" & CAS_ID & "')"


            str_Sql += " UNION ALL "

            str_Sql += "SELECT     STU_ID, VW_STUDENT_M.STU_NO, ISNULL(VW_STUDENT_M.STU_FIRSTNAME, '') " & _
                     " + ' ' + ISNULL(VW_STUDENT_M.STU_MIDNAME, '') + ' ' + ISNULL(VW_STUDENT_M.STU_LASTNAME, '') AS STUNAME, " & _
                      " ACT.ACTIVITY_SCHEDULE.CAS_ID, ACT.STUDENT_ACTIVITY.STA_CAS_ID, CASE WHEN ACT.STUDENT_ACTIVITY.STA_STU_ID IS NULL " & _
                      " THEN '0' ELSE '1' END AS AVAIL,VW_GRADE_BSU_M.GRM_DISPLAY +' & ' +  vw_SECTION_M.SCT_DESCR  as GRD_SEC" & _
                      " FROM  ACT.ACTIVITY_SCHEDULE " & _
                      " INNER JOIN ACT.STUDENT_ACTIVITY ON ACT.ACTIVITY_SCHEDULE.CAS_ID = ACT.STUDENT_ACTIVITY.STA_CAS_ID " & _
                      " INNER JOIN VW_STUDENT_M ON STA_STU_ID = VW_STUDENT_M.STU_ID INNER JOIN " & _
                      " VW_GRADE_BSU_M ON VW_STUDENT_M.STU_GRM_ID = VW_GRADE_BSU_M.GRM_ID AND " & _
                      " VW_STUDENT_M.STU_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID INNER JOIN " & _
                      " vw_SECTION_M ON VW_STUDENT_M.STU_SCT_ID = vw_SECTION_M.SCT_ID AND " & _
                      " VW_STUDENT_M.STU_ACD_ID = vw_SECTION_M.SCT_ACD_ID   " & _
                      " WHERE (ISNULL(VW_STUDENT_M.STU_LEAVEDATE, getdate())>= getdate()) " & _
                      " AND (VW_STUDENT_M.STU_CURRSTATUS <> 'CN') AND " & _
                      " (ACT.ACTIVITY_SCHEDULE.CAS_ID ='" & CAS_ID & "')"




            str_Sql += ")A "




            '" SELECT DISTINCT * FROM (SELECT     STUDENT_GROUPS_S.SSD_STU_ID AS STU_ID, VW_STUDENT_M.STU_NO, ISNULL(VW_STUDENT_M.STU_FIRSTNAME, '') " & _
            '                     "  + ' ' + ISNULL(VW_STUDENT_M.STU_MIDNAME, '') + ' ' + ISNULL(VW_STUDENT_M.STU_LASTNAME, '') AS STUNAME, " & _
            '                      " ACT.ACTIVITY_SCHEDULE.CAS_ID, ACT.STUDENT_ACTIVITY.STA_CAS_ID,CASE WHEN ACT.STUDENT_ACTIVITY.STA_STU_ID IS NULL THEN '0' ELSE '1' END AS AVAIL " & _
            '                     " FROM  STUDENT_GROUPS_S INNER JOIN ACT.ACTIVITY_SCHEDULE ON STUDENT_GROUPS_S.SSD_SGR_ID = ACT.ACTIVITY_SCHEDULE.CAS_SGR_ID AND " & _
            '                     " STUDENT_GROUPS_S.SSD_SBG_ID = ACT.ACTIVITY_SCHEDULE.CAS_SBG_ID INNER JOIN VW_STUDENT_M ON STUDENT_GROUPS_S.SSD_STU_ID = VW_STUDENT_M.STU_ID AND " & _
            '                      " STUDENT_GROUPS_S.SSD_ACD_ID = VW_STUDENT_M.STU_ACD_ID AND STUDENT_GROUPS_S.SSD_GRD_ID = VW_STUDENT_M.STU_GRD_ID LEFT OUTER JOIN " & _
            '                      " ACT.STUDENT_ACTIVITY ON ACT.ACTIVITY_SCHEDULE.CAS_ID = ACT.STUDENT_ACTIVITY.STA_CAS_ID AND ACT.STUDENT_ACTIVITY ON STUDENT_GROUPS_S.SSD_STU_ID = ACT.STUDENT_ACTIVITY.STA_STU_ID AND STUDENT_GROUPS_S.SSD_SBG_ID = ACT.STUDENT_ACTIVITY.STA_SBG_ID AND " & _
            '                      " STUDENT_GROUPS_S.SSD_SGR_ID = ACT.STUDENT_ACTIVITY.STA_SGR_ID AND " & _
            '                       " STUDENT_GROUPS_S.SSD_ACD_ID = ACT.STUDENT_ACTIVITY.STA_ACD_ID WHERE  (CONVERT(DATETIME, VW_STUDENT_M.STU_LEAVEDATE) IS NULL) AND (VW_STUDENT_M.STU_CURRSTATUS <> 'CN') AND " & _
            '                      " (ACT.ACTIVITY_SCHEDULE.CAS_ID = '" & CAS_ID & "'))A "


            Dim txtSearch As New TextBox


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & " ORDER BY A.STUNAME")

            If ds.Tables(0).Rows.Count > 0 Then

                gvAlloc.DataSource = ds.Tables(0)
                gvAlloc.DataBind()
                btnCancel.Visible = True
                btnSave.Visible = True
            Else
                btnCancel.Visible = True
                btnSave.Visible = False
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvAlloc.DataSource = ds.Tables(0)
                Try
                    gvAlloc.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAlloc.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAlloc.Rows(0).Cells.Clear()
                gvAlloc.Rows(0).Cells.Add(New TableCell)
                gvAlloc.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAlloc.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAlloc.Rows(0).Cells(0).Text = "No record available!!!"
            End If




        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
            'Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim STU_NO As String = String.Empty
        str_err = calltransaction(errorMessage, STU_NO)
        If str_err = "0" Then
            STU_NO = STU_NO.TrimEnd(",").TrimStart(",")
            If Trim(STU_NO) <> "" Then
                STU_NO = " except those Student(s)  with following Fee Id " & STU_NO & " due to the already assigned activity for a given date and time"
            Else
                STU_NO = "Record Saved Successfully"
            End If
            lblError.Text = STU_NO
            Session.Remove("hashCheck")

        Else
            lblError.Text = errorMessage
        End If
        btnCancel.Visible = True
    End Sub

    

    Function calltransaction(ByRef errorMessage As String, ByRef STU_NO As String) As Integer

        Dim STU_IDs As New StringBuilder



        Dim chk As CheckBox
        Dim STU_ID As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If
        For Each rowItem As GridViewRow In gvAlloc.Rows

            chk = DirectCast(rowItem.FindControl("chkList"), CheckBox)

            STU_ID = DirectCast(rowItem.FindControl("lblSTU_ID"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(STU_ID) = False Then
                    hash.Add(STU_ID, DirectCast(rowItem.FindControl("lblSTU_Id"), Label).Text)
                End If
            Else
                If hash.Contains(STU_ID) = True Then
                    hash.Remove(STU_ID)
                End If
            End If



        Next

        Session("hashCheck") = hash




        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                STU_IDs.Append(hashloop.Value)
                STU_IDs.Append("|")

            Next


        End If
        'For i As Integer = 0 To gvAlloc.Rows.Count - 1

        '    Dim row As GridViewRow = gvAlloc.Rows(i)

        '    Dim ckList As Boolean = DirectCast(row.FindControl("chkList"), CheckBox).Checked

        '    If ckList Then
        '        EQS_EQM_ENQIDs.Append(DirectCast(row.FindControl("lblENQID"), Label).Text)
        '        EQS_EQM_ENQIDs.Append("|")
        '    End If
        'Next

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer


                status = ACTIVITYMASTER.SaveSTUDENT_ACTIVITY(STU_IDs.ToString, ViewState("ACD_ID"), ViewState("CAS_SGR_ID"), ViewState("CAS_SBG_ID"), ViewState("viewid"), STU_NO, transaction)
                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                calltransaction = "0"
                gvAlloc.Enabled = False
            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False

        'ScriptManager1.EnablePartialRendering = False
    End Sub


    
    'Protected Sub gvAlloc_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim hash As New Hashtable
    '    If Not Session("hashCheck") Is Nothing Then
    '        hash = Session("hashCheck")
    '    End If


    '    Dim chk As CheckBox
    '    Dim ENQID As String = String.Empty
    '    For Each rowItem As GridViewRow In gvAlloc.Rows
    '        chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)

    '        ENQID = gvAlloc.DataKeys(rowItem.RowIndex)("ENQID").ToString()
    '        If hash.Contains(ENQID) = True Then
    '            chk.Checked = True

    '        Else

    '            chk.Checked = False
    '        End If




    '    Next

    'End Sub
    'Protected Sub gvAlloc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAlloc.PageIndexChanging
    '    gvAlloc.PageIndex = e.NewPageIndex



    '    Dim hash As New Hashtable
    '    If Not Session("hashCheck") Is Nothing Then
    '        hash = Session("hashCheck")
    '    End If


    '    Dim chk As CheckBox
    '    Dim ENQID As String = String.Empty
    '    For Each rowItem As GridViewRow In gvAlloc.Rows
    '        chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)

    '        ENQID = gvAlloc.DataKeys(rowItem.RowIndex)("ENQID").ToString()
    '        If chk.Checked = True Then
    '            If hash.Contains(ENQID) = False Then
    '                hash.Add(ENQID, ENQID)
    '            End If
    '        Else
    '            If hash.Contains(ENQID) = True Then
    '                hash.Remove(ENQID)
    '            End If
    '        End If



    '    Next

    '    Session("hashCheck") = hash
    '    gridbind()
    'End Sub
 

   

    Protected Sub gvAlloc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lbl As Label = DirectCast(e.Row.FindControl("lblAvail"), Label)
            Dim chk As CheckBox = DirectCast(e.Row.FindControl("chkList"), CheckBox)
            If lbl.Text.Trim = "1" Then
                chk.Checked = True
            ElseIf lbl.Text.Trim = "1" Then
                chk.Checked = False
            End If
        End If
    End Sub

    
End Class
