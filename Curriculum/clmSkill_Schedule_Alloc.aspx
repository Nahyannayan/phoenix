<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSkill_Schedule_Alloc.aspx.vb" Inherits="Students_studGrade_Att" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
   
   <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <script language="javascript" type="text/javascript">
           function change_chk_state(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkList")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }
               
          
    </script>
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Allocate Student
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
    

    
    <table id="tbl_AddGroup" runat="server" align="center" width="100%">
        <tr>
            <td align="left">
               
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
                
            </td>
        </tr>
        <tr >
            <td align="center" >
                <table align="center" width="100%">
                    <tr >
                        <td align="left" >
                            <asp:Literal id="ltLabel" runat="server" Text=""></asp:Literal></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label"> Academic Year</span></td>
                       
                        <td align="left" width="30%">
                            <asp:Label id="ltAcd_Year" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left" width="20%">
                           <span class="field-label"> Term</span></td>
                       
                        <td align="left" width="30%">
                            <asp:Label id="ltTerm" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" >
                          <span class="field-label">  Grade</span></td>
                      
                        <td align="left" >
                            <asp:Label id="ltGrade" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left" >
                           <span class="field-label"> Subject</span></td>
                       
                        <td align="left" >
                            <asp:Label id="ltSub" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" >
                          <span class="field-label">  Assessment</span></td>
                       
                        <td align="left" >
                            <asp:Label id="ltActivity" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left"  >
                           <span class="field-label"> Group</span></td>
                       
                        <td align="left">
                            <asp:Label id="ltGroup" runat="server" CssClass="field-value"></asp:Label></td>
                    
                    </tr>
                    
                    <tr>
                        <td align="left" >
                          <span class="field-label">Level</span></td>
                   
                        <td align="left">
                            <asp:Label id="ltLevel" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
        </tr>
        <tr>
            <td colspan="4">
        
                <asp:GridView id="gvAlloc" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    PageSize="40" Width="100%" DataKeyNames="STU_ID" OnRowDataBound="gvAlloc_RowDataBound">
                    <rowstyle cssclass="griditem"  />
                    <columns>
<asp:TemplateField HeaderText="Select"><HeaderTemplate>
&nbsp;<asp:CheckBox id="chkAll" onclick="javascript:change_chk_state(this);" runat="server"></asp:CheckBox>&nbsp;
</HeaderTemplate>
<ItemTemplate>
&nbsp;<asp:CheckBox id="chkList" runat="server"></asp:CheckBox> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Stu.ID" Visible="False"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblstu_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label> 
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student No"><ItemTemplate>
<asp:Label id="lblStu_No" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblSTUNAME" runat="server" Text='<%# Bind("STUNAME") %>'></asp:Label> 
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" ></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="GRD_SEC" HeaderText="Grade &amp; Section">
<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="AVAIL" Visible="False"><ItemTemplate>
<asp:Label id="lblAvail" runat="server" Text='<%# Bind("AVAIL") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
</columns>
                    <selectedrowstyle  />
                    <headerstyle  horizontalalign="Center" verticalalign="Middle" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView>
              
            </td>
        </tr>
        <tr>
             <td colspan="4">
                &nbsp;
                                        </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                    Text="Cancel" /></td>
        </tr>
        <tr>
            <td colspan="4">
                 <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />&nbsp;
                
                <asp:HiddenField ID="hiddenENQIDs" runat="server" /> 


                </td>
        </tr>
    </table>
     
   
            </div>
        </div>
    </div>


</asp:Content>

