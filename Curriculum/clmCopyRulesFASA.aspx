﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmCopyRulesFASA.aspx.vb" Inherits="Curriculum_clmCopyRulesFASA" Title="Untitled Page" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <style>
        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}

.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Copy Rules FA To Term
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" width="100%">

                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"  >
                            <table width="100%">
                                <tr>
                                    <td style="vertical-align:top !important" width="50%">
                                        <table width="100%">
                                            <tr class="gridheader_pop">
                                                <td align="left" colspan="2" valign="middle" class="title-bg">Copy From 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" width="40%">
                                                    <asp:Label ID="lblStu" runat="server" CssClass="field-label" Text="Academic Year"></asp:Label>
                                                </td>

                                                <td align="left" class="matters" width="60%">
                                                    <asp:DropDownList ID="ddlAcademicYearFrom" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters"><span class="field-label">Term</span>
                                                </td>

                                                <td align="left" class="matters">
                                                    <asp:DropDownList ID="ddlTermFrom" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters">
                                                    <asp:Label ID="Label1" CssClass="field-label" runat="server" Text="Report Card"></asp:Label>
                                                </td>

                                                <td align="left" class="matters">
                                                    <asp:DropDownList ID="ddlReportCardFrom" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr><td colspan="2">&nbsp;</td></tr>
                                        </table>
                                    </td>
                                    <td width="50%">
                                        <table width="100%">
                                            <tr class="gridheader_pop">
                                                <td align="left" colspan="2" valign="middle" class="title-bg">Copy To
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" width="40%">
                                                    <asp:Label ID="Label4" runat="server" CssClass="field-label" Text="Academic Year"></asp:Label>
                                                </td>
                                                <td align="left" class="matters" width="60%">
                                                    <asp:DropDownList ID="ddlAcademicYearTo" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters"><span class="field-label">Term</span>
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:DropDownList ID="ddlTermTo" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters">
                                                    <asp:Label ID="Label5" runat="server" CssClass="field-label" Text="Report Card"></asp:Label>
                                                </td>

                                                <td align="left" class="matters">
                                                    <asp:DropDownList ID="ddlReportCardTo" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters">
                                                    <asp:Label ID="Label6" runat="server" CssClass="field-label" Text="Report Schedule"></asp:Label>
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:DropDownList ID="ddlReportScheduleTo" AutoPostBack="true" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" class="matters" width="40%">
                                                    <asp:Label ID="Label3" runat="server" CssClass="field-label" Text="Grade"></asp:Label>
                                                </td>
                                                <td align="left" class="matters" width="60%">
                                                                <telerik:RadComboBox RenderMode="Lightweight" ID="lstGrade" Width="100%" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"></telerik:RadComboBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:GridView ID="gvHeader" runat="server" EmptyDataText="No Records" Width="100%" CssClass="table table-bordered table-row"
                                            AutoGenerateColumns="false" PageSize="2">
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="sbgId" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRpfId" runat="server" Text='<%# Bind("RPF_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Report Schedule">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRpf" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Map to">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlHeader" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green"></SelectedRowStyle>
                                            <HeaderStyle CssClass="gridheader_pop" Height="30px"></HeaderStyle>
                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters">
                                        <table width="100%">
                                            <tr>
                                                <td class="matters" width="40%"><span class="field-label">Rule Description</span>
                                                </td>
                                                <td width="60%">
                                                    <table>
                                                        <tr>
                                                            <td class="matters"><span class="field-label">Replace text</span>
                                                            </td>
                                                            <td class="matters">
                                                                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="matters"><span class="field-label">With</span>
                                                            </td>
                                                            <td class="matters">
                                                                <asp:TextBox ID="txtTo" runat="server" CausesValidation="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" >
                                        <asp:Button ID="btnCopy" runat="server" CssClass="button" Text="Copy Rules" ValidationGroup="groupM1"
                                            TabIndex="7" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

