﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_DashBoards_clmDBSENDocUploads
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblName.Text = Request.QueryString("name")
        lblStudentID.Text = Request.QueryString("studentid")
        lblGrade.Text = Request.QueryString("class")
        lblHdr.Text = Request.QueryString("type") + " Documents"
        BindPhoto()
        BindData()
    End Sub

    Sub BindPhoto()
        If Request.QueryString("photopath") <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + Request.QueryString("photopath")
            imgStudent.ImageUrl = strImagePath
        Else
            imgStudent.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub

    Sub BindData()
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim str_Sql As String = ""

            Dim ds As DataSet
            str_Sql = "select doc_id,doc_name from SEN_ACE_STUD_DOCUMENT where doc_stu_id=" + Encr_decrData.Decrypt(Request.QueryString("stu_id").Replace(" ", "+")) _
                     & " and doc_category='" + Request.QueryString("type") + "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            dlDocs.DataSource = ds.Tables(0)
            dlDocs.DataBind()
        Catch ex As Exception
        End Try
    End Sub



    Protected Sub dlDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlDocs.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim txtfile As LinkButton = e.Item.FindControl("txtfile")
            Dim img As ImageButton = e.Item.FindControl("imgF")
            Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
            If s = ".xls" Or s = ".xlsx" Then
                img.ImageUrl = "~/images/Curriculum/Excel-icon.png"
            ElseIf s = ".doc" Then
                img.ImageUrl = "~/images/Curriculum/word-icon.png"
            ElseIf s = ".pdf" Then
                img.ImageUrl = "~/images/Curriculum/pdf-iconlarge.png"
            Else
                img.ImageUrl = ""
            End If
        End If
    End Sub

    Protected Sub imgF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select doc_file,doc_name,doc_type from SEN_ACE_STUD_DOCUMENT where doc_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If
    End Sub
    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select doc_file,doc_name,doc_type from SEN_ACE_STUD_DOCUMENT where doc_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub

    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("doc_file"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("doc_name").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)

            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("doc_type").ToString()
            Response.AddHeader("content-disposition", "attachment:filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
End Class
