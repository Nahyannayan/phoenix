﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmDBStudProfile.aspx.vb"
    Inherits="Curriculum_DashBoards_clmDBStudProfile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../../cssfiles/curriculum.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
        <script type="text/javascript" src="FusionCharts/FusionCharts.js"></script>
<script type="text/javascript" src="FusionCharts/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <fieldset style="height: 415px;">
                <legend style="color: black; font-weight: bold; font-family: verdana; font-size: 8pt">
                    <asp:Label ID="lblHdr" runat="server"></asp:Label></legend>
                <table border="1" cellpadding="0" cellspacing="0" class="tableChart" width="100%">
                    <tr>
                        <td width="120px" class="tdChart">
                            <asp:Image ID="imgStudent" runat="server" Height="132px" Width="120px" />
                        </td>
                        <td class="tdChart" valign="top">
                            <table cellpadding="5" cellspacing="5" border="0" class="tableNoborder">
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" class="chartFields">
                                        Student ID
                                    </td>
                                    <td class="chartFields">
                                        :
                                    </td>
                                    <td class="chartFields">
                                        <asp:Label align="left" ID="lblStudentID" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="chartFields">
                                        Student Name
                                    </td>
                                    <td class="chartFields">
                                        :
                                    </td>
                                    <td class="chartFields">
                                        <asp:Label align="left" ID="lblName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="chartFields">
                                        Grade & Section
                                    </td>
                                    <td class="chartFields">
                                        :
                                    </td>
                                    <td class="chartFields">
                                        <asp:Label align="left" ID="lblGrade" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" class="tdChart">
                            <table>
                                <tr>
                                    <td class="chartFields">
                                        Attendance As on Date</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="chartFields" colspan="6" rowspan="4" valign="top">
                            <table id="table1" runat="server" class="tableChart" border="1" width="100%" style="border: solid 1px #B7B7B7;">
                                <tr>
                                    <td align="left" colspan="6" class="chartFields">
                                        Contact Details</td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                    </td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                    </td>
                                    <td class="chartFields">
                                        Father</td>
                                    <td class="chartFields">
                                        Mother</td>
                                    <td class="chartFields">
                                        Guardian</td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Name</td>
                                    <td align="left" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields">
                                        <asp:Literal ID="Ltl_Fname" runat="server"></asp:Literal></td>
                                    <td class="chartFields">
                                        <asp:Literal ID="Ltl_Mname" runat="server"></asp:Literal></td>
                                    <td class="chartFields">
                                        <asp:Literal ID="Ltl_Gname" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Nationality</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_Fnationality" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_Mnationality" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_Gnationality" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Comm.POB</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_Fpob" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_Mpob" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_Gpob" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Emirate</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_FEmirate" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_MEmirate" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_GEmirate" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Phone Res</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_FPhoneRes" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_MPhoneRes" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_GPhoneRes" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Office Phone</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_FOfficePhone" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_MOfficePhone" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_GOfficePhone" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Mobile</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_FMobile" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_MMobile" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_GMobile" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Email</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_FEmail" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_MEmail" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_GEmail" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Fax</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_FFax" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_MFax" runat="server"></asp:Literal></td>
                                    <td class="chartFields" style="width: 100px">
                                        <asp:Literal ID="Ltl_GFax" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields" style="width: 100px">
                                        Occupation</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields">
                                        <asp:Literal ID="Ltl_FOccupation" runat="server"></asp:Literal></td>
                                    <td class="chartFields">
                                        <asp:Literal ID="Ltl_MOccupation" runat="server"></asp:Literal></td>
                                    <td class="chartFields">
                                        <asp:Literal ID="Ltl_GOccupation" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td class="chartFields">
                                        Company</td>
                                    <td align="center" class="chartFields" style="width: 1px">
                                        :</td>
                                    <td class="chartFields" style="width: 200px">
                                        <asp:Literal ID="Ltl_FCompany" runat="server"></asp:Literal></td>
                                    <td class="chartFields">
                                        <asp:Literal ID="Ltl_MCompany" runat="server"></asp:Literal></td>
                                    <td class="chartFields">
                                        <asp:Literal ID="Ltl_GCompany" runat="server"></asp:Literal></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <asp:HiddenField ID="HF_stuid" runat="server" />
        </div>
    </form>
</body>
</html>
