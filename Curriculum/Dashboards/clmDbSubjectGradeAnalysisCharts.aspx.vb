﻿Imports InfosoftGlobal
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Curriculum_DashBoards_clmDbSubjectGradeAnalysisCharts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblHdr.Text = "Grade " + Request.QueryString("grd_id") + " " + Request.QueryString("subject")
        getRpf_Id()
        PoplateCharts()
    End Sub

    Sub getRpf_Id()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec DHB.getRPFID_DMHS " _
                               & Request.QueryString("hdr_id") + "," _
                               & Request.QueryString("acd_id") + "," _
                               & "'" + Request.QueryString("grd_id") + "'"
        hfRPF_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub PoplateCharts()

        Dim sChart As New StringBuilder

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec [DHB].[STUDENTSUBJECTGRADEANALYSIS_DMHS] " _
                                 & hfRPF_ID.Value + "," _
                                 & Request.QueryString("acd_id") + "," _
                                 & "'" + Request.QueryString("grd_id") + "'," _
                                 & Request.QueryString("sbg_id") + "," _
                                 & "0,''"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGrade.DataSource = ds
        gvGrade.DataBind()

        Dim i As Integer
        Dim columns As Integer = ds.Tables(0).Columns.Count
        Dim totalStudents As Double = CDbl(ds.Tables(0).Rows(0).Item(columns - 1))
        If totalStudents = 0 Then totalStudents = 1

        sChart.AppendLine("<chart chartLeftMargin='0'   baseFontSize='13' captionPadding='0'  chartRightMargin='0' chartTopMargin='0' chartBottomMargin='10'  palette='3' showLegend='1'  showPercentValues='1'>")
        For i = 0 To ds.Tables(0).Columns.Count - 2
            If ds.Tables(0).Rows(0).Item(i) <> "0" Then
                sChart.AppendLine("<set isSliced='1' showLabel='0' toolText='" + ds.Tables(0).Columns(i).ColumnName + "' label='" + ds.Tables(0).Columns(i).ColumnName + "' value='" + (Math.Round(ds.Tables(0).Rows(0).Item(i) * 100 / totalStudents, 2)).ToString + "' />")
            End If

        Next
        sChart.AppendLine("</chart>")
        ltGrade.Text = FusionCharts.RenderChart("FusionCharts/Pie3D.swf", "", sChart.ToString, "chart1", "350", "300", False, False)

        str_query = "exec [DHB].[STUDENTSUBJECTGRADEANALYSIS_DMHS] " _
                          & hfRPF_ID.Value + "," _
                          & Request.QueryString("acd_id") + "," _
                          & "'" + Request.QueryString("grd_id") + "'," _
                          & Request.QueryString("sbg_id") + "," _
                          & Request.QueryString("sct_id") + "," _
                          & "'" + Request.QueryString("stu_ids") + "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSelected.DataSource = ds
        gvSelected.DataBind()

        sChart = New StringBuilder
        sChart.AppendLine("<chart chartLeftMargin='0'  baseFontSize='13' captionPadding='0'   chartRightMargin='0' chartTopMargin='0' chartBottomMargin='10'  palette='3' showLegend='1'  showPercentValues='1'>")
        For i = 0 To ds.Tables(0).Columns.Count - 2
            If ds.Tables(0).Rows(0).Item(i) <> "0" Then
                sChart.AppendLine("<set isSliced='1' showLabel='0'  toolText='" + ds.Tables(0).Columns(i).ColumnName + "' label='" + ds.Tables(0).Columns(i).ColumnName + "' value='" + (Math.Round(ds.Tables(0).Rows(0).Item(i) * 100 / totalStudents, 2)).ToString + "' />")
            End If

        Next
        sChart.AppendLine("</chart>")
        ltStudents.Text = FusionCharts.RenderChart("FusionCharts/Pie3D.swf", "", sChart.ToString, "chart2", "350", "300", False, False)

    End Sub
End Class
