﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports CURRICULUM
Partial Class Curriculum_DashBoards_clmDbStudentDetails_MHS
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            hfYEAR1.Value = ""
            hfYEAR2.Value = ""
            hfYEAR3.Value = ""
            '  Try

            'hfACD_ID.Value = 881
            'hfGRD_ID.Value = "05"
            'hfSCT_ID.Value = 45523
            'hfSBG_ID.Value = 24634
            'hfSubject.Value = "MATHEMATICS"
            'hfSTU_IDS.Value = ""

            ' hfColumns.Value = "5|6|7|8|9|10|11|12|13|14|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63"

            'hfSTU_IDS.Value = "20106266"

            hfACD_ID.Value = Request.QueryString("acdid")
            hfGRD_ID.Value = Request.QueryString("grade")
            hfSCT_ID.Value = Request.QueryString("section")
            hfSBG_ID.Value = Request.QueryString("sbgid")
            hfSubject.Value = Request.QueryString("subject")
            hfSTU_IDS.Value = Request.QueryString("stuids")
            hfColumns.Value = Request.QueryString("columns")
            hfSGR_ID.Value = Request.QueryString("group")

            Page.Title = hfSubject.Value
            GridBind()
            ' Catch ex As Exception
            'UtilityObj.Errorlog(ex.Message)
            ' End Try
            gvStud.Attributes.Add("bordercolor", "#fffff")
        End If
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC  DHB.STUDENT_DETAILS_DMHS " _
                                & hfACD_ID.Value + ",'" _
                                & hfGRD_ID.Value + "'," _
                                & hfSCT_ID.Value + "," _
                                & hfSBG_ID.Value + "," _
                                & "'" + hfSubject.Value + "'," _
                                & "'" + hfSTU_IDS.Value + "'," _
                                & "'" + hfColumns.Value + "'," _
                                & hfSGR_ID.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer
        Dim htHeader As New Hashtable
        For i = 0 To ds.Tables(0).Columns.Count - 1
            htHeader.Add(ds.Tables(0).Columns(i).ColumnName, i)
        Next

        Session("htHeader") = htHeader


        gvStud.DataSource = ds
        gvStud.DataBind()

        If gvStud.Columns.Count > 0 Then
            gvStud.Columns(5).ItemStyle.Width = 100
        End If

      
     
    End Sub

    Sub SetGridHeader()
        '  gvStud.HeaderRow.Visible = False

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC  DHB.getSTUDENTDETAILS_HEADER_DMHS " + hfACD_ID.Value + ",0,'" + hfColumns.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer


        Dim HeaderCell1 As New TableCell()
        Dim HeaderCell2 As New TableCell()
        Dim HeaderCell3 As New TableCell()

        Dim HeaderGridRow1 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
        Dim HeaderGridRow2 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
        Dim HeaderGridRow3 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)



        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                HeaderCell1 = New TableCell()
                'Dim lbl As New Label
                'lbl.Text = .Item(0)

                HeaderCell1.Text = .Item(0)

                Dim sp As New HtmlGenericControl("span")
                sp.InnerHtml = .Item(0)
                sp.Attributes("class") = "gridrotate2"

                If .Item(0).ToString.Length > 3 Then
                    sp.Attributes("class") = "gridrotate3"
                Else
                    sp.Attributes("class") = "gridheader1"

                End If

                HeaderCell1.Controls.Add(sp)
                If i = 0 Then
                    HeaderCell1.Visible = False
                End If
                HeaderGridRow1.Cells.Add(HeaderCell1)
                HeaderGridRow1.VerticalAlign = VerticalAlign.Bottom
            End With
        Next

        str_query = "EXEC  DHB.getSTUDENTDETAILS_HEADER_DMHS " + hfACD_ID.Value + ",1,'" + hfColumns.Value + "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                HeaderCell2 = New TableCell()

                Dim sp As New HtmlGenericControl("span")
                sp.InnerHtml = .Item(1)
                sp.Attributes("class") = "gridrotate2"

                If .Item(1).ToString.ToLower.Contains("year average") Then
                    If hfYEAR2.Value <> "" And hfYEAR3.Value = "" Then
                        hfYEAR3.Value = .Item(1)
                    End If
                    If hfYEAR1.Value <> "" And hfYEAR2.Value = "" Then
                        hfYEAR2.Value = .Item(1)
                    End If

                    If hfYEAR1.Value = "" Then
                        hfYEAR1.Value = .Item(1)
                    End If


                   

                End If


                Select Case .Item(0)
                    Case "90", "91", "92", "108", "207", "307"
                        Dim queryStr As String = "&hdr_id=" + .Item(0).ToString + "&acd_id=" + hfACD_ID.Value + "&grd_id=" + hfGRD_ID.Value + "&sbg_id=" + hfSBG_ID.Value + "&subject=" + hfSubject.Value + "&sct_id=" + hfSCT_ID.Value + "&stu_ids=" + hfSTU_IDS.Value
                        sp.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','progresscharts','" + .Item(1).ToString.ToUpper + "')")
                        sp.Attributes.Add("onmouseover", "javascript:this.style.color='#ff6600'") '#00CC99 old color
                        sp.Attributes.Add("onmouseout", "javascript:this.style.color='#fff'")
                        sp.Attributes("class") = "gridrotate2_pointer"
                    Case "109", "208", "308"
                        Dim title As String
                        If .Item(0) = "109" Then
                            title = "SUBJECT GRADE ANALYSIS TERM 1"
                        ElseIf .Item(0) = "208" Then
                            title = "SUBJECT GRADE ANALYSIS TERM 2"
                        Else
                            title = "SUBJECT GRADE ANALYSIS TERM 3"
                        End If
                        Dim queryStr As String = "&hdr_id=" + .Item(0).ToString + "&acd_id=" + hfACD_ID.Value + "&grd_id=" + hfGRD_ID.Value + "&sbg_id=" + hfSBG_ID.Value + "&subject=" + hfSubject.Value + "&sct_id=" + hfSCT_ID.Value + "&stu_ids=" + hfSTU_IDS.Value
                        sp.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','gradecharts','" + title + "')")
                        sp.Attributes.Add("onmouseover", "javascript:this.style.color='#ff6600'") '#00CC99 old color
                        sp.Attributes.Add("onmouseout", "javascript:this.style.color='#fff'")
                        sp.Attributes("class") = "gridrotate2_pointer"
                End Select
                'Dim lbl As New Label
                'lbl.Text = .Item(1)
                'lbl.CssClass = "gridrotate2"


                HeaderCell2.Controls.Clear()
                HeaderCell2.Controls.Add(sp)
                'HeaderCell2.Width = 20
                ' HeaderCell2.Text = .Item(1)
                HeaderCell2.ColumnSpan = .Item(2)
                '  HeaderCell2.CssClass = "gridrotate2"
                HeaderCell2.Height = 10
                HeaderCell2.HorizontalAlign = HorizontalAlign.Center
                HeaderCell2.VerticalAlign = VerticalAlign.Bottom
                If i = 0 Then
                    HeaderCell2.Visible = False
                End If
                HeaderGridRow2.Cells.Add(HeaderCell2)
                HeaderGridRow2.VerticalAlign = VerticalAlign.Bottom

            End With
        Next

        str_query = "EXEC  DHB.getSTUDENTDETAILS_HEADER_DMHS " + hfACD_ID.Value + ",2,'" + hfColumns.Value + "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                HeaderCell3 = New TableCell()
                HeaderCell3.Text = .Item(1)
                HeaderCell3.ColumnSpan = .Item(2)
                HeaderGridRow3.Cells.Add(HeaderCell3)

                HeaderCell3.CssClass = "gridheader1"

                If i = 0 Then
                    HeaderCell3.Visible = False
                End If
            End With
        Next


        gvStud.Controls(0).Controls.AddAt(0, HeaderGridRow1)
        gvStud.Controls(0).Controls.AddAt(0, HeaderGridRow2)
        gvStud.Controls(0).Controls.AddAt(0, HeaderGridRow3)
    End Sub


    Sub ShowData(ByVal gRow As GridViewRow)
        'Try
        Dim stu As String() = gRow.Cells(0).Text.Split("|")
        Dim stu_id As String = stu(0)
        Dim photopath As String = stu(1)


        Dim i As Integer
        Dim sData As String()
        Dim htHeader As Hashtable = Session("htHeader")
       
        Dim lbl As New Label
        lbl.Text = gRow.Cells(1).Text
        Dim qStr As String = "&studentid=" + gRow.Cells(2).Text _
                & "&name=" + gRow.Cells(1).Text _
                & "&class=" + gRow.Cells(3).Text + " " + gRow.Cells(4).Text _
                & "&photopath=" + photopath _
                & "&acd_id=" + hfACD_ID.Value _
                & "&grd_id=" + hfGRD_ID.Value _
                & "&sct_id=" + hfSCT_ID.Value _
                & "&stu_id=" + stu_id

        lbl.Attributes.Add("onclick", "javascript:ShowPopup('" + qStr + "','studprofile','STUDENT PROFILE')")
        lbl.Attributes.Add("style", "cursor:pointer")
        gRow.Cells(1).Controls.Clear()
        gRow.Cells(1).Controls.Add(lbl)


        For i = 4 To gRow.Cells.Count - 1
            sData = gRow.Cells(i).Text.Split("__")

            Select Case sData(0)
               

                Case "20"  'SWOC ANALYSIS
                    Dim img As New Image
                    img.ImageUrl = "~/Images/Curriculum/PDF-icon7.png"
                    img.CssClass = "ImageStyle"
                    Dim queryStr As String = "&stu_id=" + stu_id + "&type=swoc&id=" + sData(2) + "&acd_id=" + hfACD_ID.Value + "&grd_id=" + hfGRD_ID.Value + "&sct_id=" + hfSCT_ID.Value
                    img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','reports','SWOC ANALYSIS')")
                    gRow.Cells(i).Controls.Clear()
                    gRow.Cells(i).Controls.Add(img)

                Case "21", "22", "23" 'SMART ANALYSIS
                    Dim img As New Image
                    img.ImageUrl = "~/Images/Curriculum/PDF-icon7.png"
                    img.CssClass = "ImageStyle"

                    Dim queryStr As String
                    If sData.Length >= 3 Then
                        queryStr = "&stu_id=" + stu_id + "&type=smart&id=" + sData(2) + "&acd_id=" + hfACD_ID.Value + "&grd_id=" + hfGRD_ID.Value + "&sct_id=" + hfSCT_ID.Value
                        img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','reports','SMART ANALYSIS')")
                    End If
                    gRow.Cells(i).Controls.Clear()
                    gRow.Cells(i).Controls.Add(img)
                Case "30", "31", "32"
                    If sData.Length > 2 Then
                        If sData(2) <> "0.00" And sData(2) <> "0" Then
                            gRow.Cells(i).Text = sData(2).Replace(".00", "")
                        Else
                            gRow.Cells(i).Text = ""
                        End If
                        If sData(4) <> "" Then
                            ' Dim strtooltip As String = "<div class=""bubble"">" + sData(4) + "</div>"
                            gRow.Cells(i).Attributes.Add("title", sData(4))
                            ' gRow.Cells(i).Attributes.Add("onmouseover", "'123'")
                            gRow.Cells(i).Attributes.Add("style", "cursor:pointer")
                        End If
                    End If
                    gRow.Cells(i).HorizontalAlign = HorizontalAlign.Center
                Case "11", "12", "13", "14" 'SUPPORT

                    Dim img As New Image
                    img.ImageUrl = "~/Images/Curriculum/file-icon4.png"
                    img.CssClass = "ImageStyle"
                    Dim queryStr As String
                    Dim docCat As String

                    If sData(0) = "11" Then
                        docCat = "IEP"
                    ElseIf sData(0) = "12" Then
                        docCat = "IAP"
                    ElseIf sData(0) = "13" Then
                        docCat = "BIP"
                    ElseIf sData(0) = "14" Then
                        docCat = "OTHER"
                    End If

                    If sData(2) <> 0 Then
                        queryStr = "&studentid=" + gRow.Cells(2).Text _
                               & "&name=" + gRow.Cells(1).Text _
                               & "&class=" + gRow.Cells(3).Text + " " + gRow.Cells(4).Text _
                               & "&photopath=" + photopath _
                               & "&acd_id=" + hfACD_ID.Value _
                               & "&grd_id=" + hfGRD_ID.Value _
                               & "&sct_id=" + hfSCT_ID.Value _
                               & "&stu_id=" + stu_id _
                               & "&type=" + docCat

                        img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','docupload','DOCUMENTS')")
                        gRow.Cells(i).Controls.Clear()
                        gRow.Cells(i).Controls.Add(img)
                    Else
                        gRow.Cells(i).Text = ""
                    End If
                Case "50", "51", "52" 'ACE REPORTS
                    Dim img As New Image
                    img.ImageUrl = "~/Images/Curriculum/PDF-icon7.png"
                    img.CssClass = "ImageStyle"
                    Dim queryStr As String
                    If sData.Length >= 4 Then
                        queryStr = "&stu_id=" + stu_id + "&type=ace&id=" + sData(2) + "&acd_id=" + hfACD_ID.Value _
                                 & "&rsm_id=" + sData(2) + "&rpf_id=" + sData(4) + "&rpf_descr=" + sData(6)
                        img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','reports','REPORT CARDS')")
                    End If
                    gRow.Cells(i).Controls.Clear()
                    gRow.Cells(i).Controls.Add(img)
                Case "60", "61", "62", "112", "211", "311" 'REPORTCARD
                    Dim img As New Image
                    img.ImageUrl = "~/Images/Curriculum/PDF-icon7.png"
                    img.CssClass = "ImageStyle"
                    Dim queryStr As String
                    If sData.Length >= 4 Then
                        queryStr = "&stu_id=" + stu_id + "&type=progressreports&id=" + sData(2) + "&acd_id=" + hfACD_ID.Value + "&grd_id=" + hfGRD_ID.Value + "&sct_id=" + hfSCT_ID.Value _
                                 & "&rsm_id=" + sData(2) + "&rpf_id=" + sData(4)
                        img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','reports','REPORT CARDS')")
                    End If
                    gRow.Cells(i).Controls.Clear()
                    gRow.Cells(i).Controls.Add(img)
                Case "93", "111", "210", "310" 'PROGRES GRAPGH
                    Dim img As New Image
                    img.ImageUrl = "~/Images/Curriculum/graph5.png"
                    img.CssClass = "ImageStyle"

                    Dim queryStr As String
                    If sData(0) = "93" Then 'YEAR AVERAGE
                        queryStr = "&studentid=" + gRow.Cells(2).Text _
                                 & "&name=" + gRow.Cells(1).Text _
                                 & "&class=" + gRow.Cells(3).Text + " " + gRow.Cells(4).Text _
                                 & "&photopath=" + photopath _
                                 & "&acd_id=" + hfACD_ID.Value _
                                 & "&grd_id=" + hfGRD_ID.Value _
                                 & "&sct_id=" + hfSCT_ID.Value _
                                 & "&stu_id=" + stu_id _
                                 & "&type=yearprogress&sdata=" + hfYEAR1.Value + "__" + IIf(htHeader.ContainsKey("90") = True, gRow.Cells(htHeader("90")).Text.Replace("&nbsp;", ""), "0") _
                                         & "|" + hfYEAR2.Value + "__" + IIf(htHeader.ContainsKey("91") = True, gRow.Cells(htHeader("91")).Text.Replace("&nbsp;", ""), "0") + "|" _
                                         & hfYEAR3.Value + "__" + IIf(htHeader.ContainsKey("92") = True, gRow.Cells(htHeader("92")).Text.Replace("&nbsp;", ""), "0")

                        img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','charts','PROGRESS ACROSS THREE YEARS')")

                    ElseIf sData(0) = "111" Then 'TERM 1
                        queryStr = "&studentid=" + gRow.Cells(2).Text _
                                 & "&name=" + gRow.Cells(1).Text _
                                 & "&class=" + gRow.Cells(3).Text + " " + gRow.Cells(4).Text _
                                 & "&photopath=" + photopath _
                                 & "&acd_id=" + hfACD_ID.Value _
                                 & "&grd_id=" + hfGRD_ID.Value _
                                 & "&sct_id=" + hfSCT_ID.Value _
                                 & "&stu_id=" + stu_id _
                                  & "&type=yearprogress&sdata=" + hfYEAR1.Value + "__" + IIf(htHeader.ContainsKey("90") = True, gRow.Cells(htHeader("90")).Text.ToString.Replace("&nbsp;", ""), "0") + "|" _
                                                             & hfYEAR2.Value + "__" + IIf(htHeader.ContainsKey("91") = True, gRow.Cells(htHeader("91")).Text.ToString.Replace("&nbsp;", ""), "0") + "|" _
                                                             & hfYEAR3.Value + "__" + IIf(htHeader.ContainsKey("92") = True, gRow.Cells(htHeader("92")).Text.ToString.Replace("&nbsp;", ""), "0") _
                                                             & "|Progress at the end of Term 1 " + "__" + IIf(htHeader.ContainsKey("109") = True, gRow.Cells(htHeader("109")).Text.Replace("&nbsp;", ""), "0")
                        ' & "&type=yearprogress&sdata=" + hfYEAR1.Value + "__" + gRow.Cells(i - 13).Text.ToString.Replace("&nbsp;", "") + "|" + hfYEAR2.Value + "__" + gRow.Cells(i - 12).Text.ToString.Replace("&nbsp;", "") + "|" + hfYEAR3.Value + "__" + gRow.Cells(i - 11).Text.ToString.Replace("&nbsp;", "") + "|Progress at the end of Term 1 " + "__" + gRow.Cells(i - 2).Text.Replace("&nbsp;", "")

                        img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','charts','PROGRESS AT THE END OF TERM 1')")

                    ElseIf sData(0) = "210" Then 'TERM 2
                        queryStr = "&studentid=" + gRow.Cells(2).Text _
                                 & "&name=" + gRow.Cells(1).Text _
                                 & "&class=" + gRow.Cells(3).Text + " " + gRow.Cells(4).Text _
                                 & "&photopath=" + photopath _
                                 & "&acd_id=" + hfACD_ID.Value _
                                 & "&grd_id=" + hfGRD_ID.Value _
                                 & "&sct_id=" + hfSCT_ID.Value _
                                 & "&stu_id=" + stu_id _
                                 & "&type=yearprogress&sdata=" + hfYEAR1.Value + "__" + IIf(htHeader.ContainsKey("90") = True, gRow.Cells(htHeader("90")).Text.Replace("&nbsp;", ""), "0") + "|" _
                                          & hfYEAR2.Value + "__" + IIf(htHeader.ContainsKey("91") = True, gRow.Cells(htHeader("91")).Text.Replace("&nbsp;", ""), "0") + "|" _
                                          & hfYEAR3.Value + "__" + IIf(htHeader.ContainsKey("92") = True, gRow.Cells(htHeader("92")).Text.Replace("&nbsp;", ""), "0") _
                                          & "|Progress at the end of Term 1 " + "__" + IIf(htHeader.ContainsKey("109") = True, gRow.Cells(htHeader("109")).Text.Replace("&nbsp;", ""), "0") _
                                          & "|Progress at the end of Term 2 " + "__" + IIf(htHeader.ContainsKey("208") = True, gRow.Cells(htHeader("208")).Text.Replace("&nbsp;", ""), "0")

                        '& "&type=yearprogress&sdata=" + hfYEAR1.Value + "__" + gRow.Cells(i - 22).Text.Replace("&nbsp;", "") + "|" + hfYEAR2.Value + "__" + gRow.Cells(i - 21).Text.Replace("&nbsp;", "") + "|" + hfYEAR3.Value + "__" + gRow.Cells(i - 20).Text.Replace("&nbsp;", "") _
                        '                        & "|Progress at the end of Term 1 " + "__" + gRow.Cells(i - 11).Text.Replace("&nbsp;", "") _
                        '                        & "|Progress at the end of Term 2 " + "__" + gRow.Cells(i - 2).Text.Replace("&nbsp;", "")
                        img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','charts','PROGRESS AT THE END OF TERM 2')")
                    ElseIf sData(0) = "310" Then 'TERM 3
                        queryStr = "&studentid=" + gRow.Cells(2).Text _
                                 & "&name=" + gRow.Cells(1).Text _
                                 & "&class=" + gRow.Cells(3).Text + " " + gRow.Cells(4).Text _
                                 & "&photopath=" + photopath _
                                 & "&acd_id=" + hfACD_ID.Value _
                                 & "&grd_id=" + hfGRD_ID.Value _
                                 & "&sct_id=" + hfSCT_ID.Value _
                                 & "&stu_id=" + stu_id _
                                 & "&type=yearprogress&sdata=" + hfYEAR1.Value + "__" + IIf(htHeader.ContainsKey("90") = True, gRow.Cells(htHeader("90")).Text.Replace("&nbsp;", ""), "0") + "|" _
                                          & hfYEAR2.Value + "__" + IIf(htHeader.ContainsKey("91") = True, gRow.Cells(htHeader("91")).Text.Replace("&nbsp;", ""), "0") + "|" _
                                          & hfYEAR3.Value + "__" + IIf(htHeader.ContainsKey("92") = True, gRow.Cells(htHeader("92")).Text.Replace("&nbsp;", ""), "0") _
                                          & "|Progress at the end of Term 1 " + "__" + IIf(htHeader.ContainsKey("109") = True, gRow.Cells(htHeader("109")).Text.Replace("&nbsp;", ""), "0") _
                                          & "|Progress at the end of Term 2 " + "__" + IIf(htHeader.ContainsKey("208") = True, gRow.Cells(htHeader("208")).Text.Replace("&nbsp;", ""), "0") _
                                          & "|Progress at the end of Term 3 " + "__" + IIf(htHeader.ContainsKey("308") = True, gRow.Cells(htHeader("308")).Text.Replace("&nbsp;", ""), "0")

                        '& "&type=yearprogress&sdata=" + hfYEAR1.Value + "__" + gRow.Cells(i - 31).Text.Replace("&nbsp;", "") + "|" + hfYEAR2.Value + "__" + gRow.Cells(i - 30).Text.Replace("&nbsp;", "") + "|" + hfYEAR3.Value + "__" + gRow.Cells(i - 29).Text.Replace("&nbsp;", "") _
                        '                        & "|Progress at the end of Term 1 " + "__" + gRow.Cells(i - 20).Text.Replace("&nbsp;", "") _
                        '                        & "|Progress at the end of Term 2 " + "__" + gRow.Cells(i - 11).Text.Replace("&nbsp;", "") _
                        '                        & "|Progress at the end of Term 3 " + "__" + gRow.Cells(i - 2).Text.Replace("&nbsp;", "")

                        img.Attributes.Add("onclick", "javascript:ShowPopup('" + queryStr + "','charts','PROGRESS AT THE END OF TERM 3')")

                    End If
                    gRow.Cells(i).Controls.Clear()
                    gRow.Cells(i).Controls.Add(img)
                Case Else
                    If sData.Length > 2 Then
                        If sData(2) <> "0.00" And sData(2) <> "0" Then
                            gRow.Cells(i).Text = sData(2).Replace(".00", "")
                        Else
                            gRow.Cells(i).Text = ""
                        End If
                    End If
                    gRow.Cells(i).HorizontalAlign = HorizontalAlign.Center
            End Select
        Next
        ' Catch ex As Exception
        'End Try
    End Sub

    Protected Sub gvStud_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvStud.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Visible = False
            SetGridHeader()
        End If
        e.Row.Cells(0).Visible = False
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            ShowData(e.Row)
        End If
    End Sub
End Class
