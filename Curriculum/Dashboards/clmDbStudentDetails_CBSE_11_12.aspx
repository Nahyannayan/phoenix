﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmDbStudentDetails_CBSE_11_12.aspx.vb"
    Inherits="Curriculum_DashBoards_clmDbStudentDetails_CBSE_11_12" %>
 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../../cssfiles/curriculum.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Master Tracker</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    </script>
</head>
<body>

    <form id="form1" runat="server">



    <script language="javascript" type="text/javascript">

        function openWin(qStr) {
            var sFeatures;
            sFeatures = "dialogWidth: 1050px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var oWnd = radopen("../../Students/StudPro_details.aspx?" + qStr, "RadWindow1");

           }

         
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function ShowRPTSETUP_S(id, status) {

            var sFeatures;
            sFeatures = "dialogWidth: 1050px; ";
            sFeatures += "dialogHeight: 750px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/StudPro_details.aspx?id=' + id + '&status=' + status + '';
            //alert(status) 

            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }

        }




          function ShowPopup(queryStr, filetype, titleStr) {

            document.getElementById('<%=lblTitle.ClientID %>').innerText = titleStr;
          
            var iFrm = document.getElementById("ifDB");
            if (filetype == "charts") {
                iFrm.src = "clmDBStudentCharts.aspx?" + queryStr;
            }
            else if (filetype == "progresscharts") {
                iFrm.src = "clmDBProgressCharts.aspx?" + queryStr;
            }
            else if (filetype == "gradecharts") {
            iFrm.src = "clmDbSubjectGradeAnalysisCharts.aspx?" + queryStr;
            }
            else if (filetype == "docupload") {
            iFrm.src = "clmDBSENDocUploads.aspx?" + queryStr;
           }
           else if (filetype == "studprofile") {
            // iFrm.src = "clmDBStudProfile.aspx?" + queryStr;
            iFrm.src = "../../Students/StudPro_details.aspx?" + queryStr;
              }
            else {
                iFrm.src = "clmDBStudentReports.aspx?" + queryStr;
            }

            var modalPopupBehaviorCtrl = $find('ModalPopupBehaviorID');
            modalPopupBehaviorCtrl.show();
          

        }
    
    </script>
            
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px" >
            </telerik:RadWindow>
          
        </Windows>
    </telerik:RadWindowManager>

    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
    </ajaxToolkit:ToolkitScriptManager>
    <div>

        <ajaxToolkit:ModalPopupExtender ID="mpe" OnOkScript="onPrvOk();" CancelControlID="ImageButton1"
            DynamicServicePath="" runat="server" Enabled="True" TargetControlID="btnTarget"
            BehaviorID="ModalPopupBehaviorID" PopupControlID="plPrev" BackgroundCssClass="modalBackground" />
        
                <asp:Button ID="btnTarget" runat="server" Text="Button" Style="display: none" />
        <div id="plPrev" runat="server" class="PnlPopup">
            <div style="width: 835px;">
                <div class="msgClose" style=" height: 20px; vertical-align: top;">
                    <table width="100%" border="0">
                        <tr>
                            <td>
                              <asp:Label ID="lblTitle" CssClass="lblTitle"  runat="server"></asp:Label>
                            </td>
                            <td align="right" valign="top">
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/Curriculum/btnClose.png" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <asp:Panel ID="plPrev1" runat="server" BackColor="White" Height="500px" ScrollBars="vertical"
                Width="835px">
                <iframe id="ifDB" height="100%" src="clmDBStudentReports.aspx" scrolling="yes" marginwidth="0px"
                    frameborder="0" width="918px"></iframe>
            </asp:Panel>
        </div>
    </div>
    <div>
        <table cellpadding="5" cellspacing="5">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvStud" runat="server" CssClass="gridstyle">
                        <RowStyle Wrap="false" CssClass="gridrowstyle1" />
                        <AlternatingRowStyle CssClass="gridrowstyle2" />
                        <HeaderStyle VerticalAlign="Bottom" HorizontalAlign="Center" CssClass="gridheader" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hfSTU_IDS" runat="server" />
    <asp:HiddenField ID="hfACD_ID" runat="server" />
    <asp:HiddenField ID="hfGRD_ID" runat="server" />
    <asp:HiddenField ID="hfSCT_ID" runat="server" />
    <asp:HiddenField ID="hfSBG_ID" runat="server" />
    <asp:HiddenField ID="hfSubject" runat="server" />
    <asp:HiddenField ID="hfYEAR1" runat="server" />
    <asp:HiddenField ID="hfYEAR2" runat="server" />
    <asp:HiddenField ID="hfYEAR3" runat="server" />
    <asp:HiddenField ID="hfColumns" runat="server" />
    <asp:HiddenField ID="hfbPrimary" runat="server" />
    <br />
    <asp:HiddenField ID="hfSGR_ID" runat="server" />
    </form>
</body>
</html>
<%--VerticalAlign="Bottom" HorizontalAlign="Center" Width="20px"--%>