﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmDbSubjectGradeAnalysisCharts.aspx.vb"
    Inherits="Curriculum_DashBoards_clmDbSubjectGradeAnalysisCharts" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../../cssfiles/curriculum.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
      <script type="text/javascript" src="FusionCharts/FusionCharts.js"></script>

    <script type="text/javascript" src="FusionCharts/jquery.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <table border="0" width="800px">
                <tr>
                    <td valign="top" width="50%">
                        <br />
                        <fieldset style="height: 415px">
                            <legend style="color: black; font-weight: bold; font-family: verdana; font-size: 8pt">
                                <asp:Label ID="lblHdr" runat="server"></asp:Label></legend>
                            <table border="1" cellpadding="10" cellspacing="10" class="tableChart">
                                <tr>
                                    <td class="tdChart" align="center">
                                        <table cellpadding="5" cellspacing="5">
                                            <tr>
                                                <td class="chartFields">
                                                    <asp:GridView ID="gvGrade"  runat="server"  HorizontalAlign="Center" Width="350px" Height="50px"    >
                                                        <RowStyle CssClass="chartgridrowstyle" Width="40px"  HorizontalAlign="Center"/>
                                                        <HeaderStyle CssClass="chartgridheader" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <asp:Literal ID="ltGrade" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td valign="top">
                        <br />
                        <fieldset style="height: 415px">
                            <legend style="color: black; font-weight: bold; font-family: verdana; font-size: 8pt">
                                Selected Students</legend>
                            <table border="1" cellpadding="10" cellspacing="10" class="tableChart">
                                <tr>
                                    <td class="tdChart" align="center">
                                        <table cellpadding="5" cellspacing="5">
                                            <tr>
                                                <td class="chartFields">
                                                      <asp:GridView ID="gvSelected"  runat="server"  HorizontalAlign="Center" Width="350px" Height="50px"    >
                                                        <RowStyle CssClass="chartgridrowstyle" Width="40px"  HorizontalAlign="Center"/>
                                                        <HeaderStyle CssClass="chartgridheader" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <asp:Literal ID="ltStudents" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:HiddenField ID="hfRPF_ID" runat="server" />
    </form>
</body>
</html>
