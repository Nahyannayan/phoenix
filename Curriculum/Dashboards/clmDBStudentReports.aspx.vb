﻿
Partial Class Curriculum_DashBoards_clmDBStudentReports
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim id As String = Request.QueryString("type")
        lbl1.Text = id

        ShowReport(Request.QueryString("type"))
    End Sub

    Sub ShowReport(ByVal type As String)

        Select Case Type
            Case "swoc"
                CallSWOCReport()

            Case "smart"

                CallSMARTReport()
            Case "progressreports"
                CallProgressReports()
        End Select

    End Sub

    Sub CallSWOCReport()

        Dim param As New Hashtable
        param.Add("@ACD_ID", Request.QueryString("acd_id"))
        param.Add("@SWM_ID", Request.QueryString("id"))
        param.Add("@SCT_ID", Request.QueryString("sct_id"))
        param.Add("@STU_ID", Request.QueryString("stu_id"))
        param.Add("@GRD_ID", Request.QueryString("grd_id"))
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportPath = Server.MapPath("../Reports/Rpt/rptSWOCStudentDetails.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        Dim rptDownload As New ReportDownload
        rptDownload.LoadReports(rptClass, rs, "display")
        rptDownload = Nothing

    End Sub

    Sub CallSMARTReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", Request.QueryString("acd_id"))
        param.Add("@TGM_ID", Request.QueryString("id"))
        param.Add("@SCT_ID", Request.QueryString("sct_id"))
        param.Add("@STU_ID", Request.QueryString("stu_id"))
        param.Add("@GRD_ID", Request.QueryString("grd_id"))
      
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportPath = Server.MapPath("../Reports/Rpt/rptSmartTargetPrint.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        Dim rptDownload As New ReportDownload
        rptDownload.LoadReports(rptClass, rs, "display")
        rptDownload = Nothing
    End Sub

    Sub CallProgressReports()
        Try

        
            Dim lblRpfId As New Label
            Dim lblRsmId As New Label
            Dim url As String
            Dim viewid As String
          

            '  ViewState("MainMnu_code") = "StudentProfile"
            ViewState("MainMnu_code") = "PdfReport"

            Dim str As String = "/PHOENIXBETA/Curriculum/Reports/Aspx/rptMonthlyProgressReportStudWise.aspx?" _
            & "MainMnu_code=" + Encr_decrData.Encrypt(ViewState("MainMnu_code")) _
            & "&datamode=" + Encr_decrData.Encrypt("view") _
            & "&rsmid=" + Encr_decrData.Encrypt(Request.QueryString("rsm_id")) _
            & "&rpfid=" + Encr_decrData.Encrypt(Request.QueryString("rpf_id")) _
            & "&acdid=" + Encr_decrData.Encrypt(Request.QueryString("acd_id")) _
            & "&stuid=" + Request.QueryString("stu_id") _
            & "&grdid=" + Encr_decrData.Encrypt(Request.QueryString("grd_id"))
            Response.Redirect(str)
        Catch ex As Exception

        End Try
    End Sub
End Class
