﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmDBSENDocUploads.aspx.vb"
    Inherits="Curriculum_DashBoards_clmDBSENDocUploads" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../../cssfiles/curriculum.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <table border="0" width="800px">
            <tr>
                <td valign="top" width="50%">
                    <br />
        <fieldset style="height: 415px;">
            <legend style="color: black; font-weight: bold; font-family: verdana; font-size: 8pt">
                <asp:Label ID="lblHdr" runat="server"></asp:Label></legend>
            <table border="1" cellpadding="0" cellspacing="0" class="tableChart" width="100%">
           <tr>
                <td  Width="120px" class="tdChart">
                    <asp:Image ID="imgStudent" runat="server" Height="132px" Width="120px" />
                </td>
                <td class="tdChart" valign="top" >
                    <table cellpadding="5" cellspacing="5" border="0" class="tableNoborder">
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                            <td align="left"  class="chartFields">
                                Student ID
                            </td>
                            <td   class="chartFields">
                                :
                            </td>
                            <td   class="chartFields">
                                <asp:Label align="left" ID="lblStudentID" runat="server" ></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td align="left" class="chartFields">
                                Student Name
                            </td>
                            <td   class="chartFields">
                                :
                            </td>
                            <td   class="chartFields">
                                <asp:Label align="left" ID="lblName" runat="server" ></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td align="left"   class="chartFields">
                                Grade & Section
                            </td>
                            <td   class="chartFields">
                                :
                            </td>
                            <td   class="chartFields">
                                <asp:Label align="left" ID="lblGrade" runat="server" ></asp:Label>
                            </td>
                        </tr>
                       
                    </table>
                </td>
              
            </tr>
                <tr>
                    <td class="tdChart" align="left" colspan="2">
                              <asp:DataList ID="dlDocs" runat="server" RepeatColumns="5" CellPadding="10" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table >
                                                <tr>
                                                    <td align="center" valign="top" style="height:40px" >
                                                        <asp:ImageButton ID="imgF" runat="server" Width="50px"  OnClick="imgF_Click" CommandArgument='<%# Bind("doc_id") %>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height:40px;width:100px" align="center" valign="top"  class="chartFields">
                                                        <asp:LinkButton ID="txtfile" ForeColor="Black" runat="server" Width="50px" Text='<%# Bind("doc_name") %>'
                                                            CommandName="View" CommandArgument='<%# Bind("doc_id") %>' OnClick="txtfile_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        
                                    </asp:DataList>
                                </td>
                            </tr>
                        </table>
        </fieldset>
         </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
