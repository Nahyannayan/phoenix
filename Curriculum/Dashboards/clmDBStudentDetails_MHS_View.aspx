﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmDBStudentDetails_MHS_View.aspx.vb" Inherits="Curriculum_DashBoards_clmDBStudentDetails_MHS_View"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        table td{
            vertical-align :top !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value.substring(0, 2);
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var gender = "ALL";
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }

            var oWnd = radopen("../../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs + "&gender=" + gender, "RadWindow1");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
               document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[1];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value.substring(0, 2);
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var gender = "ALL";
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs + "&gender=" + gender, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = document.getElementById('<%=h_STU_IDs.ClientID %>').value + result; //NameandCode[0];
            }
            else {
                return false;
            }
        }


        function fnSelectAll1(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    //  alert(curr_elem.id.substring(0,30));
                    if (curr_elem.id.substring(0, 30) == "ctl00_cphMasterpage_lstSelect1") {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }

        }

        function fnSelectAll2(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    //  alert(curr_elem.id.substring(0,30));
                    if (curr_elem.id.substring(0, 30) == "ctl00_cphMasterpage_lstSelect2") {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }

        }

        function fnSelectAll3(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    //  alert(curr_elem.id.substring(0,30));
                    if (curr_elem.id.substring(0, 30) == "ctl00_cphMasterpage_lstSelect3") {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }

        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Master Tracker
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label>
                            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0"
                                style="border-collapse: collapse">

                                <tr>
                                    <td align="left" width="30%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="129px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>&nbsp;<asp:CheckBox ID="chkSen" Text="SEN Students" runat="server" AutoPostBack="true" />
                                    </td>
                                </tr>

                                <tr id="trSection" runat="server">
                                    <td align="left" width="30%"><span class="field-label">Section</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" width="30%"><span class="field-label">Subject</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                </tr>

                                <tr id="trGroup" runat="server">
                                    <td align="left" width="30%"><span class="field-label">Group</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGroup" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trStudent" runat="server">
                                    <td align="left" width="30%"><span class="field-label">Select Students</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;"
                                            OnClick="imgStudent_Click"></asp:ImageButton>
                                        <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            PageSize="5" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Stud. No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>
                                        <asp:LinkButton ID="lnkClear" SkinID="linkred" runat="server" Text="clear"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top">
                                        <table width="100%" valign="top">
                                            <tr valign="top">
                                                <td valign="top" width="30%">
                                                     <asp:CheckBox ID="chkSelect1" onclick="javascript:fnSelectAll1(this);" runat="server"
                                                                    Text="STUDENT PROFILE" CssClass="field-label" />
                                                                <br />
                                                    <div class="checkbox-list">
                                                                <asp:CheckBoxList ID="lstSelect1" runat="server" CssClass="field-label">
                                                                    <asp:ListItem Value="LP">Learning Preference (VARK)</asp:ListItem>
                                                                    <asp:ListItem Value="SWOC">SWOC Analysis (PDF)</asp:ListItem>
                                                                    <asp:ListItem Value="SMARTPDF">SMART Target (PDF)</asp:ListItem>
                                                                    <asp:ListItem Value="MNT">MENTORING</asp:ListItem>
                                                                    <asp:ListItem Value="ENH">ENANCEMENT</asp:ListItem>
                                                                    <asp:ListItem Value="ACE">ACE</asp:ListItem>
                                                                    <asp:ListItem Value="MTR">MTR</asp:ListItem>
                                                                    <asp:ListItem Value="CAT">CAT 4 Scores</asp:ListItem>
                                                                    <asp:ListItem Value="ASSET">ASSET</asp:ListItem>
                                                                    <asp:ListItem Value="SEN">SEN</asp:ListItem>
                                                                    <asp:ListItem Value="SUPPORT">SUPPORT</asp:ListItem>
                                                                </asp:CheckBoxList></div>
                                                </td>
                                                <td valign="top" width="30%">
                                                     <asp:CheckBox ID="chkSelect2" onclick="javascript:fnSelectAll2(this);" runat="server"
                                                                    Text="TERM" CssClass="field-label" />
                                                                <br />
                                                    <div class="checkbox-list" CssClass="field-label">
                                                                <asp:CheckBoxList ID="lstSelect2" runat="server">
                                                                    <asp:ListItem Value="T1">Term 1</asp:ListItem>
                                                                    <asp:ListItem Value="T2">Term 2</asp:ListItem>
                                                                    <asp:ListItem Value="T3">Term 3</asp:ListItem>
                                                                </asp:CheckBoxList></div>
                                                </td>
                                                <td valign="top" width="30%">
                                                     <asp:CheckBox ID="chkSelect3" onclick="javascript:fnSelectAll3(this);" runat="server"
                                                                    Text="TERM" CssClass="field-label"/><br />
                                                    <div class="checkbox-list" CssClass="field-label">
                                                                <asp:CheckBoxList ID="lstSelect3" runat="server">
                                                                    <asp:ListItem Value="YP">3 YEARS AVERAGE</asp:ListItem>
                                                                    <asp:ListItem Value="ELT">ELT</asp:ListItem>
                                                                    <asp:ListItem Value="FA">FA</asp:ListItem>
                                                                    <asp:ListItem Value="SA">SA</asp:ListItem>
                                                                    <asp:ListItem Value="SMARTSCORE">SMART Target Score</asp:ListItem>
                                                                    <asp:ListItem Value="TE">Term Exams</asp:ListItem>
                                                                    <asp:ListItem Value="RC">Report Card</asp:ListItem>
                                                                </asp:CheckBoxList></div>
                                                </td>
                                            </tr>
                                        </table>
                                                                                                                                                        
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td valign="bottom" align="center" colspan="2">
                                        <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" ValidationGroup="groupM1" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
