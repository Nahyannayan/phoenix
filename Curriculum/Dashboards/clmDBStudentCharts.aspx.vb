﻿Imports InfosoftGlobal
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Curriculum_DashBoards_clmDBStudentCharts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sChart As New StringBuilder

            Dim sType As String = Request.QueryString("type")
            Dim sData As String = Request.QueryString("sdata")

            lblName.Text = Request.QueryString("name")
            lblStudentID.Text = Request.QueryString("studentid")
            lblGrade.Text = Request.QueryString("class")

            If sType = "yearprogress" Then
                sChart.Append("<chart caption='Student Progress' palette='3' paletteColors='FF5904,0372AB,FF0000'  xAxisName='Year' yAxisName='Marks' numberPrefix=''>")
            End If

            Dim strTemp As String() = sData.Split("|")

            Dim i As Integer
            For i = 0 To strTemp.Length - 1
                Dim str As String() = strTemp(i).ToString.Split("__")
                If str(2) <> "" And str(2) <> "0" And str(2) <> "&nbsp;" Then
                    sChart.Append("<set label='" + str(0) + "' value='" + str(2) + "' />")
                End If
            Next
            sChart.Append("</chart>")

            Literal1.Text = FusionCharts.RenderChart("FusionCharts/Line.swf", "", sChart.ToString, "myFirst", "600", "300", False, False)
            BindPhoto()
        Catch ex As Exception
        End Try
    End Sub


    Sub BindPhoto()

        If Request.QueryString("photopath") <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + Request.QueryString("photopath")
            imgStudent.ImageUrl = strImagePath
        Else
            imgStudent.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub

   

End Class
