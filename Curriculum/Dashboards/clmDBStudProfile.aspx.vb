﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports InfosoftGlobal

Partial Class Curriculum_DashBoards_clmDBStudProfile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblName.Text = Request.QueryString("name")
        lblStudentID.Text = Request.QueryString("studentid")
        lblGrade.Text = Request.QueryString("class")
        lblHdr.Text = Request.QueryString("type") + " Documents"
        BindPhoto()


        ViewState("viewid") = Request.QueryString("stu_id")
        HF_stuid.Value = Request.QueryString("stu_id")
        binddetails(HF_stuid.Value)
        ShowAttendance()
    End Sub

    Sub BindPhoto()
        If Request.QueryString("photopath") <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + Request.QueryString("photopath")
            imgStudent.ImageUrl = strImagePath
        Else
            imgStudent.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub

    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As DataSet
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "stu_contactDetails", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        Ltl_Fname.Text = Convert.ToString(readerStudent_Detail("STS_FNAME"))
                        Ltl_Fnationality.Text = readerStudent_Detail("FNATIONALITY").ToString
                        Ltl_Fpob.Text = readerStudent_Detail("STS_FCOMPOBOX").ToString
                        Ltl_FEmirate.Text = readerStudent_Detail("STS_FEMIR").ToString
                        Ltl_FPhoneRes.Text = readerStudent_Detail("STS_FRESPHONE").ToString
                        Ltl_FOfficePhone.Text = readerStudent_Detail("STS_FOFFPHONE").ToString
                        Ltl_FMobile.Text = readerStudent_Detail("STS_FMOBILE").ToString
                        ' Ltl_FEmail.Text = readerStudent_Detail("STS_FEMAIL").ToString
                        Ltl_FFax.Text = readerStudent_Detail("STS_FFAX").ToString
                        Ltl_FOccupation.Text = readerStudent_Detail("STS_FOCC").ToString
                        Ltl_FCompany.Text = readerStudent_Detail("STS_FCOMPANY").ToString
                        If Convert.ToString(readerStudent_Detail("STS_FEMAIL")).Trim <> "" Then
                            Ltl_FEmail.Text = "<a href='mailto:" & Convert.ToString(readerStudent_Detail("STS_FEMAIL")) & "' >" & Convert.ToString(readerStudent_Detail("STS_FEMAIL")) & "</a>"
                        End If


                        Ltl_Mname.Text = readerStudent_Detail("STS_MNAME").ToString
                        Ltl_Mnationality.Text = readerStudent_Detail("MNATIONALITY").ToString
                        Ltl_Mpob.Text = readerStudent_Detail("STS_MCOMPOBOX").ToString
                        Ltl_MEmirate.Text = readerStudent_Detail("STS_MEMIR").ToString
                        Ltl_MPhoneRes.Text = readerStudent_Detail("STS_MRESPHONE").ToString
                        Ltl_MOfficePhone.Text = readerStudent_Detail("STS_MOFFPHONE").ToString
                        Ltl_MMobile.Text = readerStudent_Detail("STS_MMOBILE").ToString
                        ' Ltl_MEmail.Text = readerStudent_Detail("STS_MEMAIL").ToString
                        Ltl_MFax.Text = readerStudent_Detail("STS_MFAX").ToString
                        Ltl_MOccupation.Text = readerStudent_Detail("STS_MOCC").ToString
                        Ltl_MCompany.Text = readerStudent_Detail("STS_MCOMPANY").ToString
                        If Convert.ToString(readerStudent_Detail("STS_MEMAIL")).Trim <> "" Then
                            Ltl_MEmail.Text = "<a href='mailto:" & Convert.ToString(readerStudent_Detail("STS_MEMAIL")) & "' >" & Convert.ToString(readerStudent_Detail("STS_MEMAIL")) & "</a>"
                        End If


                        Ltl_Mname.Text = readerStudent_Detail("STS_GNAME").ToString
                        Ltl_Gnationality.Text = readerStudent_Detail("GNATIONALITY").ToString
                        Ltl_Gpob.Text = readerStudent_Detail("STS_GCOMPOBOX").ToString
                        Ltl_GEmirate.Text = readerStudent_Detail("STS_GEMIR").ToString
                        Ltl_GPhoneRes.Text = readerStudent_Detail("STS_GRESPHONE").ToString
                        Ltl_GOfficePhone.Text = readerStudent_Detail("STS_GOFFPHONE").ToString
                        Ltl_GMobile.Text = readerStudent_Detail("STS_GMOBILE").ToString
                        ' Ltl_GEmail.Text = readerStudent_Detail("STS_GEMAIL").ToString
                        Ltl_GFax.Text = readerStudent_Detail("STS_GFAX").ToString
                        Ltl_GOccupation.Text = readerStudent_Detail("STS_GOCC").ToString
                        Ltl_GCompany.Text = readerStudent_Detail("STS_GCOMPANY").ToString
                        If Convert.ToString(readerStudent_Detail("STS_GEMAIL")).Trim <> "" Then
                            Ltl_GEmail.Text = "<a href='mailto:" & Convert.ToString(readerStudent_Detail("STS_GEMAIL")) & "' >" & Convert.ToString(readerStudent_Detail("STS_GEMAIL")) & "</a>"
                        End If
                        Dim col As Integer
                        If readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "F" Then
                            col = 2
                        ElseIf readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "M" Then
                            col = 3

                        ElseIf readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "G" Then
                            col = 4
                        Else
                            col = 2
                        End If


                        ' table1.Rows(0).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(1).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(2).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(3).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(4).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(5).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(6).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(7).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(8).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(9).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(10).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(11).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(12).Cells(col).BgColor = "#FFFFC"
                    End While
                Else

                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception

        End Try

    End Sub

    Sub ShowAttendance()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim percent As Double
            ' Format(Now, "dd/MMM/yyyy")
            Dim str_query As String = "EXEC [ATT].[GETSTUDATT_TILLDATE_WITH_STU_ID] " _
                                    & "@P_ACD_ID=" + Request.QueryString("acd_id") + "," _
                                    & "@P_GRD_ID='" + Request.QueryString("grd_id") + "'," _
                                    & "@P_SCT_ID=" + Request.QueryString("sct_id") + "," _
                                    & "@TILLDATE='" + Format(Now, "dd/MMM/yyyy") + "'," _
                                    & "@P_STU_ID=" + Request.QueryString("stu_id")

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            With ds.Tables(0).Rows(0)
                If .Item("TOT_ATT_MRKD") <> 0 Then
                    percent = .Item("TOT_PRS") * 100 / .Item("TOT_ATT_MRKD")
                End If
            End With

            Dim sChart As New StringBuilder
            sChart.AppendLine("<chart  lowerLimit='0' upperLimit='100'   gaugeStartAngle='180' gaugeEndAngle='0' palette='1' numberSuffix='%' tickValueDistance='20' showValue='1'>")
            sChart.AppendLine("<colorRange><color minValue='0' maxValue='75' code='FF654F'/><color minValue='75' maxValue='90' code='F6BD0F'/><color minValue='90' maxValue='100' code='8BBA00'/></colorRange>")
            sChart.AppendLine("<dials><dial value='" + Math.Round(percent).ToString + "' /> </dials>")
            sChart.AppendLine("</chart>")
            Literal2.Text = FusionCharts.RenderChart("FusionCharts/AngularGauge.swf", "", sChart.ToString, "myFirst", "120", "100", False, False)
        Catch ex As Exception
        End Try
    End Sub

End Class
