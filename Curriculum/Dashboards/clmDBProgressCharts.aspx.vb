﻿Imports InfosoftGlobal
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Curriculum_DashBoards_clmDBProgressCharts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblHdr.Text = "Grade " + Request.QueryString("grd_id") + " " + Request.QueryString("subject")
        getRpf_Id()
        PoplateCharts()
    End Sub

    Sub getRpf_Id()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("SBSUID") = "123004" Then
            str_query = "exec DHB.getRPFID_DMHS " _
                                   & Request.QueryString("hdr_id") + "," _
                                   & Request.QueryString("acd_id") + "," _
                                   & "'" + Request.QueryString("grd_id") + "'"

            hfRPF_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Else
            str_query = "exec DHB.CBSE_getRPFID_RSDID " _
                                  & Request.QueryString("hdr_id") + "," _
                                  & Request.QueryString("acd_id") + "," _
                                  & "'" + Request.QueryString("grd_id") + "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                hfRPF_ID.Value = ds.Tables(0).Rows(0).Item(0)
                hfRSD_ID.Value = ds.Tables(0).Rows(0).Item(1)
            End If
        End If


    End Sub

    Sub PoplateCharts()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("SBSUID") = "123004" Then
            str_query = "exec [DHB].[STUDENTPROGRESSANALYSIS_DMHS] " _
                                     & hfRPF_ID.Value + "," _
                                     & Request.QueryString("acd_id") + "," _
                                     & "'" + Request.QueryString("grd_id") + "'," _
                                     & Request.QueryString("sbg_id") + "," _
                                     & "0,''"


        Else
            str_query = "exec [DHB].[CBSE_STUDENTPROGRESSANALYSIS] " _
                               & hfRPF_ID.Value + "," _
                               & Request.QueryString("acd_id") + "," _
                               & "'" + Request.QueryString("grd_id") + "'," _
                               & Request.QueryString("sbg_id") + "," _
                               & "0,''," _
                               & +hfRSD_ID.Value
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim sChart As New StringBuilder
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)

                sChart.AppendLine("<chart  gaugeOuterRadius='70'  gaugeInnerRadius='30'  lowerLimit='0' upperLimit='100'   gaugeStartAngle='180' gaugeEndAngle='0' palette='3'  tickValueDistance='12' showValue='1'>")
                sChart.AppendLine("<colorRange><color minValue='0' maxValue='40' code='FF654F'/><color minValue='40' maxValue='80' code='F6BD0F'/><color minValue='80' maxValue='100' code='8BBA00'/></colorRange>")
                sChart.AppendLine("<dials><dial value='" + ds.Tables(0).Rows(0).Item(0).ToString + "' valueX='95' ValueY='55' /> </dials>")
                sChart.AppendLine("<styles> <definition><style type='font' name='myValueFont' bold='1' size='18'/> </definition>")
                sChart.AppendLine("<application><apply toObject='Value' styles='myValueFont'/></application></styles>")
                sChart.AppendLine("</chart>")
                Literal1.Text = FusionCharts.RenderChart("FusionCharts/AngularGauge.swf", "", sChart.ToString, "chart1", "180", "120", False, False)

                sChart = New StringBuilder
                sChart.AppendLine("<chart  gaugeOuterRadius='70'  gaugeInnerRadius='30' lowerLimit='0' upperLimit='100'   gaugeStartAngle='180' gaugeEndAngle='0' palette='3'  tickValueDistance='12' showValue='1'>")
                sChart.AppendLine("<colorRange><color minValue='0' maxValue='40' code='FF654F'/><color minValue='40' maxValue='80' code='F6BD0F'/><color minValue='80' maxValue='100' code='8BBA00'/></colorRange>")
                sChart.AppendLine("<dials><dial value='" + ds.Tables(0).Rows(0).Item(1).ToString + "' valueX='95' ValueY='55' /> </dials>")
                sChart.AppendLine("<styles> <definition><style type='font' name='myValueFont' bold='1' size='18'/> </definition>")
                sChart.AppendLine("<application><apply toObject='Value' styles='myValueFont'/></application></styles>")
                sChart.AppendLine("</chart>")
                Literal2.Text = FusionCharts.RenderChart("FusionCharts/AngularGauge.swf", "", sChart.ToString, "chart2", "180", "120", False, False)

                lblAbove80.Text = .Item(2).ToString + "%"
                lblAbove60.Text = .Item(3).ToString + "%"
                lblAbove40.Text = .Item(4).ToString + "%"
                lblBelow40.Text = .Item(5).ToString + "%"
                lblBelow20.Text = .Item(6).ToString + "%"
            End With

        End If
        If Session("SBSUID") = "123004" Then

            str_query = "exec [DHB].[STUDENTPROGRESSANALYSIS_DMHS] " _
                                & hfRPF_ID.Value + "," _
                                & Request.QueryString("acd_id") + "," _
                                & "'" + Request.QueryString("grd_id") + "'," _
                                & Request.QueryString("sbg_id") + "," _
                                & Request.QueryString("sct_id") + "," _
                                & "'" + Request.QueryString("stu_ids") + "'"

        Else

            str_query = "exec [DHB].[CBSE_STUDENTPROGRESSANALYSIS] " _
                              & hfRPF_ID.Value + "," _
                              & Request.QueryString("acd_id") + "," _
                              & "'" + Request.QueryString("grd_id") + "'," _
                              & Request.QueryString("sbg_id") + "," _
                              & Request.QueryString("sct_id") + "," _
                              & "'" + Request.QueryString("stu_ids") + "'," _
                              & +hfRSD_ID.Value

        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                sChart = New StringBuilder
                sChart.AppendLine("<chart   gaugeOuterRadius='70'  gaugeInnerRadius='30' lowerLimit='0' upperLimit='100'   gaugeStartAngle='180' gaugeEndAngle='0' palette='3'  tickValueDistance='12' showValue='1'>")
                sChart.AppendLine("<colorRange><color minValue='0' maxValue='40' code='FF654F'/><color minValue='40' maxValue='80' code='F6BD0F'/><color minValue='80' maxValue='100' code='8BBA00'/></colorRange>")
                sChart.AppendLine("<dials><dial value='" + ds.Tables(0).Rows(0).Item(0).ToString + "' valueX='95' ValueY='55' /> </dials>")
                sChart.AppendLine("<styles> <definition><style type='font' name='myValueFont' bold='1' size='18'/> </definition>")
                sChart.AppendLine("<application><apply toObject='Value' styles='myValueFont'/></application></styles>")
                sChart.AppendLine("</chart>")
                Literal3.Text = FusionCharts.RenderChart("FusionCharts/AngularGauge.swf", "", sChart.ToString, "chart3", "180", "120", False, False)

                sChart = New StringBuilder
                sChart.AppendLine("<chart  gaugeOuterRadius='70'  gaugeInnerRadius='30' lowerLimit='0' upperLimit='100'   gaugeStartAngle='180' gaugeEndAngle='0' palette='3'  tickValueDistance='12' showValue='1'>")
                sChart.AppendLine("<colorRange><color minValue='0' maxValue='40' code='FF654F'/><color minValue='40' maxValue='80' code='F6BD0F'/><color minValue='80' maxValue='100' code='8BBA00'/></colorRange>")
                sChart.AppendLine("<dials><dial value='" + ds.Tables(0).Rows(0).Item(1).ToString + "' valueX='95' ValueY='55' /> </dials>")
                sChart.AppendLine("<styles> <definition><style type='font' name='myValueFont' bold='1' size='18'/> </definition>")
                sChart.AppendLine("<application><apply toObject='Value' styles='myValueFont'/></application></styles>")
                sChart.AppendLine("</chart>")
                Literal4.Text = FusionCharts.RenderChart("FusionCharts/AngularGauge.swf", "", sChart.ToString, "chart4", "180", "120", False, False)

                lblAbove80_1.Text = .Item(2).ToString + "%"
                lblAbove60_1.Text = .Item(3).ToString + "%"
                lblAbove40_1.Text = .Item(4).ToString + "%"
                lblBelow40_1.Text = .Item(5).ToString + "%"
                lblBelow20_1.Text = .Item(6).ToString + "%"

            End With

        End If



    End Sub
End Class
