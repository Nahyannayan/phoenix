﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmDBStudentCharts.aspx.vb"
    Inherits="Curriculum_DashBoards_clmDBStudentCharts" %>
 <link href="../../cssfiles/curriculum.css" rel="stylesheet" type="text/css" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <script type="text/javascript" src="FusionCharts/FusionCharts.js"></script>
<script type="text/javascript" src="FusionCharts/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div >
    <table ><tr><td  width="800px" align="center" ><br />
        <table border="1"  cellpadding="0" cellspacing="0" class="tableChart"  >
            <tr>
                <td  Width="120px" class="tdChart">
                    <asp:Image ID="imgStudent" runat="server" Height="132px" Width="120px" />
                </td>
                <td class="tdChart" valign="top" >
                    <table cellpadding="5" cellspacing="5" border="0" class="tableNoborder">
                        <tr><td> &nbsp;</td></tr>
                        <tr>
                            <td align="left"  class="chartFields">
                                Student ID
                            </td>
                            <td   class="chartFields">
                                :
                            </td>
                            <td   class="chartFields">
                                <asp:Label align="left" ID="lblStudentID" runat="server" ></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td align="left" class="chartFields">
                                Student Name
                            </td>
                            <td   class="chartFields">
                                :
                            </td>
                            <td   class="chartFields">
                                <asp:Label align="left" ID="lblName" runat="server" ></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td align="left"   class="chartFields">
                                Grade & Section
                            </td>
                            <td   class="chartFields">
                                :
                            </td>
                            <td   class="chartFields">
                                <asp:Label align="left" ID="lblGrade" runat="server" ></asp:Label>
                            </td>
                        </tr>
                       
                    </table>
                </td>
            
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
        </td></tr></table>
    </div>
    </form>
</body>
</html>
