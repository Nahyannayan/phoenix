﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports ResponseHelper
Imports CURRICULUM
Partial Class Curriculum_DashBoards_clmDBStudentDetails_MHS_View
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")



                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400225" And ViewState("MainMnu_code") <> "C400226") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                   
                End If

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
             


                lnkClear.Visible = False

                If ViewState("MainMnu_code") = "C400225" Then
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    trGroup.Visible = False

                    PopulateGrade(ddlAcademicYear.SelectedValue.ToString)
                    PopulateSection(ddlAcademicYear.SelectedValue.ToString)
                ElseIf ViewState("MainMnu_code") = "C400226" Then
                    trSection.Visible = False
                    trStudent.Visible = False
                    chkSen.Visible = False

                    If Session("CurrSuperUser") = "Y" Then
                        PopulateGrade(ddlAcademicYear.SelectedValue.ToString)
                    Else
                        PopulateGradeByTeacher(ddlAcademicYear.SelectedValue.ToString)
                    End If
                End If

                If Session("CurrSuperUser") = "Y" Then
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                Else
                    chkSen.Visible = False
                    ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                End If

                If ViewState("MainMnu_code") = "C400226" Then
                    PopulateGroups()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
            lnkClear.Visible = True
            grdStudent.Visible = True
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub


    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub
    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Public Sub PopulateGrade(ByVal acdid As String)
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                           & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                           & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                           & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and "

            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND grm_grd_id IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_ACD_ID=" + acdid + " AND GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')) " _
                         & ")"
            End If

            str_query += "  grm_acd_id=" + acdid + " order by grd_displayorder"
        Else
            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                      & " grade_bsu_m inner join grade_m on grm_grd_id=grd_id " _
                      & " inner join stream_m on grm_stm_id=stm_id " _
                      & " inner join section_m on sct_grm_id=grm_id " _
                      & " WHERE  grm_acd_id=" + acdid + " and sct_emp_id=" + Session("EMPLOYEEID")


        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

    End Sub

    Public Sub PopulateGradeByTeacher(ByVal acdid As String)
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                       & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                       & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                       & " and grade_bsu_m.grm_stm_id=stream_m.stm_id  "

        str_query += " AND grm_grd_id IN( SELECT SGR_GRD_ID FROM GROUPS_M INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID WHERE SGR_ACd_ID=" + acdid _
                     & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") + " AND SGS_TODATE IS NULL" _
                     & ")"
        
        str_query += " and grm_acd_id=" + acdid + " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

    End Sub


    Sub PopulateSection(ByVal acdid As String)
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M AS A " _
                         & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                         & " WHERE GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) _
                         & " AND GRM_ACD_ID=" + acdid _
                         & " ORDER BY SCT_DESCR"
        Else
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) _
                       & " AND GRM_ACD_ID=" + acdid _
                       & " AND SCT_EMP_ID=" + Session("EMPLOYEEID") _
                       & " ORDER BY SCT_DESCR"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub

    Function getColumns() As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim strClms As String = ""

        Dim i As Integer
        For i = 0 To lstSelect1.Items.Count - 1
            With lstSelect1.Items
                If .Item(i).Selected = True Then
                    If strClms <> "" Then
                        strClms += "|"
                    End If
                    strClms += .Item(i).Value
                End If
            End With
        Next

        For i = 0 To lstSelect3.Items.Count - 1
            With lstSelect3.Items
                If .Item(i).Selected = True Then
                    If strClms <> "" Then
                        strClms += "|"
                    End If
                    strClms += .Item(i).Value
                End If
            End With
        Next

        If strClms = "" Then
            strClms = "ALL"
        End If

        str_query = "EXEC  DHB.getSTUDENTDETAILS_DMHS_COLUMNS " _
              & "'" + ddlAcademicYear.SelectedValue.ToString + "'," _
              & "'" + lstSelect2.Items(0).Selected.ToString + "'," _
              & "'" + lstSelect2.Items(1).Selected.ToString + "'," _
              & "'" + lstSelect2.Items(2).Selected.ToString + "'," _
              & "'" + strClms + "'"

        Dim columns As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return columns
    End Function


    Sub PopulateGroups()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M WHERE SGR_SBG_ID=" + ddlSubject.SelectedValue.ToString _
                       & " ORDER BY SGR_DESCR"
        Else
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID " _
                     & " WHERE SGS_TODATE IS NULL AND SGR_SBG_ID=" + ddlSubject.SelectedValue.ToString
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()

    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        If ViewState("MainMnu_code") = "C400225" Then
            PopulateGrade(ddlAcademicYear.SelectedValue.ToString)
            PopulateSection(ddlAcademicYear.SelectedValue.ToString)
        Else
            If Session("CurrSuperUser") = "Y" Then
                PopulateGrade(ddlAcademicYear.SelectedValue.ToString)
            Else
                PopulateGradeByTeacher(ddlAcademicYear.SelectedValue.ToString)
            End If
        End If

        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        If ViewState("MainMnu_code") = "C400226" Then
            PopulateGroups()
        End If

        h_STU_IDs.Value = ""
        grdStudent.Visible = False
        lnkClear.Visible = False
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        If ViewState("MainMnu_code") = "C400226" Then
            PopulateGroups()
        Else
            PopulateSection(ddlAcademicYear.SelectedValue.ToString)
        End If


        h_STU_IDs.Value = ""
        grdStudent.Visible = False
        lnkClear.Visible = False
    End Sub

    Sub GetSENStudents()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = " SELECT isnull(STUFF((SELECT '|'+ CONVERT(varchar(100),stu_id) " _
                                & " FROM STUDENT_M INNER JOIN OASIS..STUDENT_PROMO_S ON STU_ID=STP_STU_ID" _
                                & " WHERE STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND STP_GRD_ID='" + grade(0) + "' AND STP_STM_ID='" + grade(1) + "'" _
                                & " AND ISNULL(STU_bSEN_KHDA,0)=1 for xml path('')),1,1,''),'0')"
        h_STU_IDs.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Dim url As String
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim columns As String = getColumns()
        Session("htHeader") = Nothing
        Dim section, group As String

        If ViewState("MainMnu_code") = "C400225" Then
            section = ddlSection.SelectedValue.ToString
            group = "0"
        Else
            section = "0"
            group = ddlGroup.SelectedValue.ToString
        End If

        If chkSen.Checked = True Then
            GetSENStudents()
        End If

        If h_STU_IDs.Value = "0" Then
            h_STU_IDs.Value = ""
            lblError.Text = "No Records !!!"
            Exit Sub
        End If
        Dim strRedirect As String = "~/Curriculum/DashBoards/clmDbStudentDetails_MHS.aspx"
        url = String.Format("{0}?&grade={1}&section={2}&acdid={3}&sbgid={4}&subject={5}&stuids={6}&columns={7}&group={8}", _
                            strRedirect, grade(0), section, ddlAcademicYear.SelectedValue.ToString, _
                           ddlSubject.SelectedValue.ToString, ddlSubject.SelectedItem.Text, h_STU_IDs.Value.Replace("___", "|"), columns, group)
        ResponseHelper.Redirect(url, "_blank", "")


    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        h_STU_IDs.Value = ""
        grdStudent.Visible = False
        lnkClear.Visible = False
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        PopulateGroups()
    End Sub

    Protected Sub chkSen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSen.CheckedChanged
        If chkSen.Checked = True Then
            trSection.Visible = False
            trStudent.Visible = False
        Else
            trSection.Visible = True
            trStudent.Visible = True
        End If
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If
    End Sub
End Class
