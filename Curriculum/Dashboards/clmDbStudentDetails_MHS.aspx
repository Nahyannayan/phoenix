﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmDbStudentDetails_MHS.aspx.vb"
    Inherits="Curriculum_DashBoards_clmDbStudentDetails_MHS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../../cssfiles/curriculum.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Master Tracker</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />


</script>
</head>
<body>

    <form id="form1" runat="server">

    <script language="javascript" type="text/javascript">

        function ShowPopup(queryStr, filetype, titleStr) {

            document.getElementById('<%=lblTitle.ClientID %>').innerText = titleStr;
          
            var iFrm = document.getElementById("ifDB");
            if (filetype == "charts") {
                iFrm.src = "clmDBStudentCharts.aspx?" + queryStr;
            }
            else if (filetype == "progresscharts") {
                iFrm.src = "clmDBProgressCharts.aspx?" + queryStr;
            }
            else if (filetype == "gradecharts") {
            iFrm.src = "clmDbSubjectGradeAnalysisCharts.aspx?" + queryStr;
            }
            else if (filetype == "docupload") {
            iFrm.src = "clmDBSENDocUploads.aspx?" + queryStr;
           }
        else if (filetype == "studprofile") {
        iFrm.src = "clmDBStudProfile.aspx?" + queryStr;
              }
            else {
                iFrm.src = "clmDBStudentReports.aspx?" + queryStr;
            }

            var modalPopupBehaviorCtrl = $find('ModalPopupBehaviorID');
            modalPopupBehaviorCtrl.show();
          

        }
    
    </script>

    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
        <ajaxToolkit:ModalPopupExtender ID="mpe" OnOkScript="onPrvOk();" CancelControlID="ImageButton1"
            DynamicServicePath="" runat="server" Enabled="True" TargetControlID="btnTarget"
            BehaviorID="ModalPopupBehaviorID" PopupControlID="plPrev" BackgroundCssClass="modalBackground" />
        
                <asp:Button ID="btnTarget" runat="server" Text="Button" Style="display: none" />
        <div id="plPrev" runat="server" class="panel-cover" style="width:835px;height:500px">
            <div width="100%">
                <div class="msgClose" style="  vertical-align: top;">
                    <table width="100%" border="0">
                        <tr>
                            <td>                              
                                <asp:Label ID="lblTitle" CssClass="lblTitle"  runat="server"></asp:Label>
                            </td>
                            <td align="right" valign="top">
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/Curriculum/btnCloseblack.png" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <asp:Panel ID="plPrev1" runat="server" BackColor="White" ScrollBars="vertical" Height="500px"
                Width="100%">
                <iframe id="ifDB" height="100%" src="clmDBStudentReports.aspx" scrolling="yes" marginwidth="0px"
                    frameborder="0" width="100%"></iframe>
            </asp:Panel>
        </div>
    </div>
    <div>
        <table cellpadding="5" celspacing="5">
            <tr>
                <td>
                    <asp:GridView ID="gvStud" runat="server" CssClass="gridstyle">
                        <RowStyle Wrap="false" CssClass="gridrowstyle1" />
                        <AlternatingRowStyle CssClass="gridrowstyle2" />
                        <HeaderStyle VerticalAlign="Bottom" HorizontalAlign="Center" CssClass="gridheader" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hfSTU_IDS" runat="server" />
    <asp:HiddenField ID="hfACD_ID" runat="server" />
    <asp:HiddenField ID="hfGRD_ID" runat="server" />
    <asp:HiddenField ID="hfSCT_ID" runat="server" />
    <asp:HiddenField ID="hfSBG_ID" runat="server" />
    <asp:HiddenField ID="hfSubject" runat="server" />
    <asp:HiddenField ID="hfYEAR1" runat="server" />
    <asp:HiddenField ID="hfYEAR2" runat="server" />
    <asp:HiddenField ID="hfYEAR3" runat="server" />
    <asp:HiddenField ID="hfColumns" runat="server" />
    <br />
    <asp:HiddenField ID="hfSGR_ID" runat="server" />
    </form>
</body>
</html>
<%--VerticalAlign="Bottom" HorizontalAlign="Center" Width="20px"--%>