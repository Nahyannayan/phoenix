﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmDBProgressCharts.aspx.vb"
    Inherits="Curriculum_DashBoards_clmDBProgressCharts" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../../cssfiles/curriculum.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <script type="text/javascript" src="FusionCharts/FusionCharts.js"></script>

    <script type="text/javascript" src="FusionCharts/jquery.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" width="800px">
            <tr>
                <td valign="top" width="50%">
                    <br />
                    <fieldset  style="height:415px">
                        <legend style="color: black; font-weight: bold;font-family:verdana;font-size:8pt"><asp:Label ID="lblHdr" runat="server"></asp:Label></legend>
                        <table border="1" cellpadding="10" cellspacing="10" class="tableChart">
                            <tr>
                                <td class="tdChart" align="center">
                                    <table cellpadding="5" cellspacing="5">
                                        <tr>
                                            <td class="chartFields">
                                                Average
                                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                            </td>
                                            <td class="chartFields">
                                                Highest<asp:Literal ID="Literal2" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="10" cellspacing="10" border="1" class="tableChart">
                                        <tr>
                                            <td height="40px" class="tdChart" valign="top">
                                                <asp:Label ID="lblText1Above80" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"
                                                    Text="Well Above Curriculum Expectations" ForeColor="#2299B3" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <asp:Label ID="lblAbove80" Font-Size="14pt"  Font-Bold="true" Font-Names="Verdana" ForeColor="#7CBD47"
                                                    runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <asp:Label ID="lblTextAbove80" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#2299B3"
                                                    Text=">=80%" runat="server" ></asp:Label>
                                            </td>
                                            <td height="40px" class="tdChart" valign="top">
                                                <asp:Label ID="lblText1Above60" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="Above Curriculum Expectations" runat="server"></asp:Label>
                                                 <br />
                                                <br />
                                                <asp:Label ID="lblAbove60" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#7CBD47"
                                                    runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <asp:Label ID="lblTextAbove60" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text=">=60%" runat="server"></asp:Label>
                                               
                                            </td>
                                            <td height="40px" class="tdChart" valign="top">
                                            <asp:Label ID="lblText1Above40" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="In line with curriculum expectations" runat="server"></asp:Label>
                                                <br />
                                                 <br />
                                                <asp:Label ID="lblAbove40" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#C69B1B"
                                                    runat="server"></asp:Label>
                                                <br />
                                                 <br />
                                                <asp:Label ID="lblTextAbove40" Font-Size="XX-Small" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text=">=40%" runat="server"></asp:Label>
                                               
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdChart" valign="top">
                                               <asp:Label ID="lblText1Below40" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="Below curriculum expectations" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <asp:Label ID="lblBelow40" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#F6C380"
                                                    runat="server"></asp:Label>
                                                <br /><br />
                                                <asp:Label ID="lblTextBelow40" Font-Size="XX-Small" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="<40%" runat="server"></asp:Label>
                                               
                                            </td>
                                            <td class="tdChart" valign="top">
                                                <asp:Label ID="lblText1Below20" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="Well below curriculum expectations" runat="server"></asp:Label>
                                                <br /><br />
                                                <asp:Label ID="lblBelow20" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#F6C380"
                                                    runat="server"></asp:Label>
                                                <br /><br />
                                                <asp:Label ID="lblTextBelow20" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="<20%" runat="server"></asp:Label>
                                              
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td valign="top"> <br />
                    <fieldset style="height:415px">
                        <legend style="color: black; font-weight: bold;font-family:verdana;font-size:8pt">Selected Students</legend>
                        <table border="1" cellpadding="10" cellspacing="10" class="tableChart">
                            <tr>
                                <td class="tdChart" align="center" >
                                    <table cellpadding="5" cellspacing="5">
                                        <tr>
                                            <td class="chartFields">
                                                Average
                                                <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                                            </td>
                                            <td class="chartFields">
                                                Highest
                                                <asp:Literal ID="Literal4" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                            </tr>
                            <tr>
                                <td>
                                     <table cellpadding="10" cellspacing="10" border="1" class="tableChart">
                                        <tr>
                                            <td height="40px" class="tdChart">
                                                <asp:Label ID="lblText1Above80_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="Well Above Curriculum Expectations" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <asp:Label ID="lblAbove80_1" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#7CBD47"
                                                    runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <asp:Label ID="lblTextAbove80_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text=">=80%" runat="server"></asp:Label>
                                            </td>
                                            <td height="40px" class="tdChart">
                                                <asp:Label ID="lblText1Above60_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="Above Curriculum Expectations" runat="server"></asp:Label>
                                              <br />
                                                <br />
                                                <asp:Label ID="lblAbove60_1" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#7CBD47"
                                                    runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <asp:Label ID="lblTextAbove60_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text=">=60%" runat="server"></asp:Label>
                                               
                                            </td>
                                            <td height="40px" class="tdChart">
                                            <asp:Label ID="lblText1Above40_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="In line with curriculum expectations" runat="server"></asp:Label>
                                                <br />
                                                 <br />
                                                <asp:Label ID="lblAbove40_1" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#C69B1B"
                                                    runat="server"></asp:Label>
                                                <br />
                                                 <br />
                                                <asp:Label ID="lblTextAbove40_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text=">=40%" runat="server"></asp:Label>
                                               
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdChart">
                                               <asp:Label ID="lblText1Below40_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="Below curriculum expectations" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <asp:Label ID="lblBelow40_1" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#F6C380"
                                                    runat="server"></asp:Label>
                                                <br /><br />
                                                <asp:Label ID="lblTextBelow40_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="<40%" runat="server"></asp:Label>
                                               
                                            </td>
                                            <td class="tdChart">
                                                <asp:Label ID="lblText1Below20_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="Well below curriculum expectations" runat="server"></asp:Label>
                                                <br /><br />
                                                <asp:Label ID="lblBelow20_1" Font-Size="14pt" Font-Bold="true" Font-Names="Verdana" ForeColor="#F6C380"
                                                    runat="server"></asp:Label>
                                                <br /><br />
                                                <asp:Label ID="lblTextBelow20_1" Font-Size="6pt" Font-Bold="true" Font-Names="Verdana"  ForeColor="#2299B3"
                                                    Text="<20%" runat="server"></asp:Label>
                                              
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hfRPF_ID" runat="server" />
    <asp:HiddenField ID="hfRSD_ID" runat="server" />
    </form>
</body>
</html>
