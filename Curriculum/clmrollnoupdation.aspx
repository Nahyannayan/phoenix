<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmrollnoupdation.aspx.vb" Inherits="Curriculum_clmrollnoupdation" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Update Student Board Registration Number
        </div>
        <div class="card-body">
            <div class="table-responsive ">

                <table class="matters" align="center" style="border-collapse: collapse" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="20">
                        <td align="left" valign="middle">
                            <span class="field-label">Grade</span></td>

                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                        <td align="left" valign="middle">
                            <span class="field-label">Section</span>
                        </td>

                        <td align="left" colspan="1" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="6">


                            <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="0"
                                Style="width: 100%;">
                                <ajaxToolkit:TabPanel ID="HT1" runat="server" HeaderText="Roll No Updation">
                                    <ContentTemplate>
                                        <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                                            cellspacing="0" width="100%">
                                            <tr runat="server">
                                                <td runat="server">
                                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                                        SkinID="error" Style="text-align: center"></asp:Label></td>
                                            </tr>

                                            <tr runat="server">
                                                <td align="left" runat="server">
                                                    <asp:GridView ID="gvComments" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row"
                                                        EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="15" Width="100%">
                                                        <RowStyle CssClass="griditem" Height="25px" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="CMTID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="cmtId" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Student ID">

                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Student ID"></asp:Label><br />

                                                                    <asp:TextBox ID="txtOption" runat="server"></asp:TextBox>

                                                                    <asp:ImageButton ID="btnEmpid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEmpid_Search_Click" />

                                                                </HeaderTemplate>

                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSubId" runat="server" Text='<%# Bind("STU_no") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblopt1" runat="server" CssClass="gridheader_text" Text="Name"></asp:Label><br />

                                                                    <asp:TextBox ID="txtOption1" runat="server"></asp:TextBox>

                                                                    <asp:ImageButton ID="btnstuname_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnstuname_Search_Click" />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("STU_name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle></HeaderStyle>
                                                                <ItemStyle></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Roll No">

                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="lblclm" runat="server" Text='<%# Bind("stu_regno") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="Update" Text="Update" HeaderText="Update">
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <SelectedRowStyle CssClass="Green" />
                                                        <HeaderStyle CssClass="gridheader_pop" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </ContentTemplate>

                                </ajaxToolkit:TabPanel>






                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Upload">

                                    <ContentTemplate>
                                        <table align="center" border="0" cellpadding="0"
                                            cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <table width="100%" id="Table3"  cellpadding="0" cellspacing="0" style="border-collapse: collapse">
                                                        <tr>
                                                            <td colspan="3">
                                                                <tr id="Tr1" runat="server">
                                                                    <td id="Td1" runat="server" colspan="3">
                                                                        <asp:Label ID="Label1" runat="server" Text="Please upload the excel with column names as StudentID,StudentName,RollNo" CssClass="alert-info" EnableViewState="False"
                                                                           Style="text-align: center"></asp:Label></td>
                                                                </tr>
                                                                <asp:Label ID="lblerror3" runat="server" CssClass="error" EnableViewState="False"
                                                                    SkinID="error" Style="text-align: left"></asp:Label></td>
                                                        </tr>

                                                        
                                                                <td align="left">
                                                                    <span class="field-label">Select File</span>
                                                                </td>

                                                                <td align="left">
                                                                    <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True"
                                                                        Width="204px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" align="center">
                                                                    <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload"
                                                                        Width="101px" OnClick="btnUpload_Click" CausesValidation="False" />
                                                                </td>
                                                            </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>





                                    </ContentTemplate>

                                </ajaxToolkit:TabPanel>









                            </ajaxToolkit:TabContainer>

                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

</asp:Content>

