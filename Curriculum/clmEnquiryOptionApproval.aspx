﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmEnquiryOptionApproval.aspx.vb" Inherits="Curriculum_clmStudOptionApproval"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="js/jquery.min.js" type="text/javascript"></script>

    <script src="js/jquery.tabSlideOut.v1.3.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $('.slide-out-div').tabSlideOut({
                tabHandle: '.handle',                     //class of the element that will become your tab
                pathToTabImage: '../images/tabmenu/counttab.png', //path to the image for the tab //Optionally can be set using css
                imageHeight: '122px',                     //height of tab image           //Optionally can be set using css
                imageWidth: '40px',                       //width of tab image            //Optionally can be set using css
                tabLocation: 'right',                      //side of screen where tab lives, top, right, bottom, or left
                speed: 300,                               //speed of animation
                action: 'click',                          //options: 'click' or 'hover', action to trigger animation
                topPos: '0px',                          //position from the top/ use if tabLocation is left or right
                leftPos: '0px',                          //position from left/ use if tabLocation is bottom or top
                fixedPosition: true                       //options: true makes it stick(fixed position) on scroll
            });

        });

    </script>

    <style type="text/css">
        .slide-out-div {
            padding: 20px;
            width: 83%;
            height: 500px;
            background: #fff;
            border: 1px solid #29216d;
    </style>

    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvEnquiry.ClientID%>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }



        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1 && document.forms[0].elements[i].disabled == false) {
                    //if (document.forms[0].elements[i].type=='checkbox' )  
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Enquiry Option Interview 
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server"  width="100%">
                    <tr>
                        <td align="center" class="matters"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" width="100%">
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Select Academic Year</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                              >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Select Grade</span>
                                    </td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                            >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" colspan="4">
                                        <asp:DataList ID="dlOptions" runat="server" RepeatColumns="10" RepeatDirection="Horizontal"
                                              Visible="false">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="lblOption" runat="server" Text='<%# Bind("OPT_DESCR") %>'  
                                                                Style="text-decoration: none; color: Green"></asp:LinkButton>
                                                            <asp:Label ID="lblOptID" runat="server" Text='<%# Bind("OPT_ID") %>' Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvOptions" runat="server" AutoGenerateColumns="false" SkinID="GridViewRed">
                                                                <Columns>
                                                                    <asp:BoundField DataField="SBG_DESCR" HeaderText="Subjects">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="REQ_COUNT_E" HeaderText="Requested Applicants">
                                                                        <ItemStyle HorizontalAlign="center"  />
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="ALT_COUNT_E" HeaderText="Alloted Applicants">
                                                                        <ItemStyle HorizontalAlign="center"  />
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="REQ_COUNT_S" HeaderText="Requested Students">
                                                                        <ItemStyle HorizontalAlign="center"  />
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="ALT_COUNT_S" HeaderText="Alloted Students">
                                                                        <ItemStyle HorizontalAlign="center"  />
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="Total Requested">
                                                                        <ItemStyle HorizontalAlign="center"  />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblReq" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Total Allotted">
                                                                        <ItemStyle HorizontalAlign="center"  />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAllot" runat="server" ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                                                OffsetX="40" OffsetY="20" PopDelay="50" PopupControlID="gvOptions" PopupPosition="Left"
                                                                TargetControlID="lblOption">
                                                            </ajaxToolkit:HoverMenuExtender>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                        <asp:Literal ID="ltMarquee" runat="server"></asp:Literal>
                                    </td>


                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Appl No</span>
                                    </td>

                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtApplNo" runat="server"></asp:TextBox>	
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Applicant Name</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtApplName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" colspan="4">
                                        <asp:RadioButton ID="rdAllOption" runat="server" BorderStyle="None" Checked="True" CssClass="field-label"
                                            GroupName="G1" Text="All Options" AutoPostBack="true" />
                                        <asp:RadioButton ID="rdSubject" runat="server" GroupName="G1" Text="Filter By Subject" CssClass="field-label"
                                            AutoPostBack="true" />
                                        <asp:RadioButton ID="rdOption" runat="server" GroupName="G1" Text="Filter By Option" CssClass="field-label"
                                            AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr id="trSubject" runat="server">
                                    <td align="left" class="matters"><span class="field-label">Select Subject</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlSubject1" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Select Option</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlOption1" runat="server"  >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trOption" runat="server">
                                    <td align="left" class="matters"><span class="field-label">Select Option</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlOption2" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Select Subject</span>
                                    </td>
 
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlSubject2" runat="server"  >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"  colspan="4">
                            <asp:RadioButton ID="rdNotAlloted" runat="server" GroupName="G2" Text="Not Interviewed" CssClass="field-label"
                                Checked="True" />
                                        <asp:RadioButton ID="rdAlloted" runat="server" GroupName="G2" Text="Approved" CssClass="field-label" />
                                        <asp:RadioButton ID="rdReject" runat="server" GroupName="G2" Text="Rejected" CssClass="field-label" />
                                        <asp:RadioButton ID="rdOnHold" runat="server" GroupName="G2" Text="On Hold  for Non Payment" CssClass="field-label" />
                                    </td>
                                    </tr>
                                <tr>
                                    <td class="matters" colspan="2">
                                        <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Not Requested Applicants" CssClass="field-label"
                                            BorderStyle="None" />
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Show Records</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtRecords" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" colspan="4" align="center">
                                   
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                                         />
                                    </td>
                                </tr>
                                <tr id="trGrd" runat="server">
                                    <td colspan="4" align="center" style="text-align:center">
                                        <asp:Label ID="lblGrdError" runat="server" CssClass="error" ></asp:Label>
                                        <asp:GridView ID="gvEnquiry" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                          PageSize="20"   BorderStyle="None">
                                            <RowStyle CssClass="griditem"   Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <HeaderStyle   CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                    Select<br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                   
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);"
                                                            Enabled='<%# Bind("bENABLE") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="stu_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("EQS_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SL.No">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNoView() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Appl No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblApplNo" runat="server" Text='<%# Bind("EQS_APPLNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblName" runat="server" Text='<%# Bind("ENQ_NAME") %>' Style="text-decoration: none;"
                                                            ForeColor="#1B80B6"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Level/Grade" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltLevel" runat="server"></asp:Literal>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section" Visible="false">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H1" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H1" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt1" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption1"  Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H2" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H2" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt2" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption2"   Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H3" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H3" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt3" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption3"   Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H4" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H4" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt4" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption4"   Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H5" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H5" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt5" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption5"  Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H6" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H6" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt6" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption6"   Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H7" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H7" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt7" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption7"  Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H8" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H8" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt8" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption8"  Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H9" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H9" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt9" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption9"  Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H10" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H10" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt10" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption10"  Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H11" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H11" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt11" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption11"  Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOpt_H12" runat="server"></asp:Label>
                                                        <asp:Label ID="lblOptId_H13" Visible="false" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOpt12" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlOption12"  Visible="false" runat="server">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="REMARKS">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtRemarks" Text='<%# Bind("REMARKS") %>' TextMode="multiLine" Enabled='<%# Bind("bENABLE") %>' Width="100%"
                                                            SkinID="multitext" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Edit Request"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click" Enabled='<%# Bind("bENABLE") %>'
                                                            Text="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle  ></HeaderStyle>
                                        </asp:GridView>
                                        <asp:HiddenField ID="hfTotalOptions" runat="server" />
                                        <asp:HiddenField ID="hfGRD_ID" runat="server" />
                                        <asp:HiddenField ID="hfOptionFilter" runat="server" />
                                        <br />
                                        <asp:HiddenField ID="hfSubjectFilter" runat="server" />
                                    </td>
                                </tr>
                                <tr id="trApr2" runat="server">
                                    <td colspan="4" align="left">
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="button" TabIndex="4"
                                             />
                                        <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="button" TabIndex="4"
                                         />
                                        <asp:Button ID="btnOnHold" runat="server" Text="On Hold" CssClass="button" TabIndex="4"
                                            />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="slide-out-div">
                    <a class="handle" href=""></a>
                    <div style="overflow: scroll;  >
                        <asp:GridView ID="gvSubjectCounts" runat="server" AutoGenerateColumns="false"  CssClass="table table-bordered table-row">
                            <RowStyle   />
                            <Columns>
                                <asp:BoundField DataField="SBG_DESCR" HeaderText="Subjects">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="REQ_COUNT_E" HeaderText="Requested Applicants">
                                    <ItemStyle HorizontalAlign="center"   />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ALT_COUNT_E" HeaderText="Alloted Applicants">
                                    <ItemStyle HorizontalAlign="center"   />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ALT_COUNT_H" HeaderText="OnHold Applicants">
                                    <ItemStyle HorizontalAlign="center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="REQ_COUNT_S" HeaderText="Requested Students">
                                    <ItemStyle HorizontalAlign="center"  />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ALT_COUNT_S" HeaderText="Alloted Students">
                                    <ItemStyle HorizontalAlign="center"  />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SEAT_COUNT" HeaderText="Total Seats">
                                    <ItemStyle HorizontalAlign="center"  />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Total Requested">
                                    <ItemStyle HorizontalAlign="center"  />
                                    <ItemTemplate>
                                        <asp:Label ID="lblReq" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Allotted">
                                    <ItemStyle HorizontalAlign="center"  />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAllot" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Seats Available">
                                    <ItemStyle HorizontalAlign="center"  />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAvailable" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                    HorizontalSide="Center" TargetControlID="lblGrdError" VerticalSide="Middle">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </div>
        </div>
    </div>
</asp:Content>
