﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Math
Partial Class Curriculum_ClmReportWriting_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330333") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindReport()
                    gvReport.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec RPT.getREPORTCARDBYEMPLOYEE " + Session("EmployeeID") + "," + Session("Current_ACD_ID")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvReport.DataSource = ds
        gvReport.DataBind()
    End Sub

    Sub BindGroups(ByVal rpf_descr As String, ByVal grades As String, ByVal gvGroup As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC RPT.getREPORTCARDBYEMPLOYEE_SUB " _
                                 & Session("EmployeeID") + "," _
                                 & "'" + rpf_descr + "'," _
                                 & Session("Current_ACD_ID") + "," _
                                 & "'" + grades + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGroup.DataSource = ds
        gvGroup.DataBind()
        gvGroup.Attributes.Add("bordercolor", "#1b80b6")
    End Sub

#End Region

    Protected Sub gvReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReport.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblGrades As Label
            Dim lblRpf As Label
            Dim gvGroup As GridView

            lblGrades = e.Row.FindControl("lblGrades")
            lblRpf = e.Row.FindControl("lblRpf")
            gvGroup = e.Row.FindControl("gvGroup")

            BindGroups(lblRpf.Text, lblGrades.Text, gvGroup)
        End If
    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        ViewState("datamode") = Encr_decrData.Encrypt("edit")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

        Dim lblReport As Label = TryCast(sender.FindControl("lblReport"), Label)
        Dim lblSgr_ID As Label = TryCast(sender.FindControl("lblSgr_ID"), Label)
        Dim lblGrdId As Label = TryCast(sender.FindControl("lblGrdId"), Label)
        Dim lblSubject As Label = TryCast(sender.FindControl("lblSubject"), Label)
        Dim lblGroup As Label = TryCast(sender.FindControl("lblGroup"), Label)
        Dim lblGrade As Label = TryCast(sender.FindControl("lblGrade"), Label)
        Dim lblSbgId As Label = TryCast(sender.FindControl("lblSbgId"), Label)



        Dim url As String
        url = String.Format("~\Curriculum\clmReportWriting_M.aspx?MainMnu_code={0}&datamode={1}" _
                           & "&rpf=" + Encr_decrData.Encrypt(lblReport.Text) _
                           & "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                           & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                           & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                           & "&GRDID=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                           & "&grpid=" + Encr_decrData.Encrypt(lblSgr_ID.Text) _
                           & "&sbgid=" + Encr_decrData.Encrypt(lblSbgId.Text), ViewState("MainMnu_code"), ViewState("datamode"))


        Response.Redirect(url)

    End Sub
End Class
