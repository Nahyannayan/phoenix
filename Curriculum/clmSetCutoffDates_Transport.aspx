<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSetCutoffDates_Transport.aspx.vb" Inherits="clmSetCutoffDates_Transport" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register src="../Asset/UserControls/usrDatePicker.ascx" tagname="usrDatePicker" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
  
	
</script>
   
<table id="Table1" border="0" width="100%">
     
    </table>
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left" >
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                </span>
            </td>
        </tr>
        <tr style="font-size: 8pt; color: #800000" valign="bottom">
            <td align="center" class="matters"  valign="middle">
                &nbsp;Fields Marked<span style="font-size: 8pt; color: #800000"> </span>with(<span
                    style="font-size: 8pt; color: #800000">*</span>)are mandatory</td>
        </tr>
        <tr>
            <td class="matters"  valign="bottom" >
                <table align="center" border="1" bordercolor="#1b80b6" style="border-collapse:collapse" cellpadding="5" cellspacing="0"
                    class="matters"  width="55%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <asp:Literal id="ltLabel" runat="server" 
                                Text="SET CUT OFF DETAILS FOR TRANSPORT"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            Academic Year</td>
                        <td align="center" >
                            :</td>
                        <td align="left" colspan="4" >
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Report Type</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" colspan="4">
                            <asp:DropDownList id="ddlCAD_CAM_ID" AutoPostBack="true"  runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                Report Printed For </td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" colspan="4">
                <asp:DropDownList id="ddlReportPrintedFor" runat="server"  AutoPostBack ="true" Width="264px">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            Trasport
                            Fee Cut Off Amount</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" >
                            <asp:TextBox ID="txtAmount" runat="server" Width="58px"></asp:TextBox>
                        </td>
                        <td align="left" >
                            Date</td>
                        <td align="center" style="width: 2px; height: 21px">
                            :</td>
                        <td align="left" >
                            <uc1:usrDatePicker ID="usrDPFeeCutoff" runat="server" />
                        </td>
                    </tr>
                                      
                    </table>
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 9px" valign="bottom">
                <br />
                &nbsp;</td>
        </tr>
        <tr>
            <td class="matters" style="height: 10px" valign="bottom">
                <asp:Button id="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnEdit_Click" Text="Edit" /><asp:Button id="btnSave" 
                    runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" 
                    OnClick="btnCancel_Click" /></td>
        </tr>
    </table>
    <input id="h_Row" type="hidden" runat="server" />
</asp:Content>

