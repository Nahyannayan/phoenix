<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmHomeworkDets.aspx.vb" Inherits="Curriculum_clmHomeworkDets" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Homework Details</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" width="100%" runat="server" align="center">
                    <tr>
                        <td   valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                 ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Academic Year</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left" colspan="2">
                                        <asp:LinkButton ID="lnkShowdetails" Text="View Details" runat="server"  
                                            >[+]Details</asp:LinkButton>
                                        <asp:Panel ID="pnlsetup" runat="server" CssClass="darkPanlvisible" Visible="false">
                                            <ajaxToolkit:ModalPopupExtender ID="mpe"
                                                DynamicServicePath="" runat="server" Enabled="True" TargetControlID="lnkShowdetails"
                                                PopupControlID="pnlsetup" BackgroundCssClass="modalBackground" />
                                            <div class="panelFeeSetup" style="background-color:#fff;">
                                                <table width="100%" class="panel-cover"  >
                                                    <tr>
                                                        <td align="right"  >
                                                            <asp:LinkButton ID="lbPnlJobClose" CssClass="closebtnFee" runat="server" OnClick="lbPnlJobClose_Click"><span style="padding: 1px 4px;background-color: #ccc6c6;border:1px solid #999;color:  #333;border-radius:  4px;box-shadow: 1px 2px 5px rgba(0,0,0,0.1);text-shadow: 2px 0px 2px rgba(0,0,0,0.4);">X</span></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <iframe runat="server" src="#" frameborder="0" width="800px" scrolling="no" height="500px" id="IfRefClose"></iframe>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </td>

                                </tr>
                                <tr id="trAcd2" runat="server">
                                    <td align="left" class="matters"><span class="field-label">Grade</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Subject</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Group</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" class="matters"><span class="field-label">Creation Date</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtCDate" runat="server"></asp:TextBox><asp:ImageButton
                                            ID="imgCDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgCDate" TargetControlID="txtCDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Submit Date</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtSDate" runat="server"></asp:TextBox><asp:ImageButton
                                            ID="imgSDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgSDate" TargetControlID="txtSDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Preparation Time/Minutes</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtTime" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTime"
                                            ErrorMessage="*Error" ValidationExpression="^\d*([.]\d*)?|[.]\d+$"> </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Description</span>
                                    </td>
                                    <td align="left" colspan="2" class="matters">
                                        <asp:TextBox ID="txtDesc" runat="server" TextMode="multiline" ></asp:TextBox>
                                    </td>
                                    <td class="matters">
                                        <asp:CheckBox ID="cbOnline" runat="server" Text="Show in Parent Login" CssClass="field-label" Checked="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                                <tr id="Tr6" runat="server">
                                    <td id="Td12" align="center" class="matters" runat="server" colspan="4" style="text-align: center">
                                        <asp:GridView ID="grdType" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="10" Width="100%" AllowPaging="true" OnPageIndexChanging="grdType_PageIndexChanging">
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="CMTID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="tyId" runat="server" Text='<%# bind("HW_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpart" runat="server" Text='<%# Bind("HW_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Group">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltype" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cre Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCdate" runat="server" Text='<%# Bind("HW_CDATE","{0:dd/MMMM/yyyy}") %>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Submit Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSDate" runat="server" Text='<%# Bind("HW_SDATE","{0:dd/MMMM/yyyy}") %>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Prep Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPTime" runat="server" Text='<%# Bind("HW_TIME") %>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("HW_DESC") %>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Edit" Text="Edit" HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                            Text="Delete"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                            runat="server">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" Height="30px" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="H_CAT_ID" runat="server" />
                <script language="javascript" type="text/javascript">


                    function IfRefClose_onclick() {

                    }


                </script>
            </div>
        </div>
    </div>
</asp:Content>
