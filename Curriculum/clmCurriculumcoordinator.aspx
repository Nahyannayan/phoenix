﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmCurriculumcoordinator.aspx.vb" Inherits="Curriculum_clmCurriculumcoordinator" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Curriculum Coordinators"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <table id="Table1" runat="server" align="center" 
                    cellpadding="5"
        cellspacing="0" style="width: 100%" >
        <tr>
            <td align="center"  >
            
                <table id="Table2" runat="server" align="center" 
                    cellpadding="5" cellspacing="0" style="width: 100%;border-collapse:collapse" >
                    
                    
                    <tr>
                        <td align="center" style="width: 100%">
                            <asp:GridView ID="gvOption" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-row"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                      HeaderStyle-Height="30" PageSize="20" Width="100%"  BorderStyle="None">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOptId" runat="server" Text='<%# Bind("EMP_Name") %>'></asp:Label>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("EMP_id") %>' Visible =false ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <HeaderTemplate>
                                            
                                                        <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Emp Name"></asp:Label><br />
                                                   
                                                                    <div id="Div1" class="chromestyle" style="display:none;">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu1"></a>
                                                                                <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                               
                                                                    <asp:TextBox ID="txtOption" runat="server" Width="60px"></asp:TextBox>
                                                                
                                                                        <asp:ImageButton ID="btnEmpid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEmpid_Search_Click" />
                                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOption" runat="server" text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                  <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOptId1" runat="server" Text='<%# Bind("USR_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <HeaderTemplate>
                                            
                                                        <asp:Label ID="lblopt1" runat="server" CssClass="gridheader_text" Text="User Name"></asp:Label><br />
                                                    
                                                                    <div id="Div11" class="chromestyle" style="display:none;">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu1"></a>
                                                                                <img id="mnu_1_img1" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                
                                                                    <asp:TextBox ID="txtOption1" runat="server" Width="60px"></asp:TextBox>
                                                               
                                                                        <asp:ImageButton ID="btnuid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnuid_Search_Click" />
                                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOption1" runat="server" text='<%# Bind("USR_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                 
                                
                                        
                                    <asp:TemplateField>
                                     <ItemTemplate>
                                   <asp:CheckBox ID="cb1" runat="server" text='<%# Bind("CUR_EMP_ID") %>'/>
                                     </ItemTemplate>
                                        
                                    </asp:TemplateField>  
                                                                        
                                     
                                    <asp:ButtonField CommandName="Save" Text="Save" HeaderText="Save">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px">
                                        </ItemStyle>
                                    </asp:ButtonField>   
                                    
                                    
                                    
                                </Columns>
                                
                                                              
                                
                                
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                </div>
        </div>
    </div>
</asp:Content>

