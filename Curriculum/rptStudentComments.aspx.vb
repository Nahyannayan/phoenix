Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Configuration
Imports CURRICULUM
Imports ActivityFunctions
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports System.Web.Security
Imports Telerik.QuickStart
Imports Telerik.Web.UI
Imports System.Drawing
Imports System.Collections.Generic

Partial Class Curriculum_rptStudentComments
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Private NVCStudentPage As NameValueCollection

   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        lblError.Text = ""
        Session.Timeout = 60

        If Page.IsPostBack = False Then
            Session("StuComments") = ReportFunctions.CreateTableStuComments()
            'Session("StuComments") = ReportFunctions.CreateTableStuMarks()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330004") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    ViewState("datamode") = "add"
                    trGroup.Visible = False
                    If ViewState("datamode") = "add" Then
                        btnAdd.Text = "Add"
                        'BindGrid()
                        'ddlAcdID = ActivityFunctions.PopulateAcademicYear(ddlAcdID, Session("clm"), Session("sBsuid"))
                        FillAcd()
                        FillReport()
                        FillGrade()
                        FillRepSchedule()
                        BindMainHeader()
                        FillSection()
                        BindStudentCombo()
                        trSave1.Visible = False
                        trSave.Visible = False
                    Else

                        H_ACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("AccId").Replace(" ", "+"))
                        H_GRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("GradeId").Replace(" ", "+"))
                        H_SBJ_ID.Value = Encr_decrData.Decrypt(Request.QueryString("SubjID").Replace(" ", "+"))
                        H_RPT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("MarksId").Replace(" ", "+"))
                        'DisplayRecordForEdit()
                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private Functions"
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function

    Sub BindMainHeader()
        ddlMainHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReport.SelectedValue.ToString + "' AND RSD_bHASCHILD='TRUE' AND RSD_bALLSUBJECTS='FALSE'"

        If Session("CLM") <> 1 Then
            Select Case ddlRepSch.SelectedItem.Text.ToUpper
                Case "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF NOVEMBER", "END OF DECEMBER", "END OF FEBRUARY", "END OF MARCH", "END OF APRIL"
                    str_query += " AND RSD_HEADER<>'TEACHER OBSERVATIONS' "
            End Select
        Else
            'If Session("sBsuid") = "133006" Then
            '    Select Case ddlRepSch.SelectedItem.Text.ToUpper
            '        Case "MAY", "JUNE", "DECEMBER"
            '            str_query += " AND RSD_HEADER<>'TEACHER OBSERVATIONS' "
            '    End Select
            'Else
            '    Select Case ddlRepSch.SelectedItem.Text.ToUpper
            '        Case "ON ENTRY APRIL", "END OF APRIL", "END OF MAY", "END OF JUNE", "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF NOVEMBER", "END OF DECEMBER", "END OF JANUARY"
            '            str_query += " AND RSD_HEADER<>'TEACHER OBSERVATIONS' "
            '    End Select
            'End If
            Select Case ddlRepSch.SelectedItem.Text.ToUpper
                Case "ON ENTRY APRIL", "END OF APRIL", "END OF MAY", "END OF JUNE", "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF NOVEMBER", "END OF DECEMBER", "END OF JANUARY"
                    str_query += " AND RSD_HEADER<>'TEACHER OBSERVATIONS' "
            End Select
        End If

        str_query += " ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlMainHeader.DataSource = ds
        ddlMainHeader.DataTextField = "RSD_HEADER"
        ddlMainHeader.DataValueField = "RSD_ID"
        ddlMainHeader.DataBind()
        If ddlMainHeader.Items.Count = 0 Then
            ddlMainHeader.Visible = False
        Else
            ddlMainHeader.Visible = True
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Private Sub SaveRecord()

        Dim iReturnvalue As Integer
        Dim iIndex As Integer
        Dim cmd As New SqlCommand
        Dim lblStudId As New Label
        Dim lblStudName As New Label
        Dim txtComments As New TextBox
        Dim txtCmntsShort As New TextBox
        Dim StrSql As String
        Dim ds As DataSet
        Dim RSTID As Integer
        Dim strComments As String
        Dim stTrans As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try

            objConn.Open()
            stTrans = objConn.BeginTransaction

            For iIndex = 0 To Session("StuComments").Rows.Count - 1
                If IsDBNull(Session("StuComments").Rows(iIndex)("Id")) = False Then

                    If Session("Current_acd_id") = ddlAcdID.SelectedValue.ToString Then
                        cmd = New SqlCommand("[RPT].[SaveREPORT_STUDCOMMENTS_S]", objConn, stTrans)
                    Else
                        cmd = New SqlCommand("[RPT].[SaveREPORT_STUDCOMMENTS_PREVYEARS]", objConn, stTrans)
                    End If

                    cmd.CommandType = CommandType.StoredProcedure
                    With Session("StuComments")
                        If ViewState("datamode") = "add" Then
                            cmd.Parameters.AddWithValue("@RST_ID", 0)
                        End If
                        If ViewState("datamode") = "edit" Then
                            cmd.Parameters.AddWithValue("@RST_ID", .Rows(iIndex)("id"))
                        End If

                        cmd.Parameters.AddWithValue("@RST_RPF_ID", .Rows(iIndex)("RST_RPF_ID"))
                        cmd.Parameters.AddWithValue("@RST_RSD_ID", .Rows(iIndex)("RST_RSD_ID"))
                        cmd.Parameters.AddWithValue("@RST_ACD_ID", .Rows(iIndex)("RST_ACD_ID"))
                        cmd.Parameters.AddWithValue("@RST_GRD_ID", .Rows(iIndex)("RST_GRD_ID"))
                        cmd.Parameters.AddWithValue("@RST_STU_ID", .Rows(iIndex)("RST_STU_ID"))
                        cmd.Parameters.AddWithValue("@RST_RSS_ID", 0)
                        cmd.Parameters.AddWithValue("@RST_SGR_ID", 0)
                        cmd.Parameters.AddWithValue("@RST_SBG_ID", 0)
                        cmd.Parameters.AddWithValue("@RST_TYPE_LEVEL", "")
                        cmd.Parameters.AddWithValue("@RST_MARK", 0)
                        cmd.Parameters.AddWithValue("@RST_COMMENTS", .Rows(iIndex)("RST_COMMENTS"))
                        cmd.Parameters.AddWithValue("@RST_GRADING", "")
                        If ViewState("datamode") = "add" Then
                            cmd.Parameters.AddWithValue("@bEdit", 0)
                        End If
                        If ViewState("datamode") = "edit" Then
                            cmd.Parameters.AddWithValue("@bEdit", 1)
                        End If
                        cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                        cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                        cmd.ExecuteNonQuery()
                        iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

                    End With
                End If
            Next
            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                Session("StuComments").Rows.Clear()
                Exit Sub
            End If
            stTrans.Commit()
            lblError.Text = "Successfully Saved"
            Session("StuComments").Rows.Clear()
            'Session("SaveTrue") = "Successfully Saved"


        Catch ex As Exception
            stTrans.Rollback()
            Session("StuComments").Rows.Clear()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
            'Session("SaveTrue") = "Request could not be processed"
        Finally
            objConn.Close()
        End Try
    End Sub



    Private Sub ClearComments()
        btnAdd.Text = "Add"
    End Sub

    Private Sub ClearEntireScreen()
        btnAdd.Text = "Add"
    End Sub


    Private Sub GetStudentsList()


        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim intRowCnt As Integer = 0
        Try
            'Instantiate a New Hash Table And Keeps into a ViewState
            NVCStudentPage = New NameValueCollection
            ViewState("StuPages") = NVCStudentPage
            ViewState("PageNo") = "0"

            If Not ddlGrade.SelectedIndex = -1 Then
                strCriteria = " AND STU_GRD_ID='" & H_GRD_ID.Value & "' "
            End If

            If H_GRP_ID.Value <> "" And H_GRP_ID.Value <> "0" Then
                strCriteria += " AND SGR_ID='" & H_GRP_ID.Value & "' "
            End If
            If Not ddlSection.SelectedIndex = -1 Then
                strCriteria += " AND STU_SCT_ID='" & H_SCT_ID.Value & "' "
            End If
            If Not cmbStudent.SelectedItem Is Nothing Then
                If cmbStudent.SelectedItem.Text <> "" Then
                    strCriteria += " AND STU_ID=" & GetCheckedStudent() & " "
                End If
            End If           
            If Session("CURRENT_ACD_ID") = H_ACD_ID.Value Then
                If Session("sbsuid") <> "126008" Then
                    strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,STU_NAME as StudentName, " & GetReportHeader() & "  " & _
                            " FROM VW_STUDENT_DETAILS " & _
                            " INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID AND SSD_ACD_ID='" & H_ACD_ID.Value & "'" & _
                            " INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID " & _
                            " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"

                    strSql += strCriteria & " ORDER BY STU_NAME"
                Else
                    strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,isnull(STU_PASSPORT_NAME,STU_NAME) as StudentName, " & GetReportHeader() & "  " & _
                " FROM VW_STUDENT_DETAILS " & _
                " INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID AND SSD_ACD_ID='" & H_ACD_ID.Value & "'" & _
                " INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID " & _
                " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"

                    strSql += strCriteria & " ORDER BY  STU_PASSPORT_NAME,STU_NAME"
                End If
            Else
                strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,STU_NAME as StudentName, " & GetReportHeader() & "  " & _
                       " FROM VW_STUDENT_DETAILS_PREVYEARS " & _
                       " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"

                strSql += strCriteria & " ORDER BY STU_NAME"
            End If
            dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            If Session("sbsuid") = "125017" Then
                If dsStudents.Tables(0).Rows.Count >= 1 Then
                    For Each drRow As DataRow In dsStudents.Tables(0).Rows
                        intRowCnt += 1
                        If intRowCnt < 11 Then
                            NVCStudentPage.Add(1, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 11 And intRowCnt < 21 Then
                            NVCStudentPage.Add(2, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 21 And intRowCnt < 31 Then
                            NVCStudentPage.Add(3, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 31 And intRowCnt < 41 Then
                            NVCStudentPage.Add(4, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 41 And intRowCnt < 51 Then
                            NVCStudentPage.Add(5, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 51 And intRowCnt < 61 Then
                            NVCStudentPage.Add(6, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 61 And intRowCnt < 71 Then
                            NVCStudentPage.Add(7, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 71 And intRowCnt < 81 Then
                            NVCStudentPage.Add(8, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 81 And intRowCnt < 91 Then
                            NVCStudentPage.Add(9, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 91 And intRowCnt < 101 Then
                            NVCStudentPage.Add(10, drRow.Item("StudentID"))
                        Else
                            NVCStudentPage.Add(11, drRow.Item("StudentID"))
                        End If
                    Next
                End If
            ElseIf Session("sbsuid") = "135010" Then
                If dsStudents.Tables(0).Rows.Count >= 1 Then
                    For Each drRow As DataRow In dsStudents.Tables(0).Rows
                        intRowCnt += 1
                        If intRowCnt < 16 Then
                            NVCStudentPage.Add(1, drRow.Item("STU_ID"))
                        ElseIf intRowCnt >= 16 And intRowCnt < 31 Then
                            NVCStudentPage.Add(2, drRow.Item("STU_ID"))
                        ElseIf intRowCnt >= 31 And intRowCnt < 46 Then
                            NVCStudentPage.Add(3, drRow.Item("STU_ID"))
                        ElseIf intRowCnt >= 46 And intRowCnt < 61 Then
                            NVCStudentPage.Add(4, drRow.Item("STU_ID"))
                        ElseIf intRowCnt >= 61 And intRowCnt < 76 Then
                            NVCStudentPage.Add(5, drRow.Item("STU_ID"))
                        ElseIf intRowCnt >= 76 And intRowCnt < 81 Then
                            NVCStudentPage.Add(8, drRow.Item("STU_ID"))
                        ElseIf intRowCnt >= 81 And intRowCnt < 91 Then
                            NVCStudentPage.Add(9, drRow.Item("STU_ID"))
                        ElseIf intRowCnt >= 91 And intRowCnt < 101 Then
                            NVCStudentPage.Add(10, drRow.Item("STU_ID"))
                        Else
                            NVCStudentPage.Add(11, drRow.Item("STU_ID"))
                        End If
                    Next
                End If
            ElseIf Session("sbsuid") <> "125010" And Session("sbsuid") <> "123004" Then
                If dsStudents.Tables(0).Rows.Count >= 1 Then
                    For Each drRow As DataRow In dsStudents.Tables(0).Rows
                        intRowCnt += 1
                        If intRowCnt < 6 Then
                            NVCStudentPage.Add(1, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 6 And intRowCnt < 11 Then
                            NVCStudentPage.Add(2, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 11 And intRowCnt < 16 Then
                            NVCStudentPage.Add(3, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 16 And intRowCnt < 21 Then
                            NVCStudentPage.Add(4, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 21 And intRowCnt < 26 Then
                            NVCStudentPage.Add(5, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 26 And intRowCnt < 31 Then
                            NVCStudentPage.Add(6, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 31 And intRowCnt < 36 Then
                            NVCStudentPage.Add(7, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 36 And intRowCnt < 41 Then
                            NVCStudentPage.Add(8, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 41 And intRowCnt < 46 Then
                            NVCStudentPage.Add(9, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 46 And intRowCnt < 51 Then
                            NVCStudentPage.Add(10, drRow.Item("StudentID"))
                        Else
                            NVCStudentPage.Add(11, drRow.Item("StudentID"))
                        End If

                    Next
                End If
            ElseIf Session("sbsuid") = "125010" Then  'for tws show 10records in one page
                If dsStudents.Tables(0).Rows.Count >= 1 Then
                    For Each drRow As DataRow In dsStudents.Tables(0).Rows
                        intRowCnt += 1
                        If intRowCnt < 21 Then
                            NVCStudentPage.Add(1, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 21 And intRowCnt < 41 Then
                            NVCStudentPage.Add(2, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 41 And intRowCnt < 61 Then
                            NVCStudentPage.Add(3, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 61 And intRowCnt < 81 Then
                            NVCStudentPage.Add(4, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 81 And intRowCnt < 101 Then
                            NVCStudentPage.Add(5, drRow.Item("StudentID"))
                        Else
                            NVCStudentPage.Add(6, drRow.Item("StudentID"))
                        End If

                    Next
                End If
            Else
                For Each drRow As DataRow In dsStudents.Tables(0).Rows
                    NVCStudentPage.Add(1, drRow.Item("StudentID"))
                Next
                btnSave.Text = "Save"
                btnSave1.Text = "Save"
            End If

            ViewState("StuPages") = NVCStudentPage
            ViewState("PageNo") = "1"
            ViewState("TotalPages") = NVCStudentPage.Count
            lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")

            Dim i As Integer
            Dim dt As New DataTable
            dt.Columns.Add(New DataColumn("pageno", GetType(String)))
            Dim dr As DataRow
            For i = 1 To Val(ViewState("TotalPages"))
                dr = dt.NewRow
                dr.Item(0) = i
                dt.Rows.Add(dr)
            Next

            dlPages.DataSource = dt
            dlPages.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lblPageNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblPageNo As LinkButton
        lblPageNo = TryCast(sender.FindControl("lblPageNo"), LinkButton)
        ViewState("PageNo") = CInt(lblPageNo.Text)
        lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")
        gvStudents.Columns.Clear()
        BindStudents()
    End Sub
    Private Function BindStudents()
        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        gvStudents.DataSourceID = ""
        Dim tmpClmCount As Integer = 0
        Dim str_filter_StudentNo As String = String.Empty
        Dim str_filter_StudName As String = String.Empty
        Dim txtSearch As New TextBox
        Dim str_search As String
        Dim str_STUD_NO As String = String.Empty
        Dim str_STUD_NAME As String = String.Empty
        Dim ds As DataSet
        Dim GvHeader As NameValueCollection
        Dim strCondition As String = ""
        Dim strStuIDs As String = ""
        Dim cArr As New ArrayList

        Try

            If Not ViewState("StuPages") Is Nothing Then
                NVCStudentPage = ViewState("StuPages")
                If Not NVCStudentPage.GetValues(ViewState("PageNo").ToString) Is Nothing Then
                    Dim x As String() = NVCStudentPage.GetValues(ViewState("PageNo").ToString)
                    For Each x1 As String In x
                        strStuIDs = strStuIDs + x1 + ","
                    Next
                    If strStuIDs.Contains(",") = True Then
                        strStuIDs = strStuIDs.Substring(0, Len(strStuIDs) - 1)
                    End If
                    strCriteria += " AND STU_ID IN (" & strStuIDs & ")"
                End If
            End If

            If Not ddlGrade.SelectedIndex = -1 Then
                strCriteria += " AND STU_GRD_ID='" & H_GRD_ID.Value & "' "
            End If

            If H_GRP_ID.Value <> "" And H_GRP_ID.Value <> "0" Then
                strCriteria += " AND SGR_ID='" & H_GRP_ID.Value & "' "
            End If
            If Not ddlSection.SelectedIndex = -1 Then
                strCriteria += " AND STU_SCT_ID='" & H_SCT_ID.Value & "' "
            End If

            'If cmbStudent.SelectedItem.Text <> "" Then
            '    strCriteria += " AND STU_ID=" & GetCheckedStudent() & " "
            'End If


            If Session("CURRENT_ACD_ID") = H_ACD_ID.Value Then
                strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,STU_NAME as StudentName, " & GetReportHeader() & "  " & _
                        " FROM VW_STUDENT_DETAILS " & _
                        " INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID AND SSD_ACD_ID='" & H_ACD_ID.Value & "'" & _
                        " INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID " & _
                        " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"
            Else
                strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,STU_NAME as StudentName, " & GetReportHeader() & "  " & _
                 " FROM VW_STUDENT_DETAILS_PREVYEARS " & _
                 " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"
            End If


            strSql += strCriteria & " ORDER BY STU_NAME"
            GvHeader = Session("ReportHeader")

            If GvHeader.Count > 0 Then
                dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
                If dsStudents.Tables(0).Rows.Count = 0 Then
                    'gvStudents.DataSource = dsStudents.Tables(0)
                    'dsStudents.Tables(0).Rows.Add(dsStudents.Tables(0).NewRow())
                    'gvStudents.DataBind()
                    'gvStudents.Columns.Clear()
                    'gvStudents.DataSource = Nothing
                    'gvStudents.DataBind()
                    'Dim ColumnCount As Integer = gvStudents.Columns.Count
                    'If gvStudents.Rows.Count > 0 Then
                    '    gvStudents.Rows(0).Cells.Clear()
                    '    gvStudents.Rows(0).Cells.Add(New TableCell)
                    '    gvStudents.Rows(0).Cells(0).ColumnSpan = ColumnCount
                    '    gvStudents.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    '    gvStudents.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                    'End If
                    BindBlankRow()
                    trSave1.Visible = False
                    trSave.Visible = False

                Else
                    Dim Col As DataControlField
                    Dim clmCnt As Integer = 0

                    For Each clm As DataColumn In dsStudents.Tables(0).Columns

                        If clmCnt < 3 Then
                            Dim Field As New BoundField
                            Field.DataField = clm.ColumnName
                            Field.HeaderText = clm.ColumnName
                            Col = Field
                            gvStudents.Columns.Insert(clmCnt, Col)
                        ElseIf clmCnt >= 3 Then
                            tmpClmCount = tmpClmCount + 1
                            Dim TField As New TemplateField
                            Dim DTField As DynamicTemplate = New DynamicTemplate(ListItemType.Item)
                            Dim TxtItem As New TextBox()
                            Dim PnlItem As New Panel()
                            TxtItem.ID = "txt" + tmpClmCount.ToString

                            If GvHeader.Item(clm.ColumnName).Split("_")(3) <> "" Then
                                TxtItem.CssClass = GvHeader.Item(clm.ColumnName).Split("_")(3)
                                If TxtItem.CssClass = "textboxmulti" Then
                                    TxtItem.TextMode = TextBoxMode.MultiLine
                                    TxtItem.EnableTheming = False

                                End If
                            End If
                            If GvHeader.Item(clm.ColumnName).Split("_")(2) = "C" And TxtItem.CssClass = "textboxmulti" Then
                                TxtItem.SkinID = "MultiText"
                                TxtItem.Visible = True
                                TxtItem.Text = ""

                                '-----------------------------------------
                                Dim imgbtn As New ImageButton
                                imgbtn.ID = "img" + tmpClmCount.ToString
                                imgbtn.ImageUrl = "../Images/comment.jpg"
                                imgbtn.Width = 20
                                imgbtn.Height = 20
                                imgbtn.ToolTip = "Press Alt + Z "
                                imgbtn.AccessKey = "Z"
                                DTField.AddControl(TxtItem, "Text", clm.ColumnName)
                                DTField.AddControl(imgbtn, "ToolTip", clm.ColumnName)

                                cArr.Add(TxtItem.ID)
                            ElseIf GvHeader.Item(clm.ColumnName).Split("_")(2) = "D" Then
                                Dim ddlItem As New DropDownList
                                ddlItem = GetHeaderValues(ddlItem, GvHeader.Item(clm.ColumnName).Split("_")(0))
                                'If Session("sbsuid") = "125010" Then
                                ' If ddlItem.Width > 70 Then
                                'If ddlGrade.SelectedValue = "11" Then
                                'Item.Width = 300
                                'End If

                                'End If
                                ' End If
                                If ddlItem.Items.Count > 1 Then
                                    ddlItem.ID = "txt" + tmpClmCount.ToString
                                    DTField.AddControl(ddlItem, "Text", clm.ColumnName)
                                Else
                                    TxtItem.TextMode = TextBoxMode.SingleLine
                                    DTField.AddControl(TxtItem, "Text", clm.ColumnName)
                                End If
                            Else
                                'TxtItem.CssClass = "inputboxnor" 'inputbox_multi
                                TxtItem.TextMode = TextBoxMode.SingleLine
                                TxtItem.Visible = True
                                TxtItem.Text = ""
                                'TxtItem.Width = 150

                                '------------------------------------------
                                DTField.AddControl(TxtItem, "Text", clm.ColumnName)

                            End If
                            TField.ItemTemplate = DTField
                            TField.HeaderText = clm.ColumnName
                            gvStudents.Columns.Insert(clmCnt, TField)

                        End If
                        clmCnt = clmCnt + 1
                    Next
                    H_CommentCOLS.Value = tmpClmCount
                    Session("ColummnCnt") = tmpClmCount
                    gvStudents.DataSource = dsStudents.Tables(0)
                    gvStudents.DataBind()

                    gvStudents.Columns(1).HeaderStyle.Width = 0
                    gvStudents.Columns(1).ItemStyle.Width = 0
                    'imgReport.Enabled = False
                    'imgRptSchedule.Enabled = False
                    'ImgRptHeader.Enabled = False
                    ddlReport.Enabled = False
                    ddlGrade.Enabled = False
                    ImgGroup.Enabled = False
                    'ImgSection.Enabled = False
                    btnView.Enabled = False
                    'txtRptSchedule.Enabled = False
                    txtGroup.Enabled = False
                    ddlRepSch.Enabled = False
                    ddlSection.Enabled = False
                    'txtSection.Enabled = False
                    trSave1.Visible = True
                    trSave.Visible = True
                    trGroup.Visible = False
                End If
            Else
                BindBlankRow()
                trSave1.Visible = False
                trSave.Visible = False
            End If

            Dim cAr As String()
            ' ReDim cAr(cArr.Count * gvStudents.Rows.Count)
            Dim k As Integer
            Dim p As Integer
            Dim t As Integer = 0
            Dim cst As String = ""


            Dim LENGTH_TEXT As Integer

            Dim lengthFunction As String

            If Session("sbsuid") = "125017" Then
                If ddlReport.SelectedItem.Text.ToString.ToUpper.Contains("MID SEMESTER") Then
                    LENGTH_TEXT = 300
                Else
                    LENGTH_TEXT = 560
                End If
            ElseIf Session("sbsuid") = "125002" Then
                LENGTH_TEXT = 3500
            Else
                LENGTH_TEXT = 3000
            End If
            lengthFunction = "function isMaxLength(txtBox,evt) {"
            lengthFunction += " if(txtBox) { "
            '   lengthFunction += "alert(txtBox.value.length);"
            lengthFunction += " var charCode = (evt.which) ? evt.which : event.keyCode ;"
            lengthFunction += " if (charCode==46 || charCode==8 || charCode==37 || charCode==38 || charCode==39 || charCode==40 || charCode==35 || charCode==36    ){return true;}"
            lengthFunction += "  if (txtBox.value.length<" + LENGTH_TEXT.ToString + ")"
            lengthFunction += "{ return true;} else {return false;}"
            '            lengthFunction += "     return ( txtBox.value.length <=" + LENGTH_TEXT.ToString + ");"
            lengthFunction += " }"
            lengthFunction += "}"




            lengthFunction += "function isMaxLength1(txtBox,evt) {"
            lengthFunction += "txtBox.style.height = 60+ ""px""; "
            lengthFunction += "txtBox.style.width = 200+ ""px""; "
            lengthFunction += " if(txtBox) { "
            '   lengthFunction += "alert(txtBox.value.length);"

            lengthFunction += "var str=txtBox.value; var charCode = (evt.which) ? evt.which : event.keyCode ;"
            lengthFunction += " if (charCode==46 || charCode==8 || charCode==37 || charCode==38 || charCode==39 || charCode==40 || charCode==35 || charCode==36    ){return true;}"
            lengthFunction += "  if (txtBox.value.length<" + LENGTH_TEXT.ToString + ")"
            lengthFunction += "{ return true;} else {alert('The comment length has exceeded then maximum permissible length of " + LENGTH_TEXT.ToString + " characters.The remaining text will be truncated.');txtBox.value=str.substring(0," + (LENGTH_TEXT - 1).ToString + ");return false;}"
            '            lengthFunction += "     return ( txtBox.value.length <=" + LENGTH_TEXT.ToString + ");"
            lengthFunction += " }"
            lengthFunction += "}"

            lengthFunction += "function textHeightIncrease(txtBox,evt) {"
            '  lengthFunction += "txtBox.style.height = txtBox.scrollHeight +150+ ""px""; "
            lengthFunction += "txtBox.style.height = 200+ ""px""; "
            lengthFunction += "txtBox.style.width = 400+ ""px""; "
            lengthFunction += "return true;"
            lengthFunction += "}"


            Dim txtItem1 As TextBox

            For p = 0 To gvStudents.Rows.Count - 1
                For k = 0 To cArr.Count - 1
                    If cst <> "" Then
                        cst += "|"
                    End If
                    cst += gvStudents.Rows(p).FindControl(cArr.Item(k).ToString).ClientID
                    Try
                        txtItem1 = gvStudents.Rows(p).FindControl(cArr.Item(k).ToString)
                        txtItem1.Attributes.Add("onkeydown", "return isMaxLength(this,event);")
                        txtItem1.Attributes.Add("onmousedown", "return textHeightIncrease(this,event);")
                        txtItem1.Attributes.Add("onblur", "return isMaxLength1(this,event);")
                        ClientScript.RegisterClientScriptBlock(Page.GetType(), txtItem1.ClientID, lengthFunction, True)
                    Catch ex As Exception
                    End Try
                Next
            Next

            If cst <> "" Then
                cAr = cst.Split("|")
                RadSpell1.ControlsToCheck = cAr
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function

    Private Function GetReportHeader() As String
        Try
            Dim NVCReportSetup As New NameValueCollection
            Dim strHeader As String = ""
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet


            Dim strSQL As String = "SELECT CAST(RSD_ID AS VARCHAR) + '_' + CAST(RSD_RSM_ID AS VARCHAR) + '_' + RSD_RESULT + '_' + RSD_CSSCLASS AS Value ,REPLACE(RSD_HEADER,'.',' ') AS RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" & H_RPT_ID.Value & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS=0  AND ISNULL(RSD_bSUPRESS,0)=0 "

            If ddlMainHeader.Items.Count <> 0 Then
                strSQL += " AND RSD_MAIN_HEADER_ID=" + ddlMainHeader.SelectedValue.ToString
            End If

            strSQL += " ORDER BY RSD_DISPLAYORDER"
            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
            With dsHeader.Tables(0)
                For intCnt As Integer = 0 To .Rows.Count - 1
                    strHeader += "'' AS '" + .Rows(intCnt).Item("RSD_HEADER").ToString + "' ,"
                    NVCReportSetup.Add(.Rows(intCnt).Item("RSD_HEADER").ToString, .Rows(intCnt).Item("Value").ToString)
                Next
                If strHeader.Length > 1 Then
                    strHeader = strHeader.Substring(0, strHeader.Length - 1)
                End If
            End With
            Session("ReportHeader") = NVCReportSetup
            Return strHeader
        Catch ex As Exception

        End Try

    End Function

    Sub GetHeaderKeys()
        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsProcessRule As New DataSet()
        Dim sb As New StringBuilder

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        Dim sqlstr As String = ""
        Dim reader As SqlDataReader

        Try
            If Session("SBSUID") = "125017" Then

                sb.Append("<DIV style='text-align:left;'>PA - Positive Attitude</Div>")
                sb.Append("<DIV style='text-align:left;'>SA - Satisfactory Attitude</Div>")
                sb.Append("<DIV style='text-align:'>GE - Greater Effort</Div>")
                sb.Append("<DIV style='text-align:'>N/A- Not Applicable</Div>")


                ltProcess.Text = sb.ToString
                If sb.ToString = "" Then
                    lbtnKeys.Visible = False
                Else
                    lbtnKeys.Visible = True
                End If

                lbtnKeys.Text = "Keys : "
            Else

                Dim strSQL As String = "SELECT ISNULL(RSD_SUB_DESC,'-') AS RSD_SUB_DESC ,REPLACE(RSD_HEADER,'.','') AS RSD_HEADER " _
                                       & " FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" & ddlReport.SelectedValue.ToString _
                                       & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS=0 " _
                                       & " ORDER BY RSD_DISPLAYORDER "

                reader = SqlHelper.ExecuteReader(conn, CommandType.Text, strSQL)

                While reader.Read
                    sb.Append("<DIV style='text-align:left;'>" + reader.GetString(1) + " - " + reader.GetString(0) + "</Div>")
                End While
                reader.Close()
                'If sb.ToString = "" Then
                '    sb.Append("<div style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; color: #000000;padding:5pt;font-weight: bold;'>No Rule Available !!!</div>")
                'End If
                ltProcess.Text = sb.ToString
                If sb.ToString = "" Then
                    lbtnKeys.Visible = False
                Else
                    lbtnKeys.Visible = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



#End Region

#Region "Pick Up Values"

    Protected Sub imgAcdId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            'If H_ACD_ID.Value <> "" AndAlso H_ACD_ID.Value.Contains("___") Then
            '    Dim ACD_ID As String = H_ACD_ID.Value.Split("___")(0)
            '    txtAccYear.Text = H_ACD_ID.Value.Split("___")(3)
            '    H_ACD_ID.Value = ACD_ID
            'End If
        Catch ex As Exception

        End Try
    End Sub



    'Protected Sub imgRptSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        If H_SCH_ID.Value <> "" AndAlso H_SCH_ID.Value.Contains("___") Then
    '            Dim SCH_ID As String = H_SCH_ID.Value.Split("___")(0)
    '            txtRptSchedule.Text = H_SCH_ID.Value.Split("___")(3)
    '            H_SCH_ID.Value = SCH_ID
    '            'H_RPF_ID.Value = H_SCH_ID.Value.Split("___")(3) 'ReportFunctions.GetReportPrintedFor(H_SCH_ID.Value)
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub imgStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        If H_STU_ID.Value <> "" AndAlso H_STU_ID.Value.Contains("___") Then
    '            Dim STU_ID As String = H_STU_ID.Value.Split("___")(0)
    '            txtStuName.Text = H_STU_ID.Value.Split("___")(3)
    '            txtStuNo.Text = H_STU_ID.Value.Split("___")(6)
    '            H_STU_ID.Value = STU_ID
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub







#End Region

#Region " Button event Handlings"



    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblPages.Text = ""
    End Sub



#End Region



    Protected Sub btnView_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If ddlSection.Items.Count <> 0 Then
                GetStudentsList()
                BindStudents()
                'Session("SBGID") = H_SBJ_ID.Value
                Session("Grade") = ddlGrade.SelectedValue

                If Session("sbsuid") = "114003" Or Session("sbsuid") = "125017" Then
                    GetHeaderKeys()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'Try
        '    Try
        '        gvStudents.PageIndex = e.NewPageIndex
        '        gvStudents.Columns.Clear()
        '        BindStudents()
        '    Catch ex As Exception
        '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    End Try
        'Catch ex As Exception

        'End Try
    End Sub


    Sub FillAcd()

        ddlAcdID.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcdID.DataTextField = "ACY_DESCR"
        ddlAcdID.DataValueField = "ACD_ID"
        ddlAcdID.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcdID.Items(ddlAcdID.Items.IndexOf(li)).Selected = True
        If Not ddlAcdID.SelectedIndex = -1 Then
            H_ACD_ID.Value = ddlAcdID.SelectedItem.Value
        End If

        'ddlAcademicYear.Items.FindByValue(Session("ACY_ID")).Selected = True
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "edit" Then
                'UpdateMarksentry()


            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuComments") Is Nothing Then
                    AddRecord()
                    SaveRecord()
                    gvStudents.Columns.Clear()
                    'BindBlankRow()
                    BindStudents()
                End If
            End If

            Dim url As String
            'ViewState("datamode") = "add"
            'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            'ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            'url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            'Response.Redirect(url)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Function GetStudComments(ByVal STUID As String) As DataSet
        Dim i As Integer
        Dim txtComments As New TextBox
        Dim lblStudId As New Label
        Dim strSql As String
        Dim ds As DataSet
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Try

            'strSql = "SELECT RST_ID,RST_RSD_ID,REPLACE(RSD_HEADER,'.',' ') AS RSD_HEADER,RSD_RESULT,RST_COMMENTS FROM RPT.REPORT_STUDENT_S S INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID WHERE " _
            '        & " S.RST_ACD_ID='" & H_ACD_ID.Value & "' AND " _
            '        & " S.RST_GRD_ID='" & H_GRD_ID.Value & "' AND " _
            '        & " S.RST_STU_ID ='" & STUID & "' AND " _
            '        & " S.RST_RPF_ID='" & H_SCH_ID.Value & "' AND " _
            '        & " REPLACE(D.RSD_HEADER,'.',' ')='" & ColHeader & "' ORDER BY RST_ID"

            If ddlAcdID.SelectedValue.ToString = Session("current_acd_id") Then

                strSql = "SELECT RST_ID,RST_RSD_ID,REPLACE(RSD_HEADER,'.',' ') AS RSD_HEADER,RSD_RESULT,RST_COMMENTS FROM RPT.REPORT_STUDENT_S S INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID WHERE " _
                         & " S.RST_ACD_ID='" & H_ACD_ID.Value & "' AND " _
                         & " S.RST_GRD_ID='" & H_GRD_ID.Value & "' AND " _
                         & " S.RST_STU_ID ='" & STUID & "' AND " _
                         & " S.RST_RPF_ID='" & H_SCH_ID.Value & "' AND " _
                         & " RSD_bALLSUBJECTS='FALSE' "
            Else

                strSql = "SELECT RST_ID,RST_RSD_ID,REPLACE(RSD_HEADER,'.',' ') AS RSD_HEADER,RSD_RESULT,RST_COMMENTS FROM RPT.REPORT_STUDENT_PREVYEARS S INNER JOIN RPT.REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID WHERE " _
                     & " S.RST_ACD_ID='" & H_ACD_ID.Value & "' AND " _
                     & " S.RST_GRD_ID='" & H_GRD_ID.Value & "' AND " _
                     & " S.RST_STU_ID ='" & STUID & "' AND " _
                     & " S.RST_RPF_ID='" & H_SCH_ID.Value & "' AND " _
                     & " RSD_bALLSUBJECTS='FALSE' "


            End If


            If ddlMainHeader.Items.Count <> 0 Then
                strSql += " AND RSD_MAIN_HEADER_ID=" + ddlMainHeader.SelectedValue.ToString
            End If

            strSql += " ORDER BY RST_ID"

            Dim dsHeader As DataSet
            dsHeader = SqlHelper.ExecuteDataset(str_con, CommandType.Text, strSql)
            Return dsHeader

            '        End If
            '    Next
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function

    Protected Sub btnSearchStud_No_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudents()
    End Sub

    Protected Sub btnSearchStud_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudents()
    End Sub

    Protected Sub gvStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim cs As ClientScriptManager = Page.ClientScript
        Dim dsHeader As DataSet
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                e.Row.Cells(0).CssClass = "locked"
                e.Row.Cells(1).CssClass = "locked"


                Dim i As Integer
                For intcnt As Integer = 1 To CInt(Session("ColummnCnt"))

                    ''------- To Attach the Client Id For Building Javascript Array.
                    cs.RegisterArrayDeclaration("txt" + intcnt.ToString, String.Concat("'", e.Row.FindControl("txt" + intcnt.ToString).ClientID, "'"))
                    ''-------- To Handle Image Button ---------------
                    Dim imgBtn As ImageButton = TryCast(e.Row.FindControl("img" + intcnt.ToString), ImageButton)
                    If Not imgBtn Is Nothing Then
                        Dim strStuId As String = e.Row.Cells(1).Text
                        Dim str As String = gvStudents.Columns(intcnt + 2).HeaderText
                        imgBtn.OnClientClick = "javascript:getcomments('" + e.Row.FindControl("txt" + intcnt.ToString).ClientID + "','ALLGENCMTS','" + str + "','" + strStuId + "','" + H_RPT_ID.Value + "'); return false;"
                    End If

                    'Dim spell As RadSpell = TryCast(e.Row.FindControl("spell" + intcnt.ToString), RadSpell)
                    'If Not spell Is Nothing Then
                    '    spell.ControlToCheck = e.Row.FindControl("txt" + intcnt.ToString).ID
                    '    spell.IsClientID = False
                    '    'spell.
                    'End If

                    dsHeader = GetStudComments(e.Row.Cells(1).Text)

                    For Each DsCol As DataColumn In TryCast(gvStudents.DataSource, DataTable).Columns

                        If Not dsHeader Is Nothing AndAlso dsHeader.Tables(0).Rows.Count >= 1 Then
                            For i = 0 To dsHeader.Tables(0).Rows.Count - 1
                                With dsHeader.Tables(0).Rows(i)
                                    If .Item("RSD_RESULT").ToString = "C" Then
                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = .Item("RST_COMMENTS")
                                                    'TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = .Item("FTC_COMMENTS")
                                                End If
                                            End If

                                        End If
                                    ElseIf .Item("RSD_RESULT").ToString = "D" Then
                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    If e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0).GetType.ToString = "System.Web.UI.WebControls.DropDownList" Then
                                                        TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_COMMENTS").ToString

                                                    Else
                                                        TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = .Item("RST_COMMENTS")

                                                    End If
                                                End If
                                            End If

                                        End If
                                        'ElseIf .Item("RSD_RESULT").ToString = "M" Then
                                        '    If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                        '        If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                        '            If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                        '                TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_MARK").ToString
                                        '            End If
                                        '        End If

                                        '    End If
                                        'ElseIf .Item("RSD_RESULT").ToString = "G" Then
                                        '    If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                        '        If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                        '            If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                        '                TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_GRADING").ToString
                                        '            End If
                                        '        End If

                                        '    End If
                                    Else

                                    End If
                                End With
                            Next
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Style.Value = "display:none"
        End If
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(1).Style.Value = "display:none"
        End If
        'Dim lblStudId As New Label
        'Dim txtComments As New TextBox
        'Dim txtCmntsShort As New TextBox
        'Dim strQuery As String
        'Dim ds As DataSet
        'Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'lblStudId = TryCast(e.Row.FindControl("lblStudId"), Label)
        'txtComments = TryCast(e.Row.FindControl("txtComments"), TextBox)
        'txtCmntsShort = TryCast(e.Row.FindControl("txtCmntsShort"), TextBox)

        'If Not lblStudId Is Nothing Then
        '    strQuery = "SELECT RST_COMMENTS FROM RPT.REPORT_STUDENT_S WHERE RST_RSD_ID='" & H_SETUP.Value & "' AND " _
        '                & " RST_ACD_ID='" & H_ACD_ID.Value & "' AND " _
        '                & " RST_GRD_ID='" & H_GRD_ID.Value & "' AND " _
        '                & " RST_STU_ID='" & lblStudId.Text & "'"
        '    ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, strQuery)
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        If txtComments.Visible Then
        '            txtComments.Text = IIf(ds.Tables(0).Rows(0)("RST_COMMENTS") = "NULL", "", ds.Tables(0).Rows(0)("RST_COMMENTS"))
        '        End If
        '        If txtCmntsShort.Visible Then
        '            txtCmntsShort.Text = IIf(ds.Tables(0).Rows(0)("RST_COMMENTS") = "NULL", "", ds.Tables(0).Rows(0)("RST_COMMENTS"))
        '        End If
        '    End If
        'End If

    End Sub





    Private Function SetCommentControls() As String
        Dim strSql As String
        Dim ds As DataSet
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim PnlComment As New Panel
        Dim lblComments As New Label
        Dim txtCmntsShort As New TextBox
        Dim txtComments As New TextBox
        Dim strClass As String = ""
        Try
            If gvStudents.Rows.Count > 0 Then
                strSql = "SELECT RSD_CSSCLASS FROM RPT.REPORT_SETUP_D WHERE RSD_ID='" & H_SETUP.Value & "'"
                ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, strSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    strClass = ds.Tables(0).Rows(0)("RSD_CSSCLASS")


                    If strClass = "textboxsmall" Then
                        For i = 0 To gvStudents.Rows.Count - 1
                            PnlComment = TryCast(gvStudents.Rows(i).FindControl("Panel1"), Panel)
                            lblComments = TryCast(gvStudents.Rows(i).FindControl("lblComments"), Label)
                            txtCmntsShort = TryCast(gvStudents.Rows(i).FindControl("txtCmntsShort"), TextBox)
                            txtComments = TryCast(gvStudents.Rows(i).FindControl("txtComments"), TextBox)
                            txtComments.Text = ""
                            txtComments.Visible = False
                            PnlComment.Visible = False
                            lblComments.Visible = False
                            txtCmntsShort.Visible = True
                        Next
                    ElseIf strClass = "Null" Or strClass = "textboxmulti" Then
                        For i = 0 To gvStudents.Rows.Count - 1
                            PnlComment = TryCast(gvStudents.Rows(i).FindControl("Panel1"), Panel)
                            lblComments = TryCast(gvStudents.Rows(i).FindControl("lblComments"), Label)
                            txtCmntsShort = TryCast(gvStudents.Rows(i).FindControl("txtCmntsShort"), TextBox)
                            txtComments = TryCast(gvStudents.Rows(i).FindControl("txtComments"), TextBox)
                            PnlComment.Visible = True
                            lblComments.Visible = True
                            txtComments.Visible = True
                            txtCmntsShort.Text = ""
                            txtCmntsShort.Visible = False
                        Next
                    Else
                        For i = 0 To gvStudents.Rows.Count - 1
                            PnlComment = TryCast(gvStudents.Rows(i).FindControl("Panel1"), Panel)
                            lblComments = TryCast(gvStudents.Rows(i).FindControl("lblComments"), Label)
                            txtCmntsShort = TryCast(gvStudents.Rows(i).FindControl("txtCmntsShort"), TextBox)
                            txtComments = TryCast(gvStudents.Rows(i).FindControl("txtComments"), TextBox)
                            PnlComment.Visible = False
                            lblComments.Visible = False
                            txtComments.Text = ""
                            txtComments.Visible = False
                            txtCmntsShort.Visible = True
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function

    Sub FillGrade()
        Dim strSql As String
        Dim ds As DataSet
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        'strSql = "SELECT DISTINCT VW_GRADE_M.GRD_ID, VW_GRADE_M.GRD_DISPLAY " _
        '            & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
        '            & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID INNER JOIN GROUPS_TEACHER_S ON SGR_ID= SGS_SGR_ID " _
        '            & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & Session("Current_ACD_ID") & "' and VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "'"
        strSql = "SELECT  DISTINCT   VW_GRADE_M.GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY,RSG_DISPLAYORDER " _
                    & " FROM RPT.REPORTSETUP_GRADE_S INNER JOIN " _
                    & " VW_GRADE_M ON RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID = VW_GRADE_M.GRD_ID " _
                    & " INNER JOIN VW_GRADE_BSU_M ON GRD_ID=GRM_GRD_ID AND GRM_ACD_ID='" + ddlAcdID.SelectedValue.ToString + "'" _
                    & " WHERE RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID='" & H_RPT_ID.Value & "' " _
                    & " ORDER BY RPT.REPORTSETUP_GRADE_S.RSG_DISPLAYORDER"

        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, strSql)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        If Not ddlGrade.SelectedIndex = -1 Then
            H_GRD_ID.Value = ddlGrade.SelectedItem.Value
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlGrade.SelectedIndex = -1 Then
            H_GRD_ID.Value = ddlGrade.SelectedItem.Value
        End If
        FillRepSchedule()
        FillSection()
        BindStudentCombo()
    End Sub

    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlAcdID.SelectedIndex = -1 Then
            H_ACD_ID.Value = ddlAcdID.SelectedItem.Value
        End If
        FillReport()
        FillGrade()
        FillRepSchedule()
        FillSection()
        BindMainHeader()
        BindStudentCombo()
    End Sub

    Private Function AddRecord()
        Dim ldrNew As DataRow
        Dim lintIndex As Integer
        Dim GvHeader As NameValueCollection
        Dim txtCtrl As TextBox
        Dim strVArray() As String
        Dim strColArray() As String
        Dim IntRow As Integer = 0
        Dim IntCols As Integer = 0
        Dim dsHeader As DataSet
        Try
            If ViewState("datamode") = "add" And Not Session("ReportHeader") Is Nothing Then ' Session("ReportHeader")
                GvHeader = Session("ReportHeader")
                strVArray = H_CommentCOLS.Value.Split("|")
                For Each gvRow As GridViewRow In gvStudents.Rows
                    strColArray = strVArray(IntRow).Split(",")
                    IntRow = IntRow + 1
                    If CheckArray(strColArray) = True Then
                        For IntCol As Integer = 3 To gvStudents.Columns.Count - 1
                            If strColArray(IntCol - 3).ToString <> "" Then

                                ldrNew = Session("StuComments").NewRow
                                ldrNew("ID") = gvStudents.Rows.Count + 1

                                ldrNew("RST_RPF_ID") = H_SCH_ID.Value
                                ldrNew("RST_RSD_ID") = GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(0) 'H_SETUP.Value
                                ldrNew("RST_ACD_ID") = H_ACD_ID.Value
                                ldrNew("RST_GRD_ID") = H_GRD_ID.Value
                                'ldrNew("RST_STU_ID") = H_STU_ID.Value & gvRow.Cells(1).Text
                                ldrNew("RST_STU_ID") = gvRow.Cells(1).Text

                                If GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(2) <> "" Then
                                    If GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(2) = "C" Then
                                        'ldrNew("RST_COMMENTS") = strColArray(IntCol - 3).ToString 'txtComments.Text
                                        ldrNew("RST_COMMENTS") = strColArray(IntCol - 3).ToString.Replace("~", ",")
                                    ElseIf GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(2) = "D" Then
                                        ldrNew("RST_COMMENTS") = strColArray(IntCol - 3).ToString
                                    End If
                                End If
                                Session("StuComments").Rows.Add(ldrNew)
                            Else
                                DeleteHeaderComments(H_SCH_ID.Value, GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(0), H_ACD_ID.Value, H_GRD_ID.Value, Convert.ToInt32(gvRow.Cells(1).Text))

                            End If
                        Next
                    Else
                        For IntCol As Integer = 3 To gvStudents.Columns.Count - 1
                            Dim StrSql As String
                            Dim ds As DataSet
                            Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                            Dim iReturnvalue As Integer
                            Dim cmd As New SqlCommand
                            Dim objConn As New SqlConnection(str_con)
                            objConn.Open()
                            Dim stTrans As SqlTransaction = objConn.BeginTransaction
                            Try
                                cmd = New SqlCommand("[RPT].[DeleteREPORT_STUDCOMMENTS_S]", objConn, stTrans)

                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.Parameters.AddWithValue("@RST_RPF_ID", Convert.ToInt32(H_SCH_ID.Value))
                                cmd.Parameters.AddWithValue("@RST_RSD_ID", Convert.ToInt32(GvHeader.Item(gvStudents.Columns(IntCol).HeaderText).Split("_")(0)))
                                cmd.Parameters.AddWithValue("@RST_ACD_ID", Convert.ToInt32(H_ACD_ID.Value))
                                cmd.Parameters.AddWithValue("@RST_GRD_ID", H_GRD_ID.Value)
                                'cmd.Parameters.AddWithValue("@RST_STU_ID", H_STU_ID.Value & Convert.ToInt32(gvRow.Cells(1).Text))
                                cmd.Parameters.AddWithValue("@RST_STU_ID", Convert.ToInt32(gvRow.Cells(1).Text))
                                cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                                cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                                cmd.ExecuteNonQuery()
                                iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                                If iReturnvalue <> 0 Then
                                    stTrans.Rollback()
                                    lblError.Text = "Unexpected error"
                                    Exit Function
                                End If
                                stTrans.Commit()

                            Catch ex As Exception
                                stTrans.Rollback()
                                lblError.Text = "Unexpected error"
                                Exit Function
                            Finally
                                objConn.Close()

                            End Try
                        Next
                    End If
                    'End If
                    'End If
                    'End With
                    'End If
                Next
            End If



            If ViewState("datamode") = "edit" Then
                If Not Session("StuComments") Is Nothing Then
                    For lintIndex = 0 To Session("StuComments").Rows.Count - 1
                        If (Session("StuComments").Rows(lintIndex)("ID") = Session("gintGridLine")) Then

                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function
    Private Function CheckArray(ByVal StrArr() As String) As Boolean
        Dim blnReturn As Boolean = False
        For Each Str As String In StrArr
            If Str <> "" And Str <> "0" And Str <> "--" Then
                blnReturn = True
            End If
        Next
        Return blnReturn
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String = ""
            ViewState("datamode") = "add"
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            If gvStudents.Rows.Count >= 1 Then
                url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
            lblPages.Text = ""

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Function BindBlankRow()

        Dim strSql As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        gvStudents.DataSourceID = ""
        Try
            strSql = "SELECT DISTINCT(STU_NO) " & _
                    " FROM VW_STUDENT_DETAILS " & _
                    " INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID " & _
                    " INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID " & _
                    " INNER JOIN GROUPS_TEACHER_S ON SGS_SGR_ID=SGR_ID " & _
                    " WHERE STU_GRD_ID='" & H_GRD_ID.Value & "' AND STU_ACD_ID=" & ddlAcdID.SelectedValue & " AND SGR_ID='" & H_GRP_ID.Value & "' " & _
                    " AND STU_ID=1 AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            If dsStudents.Tables(0).Rows.Count = 0 Then
                gvStudents.DataSource = dsStudents.Tables(0)
                dsStudents.Tables(0).Rows.Add(dsStudents.Tables(0).NewRow())
                gvStudents.DataBind()
                gvStudents.Columns.Clear()
                gvStudents.DataSource = Nothing
                gvStudents.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function

    Protected Sub btnCanceldata_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            gvStudents.Columns.Clear()
            BindBlankRow()
            ImgGroup.Enabled = True
            btnView.Enabled = True
            txtGroup.Enabled = True
            txtGroup.Text = ""
            'cmbStudent.SelectedItem.Text = ""
            'cmbStudent.SelectedItem.Text = ""
            H_SCH_ID.Value = 0
            cmbStudent.ClearSelection()
            H_GRP_ID.Value = 0
            H_SCT_ID.Value = 0
            ddlReport.Enabled = True
            ddlGrade.Enabled = True
            ddlRepSch.Enabled = True
            ddlSection.Enabled = True
            trGroup.Visible = False
            trSave1.Visible = False
            trSave.Visible = False
            FillAcd()
            FillReport()
            FillGrade()
            FillRepSchedule()
            FillSection()
            lblPages.Text = ""
            BindMainHeader()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Sub FillReport()
        Dim strSQL As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        strSQL = "SELECT RSM_ID,RSM_DESCR " _
                    & " FROM RPT.REPORT_SETUP_M " _
                    & " WHERE " _
                    & " RSM_BSU_ID='" & Session("SBsuid") & "' AND " _
                    & " RSM_ACD_ID='" & H_ACD_ID.Value & "' order by RSM_DISPLAYORDER"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        ddlReport.DataSource = ds
        ddlReport.DataTextField = "RSM_DESCR"
        ddlReport.DataValueField = "RSM_ID"
        ddlReport.DataBind()
        If Not ddlReport.SelectedIndex = -1 Then
            H_RPT_ID.Value = ddlReport.SelectedItem.Value
        End If

    End Sub

    Protected Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlReport.SelectedIndex = -1 Then
            H_RPT_ID.Value = ddlReport.SelectedItem.Value
        End If
        Try
            FillGrade()
            FillRepSchedule()
            FillSection()
            BindMainHeader()
            BindStudentCombo()
        Catch ex As Exception

        End Try
        
    End Sub

    Protected Sub ImgSection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub
    Sub FillRepSchedule()
        Dim str_Sql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_critirea As String = ""
        If H_RPT_ID.Value <> "" Then
            str_critirea = " WHERE RPF_RSM_ID=" & H_RPT_ID.Value & ""
        End If
        str_Sql = "SELECT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M"
        str_Sql = str_Sql & str_critirea & " ORDER BY RPF_DISPLAYORDER"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlRepSch.DataSource = ds
        ddlRepSch.DataTextField = "RPF_DESCR"
        ddlRepSch.DataValueField = "RPF_ID"
        ddlRepSch.DataBind()
        If Not ddlRepSch.SelectedIndex = -1 Then
            H_SCH_ID.Value = ddlRepSch.SelectedItem.Value
        End If

    End Sub

    Protected Sub ddlRepSch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlRepSch.SelectedIndex = -1 Then
            H_SCH_ID.Value = ddlRepSch.SelectedItem.Value
        End If
        FillSection()
        BindStudentCombo()
    End Sub
    Sub FillSection()
        Dim str_Sql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_critirea As String = ""
        If H_ACD_ID.Value <> "" Then
            str_critirea = " AND SCT_ACD_ID='" & H_ACD_ID.Value & "'"
        End If
        If H_GRD_ID.Value <> "" Then
            str_critirea += " AND SCT_GRD_ID='" & H_GRD_ID.Value & "'"
        End If


        str_Sql = "SELECT SCT_ID,SCT_DESCR FROM VW_SECTION_M WHERE " _
                        & " SCT_BSU_ID='" & Session("SBsuid") & "' AND " _
                        & " SCT_DESCR<>'TEMP'"

        If Session("SBSUID") <> "125018" Then
            If ViewState("GRD_ACCESS") > 0 And Session("CurrSuperUser") = "Y" Then
                str_Sql += " AND ( SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE GSA_ACD_ID='" + H_ACD_ID.Value + "' AND GSA_USR_ID = '" & Session("sUsr_id") & "'), '|')) OR SCT_EMP_ID='" & Session("EmployeeID") + "')"
            Else
                If Session("EmployeeID") <> "" And Session("CurrSuperUser") <> "Y" Then
                    str_critirea += " AND SCT_EMP_ID='" & Session("EmployeeID") & "'"
                End If
            End If
        End If



        str_Sql = str_Sql + str_critirea + " ORDER BY SCT_DESCR"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If Not ddlSection.SelectedIndex = -1 Then
            H_SCT_ID.Value = ddlSection.SelectedItem.Value
        End If

    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlSection.SelectedIndex = -1 Then
            H_SCT_ID.Value = ddlSection.SelectedItem.Value
        End If
        cmbStudent.ClearSelection()
        BindStudentCombo()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "edit" Then
                'UpdateMarksentry()


            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuComments") Is Nothing Then
                    AddRecord()
                    SaveRecord()
                    gvStudents.Columns.Clear()

                    If CInt(ViewState("PageNo")) < CInt(ViewState("TotalPages")) Then
                        ViewState("PageNo") = CInt(ViewState("PageNo")) + 1
                    End If
                    lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")

                    BindStudents()
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String = ""
            ViewState("datamode") = "add"
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            If gvStudents.Rows.Count >= 1 Then
                url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
            lblError.Text = ""

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Function GetHeaderValues(ByVal ddlList As DropDownList, ByVal RSDID As String) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim dsHeader As DataSet
        Dim StrSql As String
        'Dim StrSql As String = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RPT_ID.Value & " AND RSP_RSD_ID=" & RSDID & " UNION SELECT '--' as RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S "

        '  Select Case Session("sbsuId")
        '      Case "125017", "111001", "121012", "121013", "131001", "141001", "151001", "123006", "133006", "126008"

        '      StrSql = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RPT_ID.Value & " AND RSP_RSD_ID=" & RSDID & " ORDER BY RSP_DISPLAYORDER"
        StrSql = "SELECT RSP_DESCR FROM(SELECT RSP_DESCR,isnull(RSP_DISPLAYORDER,0) as RSP_DISPLAYORDER FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RPT_ID.Value & " AND RSP_RSD_ID=" & RSDID _
      & " UNION ALL SELECT '--',-1" _
              & " )PP ORDER BY RSP_DISPLAYORDER"
        'Case Else
        '    StrSql = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RPT_ID.Value & " AND RSP_RSD_ID=" & RSDID & " UNION SELECT '--' as RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S "
        ' End Select



        dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSql)

        ddlList.DataSource = dsHeader
        ddlList.DataTextField = "RSP_DESCR"
        ddlList.DataValueField = "RSP_DESCR"
        ddlList.DataBind()
        ddlList.Font.Size = "10"
        Session("ddlListSelValue") = ddlList.Items(0).Value.ToString
        ddlList.ClearSelection()
        Return ddlList

    End Function
    Private Function DeleteHeaderComments(ByVal RPFID As Integer, ByVal RSDID As Integer, ByVal ACDID As Integer, ByVal GRDID As String, ByVal STUID As Integer)
        Try
            'Dim strSQL As String
            'strSQL = "DELETE FROM RPT.REPORT_STUDENT_S WHERE RST_RPF_ID=" & RPFID & " " & _
            '         " AND RST_RSD_ID=" & RSDID & " AND RST_ACD_ID=" & ACDID & " AND RST_GRD_ID='" & GRDID & "' AND RST_STU_ID=" & STUID & ""

            'Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            'Dim IntResult As Integer
            'IntResult = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSQL)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Function



    Protected Sub btnSaveDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDetails.Click
        Try
            If ViewState("datamode") = "edit" Then
                'UpdateMarksentry()


            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuComments") Is Nothing Then
                    AddRecord()
                    SaveRecord()
                    gvStudents.Columns.Clear()

                    BindStudents()
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlRepSch_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRepSch.SelectedIndexChanged

        BindMainHeader()

    End Sub

    '------------------------STUDENT START--------------------------------------------
    Protected Sub cmbStudent_DataBound(sender As Object, e As EventArgs)
        'set the initial footer label
        CType(cmbStudent.Footer.FindControl("RadComboItemsCount"), Literal).Text = Convert.ToString(cmbStudent.Items.Count)
    End Sub
    Protected Sub cmbStudent_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        'set the Text and Value property of every item
        'here you can set any other properties like Enabled, ToolTip, Visible, etc.
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("StudentName").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("stu_id").ToString()
    End Sub


    Protected Sub BindStudentCombo()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@ACD_ID", ddlAcdID.SelectedValue.ToString)
        param(2) = New SqlParameter("@GRDID", grade(0))
        param(3) = New SqlParameter("@SCTID", ddlSection.SelectedItem.Value)

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.LOAD_STUDENTS_COMBO", param)
        cmbStudent.DataSource = ds
        cmbStudent.DataBind()
    End Sub

    Function GetCheckedStudent() As String
        Dim str As String = ""
        Dim collection As IList(Of RadComboBoxItem) = cmbStudent.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                str += item.Value
                If str <> "" Then
                    str += "|"
                End If
            Next
            str = str.TrimEnd("|")
        Else
            If cmbStudent.SelectedIndex > -1 Then
                str = cmbStudent.SelectedItem.Value
            Else
                str = ""
            End If
        End If
        Return str

    End Function

    Protected Sub txtCommenttxt_TextChanged(sender As Object, e As EventArgs)

    End Sub
End Class
