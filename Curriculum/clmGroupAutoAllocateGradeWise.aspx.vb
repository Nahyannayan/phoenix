Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmGroupAutoAllocateGradeWise
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100120") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    PopulateGradeStream()
                    h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    GridBind()
                    If Session("multistream") = 0 Then
                        tb1.Rows(3).Visible = False
                    End If

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        Else
            studClass.SetChk(gvSubjects, Session("liUserList"))
        End If
    End Sub
    Protected Sub btnSubject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnParent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub PopulateGradeStream()

        'ddlStream.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
        '                         & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                         & " grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade.SelectedValue.ToString + "'"
        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'ddlStream.DataSource = ds
        'ddlStream.DataTextField = "stm_descr"
        'ddlStream.DataValueField = "stm_id"
        'ddlStream.DataBind()
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    'Sub PopulateSubjectTree()
    '    Dim ds As New DataSet
    '    Dim sbgIds As New ArrayList
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT DISTINCT A.SBG_ID AS SBG_ID,A.SBG_DESCR AS SBG_DESCR ,CASE A.SBG_PARENT_ID " _
    '                              & " WHEN 0 THEN NULL ELSE B.SBG_ID END AS PARENT_ID  FROM SUBJECTS_GRADE_S AS A " _
    '                              & " LEFT OUTER JOIN SUBJECTS_GRADE_S AS B ON A.SBG_PARENT_ID=B.SBG_ID AND A.SBG_GRD_ID=B.SBG_GRD_ID " _
    '                              & " AND A.SBG_STM_ID=B.SBG_STM_ID AND A.SBG_ACD_ID=B.SBG_ACD_ID " _
    '                              & " WHERE A.SBG_GRD_ID='" + ddlGrade.SelectedValue + "' AND A.SBG_STM_ID =" + ddlStream.SelectedValue.ToString + " AND A.SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
    '                              & " AND A.SBG_bOPTIONAL='FALSE'"

    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ds.DataSetName = "SUBJECTS"
    '    ds.Tables(0).TableName = "SUBJECT"
    '    Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("SUBJECT").Columns("SBG_ID"), ds.Tables("SUBJECT").Columns("PARENT_ID"), True)
    '    relation.Nested = True
    '    ds.Relations.Add(relation)
    '    XmlSubjects.Data = ds.GetXml()
    '    tvSubjects.DataBind()

    '    str_query = "SELECT DISTINCT SGR_SBG_ID FROM GROUPS_M  " _
    '             & " WHERE SGR_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
    '             & " AND SGR_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString

    '    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
    '    While reader.Read
    '        DisableSubject(reader.GetValue(0))
    '    End While

    'End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub
    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvSubjects.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSubjects.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvSubjects.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR,SBG_PARENTS FROM SUBJECTS_GRADE_S WHERE SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                  & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_STM_ID=" _
                                  & ddlStream.SelectedValue.ToString + "AND SBG_ID NOT IN (SELECT " _
                                  & " SGR_SBG_ID FROM GROUPS_M WHERE SGR_GRD_ID='" + ddlGrade.SelectedValue.ToString _
                                  & "' AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND SGR_STM_ID=" + ddlStream.SelectedValue.ToString + ") AND SBG_bOPTIONAL='FALSE'" _
                                  & " AND ISNULL(SBG_bAUTOGROUP,'FALSE')='TRUE'"

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim subjectSearch As String = ""
        Dim shortSearch As String = ""

        Dim txtSearch As New TextBox
        Dim ddlgvlang As New DropDownList
        Dim selectedLang As String = ""

        If gvSubjects.Rows.Count > 0 Then

            txtSearch = gvSubjects.HeaderRow.FindControl("txtSubject")
            strSidsearch = h_selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("SBG_DESCR", txtSearch.Text, strSearch)
            subjectSearch = txtSearch.Text

            txtSearch = gvSubjects.HeaderRow.FindControl("txtParent")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SBG_PARENTS", txtSearch.Text, strSearch)
            shortSearch = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If
        End If

        str_query += " ORDER BY SBG_DESCR,SBG_PARENTS"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvSubjects.DataBind()
            Dim columnCount As Integer = gvSubjects.Rows(0).Cells.Count
            gvSubjects.Rows(0).Cells.Clear()
            gvSubjects.Rows(0).Cells.Add(New TableCell)
            gvSubjects.Rows(0).Cells(0).ColumnSpan = columnCount
            gvSubjects.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvSubjects.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvSubjects.DataBind()
        End If

        txtSearch = gvSubjects.HeaderRow.FindControl("txtSubject")
        txtSearch.Text = subjectSearch

        txtSearch = gvSubjects.HeaderRow.FindControl("txtParent")
        txtSearch.Text = shortSearch
        studClass.SetChk(gvSubjects, Session("liUserList"))
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim sbgXml As String = GetSubjectXML()
        If sbgXml = "" Then
            lblError.Text = "Please select a subject"
        End If
        str_query = "exec saveGROUPS_AUTOALLOCATEGRADEWISE '" + ddlGrade.SelectedValue.ToString + "'," + ddlAcademicYear.SelectedValue.ToString + ",'" + sbgXml + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
       
        'End With
    End Sub

    Function GetSubjectXML() As String
        Dim row As GridViewRow
        Dim str As String = ""
        Dim lblSbgId As Label
        Dim chkSelect As CheckBox
        For Each row In gvSubjects.Rows
            chkSelect = row.FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblSbgId = row.FindControl("lblSbgId")
                str += "<ID><SBG_ID>" + lblSbgId.Text + "</SBG_ID></ID>"
            End If
        Next
        If str <> "" Then
            str = "<IDS>" + str + "</IDS>"
        End If
        Return str
    End Function



#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            PopulateGradeStream()
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString, "", ddlStream.SelectedValue)
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            'PopulateGradeStream()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Try
            ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString, "", ddlStream.SelectedValue)
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt("C100050")
            Dim url As String
            url = String.Format("~\Curriculum\clmGroup_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

   
End Class
