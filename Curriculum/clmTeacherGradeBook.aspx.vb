﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.IO
Imports GemBox.Spreadsheet


Partial Class Curriculum_clmTeacherGradeBook
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320011") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    If Session("susr_name") = "dhanya" Or Session("susr_name") = "charan" Then
                        hfEMP_ID.Value = "2325"
                    Else
                        hfEMP_ID.Value = Session("EmployeeId")
                    End If
                    '  "3201"
                    'Session("CURRENT_ACD_ID") = "1279"
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindGrade()
                    BindGroup()
                    BindTerm()
                    trDetails.Visible = False
                    trSave.Visible = False
                    'Dim lblSgrId As Label
                    'lblSgrId = dlGroups.Items(0).FindControl("lblSgrId")
                    'hfSGR_ID.Value = lblSgrId.Text
                    'getSubject(lblSgrId.Text)
                    'BindAssmt()
                    'GridBind()


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExport)

    End Sub

    Protected Sub btnGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        trDetails.Visible = True
        trSave.Visible = True
        Dim lblSgrId As Label = TryCast(sender.findcontrol("lblSgrId"), Label)
        hfSGR_ID.Value = lblSgrId.Text
        trDetails.Visible = True
        getSubject(lblSgrId.Text)
        lblDetails.Text = "Subject : " + hfSubject.Value + "&nbsp;&nbsp;&nbsp;Group : " + hfGroup.Value + ""
        BindAssmt()
        GridBind()
    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvAssmt.Rows(Val(lblindex.Text)).Visible = False
            MPOI.Show()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            ' lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub getSubject(ByVal sgr_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_SBM_ID,SBG_ID,SBG_GRD_ID,SBG_DESCR,SGR_DESCR FROM SUBJECTS_GRADE_S INNER JOIN " _
                               & " GROUPS_M ON SGR_SBG_ID=SBG_ID WHERE SGR_ID=" + sgr_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        hfSBG_ID.Value = ds.Tables(0).Rows(0).Item(1)
        hfSBM_ID.Value = ds.Tables(0).Rows(0).Item(0)
        hfGRD_ID.Value = ds.Tables(0).Rows(0).Item(2)
        hfSubject.Value = ds.Tables(0).Rows(0).Item(3)
        hfGroup.Value = ds.Tables(0).Rows(0).Item(4)
    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "GBM_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "GBM_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "GBM_SHORT"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "GBM_MAXMARK"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "GBM_DATE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "GBM_bLOCK"
        dt.Columns.Add(column)

        Return dt
    End Function

    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACd_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub

    Sub BindAssmt()

        Try
            Dim dt As DataTable = SetDataTable()
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String

            str_query = "SELECT GBM_ID,GBM_DESCR,GBM_SHORT,GBM_MAXMARK,GBM_DATE,ISNULL(GBM_bLOCK,'FALSE') GBM_bLOCK FROM ACT.TEACHER_GRADEBOOK_M" _
                                  & " WHERE GBM_EMP_ID=" + hfEMP_ID.Value + " AND GBM_SBG_ID=" + hfSBG_ID.Value _
                                  & " AND GBM_SGR_ID=" + hfSGR_ID.Value + " AND GBM_TRM_ID=" + ddlTerm.SelectedValue.ToString

            Dim i As Integer
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim dr As DataRow
            For i = 0 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    dr = dt.NewRow
                    dr.Item(0) = .Item(0)
                    dr.Item(1) = .Item(1)
                    dr.Item(2) = "edit"
                    dr.Item(3) = i.ToString
                    dr.Item(4) = .Item(2)
                    dr.Item(5) = .Item(3)
                    dr.Item(6) = Format(.Item(4), "dd/MMM/yyyy")
                    dr.Item(7) = .Item(5).ToString
                    dt.Rows.Add(dr)
                End With
            Next


            'add empty rows to show 50 rows
            For i = 0 To 50 - ds.Tables(0).Rows.Count
                dr = dt.NewRow
                dr.Item(0) = "0"
                dr.Item(1) = ""
                dr.Item(2) = "add"
                dr.Item(3) = (ds.Tables(0).Rows.Count + i).ToString
                dr.Item(4) = ""
                dr.Item(5) = ""
                dr.Item(7) = "false"
                dt.Rows.Add(dr)
            Next

            Session("dtAssmt") = dt



            gvAssmt.DataSource = dt
            gvAssmt.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try

    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String



        str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                      & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                      & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                      & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                      & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                      & " AND SGS_EMP_ID=" + hfEMP_ID.Value _
                      & " AND SGS_TODATE IS NULL" _
                      & " ORDER BY GRD_DISPLAYORDER"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
            & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
            & " AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'" _
            & " AND SGS_EMP_ID=" + hfEMP_ID.Value

        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND SGR_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        End If
        str_query += " ORDER BY SGR_DESCR"

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlGroups.DataSource = ds
        dlGroups.DataBind()
    End Sub

    Sub SaveAssessment()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim mode As String = ""
        Dim lblDelete As Label
        Dim lblGbmId As Label
        Dim txtDescr As TextBox
        Dim txtShort As TextBox
        Dim txtMax As TextBox
        Dim txtDate As TextBox
        Dim chkBlock As CheckBox

        For i = 0 To gvAssmt.Rows.Count - 1
            With gvAssmt.Rows(i)
                lblDelete = .FindControl("lblDelete")
                lblGbmId = .FindControl("lblGbmId")
                txtDescr = .FindControl("txtDescr")
                txtShort = .FindControl("txtShort")
                txtMax = .FindControl("txtMax")
                txtDate = .FindControl("txtDate")
                chkBlock = .FindControl("chkBlock")

                If lblDelete.Text = "1" Then
                    mode = "delete"
                ElseIf lblGbmId.Text = "0" And txtDescr.Text <> "" Then
                    mode = "add"
                ElseIf txtDescr.Text <> "" Then
                    mode = "edit"
                Else
                    mode = ""
                End If
            End With

            If mode = "edit" Then
                UtilityObj.InsertAuditdetails(str_conn, "edit", "ACT.TEACHER_GRADEBOOK_M", "GBM_ID", "GBM_ID", "GBM_ID=" + lblGbmId.Text)
            End If
            If mode <> "" Then
                str_query = "exec ACT.saveTEACHER_GRADEBOOK_M " _
                        & "@GBM_ID=" + lblGbmId.Text + "," _
                        & "@GBM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                        & "@GBM_GRD_ID='" + hfGRD_ID.Value + "'," _
                        & "@GBM_SBG_ID=" + hfSBG_ID.Value + "," _
                        & "@GBM_SGR_ID=" + hfSGR_ID.Value + "," _
                        & "@GBM_EMP_ID=" + hfEMP_ID.Value + "," _
                        & "@GBM_TRM_ID=" + ddlTerm.SelectedValue.ToString + "," _
                        & "@GBM_DESCR='" + txtDescr.Text + "'," _
                        & "@GBM_SHORT='" + txtShort.Text + "'," _
                        & "@GBM_MAXMARK='" + txtMax.Text + "'," _
                        & "@GBM_DATE='" + txtDate.Text + "'," _
                        & "@GBM_bLOCK='" + chkBlock.Checked.ToString + "'," _
                        & "@MODE='" + mode + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next



    End Sub

    Sub GridBind()


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC [ACT].[getTEACHERGRADEBOOK] " _
                               & "@ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                               & "@GRD_ID='" + hfGRD_ID.Value + "'," _
                               & "@EMP_ID=" + hfEMP_ID.Value + "," _
                               & "@SBG_ID=" + hfSBG_ID.Value + "," _
                               & "@SGR_ID=" + hfSGR_ID.Value + "," _
                               & "@TRM_ID=" + ddlTerm.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                gvMarks.DataSource = ds
                gvMarks.DataBind()


                Dim i, j As Integer
                For i = 0 To gvMarks.Rows.Count - 1
                    If gvMarks.Rows(i).Cells(2).Text.Contains("(TC)") Or gvMarks.Rows(i).Cells(2).Text.Contains("(SO)") Then
                        gvMarks.Rows(i).Cells(2).ForeColor = Drawing.Color.Red
                    End If

                    For j = 3 To gvMarks.Rows(i).Cells.Count - 1
                        Dim str_Data0 As String
                        Dim strArr0() As String
                        Dim hdr_mark As Integer
                        str_Data0 = gvMarks.HeaderRow.Cells(j).Text
                        strArr0 = str_Data0.Split(")")
                        str_Data0 = strArr0(0)
                        strArr0 = str_Data0.Split("(")
                        hdr_mark = Integer.Parse(strArr0(1))

                        Dim str As String() = gvMarks.Rows(i).Cells(j).Text.Split("|")
                        Dim txtMarks As New TextBox

                        txtMarks.ID = "txtMarks" + j.ToString
                        txtMarks.Text = str(0) ' gvMarks.Rows(i).Cells(j).Text.Replace("&nbsp;", "")
                        txtMarks.Width = 40
                        If str(0) = "AB" Or str(0) = "L" Then
                            txtMarks.ForeColor = Drawing.Color.Red
                        End If

                        '  e.Row.Cells(i).Controls.Clear()
                        Dim txtGbmId As New TextBox
                        txtGbmId.ID = "txtGbmId" + j.ToString
                        txtGbmId.Text = str(1)
                        txtGbmId.BorderStyle = BorderStyle.None
                        txtGbmId.Attributes.Add("style", "display:none")

                        Dim lblerrgrd As New Label
                        lblerrgrd.ID = "lblErrGrid" + j.ToString
                        lblerrgrd.Text = "Value Entered is Higher"
                        lblerrgrd.BorderStyle = BorderStyle.None
                        lblerrgrd.Attributes.Add("style", "display:none")
                        lblerrgrd.CssClass = "error"


                        txtMarks.Attributes.Add("onblur", "javascript:return checkNum(this);")
                        txtMarks.Attributes.Add("onchange", "javascript:return onchangetxt(" + hdr_mark.ToString + "," + (i + 2).ToString + "," + j.ToString + ");")
                        txtMarks.Attributes.Add("onkeydown", "javascript:ScrollDown(" + (i + 2).ToString + "," + j.ToString + ",event);")
                        txtMarks.CssClass = "inputmark"
                        gvMarks.Rows(i).Cells(j).Controls.Add(txtMarks)
                        gvMarks.Rows(i).Cells(j).Controls.Add(txtGbmId)
                        gvMarks.Rows(i).Cells(j).Controls.Add(lblerrgrd)
                        If str(2).ToLower = "true" Or str(2).ToLower = "1" Then
                            gvMarks.Rows(i).Cells(j).Enabled = False
                            'gvMarks.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                            ' txtMarks.CssClass = "inputmark_disable"
                            ' txtMarks.ForeColor = System.Drawing.Color.Black
                            ' txtMarks.ReadOnly = True
                        End If

                        ' txtGbmId.Visible = False
                    Next
                Next
                'For j = 2 To gvMarks.FooterRow.Cells.Count - 1
                '    Dim btn As New Button
                '    btn.ID = "btnSave" + i.ToString
                '    btn.Text = "Save"
                '    gvMarks.FooterRow.Cells(j).Controls.Add(btn)
                'Next
                gvMarks.FooterRow.Visible = True
            Else
                gvMarks.DataBind()
            End If
        Else
            gvMarks.DataBind()
        End If

        If gvMarks.Rows.Count = 0 Then
            btnSaveMarks.Visible = False
            btnExport.Visible = False
        Else
            btnSaveMarks.Visible = True
            btnExport.Visible = True
        End If
    End Sub


    Sub ExportExcel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC [ACT].[getTEACHERGRADEBOOK] " _
                           & "@ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                           & "@GRD_ID='" + hfGRD_ID.Value + "'," _
                           & "@EMP_ID=" + hfEMP_ID.Value + "," _
                           & "@SBG_ID=" + hfSBG_ID.Value + "," _
                           & "@SGR_ID=" + hfSGR_ID.Value + "," _
                           & "@TRM_ID=" + ddlTerm.SelectedValue.ToString + "," _
                           & "@TYPE='EXCEL'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable

        dt = ds.Tables(0)

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        'ws.InsertDataTable(dt, "A1", True)

        'ef.SaveXls(tempFileName)
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        ef.Save(tempFileName)

        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileName)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        System.IO.File.Delete(tempFileName)
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindGroup()
        BindTerm()
        BindAssmt()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindGroup()
        BindAssmt()
    End Sub

    Protected Sub lnkAssmt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAssmt.Click
        MPOI.Show()
    End Sub


    Protected Sub btnSaveAssmt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAssmt.Click
        SaveAssessment()
        BindAssmt()
        GridBind()
    End Sub

    Protected Sub gvMarks_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMarks.RowCreated
        e.Row.Cells(0).Visible = False
        Dim i As Integer
        If e.Row.RowType = DataControlRowType.DataRow Then


            For i = 3 To e.Row.Cells.Count - 1

                Dim txtMarks As New TextBox
                txtMarks.ID = "txtMarks" + i.ToString
                txtMarks.Attributes.Add("onblur", "javascript:return checkNum(this);")
                txtMarks.CssClass = "inputmark"
                '  txtMarks.Text = str(0) 'e.Row.Cells(i).Text.Replace("&nbsp;", "")
                txtMarks.Width = 40
                Dim txtGbmId As New TextBox
                txtGbmId.ID = "txtGbmId" + i.ToString
                '   lblGbmId.Text = str(1)
                ' lblGbmId.Visible = False
                '  e.Row.Cells(i).Controls.Clear()
                txtGbmId.Attributes.Add("style", "display:none")

                Dim lblerrgrd As New Label
                lblerrgrd.ID = "lblErrGrid" + i.ToString
                lblerrgrd.Attributes.Add("style", "display:none")

                e.Row.Cells(i).Controls.Add(txtMarks)
                e.Row.Cells(i).Controls.Add(txtGbmId)
                e.Row.Cells(i).Controls.Add(lblerrgrd)
            Next
        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            For i = 3 To e.Row.Cells.Count - 1
                Dim btn As New Button
                btn.ID = "btnSave" + i.ToString
                btn.Text = "Save"
                btn.CssClass = "button"
                btn.Font.Size = 7
                btn.Height = 20
                btn.Width = 45
                'btn.
                AddHandler btn.Click, AddressOf Me.btn_click
                e.Row.Cells(i).Controls.Add(btn)
            Next
        End If
    End Sub

    Private Sub btn_click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btn As Button = DirectCast(sender, Button)
        Dim index As Integer = CInt(Replace(btn.ID, "btnSave", ""))
        SaveMarks(index)
        GridBind()
    End Sub
    Protected Sub gvMarks_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim hdr_mark As Integer
        '    Dim itm_mark As Integer
        '    Dim str_Data0 As String
        '    Dim strArr0() As String

        '    Dim k As Integer = e.Row.Cells.Count

        '    If k >= 0 Then
        '        Dim mark As String = e.Row.Cells.Item(k).Text
        '        str_Data0 = gvMarks.HeaderRow.Cells(k).Text
        '        strArr0 = str_Data0.Split(")")
        '        str_Data0 = strArr0(0)
        '        strArr0 = str_Data0.Split("(")
        '        hdr_mark = Integer.Parse(strArr0(1))
        '        If mark = "" Then
        '            mark = "0"
        '        End If
        '        itm_mark = Integer.Parse(mark)
        '        If itm_mark <= hdr_mark Then
        '            lblError.Text = "Some Values are marked with higher marks. Please correct it!!!"

        '        End If
        '    End If
        'End If
    End Sub
    Sub SaveMarks(ByVal index As Integer)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim hdr_mark As Integer
        Dim itm_mark As String
        Dim str_Data0 As String
        Dim strArr0() As String

        Dim i As Integer
        Dim txtMarks As TextBox
        Dim txtGbmId As TextBox

        Dim strMarks As String
        Dim strAttendance As String
        Dim strStuId As String

        For i = 0 To gvMarks.Rows.Count - 1

            str_Data0 = gvMarks.HeaderRow.Cells(index).Text
            strArr0 = str_Data0.Split(")")
            str_Data0 = strArr0(0)
            strArr0 = str_Data0.Split("(")
            hdr_mark = Integer.Parse(strArr0(1))

            txtMarks = gvMarks.Rows(i).Cells(index).FindControl("txtMarks" + index.ToString)
            txtGbmId = gvMarks.Rows(i).Cells(index).FindControl("txtGbmId" + index.ToString)

            If txtMarks.Text = "" Then
                txtMarks.Text = "0"
            End If
            itm_mark = (txtMarks.Text)
            If IIf(IsNumeric(itm_mark), itm_mark, hdr_mark) > hdr_mark Then
                lblError.Text = "Some Values are marked with higher marks. Please correct!!!"
            Else

                strStuId = gvMarks.Rows(i).Cells(0).Text
                strMarks = "0"
                strAttendance = "P"
                If txtMarks.Text = "" Then
                    strMarks = "0"
                ElseIf txtMarks.Text.ToUpper = "AB" Or txtMarks.Text.ToString.ToUpper = "L" Then
                    strAttendance = txtMarks.Text.ToUpper
                ElseIf IsNumeric(txtMarks.Text) = True Then
                    strMarks = txtMarks.Text
                End If

                str_query = "exec ACT.saveGRADEBOOKMARKS " _
                                & " @GBM_ID =" + txtGbmId.Text + "," _
                                & " @GBD_STU_ID=" + strStuId + "," _
                                & " @GBD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                & " @GBD_GRD_ID=" + hfGRD_ID.Value + "," _
                                & " @GBD_SBG_ID=" + hfSBG_ID.Value + "," _
                                & " @GBD_SGR_ID=" + hfSGR_ID.Value + "," _
                                & " @GBD_MARKS=" + strMarks + "," _
                                & " @GBD_ATTENDANCE='" + strAttendance + "'," _
                                & " @GBD_USER='" + Session("susr_name") + "'"

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next

    End Sub

    Protected Sub btnSaveMarks_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMarks.Click
        Dim i As Integer
        For i = 3 To gvMarks.Rows(0).Cells.Count - 1
            SaveMarks(i)
        Next
        GridBind()
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        If hfSBG_ID.Value <> "" And hfSGR_ID.Value <> "" Then
            BindAssmt()
            GridBind()
        End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ExportExcel()
    End Sub

    Protected Sub lnkCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopy.Click
        Dim url As String = "~/Curriculum/clmTeacherGradeBook_CopyMarks.aspx?" _
                        & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                        & "&empid=" + Encr_decrData.Encrypt(hfEMP_ID.Value) _
                        & "&sgrid=" + Encr_decrData.Encrypt(hfSGR_ID.Value) _
                        & "&sbgid=" + Encr_decrData.Encrypt(hfSBG_ID.Value) _
                        & "&trmid=" + Encr_decrData.Encrypt(hfEMP_ID.Value) _
                        & "&group=" + Encr_decrData.Encrypt(lblDetails.Text)
        ResponseHelper.Redirect(url, "_blank", "")
    End Sub
End Class
