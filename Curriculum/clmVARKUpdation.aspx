﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="clmVARKUpdation.aspx.vb" Inherits="Curriculum_clmVARKUpdation" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">   
   
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="VARK Updation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label" >Academic Year</span></td>
                        <td align="left" >    
                        <asp:DropDownList ID="ddlAca_Year"  runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" valign="middle" width="20%">
                            <span class="field-label">Grade</span></td>
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList>                            
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">Section</span></td>
                        <td align="left" >
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>                   
                </table>
                <br />
                <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="HT1" runat="server" HeaderText="VARK Updation">
                        <ContentTemplate>
                            <div style="overflow: auto">
                                <table id="tbl_AddGroup" runat="server" align="center" cellpadding="0" cellspacing="0" width="100%">
                                    <tr id="Tr4" runat="server">
                                        <td id="Td4" runat="server">
                                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" SkinID="error" ></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="Tr2" runat="server">
                                        <td id="Td2" align="left" runat="server">
                                            <asp:GridView ID="gvComments" runat="server" AutoGenerateColumns="False"
                                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                Width="100%" CssClass="table table-bordered table-row">
                                                <RowStyle  />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="CMTID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="cmtId" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student ID">
                                                        <HeaderTemplate>
                                                            <div align="center">
                                                                <asp:Label ID="lblopt" runat="server" Text="Student ID"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtOption" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnEmpid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEmpid_Search_Click"  />
                                                            </div>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSubId" runat="server" Text='<%# Bind("STU_no") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <HeaderTemplate>
                                                            <div align="center">
                                                                <asp:Label ID="lblopt1" runat="server" Text="Name"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtOption1" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnstuname_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnstuname_Search_Click"  />
                                                            </div>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("STU_name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle></HeaderStyle>
                                                        <ItemStyle></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VARK">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlPrefer" runat="server" Text='<%# bind("vk_type") %>'>
                                                                <asp:ListItem>--</asp:ListItem>
                                                                <asp:ListItem>Visual</asp:ListItem>
                                                                <asp:ListItem>Auditory</asp:ListItem>
                                                                <asp:ListItem>Read/Write</asp:ListItem>
                                                                <asp:ListItem>Kinesthetic</asp:ListItem>
                                                                <asp:ListItem>Multimodal</asp:ListItem>
                                                                <asp:ListItem>Visual/Kinesthetic</asp:ListItem>
                                                                <asp:ListItem>Auditory/Kinesthetic</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle></HeaderStyle>
                                                        <ItemStyle></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle />
                                                <HeaderStyle />
                                                <AlternatingRowStyle />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr id="Tr1" runat="server">
                                        <td id="Td1" align="center" colspan="12" runat="server">
                                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />&nbsp;
                                <input id="h_Selected_menu_1" runat="server" type="hidden" value="="></input>
                                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="="></input></input></input>

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Upload">
                        <ContentTemplate>
                            <table align="center" width="100%">
                                <tr>
                                    <td>
                                        <table width="100%" id="Table3">
                                            <tr>
                                                <td colspan="3">
                                                    <tr id="Tr3" runat="server">
                                                        <td id="Td3" runat="server" colspan="3">
                                                            <asp:Label ID="Label1" runat="server" Text="Please upload the excel with column names as StudentID,StudentName,VARK"
                                                                CssClass="error" EnableViewState="False" SkinID="error" Style="text-align: center"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <asp:Label ID="lblerror3" runat="server" CssClass="error" EnableViewState="False"
                                                        SkinID="error" Style="text-align: left"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title-bg-light" colspan="3" align="left" class="title">Import
                                                </td>
                                                <tr>
                                                    <td align="left"><span class="field-label">Select File</span> 
                                                    </td>
                                                    <td align="left">
                                                        <asp:FileUpload ID="uploadFile" class="field-label" runat="server" BorderStyle="None" EnableTheming="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" align="center">
                                                        <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload"
                                                            OnClick="btnUpload_Click" CausesValidation="False" />
                                                    </td>
                                                </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>

            </div>
        </div>
    </div>
</asp:Content>

