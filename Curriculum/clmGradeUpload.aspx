<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGradeUpload.aspx.vb" Inherits="Curriculum_clmGradeUpload" Title="Untitled Page" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        /*div.RadUpload .ruBrowse, div.RadUpload .ruBrowse .ruButtonHover {
           
            background-position: 0 -23px !important;
            height: 30px !important;
            width: 150px !important;
            color: #fff;
            background-color: #8dc24c !important;
            border-color: #82b744 !important;
            -webkit-appearance: button !important;
            display: inline-block !important;
            font-weight: 400 !important;
            text-align: center !important;
            white-space: nowrap !important;
            vertical-align: middle !important;
            user-select: none !important;
            border: 1px solid transparent !important;
            padding: 0.375rem 0.75rem !important;
            font-size: 1rem !important;
            line-height: 1.5 !important;
            border-radius: 0.25rem !important;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out !important;
            text-transform: none !important;
            overflow: visible !important;
        }
         

       div.RadUpload .ruFakeInput{
            border-width: 1px !important;
            border-style: Solid !important;
            font-weight: normal !important;
            border-radius: 6px !important;
         
            width: 200px !important;
            height:30px !important; 
        }*/ 
        .RadUpload .ruBrowse {
text-align:left !important;
width:200px !important;
}


.RadUpload .ruFakeInput {
height:18px !important;
}
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Upload Report Card
        </div>
        <div class="card-body">
            <div class="alert alert-danger d-none col-6  alert-dismissible" id="divError">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                 <span id="spnError"></span>
            </div>
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td valign="top">
                            <table align="center" cellpadding="5" cellspacing="0" width="100%"> 
                                <tr>
                                    <td align="left"><span class="field-label">Academic Year</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td width="5%"></td>
                                    <td align="left"><span class="field-label">Select Report Card</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr> 
                                <tr>
                                    <td align="left"><span class="field-label">Select Report Schedule</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                     <td width="5%"></td>
                                    <td align="left"><span class="field-label">Select Term</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlTerm" runat="server">
                                        </asp:DropDownList></td>
                                </tr> 
                                <tr>
                                    <td align="left"><span class="field-label">Select Grade</span></td> 
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                     <td width="5%"></td>
                                </tr>
                                <tr> 
                                  <td align="left" style="vertical-align:top!important">
                                      <span class="field-label">Select Files</span>
                                     
                                  </td> 
                                    <td>
                                        Total Files:<span class="badge badge-secondary" id="spnTotal">0</span>
                                        <div class="demo-container size-narrow"> 
                                        <telerik:radprogressarea rendermode="Lightweight" runat="server" id="RadProgressArea1" /> 
                                            <telerik:radasyncupload rendermode="Lightweight" runat="server" id="AsyncUpload1" multiplefileselection="Automatic"
                                                AllowedFileExtensions=".pdf"  width="500px" CssClass="upload1" UploadedFilesRendering="BelowFileInput"
                                                OnClientFilesUploaded="OnClientFilesUploaded" OnClientFileUploaded="OnClientFileUploaded"
                                                OnClientFileUploadRemoved="OnClientFileUploadRemoved"/>
                                         
                                        </div> 
                                    </td>
                                     <td width="5%"> <span class="field-label" style="font-size: xx-large;">Or</span> </td>
                                    <td style="vertical-align:top!important">
                                        <span class="field-label">Upload Zip File</span> 
                                    </td>
                                    <td>
                                         <asp:FileUpload id="zipFileUpload" runat="server" AllowMultiple="false" accept=".zip" onchange="UploadZipFiles(this)"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td width="5%"></td>
                                    <td> <asp:Button runat="server" ID="btnUpload" CssClass="btn btn-primary d-none" Text="Upload Files" OnClick="btnUpload_Click"/>
 </td>
                                </tr>
                            </table> 
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
    <script>
        function UploadZipFiles(fileUpload) {
            if (fileUpload.value != '') {
                $("#ctl00_cphMasterpage_btnUpload").removeClass("d-none");
            } else {
                $("#ctl00_cphMasterpage_btnUpload").addClass("d-none");
            }
        }

        function OnClientFilesUploaded(sender) {
            var $ = $telerik.$;
            var total_files = sender.getUploadedFiles().length;
            $("#spnTotal").html(total_files);
            ValidateGrades(sender)
        }

        function OnClientFileUploaded(sender) {
            var total = parseInt($("#spnTotal").html()) + 1; 
            $("#spnTotal").html(total); 
        }
        function OnClientFileUploadRemoved(sender) {
            if (parseInt($("#spnTotal").html()) >= 0) {
                var total = parseInt($("#spnTotal").html()) - 1;
                $("#spnTotal").html(total);

                //if (total > 0) {
                //    $("#ctl00_cphMasterpage_btnUpload").removeClass("d-none");
                //} else {
                //    $("#ctl00_cphMasterpage_btnUpload").addClass("d-none");
                //}
                ValidateGrades(sender);
            }
        }

        function ValidateGrades(sender) {
            $("#ctl00_cphMasterpage_btnUpload").addClass("d-none");

            var total_files = sender.getUploadedFiles();
            var fileName = "";
            for (cnt = 0; cnt < sender.getUploadedFiles().length; cnt++) {               
                  fileName =fileName+","+total_files[cnt].split(".")[0]+"";
            }
            fileName = fileName.substring(1);

            var acd_id = $("#ctl00_cphMasterpage_ddlAcademicYear").val();
            var grm_grd_id = "";
            if ($("#ctl00_cphMasterpage_ddlGrade").val() == "") {
                $("#ctl00_cphMasterpage_ddlGrade option").each(function () {
                    if ($(this).val() != "") {
                        grm_grd_id = grm_grd_id + "," + $(this).val();
                    }
                });

                grm_grd_id = grm_grd_id.substring(1);
            } else {
                grm_grd_id = $("#ctl00_cphMasterpage_ddlGrade").val();
            }
 
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/curriculum/clmGradeUpload.aspx/ValidateFiles") %>', 
                data: "{'stu_nos':" + JSON.stringify(fileName)+",'acd_id':'"+acd_id+"','grm_grd_id':'"+grm_grd_id+"'}",
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    //alert(results.d);
                    if (results.d == "0") {
                       // alert("validated");
                        //btnUpload
                        $("#ctl00_cphMasterpage_btnUpload").removeClass("d-none");
                         $("#divError").addClass("d-none");
                         $("#spnError").html("")

                    } else if (results.d == "1") { 
                         $("#ctl00_cphMasterpage_btnUpload").addClass("d-none"); 
                         $("#divError").removeClass("d-none");
                         $("#spnError").html("Session Expired! Please login again.")
                    }
                    else {
                         $("#divError").removeClass("d-none");
                         $("#spnError").html("Below students are not belong to selected grades:" + results.d);
                         $("#ctl00_cphMasterpage_btnUpload").addClass("d-none");
                    }
                }
            });
        }
        
    </script>
</asp:Content>

