Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_Reports_Aspx_rptCASESchoolWiseAnalysis
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session.Remove("rptclass")

                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330470" And ViewState("MainMnu_code") <> "C330475" And ViewState("MainMnu_code") <> "C330480") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                BindAcademicYear()
                BindGrade()
                BindBuisnessUnit()
                BindSubject()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Select Case ViewState("MainMnu_code")
                    Case "C330470"
                        lblCaption.Text = "SCHOOL PERFORMANCE BY CONCEPT & SKILL"
                    Case "C330475"
                        lblCaption.Text = "SCHOOL PERFORMANCE BY SKILL"
                    Case "C330480"
                        lblCaption.Text = "SCHOOL PERFORMANCE BY CONCEPT"
                End Select
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If


        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub
    Private Sub BindBuisnessUnit()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String = "SELECT BSU_SHORTNAME,BSU_ID FROM BUSINESSUNIT_M " _
                                 & " WHERE  BSU_SHORTNAME IN('OOD','OOW','OOS','OOB','OIS','OOF','OOL','OOA','KGS','TMS','GMS')"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        lstBSU.DataSource = ds
        lstBSU.DataTextField = "BSU_SHORTNAME"
        lstBSU.DataValueField = "BSU_ID"
        lstBSU.DataBind()

        If Not lstBSU.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            lstBSU.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                               & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH','SCIENCE','ARABIC')"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBM_DESCR"
        ddlSubject.DataValueField = "SBM_ID"
        ddlSubject.DataBind()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_DESCR,ACY_ID FROM ACADEMICYEAR_M WHERE ACY_ID>=23"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()

        'str_query = " SELECT ACY_DESCR,ACD_ACY_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
        '                      & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("SBSUID") + "' AND ACD_CLM_ID=" + Session("CLM")
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'Dim li As New ListItem
        'li.Text = ds.Tables(0).Rows(0).Item(0)
        'li.Value = ds.Tables(0).Rows(0).Item(1)
        'ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
    End Sub


    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GRD_DISPLAY,GRD_ID FROM VW_GRADE_M WHERE GRD_ID  IN('04','06','08') ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim FromTo_Date As String = ""
        Dim i As Integer = 0
        Dim condition As String

        'If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
        '    FromTo_Date = " AND STUDENT_M.STU_DOJ BETWEEN '" + Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd") + "'"
        'End If
        With lstBSU
            For i = 0 To .Items.Count - 1
                If .Items(i).Selected = True Then
                    If i <> 0 Then
                        condition += "|"
                    End If
                    condition += .Items(i).Value
                End If
            Next
        End With
        Dim str As String = ""
        With lstBSU
            For i = 0 To .Items.Count - 1
                If .Items(i).Selected = True Then
                    If str <> "" Then
                        str += ","
                    End If
                    str += .Items(i).Text
                End If
            Next
        End With




        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_IDS", condition)
        param.Add("@ACY_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        param.Add("@SBM_ID", ddlSubject.SelectedValue.ToString)
        param.Add("subject", ddlSubject.SelectedItem.Text)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
         


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case ViewState("MainMnu_code")
                Case "C330470"
                    If chkGender.Checked = True Then
                        .reportPath = Server.MapPath("../RPT/CASE/rptCASESchoolPerformanceBySkill_Gender.rpt")
                    Else
                        .reportPath = Server.MapPath("../RPT/CASE/rptCASESchoolPerformanceBySkill.rpt")
                    End If
                Case "C330475"
                    If chkGender.Checked = True Then
                        .reportPath = Server.MapPath("../RPT/CASE/rptCASESchoolperformancebyOverallSkill_Gender.rpt")
                    Else
                        .reportPath = Server.MapPath("../RPT/CASE/rptCASESchoolPerformancebyOverallSkill.rpt")
                    End If

                Case "C330480"
                    If chkGender.Checked = True Then
                        .reportPath = Server.MapPath("../RPT/CASE/rptCASESchoolPerformancebyOverallConcept_Gender.rpt")
                    Else
                        .reportPath = Server.MapPath("../RPT/CASE/rptCASESchoolPerformancebyOverallConcept.rpt")
                    End If
            End Select
         



        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If
    End Sub


    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub
End Class
