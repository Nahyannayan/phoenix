﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmGroupOptionAllocationByStudent.aspx.vb" Inherits="Curriculum_clmGroupOptionAllocationByStudent"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .RadTabStripTop_Default .rtsLast .rtsOut {
            background-position: 99.99% 0 !important;
        }

        .RadTabStrip .rtsUL, .RadTabStripVertical .rtsUL {
            margin-left: 8px !important;
            height: 26px;
            min-width: 300px;
        }

        .RadTabStripTop_Default .rtsLast .rtsSelected .rtsOut {
            background-position: 99.99% -26px !Important;
        }

        .RadTabStrip_Windows7 .rtsLevel {
            background-color: transparent !important;
            margin-left: 8px;
        }

        .RadTabStrip_Default .rtsLI, .RadTabStrip_Default .rtsLink {
            font: 10px/30px 'Nunito', sans-serif !important;
        }

        table {
            font-size: 12px !important;
        }

        .std-grp-profile {
            margin: -8px !important;
        }

        .minwidth-100 {
            min-width: 100% !important;
        }

        .font-smaller {
            font-size: 10px !important;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Student Group Setting
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblTC" runat="server" align="center"
                    cellpadding="5" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" >
                            <span class="field-label">Select Grade</span>
                        </td>

                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <span class="field-label">Select Section</span>
                        </td>

                        <td align="left"  colspan="2">
                            <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <span class="field-label">Student ID</span>
                        </td>

                        <td align="left" >
                            <asp:TextBox ID="txtStuNo" runat="server"> </asp:TextBox>
                        </td>
                        <td align="left" >
                            <span class="field-label">Student Name</span>
                        </td>

                        <td align="left" >
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                        </td>
                        <td style="height: 10px">
                            <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                                Width="51px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="14">
                            <asp:DataList RepeatColumns="3" RepeatDirection="Horizontal" runat="server" ID="dlStudents" CssClass="table table-bordered">
                                <ItemTemplate>
                                    <table width="100%" >
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgStud" class="std-grp-profile" runat="server" Height="90px" ImageUrl='<%# getPhoto(Eval("Stu_PhotoPath")) %>' />
                                            </td>
                                            <td>
                                                <table cellpadding="5" width="100%">
                                                    <tr>
                                                        <td class=" font-smaller" align="left">
                                                            <span class="field-label">Student ID :</span>
                                                        </td>
                                                        <td class=" font-smaller" align="left">
                                                            <asp:Label ID="lblStudent" Text='<%# Eval("Stu_No") %>' class="field-value"
                                                                runat="server"></asp:Label>
                                                            <asp:Label ID="lblStuId" Text='<%# Eval("Stu_Id") %>'
                                                                runat="server" Visible="false"></asp:Label>
                                                            <asp:DropDownList ID="ddlStuGroup" runat="server" Visible="false"></asp:DropDownList>
                                                            <asp:DropDownList ID="ddlStuOption" runat="server" Visible="false"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class=" font-smaller" align="left">
                                                            <span class="field-label">Student Name :</span>
                                                        </td>
                                                        <td class=" font-smaller" align="left">
                                                            <asp:Label ID="lblStuNo" Text='<%# Eval("Stu_Name") %>' class="field-value"
                                                                runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <telerik:RadTabStrip runat="server" ID="rtsOption" Skin="Default" SelectedIndex="0" MultiPageID="rdPage">
                                                    <Tabs>
                                                        <telerik:RadTab Text="Manadatory Subjects">
                                                        </telerik:RadTab>
                                                        <telerik:RadTab Text="Optional Subjects">
                                                        </telerik:RadTab>
                                                    </Tabs>
                                                </telerik:RadTabStrip>

                                                <telerik:RadMultiPage ID="rdPage" runat="server">
                                                    <telerik:RadPageView ID="pgView1" runat="server" Selected="true">
                                                        <div style="height:550px;overflow:scroll">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="gvMandatoryGroup" runat="server" Height="1px" EmptyDataText="No Records"
                                                                            AutoGenerateColumns="false" PageSize="2" >
                                                                            <RowStyle CssClass="griditem_alternative" Height="25px"></RowStyle>
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="sbgId" Visible="False">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Group" Visible="False">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCSgrId" runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Subject" HeaderStyle-CssClass="title-bg-light">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>' class="field-label"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Group" HeaderStyle-CssClass="title-bg-light">
                                                                                    <ItemTemplate>
                                                                                        <asp:DropDownList ID="ddlCGroup" runat="server" CssClass="minwidth-100">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblTeacher" runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <SelectedRowStyle CssClass="Green"></SelectedRowStyle>
                                                                            <HeaderStyle CssClass="gridheader_pop" Height="30px"></HeaderStyle>

                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Button ID="btnSaveManadtoy" OnClick="btnSaveManadtoy_Click" Text="Save" CssClass="button" runat="server" /></td>
                                                                </tr>
                                                            </table>
                                                        </div>

                                                    </telerik:RadPageView>
                                                    <telerik:RadPageView ID="pgView2" runat="server">
                                                        <div style="height:550px;overflow:scroll">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="gvOptionalGroup" runat="server"  EmptyDataText="No Records"
                                                                            AutoGenerateColumns="false" PageSize="2">
                                                                            <RowStyle CssClass="griditem_alternative" Height="25px"></RowStyle>
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="sbgId" Visible="False">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblSbgId" runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Capacity" Visible="False">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblSgrId" runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Capacity" Visible="False">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblOptId" runat="server" Text='<%# Bind("OPT_ID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Option" HeaderStyle-CssClass="title-bg-light">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblOption" Text='<%# Bind("OPT_DESCR") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Subject" HeaderStyle-CssClass="title-bg-light">
                                                                                    <ItemTemplate>
                                                                                        <asp:DropDownList ID="ddlOptSubject" runat="server" AutoPostBack="true"
                                                                                            OnSelectedIndexChanged="ddlOptSubject_SelectedIndexChanged">
                                                                                        </asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Group" HeaderStyle-CssClass="title-bg-light">
                                                                                    <ItemTemplate>
                                                                                        <asp:DropDownList ID="ddlOptGroup" runat="server" CssClass="minwidth-100">
                                                                                        </asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <SelectedRowStyle CssClass="Green"></SelectedRowStyle>
                                                                            <HeaderStyle CssClass="gridheader_pop" Height="30px"></HeaderStyle>

                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Button ID="btnSaveOptional" OnClick="btnSaveOptional_Click" Text="Save" CssClass="button" runat="server" /></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        </div>
                                                    </telerik:RadPageView>
                                                </telerik:RadMultiPage>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

    <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfMinOption" runat="server"></asp:HiddenField>
</asp:Content>
