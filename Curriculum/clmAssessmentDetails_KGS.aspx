﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmAssessmentDetails_KGS.aspx.vb" Inherits="Curriculum_clmAssessmentDetails_KGS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book  mr-3"></i>
            Subject Assessment Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0" >
                    <tr>
                        <td >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" 
                                ValidationGroup="groupM1" Style="text-align: left"  />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="bottom" ><asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                             ></asp:Label></td>
                    </tr>
                    <tr>
                        <td  style="font-weight: bold;" valign="top">
                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnAddCriteria">
                                <table align="center"  cellpadding="5" 
                                    cellspacing="0"  id="tb1" runat="server" width="100%">
                                   
                                    <tr>
                                        <td align="left" width="20%"  ><span class="field-label">Select Academic Year</span></td>
                                        
                                        <td align="left"  width="30%" >
                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True"
                                                >
                                            </asp:DropDownList>&nbsp;</td>
                                        <td align="left"   width="20%"  ><span class="field-label">Select Report Card</span></td>
                                     
                                        <td align="left"  width="30%"   >
                                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%" ><span class="field-label">Select Grade</span></td>
                                        
                                        <td align="left" width="30%" >
                                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td align="left" width="20%"  ><span class="field-label">Report Schedule</span></td>
                                    
                                        <td align="left" width="30%"  >
                                            <asp:DropDownList ID="ddlRPF" runat="server">
                                            </asp:DropDownList>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"  ><span class="field-label">Select Subject</span></td>
                                      
                                        <td align="left" width="30%" >
                                            <asp:DropDownList ID="ddlSubject" runat="server"  AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td align="left"   colspan="2">
                                            <asp:Button ID="btnAddCriteria" runat="server" CssClass="button" TabIndex="7" Text="GET DATA"
                                                ValidationGroup="groupM1" />
                                        </td>
                                    </tr>
                                    <tr id="trGrid" runat="server">
                                        <td colspan="4">
                                            <asp:GridView ID="gvCriteria" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                 CssClass="table table-bordered table-row" EmptyDataText="No record added." HeaderStyle-Height="30"
                                                PageSize="20" >
                                                <RowStyle CssClass="griditem" Wrap="False" />
                                                <EmptyDataRowStyle Wrap="False" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Category ID" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCategoryID" runat="server" Text='<%# Bind("SBT_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Category">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("SBT_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                                                HeaderStyle-Height="30"
                                                                PageSize="20" >
                                                                <RowStyle  Wrap="False" />
                                                                <EmptyDataRowStyle Wrap="False" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="RSD ID" Visible="false">
                                                                        <HeaderStyle  />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRSDId" runat="server" Text='<%# Bind("RSD_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Criteria Short Name">
                                                                        <HeaderStyle  />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCriteriaShort" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Skill" Visible="true">
                                                                        <HeaderStyle Width="30" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSkills" runat="server" Text='<%# Bind("RSD_SUB_SKILLS") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Max Marks" Visible="true">
                                                                        <HeaderStyle/>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtMaxMarks" runat="server"  Text='<%# Bind("SAD_SUB_SKILLS_MAXMARKS") %>'></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                                <HeaderStyle CssClass="gridheader_pop " Wrap="False" />
                                                                <HeaderStyle CssClass="gridheader_new" />
                                                                <EditRowStyle Wrap="False" />
                                                            </asp:GridView>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comments" Visible="true">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtComments" runat="server" Text='<%# BIND("SAD_CATEGORY_SUB_NAME") %>' SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                                <EditRowStyle Wrap="False" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr id="trSave" runat="server" >
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button"/>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

