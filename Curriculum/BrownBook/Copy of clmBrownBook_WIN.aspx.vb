Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_BrownBook_clmBrownBook_WIN
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+"))
            hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))
            hfChecker.Value = Encr_decrData.Decrypt(Request.QueryString("checker").Replace(" ", "+"))


            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)
            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300110") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                hfPromotedGrade.Value = GetpromotedGrade()
                hfnewpage.Value = 0
                hfRecordNumber.Value = 0
                hfTableColumns.Value = 0
                hfPageNumber.Value = 1
                BindBrownbookDate()
                GetBrownBook(grd_id(0), Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")), grd_id(1))


                'Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'End Try
            End If
        End If
    End Sub
#Region "Private Methods"
    Function GetpromotedGrade() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TOP 1 ISNULL(GRD_ID,'') FROM GRADE_M WHERE GRD_DISPLAYORDER>" _
                                  & " (SELECT GRD_DISPLAYORDER FROM GRADE_M WHERE GRD_ID='" + hfGRD_ID.Value + "') ORDER BY GRD_DISPLAYORDER"
        Dim grdid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return "Y" + grdid
    End Function
    Sub SetHowParent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE') FROM " _
                               & " FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfShowParent1.Value = ds.Tables(0).Rows(0).Item(0)
        hfShowParent2.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetBrownBook(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim Subjects As String(,) = GetBrowbBookSubjects(grd_id, hfACD_ID.Value, stm_id)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: BROWN BOOK ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<Link REL=STYLESHEET HREF=""..\..\cssfiles\brownbook.css"" TYPE=""text/css"">")
        sb.AppendLine("<BODY>")
        sb.AppendLine("<Script language=""JavaScript"" src=""../include/com_client_func.js""></Script>")
        sb.AppendLine("<style>	body,table,td{background-color:#ffffff!important;}")
        sb.AppendLine("th{background-color:#ffffff!important;}")
        sb.AppendLine("</style>")
        If sct_id = "ALL" Then
            str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                      & "' AND SCT_ACD_ID=" + Session("Current_ACD_ID")
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetbrownBookForSection(grd_id, .Rows(i).Item(0).ToString, Session("Current_ACD_ID"), Subjects))
                Next
            End With
        Else
            sb.AppendLine(GetbrownBookForSection(grd_id, sct_id, Session("Current_ACD_ID"), Subjects))
        End If
        sb.AppendLine("</BODY></HTML>")
        Response.Write(sb.ToString())


    End Sub

    Function GetBrowbBookSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT dbo.title_case(SBG_DESCR) as sbg_descr,SBG_ID,ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR,SBM_DESCR " _
        '                       & " FROM SUBJECTS_GRADE_S AS A INNER JOIN SUBJECT_M AS B ON A.SBG_SBM_ID=B.SBM_ID " _
        '                       & " WHERE SBG_GRD_ID='" + grd_id + "' AND SBG_ACD_ID=" + acd_id _
        '                       & " AND SBG_STM_ID=" + stm_id + " AND SBG_PARENT_ID=0 AND SBG_DESCR<>'English Reading Comprehension' ORDER BY SBG_ORDER"

        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ORDER,SBG_bMAJOR,SBG_MAXMARK,SBG_MINMARK FROM GETBROWNBOOKSUBJECTS_win(" + acd_id + ",'" + grd_id + "') WHERE SBG_bMAJOR=1" _
                              & " ORDER BY SBG_ORDER"

           Dim imageLib As ImageCreationLibrary
        Dim stImagePath As String
        imageLib = New ImageCreationLibrary(stImagePath)

        Dim imgFilePath As String = ""
        stImagePath = Server.MapPath("~/Curriculum/BrownBook/SubjectVerticalText/")

        imageLib.BaseImagePath = stImagePath


        Dim f As New Font("Arial", 10, FontStyle.Regular, GraphicsUnit.Pixel, 0)



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count
        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 5)

        For j = 0 To i - 1
            With ds.Tables(0)
                Subjects(j, 0) = .Rows(j).Item("SBG_DESCR").ToString
                Subjects(j, 1) = 0
                 'All international school will enter grade only for brown boon


                Subjects(j, 2) = .Rows(j).Item("SBG_bMAJOR").ToString



                'Max and Min Mark to be updated later
                If .Rows(j).Item("SBG_DESCR").ToString.ToUpper = "ARABIC" Or .Rows(j).Item("SBG_DESCR").ToString.ToUpper = "ISLAMIC STUDIES" _
                Or .Rows(j).Item("SBG_DESCR").ToString.ToUpper = "MORAL SCIENCE" Or .Rows(j).Item("SBG_DESCR").ToString.ToUpper = "SOCIAL STUDIES" Or .Rows(j).Item("SBG_DESCR").ToString.ToUpper = "UAE SOCIAL STUDIES" Then
                    Subjects(j, 3) = "100"
                    Subjects(j, 4) = "50"
                Else
                    Subjects(j, 3) = "100"
                    Subjects(j, 4) = "40"
                End If

                imageLib.CreateVerticalTextImage(.Rows(j).Item(0), f)
            End With
        Next

        imageLib.CreateVerticalTextImage("SERIAL NO.", f)
        imageLib.CreateVerticalTextImage("SEX", f)
        imageLib.CreateVerticalTextImage("STUDENT NO.", f)
        imageLib.CreateVerticalTextImage("NATIONALITY", f)
        imageLib.CreateVerticalTextImage("Grades", f)

        Return Subjects
    End Function

    Function GetbrownBookForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,)) As String
        Dim sb As New StringBuilder
        Dim strHeader As String = GetBrownBookHeader(sct_id)


        sb.AppendLine("<table width=""1400""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=5>")
        sb.AppendLine("<tr><td algn=center>")
        sb.AppendLine(strHeader.Replace("%pageno%", hfPageNumber.Value))
        sb.AppendLine("</td></tr>")
        sb.AppendLine("<tr><td algn=center valign=top>")

        sb.AppendLine(GetSubjectsHeader(Subjects))

        sb.AppendLine(GetStudentMarks(sct_id, Subjects))

        sb.AppendLine("</td></tr>")
        sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetBrownBookHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_MOE_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table width=""100%""   align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR>")
            sb.AppendLine("<TD WIDTH=20% VALIGN=TOP align=center>")
            sb.AppendLine("<table width=""70%"" height=""30"" align=""center"" border=""0"" bordercolorlight=""#000000"" cellspacing=""0"" cellpadding=0  > ")
            sb.AppendLine("<TR><TD align=center><font  size=2pt><b>MINISTRY OF EDUCATION</b></font></TD></TR>")
            sb.AppendLine("<TR ><TD align=center><font  size=2pt><b>KHDA</b></font></TD>")
            sb.AppendLine("<TR><TD align=center><font  size=2pt><b>License No : " + reader("ACD_MOEAFFLNO") + " </b></font></TD></TR>")
            sb.AppendLine("</table><BR>")


            sb.AppendLine("</TD>")




            sb.AppendLine("<TD WIDTH=50% VALIGN=TOP>")

            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD valign=top rowspan=4><center><img src=..\" + reader("BSU_MOE_LOGO") + " height=100></center></TD></TR>")
            ' sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font  style=""text-decoration:NONE;font:14pt Old English Text MT , Times New Roman,Verdana, arial, Helvetica, sans-serif"">" + reader("BSU_NAME") + "</font>")
            sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font  style=""text-decoration:NONE;font:14pt Old English Text MT , Times New Roman,Verdana, arial, Helvetica, sans-serif"">The Winchester School</font>")
            sb.AppendLine("<BR><font  style=""text-decoration:NONE;font:bold bold 8pt  arial, Helvetica, sans-serif""> ANNUAL EXAMINATION RESULTS <BR> ACADEMIC YEAR " + reader("ACY_DESCR") + "(BRITISH CURRICULUM)</font>")
           
            sb.AppendLine("<TR><TD align=center><font style=""text-decoration:NONE;font:bold bold 8pt arial, Helvetica, sans-serif"">Grade : " + hfGRD_ID.Value + " &nbsp;&nbsp; Division : " + reader("SCT_DESCR") + "</FONT></TD></TR>")
            sb.AppendLine("</table>")

            sb.AppendLine("</TD>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=TOP ALIGN=""RIGHT"">")
            sb.AppendLine("<table width=""60%""  align=""center"" border=""1"" bordercolorlight=""#000000"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR><TD colspan=3 align=center><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif""> For Auditors Committe </TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:8pt Verdana, arial, Helvetica, sans-serif"">Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD><td><font style=""text-decoration:NONE;font:8pt Verdana, arial, Helvetica, sans-serif"">Signature :</font> &nbsp;&nbsp;&nbsp;&nbsp;</td><td><font style=""text-decoration:NONE;font:8pt Verdana, arial, Helvetica, sans-serif"">1.</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><font style=""text-decoration:NONE;font:8pt Verdana, arial, Helvetica, sans-serif"">2.</font>&nbsp;&nbsp;&nbsp;&nbsp;</td></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><font style=""text-decoration:NONE;font:8pt Verdana, arial, Helvetica, sans-serif"">3.</font>&nbsp;&nbsp;&nbsp;&nbsp;</td></TR>")
            sb.AppendLine("</table>")
            sb.AppendLine("</TD>")

            sb.AppendLine("</TR>")


            sb.AppendLine("</Table><BR>")
            sb.AppendLine("<TR><TD align=left><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> Sheet No : %pageno% &nbsp;  Date : " + hfSubmitDate.Value + "</TD>")
            sb.AppendLine("</TR>")
            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function


    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                 & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " AND DES_DESCR='" + designation + "'"
        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If emp Is Nothing Then
            emp = ""
        End If
        Return emp
    End Function
    Function GetSubjectsHeader(ByVal Subjects(,) As String) As String
        Dim sb As New StringBuilder
        Dim i As Integer

        Dim strSubjects As String = ""
        Dim strMax As String = ""
        Dim strMin As String = ""
        Dim imgUrl As String = ""

        Dim imageLib As ImageCreationLibrary
        Dim imgFilePath As String = ""
        hfTableColumns.Value = 0
        For i = 0 To Subjects.GetLength(0) - 1
            imgUrl = Subjects(i, 0)

            If checkUpper(imgUrl) = True Then
                imgUrl += "_UPPERCASE"
            End If
            imgUrl = imgUrl.Replace(" ", "%20")

            strSubjects += "<td width=""20px"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\" + imgUrl + ".jpg" + "></td>"
            hfTableColumns.Value += 1
            strSubjects += "<td width=""20px"">&nbsp;</td>"
            hfTableColumns.Value += 1
            strMax += "<td width=""20px"" align=""middle"" Class=repcolDetail>" + Subjects(i, 3) + "</td>"
            strMax += "<td width=""20px"">&nbsp;</td>"
            strMin += "<td width=""20px"" align=""middle"" Class=repcolDetail>" + Subjects(i, 4) + "</td>"
            strMin += "<td width=""20px"">&nbsp;</td>"

        Next

        sb.AppendLine("<table border=""1"" width=1400 bordercolorlight=""#000000""  align=""center"" cellspacing=""0"" cellpadding=""1""   style=""text-decoration:NONE;font:NORMAL NORMAL 7pt Verdana, arial, Helvetica, sans-serif"" >")

        sb.AppendLine("<tr height=50>")
        sb.AppendLine("<td width=""20px"" ALIGN=""middle"" valign=""BOTTOM"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\SERIAL%20NO._UPPERCASE.jpg></td>")
        sb.AppendLine("<td width=""20px"" ALIGN=""middle"" valign=""BOTTOM"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\STUDENT%20NO._UPPERCASE.jpg></td>")
        sb.AppendLine("<td width=""20px"" ALIGN=""middle"" valign=""BOTTOM"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\SEX_UPPERCASE.jpg></td>")
        sb.AppendLine("<td  width=""200px"" ALIGN=CENTER valign=""middle"" rowspan=3 Class=repcolDetail ><table border=0 ><tr><td align=""middle"" Class=repcolDetail>STUDENT NAME</td></tr><tr><td Class=repcolDetail>AS IN THE PASSPORT</td></tr><tr><td Class=repcolDetail>(ARABIC)</td></tr></table></td>")
        sb.AppendLine("<td width=""200px"" ALIGN=center valign=""middle"" rowspan=3  ><table border=0 ><tr><td align=""middle"" Class=repcolDetail>STUDENT NAME</td></tr><tr><td Class=repcolDetail>AS IN THE PASSPORT</td></tr><tr><td Class=repcolDetail>(ENGLISH)</td></tr></table></td>")
        sb.AppendLine("<td width=""20px"" ALIGN=""middle"" valign=""BOTTOM"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\NATIONALITY_UPPERCASE.jpg></td>")

        sb.AppendLine(strSubjects)
        sb.AppendLine("<td  width=""100px""  ALIGN=middle valign=""middle"" rowspan=3 Class=repcolDetail ><center>REMARKS</td>")
        hfTableColumns.Value += 7
        sb.AppendLine("</tr>")

        sb.AppendLine("<tr>")
        sb.AppendLine(strMax)
        sb.AppendLine("</tr>")

        sb.AppendLine("<tr>")
        sb.AppendLine(strMin)

        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function

    Function GetStudentMarks(ByVal sct_id As String, ByVal Subjects(,) As String)
        Dim strTable As String

        Dim strFooter As String = GetBrownBookFooter()

        Dim strPassed As String = GetStudents(sct_id, Subjects, "PASS", strFooter)
        Dim strRetest As String = GetStudents(sct_id, Subjects, "RETEST", strFooter)
        Dim strDetained As String = GetStudents(sct_id, Subjects, "FAIL", strFooter)

        Dim strSubjectHeader As String = GetSubjectsHeader(Subjects)
        Dim strPageHeader As String = GetBrownBookHeader(sct_id)


        If strRetest <> "" Then
            If hfnewpage.Value = 0 Then
                strRetest = strFooter + "<div style=""page-break-after:always"">&nbsp;</div>" + strPageHeader.Replace("%pageno%", hfPageNumber.Value) + strSubjectHeader + "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font weight=bold name=""Times new roman"" size=2pt >Retest</font></td>" + strRetest
                hfnewpage.Value = 2
            Else
                strRetest = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font weight=bold name=""Times new roman"" size=2pt color=red>RETEST</font></td>" + strRetest
            End If

        End If

        If strDetained <> "" Then
            If hfnewpage.Value = 0 Then
                strDetained = strFooter + "<div style=""page-break-after:always"">&nbsp;</div>" + strPageHeader.Replace("%pageno%", hfPageNumber.Value) + strSubjectHeader + "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font weight=bold name=""Times new roman"" size=2pt color=red>STUDENTS DETAINED</font></td>" + strDetained
                hfnewpage.Value = 2
            Else
                strDetained = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font weight=bold name=""Times new roman"" size=2pt color=red>STUDENTS DETAINED</font></td>" + strDetained
            End If
        End If

        strTable = strPassed + strRetest + strDetained

       

        If hfLastRecord.Value = 0 Then
            strTable += strFooter
        End If

        strTable = strTable.Replace("%totalstudents%", hfTotalStudents.Value.ToString)
        strTable = strTable.Replace("%passedcount%", hfPassedCount.Value.ToString)
        strTable = strTable.Replace("%detained%", hfDetained.Value.ToString)
        strTable = strTable.Replace("%tablecolumns%", hfTableColumns.Value)

        'hfPageNumber.Value += 1
        'strTable = strTable.Replace("%pageno%", hfPageNumber.Value)
        strTable = strTable.Replace("%totalpages%", hfPageNumber.Value)


        strTable = strTable.Replace("%reg_l%", hfReg_L.Value.ToString)
        strTable = strTable.Replace("%reg_nl%", hfReg_NL.Value.ToString)
        strTable = strTable.Replace("%pass_l%", hfPass_L.Value.ToString)
        strTable = strTable.Replace("%pass_nl%", hfPass_NL.Value.ToString)
        strTable = strTable.Replace("%reg_t%", (Val(hfReg_L.Value) + Val(hfReg_NL.Value)).ToString)
        strTable = strTable.Replace("%pass_t%", (Val(hfPass_L.Value) + Val(hfPass_NL.Value)).ToString)


        Dim per_l As Double = Math.Round((Val(hfPass_L.Value) / IIf(Val(hfReg_L.Value) = 0, 1, Val(hfReg_L.Value))) * 100, 1)

        Dim per_nl As Double = Math.Round((Val(hfPass_NL.Value) / IIf(Val(hfReg_NL.Value) = 0, 1, Val(hfReg_NL.Value))) * 100, 1)

        Dim pass_total As Integer = Val(hfPass_L.Value) + Val(hfPass_NL.Value)
        Dim reg_total As Integer = Val(hfReg_L.Value) + Val(hfReg_NL.Value)

        Dim per_t As Double = Math.Round((pass_total / IIf(reg_total = 0, 1, reg_total)) * 100, 1)

        strTable = strTable.Replace("%per_l%", per_l.ToString)
        strTable = strTable.Replace("%per_nl%", per_nl.ToString)
        strTable = strTable.Replace("%per_t%", per_t.ToString)


        Dim newPage As Boolean = False
        'UPDATE RETEST COUNT


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String
        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = " SELECT SBG_DESCR,COUNT(FRR_ID) FROM OASIS..STUDENT_M AS A" _
                        & " INNER JOIN RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID AND A.STU_ACD_ID=B.FRR_ACD_ID" _
                        & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID AND A.STU_ACD_ID=C.FTM_ACD_ID" _
                        & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.FTM_SBG_ID=D.SBG_ID" _
                        & " WHERE FRR_RESULT='RETEST' AND FTM_RESULT='FAIL'" _
                        & " AND STU_SCT_ID=" + sct_id + " GROUP BY SBG_DESCR  ORDER BY SBG_DESCR"
        Else
            str_query = " SELECT SBG_DESCR,COUNT(FRR_ID) FROM VW_STUDENT_DETAILS_PREVYEARS AS A" _
                       & " INNER JOIN RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID AND A.STU_ACD_ID=B.FRR_ACD_ID" _
                       & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID AND A.STU_ACD_ID=C.FTM_ACD_ID" _
                       & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.FTM_SBG_ID=D.SBG_ID" _
                       & " WHERE FRR_RESULT='RETEST' AND FTM_RESULT='FAIL'" _
                       & " AND STU_SCT_ID=" + sct_id + " GROUP BY SBG_DESCR  ORDER BY SBG_DESCR"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer
        Dim p As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace("%retestsub" + (i + 1).ToString + "%", "No. Retest in " + .Item(0).ToString)
                strTable = strTable.Replace("%rt" + (i + 1).ToString + "%", .Item(1).ToString)
            End With
        Next


        'clear extra subjets
        For i = ds.Tables(0).Rows.Count - 1 To 7
            strTable = strTable.Replace("%retestsub" + (i + 1).ToString + "%", "&nbsp;")
            strTable = strTable.Replace("%rt" + (i + 1).ToString + "%", "&nbsp;")

        Next


        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim id As String = ""

        Dim strStuId As String = ""
        Dim strRetestSubjects As String = ""

        'POPULATE STUDENT MARKS
        'If Session("Current_ACD_ID") = hfACD_ID.Value Then
        '    str_query = "SELECT SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
        '                            & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
        '                            & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
        '                            & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
        '                            & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , dbo.title_case(SBG_DESCR) SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT FROM" _
        '                            & " OASIS..STUDENT_M AS A  INNER JOIN SUBJECTS_GRADE_S B" _
        '                            & " ON A.STU_ACD_ID=B.SBG_ACD_ID AND A.STU_GRD_ID=B.SBG_GRD_ID " _
        '                            & " AND STU_STM_ID=SBG_STM_ID " _
        '                            & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
        '                            & " LEFT OUTER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
        '                            & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
        '                            & " WHERE STU_SCT_ID=" + sct_id + " AND SBG_PARENT_ID=0" _
        '                                                                & " ORDER BY STU_ID"
        'Else
        '    str_query = "SELECT SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
        '                           & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
        '                           & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
        '                           & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
        '                           & " ISNULL(FRR_RESULT,'') AS FRR_RESULT ,  dbo.title_case(SBG_DESCR) SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT FROM" _
        '                           & " VW_STUDENT_DETAILS_PREVYEARS AS A  INNER JOIN SUBJECTS_GRADE_S B" _
        '                           & " ON A.STU_ACD_ID=B.SBG_ACD_ID AND A.STU_GRD_ID=B.SBG_GRD_ID " _
        '                           & " AND STU_STM_ID=SBG_STM_ID " _
        '                           & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
        '                           & " LEFT OUTER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
        '                           & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
        '                           & " WHERE STU_SCT_ID=" + sct_id + " AND SBG_PARENT_ID=0" _
        '                                                               & " ORDER BY STU_ID"
        'End If

        'POPULATE STUDENT MARKS
        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT     SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
                                    & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
                                    & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
                                    & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
                                    & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT,ISNULL(FRR_RESULT_EDITED,'') AS FRR_RESULT_EDITED,ISNULL(SBG_bRETEST,'FALSE') AS SBG_bRETEST FROM" _
                                    & " STUDENT_M AS A  INNER JOIN STUDENT_GROUPS_S AS P " _
                                    & " ON A.STU_ID=P.SSD_STU_ID AND A.STU_ACD_ID=P.SSD_ACD_ID INNER JOIN dbo.GETBROWNBOOKSUBJECTS_WIN(" + hfACD_ID.Value + ",'" + hfGRD_ID.Value + "') B  ON P.SSD_SBG_ID=B.SBG_ID" _
                                    & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
                                    & " LEFT OUTER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
                                    & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
                                    & " WHERE STU_SCT_ID=" + sct_id _
                                     & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'" _
                                    & "  ORDER BY STU_ID"
        Else
            str_query = "SELECT   SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
                                & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
                                & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
                                & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
                                & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT,ISNULL(FRR_RESULT_EDITED,'') AS FRR_RESULT_EDITED,ISNULL(SBG_bRETEST,'FALSE') AS SBG_bRETEST FROM" _
                                & " VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN STUDENT_GROUPS_S AS P " _
                                & " ON A.STU_ID=P.SSD_STU_ID AND A.STU_ACD_ID=P.SSD_ACD_ID INNER JOIN dbo.GETBROWNBOOKSUBJECTS_WIN(" + hfACD_ID.Value + ",'" + hfGRD_ID.Value + "') B  ON P.SSD_SBG_ID=B.SBG_ID" _
                                & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
                                & " LEFT OUTER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
                                & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
                                & " WHERE STU_SCT_ID=" + sct_id _
                                & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'" _
                                & " ORDER BY STU_ID"
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)

                If strStuId <> .Item(1).ToString Then
                    If strRetestSubjects <> "" Then
                        strTable = strTable.Replace(strStuId + "_RESULT", "Retest In <BR/>" + strRetestSubjects)
                    End If
                    strStuId = .Item(1).ToString
                    strRetestSubjects = ""
                End If

                id = .Item(1).ToString + "_" + .Item("SBG_DESCR").ToString
                If .Item(8).ToString.ToUpper = "FAIL" Then
                    strMark = .Item(2).ToString
                    strGrade = .Item(3).ToString
                    If strRetestSubjects <> "" And .Item(6).ToString.ToUpper = "RETEST" Then
                        strRetestSubjects += "<BR/>"
                    End If
                    strRetestSubjects += .Item(7).ToString
                Else
                    strMark = .Item(2).ToString
                    strGrade = .Item(3).ToString
                End If
                strTable = strTable.Replace(id + "_M", strMark)
                strTable = strTable.Replace(id + "_G", strGrade)

                If .Item(6).ToString.ToUpper = "PASS" Then
                    strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "Promoted")
                ElseIf .Item(6).ToString.ToUpper = "FAIL" Then
                    strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "Detained")
                End If

                If i = ds.Tables(0).Rows.Count - 1 Then
                    If strRetestSubjects <> "" Then
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "Retest in " + strRetestSubjects)
                    End If
                End If

            End With
        Next
        strTable = ClearEmptyColumns(sct_id, Subjects, strTable)
      
        Return strTable
    End Function

    Function ClearEmptyColumns(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal strTable As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID FROM STUDENT_M WHERE STU_SCT_ID=" + sct_id _
                        & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'"
        Else
            str_query = "SELECT STU_ID FROM STUDENT_M INNER JOIN OASIS..STUDENT_PROMO_S ON STU_ID=STP_STU_ID WHERE STP_SCT_ID=" + sct_id _
                  & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'"

        End If

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i, j As Integer
        Dim id As String
        For i = 0 To ds.Tables(0).Rows.Count - 1
            For j = 0 To Subjects.GetLength(0) - 1
                id = ds.Tables(0).Rows(i).Item(0).ToString + "_" + Subjects(j, 0) + "_M"
                strTable = strTable.Replace(id, "&nbsp;")
                id = ds.Tables(0).Rows(i).Item(0).ToString + "_" + Subjects(j, 0) + "_G"
                strTable = strTable.Replace(id, "&nbsp;")
            Next
        Next

        Return strTable
    End Function


    Function GetRedLine() As String
        Dim i As Integer
        Dim sb As New StringBuilder
        sb.AppendLine("<tr>")
        For i = 0 To Val(hfTableColumns.Value) - 1
            sb.AppendLine("<td STYLE=""BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 7pt verdana,timesroman,garamond"">&nbsp;</td>")
        Next
        sb.AppendLine("</tr>")
        Return sb.ToString
    End Function
    Sub BindBrownbookDate()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(ACD_BROWNBOOKDATE,getdate()) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value
        hfBrownBookDate.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub
    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal result As String, ByVal strFooter As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strSubjectHeader As String = GetSubjectsHeader(Subjects)
        Dim strPageHeader As String = GetBrownBookHeader(sct_id)
        Dim i As Integer
        Dim j As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()

        Dim strRedLine As String

        Dim str_query As String

        Dim pCount As Int16

        If hfGRD_ID.Value = "10" Then
            pCount = 16
        Else
            pCount = 26
        End If
        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                    & " +' '+ISNULL(STU_LASTNAME,'')) AS STU_NAME,STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                                    & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                                    & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                                    & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                                    & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                                    & " STU_ID,ISNULL(FRR_RESULT,'PASSED') AS RESULT,STU_GENDER,CTY_NATIONALITY FROM OASIS..STUDENT_M AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                                    & " INNER JOIN RPT.FINAL_RESULT_S AS C ON A.STU_ID=C.FRR_STU_ID AND A.STU_ACD_ID=C.FRR_ACD_ID" _
                                    & " LEFT OUTER JOIN OASIS..COUNTRY_M AS E ON A.STU_NATIONALITY=E.CTY_ID" _
                                    & " WHERE STU_SCT_ID=" + sct_id _
                                    & " AND FRR_RESULT='" + result + "' " _
                                    & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'  AND STU_MINLIST='Regular'" _
                              & "ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Else
            str_query = "SELECT ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                              & " +' '+ISNULL(STU_LASTNAME,'')) AS STU_NAME,STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                                              & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                                              & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                                              & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                                              & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                                              & " STU_ID,ISNULL(FRR_RESULT,'PASSED') AS RESULT,STU_GENDER,CTY_NATIONALITY FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                                              & " INNER JOIN RPT.FINAL_RESULT_S AS C ON A.STU_ID=C.FRR_STU_ID AND A.STU_ACD_ID=C.FRR_ACD_ID" _
                                              & " LEFT OUTER JOIN OASIS..COUNTRY_M AS E ON A.STU_NATIONALITY=E.CTY_ID" _
                                              & " WHERE STU_SCT_ID=" + sct_id _
                                              & " AND FRR_RESULT='" + result + "' " _
                                              & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'  AND STU_MINLIST='Regular'" _
                                        & "ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count

        If result.ToUpper = "PASS" Then
            hfPassedCount.Value = ds.Tables(0).Rows.Count
        ElseIf result.ToUpper = "FAIL" Then
            hfDetained.Value = ds.Tables(0).Rows.Count
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If
        If result = "PASS" Then
            sb.AppendLine("<tr hegihgt=20px><td colspan=%tablecolumns% align=center><font weight=bold name=""Times new roman"" size=2pt >The Students Passed and Promoted To " + hfPromotedGrade.Value + "</font></td></tr>")
        End If
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                hfRecordNumber.Value += 1
                If hfRecordNumber.Value = pCount Then
                    If i = ds.Tables(0).Rows.Count - 1 And result.ToUpper = "FAIL" Then
                        hfLastRecord.Value = 1
                    End If
                    hfPageNumber.Value += 1
                    sb.AppendLine(strFooter.Replace("%pageno%", hfPageNumber.Value))

                    If hfLastRecord.Value <> 1 Then
                        sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                    End If
                    sb.AppendLine(strPageHeader.Replace("%pageno%", hfPageNumber.Value))
                    sb.AppendLine(strSubjectHeader)
                    hfRecordNumber.Value = 1
                    hfnewpage.Value = "1"
                End If

                stuNames = .Item(0).ToString.Replace("  ", " ").Split(" ")

                If .Item("CTY_NATIONALITY").ToString = "UAE" Then
                    hfReg_L.Value += 1
                    If result.ToUpper <> "FAIL" Then
                        hfPass_L.Value += 1
                    End If
                Else
                    hfReg_NL.Value += 1
                    If result.ToUpper <> "FAIL" Then
                        hfPass_NL.Value += 1
                    End If
                End If


                If result.ToUpper = "RETEST" Then 'add and empty row above  the same student

                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td HEIGHT=""10px"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_BLUEID") + "</td>")
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_GENDER") + "</td>")

                    sb.AppendLine("<td align=""right"" ><font name=""arial"" size=""2px"">" + .Item("STU_ARABICNAME") + "</font></td>")


                    If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                        sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                    Else
                        sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                    End If

                    
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>" + .Item("CTY_NATIONALITY") + "</td>")

                    For j = 0 To Subjects.GetLength(0) - 1
                            sb.AppendLine("<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")

                        sb.AppendLine("<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>")

                    Next

                    'Result
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")


                    sb.AppendLine("</tr>")
                End If

                sb.AppendLine("<tr>")
                sb.AppendLine("<td HEIGHT=""10px"" align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_BLUEID") + "</td>")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_GENDER") + "</td>")


                sb.AppendLine("<td align=""right"" ><font name=""arial"" size=""2px"">" + .Item("STU_ARABICNAME") + "</font></td>")

                If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                Else
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                End If



                sb.AppendLine("<td align=""middle"" Class=repcolDetail>" + .Item("CTY_NATIONALITY") + "</td>")

                For j = 0 To Subjects.GetLength(0) - 1
                    If Subjects(j, 0).ToUpper = "ARABIC" Or Subjects(j, 0).ToUpper = "ISLAMIC STUDIES" Or Subjects(j, 0).ToUpper = "SOCIAL STUDIES" Or Subjects(j, 0) = "UAE SOCIAL STUDIES" Or Subjects(j, 0).ToUpper = "MORAL SCIENCE" Or Subjects(j, 0).ToUpper = "PSHE" Then
                        sb.AppendLine("<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">" + .Item(4).ToString + "_" + Subjects(j, 0) + "_M" + "</font></td>")
                    Else
                        sb.AppendLine("<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">" + .Item(4).ToString + "_" + Subjects(j, 0) + "_G" + "</font></td>")
                    End If

                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                Next

             
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">" + .Item(4).ToString + "_RESULT</font></td>")

                sb.AppendLine("</tr>")


            End With
        Next





        Return sb.ToString
    End Function

    Public Function checkUpper(ByVal str As String) As Boolean
        If Strings.StrComp(str, Strings.UCase(str)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetBrownBookFooter() As String
        Dim sb As New StringBuilder

        sb.AppendLine("</table>")

        sb.AppendLine("<table  border=0  align=""center"" cellspacing=""0"" cellpadding=""5""  style=""text-decoration:NONE;font:normal normal 7Pt Verdana, arial, Helvetica, sans-serif"" >")
        sb.AppendLine("<TR HEIGHT=35>")
        sb.AppendLine("<TD align=left> Principal :</TD><TD>" + GetEmpName("principal") + " </TD> ")
        sb.AppendLine(" <TD align=left >Registrar :</TD> <TD align=left>" + GetEmpName("registrar") + "</TD> ")
        
        If hfChecker.Value = "" Then
            sb.AppendLine("<TD align=left >Checked By :</TD> <TD width=150>..........................................................</TD> ")
        Else
            sb.AppendLine("<TD align=left >Checked By :</TD> <TD width=150>" + hfChecker.Value + "</TD> ")
        End If
        sb.AppendLine("<TD  rowspan=2 align=middle>")
        sb.AppendLine("<TABLE WIDTH=250PX border=1 CELLSPACING=0 CELLPADDING=0 bordercolorlight=""#000000"">")
        sb.AppendLine("<tr><td width=""20px"" rowspan=3  ><img src=..\BrownBook\SubjectVerticalText\Grades_White.jpg></td>")
        sb.AppendLine("<td align=center class=footertablecis><B>A*</td>")
        sb.AppendLine("<td align=center class=footertablecis><B>90-100%</td>")
        sb.AppendLine("<td align=center class=footertablecis><B>C</B></td>")
        sb.AppendLine("<td align=center class=footertablecis><B>60-69%</td></tr>")
        sb.AppendLine("<tr><td align=center class=footertablecis><B>A</td>")
        sb.AppendLine("<td align=center class=footertablecis><B>80-89%</td>")
        sb.AppendLine("<td align=center class=footertablecis><B>D</td>")
        sb.AppendLine("<td align=center class=footertablecis><B>40-59%</td></tr>")
        sb.AppendLine("<tr><td align=center class=footertablecis><B>B</td>")
        sb.AppendLine("<td align=center class=footertablecis><B>70-79%</td>")
        sb.AppendLine("<td align=center class=footertablecis><B>E</td>")
        sb.AppendLine("<td align=center class=footertablecis><B>0-39%</td></tr>")
        sb.AppendLine("</TABLE>")
        sb.AppendLine("</TD>")

       

        sb.AppendLine("<TD  rowspan=2 align=middle>")
        sb.AppendLine("<TABLE WIDTH=250PX border=1 CELLSPACING=0 CELLPADDING=2 bordercolorlight=""#000000"">")
        sb.AppendLine("<tr height=""5px""><td width=""20px"" class=footertablecis>Nationality</td>")
        sb.AppendLine("<td width=""20px"" class=footertablecis>Registered</td>")
        sb.AppendLine("<td width=""20px"" class=footertablecis>Present</td>")
        sb.AppendLine("<td width=""20px"" class=footertablecis>Passed</td>")
        sb.AppendLine("<td width=""20px"" class=footertablecis>Percentage</td></tr>")
        sb.AppendLine("<tr><td class=footertablecis>UAE</td>")
        sb.AppendLine("<td class=footertablecis align=center>%reg_l%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%reg_l%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%pass_l%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%per_l%</td></tr>")
        sb.AppendLine("<tr><td class=footertablecis>Others</td>")
        sb.AppendLine("<td class=footertablecis align=center>%reg_nl%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%reg_nl%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%pass_nl%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%per_nl%</td></tr>")
        sb.AppendLine("<tr><td class=footertablecis>Total</td>")
        sb.AppendLine("<td class=footertablecis align=center>%reg_t%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%reg_t%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%pass_t%</td>")
        sb.AppendLine("<td class=footertablecis align=center>%per_t%</td></tr>")
        sb.AppendLine("</TABLE>")
        sb.AppendLine("</TD></TR>")

        sb.AppendLine("<TR HEIGHT=35> <TD align=left>Signature :</TD> <TD>............................................</TD>")
        sb.AppendLine("<TD align=left>Signature  :</TD> <TD>........................................... </TD>")
        sb.AppendLine("<TD align=left>Signature  :</TD> <TD>...........................................</TD>  </TR> ")




        sb.AppendLine("</Table>")



        Return sb.ToString
    End Function
#End Region


End Class
