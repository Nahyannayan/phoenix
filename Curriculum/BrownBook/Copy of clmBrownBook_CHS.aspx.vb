Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_BrownBook_clmBrownBook_CHS
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+"))
            hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))
            hfChecker.Value = Encr_decrData.Decrypt(Request.QueryString("checker").Replace(" ", "+"))
            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300110") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page
                ViewState("blankline") = ""

                'disable the control buttons based on the rights
                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                SetHowParent()
                BindBrownbookDate()
                hfRecordNumber.Value = 0
                hfTableColumns.Value = 0
                hfPageNumber.Value = 0
                hfRecordsPerPage.Value = Request.QueryString("records")
                If Val(hfRecordsPerPage.Value) = 0 Then
                    hfRecordsPerPage.Value = 20
                End If
                GetBrownBook(grd_id(0), Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")), grd_id(1))


                'Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'End Try
            End If
        End If
    End Sub
#Region "Private Methods"

    Sub BindBrownbookDate()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(ACD_BROWNBOOKDATE,getdate()) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value
        hfBrownBookDate.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub
    Sub SetHowParent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE')  " _
                               & " FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfShowParent1.Value = ds.Tables(0).Rows(0).Item(0)
        hfShowParent2.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetBrownBook(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder


        ViewState("sct_id") = sct_id
        Dim Subjects As String(,) = GetBrowbBookSubjects(grd_id, hfACD_ID.Value, stm_id)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: BROWN BOOK ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<Link REL=STYLESHEET HREF=""..\cssfiles\brownbook.css"" TYPE=""text/css"">")
        sb.AppendLine("<BODY>")
        sb.AppendLine("<Script language=""JavaScript"" src=""../../include/com_client_func.js""></Script>")
        sb.AppendLine("<style>	body,table,td{background-color:#ffffff!important;}")
        sb.AppendLine("th{background-color:#ffffff!important;}")
        sb.AppendLine("</style>")
        If sct_id = "ALL" Then
            str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                      & "' AND SCT_ACD_ID=" + hfACD_ID.Value
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetbrownBookForSection(grd_id, .Rows(i).Item(0).ToString, hfACD_ID.Value, Subjects))
                Next
            End With
            'ltBrownBook.Text = sb.ToString
        Else
            sb.AppendLine(GetbrownBookForSection(grd_id, sct_id, hfACD_ID.Value, Subjects))

            'ltBrownBook.Text = sb.ToString
        End If
        sb.AppendLine("</BODY></HTML>")
        Response.Write(sb.ToString())


    End Sub

    Function GetBrowbBookSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "exec GETBROWNBOOKSUBJECTS" _
                              & " @BSU_ID='" + Session("sbsuid") + "'," _
                              & " @ACD_ID =" + acd_id + "," _
                              & " @GRD_ID ='" + grd_id + "'," _
                              & " @STM_ID =" + stm_id


        Dim imageLib As ImageCreationLibrary
        Dim stImagePath As String
        imageLib = New ImageCreationLibrary(stImagePath)

        Dim imgFilePath As String = ""
        stImagePath = Server.MapPath("~/Curriculum/BrownBook/SubjectVerticalText/")

        imageLib.BaseImagePath = stImagePath


        Dim f As New Font("Arial", 10, FontStyle.Regular, GraphicsUnit.Pixel, 0)



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count
        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 6)

        For j = 0 To i - 1
            With ds.Tables(0)

                'subject
                Subjects(j, 0) = .Rows(j).Item(0).ToString


                'sbgid
                Subjects(j, 1) = .Rows(j).Item(1).ToString

                'major minor

                'for kg1 and kg2 show grades only there fore set them as minor subjects
                'All international school will enter grade only for brown boon

                Select Case Session("sbsuid")
                    Case "114003", "115002", "125002", "125003", "125005", "125011", "125015", "125016", "125017"
                        Subjects(j, 2) = "False"
                    Case Else
                        ' Subjects(j, 2) = .Rows(j).Item(2).ToString

                        If hfGRD_ID.Value = "KG1" Or hfGRD_ID.Value = "KG2" Then
                            Subjects(j, 2) = "False"
                        Else
                            Subjects(j, 2) = .Rows(j).Item(2).ToString
                        End If
                End Select


                'Max and Min Mark to be updated later

                Subjects(j, 3) = .Rows(j).Item(3).ToString.Replace(".000", "").Replace(".00", "")
                Subjects(j, 4) = .Rows(j).Item(4).ToString.Replace(".000", "").Replace(".00", "")

                'SBM_DESCR
                Subjects(j, 5) = .Rows(j).Item(5)

                'create image
                '  imageLib.CreateVerticalTextImage(.Rows(j).Item(0), New System.Drawing.Font(System.Drawing.FontFamily.GenericSerif, 12))
                imageLib.CreateVerticalTextImage(.Rows(j).Item(0), f)
            End With
        Next

        imageLib.CreateVerticalTextImage("Average Grade", f)
        imageLib.CreateVerticalTextImage("Total", f)
        imageLib.CreateVerticalTextImage("Religion", f)
        imageLib.CreateVerticalTextImage("Gender", f)
        imageLib.CreateVerticalTextImage("Percentage", f)
        imageLib.CreateVerticalTextImage("Rank", f)
        imageLib.CreateVerticalTextImage("Level", f)
        Return Subjects
    End Function

    Function GetbrownBookForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,)) As String
        Dim sb As New StringBuilder
        Dim strHeader As String = GetBrownBookHeader(sct_id)
        ' sb.AppendLine(strHeader)
        sb.AppendLine("<table width=""1400""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=5>")

        sb.AppendLine("<tr><td algn=center >")
        sb.AppendLine(strHeader)
        sb.AppendLine("</td></tr>")

        sb.AppendLine("<tr><td algn=center VALIGN=TOP>")

        sb.AppendLine(GetSubjectsHeader(Subjects))

        sb.AppendLine(GetStudentMarks(sct_id, Subjects))

        sb.AppendLine("</table>")

        sb.AppendLine("</td></tr>")
        sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetBrownBookHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_MOE_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " UPPER(ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')) " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table  aligh=left width=100% border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR>")

            sb.AppendLine("<TD WIDTH=30%  VALIGN=TOP>")

            sb.AppendLine("<table width=""90%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font:bold bold 8Pt Verdana,arial, Helvetica, sans-serif"" > ")
            sb.AppendLine("<TR><TD align=""left""> <Img Src=""..\..\images\ADEC-logo.jpg"" height=""100""></TD><td>Abu Dhabi Education Council<br> Private Education</TR>")
            sb.AppendLine("</table><BR>")

        

            sb.AppendLine("<table width=""90%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font:bold bold 10Pt  arial, Helvetica, sans-serif"">")
            sb.AppendLine("<TR height=20>")
            sb.AppendLine("<TD><font  style=""text-decoration:NONE;font:bold bold 9pt  arial, Helvetica, sans-serif"">GRADE :    " + reader("GRM_DISPLAY") + "   &nbsp;&nbsp; SECTION : " + reader("SCT_DESCR") + " </font></TD> </TR>")

            sb.AppendLine("</Table>")

            sb.AppendLine("</TD>")


            sb.AppendLine("<TD WIDTH=40% align=""center"" VALIGN=TOP>")

            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD valign=top rowspan=4><center><img src=..\" + reader("BSU_MOE_LOGO") + " height=100></center></TD></TR>")

            sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font  style=""text-decoration:NONE;font:bold bold 13pt  arial, Helvetica, sans-serif"">" + Replace(reader("BSU_NAME"), "BOYS", "<br>BOYS'") + "</I>")
            sb.AppendLine("<BR> Annual Examination Results  " + reader("ACY_DESCR") + "<BR>")
            Select Case hfGRD_ID.Value
                Case "01", "02"
                    sb.AppendLine("<font  style=""text-decoration:NONE;font:bold bold 9pt  arial, Helvetica, sans-serif"">KEYSTAGE 1</font><BR>")
                Case "03", "04", "05", "06"
                    sb.AppendLine("<font  style=""text-decoration:NONE;font:bold bold 9pt  arial, Helvetica, sans-serif"">KEYSTAGE 2</font><BR>")
                Case "07", "08", "09"
                    sb.AppendLine("<font  style=""text-decoration:NONE;font:bold bold 9pt  arial, Helvetica, sans-serif"">KEYSTAGE 3</font><BR>")
                Case "10"
                    sb.AppendLine("<font  style=""text-decoration:NONE;font:bold bold 9pt  arial, Helvetica, sans-serif"">KEYSTAGE 4</font><BR>")
            End Select

            sb.AppendLine("<font  style=""text-decoration:NONE;font:bold bold 9pt  arial, Helvetica, sans-serif"">CURRICULUM : BRITISH</font>")
            sb.AppendLine("</TD></TR>")

            sb.AppendLine("</table>")

            sb.AppendLine("</TD>")


           



            'sb.AppendLine("</TR>")

            'sb.AppendLine("<TR><TD colspan=2><font style=""text-decoration:NONE;font:bold bold 10Pt  arial, Helvetica, sans-serif"">&nbsp;&nbsp;&nbsp;&nbsp; Class Teacher :  " + reader("EMP_NAME") + " </TD>")

          
            sb.AppendLine("<TD align=right valign=bottom><font style=""text-decoration:NONE;font:bold bold 9pt Verdana, arial, Helvetica, sans-serif"">DATE : " + Format(CDate(hfSubmitDate.Value), "MMMM").ToUpper + " " + CDate(hfSubmitDate.Value).Year.ToString + " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>")


            sb.AppendLine("</TR>")
            sb.AppendLine("</Table>")

            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function

    Function GetSubjectsHeader(ByVal Subjects(,) As String) As String
        ViewState("blankline") = ""
        Dim sb As New StringBuilder
        Dim i As Integer
        Dim strSubjects As String = ""
        Dim strBlankSubject As String = ""
        Dim strMax As String = ""
        Dim strMin As String = ""
        Dim imgUrl As String = ""

        Dim imageLib As ImageCreationLibrary
        Dim imgFilePath As String = ""
        hfTableColumns.Value = 0

        Dim style As String = ""

        ViewState("blankline") += "<tr >"
        For i = 0 To Subjects.GetLength(0) - 1
          
            imgUrl = Subjects(i, 0)

            If checkUpper(imgUrl) = True Then
                imgUrl += "_UPPERCASE"
            End If
            imgUrl = imgUrl.Replace(" ", "%20")

            strSubjects += "<td " + style + " width=""20"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\" + imgUrl.Replace("/", "_") + ".jpg" + "></td>"

            strBlankSubject += "<td " + style + "  width=""20px"" align=""middle"" Class=repcolDetail><font style=""font:8pt Arial"">&nbsp;</font></td>"

            hfTableColumns.Value += 1

            'if major subject add one more column
            '  If Subjects(i, 2).ToLower = "true" Then
            strMax += "<td " + style + "   align=""center"" Class=repcolDetail>" + Subjects(i, 3) + "</td>"
            strMin += "<td " + style + "  align=""center"" Class=repcolDetail>" + Subjects(i, 4) + "</td>"
            ' Else
            '  strMin += "<td  align=""center"" Class=repcolDetail>GR</td>"
            '  strMax += "<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>"
            ' End If
        Next

        sb.AppendLine("<table border=""1"" bordercolorlight=""#000000"" WIDTH=""1400"" align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font:NORMAL NORMAL 7pt Verdana, arial, Helvetica, sans-serif"" >")

        sb.AppendLine("<tr height=50>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> SR. NO  </td>")

       
        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td  width=""270"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> Name of the Student  <br> (in Arabic)</td>")

        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td  width=""270"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> Name of the Student <br>  (in English)</td>")

        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td  width=""20"" ALIGN=""middle"" valign=""bottom"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\NATIONALITY.jpg></td></td>")

        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td  width=""20"" ALIGN=""middle"" valign=""bottom"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\RELIGION.jpg></td></td>")


        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td  width=""20"" ALIGN=""middle"" valign=""bottom"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\GENDER.jpg></td></td>")

        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td width=""20"" align=""middle"" valign=""bottom"" Class=repcolDetail> Marks </td>")

        ViewState("blankline") += "<td>&nbsp;</td>"


        hfTableColumns.Value += 7
        sb.AppendLine(strSubjects)

        ViewState("blankline") += strBlankSubject

        If Session("sbsuid") = "131002" And hfGRD_ID.Value = "11" And hfSTM_ID.Value = "2" Then
            sb.AppendLine("<td width=""2%"" align=""middle"" Class=repcolDetail>&nbsp;</td>")

            ViewState("blankline") += "<td>&nbsp;</td>"
            hfTableColumns.Value += 1
        End If

        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\AVERAGE%20GRADE.jpg></td>")

        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\TOTAL.jpg></td>")

        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\PERCENTAGE.jpg></td>")

        ViewState("blankline") += "<td>&nbsp;</td>"

        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\RANK.jpg></td>")

        ViewState("blankline") += "<td>&nbsp;</td>"

        If hfGRD_ID.Value <> "10" And hfGRD_ID.Value <> "11" Then
            sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\LEVEL.jpg></td>")
            hfTableColumns.Value += 6
        Else
            hfTableColumns.Value += 7
        End If
        ViewState("blankline") += "<td>&nbsp;</td>"
        sb.AppendLine("<td  width=""100""  ALIGN=middle   valign=""middle"" rowspan=3 Class=repcolDetail ><center>Remarks(Passed/<br>Detained/Retest)</td>")
        If hfGRD_ID.Value <> "10" And hfGRD_ID.Value <> "11" Then
            ViewState("blankline") += "<td>&nbsp;</td>"
        End If




        sb.AppendLine("</tr>")

        sb.AppendLine("<tr>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail> MAX </td>")
        sb.AppendLine(strMax)
        If Session("sbsuid") = "131002" And hfGRD_ID.Value = "11" And hfSTM_ID.Value = "2" Then
            sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        End If
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        If hfGRD_ID.Value <> "10" And hfGRD_ID.Value <> "11" Then
            sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        End If
        sb.AppendLine("</tr>")

        sb.AppendLine("<tr>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>MIN</td>")
        sb.AppendLine(strMin)

        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        If hfGRD_ID.Value <> "10" And hfGRD_ID.Value <> "11" Then
            sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        End If

        sb.AppendLine("</tr>")
        ViewState("blankline") += "</tr>"
        Return sb.ToString
    End Function

    Function GetStudentMarks(ByVal sct_id As String, ByVal Subjects(,) As String)
        Dim strTable As String

        Dim strFooter As String = GetBrownBookFooter()

        Dim strPassed As String = GetStudents(sct_id, Subjects, "PASS", strFooter)
        Dim strRetest As String = GetStudents(sct_id, Subjects, "RETEST", strFooter)
        Dim strDetained As String = GetStudents(sct_id, Subjects, "FAIL", strFooter)
        Dim q As Integer
        Dim t As Integer
        Dim strBlank As String = ""
        Dim promotedGrade As String = GetpromotedGrade()

        If promotedGrade <> "" And promotedGrade <> "13" Then
            If promotedGrade <> "10" Then
                promotedGrade = Replace(promotedGrade, "0", "")
            End If
            strPassed = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:10pt Arial"" ><b>PASSED AND PROMOTED TO GRADE " + promotedGrade + "</b></font></td>" + strPassed
        End If

        If strRetest <> "" Then
            strRetest = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:10pt Arial"" ><b>RETEST</b></font></td>" + strRetest
        End If

        If strDetained <> "" Then
            strDetained = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:10pt Arial size=""3px"" ><b>STUDENTS DETAINED</b></font></td>" + strDetained
        End If

        strTable = strPassed + strRetest + strDetained



        If hfLastRecord.Value = 0 Then

            'strDetained += "</TABLE>"
            If hfRecordNumber.Value > 25 Then
                hfRecordNumber.Value = 25
            End If
            t = 25 - hfRecordNumber.Value
            For q = 1 To t
                strBlank += ViewState("blankline")
            Next
            strTable += strBlank
            'strTable += GetRedLine()
            strTable += strFooter
        End If

        strTable = strTable.Replace("%totalstudents%", hfTotalStudents.Value.ToString)
        strTable = strTable.Replace("%passedcount%", hfPassedCount.Value.ToString)
        strTable = strTable.Replace("%detained%", hfDetained.Value.ToString)

        hfPageNumber.Value += 1
        '   strTable = strTable.Replace("%pageno%", hfPageNumber.Value)
        strTable = strTable.Replace("%totalpages%", hfPageNumber.Value)



        'UPDATE RETEST COUNT


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If hfACD_ID.Value = Session("Current_ACD_ID") Then
            str_query = " SELECT SBG_DESCR,COUNT(FRR_ID) FROM STUDENT_M AS A" _
                    & " INNER JOIN RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID AND A.STU_ACD_ID=B.FRR_ACD_ID" _
                    & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID AND A.STU_ACD_ID=C.FTM_ACD_ID" _
                    & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.FTM_SBG_ID=D.SBG_ID" _
                    & " WHERE FRR_RESULT='RETEST' AND FTM_RESULT='FAIL' AND ISNULL(SBG_bRETEST,'FALSE')='TRUE' " _
                    & " AND STU_SCT_ID=" + sct_id + " GROUP BY SBG_DESCR  ORDER BY SBG_DESCR"
        Else
            str_query = " SELECT SBG_DESCR,COUNT(FRR_ID) FROM VW_STUDENT_DETAILS_PREVYEARS AS A" _
                  & " INNER JOIN RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID AND A.STU_ACD_ID=B.FRR_ACD_ID" _
                  & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID AND A.STU_ACD_ID=C.FTM_ACD_ID" _
                  & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.FTM_SBG_ID=D.SBG_ID" _
                  & " WHERE FRR_RESULT='RETEST' AND FTM_RESULT='FAIL' AND ISNULL(SBG_bRETEST,'FALSE')='TRUE' " _
                  & " AND STU_SCT_ID=" + sct_id + " GROUP BY SBG_DESCR  ORDER BY SBG_DESCR"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer
        Dim p As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace("%retestsub" + (i + 1).ToString + "%", "No. Retest in " + .Item(0).ToString)
               
            End With
        Next


        ''clear extra subjets
        'For i = ds.Tables(0).Rows.Count - 1 To 7
        '    strTable = strTable.Replace("%retestsub" + (i + 1).ToString + "%", "&nbsp;")
        '    strTable = strTable.Replace("%rt" + (i + 1).ToString + "%", "&nbsp;")

        'Next


        strTable = strTable.Replace("%retestcount%", hfRetestCount.Value.ToString)
        Dim chr As Char() = {","}
        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim id As String = ""

        Dim strStuId As String = ""
        Dim strRetestSubjects As String = ""

        'POPULATE STUDENT MARKS
        If hfACD_ID.Value = Session("Current_ACD_ID") Then

            str_query = "SELECT SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
                                    & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
                                    & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
                                    & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
                                    & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT,ISNULL(FRR_RESULT_EDITED,'') AS FRR_RESULT_EDITED,ISNULL(SBG_bRETEST,'FALSE') AS SBG_bRETEST FROM" _
                                    & " STUDENT_M AS A  INNER JOIN SUBJECTS_GRADE_S B" _
                                    & " ON A.STU_ACD_ID=B.SBG_ACD_ID AND A.STU_GRD_ID=B.SBG_GRD_ID " _
                                    & " AND STU_STM_ID=SBG_STM_ID " _
                                    & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
                                    & " LEFT OUTER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
                                    & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
                                    & " WHERE STU_SCT_ID=" + sct_id + " AND STU_CURRSTATUS<>'CN' AND SBG_PARENT_ID=0" _
                                    & " ORDER BY STU_ID"
        Else
            str_query = "SELECT SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
                                     & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
                                     & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
                                     & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
                                     & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT,ISNULL(FRR_RESULT_EDITED,'') AS FRR_RESULT_EDITED,ISNULL(SBG_bRETEST,'FALSE') AS SBG_bRETEST FROM" _
                                     & " VW_STUDENT_DETAILS_PREVYEARS AS A  INNER JOIN SUBJECTS_GRADE_S B" _
                                     & " ON A.STU_ACD_ID=B.SBG_ACD_ID AND A.STU_GRD_ID=B.SBG_GRD_ID " _
                                     & " AND STU_STM_ID=SBG_STM_ID " _
                                     & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
                                     & " LEFT OUTER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
                                     & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
                                     & " WHERE STU_SCT_ID=" + sct_id + " AND STU_CURRSTATUS<>'CN' AND SBG_PARENT_ID=0" _
                                     & " ORDER BY STU_ID"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)

                If strStuId <> .Item(1).ToString Then
                    If strRetestSubjects <> "" Then
                        strTable = strTable.Replace(strStuId + "_RESULT", "<font color=""red"">RETEST IN <BR/>" + strRetestSubjects + "</FONT>")
                    End If
                    strStuId = .Item(1).ToString
                    strRetestSubjects = ""
                End If

                id = .Item(1).ToString + "_" + .Item(0).ToString
                If .Item(8).ToString.ToUpper = "FAIL" Then
                    If .Item(2).ToString = "0" Then
                        strMark = "<font >&nbsp;</font>"
                        strGrade = "<font >&nbsp;</font>"
                    Else
                        strMark = "<font color=""red""><u>" + .Item(2).ToString + "</u></font>"
                        strGrade = "<font color=""red""><u>" + .Item(3).ToString + "</u></font>"
                    End If

                    If strRetestSubjects <> "" And .Item(6).ToString.ToUpper = "RETEST" And .Item("SBG_bRETEST").ToString.ToLower = "true" Then
                        strRetestSubjects += ",<BR/>"
                    End If
                    If .Item("SBG_bRETEST").ToString.ToLower = "true" Then
                        strRetestSubjects += .Item(7).ToString
                    End If
                Else
                    If .Item(2).ToString = "0" Then
                        strMark = "<font >&nbsp;</font>"
                        strGrade = "<font >&nbsp;</font>"
                    Else
                        strMark = .Item(2).ToString
                        strGrade = .Item(3).ToString
                    End If
                End If
                strTable = strTable.Replace(id + "_M", strMark)
                ' strTable = strTable.Replace(id + "_G", strGrade)

                'If strRetestSubjects <> "" Then
                '    strRetestSubjects.TrimEnd(chr)
                'End If

                If .Item(6).ToString.ToUpper = "PASS" Then
                    If .Item(9).ToString.ToUpper <> "" Then
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", .Item(9).ToString.Replace("''", "'"))
                    Else
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "PASSED")
                    End If
                ElseIf .Item(6).ToString.ToUpper = "FAIL" Then
                    If .Item(9).ToString.ToUpper <> "" Then
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "<font color=""red"">" + .Item(9).ToString.Replace("''", "'") + "</font>")
                    Else
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "<font color=""red"">DETAINED</font>")
                    End If
                End If




                If i = ds.Tables(0).Rows.Count - 1 Then
                    If strRetestSubjects <> "" Then
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "<font color=""red"">RETEST IN <br>" + strRetestSubjects + "</font>")
                    End If
                End If

            End With
        Next



        'POPULATE TOTAL AND PERCENTAGE
        Dim percentage As Double
        If hfACD_ID.Value = Session("Current_ACD_ID") Then
     
            str_query = "SELECT STU_ID,SUM(ISNULL(FTM_MARK,0)),SUM(ISNULL(FTM_MAXMARK,0)) FROM OASIS..STUDENT_M AS A  " _
                           & " LEFT OUTER JOIN  RPT.FINAL_TOTALMARKS_S  AS B ON A.STU_ID=B.FTM_STU_ID AND " _
                           & " A.STU_ACD_ID=B.FTM_ACD_ID WHERE STU_SCT_ID=" + sct_id _
                           & " GROUP BY STU_ID"


        Else
           
            str_query = "SELECT STU_ID,SUM(ISNULL(FTM_MARK,0)),SUM(ISNULL(FTM_MAXMARK,0)) FROM OASIS..STUDENT_M AS A  " _
                           & " LEFT OUTER JOIN  RPT.FINAL_TOTALMARKS_S  AS B ON A.STU_ID=B.FTM_STU_ID  " _
                            & " INNER JOIN OASIS..STUDENT_PROMO_S ON STU_ID=STP_STU_ID" _
                           & " AND STP_ACD_ID=B.FTM_ACD_ID WHERE STP_SCT_ID=" + sct_id _
                           & " GROUP BY STU_ID"

        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace(.Item(0).ToString + "_TOTAL", IIf(Math.Round(CDbl(.Item(1)), 0).ToString = "0", "&nbsp;", Math.Round(CDbl(.Item(1)), 0).ToString))

                If CDbl(.Item(2)) <> 0 Then
                    percentage = Math.Round(CDbl(.Item(1)) * 100 / CDbl(.Item(2)), 0)
                Else
                    percentage = 0
                End If

                strTable = strTable.Replace(.Item(0).ToString + "_PERCENTAGE", IIf(percentage.ToString = "0", "&nbsp;", percentage.ToString))

                If percentage >= 90 Then
                    strTable = strTable.Replace(.Item(0).ToString + "_AVGGRADE", IIf(percentage.ToString = "0", "&nbsp;", "A"))
                    Select Case hfGRD_ID.Value
                        Case "01", "02"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L3"))
                        Case "03", "04", "05", "06"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L5"))
                        Case "07", "08", "09"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L7"))
                    End Select
                ElseIf percentage >= 75 And percentage < 89.9 Then
                    strTable = strTable.Replace(.Item(0).ToString + "_AVGGRADE", IIf(percentage.ToString = "0", "&nbsp;", "B"))
                    Select Case hfGRD_ID.Value
                        Case "01", "02"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L2"))
                        Case "03", "04", "05", "06"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L4"))
                        Case "07", "08", "09"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L6"))
                    End Select
                ElseIf percentage >= 60 And percentage < 74.9 Then
                    strTable = strTable.Replace(.Item(0).ToString + "_AVGGRADE", IIf(percentage.ToString = "0", "&nbsp;", "C"))
                    Select Case hfGRD_ID.Value
                        Case "01", "02"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L2"))
                        Case "03", "04", "05", "06"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L3"))
                        Case "07", "08", "09"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L5"))
                    End Select
                Else
                    strTable = strTable.Replace(.Item(0).ToString + "_AVGGRADE", IIf(percentage.ToString = "0", "&nbsp;", "D"))
                    Select Case hfGRD_ID.Value
                        Case "01", "02"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L1"))
                        Case "03", "04", "05", "06"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L2"))
                        Case "07", "08", "09"
                            strTable = strTable.Replace(.Item(0).ToString + "_LEVEL", IIf(percentage.ToString = "0", "&nbsp;", "L4"))
                    End Select
                End If

            End With
        Next

        'RANK

        str_query = "EXEC GETSTUDENTRANK_ALL " + sct_id
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)

                strTable = strTable.Replace(.Item(0).ToString + "_RANK", .Item(2).ToString)

            End With
        Next

        Return strTable
    End Function

    Function GetpromotedGrade() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TOP 1 ISNULL(GRD_ID,'') FROM GRADE_M WHERE GRD_DISPLAYORDER>" _
                                  & " (SELECT GRD_DISPLAYORDER FROM GRADE_M WHERE GRD_ID='" + hfGRD_ID.Value + "') ORDER BY GRD_DISPLAYORDER"
        Dim grdid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return grdid
    End Function

    Function GetRedLine() As String
        Dim i As Integer
        Dim sb As New StringBuilder
        sb.AppendLine("<tr>")
        For i = 0 To Val(hfTableColumns.Value) - 1
            sb.AppendLine("<td STYLE=""BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 7pt verdana,timesroman,garamond"">&nbsp;</td>")
        Next
        sb.AppendLine("</tr>")
        Return sb.ToString
    End Function

    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal result As String, ByVal strFooter As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strSubjectHeader As String = GetSubjectsHeader(Subjects)
        Dim strPageHeader As String = GetBrownBookHeader(sct_id)
        Dim i As Integer
        Dim j As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()

        Dim strRedLine As String
        Dim str_query As String

        Dim style As String = ""

        If hfACD_ID.Value = Session("Current_ACD_ID") Then
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                    & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                                    & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                                    & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                                    & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                                    & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                                    & " STU_ID,ISNULL(FRR_RESULT,'PASSED') AS RESULT,STU_GENDER,STU_RLG_ID,CTY_SHORT,CTY_GROUP FROM STUDENT_M AS A INNER JOIN STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                                    & " INNER JOIN RPT.FINAL_RESULT_S AS C ON A.STU_ID=C.FRR_STU_ID AND A.STU_ACD_ID=C.FRR_ACD_ID" _
                                    & " INNER JOIN OASIS..COUNTRY_M AS D ON A.STU_NATIONALITY=D.CTY_ID" _
                                    & " WHERE STU_SCT_ID=" + sct_id + "  AND STU_CURRSTATUS NOT IN('CN','TF') " _
                                    & " AND FRR_RESULT='" + result + "' " _
                                    & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "' AND STU_MINLIST='REGULAR'" _
                                    & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Else
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                                & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                                & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                                & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                                & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                                & " STU_ID,ISNULL(FRR_RESULT,'PASSED') AS RESULT,STU_GENDER,STU_RLG_ID,CTY_SHORT,CTY_GROUP FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                                & " INNER JOIN RPT.FINAL_RESULT_S AS C ON A.STU_ID=C.FRR_STU_ID AND A.STU_ACD_ID=C.FRR_ACD_ID" _
                                & " INNER JOIN OASIS..COUNTRY_M AS D ON A.STU_NATIONALITY=D.CTY_ID" _
                                & " WHERE STU_SCT_ID=" + sct_id + "  AND STU_CURRSTATUS NOT IN('CN','TF') " _
                                & " AND FRR_RESULT='" + result + "' " _
                                & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "' AND STU_MINLIST='REGULAR'" _
                                & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"




        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count

        If result.ToUpper = "PASS" Then
            hfPassedCount.Value = ds.Tables(0).Rows.Count
        ElseIf result.ToUpper = "FAIL" Then
            hfDetained.Value = ds.Tables(0).Rows.Count
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If

        If result.ToUpper = "RETEST" Then
            hfRetestCount.Value = ds.Tables(0).Rows.Count
        End If
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                hfRecordNumber.Value += 1
               
                If hfRecordNumber.Value = hfRecordsPerPage.Value + 1 Then
                    If i = ds.Tables(0).Rows.Count - 1 And result.ToUpper = "FAIL" Then
                        hfLastRecord.Value = 1
                        ' sb.AppendLine(GetRedLine)
                    End If
                    hfPageNumber.Value += 1
                    sb.AppendLine(strFooter.Replace("%pageno%", hfPageNumber.Value))

                    If hfLastRecord.Value <> 1 Then
                        sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                    End If
                    'End If
                    sb.AppendLine(strPageHeader)
                    sb.AppendLine(strSubjectHeader)
                    hfRecordNumber.Value = 1
                End If

                stuNames = .Item(0).ToString.Trim.Replace("  ", " ").Split(" ")

                If result.ToUpper = "RETEST" Then 'add and empty row above  the same student
                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td HEIGHT=""30px"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""left"" Class=repcolDetail>&nbsp;</td>")
                   
                    If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                          sb.AppendLine("<td align=""left"" Class=repcolDetail>&nbsp;</td>")
                    Else
                         sb.AppendLine("<td align=""left"" Class=repcolDetail>&nbsp;</td>")
                    End If

                    sb.AppendLine("<td align=""left"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""left"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""left"" Class=repcolDetail>&nbsp;</td>")

                    For j = 0 To Subjects.GetLength(0) - 1
                        sb.AppendLine("<td width=""20px"" " + style + " align=""CENTER"" ><font style=""font:8pt Arial""><input style=""width:20px;border-width:0;font:8pt Arial"" id=""Text" + .Item("STU_ID").ToString + "_" + j.ToString + " type=""text""  /></font></td>")
                    Next
                    If Session("sbsuid") = "131002" And hfGRD_ID.Value = "11" And hfSTM_ID.Value = "2" Then
                        sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    End If
                    'Average Grade
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    'Total Marks
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail><input style=""width:20px;border-width:0;font:8pt Arial"" id=""Text" + .Item("STU_ID").ToString + "_" + j.ToString + " type=""text""  /></td>")
                    'Percentage
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail><input style=""width:20px;border-width:0;font:8pt Arial"" id=""Text" + .Item("STU_ID").ToString + "_" + j.ToString + " type=""text""  /></td>")
                    'RankGrade
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    'Level
                    If hfGRD_ID.Value <> "10" And hfGRD_ID.Value <> "11" Then
                        sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    End If
                    'Result
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail><input style=""width:60px;border-width:0;font:8pt Arial"" id=""Text" + .Item("STU_ID").ToString + "_" + j.ToString + " type=""text""  /></td>")

                    sb.AppendLine("</tr>")
                End If

                sb.AppendLine("<tr>")
                sb.AppendLine("<td  align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td align=""right"" ><font name=""arial"" size=""2px"">" + .Item("STU_ARABICNAME") + "</font></td>")



                If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                    sb.AppendLine("<td align=""left"" width=""270"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                Else
                    sb.AppendLine("<td align=""left"" width=""270"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                End If

                If .Item("CTY_GROUP").ToString.ToUpper = "ARAB COUNTRIES" Then
                    sb.AppendLine("<td align=""left"" width=""20"" Class=repcolDetail>&nbsp;" + .Item("CTY_SHORT") + "</td>")
                Else
                    sb.AppendLine("<td align=""left"" width=""20"" Class=repcolDetail>&nbsp;</td>")
                End If

                If .Item("STU_RLG_ID").ToString.ToUpper = "ISL" Then
                    sb.AppendLine("<td align=""left"" width=""20"" Class=repcolDetail>&nbsp;M</td>")
                Else
                    sb.AppendLine("<td align=""left"" width=""20"" Class=repcolDetail>&nbsp;</td>")
                End If

                sb.AppendLine("<td align=""left"" width=""20"" Class=repcolDetail>&nbsp;" + .Item("STU_GENDER").ToString.ToUpper + "</td>")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")

                For j = 0 To Subjects.GetLength(0) - 1
                    sb.AppendLine("<td " + style + " width=""35px"" align=""CENTER"" ><font style=""font:8pt Arial"">" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + "M" + "</font></td>")
                Next

                'Average Grade
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_AVGGRADE</font></td>")
                'Total Marks
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_TOTAL</font></td>")
                'Percentage
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_PERCENTAGE</font></td>")
                'Rank
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_RANK</font></td>")
                'Rank
                If hfGRD_ID.Value <> "10" And hfGRD_ID.Value <> "11" Then
                    sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_LEVEL</font></td>")
                End If
                'Result
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_RESULT </font></td>")



                sb.AppendLine("</tr>")


            End With
        Next





        Return sb.ToString
    End Function



    Public Function checkUpper(ByVal str As String) As Boolean
        If Strings.StrComp(str, Strings.UCase(str)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetBrownBookFooter() As String
        Dim sb As New StringBuilder

        Dim colspan As String

        If hfGRD_ID.Value = "12" Then
            colspan = "2"
        Else
            colspan = "4"
        End If

        'sb.AppendLine("<tr>")
        'sb.AppendLine("<td  colspan=3 align=""center"" rowspan=3><font style=""font:8pt Arial"">Yearly Performance Summary</font></td>")
        'sb.AppendLine("<td  style=""BORDER-BOTTOM-WIDTH: 1.5px;""><font style=""font:8pt Arial"">No. on Roll</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">%totalstudents%</td>")
        'sb.AppendLine("<td colspan=4><font style=""font:8pt Arial"">No. Passed and Promoted</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">%passedcount%</td>")
        'sb.AppendLine("<td colspan=4><font style=""font:8pt Arial"">No.Retest</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">%retestcount%</td>")

        'sb.AppendLine("<td colspan=" + colspan + "><font style=""font:8pt Arial"">No. Detained</td>")

        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">%detained%</td>")

        '  If hfGRD_ID.Value = "12" Then
        '      sb.AppendLine("<td  rowspan=3 colspan=" + (hfTableColumns.Value - 18).ToString + " align=""left"">" _
        '      & "<font style=""font:8pt Arial"">&nbsp;Principal _____________________ <br><br>&nbsp;Date " + hfSubmitDate.Value + "</font></td>")

        '  Else
        '      sb.AppendLine("<td  rowspan=3 colspan=" + (hfTableColumns.Value - 20).ToString + " align=""left"">" _
        '& "<font style=""font:8pt Arial"">&nbsp;Principal _____________________ <br><br>&nbsp;Date " + hfSubmitDate.Value + "</font></td>")
        '  End If

        '  sb.AppendLine("</tr>")


        'sb.AppendLine("<tr>")

        'sb.AppendLine("<td  style=""BORDER-BOTTOM-WIDTH: 1.5px;""><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td colspan=4><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td colspan=4><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td colspan=" + colspan + "><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td ><font style=""font:8pt Arial"">&nbsp;</td>")


        'sb.AppendLine("</tr>")


        'sb.AppendLine("<tr>")

        'sb.AppendLine("<td  style=""BORDER-BOTTOM-WIDTH: 1.5px;""><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td colspan=4><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td colspan=4><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td align=center><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td colspan=" + colspan + "><font style=""font:8pt Arial"">&nbsp;</td>")
        'sb.AppendLine("<td ><font style=""font:8pt Arial"">&nbsp;</td>")


        'sb.AppendLine("</tr>")



        sb.AppendLine("</table>")

        sb.AppendLine("<table  border=0 width=""100%""  align=""center"" cellspacing=""0"" cellpadding=""0""  style=""text-decoration:NONE;font:normal normal 7Pt Verdana, arial, Helvetica, sans-serif"" >")
        sb.AppendLine("<tr><td colspan=" + hfTableColumns.Value + ">&nbsp;</td></tr>")
        sb.AppendLine("<TR HEIGHT=35> <TD align=left width=100>Class Teacher :</TD> <TD align=left>" + hfClassTeacher.Value + "</TD> ")
       
        sb.AppendLine("<TD width=350>Principal :" + GetEmpName("principal", "Short") + "</TD> ")

        Select Case hfGRD_ID.Value
            Case "01", "02"
                sb.AppendLine("<TD width=300 rowspan=2 align=left valign=top><b>GRADE BOUNDARIES AND LEVEL</b><BR> ")
                sb.AppendLine("A = 90-100% - Exceptional(Level 3)<br>")
                sb.AppendLine("B = 75-89% - Good(Level 2)<br>")
                sb.AppendLine("C = 60-74% - Standard(Level 2)<br>")
                sb.AppendLine("D = 50-59% - Basic(Level 1)<br>")
                sb.AppendLine("Expected Attainment is Level 2 at age 7(Year 2) - <br>end of Keystage 1")
            Case "03", "04", "05", "06"
                sb.AppendLine("<TD width=300 rowspan=2 align=left valign=top><b>GRADE BOUNDARIES AND LEVEL</b><BR> ")
                sb.AppendLine("A = 90-100% - Exceptional(Level 5)<br>")
                sb.AppendLine("B = 75-89% - Good(Level 4)<br>")
                sb.AppendLine("C = 60-74% - Standard(Level 3)<br>")
                sb.AppendLine("D = 50-59% - Basic(Level 2)<br>")
                sb.AppendLine("Expected Attainment is Level 3/4 at age 11(Year 6) - <br>end of Keystage 2")
            Case "07", "08", "09"
                sb.AppendLine("<TD width=300 rowspan=2 align=left valign=top><b>GRADE BOUNDARIES AND LEVEL</b><BR> ")
                sb.AppendLine("A = 90-100% - Exceptional(Level 7)<br>")
                sb.AppendLine("B = 75-89% - Good(Level 6)<br>")
                sb.AppendLine("C = 60-74% - Standard(Level 5)<br>")
                sb.AppendLine("D = 50-59% - Basic(Level 3/4)<br>")
                sb.AppendLine("Expected Attainment is Level 5/6 at age 14(Year 9) - <br>end of Keystage 3")
            Case "10"
                sb.AppendLine("<TD width=300 rowspan=2 align=left valign=top><b>Keystage 4 Grade Equivalancies</b><BR> ")
                sb.AppendLine("A* = 95-100%&nbsp;&nbsp;&nbsp; E=50-59%<br>")
                sb.AppendLine("A = 90-94%&nbsp;&nbsp;&nbsp;  F=40-49%<br>")
                sb.AppendLine("B = 80-89%&nbsp;&nbsp;&nbsp;  G=30-39%<br>")
                sb.AppendLine("C = 70-79%&nbsp;&nbsp;&nbsp;  MOE pass is E<br>")
                sb.AppendLine("D = 60-59%<br>")
                
        End Select

     
        sb.AppendLine("</TD></TR>")
        sb.AppendLine("<TR HEIGHT=35> <TD align=left>Signature  :</TD> <TD>_______________________________</TD> ")
        sb.AppendLine("<TD align=left>School Seal  :&nbsp;&nbsp;</TD> ")
        sb.AppendLine("</TR>")

           sb.AppendLine("</Table>")



        Return sb.ToString
    End Function
    Function GetEmpName(ByVal designation As String, ByVal sType As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If sType = "Short" Then
            str_query = "SELECT isnull(EMP_SALUTE,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                     & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE  emp_status=1 and EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                     & " AND DES_DESCR like '%" + designation + "%'"
        Else
            str_query = "SELECT isnull(EMP_SALUTE,'')+' '+ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                     & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE  emp_status=1 and EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                     & " AND DES_DESCR like '%" + designation + "%'"
        End If

        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return emp
    End Function
#End Region


End Class
