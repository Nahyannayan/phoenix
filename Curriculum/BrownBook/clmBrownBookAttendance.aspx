<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmBrownBookAttendance.aspx.vb" Inherits="Curriculum_BrownBook_clmBrownBookAttendance" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Edit Final Attendance And Remarks
        </div>
        <div class="card-body">
            <div class="table-responsive">
 <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="0" cellspacing="0" width="100%">
                 
               <tr>
              
                <td align="left" class="title" >
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                   </td>
              </tr>
             
           <tr>
            <td align="center">
               
                <table ID="tblTC" runat="server" align="center" cellpadding="0" cellspacing="0" width="100%">
                    
                  <tr>
                        <td align="left" width="20%">
                            <span  class="field-label">Select Academic Year</span></td>
                        
                        <td align="left"  colspan="2">
                           <asp:DropDownList ID="ddlAcademicYear" SKINID="smallcmb" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>               </td>
                         
                    </tr>
                  
                    
                    <tr>
                    
                       <td align="left">
                           <span class="field-label">Select Grade</span></td>
                       <td align="left" >
                            <asp:DropDownList ID="ddlGrade" SKINID="smallcmb" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>               
                      </td>
                             
                           
                        <td align="left">
                            <span class="field-label">Select Section</span></td>
                        
                        <td align="left"  colspan="2">
                            <asp:DropDownList ID="ddlSection" SKINID="smallcmb" runat="server" >
                            </asp:DropDownList>               
                      </td>
                    
                    
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Student ID</span></td>
                       
                        <td align="left" >
                            <asp:TextBox id="txtStuNo" runat="server">
                            </asp:TextBox></td>
                        <td align="left">
                            <span class="field-label">Student Name</span></td>
                       
                        <td align="left" >
                            <asp:TextBox id="txtName" runat="server"></asp:TextBox></td>
                        </tr>
                    
                    <tr>
                        <td align="center" colspan="4">
                       <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"  /></td>
                    
                    </tr>
                    
                    
                      <tr>
                      
                      
                      
                     <td align="center" colspan="7" valign="top" width="100%">
                     
                     
                     <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                      HeaderStyle-Height="30" PageSize="20" Allowpaging="true">
                         <RowStyle CssClass="griditem" />
                        <Columns>
                        <asp:TemplateField HeaderText="HideID" Visible="False"><ItemTemplate>
                        <asp:Label ID="lblStuId" runat="server"  Text='<%# Bind("Stu_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                                              
                        <asp:TemplateField HeaderText="Student No">
                        <ItemTemplate>
                        <asp:Label ID="lblStuNo"  runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle></ItemStyle>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Student Name" ><ItemTemplate>
                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle ></ItemStyle>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Grade" ><ItemTemplate>
                        <asp:Label ID="lblGrade"  runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle ></ItemStyle>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Section" ><ItemTemplate>
                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle></ItemStyle>
                        </asp:TemplateField>
                        
                        
                        <asp:TemplateField HeaderText="Att %" ><ItemTemplate>
                        <asp:Label ID="lblPercent"   runat="server" Text='<%# Bind("ATT_PERCENT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle ></ItemStyle>
                        </asp:TemplateField>
                        
                        
                                             
                        <asp:TemplateField HeaderText="Attendance" ><ItemTemplate>
                        <asp:TextBox ID="txtATT" ENABLED="false" runat="server" Text='<%# Bind("ATT_VALUE") %>'></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle ></ItemStyle>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Total Days" ><ItemTemplate>
                        <asp:TextBox ID="txtTotal"  ENABLED="false" runat="server" Text='<%# Bind("ATT_TOTALDAYS") %>'></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle></ItemStyle>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Remarks" ><ItemTemplate>
                        <asp:TextBox ID="txtRemarks" ENABLED="false" runat="server" Text='<%# Bind("FRR_RESULT_EDITED") %>'></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle ></ItemStyle>
                        </asp:TemplateField>
                                              
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                        <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                        <ItemTemplate>
                        <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click"  Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateField> 
                        
                     
                                  
                        </Columns>  
                         <SelectedRowStyle CssClass="Green" />
                         <HeaderStyle  CssClass="gridheader_pop" />
                         <AlternatingRowStyle CssClass="griditem_alternative" />
                     </asp:GridView>
                     </td></tr>
                    
                    </table>
                    
                    
            
                <asp:HiddenField id="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField id="hfGRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField id="hfSCT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField id="hfSTUNO" runat="server"></asp:HiddenField>
                <asp:HiddenField id="hfNAME" runat="server"></asp:HiddenField>
                <asp:HiddenField id="hfRSM_ID" runat="server"> </asp:HiddenField>
                 <asp:HiddenField id="hfRPF_ID" runat="server"> </asp:HiddenField>
            <input id="h_DivStyle" runat="server" type="hidden" value="=" />
               </td></tr>
           
    
</table>
    
      </div>
            </div>
        </div>


</asp:Content>

