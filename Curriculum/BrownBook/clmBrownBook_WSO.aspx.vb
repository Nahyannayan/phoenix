Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_BrownBook_clmBrownBook_WSO
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Dim studclass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+"))
            hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))
            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)
            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            hfChecker.Value = Encr_decrData.Decrypt(Request.QueryString("checker").Replace(" ", "+"))

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300110") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                SetHowParent()
                BindBrownbookDate()
                hfRecordNumber.Value = 0
                hfTableColumns.Value = 0
                hfPageNumber.Value = 0
                hfRecordsPerPage.Value = Request.QueryString("records")
                If Val(hfRecordsPerPage.Value) = 0 Then
                    hfRecordsPerPage.Value = 20
                End If
                GetBrownBook(grd_id(0), Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")), grd_id(1))


                'Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'End Try
            End If
        End If
    End Sub

#Region "Private Methods"

    Sub BindBrownbookDate()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(ACD_BROWNBOOKDATE,getdate()) FROM ACADEMICYEAR_D WHERE ACD_ID=" + Session("CURRENT_ACD_ID")
        hfBrownBookDate.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub SetHowParent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE')  " _
                               & " FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfShowParent1.Value = ds.Tables(0).Rows(0).Item(0)
        hfShowParent2.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetBrownBook(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim Subjects As String(,) = GetBrowbBookSubjects(grd_id, hfACD_ID.Value, stm_id, sct_id)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: BROWN BOOK ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<Link REL=STYLESHEET HREF=""..\cssfiles\brownbook.css"" TYPE=""text/css"">")
        sb.AppendLine("<BODY>")
        sb.AppendLine("<Script language=""JavaScript"" src=""../../include/com_client_func.js""></Script>")
        sb.AppendLine("<style>	body,table,td{background-color:#ffffff!important;}")
        sb.AppendLine("th{background-color:#ffffff!important;}")
        sb.AppendLine("</style>")
        If sct_id = "ALL" Then
            str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                      & "' AND SCT_ACD_ID=" + Session("Current_ACD_ID")
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetbrownBookForSection(grd_id, .Rows(i).Item(0).ToString, Session("Current_ACD_ID"), Subjects))
                Next
            End With
            'ltBrownBook.Text = sb.ToString
        Else
            sb.AppendLine(GetbrownBookForSection(grd_id, sct_id, Session("Current_ACD_ID"), Subjects))

            'ltBrownBook.Text = sb.ToString
        End If
        sb.AppendLine("</BODY></HTML>")
        Response.Write(sb.ToString())


    End Sub

    Function GetBrowbBookSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        'ALL ARABIC SUBJECTS SHOULD COME UNDER ON COLUMS
        'SECOND LANGUAGE SHOULD COME UNDER ONE COLUMN

        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ORDER,SBG_bMAJOR,SBG_MAXMARK,SBG_MINMARK FROM GETBROWNBOOKSUBJECTS_JC(" + acd_id + ",'" + grd_id + "','" + sct_id + "') WHERE SBG_bMAJOR=1" _
                                & " ORDER BY SBG_ORDER"


        Dim imageLib As ImageCreationLibrary
        Dim stImagePath As String
        imageLib = New ImageCreationLibrary(stImagePath)

        Dim imgFilePath As String = ""
        stImagePath = Server.MapPath("~/Curriculum/BrownBook/SubjectVerticalText/")

        imageLib.BaseImagePath = stImagePath


        Dim f As New Font("Arial", 10, FontStyle.Regular, GraphicsUnit.Pixel, 0)



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count
        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 6)



        For j = 0 To i - 1
            With ds.Tables(0)




                'subject
                Subjects(j, 0) = .Rows(j).Item(0).ToString


                'sbgid
                'Subjects(j, 1) = .Rows(j).Item(1).ToString
                Subjects(j, 1) = 0

                'major minor




                Subjects(j, 2) = .Rows(j).Item(2).ToString





                Subjects(j, 3) = .Rows(j).Item(3).ToString.Replace(".000", "").Replace(".00", "")
                Subjects(j, 4) = .Rows(j).Item(4).ToString.Replace(".000", "").Replace(".00", "")



                'SBM_DESCR
                Subjects(j, 5) = .Rows(j).Item(0)

                'create image
                '  imageLib.CreateVerticalTextImage(.Rows(j).Item(0), New System.Drawing.Font(System.Drawing.FontFamily.GenericSerif, 12))
                imageLib.CreateVerticalTextImage(.Rows(j).Item(0), f)
            End With
        Next

        imageLib.CreateVerticalTextImage("CONDUCT", f)
        imageLib.CreateVerticalTextImage("NATIONALITY", f)
        imageLib.CreateVerticalTextImage("Total", f)
        imageLib.CreateVerticalTextImage("Percentage", f)
        imageLib.CreateVerticalTextImage("Total for non Muslim", f)
        imageLib.CreateVerticalTextImage("M/F", f)

        Return Subjects
    End Function

    Function GetbrownBookForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,)) As String
        Dim sb As New StringBuilder
        Dim strHeader As String = GetBrownBookHeader(sct_id)
        ' sb.AppendLine(strHeader)
        sb.AppendLine("<table width=""1500""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=5>")

        sb.AppendLine("<tr><td algn=center >")
        sb.AppendLine(strHeader)
        sb.AppendLine("</td></tr>")

        sb.AppendLine("<tr><td algn=center VALIGN=TOP>")

        sb.AppendLine(GetSubjectsHeader(Subjects))

        sb.AppendLine(GetStudentMarks(sct_id, Subjects))

        sb.AppendLine("</table>")

        sb.AppendLine("</td></tr>")
        sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetBrownBookHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_MOE_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " ISNULL(EMP_SALUTE,'')+' '+ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table border=0 aligh=left width=100% border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR>")


            sb.AppendLine("<TD WIDTH=30% VALIGN=TOP align=center>")

            sb.AppendLine("<table width=""70%"" height=""30"" align=""center"" border=""1"" bordercolorlight=""#000000"" cellspacing=""0"" cellpadding=0  > ")
            sb.AppendLine("<TR><TD align=""center"">&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD>")
            sb.AppendLine("<TD align=right><font  size=2pt><b>خاص بلجنة التدقيق</b></font></TD></TR>")
            sb.AppendLine("<TR ><TD align=right><font  size=2pt><b>التاريخ</b></font></TD>")
            sb.AppendLine("<TD align=right><font  size=2pt><b>توقيعه</b></font></TD>")
            sb.AppendLine("<TD align=right><font  size=2pt><b>اسم المدقق</b></font></TD></TR>")
            sb.AppendLine("<TR><TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD></TR>")
            sb.AppendLine("</table><BR>")


            sb.AppendLine("</TD>")




            sb.AppendLine("<TD WIDTH=35% align=""center"" VALIGN=TOP>")

            'sb.AppendLine("<table width=""100%""  align=""center"" border=""1"" cellspacing=""0"" cellpadding=0>")
            'sb.AppendLine("<TR><TD valign=top rowspan=4><center><img src=..\" + reader("BSU_MOE_LOGO") + " height=100></center></TD></TR>")
            'sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font  style=""text-decoration:NONE;font:bold bold 14pt arial, Helvetica, sans-serif"">" + reader("BSU_NAME") + "</font>")
            'sb.AppendLine("<BR><font  style=""text-decoration:NONE;font:bold bold 10pt arial, Helvetica, sans-serif"">Annual Examination Results  " + reader("ACY_DESCR") + "<BR>The British Curriculum</font></TD></TR>")

            'sb.AppendLine("</table>")


            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR><TD valign=top><center><img src=..\" + reader("BSU_MOE_LOGO") + " height=100></center></TD>")

            sb.AppendLine("<TD align=""center"" valign=""TOP""><font  style=""text-decoration:NONE;font:bold bold 14pt arial"" ><i>" + reader("BSU_NAME") + "</i></font>")
            sb.AppendLine("<BR><font  style=""text-decoration:NONE;font:bold bold 10pt arial, Helvetica, sans-serif"" ><i>Annual Examination Results  " + reader("ACY_DESCR") + "")
            sb.AppendLine("<br>British Curriculum</font></i></TD></tr>")

            sb.AppendLine("<TR  align=center>")
            sb.AppendLine("<td>&nbsp;</td><TD><font  style=""text-decoration:NONE;font:bold bold 8pt arial, Helvetica, sans-serif"">GRADE :    " + reader("GRM_DISPLAY") + "   &nbsp;&nbsp; SECTION : " + reader("SCT_DESCR") + "</font> </TD> </TR>")


            sb.AppendLine("</table>")




            sb.AppendLine("</TD>")

            sb.AppendLine("<TD WIDTH=15% VALIGN=TOP ALIGN=""RIGHT"">")
            sb.AppendLine("<table width=""85%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif""> MINISTRY OF EDUCATION </TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> KNOWLEDGE AND HUMAN </TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> DEVELOPMENT AUTHORITY </TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> SCHOOL LICENSE NO : " + reader("ACD_MOEAFFLNO") + " </TD></TR>")
            sb.AppendLine("</table>")
            sb.AppendLine("</TD>")



            'sb.AppendLine("<TD WIDTH=30%  VALIGN=TOP>")

            'sb.AppendLine("<table width=""90%"" height=""50"" align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font:bold bold 8Pt Verdana,arial, Helvetica, sans-serif"" > ")
            'sb.AppendLine("<TR><TD align=""center""> <Img Src=""..\..\images\MOE.GIF"" height=""70""></TD>")
            'sb.AppendLine("</TR>")
            'sb.AppendLine("</table><BR>")



            'sb.AppendLine("</TD>")


            sb.AppendLine("</TR>")

            'sb.AppendLine("<TR><TD colspan=2><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">&nbsp;&nbsp;&nbsp;&nbsp; Class Teacher :  " + reader("EMP_NAME") + " </TD>")
            'sb.AppendLine("<TD align=right><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> Date : " + hfSubmitDate.Value + " </TD>")
            'sb.AppendLine("</TR>")



            sb.AppendLine("</Table><BR>")

            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function

    Function GetSubjectsHeader(ByVal Subjects(,) As String) As String
        Dim sb As New StringBuilder
        Dim i As Integer

        Dim strSubjects As String = ""
        Dim strMax As String = ""
        Dim strMin As String = ""
        Dim imgUrl As String = ""

        Dim imageLib As ImageCreationLibrary
        Dim imgFilePath As String = ""
        hfTableColumns.Value = 0
        For i = 0 To Subjects.GetLength(0) - 1
            imgUrl = Subjects(i, 0)

            If checkUpper(imgUrl) = True Then
                imgUrl += "_UPPERCASE"
            End If
            imgUrl = imgUrl.Replace(" ", "%20")

            strSubjects += "<td width=""20"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\" + imgUrl.Replace("/", "_") + ".jpg" + "></td>"
            hfTableColumns.Value += 1

            strMax += "<td width=""20"" align=""middle"" Class=repcolDetail><center>" + Subjects(i, 3) + "</td>"
            strMin += "<td width=""20"" align=""middle"" Class=repcolDetail><center>" + Subjects(i, 4) + "</td>"
        Next

        sb.AppendLine("<table border=""1"" bordercolorlight=""#000000"" WIDTH=""1500"" align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font:NORMAL NORMAL 7pt Verdana, arial, Helvetica, sans-serif"" >")

        sb.AppendLine("<tr height=50>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""BOTTOM"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\SERIAL%20NO._UPPERCASE.jpg></td>")
          sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""BOTTOM"" rowspan=3 Class=repcolDetail ><center><img src=..\BrownBook\SubjectVerticalText\STUDENT%20NO._UPPERCASE.jpg></td>")
        sb.AppendLine("<td  width=""250"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> STUDENT NAME <BR> AS IN THE PASSPORT <BR> (ENGLISH)</td>")
        sb.AppendLine("<td  width=""250"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> STUDENT NAME <BR> AS IN THE PASSPORT  <BR> (ARABIC)</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" valign=""bottom"" Class=repcolDetail><img src=..\BrownBook\SubjectVerticalText\NATIONALITY.jpg></td>")
        hfTableColumns.Value += 5
        sb.AppendLine(strSubjects)
        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\Attendance.jpg></td>")
        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\PERCENTAGE.jpg></td>")
        sb.AppendLine("<td ALIGN=middle valign=""middle"" rowspan=3 Class=repcolDetail ><center>REMARKS</td>")
        sb.AppendLine("</tr>")
        sb.AppendLine("<tr>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine(strMax)
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("</tr>")
        sb.AppendLine("<tr>")


        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        
        sb.AppendLine(strMin)
        sb.AppendLine("<td  align=""middle"" Class=repcolDetail>&nbsp;</td>")

        hfTableColumns.Value += 3


        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function

    Function GetStudentMarks(ByVal sct_id As String, ByVal Subjects(,) As String)

        hfReg_L.Value = 0
        hfReg_NL.Value = 0
        hfPass_NL.Value = 0
        hfPass_L.Value = 0


        Dim strTable As String

        Dim strFooter As String = GetBrownBookFooter()

        Dim strPassed As String = GetStudents(sct_id, Subjects, "PASS", strFooter)
        Dim strRetest As String = GetStudents(sct_id, Subjects, "RETEST", strFooter)
        Dim strDetained As String = GetStudents(sct_id, Subjects, "FAIL", strFooter)



        Dim promotedGrade As String = GetpromotedGrade()

        If promotedGrade <> "" Then
            If promotedGrade <> "10" Then
                promotedGrade = Replace(promotedGrade, "0", "")
            End If
            strPassed = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:9pt Arial"" ><b>PASSED AND PROMOTED TO YEAR " + promotedGrade + "</b></font></td>" + strPassed
        End If

        If strRetest <> "" Then
            strRetest = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:9pt Arial"" ><b>RETEST</b></font></td>" + strRetest
        End If

        If strDetained <> "" Then
            strDetained = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:9pt Arial size=""3px"" ><b>STUDENTS DETAINED</b></font></td>" + strDetained
        End If

        strTable = strPassed + strRetest + strDetained

        If hfLastRecord.Value = 0 Then
            '  strTable += GetRedLine()
            'strDetained += "</TABLE>"
            strTable += strFooter
        End If

        strTable = strTable.Replace("%reg_l%", hfReg_L.Value.ToString)
        strTable = strTable.Replace("%reg_nl%", hfReg_NL.Value.ToString)
        strTable = strTable.Replace("%pass_l%", hfPass_L.Value.ToString)
        strTable = strTable.Replace("%pass_nl%", hfPass_NL.Value.ToString)
        strTable = strTable.Replace("%reg_t%", (Val(hfReg_L.Value) + Val(hfReg_NL.Value)).ToString)
        strTable = strTable.Replace("%pass_t%", (Val(hfPass_L.Value) + Val(hfPass_NL.Value)).ToString)



        hfPageNumber.Value += 1
        strTable = strTable.Replace("%pageno%", hfPageNumber.Value)
        strTable = strTable.Replace("%totalpages%", hfPageNumber.Value)



        'UPDATE RETEST COUNT


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = " SELECT SBG_DESCR,COUNT(FRR_ID) FROM STUDENT_M AS A" _
                    & " INNER JOIN RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID AND A.STU_ACD_ID=B.FRR_ACD_ID" _
                    & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID AND A.STU_ACD_ID=C.FTM_ACD_ID" _
                    & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.FTM_SBG_ID=D.SBG_ID" _
                    & " WHERE FRR_RESULT='RETEST' AND FTM_RESULT='FAIL' AND ISNULL(SBG_bRETEST,'FALSE')='TRUE' " _
                    & " AND STU_SCT_ID=" + sct_id + " GROUP BY SBG_DESCR  ORDER BY SBG_DESCR"
        Else
            str_query = " SELECT SBG_DESCR,COUNT(FRR_ID) FROM VW_STUDENT_DETAILS_PREVYEARS AS A" _
                  & " INNER JOIN RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID AND A.STU_ACD_ID=B.FRR_ACD_ID" _
                  & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID AND A.STU_ACD_ID=C.FTM_ACD_ID" _
                  & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.FTM_SBG_ID=D.SBG_ID" _
                  & " WHERE FRR_RESULT='RETEST' AND FTM_RESULT='FAIL' AND ISNULL(SBG_bRETEST,'FALSE')='TRUE' " _
                  & " AND STU_SCT_ID=" + sct_id + " GROUP BY SBG_DESCR  ORDER BY SBG_DESCR"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer
        Dim p As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace("%retestsub" + (i + 1).ToString + "%", "No. Retest in " + .Item(0).ToString)
                strTable = strTable.Replace("%rt" + (i + 1).ToString + "%", .Item(1).ToString)
            End With
        Next


        'clear extra subjets
        For i = ds.Tables(0).Rows.Count - 1 To 7
            strTable = strTable.Replace("%retestsub" + (i + 1).ToString + "%", "&nbsp;")
            strTable = strTable.Replace("%rt" + (i + 1).ToString + "%", "&nbsp;")

        Next


        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim id As String = ""

        Dim strStuId As String = ""
        Dim strRetestSubjects As String = ""

        'POPULATE STUDENT MARKS
        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT     SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
                                    & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
                                    & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
                                    & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
                                    & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT,ISNULL(FRR_RESULT_EDITED,'') AS FRR_RESULT_EDITED,ISNULL(SBG_bRETEST,'FALSE') AS SBG_bRETEST FROM" _
                                    & " STUDENT_M AS A  INNER JOIN STUDENT_GROUPS_S AS P " _
                                    & " ON A.STU_ID=P.SSD_STU_ID AND A.STU_ACD_ID=P.SSD_ACD_ID INNER JOIN  dbo.GETBROWNBOOKSUBJECTS_JC(" + hfACD_ID.Value + ",'" + hfGRD_ID.Value + "','" + sct_id + "') B ON P.SSD_SBG_ID=B.SBG_ID" _
                                    & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
                                    & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
                                    & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
                                    & " WHERE STU_SCT_ID=" + sct_id _
                                     & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'" _
                                    & "  ORDER BY STU_ID"
        Else
            str_query = "SELECT   SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
                                & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
                                & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
                                & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
                                & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT,ISNULL(FRR_RESULT_EDITED,'') AS FRR_RESULT_EDITED,ISNULL(SBG_bRETEST,'FALSE') AS SBG_bRETEST FROM" _
                                & " VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN STUDENT_GROUPS_S AS P " _
                                & " ON A.STU_ID=P.SSD_STU_ID AND A.STU_ACD_ID=P.SSD_ACD_ID INNER JOIN  dbo.GETBROWNBOOKSUBJECTS_JC(" + hfACD_ID.Value + ",'" + hfGRD_ID.Value + "','" + sct_id + "') B ON P.SSD_SBG_ID=B.SBG_ID" _
                                & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
                                & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
                                & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
                                & " WHERE STU_SCT_ID=" + sct_id _
                                & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'" _
                                & " ORDER BY STU_ID"
        End If



        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)

                If strStuId <> .Item(1).ToString Then
                    If strRetestSubjects <> "" Then
                        strTable = strTable.Replace(strStuId + "_RESULT", "<font >RETEST IN " + strRetestSubjects + "</FONT>")
                    End If
                    strStuId = .Item(1).ToString
                    strRetestSubjects = ""
                End If

                id = .Item(1).ToString + "_" + .Item("SBG_DESCR").ToString
                If .Item(8).ToString.ToUpper = "FAIL" Then
                    If .Item(2).ToString = "0" Then
                        strMark = "<font >&nbsp;</font>"
                        strGrade = "<font >&nbsp;</font>"
                    Else
                        strMark = "<font color=""red"">" + .Item(2).ToString + "</font>"
                        strGrade = "<font color=""red"">" + .Item(3).ToString + "</font>"
                    End If


                    If .Item("SBG_bRETEST").ToString.ToLower = "true" Then
                        If strRetestSubjects <> "" And .Item(6).ToString.ToUpper = "RETEST" Then
                            'strRetestSubjects += ",<BR/>"
                            strRetestSubjects += ", "
                        End If

                        strRetestSubjects += .Item(7).ToString
                    End If
                Else
                    'If .Item(2).ToString = "0" Then
                    '    strMark = "<font >&nbsp;</font>"
                    '    strGrade = "<font >&nbsp;</font>"
                    'Else
                    strMark = .Item(2).ToString
                    strGrade = .Item(3).ToString
                    'End If
                End If
                strTable = strTable.Replace(id + "_M", strGrade)
                strTable = strTable.Replace(id + "_G", strGrade)

                If .Item(6).ToString.ToUpper = "PASS" Then
                    If .Item(9).ToString.ToUpper <> "" Then
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", .Item(9).ToString.Replace("''", "'"))
                    Else
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "PASSED")
                        ' strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "Promoted")
                    End If
                ElseIf .Item(6).ToString.ToUpper = "FAIL" Then
                    strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "<font >Detained</font>")
                End If

                If i = ds.Tables(0).Rows.Count - 1 Then
                    If strRetestSubjects <> "" Then
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "<font >Retest In" + strRetestSubjects + "</font>")
                    End If
                End If

            End With
        Next

        ''POPULATE CONDUCT,TOTAL,PERCENTAGE AND RANK

        str_query = "EXEC RPT.GETSTUDENTTOTALPERCENTAGE_JC " + sct_id
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace(.Item(0).ToString + "_TOTAL", .Item(1).ToString)
                strTable = strTable.Replace(.Item(0).ToString + "_NONMUSLIMS", .Item(2).ToString)
                strTable = strTable.Replace(.Item(0).ToString + "_PERCENTAGE", .Item(3).ToString)
            End With
        Next

        ''POPULATE ATTNDANCE
        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID,isnull(ATT_TOTALDAYS,0) AS ATTPERCENTAGE FROM STUDENT_M AS A " _
                    & " LEFT OUTER JOIN RPT.FINAL_ATTENDANCE AS B ON b.ATT_STU_ID=A.STU_ID AND B.ATT_ACD_ID=A.STU_ACD_ID " _
                    & " WHERE STU_SCT_ID=" + sct_id
        Else
            str_query = "SELECT STU_ID,isnull(ATT_TOTALDAYS,0)-isnull(ATT_VALUE,0) AS ATTPERCENTAGE FROM VW_STUDENT_DETAILS_PREVYEARS AS A " _
                 & " LEFT OUTER JOIN RPT.FINAL_ATTENDANCE AS B ON b.ATT_STU_ID=A.STU_ID AND B.ATT_ACD_ID=A.STU_ACD_ID " _
                 & " WHERE STU_SCT_ID=" + sct_id
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace(.Item(0).ToString + "_ATTENDANCE", IIf(.Item(1).ToString = "0", "&nbsp;", .Item(1).ToString))
            End With
        Next

        strTable = ClearEmptyColumns(sct_id, Subjects, strTable)
        Return strTable
    End Function
    Function ClearEmptyColumns(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal strTable As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID FROM STUDENT_M WHERE STU_SCT_ID=" + sct_id _
                        & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'"
        Else
            str_query = "SELECT STU_ID FROM STUDENT_M INNER JOIN OASIS..STUDENT_PROMO_S ON STU_ID=STP_STU_ID WHERE STP_SCT_ID=" + sct_id _
                  & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'"

        End If

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i, j As Integer
        Dim id As String
        For i = 0 To ds.Tables(0).Rows.Count - 1
            For j = 0 To Subjects.GetLength(0) - 1
                id = ds.Tables(0).Rows(i).Item(0).ToString + "_" + Subjects(j, 0) + "_M"
                strTable = strTable.Replace(id, "&nbsp;")
            Next
        Next

        Return strTable
    End Function


    Function GetpromotedGrade() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TOP 1 ISNULL(GRD_ID,'') FROM GRADE_M WHERE GRD_DISPLAYORDER>" _
                                  & " (SELECT GRD_DISPLAYORDER FROM GRADE_M WHERE GRD_ID='" + hfGRD_ID.Value + "') ORDER BY GRD_DISPLAYORDER"
        Dim grdid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return grdid
    End Function

    Function GetRedLine() As String
        Dim i As Integer
        Dim sb As New StringBuilder
        sb.AppendLine("<tr>")
        For i = 0 To Val(hfTableColumns.Value) - 1
            sb.AppendLine("<td STYLE=""BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 7pt verdana,timesroman,garamond"">&nbsp;</td>")
        Next
        sb.AppendLine("</tr>")
        Return sb.ToString
    End Function

    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal result As String, ByVal strFooter As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strSubjectHeader As String = GetSubjectsHeader(Subjects)
        Dim strPageHeader As String = GetBrownBookHeader(sct_id)
        Dim i As Integer
        Dim j As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()

        Dim strRedLine As String



        Dim str_query As String

        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                    & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                                    & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                                    & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                                    & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                                    & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                                    & " STU_ID,ISNULL(FRR_RESULT,'PASSED') AS RESULT,CTY_NATIONALITY AS NATIONALITY,ISNULL(STU_GENDER,'') STU_GENDER FROM STUDENT_M AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                                    & " INNER JOIN RPT.FINAL_RESULT_S AS C ON A.STU_ID=C.FRR_STU_ID AND A.STU_ACD_ID=C.FRR_ACD_ID" _
                                    & " LEFT OUTER JOIN OASIS..COUNTRY_M AS D ON A.STU_NATIONALITY=D.CTY_ID" _
                                    & " WHERE STU_SCT_ID=" + sct_id + "  AND STU_CURRSTATUS<>'CN' " _
                                    & " AND FRR_RESULT='" + result + "' " _
                                    & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "' AND STU_MINLIST='REGULAR'" _
                                    & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Else
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                        & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                        & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                        & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                        & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                        & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                        & " STU_ID,ISNULL(FRR_RESULT,'PASSED') AS RESULT,CTY_NATIONALITY as NATIONALITY,ISNULL(STU_GENDER,'') STU_GENDER FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                        & " INNER JOIN RPT.FINAL_RESULT_S AS C ON A.STU_ID=C.FRR_STU_ID AND A.STU_ACD_ID=C.FRR_ACD_ID" _
                        & " LEFT OUTER JOIN OASIS..COUNTRY_M AS D ON A.STU_NATIONALITY=D.CTY_ID" _
                        & " WHERE STU_SCT_ID=" + sct_id + "  AND STU_CURRSTATUS<>'CN' " _
                        & " AND FRR_RESULT='" + result + "' " _
                        & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "' AND STU_MINLIST='REGULAR'" _
                        & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count

        If result.ToUpper = "PASS" Then
            hfPassedCount.Value = ds.Tables(0).Rows.Count
        ElseIf result.ToUpper = "FAIL" Then
            hfDetained.Value = ds.Tables(0).Rows.Count
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                hfRecordNumber.Value += 1
                If hfRecordNumber.Value = hfRecordsPerPage.Value + 1 Then
                    If i = ds.Tables(0).Rows.Count - 1 And result.ToUpper = "FAIL" Then
                        hfLastRecord.Value = 1
                        sb.AppendLine(GetRedLine)
                    End If
                    hfPageNumber.Value += 1
                    sb.AppendLine(strFooter.Replace("%pageno%", hfPageNumber.Value))

                    'sb.AppendLine("</table>")
                    ' If i <> ds.Tables(0).Rows.Count - 1 Then
                    If hfLastRecord.Value <> 1 Then
                        sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                    End If
                    'End If
                    sb.AppendLine(strPageHeader)
                    sb.AppendLine(strSubjectHeader)
                    hfRecordNumber.Value = 1
                End If

                stuNames = .Item(0).ToString.Trim.Replace("  ", " ").Split(" ")

                If .Item("NATIONALITY").ToString = "UAE" Then
                    hfReg_L.Value += 1
                    If result.ToUpper <> "FAIL" And result.ToUpper <> "RETEST" Then
                        hfPass_L.Value += 1
                    End If
                Else
                    hfReg_NL.Value += 1
                    If result.ToUpper <> "FAIL" And result.ToUpper <> "RETEST" Then
                        hfPass_NL.Value += 1
                    End If
                End If

                If result.ToUpper = "RETEST" Then 'add and empty row above  the same student

                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td HEIGHT=""22px"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_BLUEID") + "</td>")

                  
                    If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                        sb.AppendLine("<td align=""left"" width=""200"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                    Else
                        sb.AppendLine("<td align=""left"" width=""200"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                    End If
                    sb.AppendLine("<td align=""right"" ><font name=""arial"" size=""2px"">" + .Item("STU_ARABICNAME") + "</font></td>")


                 
                    '     sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;" + .Item("NATIONALITY").ToString + "</td>")


                    For j = 0 To Subjects.GetLength(0) - 1
                        If Subjects(j, 2).ToLower = "true" Then
                            sb.AppendLine("<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Arial"">&nbsp;</font></td>")
                        End If
                    Next


                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")



                       sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    'sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")


                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")


                    sb.AppendLine("</tr>")
                End If

                sb.AppendLine("<tr>")
                sb.AppendLine("<td HEIGHT=""22px"" align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_BLUEID") + "</td>")


              
                If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                    sb.AppendLine("<td align=""left"" width=""200"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                Else
                    sb.AppendLine("<td align=""left"" width=""200"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                End If
                sb.AppendLine("<td align=""right"" ><font name=""arial"" size=""2px"">" + .Item("STU_ARABICNAME") + "</font></td>")


             
                ' sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")

                sb.AppendLine("<td align=""left"" Class=repcolDetail>&nbsp;" + .Item("NATIONALITY").ToString + "</td>")


                For j = 0 To Subjects.GetLength(0) - 1

                    sb.AppendLine("<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Arial"">" + .Item(4).ToString + "_" + Subjects(j, 0) + "_M" + "</font></td>")

                Next



                'Attendance
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_ATTENDANCE</font></td>")

                
                'sb.AppendLine("<td align=""middle"" Class=repcolDetail><font style=""font:8pt Arial"">&nbsp;</font></td>")

                If hfGRD_ID.Value = "KG1" Or hfGRD_ID.Value = "KG2" Then
                    sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">&nbsp;</font></td>")
                    sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"" >&nbsp;</font></td>")
                Else
                    'Percentage
                    sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_PERCENTAGE</font></td>")
                    'Result
                    sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"" >" + .Item("STU_ID").ToString + "_RESULT </font></td>")
                End If


                sb.AppendLine("</tr>")


            End With
        Next





        Return sb.ToString
    End Function


    Public Function checkUpper(ByVal str As String) As Boolean
        If Strings.StrComp(str, Strings.UCase(str)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetBrownBookFooter() As String
        Dim sb As New StringBuilder





        'sb.AppendLine("<TD  rowspan=2 align=middle colspan=5>")

        sb.AppendLine("</table>")

        sb.Append("<table border=0 width=""1500"">")
     

        sb.AppendLine("<tr><td>")
        sb.AppendLine("<table  border=0 width=""100%""  align=""center"" cellspacing=""0"" cellpadding=""2""  style=""text-decoration:NONE;font:normal normal 7Pt Verdana, arial, Helvetica, sans-serif"" >")
        sb.AppendLine("<TR HEIGHT=35> <TD align=left width=150>Registrar :</TD> <TD align=left>" + GetEmpName("REGISTRAR", "") + "</TD> ")
        ' sb.AppendLine("<TD width=300 rowspan=2 align=middle>&nbsp;</TD> ")

        sb.AppendLine("<TD  align=left>   Principal:&nbsp;&nbsp;</TD> <TD width=200>&nbsp;&nbsp;" + GetEmpName("Principal", "full") + "</TD> ")
        sb.AppendLine("<TD  align=left>   Name of the Verifier in Ministry :&nbsp;&nbsp;</TD> <TD width=200>&nbsp;&nbsp;</TD> ")

        sb.AppendLine("</TR></td>")



        sb.AppendLine("<TR HEIGHT=35> <TD align=left>Signature :</TD> <TD>_______________________________</TD> ")
        sb.AppendLine("<TD align=left>Signature :</TD> <TD>_______________________________</TD> ")
        sb.AppendLine(" <TD align=left>Signature :</TD> <TD>_______________________________</TD> ")
        sb.AppendLine("</TR></table></td>")


        sb.AppendLine("<td align=right>")
        sb.AppendLine("<table  border=1width=100% bordercolorlight=#000000  align=""center"" cellspacing=""0"" cellpadding=""2""  style=""text-decoration:NONE;font:normal normal 7Pt Verdana, arial, Helvetica, sans-serif"" >")
        sb.AppendLine("<tr><td style=""border-bottom: #000000 1pt solid; border-right: #000000 1pt solid ;"">Nationality</td><td  style=""border-bottom: #000000 1pt solid; border-right: #000000 1pt solid ;"">Number registered</td><td  style=""border-bottom: #000000 1pt solid; border-right: #000000 1pt solid ;"">Number Present</td><td  style=""border-bottom: #000000 1pt solid;"">Number Passed</td></tr>")
        sb.AppendLine("<tr><td style=""border-right: #000000 1pt solid ;"">Local</td><td style=""border-right: #000000 1pt solid ;"" align=center>%reg_l%</td><td style=""border-right: #000000 1pt solid ;"" align=center>%reg_l%</td><td align=center>%pass_l%</td></tr>")
        sb.AppendLine("<tr><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"">Non Local</td><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"" align=center>%reg_nl%</td><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"" align=center>%reg_nl%</td><td style=""border-top: #000000 1pt solid;"" align=center>%pass_nl%</td></tr>")
        sb.AppendLine("<tr><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"">Total</td><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"" align=center>%reg_t%</td><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"" align=center>%reg_t%</td><td style=""border-top: #000000 1pt solid;"" align=center>%pass_t%</td></tr>")
        sb.AppendLine("</table>")
        sb.AppendLine("</td></tr>")


       
        'If hfChecker.Value = "" Then
        '    sb.AppendLine("<TD align=left width=150 >Checked By :</TD><TD width=150>______________________________</TD> ")
        'Else
        '    sb.AppendLine("<TD align=left >Re-checker :</TD> <TD width=150>" + hfChecker.Value + "</TD> ")
        'End If
        '' sb.AppendLine("<TD width=300 rowspan=2 align=middle><img src=""..\..\images\brownbookseal.jpg""</TD> ")





        ''sb.AppendLine("<TD width=300 rowspan=2 align=middle><img src=""..\..\images\SchoolStamp.jpg""/></TD> ")
        '    sb.AppendLine("<TR Height=35> <TD Colspan=" + hfTableColumns.Value + " align=right>page&nbsp;%pageno% of %totalpages% &nbsp;&nbsp;&nbsp;&nbsp;</TD></TR>")
        'sb.AppendLine("</Table>")
        '  sb.AppendLine("<td></tr>")

        sb.AppendLine("</table>")
        Return sb.ToString
    End Function

    Function GetEmpName(ByVal designation As String, ByVal sType As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If sType = "Short" Then
            str_query = "SELECT Left(ISNULL(EMP_FNAME,''),1)+'. '+Left(ISNULL(EMP_MNAME,''),1)+'. '+ISNULL(EMP_LNAME,'') FROM " _
                                     & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE  emp_status=1 and EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                     & " AND DES_DESCR like '" + designation + "%'"
        Else
            str_query = "SELECT isnull(EMP_SALUTE,'')+' '+ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                     & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE  emp_status=1 and EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                     & " AND DES_DESCR like '" + designation + "%'"
        End If

        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return emp
    End Function
#End Region


End Class
