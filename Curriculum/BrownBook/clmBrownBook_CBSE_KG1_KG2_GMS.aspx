<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmBrownBook_CBSE_KG1_KG2_GMS.aspx.vb" Inherits="Curriculum_BrownBook_clmBrownBook_CBSE_KG1_KG2_GMS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
textarea {
overflow:auto;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:Literal ID="ltBrownBook" runat="server"></asp:Literal>
        <asp:HiddenField ID="hfSBG_IDS" runat="server" /><asp:HiddenField ID="hfRecordNumber" runat="server" Visible="False" />
        <asp:HiddenField ID="hfPageNumber" runat="server" Visible="False" />
        <asp:HiddenField ID="hfTableColumns" runat="server" Visible="False" />
        <br />
         <asp:HiddenField ID="hfRecordsPerPage" runat="server" Visible="False" />
        <asp:HiddenField ID="hfLastRecord" runat="server" Value="0" Visible="False" />
        </div><asp:HiddenField ID="hfClassTeacher" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfSubmitDate" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfPassedCount" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfTotalStudents" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfDetained" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfSTM_ID" runat="server" />
        <asp:HiddenField ID="hfShowParent1" runat="server" />
        <asp:HiddenField ID="hfShowParent2" runat="server" /><asp:HiddenField ID="hfRpfDate" runat="server" Value="0" Visible="False" />
        <br /><asp:HiddenField ID="hfBrownBookDate" runat="server" Value="0" Visible="False" />
        &nbsp;<br />
        &nbsp;
        <asp:HiddenField ID="hfACD_ID" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfChecker" runat="server" Value="" Visible="False" />
      
    </form>
</body>
</html>
