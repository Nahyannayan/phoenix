Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Math
Partial Class Curriculum_BrownBook_clmBrownBookAttendance
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300310") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                    BindGrade()
                    PopulateSection()

                    'tblTC.Rows(4).Visible = False
                    'tblTC.Rows(5).Visible = False

                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub


#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub PopulateSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + grade(0) + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)

    End Sub


    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY," _
                                & " GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN STREAM_M AS E ON A.GRM_STM_ID=E.STM_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)

    End Sub


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String


        str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                    & " STU_SCT_ID,STU_GRD_ID,GRM_DISPLAY,SCT_DESCR,FRR_RESULT_EDITED,ATT_VALUE,ATT_TOTALDAYS,CONVERT(INT,ROUND(ISNULL(CONVERT(NUMERIC(10,2),ATT_VALUE)*100/CONVERT(NUMERIC(10,2),ATT_TOTALDAYS),0),0)) AS ATT_PERCENT" _
                    & " FROM STUDENT_M  AS A" _
                    & " INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                    & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                    & " INNER JOIN OASIS_CURRICULUM.RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID" _
                    & " INNER JOIN OASIS_CURRICULUM.RPT.FINAL_ATTENDANCE AS E ON E.ATT_STU_ID=A.STU_ID AND E.ATT_ACD_ID=A.STU_ACD_ID" _
                    & " WHERE CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                    & " AND STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + hfACD_ID.Value



        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            Dim grade As String() = hfGRD_ID.Value.Split("|")
            str_query += " AND STU_GRD_ID= '" + grade(0) + "' AND STU_STM_ID=" + grade(1)
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox



        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ViewState("norecord") = "1"
        Else
            gvStud.DataBind()
            ViewState("norecord") = "0"
        End If



        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub
    Sub SaveData(ByVal stu_id As String, ByVal att As String, ByVal total As String, ByVal remarks As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC RPT.saveFINALATTENDANCEANDREMARKS " _
                                  & stu_id + "," _
                                  & hfACD_ID.Value + "," _
                                  & "'" + att + "'," _
                                  & "'" + total + "'," _
                                  & IIf(remarks <> "", "'" + remarks.Replace("'", "''") + "'", "NULL")

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    End Sub

#End Region
    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEdit As LinkButton = DirectCast(sender, LinkButton)
        Dim txtATT As TextBox = TryCast(sender.FindControl("txtATT"), TextBox)
        Dim txtTotal As TextBox = TryCast(sender.FindControl("txtTotal"), TextBox)
        Dim txtRemarks As TextBox = TryCast(sender.FindControl("txtRemarks"), TextBox)

        If lblEdit.Text = "Edit" Then
            lblEdit.Text = "Update"
            txtATT.Enabled = True
            txtTotal.Enabled = True
            txtRemarks.Text = txtRemarks.Text.Replace("''", "'")
            txtRemarks.Enabled = True
        Else
            lblEdit.Text = "Edit"
            Dim lblStuId As Label = TryCast(sender.FindControl("lblStuId"), Label)
            SaveData(lblStuId.Text, txtATT.Text, txtTotal.Text, txtRemarks.Text)
            txtATT.Enabled = False
            txtTotal.Enabled = False
            txtRemarks.Enabled = False
        End If
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        hfSCT_ID.Value = ddlSection.SelectedValue.ToString
        hfNAME.Value = txtName.Text
        hfSTUNO.Value = txtStuNo.Text

        GridBind()
        'tblTC.Rows(4).Visible = True
        'tblTC.Rows(5).Visible = True
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        PopulateSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
End Class
