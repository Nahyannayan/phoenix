<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmBrownBook_View.aspx.vb" Inherits="Curriculum_BrownBook_clmBrownBook_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
 
         
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Brown Book
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_AddGroup" runat="server" align="center" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left">
                &nbsp;<asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error"
                    EnableViewState="False" ForeColor="" HeaderText="You must enter a value in the following fields:"
                    SkinID="error" ValidationGroup="groupM1" Width="442px"></asp:ValidationSummary></td>
        </tr>
        <tr valign="bottom">
            <td align="left" valign="bottom">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"></asp:Label></td>
        </tr>
        <tr>
            <td >
                <table align="center" width="100%"  cellpadding="5" cellspacing="0"
                    >
                    
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label"> Academic Year</span></td>
                        
                        <td align="left"  >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" Style="position: relative" AutoPostBack="True" Width="129px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label"> Grade</span></td>
                       
                        <td align="left"  >
                            <asp:DropDownList ID="ddlGrade" runat="server" Style="position: relative" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left"  >
                            <span class="field-label">Section</span></td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlSection" runat="server" Style="position: relative">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Submit Date</span></td>
                        
                        <td align="left" >
                            <asp:TextBox id="txtDate" runat="server">
                            </asp:TextBox>&nbsp;<asp:ImageButton id="imgDate" runat="server" ImageUrl="~/Images/calendar.gif">
                            </asp:ImageButton></td>
                        <td align="left" >
                            <span class="field-label">Checked By</span></td>
                        
                        <td align="left" >
                            <asp:TextBox id="txtChecker" runat="server">
                            </asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label"> Records&nbsp; Per Page</span></td>
                       
                        <td align="left"  >
                            <asp:TextBox id="txtRecords" runat="server">
                            </asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td  style="height: 1px" valign="bottom">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td  style="height: 19px" valign="bottom" align="center">
                &nbsp;
                <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" ValidationGroup="groupM1" />&nbsp;
            </td>
        </tr>
        <tr>
            <td  valign="bottom">
                &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDate"
        TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
        Display="None" ErrorMessage="Please enter the submit date" ValidationGroup="groupM1">
    </asp:RequiredFieldValidator>
    <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    

                    </div>
            </div>
        </div>

</asp:Content>

