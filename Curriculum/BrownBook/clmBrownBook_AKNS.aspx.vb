Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_BrownBook_clmBrownBook_AKNS
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Dim studclass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+"))
            hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))
            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)
            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            hfChecker.Value = Encr_decrData.Decrypt(Request.QueryString("checker").Replace(" ", "+"))

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300110") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                SetHowParent()
                BindBrownbookDate()
                hfRecordNumber.Value = 0
                hfTableColumns.Value = 0
                hfPageNumber.Value = 0
                GetBrownBook(grd_id(0), Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")), grd_id(1))


                'Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'End Try
            End If
        End If
    End Sub
#Region "Private Methods"

    Sub BindBrownbookDate()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(ACD_BROWNBOOKDATE,getdate()) FROM ACADEMICYEAR_D WHERE ACD_ID=" + Session("CURRENT_ACD_ID")
        hfBrownBookDate.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub SetHowParent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE')  " _
                               & " FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfShowParent1.Value = ds.Tables(0).Rows(0).Item(0)
        hfShowParent2.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetBrownBook(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim Subjects As String(,) = GetBrowbBookSubjects(grd_id, hfACD_ID.Value, stm_id, sct_id)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: BROWN BOOK ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<Link REL=STYLESHEET HREF=""..\cssfiles\brownbook.css"" TYPE=""text/css"">")
        sb.AppendLine("<BODY>")
        sb.AppendLine("<Script language=""JavaScript"" src=""../../include/com_client_func.js""></Script>")
        sb.AppendLine("<style>	body,table,td{background-color:#ffffff!important;}")
        sb.AppendLine("th{background-color:#ffffff!important;}")
        sb.AppendLine("</style>")
        If sct_id = "ALL" Then
            str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                      & "' AND SCT_ACD_ID=" + Session("Current_ACD_ID")
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetbrownBookForSection(grd_id, .Rows(i).Item(0).ToString, Session("Current_ACD_ID"), Subjects))
                Next
            End With
            'ltBrownBook.Text = sb.ToString
        Else
            sb.AppendLine(GetbrownBookForSection(grd_id, sct_id, Session("Current_ACD_ID"), Subjects))

            'ltBrownBook.Text = sb.ToString
        End If
        sb.AppendLine("</BODY></HTML>")
        Response.Write(sb.ToString())


    End Sub

    Function GetBrowbBookSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        'ALL ARABIC SUBJECTS SHOULD COME UNDER ON COLUMS
        'SECOND LANGUAGE SHOULD COME UNDER ONE COLUMN

        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_ORDER,SBG_bMAJOR,SBG_MAXMARK,SBG_MINMARK FROM GETBROWNBOOKSUBJECTS_AKNS(" + acd_id + ",'" + grd_id + "') WHERE SBG_bMAJOR=1" _
                                & " ORDER BY SBG_ORDER"


        Dim imageLib As ImageCreationLibrary
        Dim stImagePath As String
        imageLib = New ImageCreationLibrary(stImagePath)

        Dim imgFilePath As String = ""
        stImagePath = Server.MapPath("~/Curriculum/BrownBook/SubjectVerticalText/")

        imageLib.BaseImagePath = stImagePath


        Dim f As New Font("Arial", 10, FontStyle.Regular, GraphicsUnit.Pixel, 0)



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count
        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 6)



        For j = 0 To i - 1
            With ds.Tables(0)




                'subject
                Subjects(j, 0) = .Rows(j).Item(0).ToString


                'sbgid
                'Subjects(j, 1) = .Rows(j).Item(1).ToString
                Subjects(j, 1) = 0

                'major minor




                Subjects(j, 2) = .Rows(j).Item(2).ToString





                Subjects(j, 3) = .Rows(j).Item(3).ToString.Replace(".000", "").Replace(".00", "")
                Subjects(j, 4) = .Rows(j).Item(4).ToString.Replace(".000", "").Replace(".00", "")



                'SBM_DESCR
                Subjects(j, 5) = .Rows(j).Item(0)

                'create image
                '  imageLib.CreateVerticalTextImage(.Rows(j).Item(0), New System.Drawing.Font(System.Drawing.FontFamily.GenericSerif, 12))
                imageLib.CreateVerticalTextImage(.Rows(j).Item(0), f)
            End With
        Next

        imageLib.CreateVerticalTextImage("CONDUCT", f)
        imageLib.CreateVerticalTextImage("NATIONALITY", f)
        imageLib.CreateVerticalTextImage("TOTAL", f)
        imageLib.CreateVerticalTextImage("PERCENTAGE", f)
        imageLib.CreateVerticalTextImage("RANK", f)

        Return Subjects
    End Function

    Function GetbrownBookForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,)) As String
        Dim sb As New StringBuilder
        Dim strHeader As String = GetBrownBookHeader(sct_id)
        ' sb.AppendLine(strHeader)
        sb.AppendLine("<table width=""1500""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=5>")

        sb.AppendLine("<tr><td algn=center >")
        sb.AppendLine(strHeader)
        sb.AppendLine("</td></tr>")

        sb.AppendLine("<tr><td algn=center VALIGN=TOP>")

        sb.AppendLine(GetSubjectsHeader(Subjects))

        sb.AppendLine(GetStudentMarks(sct_id, Subjects))

        sb.AppendLine("</table>")

        sb.AppendLine("</td></tr>")
        sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetBrownBookHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_MOE_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " ISNULL(EMP_SALUTE,'')+' '+ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table border=0 aligh=left width=100% border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR>")


            sb.AppendLine("<TD WIDTH=30% VALIGN=TOP align=center>")

            sb.AppendLine("<table width=""70%"" height=""30"" align=""center"" border=""1"" bordercolorlight=""#000000"" cellspacing=""0"" cellpadding=0  > ")
            sb.AppendLine("<TR><TD align=""center"">&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD>")
            sb.AppendLine("<TD align=right><font  size=2pt><b>خاص بلجنة التدقيق</b></font></TD></TR>")
            sb.AppendLine("<TR ><TD align=right><font  size=2pt><b>التاريخ</b></font></TD>")
            sb.AppendLine("<TD align=right><font  size=2pt><b>توقيعه</b></font></TD>")
            sb.AppendLine("<TD align=right><font  size=2pt><b>اسم المدقق</b></font></TD></TR>")
            sb.AppendLine("<TR><TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD>")
            sb.AppendLine("<TD>&nbsp;</TD></TR>")
            sb.AppendLine("</table><BR>")


            sb.AppendLine("</TD>")




            sb.AppendLine("<TD WIDTH=35% align=""center"" VALIGN=TOP>")

            'sb.AppendLine("<table width=""100%""  align=""center"" border=""1"" cellspacing=""0"" cellpadding=0>")
            'sb.AppendLine("<TR><TD valign=top rowspan=4><center><img src=..\" + reader("BSU_MOE_LOGO") + " height=100></center></TD></TR>")
            'sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font  style=""text-decoration:NONE;font:bold bold 14pt arial, Helvetica, sans-serif"">" + reader("BSU_NAME") + "</font>")
            'sb.AppendLine("<BR><font  style=""text-decoration:NONE;font:bold bold 10pt arial, Helvetica, sans-serif"">Annual Examination Results  " + reader("ACY_DESCR") + "<BR>The British Curriculum</font></TD></TR>")

            'sb.AppendLine("</table>")


            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font  style=""text-decoration:NONE;font:bold bold 14pt arial"" color=""#0155F9"">" + reader("BSU_NAME") + "</font>")
            sb.AppendLine("<BR><font  style=""text-decoration:NONE;font:bold bold 10pt arial, Helvetica, sans-serif"" color=""#0155F9"">Annual Examination Results  " + reader("ACY_DESCR") + "</font></TD>")
            sb.AppendLine("<TD valign=top><center><img src=..\" + reader("BSU_MOE_LOGO") + " height=100></center></TD></tr>")

            sb.AppendLine("<TR colpan=2 align=center>")
            sb.AppendLine("<TD><font  style=""text-decoration:NONE;font:bold bold 8pt arial, Helvetica, sans-serif"">GRADE :    " + reader("GRM_DISPLAY") + "   &nbsp;&nbsp; SECTION : " + reader("SCT_DESCR") + "</font> </TD> </TR>")


            sb.AppendLine("</table>")




            sb.AppendLine("</TD>")

            sb.AppendLine("<TD WIDTH=15% VALIGN=TOP ALIGN=""RIGHT"">")
            sb.AppendLine("<table width=""80%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif""> MINISTRY OF EDUCATION </TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> DUBAI EDUCATION ZONE </TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> PRIVATE EDUCATION DEPARTMENT </TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> SCHOOL LICENSE NO : " + reader("ACD_MOEAFFLNO") + " </TD></TR>")
            sb.AppendLine("</table>")
            sb.AppendLine("</TD>")



            sb.AppendLine("<TD WIDTH=30%  VALIGN=TOP>")

            sb.AppendLine("<table width=""90%"" height=""50"" align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font:bold bold 8Pt Verdana,arial, Helvetica, sans-serif"" > ")
            sb.AppendLine("<TR><TD align=""center""> <Img Src=""..\..\images\MOE.GIF"" height=""70""></TD>")
            sb.AppendLine("</TR>")
            sb.AppendLine("</table><BR>")



            sb.AppendLine("</TD>")


            sb.AppendLine("</TR>")

            'sb.AppendLine("<TR><TD colspan=2><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">&nbsp;&nbsp;&nbsp;&nbsp; Class Teacher :  " + reader("EMP_NAME") + " </TD>")
            'sb.AppendLine("<TD align=right><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> Date : " + hfSubmitDate.Value + " </TD>")
            'sb.AppendLine("</TR>")



            sb.AppendLine("</Table><BR>")

            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function

    Function GetSubjectsHeader(ByVal Subjects(,) As String) As String
        Dim sb As New StringBuilder
        Dim i As Integer

        Dim strSubjects As String = ""
        Dim strMax As String = ""
        Dim strMin As String = ""
        Dim imgUrl As String = ""

        Dim imageLib As ImageCreationLibrary
        Dim imgFilePath As String = ""
        hfTableColumns.Value = 0
        For i = 0 To Subjects.GetLength(0) - 1
            imgUrl = Subjects(i, 0)

            If checkUpper(imgUrl) = True Then
                imgUrl += "_UPPERCASE"
            End If
            imgUrl = imgUrl.Replace(" ", "%20")
            Select Case hfGRD_ID.Value
                Case "02", "03", "04", "05", "06", "07"
                    If Subjects(i, 0).ToUpper <> "FRENCH" Then
                        strSubjects += "<td width=""20"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\" + imgUrl.Replace("/", "_") + ".jpg" + "></td>"
                        hfTableColumns.Value += 1
                        strMax += "<td width=""20"" align=""middle"" Class=repcolDetail>" + Subjects(i, 3) + "</td>"
                        strMin += "<td width=""20"" align=""middle"" Class=repcolDetail>" + Subjects(i, 4) + "</td>"
                    End If
                Case Else
                    strSubjects += "<td width=""20"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\" + imgUrl.Replace("/", "_") + ".jpg" + "></td>"
                    hfTableColumns.Value += 1
                    strMax += "<td width=""20"" align=""middle"" Class=repcolDetail>" + Subjects(i, 3) + "</td>"
                    strMin += "<td width=""20"" align=""middle"" Class=repcolDetail>" + Subjects(i, 4) + "</td>"
            End Select
        
            
        Next

        sb.AppendLine("<table border=""1"" bordercolorlight=""#000000"" WIDTH=""1500"" align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font:NORMAL NORMAL 7pt Verdana, arial, Helvetica, sans-serif"" >")

        sb.AppendLine("<tr height=50>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> Sr. No  </td>")
        sb.AppendLine("<td width=""50"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> Admin. No  </td>")
        sb.AppendLine("<td  width=""250"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> Name of the Student <br>  (in English)</td>")
        sb.AppendLine("<td  width=""250"" ALIGN=""middle"" valign=""middle"" rowspan=3 Class=repcolDetail ><center> Name of the Student  <br> (in Arabic)</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" valign=""bottom"" Class=repcolDetail><img src=..\BrownBook\SubjectVerticalText\NATIONALITY_UPPERCASE.jpg></td>")
        hfTableColumns.Value += 5
        sb.AppendLine(strSubjects)


        Select Case hfGRD_ID.Value
            Case "07", "08", "09", "10", "11", "12"
                sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\CONDUCT_UPPERCASE.jpg></td>")
        End Select

        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\TOTAL_UPPERCASE.jpg></td>")
        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\PERCENTAGE_UPPERCASE.jpg></td>")

        Select Case hfGRD_ID.Value
            Case "02", "03", "04", "05", "06", "07"
                sb.AppendLine("<td width=""20"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\FRENCH_UPPERCASE.jpg" + "></td>")
                hfTableColumns.Value += 1
        End Select
        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\RANK_UPPERCASE.jpg></td>")
        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\AbsentWithExcuse.jpg></td>")
        sb.AppendLine("<td width=""2%"" align=""middle"" valign=""bottom"" height=""100px"" ><img src=..\BrownBook\SubjectVerticalText\AbsentWithoutExcuse.jpg></td>")
        sb.AppendLine("<td ALIGN=middle valign=""middle"" rowspan=3 Class=repcolDetail ><center>REMARKS<BR>(PASSED/<BR>DETAINED/<BR>RETEST)</td>")
        hfTableColumns.Value += 2
        sb.AppendLine("</tr>")

        sb.AppendLine("<tr>")

        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine(strMax)

        Select Case hfGRD_ID.Value
            Case "07", "08", "09", "10", "11", "12"
                sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>100</td>")
        End Select

        'total max
        Select Case hfGRD_ID.Value
            Case "01", "02", "03", "04", "05", "06", "07"
                sb.AppendLine("<td  align=""middle"" Class=repcolDetail>900</td>")
            Case "08"
                sb.AppendLine("<td  align=""middle"" Class=repcolDetail>1000</td>")
            Case Else
                sb.AppendLine("<td  align=""middle"" Class=repcolDetail>1100</td>")
        End Select


        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        Select Case hfGRD_ID.Value
            Case "02", "03", "04", "05", "06", "07"
                sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>100</td>")
        End Select
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")


        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")



        sb.AppendLine("</tr>")
        sb.AppendLine("<tr>")


        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine(strMin)

        Select Case hfGRD_ID.Value
            Case "07", "08", "09", "10", "11", "12"
                sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>50</td>")
                hfTableColumns.Value += 1
        End Select

        'total min
        Select Case hfGRD_ID.Value
            Case "01", "02", "03", "04", "05", "06", "07"
                sb.AppendLine("<td  align=""middle"" Class=repcolDetail>450</td>")
            Case "08"
                sb.AppendLine("<td  align=""middle"" Class=repcolDetail>500</td>")
            Case "09"
                sb.AppendLine("<td  align=""middle"" Class=repcolDetail>550</td>")
            Case Else
                sb.AppendLine("<td  align=""middle"" Class=repcolDetail>580</td>")
        End Select

        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        Select Case hfGRD_ID.Value
            Case "02", "03", "04", "05", "06", "07"
                sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>50</td>")
        End Select
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")



        hfTableColumns.Value += 4


        sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function

    Function GetStudentMarks(ByVal sct_id As String, ByVal Subjects(,) As String)

        hfReg_L.Value = 0
        hfReg_NL.Value = 0
        hfPass_NL.Value = 0
        hfPass_L.Value = 0


        Dim strTable As String

        Dim strFooter As String = GetBrownBookFooter()

        Dim strPassed As String = GetStudents(sct_id, Subjects, "PASS", strFooter)
        Dim strRetest As String = GetStudents(sct_id, Subjects, "RETEST", strFooter)
        Dim strDetained As String = GetStudents(sct_id, Subjects, "FAIL", strFooter)



        Dim promotedGrade As String = GetpromotedGrade()

        If promotedGrade <> "" Then
            If promotedGrade <> "10" Then
                promotedGrade = Replace(promotedGrade, "0", "")
            End If
            If promotedGrade = "13" Then
                strPassed = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:9pt Arial"" ><b>PASSED AND PROMOTED</b></font></td>" + strPassed
            Else
                strPassed = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:9pt Arial"" ><b>PASSED AND PROMOTED TO GRADE " + promotedGrade + "</b></font></td>" + strPassed
            End If

        End If

        If strRetest <> "" Then
            strRetest = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:9pt Arial"" ><b>RETEST</b></font></td>" + strRetest
        End If

        If strDetained <> "" Then
            strDetained = "<tr><td align=""center"" colspan=" + hfTableColumns.Value + "><font style=""font:9pt Arial size=""3px"" ><b>STUDENTS DETAINED</b></font></td>" + strDetained
        End If

        strTable = strPassed + strRetest + strDetained

        If hfLastRecord.Value = 0 Then
            '  strTable += GetRedLine()
            'strDetained += "</TABLE>"
            strTable += strFooter
        End If

        strTable = strTable.Replace("%reg_l%", hfReg_L.Value.ToString)
        strTable = strTable.Replace("%reg_nl%", hfReg_NL.Value.ToString)
        strTable = strTable.Replace("%pass_l%", hfPass_L.Value.ToString)
        strTable = strTable.Replace("%pass_nl%", hfPass_NL.Value.ToString)
        strTable = strTable.Replace("%reg_t%", (Val(hfReg_L.Value) + Val(hfReg_NL.Value)).ToString)
        strTable = strTable.Replace("%pass_t%", (Val(hfPass_L.Value) + Val(hfPass_NL.Value)).ToString)



        hfPageNumber.Value += 1
        strTable = strTable.Replace("%pageno%", hfPageNumber.Value)
        strTable = strTable.Replace("%totalpages%", hfPageNumber.Value)



        'UPDATE RETEST COUNT


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = " SELECT SBG_DESCR,COUNT(FRR_ID) FROM STUDENT_M AS A" _
                    & " INNER JOIN RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID AND A.STU_ACD_ID=B.FRR_ACD_ID" _
                    & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID AND A.STU_ACD_ID=C.FTM_ACD_ID" _
                    & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.FTM_SBG_ID=D.SBG_ID" _
                    & " WHERE FRR_RESULT='RETEST' AND FTM_RESULT='FAIL' AND ISNULL(SBG_bRETEST,'FALSE')='TRUE' " _
                    & " AND STU_SCT_ID=" + sct_id + " GROUP BY SBG_DESCR  ORDER BY SBG_DESCR"
        Else
            str_query = " SELECT SBG_DESCR,COUNT(FRR_ID) FROM VW_STUDENT_DETAILS_PREVYEARS AS A" _
                  & " INNER JOIN RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID AND A.STU_ACD_ID=B.FRR_ACD_ID" _
                  & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID AND A.STU_ACD_ID=C.FTM_ACD_ID" _
                  & " INNER JOIN SUBJECTS_GRADE_S AS D ON C.FTM_SBG_ID=D.SBG_ID" _
                  & " WHERE FRR_RESULT='RETEST' AND FTM_RESULT='FAIL' AND ISNULL(SBG_bRETEST,'FALSE')='TRUE' " _
                  & " AND STU_SCT_ID=" + sct_id + " GROUP BY SBG_DESCR  ORDER BY SBG_DESCR"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer
        Dim p As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace("%retestsub" + (i + 1).ToString + "%", "No. Retest in " + .Item(0).ToString)
                strTable = strTable.Replace("%rt" + (i + 1).ToString + "%", .Item(1).ToString)
            End With
        Next


        'clear extra subjets
        For i = ds.Tables(0).Rows.Count - 1 To 7
            strTable = strTable.Replace("%retestsub" + (i + 1).ToString + "%", "&nbsp;")
            strTable = strTable.Replace("%rt" + (i + 1).ToString + "%", "&nbsp;")

        Next


        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim id As String = ""

        Dim strStuId As String = ""
        Dim strRetestSubjects As String = ""

        'POPULATE STUDENT MARKS
        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT     SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
                                    & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
                                    & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
                                    & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
                                    & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT,ISNULL(FRR_RESULT_EDITED,'') AS FRR_RESULT_EDITED,ISNULL(SBG_bRETEST,'FALSE') AS SBG_bRETEST FROM" _
                                    & " STUDENT_M AS A  INNER JOIN STUDENT_GROUPS_S AS P " _
                                    & " ON A.STU_ID=P.SSD_STU_ID AND A.STU_ACD_ID=P.SSD_ACD_ID INNER JOIN  dbo.GETBROWNBOOKSUBJECTS_AKNS(" + hfACD_ID.Value + ",'" + hfGRD_ID.Value + "') B ON P.SSD_SBG_ID=B.SBG_ID" _
                                    & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
                                    & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
                                    & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
                                    & " WHERE STU_SCT_ID=" + sct_id _
                                     & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'" _
                                    & "  ORDER BY STU_ID"
        Else
            str_query = "SELECT   SBG_ID,STU_ID,CASE  WHEN FTM_MARK IS NULL THEN '&nbsp;' " _
                                & " ELSE REPLACE(REPLACE(CAST(CONVERT(NUMERIC(6,2),FTM_MARK) " _
                                & " AS VARCHAR),'.00',''),'.0','') END AS MARKS,ISNULL(FTM_GRADE,'&nbsp;')," _
                                & " ISNULL(FTM_MAXMARK,0) AS FTM_MAXMARK,ISNULL(FTM_MINMARK,0) AS FTM_MINMARK," _
                                & " ISNULL(FRR_RESULT,'') AS FRR_RESULT , SBG_DESCR,ISNULL(FTM_RESULT,'PASS') AS FTM_RESULT,ISNULL(FRR_RESULT_EDITED,'') AS FRR_RESULT_EDITED,ISNULL(SBG_bRETEST,'FALSE') AS SBG_bRETEST FROM" _
                                & " VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN STUDENT_GROUPS_S AS P " _
                                & " ON A.STU_ID=P.SSD_STU_ID AND A.STU_ACD_ID=P.SSD_ACD_ID INNER JOIN  dbo.GETBROWNBOOKSUBJECTS_AKNS(" + hfACD_ID.Value + ",'" + hfGRD_ID.Value + "') B ON P.SSD_SBG_ID=B.SBG_ID" _
                                & " INNER JOIN RPT.FINAL_RESULT_S AS D ON A.STU_ID=D.FRR_STU_ID AND A.STU_ACD_ID=D.FRR_ACD_ID " _
                                & " INNER JOIN RPT.FINAL_TOTALMARKS_S AS C ON A.STU_ID=C.FTM_STU_ID" _
                                & " AND A.STU_ACD_ID=C.FTM_ACD_ID AND C.FTM_SBG_ID=B.SBG_ID" _
                                & " WHERE STU_SCT_ID=" + sct_id _
                                & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'" _
                                & " ORDER BY STU_ID"
        End If



        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)

                If strStuId <> .Item(1).ToString Then
                    If strRetestSubjects <> "" Then
                        strTable = strTable.Replace(strStuId + "_RESULT", "<font color=""#0155F9"">RETEST IN " + strRetestSubjects + "</FONT>")
                    End If
                    strStuId = .Item(1).ToString
                    strRetestSubjects = ""
                End If

                id = .Item(1).ToString + "_" + .Item("SBG_DESCR").ToString
                If .Item(8).ToString.ToUpper = "FAIL" Then
                    If .Item(2).ToString = "0" Then
                        strMark = "<font >&nbsp;</font>"
                        strGrade = "<font >&nbsp;</font>"
                    Else
                        strMark = "<font color=""red"">" + .Item(2).ToString + "</font>"
                        strGrade = "<font color=""red"">" + .Item(3).ToString + "</font>"
                    End If


                    If .Item("SBG_bRETEST").ToString.ToLower = "true" Then
                        If strRetestSubjects <> "" And .Item(6).ToString.ToUpper = "RETEST" Then
                            'strRetestSubjects += ",<BR/>"
                            strRetestSubjects += ", "
                        End If

                        strRetestSubjects += .Item(7).ToString
                    End If
                Else
                    If .Item(2).ToString = "0" Then
                        strMark = "<font >&nbsp;</font>"
                        strGrade = "<font >&nbsp;</font>"
                    Else
                        strMark = .Item(2).ToString
                        strGrade = .Item(3).ToString
                    End If
                End If
                strTable = strTable.Replace(id + "_M", strMark)
                strTable = strTable.Replace(id + "_G", strGrade)

                If .Item(6).ToString.ToUpper = "PASS" Then
                    If .Item(9).ToString.ToUpper <> "" Then
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", .Item(9).ToString.Replace("''", "'"))
                    Else
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "PASSED")
                        ' strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "Promoted")
                    End If
                ElseIf .Item(6).ToString.ToUpper = "FAIL" Then
                    strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "<font color=""#0155F9"">Detained</font>")
                End If

                If i = ds.Tables(0).Rows.Count - 1 Then
                    If strRetestSubjects <> "" Then
                        strTable = strTable.Replace(.Item(1).ToString + "_RESULT", "<font color=""#0155F9"">Retest In" + strRetestSubjects + "</font>")
                    End If
                End If

            End With
        Next

        ''POPULATE CONDUCT,TOTAL,PERCENTAGE AND RANK

        str_query = "EXEC RPT.GETSTUDENTTOTALRANKPERCENTAGE_AKNS " + sct_id
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace(.Item(0).ToString + "_TOTAL", .Item(1).ToString)
                strTable = strTable.Replace(.Item(0).ToString + "_PERCENTAGE", .Item(2).ToString)
                strTable = strTable.Replace(.Item(0).ToString + "_RANK", .Item(3).ToString)
                strTable = strTable.Replace(.Item(0).ToString + "_CONDUCT", .Item(4).ToString)
            End With
        Next

        ''POPULATE ATTNDANCE
        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID,isnull(ATT_TOTALDAYS,0)-isnull(ATT_VALUE,0) AS ATTPERCENTAGE FROM STUDENT_M AS A " _
                    & " LEFT OUTER JOIN RPT.FINAL_ATTENDANCE AS B ON b.ATT_STU_ID=A.STU_ID AND B.ATT_ACD_ID=A.STU_ACD_ID " _
                    & " WHERE STU_SCT_ID=" + sct_id
        Else
            str_query = "SELECT STU_ID,isnull(ATT_TOTALDAYS,0)-isnull(ATT_VALUE,0) AS ATTPERCENTAGE FROM VW_STUDENT_DETAILS_PREVYEARS AS A " _
                 & " LEFT OUTER JOIN RPT.FINAL_ATTENDANCE AS B ON b.ATT_STU_ID=A.STU_ID AND B.ATT_ACD_ID=A.STU_ACD_ID " _
                 & " WHERE STU_SCT_ID=" + sct_id
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strTable = strTable.Replace(.Item(0).ToString + "_ATTENDANCE", IIf(.Item(1).ToString = "0", "&nbsp;", .Item(1).ToString))
            End With
        Next

        strTable = ClearEmptyColumns(sct_id, Subjects, strTable)
        Return strTable
    End Function
    Function ClearEmptyColumns(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal strTable As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID FROM STUDENT_M WHERE STU_SCT_ID=" + sct_id _
                        & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'"
        Else
            str_query = "SELECT STU_ID FROM STUDENT_M INNER JOIN OASIS..STUDENT_PROMO_S ON STU_ID=STP_STU_ID WHERE STP_SCT_ID=" + sct_id _
                  & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'"

        End If

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i, j As Integer
        Dim id As String
        For i = 0 To ds.Tables(0).Rows.Count - 1
            For j = 0 To Subjects.GetLength(0) - 1
                id = ds.Tables(0).Rows(i).Item(0).ToString + "_" + Subjects(j, 0) + "_M"
                strTable = strTable.Replace(id, "&nbsp;")
            Next
        Next

        Return strTable
    End Function


    Function GetpromotedGrade() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TOP 1 ISNULL(GRD_ID,'') FROM GRADE_M WHERE GRD_DISPLAYORDER>" _
                                  & " (SELECT GRD_DISPLAYORDER FROM GRADE_M WHERE GRD_ID='" + hfGRD_ID.Value + "') ORDER BY GRD_DISPLAYORDER"
        Dim grdid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return grdid
    End Function

    Function GetRedLine() As String
        Dim i As Integer
        Dim sb As New StringBuilder
        sb.AppendLine("<tr>")
        For i = 0 To Val(hfTableColumns.Value) - 1
            sb.AppendLine("<td STYLE=""BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 7pt verdana,timesroman,garamond"">&nbsp;</td>")
        Next
        sb.AppendLine("</tr>")
        Return sb.ToString
    End Function

    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal result As String, ByVal strFooter As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strSubjectHeader As String = GetSubjectsHeader(Subjects)
        Dim strPageHeader As String = GetBrownBookHeader(sct_id)
        Dim i As Integer
        Dim j As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()

        Dim strRedLine As String



        Dim str_query As String

        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                    & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                                    & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                                    & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                                    & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                                    & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                                    & " STU_ID,ISNULL(FRR_RESULT,'PASSED') AS RESULT,CTY_NATIONALITY AS NATIONALITY FROM STUDENT_M AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                                    & " INNER JOIN RPT.FINAL_RESULT_S AS C ON A.STU_ID=C.FRR_STU_ID AND A.STU_ACD_ID=C.FRR_ACD_ID" _
                                    & " LEFT OUTER JOIN OASIS..COUNTRY_M AS D ON A.STU_NATIONALITY=D.CTY_ID" _
                                    & " WHERE STU_SCT_ID=" + sct_id + "  AND STU_CURRSTATUS<>'CN' " _
                                    & " AND FRR_RESULT='" + result + "' " _
                                    & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "' AND STU_MINLIST='REGULAR'" _
                                    & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Else
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                        & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                        & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                        & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                        & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                        & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                        & " STU_ID,ISNULL(FRR_RESULT,'PASSED') AS RESULT,CTY_NATIONALITY as NATIONALITY FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                        & " INNER JOIN RPT.FINAL_RESULT_S AS C ON A.STU_ID=C.FRR_STU_ID AND A.STU_ACD_ID=C.FRR_ACD_ID" _
                        & " LEFT OUTER JOIN OASIS..COUNTRY_M AS D ON A.STU_NATIONALITY=D.CTY_ID" _
                        & " WHERE STU_SCT_ID=" + sct_id + "  AND STU_CURRSTATUS<>'CN' " _
                        & " AND FRR_RESULT='" + result + "' " _
                        & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "'  AND STU_MINLIST='REGULAR'" _
                        & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count

        If result.ToUpper = "PASS" Then
            hfPassedCount.Value = ds.Tables(0).Rows.Count
        ElseIf result.ToUpper = "FAIL" Then
            hfDetained.Value = ds.Tables(0).Rows.Count
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If

        Dim pcount As Integer
        '12:     e(33920)
        '11e 33917
        '11f 33918
        If sct_id = "33920" Then
            pcount = 14
        ElseIf sct_id = "33917" Or sct_id = "33918" Then
            pcount = 16
        Else
            pcount = 21
        End If
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                hfRecordNumber.Value += 1
                If hfRecordNumber.Value = pcount Then
                    If i = ds.Tables(0).Rows.Count - 1 And result.ToUpper = "FAIL" Then
                        hfLastRecord.Value = 1
                        sb.AppendLine(GetRedLine)
                    End If
                    hfPageNumber.Value += 1
                    sb.AppendLine(strFooter.Replace("%pageno%", hfPageNumber.Value))

                    'sb.AppendLine("</table>")
                    ' If i <> ds.Tables(0).Rows.Count - 1 Then
                    If hfLastRecord.Value <> 1 Then
                        sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                    End If
                    'End If
                    sb.AppendLine(strPageHeader)
                    sb.AppendLine(strSubjectHeader)
                    hfRecordNumber.Value = 1
                End If

                stuNames = .Item(0).ToString.Trim.Replace("  ", " ").Split(" ")

                If .Item("NATIONALITY").ToString = "UAE" Then
                    hfReg_L.Value += 1
                    If result.ToUpper <> "FAIL" And result.ToUpper <> "RETEST" Then
                        hfPass_L.Value += 1
                    End If
                Else
                    hfReg_NL.Value += 1
                    If result.ToUpper <> "FAIL" And result.ToUpper <> "RETEST" Then
                        hfPass_NL.Value += 1
                    End If
                End If

                If result.ToUpper = "RETEST" Then 'add and empty row above  the same student

                    sb.AppendLine("<tr>")
                    sb.AppendLine("<td HEIGHT=""22px"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_BLUEID") + "</td>")



                    If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                        sb.AppendLine("<td align=""left"" width=""200"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                    Else
                        sb.AppendLine("<td align=""left"" width=""200"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                    End If

                    sb.AppendLine("<td align=""right"" ><font name=""arial"" size=""2px"">" + .Item("STU_ARABICNAME") + "</font></td>")


                    '     sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td align=""left"" Class=repcolDetail>" + .Item("NATIONALITY").ToString + "</td>")


                    For j = 0 To Subjects.GetLength(0) - 1
                        If Subjects(j, 2).ToLower = "true" Then
                            'Select Case hfGRD_ID.Value
                            '    Case "02", "03", "04", "05", "06", "07"
                            '        If Subjects(i, 0).ToUpper <> "FRENCH" Then
                            '            sb.AppendLine("<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Arial"">&nbsp;</font></td>")
                            '        End If

                            '    Case Else
                            sb.AppendLine("<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Arial"">&nbsp;</font></td>")
                            'End Select

                        End If
                    Next


                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")

                    Select Case hfGRD_ID.Value
                        Case "07", "08", "09", "10", "11", "12"
                            sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    End Select

                    sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    sb.AppendLine("<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    'Select Case hfGRD_ID.Value
                    '    Case "02", "03", "04", "05", "06", "07"
                    '        sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")
                    'End Select

                    sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")


                    sb.AppendLine("</tr>")
                End If

                sb.AppendLine("<tr>")
                sb.AppendLine("<td HEIGHT=""22px"" align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_BLUEID") + "</td>")



                If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                    sb.AppendLine("<td align=""left"" width=""200"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                Else
                    sb.AppendLine("<td align=""left"" width=""200"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                End If

                sb.AppendLine("<td align=""right"" ><font name=""arial"" size=""2px"">" + .Item("STU_ARABICNAME") + "</font></td>")

                ' sb.AppendLine("<td align=""middle"" Class=repcolDetail>&nbsp;</td>")

                sb.AppendLine("<td align=""left"" Class=repcolDetail>" + .Item("NATIONALITY").ToString + "</td>")


                For j = 0 To Subjects.GetLength(0) - 1
                    Select Case hfGRD_ID.Value
                        Case "02", "03", "04", "05", "06", "07"
                            If Subjects(j, 0).ToUpper <> "FRENCH" Then
                                sb.AppendLine("<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Arial"">" + .Item(4).ToString + "_" + Subjects(j, 0) + "_M" + "</font></td>")
                            End If

                        Case Else
                            sb.AppendLine("<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Arial"">" + .Item(4).ToString + "_" + Subjects(j, 0) + "_M" + "</font></td>")
                    End Select


                Next

                Select Case hfGRD_ID.Value
                    Case "07", "08", "09", "10", "11", "12"
                        'Conduct
                        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_CONDUCT</font></td>")
                End Select

                ' TotalMarks
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_TOTAL</font></td>")
                'Percentage
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_PERCENTAGE</font></td>")
                Select Case hfGRD_ID.Value
                    Case "02", "03", "04", "05", "06", "07"
                        sb.AppendLine("<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Arial"">" + .Item(4).ToString + "_FRENCH_M" + "</font></td>")
                End Select
                'RankGrade
                sb.AppendLine("<td align=""middle"" >" + .Item("STU_ID").ToString + "_RANK</td>")
                'Attendance
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><font style=""font:8pt Arial"">" + .Item("STU_ID").ToString + "_ATTENDANCE</font></td>")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><font style=""font:8pt Arial"">&nbsp;</font></td>")
                'Result
                sb.AppendLine("<td align=""middle"" ><font style=""font:bold 8pt Arial"" color=""#0155F9"">" + .Item("STU_ID").ToString + "_RESULT </font></td>")

                sb.AppendLine("</tr>")


            End With
        Next





        Return sb.ToString
    End Function


    Public Function checkUpper(ByVal str As String) As Boolean
        If Strings.StrComp(str, Strings.UCase(str)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetBrownBookFooter() As String
        Dim sb As New StringBuilder





        'sb.AppendLine("<TD  rowspan=2 align=middle colspan=5>")

        sb.AppendLine("<tr><td colspan=" + (hfTableColumns.Value - 1).ToString + " align=""right"">&nbsp;</td>")




        sb.AppendLine("<td>")
        sb.AppendLine("<table  border=0 width=100% bordercolorlight=#000000  align=""center"" cellspacing=""0"" cellpadding=""2""  style=""text-decoration:NONE;font:normal normal 7Pt Verdana, arial, Helvetica, sans-serif"" >")
        sb.AppendLine("<tr><td style=""border-bottom: #000000 1pt solid; border-right: #000000 1pt solid ;"">Nationality</td><td  style=""border-bottom: #000000 1pt solid; border-right: #000000 1pt solid ;"">Number registered</td><td  style=""border-bottom: #000000 1pt solid; border-right: #000000 1pt solid ;"">Number Present</td><td  style=""border-bottom: #000000 1pt solid;"">Number Passed</td></tr>")
        sb.AppendLine("<tr><td style=""border-right: #000000 1pt solid ;"">Local</td><td style=""border-right: #000000 1pt solid ;"" align=center>%reg_l%</td><td style=""border-right: #000000 1pt solid ;"" align=center>%reg_l%</td><td align=center>%pass_l%</td></tr>")
        sb.AppendLine("<tr><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"">Non Local</td><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"" align=center>%reg_nl%</td><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"" align=center>%reg_nl%</td><td style=""border-top: #000000 1pt solid;"" align=center>%pass_nl%</td></tr>")
        sb.AppendLine("<tr><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"">Total</td><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"" align=center>%reg_t%</td><td style=""border-top: #000000 1pt solid; border-right: #000000 1pt solid ;"" align=center>%reg_t%</td><td style=""border-top: #000000 1pt solid;"" align=center>%pass_t%</td></tr>")
        sb.AppendLine("</table>")
        sb.AppendLine("</td></tr>")


        sb.AppendLine("<tr><td colspan=" + (hfTableColumns.Value).ToString + ">")
        sb.AppendLine("<table  border=0 width=""100%""  align=""center"" cellspacing=""0"" cellpadding=""2""  style=""text-decoration:NONE;font:normal normal 7Pt Verdana, arial, Helvetica, sans-serif"" >")
        sb.AppendLine("<TR HEIGHT=35> <TD align=left width=150>Registrar :</TD> <TD align=left>" + GetEmpName("REGISTRAR", "") + "</TD> ")
        sb.AppendLine("<TD width=300 rowspan=2 align=middle>&nbsp;</TD> ")

        If hfChecker.Value = "" Then
            sb.AppendLine("<TD align=left width=150 >Checked By :</TD><TD width=150>______________________________</TD> ")
        Else
            sb.AppendLine("<TD align=left >Re-checker :</TD> <TD width=150>" + hfChecker.Value + "</TD> ")
        End If
        ' sb.AppendLine("<TD width=300 rowspan=2 align=middle><img src=""..\..\images\brownbookseal.jpg""</TD> ")





        sb.AppendLine("<TD width=300 rowspan=2 align=middle><img src=""..\..\images\SchoolStamp.jpg""/></TD> ")
        sb.AppendLine("<TD  align=left>   Principal:&nbsp;&nbsp;</TD> <TD width=200>&nbsp;&nbsp;" + GetEmpName("PRINCIPAL/CEO", "full") + "</TD> </TR>")
        sb.AppendLine("<TR HEIGHT=35> <TD align=left>Signature :</TD> <TD>_______________________________</TD> ")
        sb.AppendLine("<TD align=left>Signature  :&nbsp;&nbsp;</TD> <TD>&nbsp;&nbsp;__________________________</TD> ")
        sb.AppendLine("<TD align=left>Signature  :&nbsp;&nbsp;</TD><TD>&nbsp;&nbsp;__________________________</TD> </TR>")

        sb.AppendLine("<TR Height=35> <TD Colspan=" + hfTableColumns.Value + " align=right>page&nbsp;%pageno% of %totalpages% &nbsp;&nbsp;&nbsp;&nbsp;</TD></TR>")
        sb.AppendLine("</Table>")
        sb.AppendLine("<td></tr>")

        sb.AppendLine("</table>")
        Return sb.ToString
    End Function

    Function GetEmpName(ByVal designation As String, ByVal sType As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If sType = "Short" Then
            str_query = "SELECT Left(ISNULL(EMP_FNAME,''),1)+'. '+Left(ISNULL(EMP_MNAME,''),1)+'. '+ISNULL(EMP_LNAME,'') FROM " _
                                     & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE  emp_status=1 and EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                     & " AND DES_DESCR ='" + designation + "'"
        Else
            str_query = "SELECT isnull(EMP_SALUTE,'')+' '+ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
                                     & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE  emp_status=1 and EMP_BSU_ID='" + Session("SBSUID") + "'" _
                                     & " AND DES_DESCR = '" + designation + "'"
        End If

        Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return emp
    End Function
#End Region


End Class
