﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Telerik.Web.UI
Partial Class Curriculum_clmGroupOptionAllocationByStudent
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100490") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'Session("Current_Acd_id") = "1273"
                    hfACD_ID.Value = Session("Current_Acd_id")
                    ddlGrade = studClass.PopulateGrade(ddlGrade, hfACD_ID.Value)
                    PopulateSection()
                    BindStudents()
                End If
                  Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSearch)
     
    End Sub


#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function getPhoto(ByVal stu_photopath As String) As String
        Dim strImagePath As String
        If stu_photopath <> "" Then
            strImagePath = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + stu_photopath
        Else
            strImagePath = "~/Images/Photos/no_image.gif"
        End If
        Return strImagePath
        'imgStud.AlternateText = "No Image found"
    End Function

    Sub SetMinOptionSetting()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String = "SELECT GRO_OPTIONS FROM OASIS_CURRICULUM..GRADE_MINOPTIONSETTING WHERE  " _
                                 & " GRO_ACD_ID=" + hfACD_ID.Value _
                                 & " AND GRO_GRD_ID='" + hfGRD_ID.Value + "'"
        hfMinOption.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
    End Sub

    Private Sub PopulateSection()
        ddlSection.Items.Clear()
       
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + hfACD_ID.Value + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + hfACD_ID.Value _
                                 & " AND SCT_DESCR<>'TEMP' ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
     
    End Sub

    Private Sub BindStudents()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String


        str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                    & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View',ISNULL(STU_PHOTOPATH,'') STU_PHOTOPATH " _
                    & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                    & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                    & " WHERE STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + hfACD_ID.Value _
                    & " AND SCT_DESCR<>'TEMP'"

        If txtStuNo.Text = "" And txtName.Text = "" Then
            str_query += " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) "
        End If

        If hfSCT_ID.Value <> "0" Then
            str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID= '" + hfGRD_ID.Value + "'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If
        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
      
       
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlStudents.DataSource = ds
        dlStudents.DataBind()
    End Sub


    Sub BindManadatorySubjects(ByVal gvMandatoryGroup As GridView)
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE WHEN SBG_PARENTS='NA' THEN SBG_DESCR ELSE SBG_DESCR+'('+SBG_PARENTS+')' END SBG_DESCR FROM SUBJECTS_GRADE_S " _
                               & " INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID" _
                               & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_GRD_ID='" + hfGRD_ID.Value + "' AND ISNULL(SBG_bOPTIONAL,0)=0"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvMandatoryGroup.DataSource = ds
        gvMandatoryGroup.DataBind()
      
    End Sub


    Sub BindOptions(ByVal gvOptionalGroup As GridView)
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT OPT_ID,OPT_DESCR FROM " _
                                & " OPTIONS_M INNER JOIN SUBJECTGRADE_OPTIONS_S ON OPT_ID=SGO_OPT_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S ON SBG_ID=SGO_SBG_ID" _
                                & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_GRD_ID='" + hfGRD_ID.Value + "' AND ISNULL(SBG_bOPTIONAL,0)=1"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvOptionalGroup.DataSource = ds
        gvOptionalGroup.DataBind()
    End Sub


    Sub BindOptionsandGroups()

        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE WHEN SBG_PARENTS='NA' THEN SBG_DESCR ELSE SBG_DESCR+'('+SBG_PARENTS+')' END  SBG_DESCR FROM SUBJECTS_GRADE_S " _
                               & " INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID" _
                               & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_GRD_ID='" + hfGRD_ID.Value + "' AND ISNULL(SBG_bOPTIONAL,0)=0"
        Dim dsManadatory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        str_conn = ConnectionManger.GetOASISConnectionString
        str_query = "SELECT DISTINCT OPT_ID,OPT_DESCR FROM " _
                                & " OPTIONS_M INNER JOIN SUBJECTGRADE_OPTIONS_S ON OPT_ID=SGO_OPT_ID" _
                                & " INNER JOIN SUBJECTS_GRADE_S ON SBG_ID=SGO_SBG_ID" _
                                & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_GRD_ID='" + hfGRD_ID.Value + "' AND ISNULL(SBG_bOPTIONAL,0)=1"

        Dim dsOption As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        str_query = "SELECT  SGR_ID,SGR_DESCR,SGR_SBG_ID FROM GROUPS_M " _
                    & " WHERE SGR_ACD_ID=" + hfACD_ID.Value + " AND SGR_GRD_ID='" + hfGRD_ID.Value + "'"


        Dim dsGroups As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        str_query = "SELECT DISTINCT SBG_ID,CASE WHEN SBG_PARENTS='NA' THEN SBG_DESCR ELSE SBG_DESCR+'('+SBG_PARENTS+')' END  SBG_DESCR,SGO_OPT_ID FROM SUBJECTS_GRADE_S " _
                & " INNER JOIN SUBJECTGRADE_OPTIONS_S ON SBG_ID=SGO_SBG_ID" _
                & " WHERE SBG_ACD_ID=" + hfACD_ID.Value + " AND SBG_GRD_ID='" + hfGRD_ID.Value + "' AND ISNULL(SBG_bOPTIONAL,0)=1"

        Dim dsSubject As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim pgView1 As RadPageView
        Dim pgView2 As RadPageView
        Dim gvMandatoryGroup As GridView
        Dim gvOptionalGroup As GridView
        Dim lblSbgId As Label
        Dim lblOptId As Label

        Dim lblCSgrId As Label

        Dim ddlCGroup As DropDownList
        Dim ddlOptSubject As DropDownList
        Dim ddlStuGroup As DropDownList
        Dim ddlStuOption As DropDownList
        Dim ddlOptGroup As DropDownList

        Dim lblStuId As Label


        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        For i = 0 To dlStudents.Items.Count - 1
            With dlStudents.Items(i)
                pgView1 = .FindControl("pgView1")
                pgView2 = .FindControl("pgView2")
                lblStuId = .FindControl("lblStuId")
                ddlStuGroup = .FindControl("ddlStuGroup")
                ddlStuOption = .FindControl("ddlStuOption")

                gvMandatoryGroup = pgView1.FindControl("gvMandatoryGroup")
                gvOptionalGroup = pgView2.FindControl("gvOptionalGroup")

                gvMandatoryGroup.DataSource = dsManadatory
                gvMandatoryGroup.DataBind()

                gvOptionalGroup.DataSource = dsOption
                gvOptionalGroup.DataBind()

                For j = 0 To gvMandatoryGroup.Rows.Count - 1
                    lblSbgId = gvMandatoryGroup.Rows(j).FindControl("lblSbgId")
                    ddlCGroup = gvMandatoryGroup.Rows(j).FindControl("ddlCGroup")
                    lblCSgrId = gvMandatoryGroup.Rows(j).FindControl("lblCSgrId")
                    BindGroup(ddlCGroup, dsGroups, lblSbgId.Text, lblStuId.Text, ddlStuGroup, lblCSgrId)
                Next

                For k = 0 To gvOptionalGroup.Rows.Count - 1
                    lblOptId = gvOptionalGroup.Rows(k).FindControl("lblOptId")
                    ddlOptSubject = gvOptionalGroup.Rows(k).FindControl("ddlOptSubject")
                    ddlOptGroup = gvOptionalGroup.Rows(k).FindControl("ddlOptGroup")
                    BindOptionalSubjects(ddlOptSubject, dsSubject, lblOptId.Text, ddlStuOption, dsGroups, ddlOptGroup, ddlStuGroup)
                Next
            End With
        Next
    End Sub

    Sub BindGroup(ByVal ddlGroup As DropDownList, ByVal dsGroups As DataSet, ByVal sbg_id As String, ByVal stu_id As String, ByVal ddlStuGroup As DropDownList, ByVal lblCSgrId As Label)
        Dim dv As DataView = dsGroups.Tables(0).DefaultView
        dv.RowFilter = "SGR_SBG_ID=" + sbg_id
        ddlGroup.DataSource = dv.Table
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlGroup.Items.Insert(0, li)

        Dim i As Integer
        For i = 0 To ddlStuGroup.Items.Count - 1
            If Not ddlGroup.Items.FindByValue(ddlStuGroup.Items(i).Value) Is Nothing Then
                ddlGroup.ClearSelection()
                ddlGroup.Items.FindByValue(ddlStuGroup.Items(i).Value).Selected = True
                lblCSgrId.Text = ddlStuGroup.Items(i).Value
            End If
        Next

    End Sub

  

    Sub BindOptionalSubjects(ByVal ddlSubject As DropDownList, ByVal dsSubject As DataSet, ByVal opt_id As String, ByVal ddlStuOption As DropDownList, ByVal dsGroups As DataSet, ByVal ddlOptGroup As DropDownList, ByVal ddlStuGroup As DropDownList)
        Dim dv As DataView = dsSubject.Tables(0).DefaultView
        dv.RowFilter = "SGO_OPT_ID=" + opt_id
        ddlSubject.DataSource = dv.Table
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

        Dim i As Integer
        For i = 0 To ddlStuOption.Items.Count - 1
            If opt_id = ddlStuOption.Items(i).Text Then
                If Not ddlSubject.Items.FindByValue(ddlStuOption.Items(i).Value) Is Nothing Then
                    ddlSubject.ClearSelection()
                    ddlSubject.Items.FindByValue(ddlStuOption.Items(i).Value).Selected = True
                    BindOptionGroup(ddlOptGroup, dsGroups, ddlSubject.SelectedValue, ddlStuGroup)
                End If
            End If
        Next
    End Sub

    Sub BindOptionGroup(ByVal ddlOptGroup As DropDownList, ByVal dsGroups As DataSet, ByVal sbg_id As String, ByVal ddlStuGroup As DropDownList)
        Dim dv As DataView = dsGroups.Tables(0).DefaultView
        dv.RowFilter = "SGR_SBG_ID=" + sbg_id
        ddlOptGroup.DataSource = dv.Table
        ddlOptGroup.DataTextField = "SGR_DESCR"
        ddlOptGroup.DataValueField = "SGR_ID"
        ddlOptGroup.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlOptGroup.Items.Insert(0, li)

        Dim i As Integer
        For i = 0 To ddlStuGroup.Items.Count - 1
            If Not ddlOptGroup.Items.FindByValue(ddlStuGroup.Items(i).Value) Is Nothing Then
                ddlOptGroup.ClearSelection()
                ddlOptGroup.Items.FindByValue(ddlStuGroup.Items(i).Value).Selected = True
            End If
        Next

    End Sub
    Sub BindStudentGroups(ByVal ddlStuGroup As DropDownList, ByVal lblStuId As Label)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT  SSD_SGR_ID FROM STUDENT_GROUPS_S " _
                   & " WHERE  SSD_ACD_ID=" + hfACD_ID.Value + " AND SSD_GRD_ID='" + hfGRD_ID.Value + "'" _
                   & " AND SSD_STU_ID=" + lblStuId.Text


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlStuGroup.DataSource = ds
        ddlStuGroup.DataTextField = "SSD_SGR_ID"
        ddlStuGroup.DataValueField = "SSD_SGR_ID"
        ddlStuGroup.DataBind()

    End Sub

    Sub BindStudentOptions(ByVal ddlStuOption As DropDownList, ByVal lblStuId As Label)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT  SSD_SBG_ID,SSD_OPT_ID FROM STUDENT_GROUPS_S " _
                   & " WHERE  SSD_ACD_ID=" + hfACD_ID.Value + " AND SSD_GRD_ID='" + hfGRD_ID.Value + "'" _
                   & " AND SSD_STU_ID=" + lblStuId.Text + " AND SSD_OPT_ID IS NOT NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlStuOption.DataSource = ds
        ddlStuOption.DataTextField = "SSD_OPT_ID"
        ddlStuOption.DataValueField = "SSD_SBG_ID"
        ddlStuOption.DataBind()
    End Sub

    Sub BindGroups(ByVal sbg_id As String, ByVal ddlOptGroup As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M WHERE SGR_SBG_ID=" + sbg_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlOptGroup.DataSource = ds
        ddlOptGroup.DataTextField = "SGR_DESCR"
        ddlOptGroup.DataValueField = "SGR_ID"
        ddlOptGroup.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlOptGroup.Items.Insert(0, li)
    End Sub

    Sub SaveManadatoryGroups(ByVal gvMandatoryGroup As GridView, ByVal stu_id As String, ByVal lblError As Label)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim i As Integer
        Dim lblSbgId As Label
        Dim lblCSgrId As Label
        Dim ddlCGroup As DropDownList
        Try
            For i = 0 To gvMandatoryGroup.Rows.Count - 1
                lblSbgId = gvMandatoryGroup.Rows(i).FindControl("lblSbgId")
                lblCSgrId = gvMandatoryGroup.Rows(i).FindControl("lblCSgrId")
                ddlCGroup = gvMandatoryGroup.Rows(i).FindControl("ddlCGroup")

                If lblCSgrId.Text <> ddlCGroup.SelectedValue.ToString And ddlCGroup.SelectedValue.ToString <> "0" Then
                    str_query = "exec  [dbo].[saveSTUDMANDATORYGROUP] " _
                                       & " @SSD_ACD_ID=" + hfACD_ID.Value + "," _
                                       & " @SSD_GRD_ID='" + hfGRD_ID.Value + "'," _
                                       & " @SSD_SBG_ID=" + lblSbgId.Text + "," _
                                       & " @SSD_SGR_ID=" + ddlCGroup.SelectedValue.ToString + "," _
                                       & " @SSD_SCT_ID=" + hfSCT_ID.Value + "," _
                                       & " @SSD_STU_ID=" + stu_id + ""

                    SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                End If
            Next
            lblError.Text = "Record saved successfully"
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Sub SaveOptionalGroups(ByVal gvOptionalGroup As GridView, ByVal stu_id As String, ByVal lblError As Label)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim i As Integer
        Dim lblSbgId As Label
        Dim lblSgrId As Label
        Dim ddlOptGroup As DropDownList
        Dim ddlOptSubject As DropDownList
        Try
            For i = 0 To gvOptionalGroup.Rows.Count - 1
                lblSbgId = gvOptionalGroup.Rows(i).FindControl("lblSbgId")
                lblSgrId = gvOptionalGroup.Rows(i).FindControl("lblSgrId")
                ddlOptGroup = gvOptionalGroup.Rows(i).FindControl("ddlOptGroup")
                ddlOptSubject = gvOptionalGroup.Rows(i).FindControl("ddlOptSubject")

                If lblSgrId.Text <> ddlOptGroup.SelectedValue.ToString And ddlOptGroup.SelectedValue.ToString <> "0" Then
                    str_query = "exec  [dbo].[saveSTUDENTOPTIONALGROUP] " _
                                       & " @SSD_ACD_ID=" + hfACD_ID.Value + "," _
                                       & " @SSD_GRD_ID='" + hfGRD_ID.Value + "'," _
                                       & " @SSD_SBG_ID=" + ddlOptSubject.SelectedValue.ToString + "," _
                                       & " @SSD_SGR_ID=" + ddlOptGroup.SelectedValue.ToString + "," _
                                       & " @SSD_SCT_ID=" + hfSCT_ID.Value + "," _
                                       & " @SSD_STU_ID=" + stu_id + ""

                    SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                End If
            Next
            lblError.Text = "Record saved successfully"
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Function ValidateOptionList(ByVal gvOptionalGroup As GridView, ByVal lblError As Label) As Boolean
        Dim ar As New ArrayList
        Dim i As Integer
        Dim ddlOptSubject As DropDownList
        Dim bUnique As Boolean = True
        Dim strError As String = ""
        Dim bBlank As Boolean = False
        For i = 0 To gvOptionalGroup.Rows.Count - 1
            ddlOptSubject = gvOptionalGroup.Rows(i).FindControl("ddlOptSubject")
            If ddlOptSubject.SelectedValue <> 0 Then
                If ar.Contains(ddlOptSubject.SelectedValue.ToString) Then
                    bUnique = False
                End If
                ar.Add(ddlOptSubject.SelectedValue.ToString)
            Else
                bBlank = True
            End If

        Next


        If bUnique = False Then
            strError = "Subjects have to be unique."
        End If
        If hfMinOption.Value = "0" Then
            If bBlank = True Then
                If strError <> "" Then
                    strError += vbCrLf
                End If
                strError = "Please select all options."
            End If
        ElseIf Val(hfMinOption.Value) > ar.Count Then
            If strError <> "" Then
                strError += vbCrLf
            End If
            strError = "Please select a minimum of " + hfMinOption.Value.ToString + " options."
        End If
        If strError <> "" Then
            lblError.Text = strError
            Return False
        End If
        Return True
    End Function



    Sub SaveOptions(ByVal gvOptionalGroup As GridView, ByVal stu_id As String, ByVal lblError As Label)
        Dim i As Integer
        Dim strOptions As String = ""
        Dim strSubjects As String = ""
        Dim ddlOptSubject As DropDownList
        Dim lblOptId As Label
        For i = 0 To gvOptionalGroup.Rows.Count - 1
            lblOptId = gvOptionalGroup.Rows(i).FindControl("lblOptId")
            ddlOptSubject = gvOptionalGroup.Rows(i).FindControl("ddlOptSubject")
            If ddlOptSubject.SelectedValue.ToString <> "0" Then
                strOptions += "<ID><OPT_ID>" + lblOptId.Text + "</OPT_ID><SBG_ID>" + ddlOptSubject.SelectedValue.ToString + "</SBG_ID><SBM_ID>0</SBM_ID></ID>"
                If strSubjects <> "" Then
                    strSubjects += ","
                End If
                strSubjects = ddlOptSubject.SelectedItem.Text
            End If
        Next

        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Try
            str_query = "saveSTUDENTOPTIONS " _
                      & " @ACD_ID=" + hfACD_ID.Value + "," _
                      & " @GRD_ID='" + hfGRD_ID.Value + "'," _
                      & " @STM_ID=1," _
                      & " @STU_ID=" + stu_id + "," _
                      & " @SUBJECT_IDS='<IDS>" + strOptions + "</IDS>'," _
                      & " @SUBJECTS='" + strSubjects + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            lblError.Text = "Record Saved Successfully"
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#End Region

    Protected Sub btnSaveManadtoy_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblStuId As Label = TryCast(sender.findcontrol("lblStuId"), Label)
        Dim gvMandatoryGroup As GridView = TryCast(sender.findcontrol("gvMandatoryGroup"), GridView)
        Dim lblError As Label = TryCast(sender.findcontrol("lblError"), Label)
        SaveManadatoryGroups(gvMandatoryGroup, lblStuId.Text, lblError)
    End Sub

    Protected Sub btnSaveOptional_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblStuId As Label = TryCast(sender.findcontrol("lblStuId"), Label)
        Dim gvOptionalGroup As GridView = TryCast(sender.findcontrol("gvOptionalGroup"), GridView)
        Dim lblError As Label = TryCast(sender.findcontrol("lblError"), Label)
        If ValidateOptionList(gvOptionalGroup, lblError) = True Then
            SaveOptions(gvOptionalGroup, lblStuId.Text, lblError)
            SaveOptionalGroups(gvOptionalGroup, lblStuId.Text, lblError)
        End If
    End Sub

    Protected Sub ddlOptSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlOptSubject As DropDownList = DirectCast(sender.findcontrol("ddlOptSubject"), DropDownList)
        Dim ddlOptGroup As DropDownList = TryCast(sender.findcontrol("ddlOptGroup"), DropDownList)
        BindGroups(ddlOptSubject.SelectedValue.ToString, ddlOptGroup)
    End Sub

    Protected Sub dlStudents_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlStudents.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim ddlStuGroup As DropDownList = e.Item.FindControl("ddlStuGroup")
            Dim ddlStuOption As DropDownList = e.Item.FindControl("ddlStuOption")
            Dim lblStuId As Label = e.Item.FindControl("lblStuId")
            BindStudentGroups(ddlStuGroup, lblStuId)

            BindStudentOptions(ddlStuOption, lblStuId)
            '    Dim rtsOption As RadTabStrip = e.Item.FindControl("rtsOption")
            'Dim rdPage As RadMultiPage = e.Item.FindControl("rdPage")
            'Dim pgView1 As RadPageView = e.Item.FindControl("pgView1")
            'Dim pgView2 As RadPageView = e.Item.FindControl("pgView2")

            'Dim gvMandatoryGroup As GridView = pgView1.FindControl("gvMandatoryGroup")
            'Dim gvOptionalGroup As GridView = pgView2.FindControl("gvOptionalGroup")
            'BindManadatorySubjects(gvMandatoryGroup)
            'BindOptions(gvOptionalGroup)
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        hfSCT_ID.Value = ddlSection.SelectedValue.ToString
        hfSTUNO.Value = txtStuNo.Text
        BindStudents()
        BindOptionsandGroups()
        SetMinOptionSetting()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
    End Sub
End Class
