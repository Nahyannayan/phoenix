﻿
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Imports ActivityFunctions
Partial Class Curriculum_clmAERO_Format
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100419") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    lblReportCaption.Text = Mainclass.GetMenuCaption(ViewState("MainMnu_code"))
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    BindAcyear()
                    BindTerm()
                    BindGrade()
                    BindSubject()

                    trhide.Visible = False

                    gridbind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindAcyear()
        ddlAcyear.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcyear.DataTextField = "ACY_DESCR"
        ddlAcyear.DataValueField = "ACD_ID"
        ddlAcyear.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcyear.Items(ddlAcyear.Items.IndexOf(li)).Selected = True
    End Sub
    Sub BindTerm()
        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAcyear.SelectedItem.Value)
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        ''Dim str_query As String = "SELECT TRM_DESCRIPTION,TRM_ID FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        'Dim str_query As String = " SELECT TSM_ID,TSM_DESCRIPTION FROM [dbo].[term_sub_master] " _
        '                        & " INNER JOIN [dbo].[term_master]  ON TSM_TRM_ID = TRM_ID WHERE TRM_BSU_ID ='" + Session("SBSUID") + "'" _
        '                        & " AND TRM_ACD_ID =" + ddlAcyear.SelectedItem.Value _
        '                        & " ORDER BY TSM_DISPLAY_ORDER,TSM_ID"
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                      & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                                      & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                                      & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                      & "  grm_acd_id=" + ddlAcyear.SelectedValue
        str_query += " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
    End Sub
    Sub BindSubject()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + ddlAcyear.SelectedValue


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        'Dim li As New ListItem
        'li.Text = "ALL"
        'li.Value = "0"
        'ddlSubject.Items.Insert(0, li)
    End Sub

    Sub BindAgeBand()
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("[OASIS].dbo.[GET_GRADE_AGEBAND]", sqlCon)
        cmd.CommandType = CommandType.StoredProcedure
        sqlCon.Open()
        Using dr As SqlDataReader = cmd.ExecuteReader()
            If dr.HasRows Then
                ddlAgeband.DataSource = dr
                ddlAgeband.DataTextField = "GRD_AGEB_CATEGORY"
                ddlAgeband.DataValueField = "GRD_AGEB_ID"
                ddlAgeband.DataBind()
                Dim li As New ListItem
                li.Text = "Select"
                li.Value = "0"
                ddlAgeband.Items.Insert(0, li)
            End If
        End Using
        sqlCon.Close()
    End Sub
    Protected Function CheckAgeBand() As Boolean

        Try
            Dim bool As Boolean = False
            ' code portion to find the ageband flag 
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddlAcyear.SelectedValue, SqlDbType.BigInt)
            param(2) = Mainclass.CreateSqlParameter("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0), SqlDbType.VarChar)
            bool = SqlHelper.ExecuteScalar(str_conn, "GET_GRADE_ISAGEBAND", param)
            ' end of code portion 
            Return bool

        Catch ex As Exception
            Return False
        End Try
    End Function
    Protected Sub ddlAcyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcyear.SelectedIndexChanged
        BindTerm()
        BindGrade()
        BindSubject()
        gridbind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        Dim isAgeBand As Boolean = CheckAgeBand()
        If isAgeBand Then
            BindAgeBand()
            ' ddlAgeband.Enabled = True
            trhide.Visible = True
        Else
            trhide.Visible = False
        End If
        gridbind()
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""
            Dim grade As String()
            Dim ds As New DataSet
        
            grade = ddlGrade.SelectedValue.Split("|")



            str_Sql = "select SYC_ID,SYC_DESCR,SYC_STARTDT,SYC_ENDDT,ISNULL((SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID),'')+'-' + A.SYD_DESCR AS  SYC_TOPIC  from SYL.LESSON_CRITERIA_M " _
                      & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID INNER JOIN SYL.SYLLABUS_M ON SYM_ID  = SYD_SYM_ID  where SYC_ACD_ID=" + ddlAcyear.SelectedValue + " and SYC_GRD_ID='" + grade(0) + "' and " _
                      & "SYC_SBG_ID=" + ddlSubject.SelectedValue + " and SYC_ISAGEBAND=" + IIf(trhide.Visible, ddlAgeband.SelectedValue.ToString, "0").ToString()

            If txtTopic.Text <> "" Then
                str_Sql += " and (SYD_ID=" + h_TopicID.Value + " OR SYD_PARENT_ID=" + h_TopicID.Value + ")"
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvDets.DataSource = ds.Tables(0)
                gvDets.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvDets.DataSource = ds.Tables(0)
                Try
                    gvDets.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvDets.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvDets.Rows(0).Cells.Clear()
                gvDets.Rows(0).Cells.Add(New TableCell)
                gvDets.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDets.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDets.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim cmd As New SqlCommand
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            cmd = New SqlCommand("[SYL].[SaveLESSON_CRITERIA_M]", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            grade = ddlGrade.SelectedValue.Split("|")
            cmd.Parameters.AddWithValue("@SYC_ID", IIf(h_SYC_ID.Value = "", 0, h_SYC_ID.Value))

            cmd.Parameters.AddWithValue("@SYC_ACD_ID", ddlAcyear.SelectedValue)
            cmd.Parameters.AddWithValue("@SYC_GRD_ID", grade(0))
            cmd.Parameters.AddWithValue("@SYC_SBG_ID", ddlSubject.SelectedValue)
            cmd.Parameters.AddWithValue("@SYC_SYD_ID", h_TopicID.Value)

            cmd.Parameters.AddWithValue("@SYC_DESCR", txtCrDesc.Text)
            cmd.Parameters.AddWithValue("@SYC_STARTDT", txtFromDate.Text)
            cmd.Parameters.AddWithValue("@SYC_ENDDT", txtToDate.Text)
            cmd.Parameters.AddWithValue("@SYC_LESSONS", Val(txtNoLessons.Text))
            cmd.Parameters.AddWithValue("@SYC_bRUBRIC", chkRubric.Checked)
            cmd.Parameters.AddWithValue("@SYC_bEVALUATION", chkEval.Checked)
            cmd.Parameters.AddWithValue("@SYC_bCORE", chkCore.Checked)
            cmd.Parameters.AddWithValue("@SYC_CORE_ACTIVITIES", txtCore.Text)
            cmd.Parameters.AddWithValue("@SYC_bBASIC", chkBasic.Checked)
            cmd.Parameters.AddWithValue("@SYC_BASIC_ACTIVITYIES", txtBasic.Text)
            cmd.Parameters.AddWithValue("@SYC_bEXTENDED", chkExtended.Checked)
            cmd.Parameters.AddWithValue("@SYC_bEXTENDED_ACIVITIES", txtExtended.Text)
            cmd.Parameters.AddWithValue("@SYC_LINKS", txtLinks.Text)
            cmd.Parameters.AddWithValue("@SYC_RESOURCES", txtResource.Text)
            cmd.Parameters.AddWithValue("@SYC_ISAGEBAND", ddlAgeband.SelectedValue)
          
            cmd.ExecuteNonQuery()

            stTrans.Commit()

            If btnSave.Text = "Save" Then
                lblError.Text = "Record Saved Successfully"
            Else
                lblError.Text = "Record Updated Successfully"
            End If
            objConn.Close()
            clearcontrols()
            gridbind()
            btnSave.Text = "Save"
            ''Next
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = "Unexpected error"
            objConn.Close()
            Exit Sub
        End Try
       


    End Sub
   

    Protected Sub gvDets_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDets.PageIndexChanging
        gvDets.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub gvDets_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDets.RowDeleting
        Try
            gvDets.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = gvDets.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblID"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM SYL.LESSON_CRITERIA_M WHERE SYC_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub txtTopic_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTopic.TextChanged
        gridbind()
    End Sub
    Sub clearcontrols()
        txtCrDesc.Text = ""
        txtNoLessons.Text = ""
        chkRubric.Checked = False
        chkEval.Checked = False
        chkBasic.Checked = False
        chkCore.Checked = False
        chkExtended.Checked = False
        txtFromDate.Text = ""
        txtToDate.Text = ""
        txtBasic.Text = ""
        txtCore.Text = ""
        txtExtended.Text = ""

        txtBasic.Enabled = False
        txtCore.Enabled = False
        txtExtended.Enabled = False
        txtLinks.Text = ""
        txtResource.Text = ""
    End Sub

    Protected Sub gvDets_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDets.RowEditing
        gvDets.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = gvDets.Rows(e.NewEditIndex)

        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblID"), Label)

        If lblID.Text <> "" Then
            DisplayItems(lblID.Text)
            h_SYC_ID.Value = lblID.Text
        End If
        btnSave.Text = "Update"

    End Sub

    Sub DisplayItems(ByVal id As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""

            str_query = "select SYC_DESCR,SYC_STARTDT,SYC_ENDDT,SYC_LESSONS,SYC_bRUBRIC,SYC_bEVALUATION,SYC_bCORE,SYC_CORE_ACTIVITIES,SYC_bBASIC, " _
                        & " SYC_BASIC_ACTIVITYIES,SYC_bEXTENDED,SYC_bEXTENDED_ACIVITIES,ISNULL(SYC_LINKS,'') SYC_LINKS,ISNULL(SYC_RESOURCES,'') SYC_RESOURCES from SYL.LESSON_CRITERIA_M	WHERE SYC_ID=  " & id

            If str_query <> "" Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count >= 1 Then
                    txtCrDesc.Text = ds.Tables(0).Rows(0).Item("SYC_DESCR")

                    txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", ds.Tables(0).Rows(0).Item("SYC_STARTDT"))
                    txtToDate.Text = String.Format("{0:dd/MMM/yyyy}", ds.Tables(0).Rows(0).Item("SYC_ENDDT"))
                    
                    txtNoLessons.Text = ds.Tables(0).Rows(0).Item("SYC_LESSONS")
                    chkBasic.Checked = ds.Tables(0).Rows(0).Item("SYC_bBASIC")
                    If chkBasic.Checked Then txtBasic.Enabled = True
                    chkCore.Checked = ds.Tables(0).Rows(0).Item("SYC_bCORE")
                    If chkCore.Checked Then txtCore.Enabled = True
                    chkEval.Checked = ds.Tables(0).Rows(0).Item("SYC_bEVALUATION")
                    chkExtended.Checked = ds.Tables(0).Rows(0).Item("SYC_bEXTENDED")
                    If chkExtended.Checked Then txtExtended.Enabled = True
                    chkRubric.Checked = ds.Tables(0).Rows(0).Item("SYC_bRUBRIC")
                    txtBasic.Text = ds.Tables(0).Rows(0).Item("SYC_BASIC_ACTIVITYIES").ToString()
                    txtCore.Text = ds.Tables(0).Rows(0).Item("SYC_CORE_ACTIVITIES").ToString()
                    txtExtended.Text = ds.Tables(0).Rows(0).Item("SYC_bEXTENDED_ACIVITIES").ToString()
                    txtLinks.Text = ds.Tables(0).Rows(0).Item("SYC_LINKS").ToString()
                    txtResource.Text = ds.Tables(0).Rows(0).Item("SYC_RESOURCES").ToString()

                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub ddlAgeband_SelectedIndexChanged(sender As Object, e As EventArgs)
        gridbind()
    End Sub
End Class
