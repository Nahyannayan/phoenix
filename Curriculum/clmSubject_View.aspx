<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubject_View.aspx.vb" Inherits="Curriculum_clmSubject_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
 }

 function test2(val) {
     var path;
     if (val == 'LI') {
         path = '../Images/operations/like.gif';
     } else if (val == 'NLI') {
         path = '../Images/operations/notlike.gif';
     } else if (val == 'SW') {
         path = '../Images/operations/startswith.gif';
     } else if (val == 'NSW') {
         path = '../Images/operations/notstartwith.gif';
     } else if (val == 'EW') {
         path = '../Images/operations/endswith.gif';
     } else if (val == 'NEW') {
         path = '../Images/operations/notendswith.gif';
     }
     document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
                 }

    </script>






    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book  mr-3"></i> Subject Master
        </div>
        <div class="card-body">
            <div class="table-responsive ">

                <table id="tbl_ShowScreen" runat="server" cellspacing="0" align="center" width="100%">







                    <tr height="30">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass=""></asp:Label></td>
                    </tr>

                    <tr >
                        <td  align="center" style="width:100%;" >

                            

                                <tr>
                                    <td  align="left" >
                                        <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td  align="center">
                                        <asp:GridView ID="gvclmSubject" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20"  Width="100%">
                                            <RowStyle CssClass="griditem"   />
                                            <EmptyDataRowStyle  />

                                            <Columns>
                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbmId" runat="server" Text='<%# Bind("SBM_ID") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Subject">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblSub" runat="server" CssClass="" Text="Subject"></asp:Label><br />
                                                                    <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSubject_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSubject_Search_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBM_DESCR") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Short Code">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblsh" runat="server" CssClass="" Text="Short Code"></asp:Label><br />
                                                                    <asp:TextBox ID="txtShort" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnShort_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnShort_Search_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShortCode" runat="server" Text='<%# Bind("SBM_SHORTCODE") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Language">
                                                    <HeaderTemplate>
                                                        
                                                                   <asp:Label ID="lbllangu" runat="server" CssClass="" Text="Language"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlgvlang" runat="server" CssClass="" Width="70px" AutoPostBack="True" OnSelectedIndexChanged="ddlgvlang_SelectedIndexChanged">
                                                                            <asp:ListItem>ALL</asp:ListItem>
                                                                            <asp:ListItem>YES</asp:ListItem>
                                                                            <asp:ListItem>NO</asp:ListItem>
                                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgLang" runat="server" ImageUrl='<%# BIND("IMGLANG") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                                </asp:TemplateField>

                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px"></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Height="30px" CssClass="" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            

                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />

                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

 
</asp:Content>

