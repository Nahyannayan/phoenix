Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Partial Class Curriculum_showemployee
    Inherits BasePage
    Sub gridbind()
        Dim str_txtDescr2 As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String
        Dim strGroupId As String
        strGroupId = Request.QueryString("GrpId")
        Dim txtSearch As New TextBox
        Dim str_filter As String = String.Empty
        If gvempname.Rows.Count > 0 Then
            Dim str_Sid_search() As String
            str_Sid_search = h_selected_menu_2.Value.Split("__")
            txtSearch = gvempname.HeaderRow.FindControl("txtDESCR2")
            str_txtDescr2 = txtSearch.Text.Trim
            str_filter = str_filter & set_search_filter("ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'')", str_Sid_search(0), str_txtDescr2)
        End If
        'str_Sql = "SELECT DISTINCT EMP_ID, EMP_FNAME + '  ' + EMP_MNAME + ' ' + EMP_LNAME AS empname, EMPNO FROM dbo.EMPLOYEE_M WHERE (EMP_BSU_ID ='" & Session("sBsuid").ToString() & "')"
        If strGroupId <> "0" Then
            str_Sql = "SELECT VW_EMPLOYEE_M.EMP_ID,EMPNO, VW_EMPLOYEE_M.EMP_FNAME + ' ' + VW_EMPLOYEE_M.EMP_MNAME + ' ' + VW_EMPLOYEE_M.EMP_LNAME as empname,VW_EMPLOYEE_M.EMPNO " _
                        & " FROM dbo.GROUPS_TEACHER_S INNER JOIN " _
                        & " dbo.VW_EMPLOYEE_M ON GROUPS_TEACHER_S.SGS_EMP_ID = VW_EMPLOYEE_M.EMP_ID " _
                        & " WHERE GROUPS_TEACHER_S.SGS_SGR_ID='" & strGroupId & "' and " _
                        & " VW_EMPLOYEE_M.EMP_BSU_ID='" & Session("SBsuid") & "'" _
                        & str_filter
        Else
            str_Sql = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as EMPname,emp_fname,emp_mname,emp_lname," _
                        & "emp_id,EMPNO FROM vw_employee_m WHERE EMP_ECT_ID IN(1,3,4) and emp_bsu_id='" + Session("sBsuid") + "' AND EMP_STATUS IN(1,2)" _
                        & str_filter & "order by emp_fname,emp_mname,emp_lname"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvempname.DataSource = ds
        gvempname.DataBind()

        If str_txtDescr2 <> "" Then
            txtSearch = gvempname.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2
        End If
    End Sub
    Protected Sub btnCodeSearch2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub gvempname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvempname.SelectedIndexChanged
        Dim lblempId As Label
        Dim lnkempname As LinkButton

        lblempId = gvempname.SelectedRow.FindControl("lblempId")
        lnkempname = gvempname.SelectedRow.FindControl("lnkempname")

        'Response.Write("<script language='javascript'> function listen_window(){")
        ' Response.Write("window.returnValue = '" & lnkempname.Text + "||" + lblempId.Text & "';")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.Employee ='" & lnkempname.Text + "||" + lblempId.Text & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & lnkempname.Text + "||" + lblempId.Text & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")

        ' h_SelectedId.Value = "Close"
        'Dim javaSr As String
        'javaSr = "<script language='javascript'> function listen_window(){"
        'javaSr += "window.returnValue = '" & lnkempname.Text & "||" & lblempId.Text.Replace("'", "\'") & "';"
        'javaSr += "window.close();"
        'javaSr += "} </script>"
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
        'h_SelectedId.Value = "Close"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gridbind()
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub
    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvempname.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvempname.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub gvempname_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvempname.PageIndexChanging
        gvempname.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
End Class
