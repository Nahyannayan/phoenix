﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmAEROObjectivesBulkUpload.aspx.vb" Inherits="clmAEROObjectivesBulkUpload"
    Title="Untitled Page" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .FileUploadZone {
            width: 100%;
            height: 90px;
            border-color: #CCCCCC;
            color: #767676;
            float: left;
            border: 2px dashed #b3afaf !important;
            text-align: center;
            font-size: 16px;
            color: black;
            padding-top: 34px;
        }

        .demo-container .RadAsyncUpload {
            text-align: center;
            margin-left: 0;
            margin-bottom: 28px;
        }

            .demo-container .RadAsyncUpload .ruFileWrap {
                text-align: left;
            }

        .demo-container .RadUpload .ruUploadProgress {
            width: 210px;
            display: inline-block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            vertical-align: top;
        }

        html .demo-container .ruFakeInput {
            width: 200px;
        }

        html .RadUpload .ruFileWrap {
            position: relative;
        }

        div.RadUpload .ruBrowse {
            background-position: 62px 79px;
            width: 120px;
            height: 28px;
        }

        div.RadUpload_Default .ruFileWrap .ruButtonHover {
            background-position: 100% -23px !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
             <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcyear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Term</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade </span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Subject</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trhide" runat="server">
                                    <td align="left"><span class="field-label">Age Band </span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAgeband" runat="server" AutoPostBack="true" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label"></span>
                                    </td>
                                    <td align="left">
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters"><span class="field-label">Import Data From Excel</span>
                                    </td>
                                    <td class="matters">
                                        <table width="100%">
                                            <tr>
                                                <td width="80%">
                                                    <telerik:RadAsyncUpload ID="uploadFile" RenderMode="Lightweight" HideFileInput="true" Width="100%" runat="server" MultipleFileSelection="Disabled" DropZones=".FileUploadZone"></telerik:RadAsyncUpload>
                                                </td>
                                                <td width="20%">
                                                    <span class="field-label">OR</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <div class="FileUploadZone">
                                            Drop Files Here
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnUpload" runat="server" CausesValidation="False" CssClass="button"
                                            TabIndex="30" Text="Upload Excel" />
                                         <asp:Button ID="btnTemplate" runat="server" CausesValidation="False" CssClass="button"
                                            TabIndex="30" Text="Download Template" OnClick="btnTemplate_Click" />
                                    </td>

                                </tr>
                                <tr>
                                    <td class="matters" align="center" colspan="4">
                                        <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" Visible="false"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: center">
                                        <asp:GridView ID="gvObjectives" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20">
                                            <RowStyle CssClass="griditem" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="index" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SYC_ID" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblObjId" runat="server" Text='<%# Bind("SYC_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Topic">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtTopic" runat="server" Text='<%# Bind("Topic")%>' Width="100%"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sub Topic">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSubTopic" Text='<%# Bind("Sub_Topic")%>' runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Objective">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtObjective" runat="server" Text='<%# Bind("Objective")%>' Width="100%"
                                                            TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Date">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtStartDate" runat="server" Text='<%# Bind("Start_Date")%>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgBtnT_StartDt" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="cxT_FrmDt" CssClass="black" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnT_StartDt"
                                                            TargetControlID="txtStartDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End Date">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtEndDate" runat="server" Text='<%# Bind("End_Date")%>'></asp:TextBox>
                                                        <asp:ImageButton ID="imgBtnT_EndDt" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                        <ajaxToolkit:CalendarExtender ID="cxT_EndDt" CssClass="black" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnT_EndDt"
                                                            TargetControlID="txtEndDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No Of Lessons">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtLesson" Text='<%# Bind("No_Of_Lesson")%>' runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Link To Rubric" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRubric" Checked='<%# Bind("b_Rubric")%>' runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Link To Topic Skills Evaluation" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkTopicSkill" Checked='<%# Bind("b_Topic_Skill")%>' runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Steps">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtStep" runat="server" Text='<%# Bind("Step")%>' Width="100%"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" Visible="false"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
