<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmOptionGroupsView.aspx.vb" Inherits="Students_studAuthorized_Leave_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Literal ID="ltHeader" runat="server" Text="Option Group Allocation"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

    
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" colspan="4" valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="4" valign="top">
                <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
        </tr>
        <tr>
            <td align="left" colspan="4" valign="top">
                <table id="tbl_test" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr >
                        <td align="left" class="matters" colspan="9" valign="top">
                            <asp:GridView ID="gvCAD_DRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <RowStyle />
                                <Columns>
                                    <asp:TemplateField HeaderText="ACADEMIC_YEAR">
                                        <EditItemTemplate>
                                           
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                          
                                                            <asp:Label ID="lblAcademicYr" runat="server" Text="Academic Year" ></asp:Label><br />
                                                                            <asp:TextBox ID="txtAcademicYear" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchTerm_desc" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                           
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAcademicYear" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle ></HeaderStyle>

                                        <ItemStyle HorizontalAlign="left" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                           
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                           
                                                            <asp:Label ID="lblGradeH" runat="server" Text="Grade" ></asp:Label><br />
                                                                            <asp:TextBox ID="txtGrade" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchAllocatedBy" OnClick="btnSearchFromDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" 
                                                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OPtionGroupName">
                                        <HeaderTemplate>
                                           
                                                            <asp:Label ID="lblOptionGpName" runat="server" Text="Option Group Name" CssClass="gridheader_text"></asp:Label><br />
                                                                            <asp:TextBox ID="txOptionGroupName" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchActivity" OnClick="btnSearchFromDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOptionGroupName" runat="server" Text='<%# BIND("SGM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SGM_ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSGM_ID" runat="server" Text='<%# Bind("SGM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkView" CommandName="View" CommandArgument='<%# Bind("SGM_ID") %>' runat="server">View</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle />
                            </asp:GridView>
                            &nbsp;<br />
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
        </tr>
    </table>

                </div>
        </div>
    </div>


</asp:Content>

