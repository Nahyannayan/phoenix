﻿
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Web.Configuration
Partial Class Curriculum_clmStudentSelfAwareness
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")



        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                'Session("liUserList") = Nothing
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330365") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"


                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                       BindGrade()
                    PopulateSection()
                    Show_HideRows(True)
                  
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else
            'studClass.SetChk(gvStud, Session("liUserList"))
        End If
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub Show_HideRows(ByVal display As Boolean)

        tr1.Visible = display
        tr2.Visible = display
        tr3.Visible = display

        tr4.Visible = Not display
        tr6.Visible = Not display
        tr7.Visible = Not display
    End Sub


    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE " _
                                & "  SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                 & " ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        If Session("CurrSuperUser") <> "Y" Then
            Dim li As New ListItem
            li.Text = "All"
            li.Value = "0"
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT  GRM_DISPLAY,GRM_GRD_ID FROM OASIS..GRADE_BSU_M AS A" _
                        & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                        & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                        & " ORDER BY GRD_DISPLAYORDER"
        Else
            str_query = "SELECT  GRM_DISPLAY,GRM_GRD_ID FROM OASIS..GRADE_BSU_M AS A" _
                       & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                       & " INNER JOIN VW_SECTION_M ON GRM_ID=SCT_GRM_ID" _
                       & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                       & " AND SCT_EMP_ID=" + Session("EmployeeId") _
                       & " ORDER BY GRD_DISPLAYORDER"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindStudents()
        Dim str_conn = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue.ToString Then
            str_query = "SELECT STU_ID,SECTION=GRM_DISPLAY+'-'+SCT_DESCR,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                        & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View',ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH " _
                        & " FROM STUDENT_M AS A INNER JOIN VW_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                        & " INNER JOIN VW_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                        & " WHERE STU_CURRSTATUS='EN' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Else
            str_query = "SELECT STU_ID,SECTION=GRM_DISPLAY+'-'+SCT_DESCR,STU_NO,STU_NAME," _
                        & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View',ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH " _
                        & " FROM VW_STUDENT_DETAILS_PREVYEARS" _
                        & " WHERE STU_CURRSTATUS='EN' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        End If


        If studClass.isEmpTeacher(Session("EMPLOYEEID")) = True And Session("CurrSuperUser") <> "Y" Then
            str_query += " AND STU_ID IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S AS A INNER JOIN" _
                        & " GROUPS_TEACHER_S AS B ON A.SSD_SGR_ID=B.SGS_SGR_ID AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                        & " AND SGS_TODATE IS NULL AND SSD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + ") "
        End If

        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID= " + ddlSection.SelectedValue.ToString
        End If
        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID= '" + ddlGrade.SelectedValue.ToString + "'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvStudent.DataSource = ds
        gvStudent.DataBind()
        If gvStudent.Rows.Count > 0 Then
            gvStudent.HeaderRow.Visible = False
        End If


    End Sub

    Function GetPhoto(ByVal StuPhotoPath As String)
         If StuPhotoPath <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + StuPhotoPath
            Return strImagePath
        Else
            Return "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Function
    

    Sub BindSetup()
        Dim str_conn = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SEM_ID,SEM_DESCR,ISNULL(SED_DESCR,'') SED_DESCR FROM RPT.SELFAWARENES_M " _
                               & " LEFT OUTER JOIN RPT.STUDENT_SELFAWARENESS_D ON SEM_ID=SED_SEM_ID AND SED_STU_ID=" + hfSTU_ID.Value _
                               & " WHERE SEM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SEM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSetup.DataSource = ds
        gvSetup.DataBind()

        If gvSetup.Rows.Count > 0 Then
            gvSetup.HeaderRow.Visible = False
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim lblSemId As Label
        Dim txtDescr As TextBox

        For i = 0 To gvSetup.Rows.Count - 1
            lblSemId = gvSetup.Rows(i).FindControl("lblSemId")
            txtDescr = gvSetup.Rows(i).FindControl("txtDescr")
            str_query = "exec RPT.saveSTUDENTSELFAWARENES" _
                       & " @SED_STU_ID=" + hfSTU_ID.Value + "," _
                       & " @SED_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                       & " @SED_GRD_ID=" + ddlGrade.SelectedValue.ToString + "," _
                       & " @SED_SEM_ID=" + lblSemId.Text + "," _
                       & " @SED_DESCR='" + txtDescr.Text + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next
    End Sub
#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindStudents()
        Show_HideRows(False)
        Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
        hfSTU_ID.Value = lblStuId.Text
        BindSetup()
    End Sub

    Protected Sub btnCancelSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSearch.Click
        Show_HideRows(True)
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        PopulateSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
    End Sub

    Protected Sub gvStudent_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudent.PageIndexChanged
        If gvStudent.Rows.Count > 0 Then
            Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
            hfSTU_ID.Value = lblStuId.Text
                 BindSetup()
        End If
    End Sub

    Protected Sub gvStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudent.PageIndexChanging
        gvStudent.PageIndex = e.NewPageIndex
        BindStudents()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
        If gvStudent.PageIndex <> gvStudent.PageCount - 1 Then
            SaveData()
            gvStudent.PageIndex += 1
            BindStudents()
            If gvStudent.Rows.Count > 0 Then
                Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
                hfSTU_ID.Value = lblStuId.Text
                BindSetup()
            End If
        End If
       
    End Sub
End Class
