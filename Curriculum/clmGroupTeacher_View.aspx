<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmGroupTeacher_View.aspx.vb" Inherits="Curriculum_clmGroupTeacher_View"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


       
function switchViews(obj,row)
        {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
            
            if (div.style.display=="none")
                {
                    div.style.display = "inline";
                    if (row=='alt')
                       {
                           img.src="../Images/expand_button_white_alt_down.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_Button_white_Down.jpg" ;
                       }
                   img.alt = "Click to close";
               }
           else
               {
                   div.style.display = "none";
                   if (row=='alt')
                       {
                           img.src="../Images/Expand_button_white_alt.jpg" ;
                       }
                   else
                       {
                           img.src="../Images/Expand_button_white.jpg" ;
                       }
                   img.alt = "Click to expand";
               }
       }

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i> Allocate Teachers To Groups
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="center" colspan="4" width="100%">
                <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="4" align="center" >
                            <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                
                                <tr>
                                    <td align="left" width="20%">
                                       <span class="field-label">   Select Academic Year</span></td>
                                    
                                    <td align="left" >
                                       
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                       <td align="left" width="50%" colspan="2">
                                           </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" >
                                        <asp:GridView ID="gvTeacher" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvTeacher_RowDataBound"
                                            PageSize="20" AllowPaging="True" >
                                            <columns>
           
           <asp:TemplateField HeaderText="-" Visible="False">
                       <ItemStyle HorizontalAlign="Left" />
                       <ItemTemplate>
                       <asp:Label ID="lblEmpId" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                       </ItemTemplate>
                       </asp:TemplateField>
                  
                             
                      
           <asp:TemplateField>
           <ItemTemplate>
              <a href="javascript:switchViews('div<%# Eval("EMP_ID") %>', 'one');">
                <img id="imgdiv<%# Eval("EMP_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
               </a>
            </ItemTemplate>
            <AlternatingItemTemplate>
             <a href="javascript:switchViews('div<%# Eval("EMP_ID") %>', 'alt');">
              <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
              </a>
            </AlternatingItemTemplate>
           </asp:TemplateField>
           
                   <asp:TemplateField HeaderText="Teacher Name"><HeaderTemplate>
                                 
                                  <asp:Label ID="lbl133" runat="server"  Text="Teacher Name"></asp:Label><br />
                                 <asp:TextBox ID="txtTeacher" runat="server"></asp:TextBox>
                               <asp:ImageButton ID="btnTeacher_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnTeacher_Search_Click" />
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:Label ID="lblEmpName" runat="server" text='<%# Bind("EMP_NAME") %>'></asp:Label>
                      
                    </ItemTemplate>
                    </asp:TemplateField>
                                            
                   
                        <asp:TemplateField HeaderText="Department"><HeaderTemplate>
                                
                                  <asp:Label ID="lbld" runat="server"  Text="Department"></asp:Label><br />
                                                   <asp:TextBox ID="txtDPT" runat="server"></asp:TextBox>
                               <asp:ImageButton ID="btnDPT_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnDPT_Search_Click" />
</HeaderTemplate>
<ItemTemplate>
                      <asp:Label ID="lblDPT" runat="server" text='<%# Bind("DPT_DESCR") %>'></asp:Label>
                      
</ItemTemplate>
</asp:TemplateField>
                                          
                       
               <asp:ButtonField CommandName="View" HeaderText="View" Text="View"  >
           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
           <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
           </asp:ButtonField>                    
                             
              
                
          <asp:TemplateField >
           <ItemTemplate>
                                    </td>
                                </tr>
                                <tr>
                                    <td  colspan="5">
                                        <div id="div<%# Eval("EMP_ID") %>" style="display: none; position: relative;">
                                            <asp:GridView ID="gvGroup" CssClass="table table-bordered table-row" runat="server" Width="100%" AutoGenerateColumns="false"
                                                EmptyDataText="No groups for this teacher.">
                                                <columns>
           <asp:BoundField DataField="SGR_DESCR" HeaderText="Groups"  HtmlEncode="False" >
           <ItemStyle HorizontalAlign="left" />
           </asp:BoundField>
           
           </columns>
                                                <rowstyle CssClass="griditem" />
                                                <headerstyle  />
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                </ItemTemplate> </asp:TemplateField> </Columns>
                                <rowstyle  />
                                <headerstyle  />
                                <alternatingrowstyle  />
                                <selectedrowstyle  />
                                <pagerstyle  horizontalalign="Left" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
        runat="server" type="hidden" value="=" /></td></tr> </table>

            </div>
        </div>
    </div>
   

</asp:Content>
