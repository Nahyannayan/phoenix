﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmExamSetup_Approve.aspx.vb" Inherits="Curriculum_clmExamSetup_Approve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <style>
 .darkPanlAlumini {
        width: 100%;
    height: 100%;
    position: fixed;
    left: 0%;
    top: 0%;
    background: rgba(0,0,0,0.2) !important;
    /*display: none;*/
    display: block;
}

 .inner_darkPanlAlumini {
      left: 20%;
    top: 40%;
    position: fixed;
    width: 70%;
}
    </style>
    <script type="text/javascript">
        $("body").on("click", "[id*=chkAll]", function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $("body").on("click", "[id*=btnApprove], [id*=btnReject]", function () {
            var flag = false;
            $('input:checkbox').each(function (e) {
                if (this.checked) {
                    flag = true;
                }
            });
            if (!flag) {
                alert('You must check at least one checkbox.');
                return false;
            }
        });
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Exam Setup Approve
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="tbl_AddGroup" runat="server" width="100%">
                <tr>
                    <td align="left" valign="bottom">
                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            SkinID="error" Style="text-align: center"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="Table2" runat="server" width="100%">
                            <tr>
                                <td class="matters" align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                <td class="matters" align="left" width="30%">
                                    <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td class="matters" align="left" width="20%"><span class="field-label">Grade</span></td>
                                <td class="matters" align="left" width="30%">
                                    <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="matters" align="left" width="20%"><span class="field-label">From Date</span></td>
                                <td class="matters" align="left" width="30%">
                                    <asp:TextBox ID="txtFromDate" runat="server" autocomplete="off" onFocus="this.blur()"></asp:TextBox>
                                    <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                                <td class="matters" align="left" width="20%"><span class="field-label">To Date</span></td>
                                <td class="matters" align="left" width="30%">
                                    <asp:TextBox ID="txtToDate" runat="server" autocomplete="off" onFocus="this.blur()"></asp:TextBox>
                                    <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="matters" colspan="4">
                                    <asp:RadioButton ID="rbPending" runat="server" GroupName="Fltr" Text="Pending" CssClass="field-label"></asp:RadioButton>
                                    <asp:RadioButton ID="rbApprove" runat="server" GroupName="Fltr" Text="Approve" CssClass="field-label"></asp:RadioButton>
                                    <asp:RadioButton ID="rbReject" runat="server" GroupName="Fltr" Text="Reject" CssClass="field-label"></asp:RadioButton>
                                    <asp:RadioButton ID="rbAll" runat="server" GroupName="Fltr" Text="All" CssClass="field-label"></asp:RadioButton>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" ValidationGroup="groupM1"
                                        TabIndex="7" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table width="100%">
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvExamSetup_Approve" runat="server" AllowPaging="false" AutoGenerateColumns="False" OnDataBound="OnDataBound"
                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                            PageSize="20">
                            <RowStyle CssClass="griditem" Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <Columns>
                                <asp:TemplateField HeaderText="ESA_ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblESA_ID" runat="server" Text='<%# Bind("ESA_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ESA_STU_ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblESA_STU_ID" runat="server" Text='<%# Bind("ESA_STU_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ESA_ES_ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblESA_ES_ID" runat="server" Text='<%# Bind("ESA_ES_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>

                                <asp:BoundField DataField="STU_NO" HeaderText="Student No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="130" />
                                <asp:BoundField DataField="STU_NAME" HeaderText="Student Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200" />
                                <asp:BoundField DataField="STU_GENDER" HeaderText="Gender" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70" />
                                <asp:BoundField DataField="STU_FATHER" HeaderText="Parent Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200" />
                                <asp:BoundField DataField="PARENT_MOBILE" HeaderText="Mobile No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120" />
                                <asp:BoundField DataField="PARENT_EMAIL" HeaderText="E-Mail" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150" />
                                <asp:BoundField DataField="Grade" HeaderText="Grade" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50" />
                                <asp:BoundField DataField="SCT_DESCR" HeaderText="Section" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50" />
                                <asp:BoundField DataField="ExamPaper" HeaderText="Exam Paper" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150" />

                                <asp:BoundField DataField="BoardAmount" HeaderText="Board Cost" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100" />
                                <asp:BoundField DataField="BaseAmount" HeaderText="School Cost" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100" />
                                <asp:BoundField DataField="VAT" HeaderText="VAT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100" />
                                <asp:BoundField DataField="Cost" HeaderText="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100" />

                                <asp:BoundField DataField="TentativeDate" HeaderText="Exam Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100" />
                                <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100" />
                                <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100" />

                                <asp:TemplateField HeaderText="#">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkAll" runat="server" Width="50"></asp:CheckBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkChild" runat="server" Width="50"></asp:CheckBox>
                                        <asp:HiddenField Value='<%# Bind("PARENT_EMAIL") %>' runat="server" ID="hdn_PARENT_EMAIL" />
                                        <asp:HiddenField Value='<%# Bind("ExamPaper") %>' runat="server" ID="hdn_ExamPaper" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>

                            </Columns>
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                        </asp:GridView>
                    </td>
                </tr>

            </table>
            <table width="100%">
                <tr>
                    <td align="center">
                        <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" ValidationGroup="groupM1"
                            TabIndex="7" />
                        <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" ValidationGroup="groupM1"
                            TabIndex="7" />
                    </td>
                </tr>
            </table>

            <asp:Panel ID="pnl_Approve" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini" >
                        <div >
                            <div style="float: right">
                                <asp:LinkButton ID="lnkClosePop" ToolTip="click here to close"
                                    runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkClosePop_Click"></asp:LinkButton>
                            </div>
                            <table width="100%">

                                <tr>
                                    <td><span class="field-label">Remarks</span>
                                    </td>
                                    <td>

                                        <textarea id="txtRemarks" runat="server" cols="32" rows="3"></textarea>
                                        <asp:RequiredFieldValidator ID="rfvRemark" runat="server" ErrorMessage="Enter Remarks/Comments " ControlToValidate="txtRemarks" ></asp:RequiredFieldValidator>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                       
                            <asp:Button ID="btnRejectExam" runat="server" Text="REJECT" CssClass="button"   />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </asp:Panel>
        </div>
    </div>
</asp:Content>

