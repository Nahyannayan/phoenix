Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM
Imports ActivityFunctions

Partial Class Curriculum_rptStudentMarks_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330003") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'BindDropDownBoxes()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    h_ACD_ID.Value = ddlAcademicYear.SelectedItem.Value


                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGrade.Items.Insert(0, li)
                    h_GRD_ID.Value = ddlGrade.SelectedItem.Value

                    ddlSubject = currFns.PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlSubject.Items.Insert(0, li)
                    h_SUBJ_ID.Value = ddlSubject.SelectedItem.Value

                    ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, "", ddlSubject.SelectedValue.ToString)
                    li = New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlGroup.Items.Insert(0, li)
                    h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)

                    h_selected_menu_1.Value = "LI__../Images/operations/like.gif"

                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub lnkNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Curriculum\rptStudentsMarks.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindDropDownBoxes()
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGrade.Items.Insert(0, li)
        If Not ddlGrade.SelectedIndex = -1 Then
            h_GRD_ID.Value = ddlGrade.SelectedItem.Value
        End If

        ddlSubject = currFns.PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        li = New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlSubject.Items.Insert(0, li)
        If Not ddlSubject.SelectedIndex = -1 Then
            h_SUBJ_ID.Value = ddlSubject.SelectedItem.Value
        End If

        ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString)
        li = New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGroup.Items.Insert(0, li)
        If Not ddlGroup.SelectedIndex = -1 Then
            h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)
        End If
    End Sub


    Private Sub BindGrid()
        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsMarks As DataSet
        Dim txtsubjsearch As New TextBox


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        If ddlGrade.SelectedValue <> "" Then
            strCriteria = " AND RST_GRD_ID='" & ddlGrade.SelectedValue & "' "
        End If
        If (ddlSubject.SelectedValue <> "") Then
            strCriteria += " AND RST_SBG_ID=" & ddlSubject.SelectedValue & " "
        End If
        If ddlGroup.SelectedValue <> "" Then
            strCriteria += " AND SGR_DESCR='" & ddlGroup.SelectedItem.Text & "' "
        End If

        If txtStudName.Text <> "" Then
            strCriteria += " AND RST_STU_ID=" & H_STU_ID.Value & " "
        End If
        If txtRepSchedule.Text <> "" Then
            strCriteria += " AND RST_RPF_ID=" & H_SCH_ID.Value & " "
        End If
        If txtRepHeader.Text <> "" Then
            strCriteria += " AND RST_RSD_ID=" & H_SETUP.Value & " "
        End If
        strSql = "SELECT DISTINCT RST_ID,RST_STU_ID,RST_GRD_ID,RST_SBG_ID,RST_STU_ID,RST_ACD_ID,M.ACY_DESCR,RSD_HEADER, " & _
                 "ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') +' '+ ISNULL(STU_LASTNAME,'') AS STUNAME, " & _
                 "SBG_DESCR, SGR_DESCR, RST_TYPE_LEVEL, RST_COMMENTS, RST_GRADING,RST_MARK " & _
                 "FROM RPT.REPORT_STUDENT_S AS  A " & _
                 "INNER JOIN " & _
                 "SUBJECTS_GRADE_S AS B ON A.RST_SBG_ID=B.SBG_ID " & _
                 "INNER JOIN GROUPS_M AS C ON A.RST_SGR_ID=C.SGR_ID " & _
                 "INNER JOIN VW_ACADEMICYEAR_D D ON D.ACD_ID=RST_ACD_ID " & _
                 "INNER JOIN VW_ACADEMICYEAR_M M ON M.ACY_ID=D.ACD_ACY_ID " & _
                 "INNER JOIN VW_STUDENT_M STU ON STU.STU_ID=A.RST_STU_ID " & _
                 "INNER JOIN RPT.REPORT_SETUP_D RSD ON RSD.RSD_ID=A.RST_RSD_ID " & _
                 "INNER JOIN RPT.REPORT_PRINTEDFOR_M RPF ON RPF.RPF_ID=A.RST_RPF_ID " & _
                 "WHERE SGR_ACD_ID=" & ddlAcademicYear.SelectedValue & " " & strCriteria & " AND A.RST_RSD_ID IN (SELECT RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_BDIRECTENTRY=1)"

        If (gvMarks.Rows.Count > 0) Then

            Dim strStnamsearch As String()
            Dim strSearch As String
            Dim strFilter As String = ""
            Dim txtSearch As New TextBox
            strStnamsearch = h_selected_menu_1.Value.Split("__")
            strSearch = strStnamsearch(0)
            txtSearch = gvMarks.HeaderRow.FindControl("txtStudName")
            If (txtSearch.Text <> "") Then
                strSearch = strStnamsearch(0)
                strFilter = GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)

                If strFilter <> "" Then
                    strSql += strFilter
                    strFilter = ""
                End If

            End If

            txtSearch = gvMarks.HeaderRow.FindControl("txtSubject")
            strStnamsearch = h_selected_menu_1.Value.Split("__")
            strSearch = strStnamsearch(0)
            If txtSearch.Text <> "" Then

                strFilter = GetSearchString("SBG_DESCR", txtSearch.Text.Replace("/", " "), strSearch)
                strSql += strFilter
            End If

            strSql += " ORDER BY 1 "
            'Dim ds As DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            'gvMarks.DataSource = ds

        End If




        'INNER JOIN RPT.REPORT_SETUP_M RSM ON RSM.RSM_ID=RSD.RSD_ID 
        dsMarks = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

        If dsMarks.Tables(0).Rows.Count = 0 Then
            gvMarks.DataSource = dsMarks.Tables(0)
            dsMarks.Tables(0).Rows.Add(dsMarks.Tables(0).NewRow())
            gvMarks.DataBind()
            Dim columnCount As Integer = gvMarks.Columns.Count
            gvMarks.Rows(0).Cells.Clear()
            gvMarks.Rows(0).Cells.Add(New TableCell)
            gvMarks.Rows(0).Cells(0).ColumnSpan = columnCount
            gvMarks.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvMarks.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvMarks.DataSource = dsMarks.Tables(0)
            gvMarks.DataBind()
        End If
    End Sub

#End Region

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_STU_ID.Value <> "" AndAlso H_STU_ID.Value.Contains("___") Then
                Dim STU_ID As String = H_STU_ID.Value.Split("___")(0)
                txtStudName.Text = H_STU_ID.Value.Split("___")(3)
                lblStudNo.Text = H_STU_ID.Value.Split("___")(6)
                H_STU_ID.Value = STU_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnView_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            BindGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvMarks_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            Try
                gvMarks.PageIndex = e.NewPageIndex
                BindGrid()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
          
            BindDropDownBoxes()
            If Not ddlAcademicYear.SelectedIndex = -1 Then
                h_ACD_ID.Value = ddlAcademicYear.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try
    End Sub

   
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim li As New ListItem
            ddlSubject = currFns.PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlSubject.Items.Insert(0, li)
            If Not ddlSubject.SelectedIndex = -1 Then
                h_SUBJ_ID.Value = ddlSubject.SelectedItem.Value
            End If

            ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString)
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGroup.Items.Insert(0, li)
            If Not ddlGroup.SelectedIndex = -1 Then
                h_SBG_ID.Value = ddlGroup.SelectedItem.Text
            End If
            If Not ddlGrade.SelectedIndex = -1 Then
                h_GRD_ID.Value = ddlGrade.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblMrksId As New Label
            Dim url As String
            lblMrksId = TryCast(sender.FindControl("lblId"), Label)
            If Not lblMrksId Is Nothing Then
                ViewState("datamode") = "edit"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'Encrypt the data that needs to be send through Query String
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                url = String.Format("~\Curriculum\rptStudentsMarksEdit.aspx?MainMnu_code={0}&datamode={1}&MarksId={2}&AccId={3}&SubjID={4}&GradeId={5}", ViewState("MainMnu_code"), ViewState("datamode"), Encr_decrData.Encrypt(lblMrksId.Text), Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue), Encr_decrData.Encrypt(h_SUBJ_ID.Value), Encr_decrData.Encrypt(h_GRD_ID.Value))
                Response.Redirect(url)

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim li As New ListItem
        If Not ddlSubject.SelectedIndex = -1 Then
            ddlGroup = currFns.PopulateGroupsByTeacher(Session("EmployeeID"), ddlGroup, ddlAcademicYear.SelectedValue.ToString, "", ddlSubject.SelectedValue.ToString)
            li = New ListItem
            li.Text = "ALL"
            li.Value = ""
            ddlGroup.Items.Insert(0, li)
        End If
        If Not ddlSubject.SelectedIndex = -1 Then
            h_SUBJ_ID.Value = ddlSubject.SelectedItem.Value
        End If
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvMarks.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvMarks.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

    End Sub

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvMarks.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvMarks.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlGroup.SelectedIndex = -1 Then
            h_SBG_ID.Value = IIf(ddlGroup.SelectedItem.Text = "ALL", "", ddlGroup.SelectedItem.Text)
        End If
    End Sub

    Protected Sub btnSubject_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGrid()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            BindGrid()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvMarks_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ImgReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_RPT_ID.Value <> "" AndAlso H_RPT_ID.Value.Contains("___") Then
                Dim RPT_ID As String = H_RPT_ID.Value.Split("___")(0)
                txtReport.Text = H_RPT_ID.Value.Split("___")(3)
                H_RPT_ID.Value = RPT_ID
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ImgRepSchedule_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_SCH_ID.Value <> "" AndAlso H_SCH_ID.Value.Contains("___") Then
                Dim SCH_ID As String = H_SCH_ID.Value.Split("___")(0)
                txtRepSchedule.Text = H_SCH_ID.Value.Split("___")(3)
                H_SCH_ID.Value = SCH_ID
                'H_RPF_ID.Value = H_SCH_ID.Value.Split("___")(3) 'ReportFunctions.GetReportPrintedFor(H_SCH_ID.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgRepHeader_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If H_SETUP.Value <> "" AndAlso H_SETUP.Value.Contains("___") Then
                Dim SETUP_ID As String = H_SETUP.Value.Split("___")(0)
                txtRepHeader.Text = H_SETUP.Value.Split("___")(6)
                H_SETUP.Value = SETUP_ID
                'H_RPF_ID.Value = H_SCH_ID.Value.Split("___")(3) 'ReportFunctions.GetReportPrintedFor(H_SCH_ID.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
