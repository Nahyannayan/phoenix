﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_clmCopyRulesFASA
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100510") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYearFrom = studClass.PopulateAcademicYear(ddlAcademicYearFrom, Session("clm"), Session("sbsuid"))
                    ddlAcademicYearTo = studClass.PopulateAcademicYear(ddlAcademicYearTo, Session("clm"), Session("sbsuid"))
                    BindTerm(ddlTermFrom, ddlAcademicYearFrom.SelectedValue.ToString)
                    BindTerm(ddlTermTo, ddlAcademicYearTo.SelectedValue.ToString)
                    BindReportCardFrom()
                    BindReportCardTo()
                    BindReportSchedule(ddlReportScheduleTo, ddlReportCardTo.SelectedValue.ToString)
                    BindGrade()
                    BindHeader()
                    gvHeader.Attributes.Add("bordercolor", "#1b80b6")
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindTerm(ByVal ddlTerm As DropDownList, ByVal acd_id As String)
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + acd_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub

    Sub BindReportCardFrom()
        ddlReportCardFrom.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYearFrom.SelectedValue.ToString _
                                & " AND RSM_DESCR LIKE '%FORMATIVE%'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCardFrom.DataSource = ds
        ddlReportCardFrom.DataTextField = "RSM_DESCR"
        ddlReportCardFrom.DataValueField = "RSM_ID"
        ddlReportCardFrom.DataBind()
    End Sub

    Sub BindReportCardTo()
        ddlReportCardTo.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYearTo.SelectedValue.ToString _
                                & " AND ( RSM_DESCR LIKE '%SUMMATIVE%' OR RSM_DESCR LIKE '%TERM REPORT I-IV%'  OR RSM_DESCR LIKE '%TERM REPORT I-III%' OR RSM_DESCR LIKE '%TERM REPORT I-II%')" _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCardTo.DataSource = ds
        ddlReportCardTo.DataTextField = "RSM_DESCR"
        ddlReportCardTo.DataValueField = "RSM_ID"
        ddlReportCardTo.DataBind()
    End Sub

    Sub BindReportSchedule(ByVal ddlReportSchedule As DropDownList, ByVal rsm_id As String)
        ddlReportSchedule.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + rsm_id + "'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportSchedule.DataSource = ds
        ddlReportSchedule.DataTextField = "RPF_DESCR"
        ddlReportSchedule.DataValueField = "RPF_ID"
        ddlReportSchedule.DataBind()
    End Sub

    Sub BindGrade()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYearTo.SelectedValue.ToString _
                                & " AND RSG_RSM_ID='" + ddlReportCardTo.SelectedValue.ToString + "'" _
                                & " AND RSG_GRD_ID IN(SELECT RSG_GRD_ID FROM RPT.REPORTSETUP_GRADE_S " _
                                & " WHERE RSG_RSM_ID='" + ddlReportCardFrom.SelectedValue.ToString + "')" _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstGrade.DataSource = ds
        lstGrade.DataTextField = "GRM_DISPLAY"
        lstGrade.DataValueField = "GRM_GRD_ID"
        lstGrade.DataBind()
    End Sub

    Sub BindHeader()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RPF_ID,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M " _
                               & " INNER JOIN RPT.REPORT_RULE_M ON RRM_RPF_ID=RPF_ID" _
                               & " WHERE RPF_RSM_ID='" + ddlReportCardFrom.SelectedValue.ToString + "'" _
                               & " AND RRM_TRM_ID=" + ddlTermFrom.SelectedValue.ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvHeader.DataSource = ds
        gvHeader.DataBind()


        str_query = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D " _
                      & " WHERE RSD_RSM_ID='" + ddlReportCardTo.SelectedValue.ToString + "'" _
                      & " AND RSD_HEADER IN ('FA1','FA2')"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer
        Dim ddlHeader As DropDownList
        Dim li As ListItem

        For i = 0 To gvHeader.Rows.Count - 1
            ddlHeader = gvHeader.Rows(i).FindControl("ddlHeader")
            ddlHeader.DataSource = ds
            ddlHeader.DataTextField = "RSD_HEADER"
            ddlHeader.DataValueField = "RSD_ID"
            ddlHeader.DataBind()
            li = New ListItem
            li.Text = "--"
            li.Value = "0"
            ddlHeader.Items.Insert(0, li)
        Next



    End Sub

    Function getGrades() As String
        Dim str As String = ""
        Dim i As Integer

        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstGrade.Items(i).Value
            End If
        Next
        Return str
    End Function

    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim transaction As SqlTransaction
        Dim i As Integer
        Dim lblCadId As Label
        Dim ddlAssessement As DropDownList
        Dim lblRsdId As Label

        Dim ddlHeader As DropDownList
        Dim lblRpfId As Label

        Dim rsd1, rsd2, rpf1, rpf2 As String

        lblRpfId = gvHeader.Rows(0).FindControl("lblRpfId")
        ddlHeader = gvHeader.Rows(0).FindControl("ddlHeader")

        rsd1 = ddlHeader.SelectedValue.ToString
        rpf1 = lblRpfId.Text

        lblRpfId = gvHeader.Rows(1).FindControl("lblRpfId")
        ddlHeader = gvHeader.Rows(1).FindControl("ddlHeader")

        rsd2 = ddlHeader.SelectedValue.ToString
        rpf2 = lblRpfId.Text


        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                str_query = "exec COPY.COPYRULESFASA " _
                          & "@RSM_COPYTO=" + ddlReportCardTo.SelectedValue.ToString + "," _
                          & "@RPF_COPYTO=" + ddlReportScheduleTo.SelectedValue.ToString + "," _
                          & "@RSD1_COPYTO=" + rsd1 + "," _
                          & "@RSD2_COPYTO=" + rsd2 + "," _
                          & "@RPF1_COPYFRM=" + rpf1 + "," _
                          & "@RPF2_COPYFRM=" + rpf2 + "," _
                          & "@RSM_COPYFRM=" + ddlReportCardFrom.SelectedValue.ToString + "," _
                          & "@RRM_DESCR='" + txtFrom.Text + "'," _
                          & "@GRD_IDS='" + getGrades() + "'"


                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using




    End Sub

#End Region

    Protected Sub ddlAcademicYearFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYearFrom.SelectedIndexChanged
        BindTerm(ddlTermFrom, ddlAcademicYearFrom.SelectedValue.ToString)
        BindReportCardFrom()
          BindGrade()
        BindHeader()
    End Sub


    Protected Sub ddlAcademicYearTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYearTo.SelectedIndexChanged
        BindTerm(ddlTermTo, ddlAcademicYearTo.SelectedValue.ToString)
        BindReportCardTo()
        BindReportSchedule(ddlReportScheduleTo, ddlReportCardTo.SelectedValue.ToString)
        BindGrade()
        BindHeader()
    End Sub

    Protected Sub ddlReportCardFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCardFrom.SelectedIndexChanged
        BindGrade()
         BindHeader()
    End Sub

    Protected Sub ddlReportCardTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCardTo.SelectedIndexChanged
        BindReportSchedule(ddlReportScheduleTo, ddlReportCardTo.SelectedValue.ToString)
        BindGrade()
        BindHeader()
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        SaveData()
    End Sub

  
   
    Protected Sub ddlTermFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTermFrom.SelectedIndexChanged
        BindHeader()
    End Sub
End Class

