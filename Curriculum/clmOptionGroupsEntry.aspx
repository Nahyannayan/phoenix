<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmOptionGroupsEntry.aspx.vb" Inherits="clmActivity_D_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function switchViews(obj, row) {
            var div = document.getElementById(obj);
            if (div.style.display == "none") {
                div.style.display = "inline";
            }
            else {
                div.style.display = "none";
            }
        }

        function handleError() {

            return true;
        }
        window.onerror = handleError;

        function CheckForValidate() {
            var ddlCho1 = document.getElementById("<%=ddlChoice1.ClientID %>").value;
        var ddlOpt1 = document.getElementById("<%=ddlOptional1.ClientID %>").value;

        var ddlCho2 = document.getElementById("<%=ddlChoice2.ClientID %>").value;
        var ddlOpt2 = document.getElementById("<%=ddlOptional2.ClientID %>").value;
        alert(ddlCho1);
        alert(ddlCho2);
        if (ddlCho1 == ddlCho2) {
            return false;
        }
        else {
            return true;
        }
    }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Option Group Allocation"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="Table1"  width="100%">
                    <tr >
                        <td align="left"   >                           
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                    ></asp:Label>                          
                        </td>
                    </tr>
                </table>
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    
                    <tr>
                        <td  valign="bottom">
                            <table align="center"  cellpadding="5" cellspacing="0"
                                 width="100%" style="border-collapse: collapse">
                                <tr>
                                    <td class="title-bg-light" align="left" colspan="10"><span class="field-label">Option Group Allocation For Grade</span>
                            <asp:Label ID="lblGrade" runat="server" class="field-label"></asp:Label>
                                       <span class="field-label"> &nbsp;(Academic Year :</span>
                            <asp:Label ID="lblAcademicYear" runat="server" class="field-label"></asp:Label>
                                        <span class="field-label">)</span><asp:Label ID="Label1" runat="server" class="field-label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Section</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>
                                   
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStud_ID" runat="server"></asp:TextBox>
                                    </td>
                                   
                                </tr>
                                <tr>
                                     <td align="left" width="20%"><span class="field-label">Student Name</span></td>
                                   
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStud_Name" runat="server"></asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                      <td align="center" colspan="4">
                                        <asp:Button ID="btnList" runat="server" CausesValidation="False" CssClass="button"
                                            Text="List"   /></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="No Records Found" HeaderStyle-Height="30" PageSize="20" CellPadding="1" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AGGR %" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAggrigate" Text='<%# Bind("OVERALL") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SCIENCE %" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblScience" Text='<%# Bind("SCIENCE") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MATHS %" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMaths" Text='<%# Bind("MATHS") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CHOICE 1">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChoice1" runat="server" Text='<%# Bind("CHOICE1") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CHOICE 2">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChoice2" runat="server" Text='<%# Bind("CHOICE2") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CHOICE 3">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChoice3" runat="server" Text='<%# Bind("CHOICE3") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblEdit" CommandName="Edit" runat="server" Text="Edit"
                                                            OnClick="lblEdit_Click" />

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input id="h_Row" type="hidden" runat="server" />
                <asp:Panel ID="pnlPopup" runat="server" BackColor="white" Style="display: none;" CssClass="modalPopup" Width="580">
                    <asp:UpdatePanel ID="uppnlPopup" runat="server">
                        <ContentTemplate>
                            <table  cellspacing="0" cellpadding="2" width="100%" >
                                <tbody>
                                    <tr class="title-bg-small">
                                        <td colspan="2">Select Choice </td>
                                    </tr>
                                    <tr  align="left">
                                        <td  ><span class="field-label">Student ID</span></td>
                                       
                                        <td>
                                            <asp:Label ID="lblstudNo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr  align="left">
                                        <td  ><span class="field-label">Name</span></td>
                                        
                                        <td>
                                            <asp:Label ID="lblStudName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="center" colspan="2">
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><span class="field-label">AGGR%</span> </td>
                                                                        <td><span class="field-label">SCIENCE%</span> </td>
                                                                        <td><span class="field-label">MATHS% </span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblStudAggr" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblStudScience" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblStudMATHS" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr  align="left">
                                                        <td  ><span class="field-label">CHOICE 1</span></td>
                                                        
                                                        <td>
                                                            <asp:DropDownList ID="ddlStream1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStream1_SelectedIndexChanged"></asp:DropDownList><asp:DropDownList ID="ddlChoice1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dllChoice1_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:CompareValidator ID="cmpchoice1" runat="server" ControlToCompare="ddlChoice2" ControlToValidate="ddlChoice1" Operator="NotEqual" ErrorMessage="Choice should be unique" ValidationGroup="Popup" Enabled="False">*</asp:CompareValidator>&nbsp;
                                                            <asp:Label ID="lblElective1" runat="server" Text="Elective :"></asp:Label>
                                                            <asp:DropDownList ID="ddlOptional1" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:CustomValidator ID="cvChoice1" runat="server" ControlToValidate="ddlChoice1" ErrorMessage="Choice should be unique" ValidationGroup="Popup" OnServerValidate="cvChoice1_ServerValidate">*</asp:CustomValidator></td>
                                                    </tr>
                                                    <tr  align="left">
                                                        <td  align="left" colspan="2">
                                                            <asp:Label ID="lblOption1Subjects" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr  align="left">
                                                        <td><span class="field-label">CHOICE 2</span></td>
                                                        
                                                        <td>
                                                            <asp:DropDownList ID="ddlStream2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStream2_SelectedIndexChanged"></asp:DropDownList><asp:DropDownList ID="ddlChoice2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dllChoice2_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:CompareValidator ID="cmpchoice2" runat="server" ControlToCompare="ddlChoice3" ControlToValidate="ddlChoice2" Operator="NotEqual" ErrorMessage="Choice should be unique" ValidationGroup="Popup" Enabled="False">*</asp:CompareValidator>
                                                            <asp:Label ID="lblElective2" runat="server" Text="Elective :"></asp:Label>
                                                            <asp:DropDownList ID="ddlOptional2" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:CustomValidator ID="cvChoice2" runat="server" ControlToValidate="ddlChoice2" ErrorMessage="Choice should be unique" ValidationGroup="Popup" __designer:wfdid="w2" OnServerValidate="cvChoice2_ServerValidate">*</asp:CustomValidator></td>
                                                    </tr>
                                                    <tr  align="left">
                                                        <td  align="left" colspan="2">
                                                            <asp:Label ID="lblOption2Subjects" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr  align="left">
                                                        <td ><span class="field-label">CHOICE 3</span></td>
                                                       
                                                        <td >
                                                            <asp:DropDownList ID="ddlStream3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStream3_SelectedIndexChanged"></asp:DropDownList><asp:DropDownList ID="ddlChoice3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dllChoice3_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:CompareValidator ID="cmpchoice3" runat="server" ControlToCompare="ddlChoice1" ControlToValidate="ddlChoice3" Operator="NotEqual" ErrorMessage="Choice should be unique" ValidationGroup="Popup" Enabled="False">*</asp:CompareValidator>
                                                            <asp:Label ID="lblElective3" runat="server" Text="Elective :"></asp:Label>
                                                            <asp:DropDownList ID="ddlOptional3" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:CustomValidator ID="cvChoice3" runat="server" ControlToValidate="ddlChoice3" ErrorMessage="Choice should be unique" ValidationGroup="Popup" __designer:wfdid="w3" OnServerValidate="cvChoice3_ServerValidate">*</asp:CustomValidator></td>
                                                    </tr>
                                                    <tr  align="left">
                                                        <td  align="left" colspan="2">
                                                            <asp:Label ID="lblOption3Subjects" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr  align="center">
                                                        <td  colspan="2">
                                                            <asp:Button ID="btnStudSave" runat="server" Text="Save" CssClass="button" ValidationGroup="Popup"></asp:Button>
                                                            <asp:Button ID="btnClose" runat="server" CausesValidation="false" Text="Close" CssClass="button"></asp:Button>
                                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Popup"></asp:ValidationSummary>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="MPSetOption" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="true" PopupControlID="pnlPopup"
                    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label1">
                </ajaxToolkit:ModalPopupExtender>
            </div>
        </div>
    </div>
</asp:Content>

