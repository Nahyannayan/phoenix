<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSyllabus_Lessons_M.aspx.vb" Inherits="Curriculum_clmSyllabus_Lessons_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script type= "text/javascript">    
    function GetSubjects()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var syllabusId;
            syllabusId=document.getElementById('<%=h_SyllabusId.ClientID %>').value; 
            //alert(syllabusId);               
            result = window.showModalDialog("showTopics.aspx?syllabusId="+syllabusId,"", sFeatures)
            if(result != "" && result != "undefined")
            {
               NameandCode = result.split('||');
               document.getElementById('<%=txtTopic.ClientID %>').value=NameandCode[1]; 
               document.getElementById('<%=h_TopicID.ClientID %>').value=NameandCode[0]; 
            }
            return false;
        }
    
       function GetSubjects_old()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var syllabusId;
            syllabusId=document.getElementById('<%=h_SyllabusId.ClientID %>').value; 
            //alert(syllabusId);               
           
            result = window.showModalDialog("ClmPopupForm.aspx?multiselect=false&SylID='"+syllabusId+"'&ID=PARENTTOPIC","", sFeatures)
            if(result != "" && result != "undefined")
            {
               NameandCode = result.split('___');
               document.getElementById('<%=txtTopic.ClientID %>').value=NameandCode[1]; 
               document.getElementById('<%=h_TopicID.ClientID %>').value=NameandCode[0]; 
            }
            return false;
        }
         function Getemployee()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var syllabusId;
            var groupId;
           // syllabusId=document.getElementById('<%=h_SyllabusId.ClientID %>').value; 
            //alert(syllabusId);   
            groupId=document.getElementById('<%=h_GRP_ID.ClientID %>').value;            
            result = window.showModalDialog("showemployee.aspx?GrpId="+groupId+"","", sFeatures)
             if(result != "" && result != "undefined")
            {
               NameandCode = result.split('||');
              document.getElementById('<%=txtempname.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%=h_EMP_ID.ClientID %>').value=NameandCode[1]; 
              
            }
            return false;
        }
        function GetSyllabus()
        {
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var acdid;
            var termid;
            var gradeid;
            var groupid;
            var subjectid;
            var url;
            
            acdid=document.getElementById('<%=h_ACD_ID.ClientID %>').value;
            termid=document.getElementById('<%=h_TRM_ID.ClientID %>').value;
            gradeid=document.getElementById('<%=h_GRD_ID.ClientID %>').value.split('|');
            groupid=document.getElementById('<%=h_GRP_ID.ClientID %>').value;
            subjectid=document.getElementById('<%=h_SUBJ_ID.ClientID %>').value;
            
            url="ClmPopupForm.aspx?multiselect=false&ID=SYLLABUS&AcdID="+acdid+"&TrmID="+termid+"&GrdID="+gradeid[0]+"&GrpID="+groupid+"&SubjID="+subjectid+"";
            result = window.showModalDialog(url,"", sFeatures)
            if (result=='' || result==undefined)
            {    return false;      } 
            NameandCode = result.split('___');  
            document.getElementById("<%=txtSyllabus.ClientID %>").value=NameandCode[1];
            document.getElementById("<%=h_SyllabusId.ClientID %>").value=NameandCode[0];  
            
        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 100%" >
       
        <tr id="Tr2" runat="server">
            <td align="left" >
                            <asp:Label id="lblError" runat="server" EnableViewState="False" SkinID="LabelError">
                </asp:Label></td>
        </tr>
    </table>
    <table align="center" cellpadding="5" cellspacing="0" border="1" bordercolor="#1b80b6"
                     style="border-collapse:collapse " >
        <tr style="font-size: 8pt; color: #800000" valign="bottom">
            
        </tr>
        <tr class="subheader_img" valign="top">
            <td align="left" colspan="8">
                <asp:Label id="Label5" runat="server" Text="LESSON ALLOCATION"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px">
                Academic Year</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 159px">
                <asp:DropDownList id="ddlacadamicyear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlacadamicyear_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left" class="matters" colspan="1">
                Term</td>
            <td align="left" class="matters" colspan="1" style="width: 14px">
                :</td>
            <td align="left" class="matters" colspan="1">
                <asp:DropDownList id="ddlterm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlterm_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px">
                Grade</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 159px">
                <asp:DropDownList id="ddlgrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlgrade_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left" class="matters" colspan="1">
                Subject</td>
            <td align="left" class="matters" colspan="1" style="width: 14px">
                :</td>
            <td align="left" class="matters" colspan="1">
                &nbsp;<asp:DropDownList id="ddlsubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlsubject_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px">
                Group</td>
            <td align="left" class="matters" style="font-size: 9pt; width: 15px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 159px">
                <asp:DropDownList id="ddlgroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlgroup_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left" class="matters" colspan="1">
                Syllabus</td>
            <td align="left" class="matters" colspan="1" style="width: 14px">
                :</td>
            <td align="left" class="matters" colspan="1">
                &nbsp;<asp:TextBox id="txtSyllabus" runat="server" Width="160px" AutoPostBack="True" OnTextChanged="txtSyllabus_TextChanged" Enabled="False"></asp:TextBox>
                <asp:ImageButton id="ImageButton3" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetSyllabus()">
                </asp:ImageButton></td>
        </tr>
        <tr>
            <td align="left" class="subheader_img" colspan="8">
                <asp:Label id="Label6" runat="server" Text="Details.."></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px">
                Topic<asp:Label id="Label4" runat="server" ForeColor="Red" Text="*" Width="1px"></asp:Label></td>
            <td align="left" class="matters" style="width: 15px">
                :</td>
            <td align="left" class="matters" colspan="3">
                <asp:TextBox id="txtTopic" runat="server" Width="240px" AutoPostBack="True" OnTextChanged="txtTopic_TextChanged" Enabled="False"></asp:TextBox>
                <asp:ImageButton id="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetSubjects()">
                </asp:ImageButton>
                &nbsp;&nbsp;<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server"
                    ControlToValidate="txtTopic" Display="None" ErrorMessage="Select a Topic"
                    ForeColor="White" ValidationGroup="add"></asp:RequiredFieldValidator></td>
                     <td align="left" class="matters" colspan="1" style="height: 17px">
                Employee Name<asp:Label id="Label3" runat="server" ForeColor="Red" Text="*" Width="1px"></asp:Label></td>
            <td align="left" class="matters" colspan="1" style="width: 14px; height: 17px">
                :</td>
            <td align="left" class="matters" colspan="1" style="height: 17px">
                &nbsp;<asp:TextBox id="txtempname" runat="server" Width="181px" Enabled="False"></asp:TextBox>
                <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="Getemployee()">
                </asp:ImageButton>
                <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtempname"
                    Display="None" ErrorMessage="Select  Employee" ValidationGroup="add" ForeColor="White"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px">
                Objective</td>
            <td align="left" class="matters" style="width: 15px">
                :</td>
            <td align="left" class="matters" colspan="6">
                <asp:TextBox id="txtobjective" runat="server" Width="600px" SkinID="MultiText_Medium" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
       
        <tr>
            <td align="left" class="matters" style="width: 88px; height: 18px">
                Planned StartDate</td>
            <td align="left" class="matters" style="width: 15px; height: 18px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 159px; height: 18px">
                <asp:TextBox id="txtPlannedStDate" runat="server" Enabled="False">
                </asp:TextBox></td>
            <td align="left" class="matters" colspan="1" style="height: 18px">
                Planned EndDate</td>
            <td align="left" class="matters" colspan="1" style="width: 14px; height: 18px">
                :</td>
            <td align="left" class="matters" colspan="1" style="height: 18px">
                <asp:TextBox id="txtPlannedEndDate" runat="server" Enabled="False">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px; height: 10px">
                Actual
                Start Date</td>
            <td align="left" class="matters" style="width: 15px; height: 10px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 159px; height: 10px">
                &nbsp;<asp:TextBox id="txtFromDate" runat="server" Width="117px"></asp:TextBox><asp:ImageButton
                    id="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
            <td align="left" class="matters" colspan="1" style="height: 10px">
                Actual End Date</td>
            <td align="left" class="matters" colspan="1" style="width: 14px; height: 10px">
                :</td>
            <td align="left" class="matters" colspan="1" style="height: 10px">
                <asp:TextBox id="txtToDate" runat="server" Width="117px">
                </asp:TextBox><asp:ImageButton id="imgCalendar2" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>&nbsp;
                <asp:CompareValidator id="CompareValidator1" runat="server" ControlToCompare="txtFromDate"
                    ControlToValidate="txtToDate" Display="None" ErrorMessage="End date is less than start date"
                    ForeColor="White" Operator="GreaterThanEqual" ValidationGroup="add">
                </asp:CompareValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px; height: 17px">
                Lessons Taken<asp:Label id="Label2" runat="server" ForeColor="Red" Text="*" Width="1px"></asp:Label></td>
            <td align="left" class="matters" style="width: 15px; height: 17px">
                :</td>
            <td align="left" class="matters" colspan="3" style="width: 159px; height: 17px">
                &nbsp;<asp:TextBox id="txtHrs" runat="server" Width="55px"></asp:TextBox>
                <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtHrs"
                    Display="None" ErrorMessage="Enter Hours" ForeColor="White" ValidationGroup="add"
                    Width="80px">
                </asp:RequiredFieldValidator></td>
           
        </tr>
        <tr>
            <td class="matters" colspan="10">
              <asp:GridView ID="gvLesson" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" BorderColor="#1b80b6" BorderStyle="None">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                   
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLesId" runat="server" Text='<%# Bind("LES_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField >
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Width="150px"  Text='<%# Bind("LES_DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField >
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtComments" TextMode="MultiLine" SkinID="MultiText_Medium" runat="server" Width="600px" ></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                  <asp:TemplateField Visible="false" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblSylId"  runat="server" Width="600px" ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False"  />
                                <EditRowStyle Wrap="False" />
                              
                            </asp:GridView>
            </td>
           
        </tr>
        <tr>
            <td align="center" class="matters" colspan="8">
                &nbsp;&nbsp;
                <asp:Button id="btnSave" runat="server" CssClass="button"
                    Text="Save" Width="46px" OnClick="btnSave_Click" />&nbsp;
                <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                     Text="Cancel" />
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgCalendar1"
        TargetControlID="txtFromDate" Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="imgCalendar2"
        TargetControlID="txtToDate" Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField id="h_SyllabusId" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_TopicID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_EMP_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_ACD_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_TRM_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_SYT_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_Completed" runat="server">
    </asp:HiddenField>
    &nbsp;
    &nbsp;&nbsp;
    <asp:HiddenField id="h_GRD_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_GRP_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_SUBJ_ID" runat="server">
    </asp:HiddenField>
  
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
        TargetControlID="CompareValidator1">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
        TargetControlID="RequiredFieldValidator1">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server"
        TargetControlID="RequiredFieldValidator2">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server"
        TargetControlID="RequiredFieldValidator2">
    </ajaxToolkit:ValidatorCalloutExtender>
    <br />
    <br />
</asp:Content>

