<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSMARTQuestions_M.aspx.vb" Inherits="Curriculum_clmSMARTQuestions_M" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script type="text/javascript">    
    

  function change_chk_state(src)
         {
               var chk_state=(src.checked);
               for(i=0; i<document.forms[0].elements.length; i++)
               {
                if (document.forms[0].elements[i].type=='checkbox')
                    {
                    document.forms[0].elements[i].checked=chk_state;
                    }
               }
          }     
        function client_OnTreeNodeChecked( )
        { 
                var obj = window.event.srcElement;
                var treeNodeFound = false;
                var checkedState;
                if (obj.tagName == "INPUT" && obj.type == "checkbox") {
//                //
//                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
//                change_chk_state(obj); }
//                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                    } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                    if (numTables >= 1)
                    {
                        for (i=0; i < numTables; i++)
                        {
                            if (tables[i] == obj)
                            {
                                treeNodeFound = true;
                                i++;
                                if (i == numTables)
                                    {
                                    return;
                                    }
                            }
                            if (treeNodeFound == true)
                            {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel)
                                {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                                }
                            else
                                {
                                return;
                                }
                            }
                        }
                    }
                }
        }
        </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> S.M.A.R.T Question Setup
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left"  >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td >
                <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                   
                     
                    <tr>
                        <td align="left">
                           <span class="field-label">  Academic Year</span></td>
                       
                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                           <span class="field-label">  Grade</span></td>
                       
                        <td  align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                          <td align="left">
                           <span class="field-label">  Target Setup</span></td>
                        
                        <td   align="left">
                            <asp:DropDownList ID="ddlTarget" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                           <td  align="left">
                          <asp:LinkButton Text="Copy To Grades" id="lnkCopy" runat="server" /> 
                       
                      
                        </td>
                    </tr>
                
       
                    <tr id="trGrid2" runat="server">
                        <td colspan="7" align="center">
                            <asp:GridView ID="gvQuestions" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                 PageSize="20">
                                <RowStyle  CssClass="griditem"  />
                                <EmptyDataRowStyle  />
                                <Columns>
                                   
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTgdId" runat="server" Text='<%# Bind("TGD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("TGD_SBG_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="txttype" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTextType" runat="server" Text='<%# Bind("TGD_TEXTTYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Slno">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtSlno" runat="server" Width="50px" Text='<%# Bind("TGD_SLNO") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Subject" >
                                        <ItemTemplate>
                                         <asp:DropDownList ID="ddlSubject" runat="server"></asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Question">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtQuestion" runat="server" TextMode="MultiLine" SkinID="MultiText" Width="300px" Text='<%# Bind("TGD_DESCR") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Answer Options">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOption" runat="server" Width="50px" Text='<%# Bind("TGD_OPTIONS") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Min.Answers">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtMinAnswer" runat="server" Width="50px" Text='<%# Bind("TGD_MINANSWER") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Pre-Text">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPretext" runat="server" Width="50px" Text='<%# Bind("TGD_PRETEXT") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Mandatory">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkMandatory" runat="server" Width="50px" Checked='<%# Bind("TGD_bMANDATORY") %>'></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Target Score">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTargetScore" runat="server" Width="50px" Checked='<%# Bind("TGD_bTARGET") %>'></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                     </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Text Type" >
                                        <ItemTemplate>
                                          <asp:DropDownList ID="ddlTextType"  runat="server"  >
                                          <asp:ListItem Text="Small" Value="small" ></asp:ListItem>
                                          <asp:ListItem Text="Medium" Value="medium" ></asp:ListItem>
                                          <asp:ListItem Text="Large" Value="large"></asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="index" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle  />
                                <HeaderStyle  />
                                <EditRowStyle  />
                                <AlternatingRowStyle CssClass="griditem_alternative"  />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trGrid3" runat="server">
                        <td colspan="12" align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                                 <asp:Button ID="btnPreview" runat="server" CssClass="button" Text="Preview" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>
    
      <asp:Panel ID="pnlAssmt" runat="server"  CssClass=" panel-cover" Width="100%" >
                                   <table>
                                   <tr>
                                   <td class="matters">
                                    <asp:CheckBoxList 
                                    id="lstGrade" runat="server"  Width="193px" RepeatLayout="Flow" >
                                    </asp:CheckBoxList>
                                    </td>
                                    </tr>
                                    
                                    <tr>
                                    <td> <asp:Button ID="btnCopy" Text="Copy" runat="server" CssClass="button" />
                    <asp:Button ID="btnCancelAssmt" Text="Cancel" runat="server" CssClass="button" /></td>
                                    </tr>
                                    
                    </table>                           
                       </asp:Panel>
                       
                       
     <ajaxToolkit:ModalPopupExtender ID="MPOI" runat="server" BackgroundCssClass="modalBackground"
                        DropShadow="true" PopupControlID="pnlAssmt" CancelControlID="btnCancelAssmt"
                        RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="ddlAcademicYear">
                    </ajaxToolkit:ModalPopupExtender>
    <asp:HiddenField ID="H_ROWS" runat="server" />

</asp:Content>

