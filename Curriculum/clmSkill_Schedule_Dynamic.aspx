<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSkill_Schedule_Dynamic.aspx.vb" Inherits="clmSkillSchedule_Dynamic" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc2" %>

<%@ Register Src="../UserControls/usrSkillScheduler.ascx" TagName="usrSkillScheduler"  TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 
   <script language="javascript" type="text/javascript">
   function SetMaxMark(ddlGradeSlab, txtMaxMark)
  {
    var vGradeSlabVal = ddlGradeSlab.value;
    var maxMark = vGradeSlabVal.split('___');
    if (isNaN(maxMark[1]))
     document.getElementById(txtMaxMark).value= "";
    else
     document.getElementById(txtMaxMark).value= parseFloat(maxMark[1]).toFixed(2);
  }
      function GetSUBJECT()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs =document.getElementById('<%=h_GRD_IDs.ClientID %>').value;
            if (GRD_IDs== '')
            {
                alert('Please select Grade')
                return false;
            }
            result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBJECT&GRD_IDs=" + GRD_IDs ,"", sFeatures)            
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_SBM_ID.ClientID %>').value = result;//NameandCode[0];
            }
            else
            {
                return false;
            }
        }

   </script>

    
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           <asp:label id="ltLabel" runat="server" Text="Assessment Schedule"></asp:label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"  cellspacing="0" width="100%">
        <tr>
            <td align="left"  >
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
                </span>
            </td>
        </tr>
        <tr >
            <td align="center" class="text-danger font-small" valign="middle">
                Fields Marked with ( * ) are mandatory
            </td>
        </tr>
        <tr>
            <td >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr>
                        <td align="left" colspan="9" class="subheader_img">
                            </td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label">  Academic Year</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left">
                          <span class="field-label">   Term</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList id="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                           <span class="field-label">  Assessment</span></td>
                        
                        <td align="left" colspan="3">
                            <asp:DropDownList id="ddlActivity" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlActivity_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                           <span class="field-label">  Assessment Details</span></td>
                        
                        <td align="left" colspan="3">
                            <asp:DropDownList id="ddlACTIVITYDET" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id ="20" >
                        <td align="left" >
                           <span class="field-label">  Grade</span></td>
                        
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                           <span class="field-label">  Subject</span></td>
                      
                        <td align="left" colspan="3">
                           <%-- <uc2:usrTreeView ID="usrSubjects" runat="server" />--%>
                            <asp:DropDownList ID="ddlSubjects" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label">  Start Date</span></td>
                       
                        <td align="left" >
                         <asp:TextBox id="txtDate" runat="server" >
                            </asp:TextBox> <asp:ImageButton id="imgDate" runat="server" ImageUrl="~/Images/calendar.gif">
                            </asp:ImageButton>
                        </td>
                        <td align="left" ><span class="field-label"> Grading Slab</span></td>
                       
                        <td align="left" > <asp:DropDownList ID="ddlGradeSlab" runat="server" >
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                          <span class="field-label">   Min Pass Mark</span></td>
                      
                        <td align="left" >
                            <asp:TextBox ID="txtMin" runat="server"></asp:TextBox></td>
                        <td align="left">
                          <span class="field-label">   Max Mark</span></td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtMax" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" >
                            <asp:Button ID="btnFilter" runat="server" CssClass="button" Text="Apply Filter" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <uc1:usrSkillScheduler ID="usrSkillScheduler1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnAdd_Click" Text="Add" />&nbsp;<asp:Button id="btnEdit" runat="server"
                        CausesValidation="False" CssClass="button" onclick="btnEdit_Click" Text="Edit" />
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" /><asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
        </tr>
    </table>
     <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDate"
        TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField  id="h_SBM_ID" runat="server"/>
    <asp:HiddenField id="h_GRD_IDs" runat="server"/>
    <asp:HiddenField id="h_PARENT_ID" runat="server"/>
    <asp:HiddenField id="h_CAS_ID" runat="server"/>

                
        </div>
        </div>
    </div>

</asp:Content>