<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentComments.aspx.vb" Inherits="Curriculum_rptStudentComments" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function confirm_delete() {
            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }//GRADE CMTSCAT
        function getcomments(ctlid, code, header, stuid, rsmid) {
            //  alert(ctlid); 
            var sFeatures;
            sFeatures = "dialogWidth: 530px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var result;
            var parentid;
            var varray = new Array();
            var str;
            if (code == 'ALLGENCMTS') {
                var url = "clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid
                //result = window.showModalDialog("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "", sFeatures);
                document.getElementById('<%=hfctlid.ClientID%>').value = ctlid;
                var oWnd = radopen(url, "pop_rel");
                //if (result != '' && result != undefined) {
                //    str = document.getElementById(ctlid).value;
                //    document.getElementById(ctlid).value = str + result.replace("/ap/", "'");
                //    //document.getElementById(ctlid.split(';')[1]).value=varray[0];
                //}
                //else {
                //    return false;
                //}
            }

        }

        function OnClientClose(oWnd, args) {
            //alert(0);
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Comment.split('||');
                //alert(NameandCode);
                //str = document.getElementById(ctlid).value;
                str = document.getElementById('<%=hfctlid.ClientID%>').value              
                document.getElementById(document.getElementById('<%=hfctlid.ClientID%>').value).value = document.getElementById(document.getElementById('<%=hfctlid.ClientID%>').value).value +"\n" + NameandCode//.replace("/ap/", "'");
                document.getElementById('<%= txtCommenttxt.ClientID%>').value = NameandCode//.replace("/ap/", "'");               
                __doPostBack('<%= txtCommenttxt.ClientID%>', 'TextChanged');

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function getcategory(ctlid, code) {
            //            alert(ctlid); 
            //            alert(ctlid.split(';')[0])
            //            alert(code)            
            var sFeatures;
            sFeatures = "dialogWidth: 429px; ";
            sFeatures += "dialogHeight: 345px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var result;
            var parentid;
            var varray = new Array();
            if (code == 'CMTSCAT') {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=" + code + "", "", sFeatures);

                if (result != '' && result != undefined) {
                    varray = result.split('_');
                    document.getElementById(ctlid.split(';')[0]).value = varray[3];
                    document.getElementById(ctlid.split(';')[1]).value = varray[0];
                }
                else {
                    return false;
                }
            }
            else {

                parentid = document.getElementById(ctlid.split(';')[1]).value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=" + code + "&CAT_ID=" + parentid + "", "", sFeatures);

                if (result != '' && result != undefined) {
                    document.getElementById(ctlid.split(';')[0]).value = result.split('___')[1];
                }
                else {
                    return false;
                }
            }

        }


        function GetPopUp(id) {
            //alert(id);
            var sFeatures;
            sFeatures = "dialogWidth: 429px; ";
            sFeatures += "dialogHeight: 345px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (id == 1)//Accadamic Year
            {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=ACCYEAR", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_ACD_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }



            else if (id == 3)//Group
            {
                var GradeGrpId
                GradeGrpId = document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBGROUP&SBM_IDs=" + GradeGrpId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_GRP_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }


            else if (id == 5)//Subjejct Grades
            {
                var GradeId
                GradeId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                //alert(GradeId + 'ddd');
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJGRD&GRDID=" + GradeId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_SBJ_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }

            else if (id == 7)//Report Schedule
            {
                var RPTId
                RPTId = document.getElementById('<%=H_RPT_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSCH&RPTID=" + RPTId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_SCH_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }
            else if (id == 8)//Student Info
            {
                var GradeId
                GradeId = document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs=" + GradeId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }


            else if (id == 12) {

                var GrdId
                var AcdId

                GrdId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                AcdId = document.getElementById('<%=H_ACD_ID.ClientID %>').value;
                <%--     result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=GROUP_GRADE&GRADE_ID='" + GrdId + "'&ACD_ID=" + AcdId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    NameandCode = result.split('___');
                    document.getElementById('<%=txtGroup.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%=H_GRP_ID.ClientID %>').value = NameandCode[0];
                }
                else {
                    return false;
                }--%>
                var oWnd = radopen("clmPopupForm.aspx?multiselect=false&ID=GROUP_GRADE&GRADE_ID='" + GrdId + "'&ACD_ID=" + AcdId + "", "", "pop_document");
            }
            else if (id == 13) {
                var GrdId
                var AcdId

                GrdId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                AcdId = document.getElementById('<%=H_ACD_ID.ClientID %>').value;

                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SECTION&GRADE_ID=" + GrdId + "&ACD_ID=" + AcdId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    NameandCode = result.split('___');

                    document.getElementById('<%=H_SCT_ID.ClientID %>').value = NameandCode[0];
                }
                else {
                    return false;
                }
            }
    if (id == 14)//Student Info
    {
        var GradeId;
        var acdId;
        var sgrId;
        var sctid;
        //var RepSch;
        //var RepHeader;
        GradeId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
        acdId = document.getElementById('<%=H_ACD_ID.ClientID %>').value;
        sgrId = document.getElementById('<%=H_GRP_ID.ClientID %>').value;
        sctid = document.getElementById('<%=H_SCT_ID.ClientID %>').value;
        //sgr_Descr=document.getElementById('<%=H_GRP_ID.ClientID %>').value;
        //RepSch=document.getElementById('<%=H_SCH_ID.ClientID %>').value;
        //RepHeader=''//document.getElementById('<%=H_SETUP.ClientID %>').value;
        // alert(acdId);

        result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT_COMMENTS&GETSTU_NO=true&GRD_IDs=" + GradeId + "&ACD_IDs=" + acdId + "&SCT_IDs=" + sctid + "&GRP_IDs=" + sgrId + "", "", sFeatures)
              <%--  if (result != '' && result != undefined) {
                    //alert(document.getElementById('<%=lblStudNo.ClientID %>').value);
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result.split('___')[0];
                    document.getElementById('<%=txtStudName.ClientID %>').value = result.split('___')[1];
                    document.getElementById('<%=lblStudNo.ClientID %>').innerHTML = result.split('___')[2];
                }
                else {
                    return false;
                }--%>


    }
        }//CMTSCAT



        function GetStudentValues() {
            var ColsId;
            var cellcnt = 0;
            ColsId = document.getElementById('<%=H_CommentCOLS.ClientID %>').value;
            //alert(ColsId);
            var araay = new Array();
            //alert(txt1);
            for (i = 0; i < txt1.length; i++) {

                araay[i] = new Array(ColsId)
                //alert(txt1.length);
                for (j = 0; j < ColsId; j++) {
                    var ArrName = "txt" + (Number(j) + 1);
                    //alert(ArrName);
                    ArrName = eval(ArrName);
                    try {
                        var strtext;
                        strtext = document.getElementById(ArrName[i]).value.replace(/,/gi, '~');
                        // alert(strtext);
                        araay[i][j] = strtext.replace(/,/gi, '~');
                    }
                    catch (err)
                    { }
                    //alert(araay[i][j]);
                    cellcnt = cellcnt + 1;
                }
            }
            //alert(araay.length);
            var strvalue = '';
            //Sending Array values into ServerSide
            for (var i = 0; i < araay.length; i++) {
                strvalue = strvalue + araay[i] + '|';
            }
            //alert(strvalue);   
            document.getElementById('<%=H_CommentCOLS.ClientID %>').value = strvalue;
        }


    </script>
    <script>
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        <%--       function OnClientClose(oWnd, args) {
               var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById('<%=txtGroup.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=H_GRP_ID.ClientID %>').value = NameandCode[0];
            }
          
        }--%>
    </script>


    <%--  <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_document" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow></Windows>
    </telerik:RadWindowManager>--%>
    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Form Tutor Comments"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error"></asp:ValidationSummary>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcdID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report Schedule</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlRepSch" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRepSch_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr runat="server" id="trGroup">
                        <td align="left"><span class="field-label">Group</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtGroup" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgGroup" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetPopUp(12);return false"></asp:ImageButton></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Section</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td></td>
                        <td>
                            <asp:DropDownList ID="ddlMainHeader" runat="server" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="LnkSearch" runat="server" OnClientClick="javascript:return false;">Search Students</asp:LinkButton></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnView" runat="server" CssClass="button" OnClick="btnView_Click1"
                                Text="View Students" />
                            <asp:Button ID="btnCanceldata" runat="server" CssClass="button"
                                Text="Cancel" OnClick="btnCanceldata_Click" /></td>
                    </tr>
                    <tr runat="server" id="trSave1">
                        <td align="right" colspan="6">
                            <asp:Button ID="btnSave1" runat="server" CssClass="button"
                                Text="Save & Next" ValidationGroup="MAINERROR" OnClick="btnSave1_Click" OnClientClick="GetStudentValues()" />
                            <asp:Button ID="btnCancel1" runat="server" CausesValidation="False"
                                CssClass="button" Text="Cancel" OnClick="btnCancel1_Click" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3">
                            <asp:Panel ID="Panel1" runat="server">
                                <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbStudent" Width="100%"
                                    MarkFirstMatch="true" EnableLoadOnDemand="false" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains"
                                    HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                    OnDataBound="cmbStudent_DataBound" OnItemDataBound="cmbStudent_ItemDataBound"
                                    EmptyMessage="Start typing to search...">
                                    <HeaderTemplate>
                                        <ul>
                                            <li class="col1">Student Code</li>
                                            <li class="col2">Student Name</li>
                                        </ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <ul>
                                            <li class="col1">
                                                <%# DataBinder.Eval(Container.DataItem, "stu_no")%></li>
                                            <li class="col2">
                                                <%# DataBinder.Eval(Container.DataItem, "StudentName")%></li>
                                        </ul>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                                        items
                                    </FooterTemplate>
                                </telerik:RadComboBox>
                            </asp:Panel>

                            <telerik:RadSpell ID="RadSpell1" ButtonType="LinkButton" runat="server" IsClientID="true" SupportedLanguages="en-US,English"></telerik:RadSpell>
                            <br />
                            <asp:LinkButton ID="lbtnKeys" runat="server" Visible="false" OnClientClick="javascript:return false;">Header Keys</asp:LinkButton>
                            <br />
                            <asp:Label
                                ID="lblPages" runat="server"></asp:Label><br />
                            <asp:DataList ID="dlPages" RepeatLayout="Flow" runat="server" RepeatColumns="10" >
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" Text='<%# Bind("pageno") %>' ID="lblPageNo" OnClick="lblPageNo_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">


                            <asp:GridView ID="gvStudents" runat="server" 
                                EmptyDataText="No Data Found" Width="100%" AutoGenerateColumns="False"  EnableTheming="True" OnPageIndexChanging="gvStudents_PageIndexChanging" CssClass="table table-bordered table-row" OnRowDataBound="gvStudents_RowDataBound">
                                <HeaderStyle  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:TextBox ID="txtCommenttxt" runat="server" Visible="false" OnTextChanged="txtCommenttxt_TextChanged"></asp:TextBox>



                        </td>
                    </tr>

                    <tr align="center">
                        <td align="center" colspan="4"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: left"></td>
                    </tr>
                    <tr runat="server" id="trSave">

                        <td align="right" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                            <asp:Button ID="btnSaveDetails" runat="server" CssClass="button"
                                OnClientClick="GetStudentValues()" Text="Save " ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button"
                                Text="Save & Next" ValidationGroup="MAINERROR" OnClick="btnSave1_Click" OnClientClick="GetStudentValues()" />

                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>

                <div id="popup" runat="server" class="panel-cover" style="display: none; text-align: left; vertical-align: middle; margin-top: -2px; margin-left: -3px; margin-right: 1px;">
                    <div class="title-bg">Header Keys</div>
                    <asp:Panel ID="PopupMenu" ScrollBars="Vertical" runat="server" CssClass="modalPopup1" Height="25%" Width="100%">
                        <asp:Literal ID="ltProcess" runat="server"></asp:Literal>
                    </asp:Panel>
                </div>
                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server"
                    TargetControlID="lbtnKeys"
                    PopupControlID="popup"
                    HoverCssClass="popupHover"
                    PopupPosition="Center"
                    OffsetX="0"
                    OffsetY="20"
                    PopDelay="50" />

                <asp:HiddenField ID="H_ACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_GRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_GRP_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SBJ_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_RPT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SCH_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_STU_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SETUP" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_RPF_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SCT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_CommentCOLS" runat="server"></asp:HiddenField>
                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                    CollapseControlID="LnkSearch" CollapsedSize="0" CollapsedText="Search Students"
                    ExpandControlID="LnkSearch" ExpandedText="Hide Search" TargetControlID="Panel1"
                    TextLabelID="LnkSearch" Collapsed="True">
                </ajaxToolkit:CollapsiblePanelExtender>
                <br />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            function UpdateItemCountField(sender, args) {
                //Set the footer text.
                sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
            }
            function Combo_OnClientItemsRequesting(sender, eventArgs) {
                var combo = sender;
                ComboText = combo.get_text();
                ComboInput = combo.get_element().getElementsByTagName('input')[0];
                ComboInput.focus = function () { this.value = ComboText };

                if (ComboText != '') {
                    window.setTimeout(TrapBlankCombo, 100);
                };
            }

            function TrapBlankCombo() {
                if (ComboInput) {
                    if (ComboInput.value == '') {
                        ComboInput.value = ComboText;
                    }
                    else {
                        window.setTimeout(TrapBlankCombo, 100);
                    };
                };
            }

        </script>
    </telerik:RadScriptBlock>


    <asp:HiddenField ID="hfctlid" runat="server" />
</asp:Content>

