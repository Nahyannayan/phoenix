Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
'Imports Telerik.QuickStart
Partial Class Curriculum_clmComments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            ViewState("datamode") = "add"
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
               
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330002") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("datamode") = "add" Then
                        H_CMT_ID.Value = 0
                    Else

                    End If

                    ddlReport = PopulateReports(ddlReport)
                    If ddlReport.SelectedValue <> "" Then
                        FillGrade()
                        FillSubjects()
                        gridbind()
                        H_CAT_ID.Value = 1
                        GetReportHeader(chkHeader)
                    End If
                    PopulateGrades(ddlGrade0)
                    chkCatByGrade.Visible = False
                    ddlGrade0.Visible = False
                    BindCategory()

                    If radCatBySubject.Checked = True Then
                        trcatGrade.Visible = False
                        chkGrade.Checked = True
                    Else
                        trcatGrade.Visible = True
                        chkGrade.Checked = False
                    End If
                    'disable the control buttons based on the rights

                    gvComments.Attributes.Add("bordercolor", "#1b80b6")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private methods"


    Private Sub PopulateGrades(ByVal ddlGrade As DropDownList)
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM VW_GRADE_M ORDER BY GRD_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""
         
            Dim ds As New DataSet
            Dim strSubject As String = ""
            Dim strHeader As String = ""

            If radGeneralComments.Checked = False Then
                If ddlGrade.Items.Count >= 1 Then
                    Dim GRD_ID As String = ddlGrade.SelectedItem.Value
                    str_Sql = " SELECT DISTINCT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CAT_DESC,RSD_HEADER,CMT_COMMENTS,SBG.SBG_DESCR,SBG.SBG_GRD_ID " & _
                              " FROM SUBJECTS_GRADE_S SBG INNER JOIN ACT.COMMENTS_M CMT ON  CMT.CMT_SBG_ID=SBG.SBG_ID " & _
                              " INNER JOIN ACT.CATEGORY_M ON CMT_CAT_ID=CAT_ID" & _
                              " INNER JOIN RPT.REPORT_SETUP_D ON CMT_RSD_ID=RSD_ID AND RSD_RSM_ID=" + ddlReport.SelectedValue.ToString & _
                              " WHERE CMT_GRD_ID='" & GRD_ID & "'"
                    strSubject = getSubjects()
                    If strSubject <> "" Then
                        str_Sql += "AND CMT_SBG_ID IN (" & strSubject & ")"
                    End If

                    strHeader = getHeaders()

                    If strHeader <> "" Then
                        str_Sql += " AND CMT_RSD_ID IN(" & strHeader & ")"
                    End If
                    gvComments.Columns(4).Visible = True
                    gvComments.Columns(5).Visible = True
                Else
                    str_Sql = " SELECT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CMT_COMMENTS,SBG.SBG_DESCR,SBG.SBG_GRD_ID " & _
                              " FROM SUBJECTS_GRADE_S SBG INNER JOIN ACT.COMMENTS_M CMT ON  CMT.CMT_SBG_ID=SBG.SBG_ID " & _
                              " WHERE CMT_GRD_ID='0'"
                End If
            Else

                str_Sql = " SELECT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CAT_DESC,'' AS RSD_HEADER,CMT_COMMENTS,'' AS SBG_DESCR,'' AS SBG_GRD_ID " & _
                          " FROM ACT.COMMENTS_M CMT INNER JOIN ACT.CATEGORY_M ON CMT_CAT_ID=CAT_ID WHERE CMT_bGENCOMMENTS=1 "
                gvComments.Columns(2).Visible = False
                gvComments.Columns(3).Visible = False
                gvComments.Columns(4).Visible = False
                gvComments.Columns(5).Visible = False
            End If

            If ddlCategory.SelectedValue.ToString <> "0" Then
                str_Sql += " AND CMT_CAT_ID=" + ddlCategory.SelectedValue.ToString
            End If
            'GetSelectedGrades
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & " ORDER BY 1")

            If ds.Tables(0).Rows.Count > 0 Then
                gvComments.DataSource = ds.Tables(0)
                gvComments.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvComments.DataSource = ds.Tables(0)
                Try
                    gvComments.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvComments.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvComments.Rows(0).Cells.Clear()
                gvComments.Rows(0).Cells.Add(New TableCell)
                gvComments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvComments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvComments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub FillGrade()

        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_Sql As String = "SELECT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY  " _
                       & " FROM RPT.REPORTSETUP_GRADE_S INNER JOIN " _
                       & " VW_GRADE_M ON RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID = VW_GRADE_M.GRD_ID " _
                       & " WHERE RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID='" & ddlReport.SelectedValue & "' " _
                       & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "DESCR1"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count >= 1 Then
            ddlGrade.SelectedIndex = 0
            Try
                FillSubjects()
                gridbind()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Sub FillSubjects()
        'ddlSubject.Items.Clear()
        'Dim CLMFUNCTIONS As New currFunctions
        'CLMFUNCTIONS.PopulateGradeSubjects(ddlSubject, ddlGrade, Session("sBsuid"))
        'If ddlSubject.Items.Count >= 1 Then
        '    ddlSubjectAll.Items.Clear()
        '    Dim lstItem As New ListItem
        '    lstItem.Text = "SELECT ALL"
        '    lstItem.Value = "0"
        '    ddlSubjectAll.Items.Insert(0, lstItem)
        'Else
        '    ddlSubjectAll.Items.Clear()
        'End If

        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID='" + Session("Current_ACD_ID") + "'"


             
        str_query += " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
       
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Private Function isCommentsExists() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "select count(CMT_ID) from ACT.COMMENTS_M WHERE CMT_COMMENTS='" + txtComments.Text + "' AND CMT_ID<>" + H_CMT_ID.Value.ToString
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If sbm = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Dim IntMode As Integer
        Dim INTrESULT As Integer
        Dim intGenComments As Integer

        If (ViewState("datamode") = "add") Then
            IntMode = 0
            H_CMT_ID.Value = 0
        ElseIf (ViewState("datamode") = "edit") Then
            IntMode = 1
        Else
            IntMode = 2
        End If

        If ddlCategory.SelectedValue.ToString <> "0" Then
            H_CAT_ID.Value = ddlCategory.SelectedValue.ToString
        End If
        If radGeneralComments.Checked = True Then
            'If General Comments
            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    If ViewState("datamode") = "edit" Then
                        'UtilityObj.InsertAuditdetails(transaction, "edit", "ACT.COMMENTS_M", "CMT_ID", "CMT_ID", "CMT_ID=" + H_CMT_ID.Value.ToString)
                    ElseIf ViewState("datamode") = "delete" Then
                        'UtilityObj.InsertAuditdetails(transaction, "delete", "ACT.COMMENTS_M", "CMT_ID", "CMT_ID", "CMT_ID=" + H_CMT_ID.Value.ToString)
                    End If
                    'Dim str_query As String = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + ",0,'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",'" + txtComments.Text.ToString + "'," & _
                    '                          "" + chkGen.Checked.ToString + "," & IntMode.ToString & "" & _
                    '                          ""
                    Dim str_query As String
                    If chkAOLcomment.Checked Then
                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + ",0,'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",N'" + txtComments.Text.Replace("'", "''") + "'," & _
                                              "0,0,-1,'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & ""
                    ElseIf radGeneralComments.Checked Then
                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + ",0,'" + ddlGrade0.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",N'" + txtComments.Text.Replace("'", "''") + "'," & _
                                              "0,0,0,'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & ""
                    Else
                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + ",0,'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",N'" + txtComments.Text.Replace("'", "''") + "'," & _
                                              "0,0,0,'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & ""
                    End If
                    INTrESULT = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "CMT_ID(" + H_CMT_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    transaction.Commit()
                    lblError.Text = "Record Saved Successfully"

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblError.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End Using
        Else ' If Not General Comments
            For Each lstItem As ListItem In ddlSubject.Items
                If lstItem.Selected Then ' 

                    For Each lstHeaderItem As ListItem In chkHeader.Items
                        If lstHeaderItem.Selected Then

                            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                                transaction = conn.BeginTransaction("SampleTransaction")
                                Try
                                    If ViewState("datamode") = "edit" Then
                                        'UtilityObj.InsertAuditdetails(transaction, "edit", "ACT.COMMENTS_M", "CMT_ID", "CMT_ID", "CMT_ID=" + H_CMT_ID.Value.ToString)
                                    ElseIf ViewState("datamode") = "delete" Then
                                        'UtilityObj.InsertAuditdetails(transaction, "delete", "ACT.COMMENTS_M", "CMT_ID", "CMT_ID", "CMT_ID=" + H_CMT_ID.Value.ToString)
                                    End If

                                    Dim str_query As String
                                    If chkAOLcomment.Checked Then
                                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + "," & lstItem.Value & ",'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",'" + txtComments.Text.ToString + "'," & _
                                        "" + ddlReport.SelectedValue + ",0,-1,'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & ""
                                    Else
                                        str_query = "exec ACT.saveCOMMENTS_M " + H_CMT_ID.Value + "," & lstItem.Value & ",'" + ddlGrade.SelectedValue.ToString + "'," + H_CAT_ID.Value + ",N'" + txtComments.Text.ToString + "'," & _
                                                      "" + ddlReport.SelectedValue + ",0," + lstHeaderItem.Value + ",'" + Session("sBsuid") + "'," + radGeneralComments.Checked.ToString + "," & IntMode.ToString & "" & _
                                                      ""
                                    End If
                        INTrESULT = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "CMT_ID(" + H_CMT_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        transaction.Commit()
                        lblError.Text = "Record Saved Successfully"

                                Catch myex As ArgumentException
                            transaction.Rollback()
                            lblError.Text = myex.Message
                            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                        Catch ex As Exception
                            transaction.Rollback()
                            lblError.Text = "Record could not be Saved"
                            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                        End Try
                            End Using

                        End If ' Heder Item ..If Ending 

                    Next '' Header Item Loop Ending 
                End If
            Next  'Subjects Loop Ending 
        End If

    End Sub

    Function getSubjects() As String
        Dim i As Integer
        Dim str As String = ""

        For i = 0 To ddlSubject.Items.Count - 1
            If ddlSubject.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += ddlSubject.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function

    Function getHeaders() As String
        Dim i As Integer
        Dim str As String = ""

        For i = 0 To chkHeader.Items.Count - 1
            If chkHeader.Items(i).Selected = True Then
                If str <> "" Then
                    str += ","
                End If
                str += chkHeader.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function


    Sub BindCategory()
        ddlCategory.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim strSubject As String = ""
        Dim ds As DataSet
        If radCatBySubject.Checked = True Then
            
            strSubject = getSubjects()

            Select Case Session("sbsuid")
                Case "131001", "131002", "121012", "121013", "121014", "121009", "115002", "123004", "141001", "151001"
                    str_query = "SELECT CAT_ID,CAT_DESC FROM ACT.CATEGORY_M WHERE " _
                      & " ( CAT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "')" _
                      & " AND CAT_BSU_ID='" + Session("SBSUID") + "'"
                Case Else
                    str_query = "SELECT CAT_ID,CAT_DESC FROM ACT.CATEGORY_M WHERE " _
                      & " ( CAT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' OR CAT_GRD_ID IS NULL)" _
                      & " AND CAT_BSU_ID='" + Session("SBSUID") + "'"
                    If strSubject <> "" Then
                        str_query += " AND ( CAT_ID IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE CMT_SBG_ID IN(" + strSubject + ") " _
                                  & " AND CMT_RSM_ID=" + ddlReport.SelectedValue.ToString + ") "

                        'INCLUDE NEW CATEGORIES ALSO
                        str_query += " OR CAT_ID NOT IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE CMT_BSU_ID='" + Session("SBSUID") + "')) "
                    Else
                        str_query += " AND (CAT_ID IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE  " _
                       & " CMT_RSM_ID=" + ddlReport.SelectedValue.ToString + ")"

                        str_query += " OR CAT_ID NOT IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE CMT_BSU_ID='" + Session("SBSUID") + "')) "
                    End If
            End Select
            str_query += " ORDER BY CAT_DESC"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "CAT_DESC"
            ddlCategory.DataValueField = "CAT_ID"
            ddlCategory.DataBind()

        Else
            str_query = "SELECT CAT_ID,CAT_DESC FROM ACT.CATEGORY_M WHERE " _
                    & " (CAT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' OR CAT_GRD_ID IS NULL)"
            str_query += " AND CAT_ID NOT IN(SELECT CMT_CAT_ID FROM ACT.COMMENTS_M WHERE  " _
            & " ISNULL(CMT_bGEnCOMMENTS,0)=0 AND CMT_BSU_ID='" + Session("SBSUID") + "') AND CAT_BSU_ID='" + Session("SBSUID") + "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "CAT_DESC"
            ddlCategory.DataValueField = "CAT_ID"
            ddlCategory.DataBind()
        End If

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlCategory.Items.Insert(0, li)
    End Sub
#End Region

    Private Function PopulateReports(ByVal ddlReports As DropDownList)
        ddlReports.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_Sql As String = "SELECT RSM_ID,RSM_BSU_ID ,RSM_DESCR FROM RPT.REPORT_SETUP_M WHERE RSM_BSU_ID='" & Session("sBsuId") & "' " _
                   & "  AND RSM_ACD_ID=" & Session("CURRENT_ACD_ID") & " ORDER BY RSM_DISPLAYORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlReports.DataSource = ds
        ddlReports.DataTextField = "RSM_DESCR"
        ddlReports.DataValueField = "RSM_ID"
        ddlReports.DataBind()

        Return ddlReports
    End Function

    Private Function GetReportHeader(ByVal ddlHeader As CheckBoxList) As CheckBoxList
        Try
            Dim strHeader As String = ""
            Dim strStubjects As String = ""

            Dim i As Integer
            For i = 0 To ddlSubject.Items.Count - 1
                If ddlSubject.Items(i).Selected = True Then
                    If strStubjects <> "" Then
                        strStubjects += ","
                    End If
                    strStubjects += ddlSubject.Items(i).Value
                End If
            Next

            If strStubjects = "" Then
                strStubjects = "0"
            End If

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet
            Dim strSQL As String = "SELECT RSD_ID,RSD_RSM_ID,RSD_RESULT,RSD_CSSCLASS,RSD_HEADER  FROM RPT.REPORT_SETUP_D " & _
                                   " WHERE RSD_RSM_ID=" & ddlReport.SelectedValue & " AND RSD_BDIRECTENTRY=1 AND RSD_bALLSUBJECTS='True' " & _
                                   " AND (RSD_SBG_ID IS NULL OR RSD_SBG_ID IN('" + strStubjects + "'))" & _
                                   " AND RSD_CSSCLASS='textboxmulti' ORDER BY RSD_DISPLAYORDER "
            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

            ddlHeader.DataSource = dsHeader
            ddlHeader.DataTextField = "RSD_HEADER"
            ddlHeader.DataValueField = "RSD_ID"
            ddlHeader.DataBind()

            'If ddlHeader.Items.Count >= 1 Then
            '    Dim lstItem As New ListItem
            '    lstItem.Text = "SELECT ALL"
            '    lstItem.Value = "0"

            '    chkHeaderALL.Items.Insert(0, lstItem)
            'Else
            '    chkHeaderALL.Items.Clear()
            'End If

            Return ddlHeader
        Catch ex As Exception

        End Try

    End Function

    Private Function CheckALL(ByVal ChkList As CheckBoxList)
        Try
            For Each LstItem As ListItem In ChkList.Items
                LstItem.Selected = True
            Next
        Catch ex As Exception

        End Try
    End Function
    Private Function UnCheckALL(ByVal ChkList As CheckBoxList)
        Try
            For Each LstItem As ListItem In ChkList.Items
                LstItem.Selected = False
            Next
        Catch ex As Exception

        End Try
    End Function

    Private Function GetHeaderIDs(ByVal SBGID As Integer, ByVal GRDID As String, ByVal RSMID As Integer)
        Try
            Dim strHeader As String = ""
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim dsHeader As DataSet
            Dim StrSQL As String = ""
            Dim strFilter As String = ""

            StrSQL = "SELECT CMT_RSD_ID " & _
                        "FROM ACT.COMMENTS_M  CMT " & _
                        " WHERE CMT_GRD_ID='" & GRDID & "'"

            If SBGID > 0 Then
                strFilter = " AND CMT_SBG_ID=" & SBGID & " "
            End If
            If RSMID > 0 Then
                strFilter = " AND CMT_RSM_ID=" & RSMID & ""
            End If
            'StrSQL += strFilter
            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL & strFilter)

            For Each DrROW As DataRow In dsHeader.Tables(0).Rows
                chkHeader.SelectedValue = DrROW.Item(0).ToString
            Next

        Catch ex As Exception

        End Try

    End Function


    Private Function PopulateReportSchedule(ByVal ddlReportSChedule As DropDownList, ByVal ddlReportsM As DropDownList)
        ddlReportSChedule.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_Sql As String = "SELECT RPF_ID ,RPF_ID ,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M "
        If ddlReportsM.SelectedValue <> "" Then
            str_Sql += " WHERE RPF_RSM_ID=" & ddlReportsM.SelectedValue & ""
        End If
        str_Sql += " ORDER BY RPF_DISPLAYORDER "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlReportSChedule.DataSource = ds
        ddlReportSChedule.DataTextField = "RPF_DESCR"
        ddlReportSChedule.DataValueField = "RPF_ID"
        ddlReportSChedule.DataBind()

        Return ddlReportSChedule
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblError.Text = ""
            ViewState("datamode") = "add"
            clearControls()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If isCommentsExists() = False Then
                SaveData()
            ' Else
            'lblError.Text = "This subject already exists"
            ' End If
            clearControls()
            ViewState("datamode") = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            H_CMT_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            FillSubjects()
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub clearControls()
        txtComments.Text = ""
        'txtCategory.Text = ""
        'H_CAT_ID.Value = 0
        btnSave.Text = "Save"
    End Sub

    Protected Sub gvComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            gvComments.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

  

    Private Sub DisplayComments(ByVal ID As Int64)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""

            If radCatBySubject.Checked = True Then
                str_query = "SELECT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CMT_CAT_ID,CMT_COMMENTS,CAT.CAT_DESC,GRD_DISPLAY,SBG_DESCR,RSM_DESCR,CAT_DESC,CMT_RSM_ID,CMT_RPF_ID,CMT_RSD_ID " & _
                            "FROM ACT.COMMENTS_M  CMT " & _
                            "INNER JOIN ACT.CATEGORY_M CAT ON CAT.CAT_ID=CMT.CMT_CAT_ID " & _
                            "INNER JOIN VW_GRADE_M GRD ON GRD.GRD_ID=CMT.CMT_GRD_ID " & _
                            "INNER JOIN SUBJECTS_GRADE_S SUB ON SUB.SBG_ID = CMT.CMT_SBG_ID " & _
                            "INNER JOIN RPT.REPORT_SETUP_M REP ON REP.RSM_ID=CMT.CMT_RSM_ID " & _
                            " WHERE CMT_ID=" & ID
            ElseIf radGeneralComments.Checked = True Then
                str_query = "SELECT CMT_ID,CMT_SBG_ID,CMT_GRD_ID,CMT_CAT_ID,CMT_COMMENTS,CAT.CAT_DESC,CAT_DESC,CMT_RSM_ID,CMT_RPF_ID,CMT_RSD_ID " & _
                            "FROM ACT.COMMENTS_M  CMT " & _
                            "INNER JOIN ACT.CATEGORY_M CAT ON CAT.CAT_ID=CMT.CMT_CAT_ID " & _
                            " WHERE CMT_ID=" & ID
            End If
            If str_query <> "" Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count >= 1 Then
                    txtComments.Text = ds.Tables(0).Rows(0).Item("CMT_COMMENTS")
                    'txtCategory.Text = ds.Tables(0).Rows(0).Item("CAT_DESC")
                    H_CAT_ID.Value = ds.Tables(0).Rows(0).Item("CMT_CAT_ID")

                    H_CMT_ID.Value = ds.Tables(0).Rows(0).Item("CMT_ID")
                    If radCatBySubject.Checked = True Then
                        Dim li As New ListItem
                        li.Text = ds.Tables(0).Rows(0).Item("GRD_DISPLAY")
                        li.Value = ds.Tables(0).Rows(0).Item("CMT_GRD_ID")
                        ddlGrade.Items(ddlGrade.Items.IndexOf(li)).Selected = True
                        ddlSubject.SelectedValue = ds.Tables(0).Rows(0).Item("CMT_SBG_ID")
                        If IsDBNull(ds.Tables(0).Rows(0).Item("CMT_RSM_ID")) = False Then
                            li.Text = ""
                            li.Value = ""
                            li.Text = ds.Tables(0).Rows(0).Item("RSM_DESCR")
                            li.Value = ds.Tables(0).Rows(0).Item("CMT_RSM_ID")
                            ddlReport.Items(ddlReport.Items.IndexOf(li)).Selected = True
                        End If
                    ElseIf chkCatByGrade.Checked Then
                        Dim grd_id As String = ds.Tables(0).Rows(0).Item("CMT_GRD_ID")
                        ddlGrade0.ClearSelection()
                        ddlGrade0.Items.FindByValue(grd_id).Selected = True
                    End If
                    ' txtCategory.Text = IIf(IsDBNull(ds.Tables(0).Rows(0).Item("CAT_DESC")), "", ds.Tables(0).Rows(0).Item("CAT_DESC"))
                    If IsDBNull(ds.Tables(0).Rows(0).Item("CMT_RSD_ID")) = False Then
                        Dim IntSubject As Integer = 0
                        Dim strGrade As String = ""
                        Dim IntReport As Integer = 0
                        If Not ddlSubject.SelectedIndex = -1 Then
                            IntSubject = ddlSubject.SelectedValue
                        End If
                        If Not ddlGrade.SelectedIndex = -1 Then
                            strGrade = ddlGrade.SelectedValue
                        End If
                        If Not ddlReport.SelectedIndex = -1 Then
                            IntReport = ddlReport.SelectedValue
                        End If
                        GetHeaderIDs(IntSubject, strGrade, IntReport)
                    End If
                    ViewState("datamode") = "edit"
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Private Sub SaveCategory()
        Dim transaction As SqlTransaction
        Dim IntMode As Integer
        IntMode = 0


        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If ViewState("datamode") = "edit" Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "CATEGORY_M", "CAT_ID", "CAT_ID", "CAT_ID=" + H_CMT_ID.Value.ToString)
                ElseIf ViewState("datamode") = "delete" Then
                    UtilityObj.InsertAuditdetails(transaction, "delete", "CATEGORY_M", "CAT_ID", "CAT_ID", "CAT_ID=" + H_CMT_ID.Value.ToString)
                End If
                If chkGrade.Checked = True Then
                    'ViewState("grade")
                    Dim str_query As String = "exec ACT.saveCATEGORY_M 0,'" + txtCategory.Text + "','" + Session("sBsuid") + "','" + ddlGrade.SelectedValue.ToString + "'," + IntMode.ToString + ""
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                Else
                    Dim str_query As String = "exec ACT.saveCATEGORY_M 0,'" + txtCategory.Text + "','" + Session("sBsuid") + "',''," + IntMode.ToString + ""
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                End If

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "CAT_ID(" + H_CMT_ID.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub


    Private Sub DELETEComments(ByVal ID As Int64)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""

            str_query = "DELETE FROM ACT.COMMENTS_M WHERE CMT_ID=" & ID
            Dim intCnt As Integer = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            If intCnt >= 1 Then
                lblError.Text = "Comments Deleted"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvComments_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvComments.RowDeleting
        Try
            gvComments.SelectedIndex = e.RowIndex
            'Dim ID As Integer = CInt(gvComments.DataKeys(e.RowIndex).Value)

            Dim row As GridViewRow = gvComments.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("cmtId"), Label)

            If lblID.Text <> "" Then
                DELETEComments(lblID.Text)
            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvComments_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvComments.RowEditing

        gvComments.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = gvComments.Rows(e.NewEditIndex)

        Dim lblID As New Label
        lblID = TryCast(row.FindControl("cmtId"), Label)

        If lblID.Text <> "" Then
            DisplayComments(lblID.Text)
        End If

        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)
        btnSave.Visible = True

        btnSave.Text = "Update"
        'gridbind()
    End Sub

    Protected Sub ddlSubjects_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Try
                BindCategory()
                gridbind()
            Catch ex As Exception

            End Try

        Catch ex As Exception

        End Try
    End Sub



    Private Function GetSelectedGrades() As String
        Try
            Dim strSubjects As String = ""

            For Each lstItem As ListItem In ddlSubject.Items
                If lstItem.Selected Then
                    strSubjects += lstItem.Value.ToString() + ","
                End If
            Next

            If strSubjects.Length >= 1 Then
                strSubjects = strSubjects.Substring(0, strSubjects.Length - 1)
            Else
                strSubjects = "0"
            End If
            Return strSubjects
        Catch ex As Exception

        End Try
    End Function

    'Protected Sub chkGen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If chkGen.Checked = True Then
    '        ddlGrade.Enabled = False
    '        ddlSubject.Enabled = False
    '        'txtCategory.Enabled = False
    '        'imgSubject.Enabled = False
    '        ddlReport.Enabled = False
    '        chkHeader.Enabled = False
    '        chkHeaderALL.Enabled = False
    '        ddlSubjectAll.Enabled = False
    '        rfsGrade.Enabled = False
    '        rfsHeader.Enabled = False
    '        chkCategory.Checked = False
    '        chkCategory.Enabled = False
    '        gridbind()
    '    Else

    '        ddlGrade.Enabled = True
    '        ddlSubject.Enabled = True
    '        txtCategory.Enabled = True
    '        imgSubject.Enabled = True
    '        ddlReport.Enabled = True
    '        chkHeader.Enabled = True
    '        rfsGrade.Enabled = True
    '        rfsHeader.Enabled = True
    '        chkHeaderALL.Enabled = True
    '        ddlSubjectAll.Enabled = True
    '        chkCategory.Enabled = True
    '        gridbind()
    '    End If

    'End Sub

    Protected Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            FillGrade()
            FillSubjects()
            BindCategory()
            If ddlReport.SelectedValue <> "" Then
                GetReportHeader(chkHeader)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlrptSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub chkHeader_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub ddlSubjectAll_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If ddlSubjectAll.Items.Count = 1 Then
    '            If ddlSubjectAll.Items(0).Selected = True Then
    '                CheckALL(ddlSubject)
    '                gridbind()
    '            Else
    '                UnCheckALL(ddlSubject)
    '                gridbind()
    '            End If
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub chkHeaderALL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If chkHeaderALL.Items.Count = 1 Then
    '            If chkHeaderALL.Items(0).Selected = True Then
    '                CheckALL(chkHeader)
    '                gridbind()
    '            Else
    '                UnCheckALL(chkHeader)
    '                gridbind()
    '            End If
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindCategory()
        GetReportHeader(chkHeader)
        gridbind()
    End Sub

    Protected Sub chkCatByGrade_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCatByGrade.CheckedChanged
        ddlGrade0.Enabled = chkCatByGrade.Checked        
    End Sub

    Private Sub ViewAll()
        trSubjects.Visible = True
        trReportHeaders.Visible = True
        trReportCard.Visible = True
    End Sub

    Protected Sub radGeneralComments_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radGeneralComments.CheckedChanged
        ViewAll()
        If radGeneralComments.Checked Then
            trSubjects.Visible = False
            trReportHeaders.Visible = False
            trReportCard.Visible = False
            chkCatByGrade.Visible = True
            ddlGrade0.Visible = True
            ddlGrade0.Enabled = chkCatByGrade.Checked
            BindCategory()
            gridbind()
        End If
    End Sub

    Protected Sub radCatBySubject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radCatBySubject.CheckedChanged
        ViewAll()
        chkCatByGrade.Visible = False
        ddlGrade0.Visible = False
        If radCatBySubject.Checked = True Then
            BindCategory()
            gridbind()
        End If
    End Sub


    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        gridbind()
    End Sub

   

    Protected Sub btnSaveCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCategory.Click
        If radCatBySubject.Checked = True Then
            trcatGrade.Visible = False
            chkGrade.Checked = True
        Else
            trcatGrade.Visible = True
            chkGrade.Checked = False
        End If
        SaveCategory()
        BindCategory()
        'Panel1.Visible = False
    End Sub

 
End Class
