﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmObjTrackStudentLevels_Popup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            lblAcademicYear.Text = Request.QueryString("accyear")
            lblGrade.Text = Request.QueryString("grade")
            lblGroup.Text = Request.QueryString("group")
            lblSubject.Text = Request.QueryString("subject")
            lblName.Text = Request.QueryString("name")
            lblStudentID.Text = Request.QueryString("stuno")
            '  lblCurrentLevel.Text = Request.QueryString("clvl")

            hSBM_ID.Value = Request.QueryString("sbmid").Replace(".00", "").Replace(",", "")
            hACD_ID.Value = Request.QueryString("acdid").Replace(".00", "").Replace(",", "")
            hSGR_ID.Value = Request.QueryString("sgrid").Replace(".00", "").Replace(",", "")

            Dim pg As String = Request.QueryString("page").Replace(".00", "").Replace(",", "")

            If pg = "objectives" Then
                tabPopup.ActiveTabIndex = 1
            End If
            BindCategory()
            GetStu_ID()
            BindPhoto()
            BindLevel()

            GridBind()
            GetCurrentLevel()
            BindObjectiveCount(True)
        End If
    End Sub

    Sub BindObjectiveCount(ByVal bTotal As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(STO_ID) FROM OBJTRACK.STUDENT_OBJECTIVES_S WHERE STO_SBM_ID=" + hSBM_ID.Value _
                                & " AND STO_bCOMPLETED='Y' AND STO_STU_ID=" + hSTU_ID.Value

        Dim str As String
        Dim str1 As String

        If bTotal = True Then
            str = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString
            lblTotalObjectives.Text = "Total Objectives Completed : " + str
        End If

        str_query = "SELECT COUNT(STO_ID) FROM OBJTRACK.STUDENT_OBJECTIVES_S WHERE STO_SBM_ID=" + hSBM_ID.Value _
                                & " AND STO_bCOMPLETED='Y' AND STO_LVL_ID=" + ddlLevel.SelectedValue.ToString _
                                & " AND STO_STU_ID=" + hSTU_ID.Value
        str1 = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString
        lblLevelObjectives.Text = "Objectives Completed in " + ddlLevel.SelectedItem.Text + " : " + str1
    End Sub

    Sub BindPhoto()
       
        If hStuPhotoPath.Value <> "" Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + hStuPhotoPath.Value
            imgStud.ImageUrl = strImagePath


        Else
            imgStud.ImageUrl = "~/Images/Photos/no_image.gif"
        End If
        'imgStud.AlternateText = "No Image found"
    End Sub


    Sub BindCategory()
        ddlCategory.Items.Clear()


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT OBT_ID,OBT_DESCR FROM " _
                                & " OBJTRACK.OBJECTIVECATEGORY_M WHERE OBT_BSU_ID='" + Session("SBSUID") + "'" _
                                & " AND OBT_SBM_ID=" + hSBM_ID.Value _
                                & " ORDER BY OBT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCategory.DataSource = ds
        ddlCategory.DataTextField = "OBT_DESCR"
        ddlCategory.DataValueField = "OBT_ID"
        ddlCategory.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlCategory.Items.Insert(0, li)
    End Sub

    Sub BindLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT LVL_ID,LVL_DESCR FROM OBJTRACK.LEVEL_BSU_M WHERE LVL_BSU_ID='" + Session("SBSUID") + "'"

        If lblSubject.Text.ToUpper <> "ISLAMIC STUDIES" Then
            str_query += " AND ISNULL(LVL_bISL,'false')='FALSE'"
        End If

        str_query += " AND LVL_ID IN(SELECT OBJ_LVL_ID FROM OBJTRACK.OBJECTIVES_M WHERE OBJ_BSU_ID='" + Session("SBSUID") + "'" _
                          & " AND OBJ_SBM_ID=" + hSBM_ID.Value + ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "LVL_DESCR"
        ddlLevel.DataValueField = "LVL_ID"
        ddlLevel.DataBind()

        str_query = "SELECT DISTINCT NCL_LVL_ID FROM OBJTRACK.NCLEVEL_BSU_M AS A" _
                    & " INNER JOIN OBJTRACK.STUDENT_CURRENTLEVEL_S AS B ON " _
                    & " A.NCL_DESCR=B.SCL_CURRENTLEVEL  AND B.SCL_SBM_ID=A.NCL_SBM_ID  " _
                    & " WHERE SCL_STU_ID=" + hSTU_ID.Value + " AND SCL_SBM_ID=" + hSBM_ID.Value _
                    & " AND NCL_ACD_ID=" + hACD_ID.Value

        Dim lvlid As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If Not ddlLevel.Items.FindByValue(lvlid) Is Nothing Then
            ddlLevel.Items.FindByValue(lvlid).Selected = True
        End If
    End Sub

    Sub GetStu_ID()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_ID,ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH FROM STUDENT_M WHERE STU_NO='" + lblStudentID.Text + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            hSTU_ID.Value = ds.Tables(0).Rows(0).Item(0)
            hStuPhotoPath.Value = ds.Tables(0).Rows(0).Item(1)
        End If
    End Sub
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT OBJ_DESCR,OBJ_ID FROM OBJTRACK.OBJECTIVES_M WHERE OBJ_LVL_ID=" + ddlLevel.SelectedValue.ToString _
                                & " AND OBJ_SBM_ID=" + hSBM_ID.Value + " AND OBJ_BSU_ID='" + Session("SBSUID") + "'"

        If ddlCategory.SelectedValue.ToString <> "0" Then
            str_query += "AND OBJ_OBT_ID=" + ddlCategory.SelectedValue.ToString
        End If

        str_query += " ORDER BY OBJ_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvObjectives.DataSource = ds
        gvObjectives.DataBind()

        GetStudentObjectives()

        BindTargetGrid()
    End Sub


    Sub BindTargetGrid()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If hObjCompleted.Value <> "" Then
            str_query = "SELECT OBJ_DESCR,OBJ_ID FROM OBJTRACK.OBJECTIVES_M WHERE OBJ_LVL_ID=" + ddlLevel.SelectedValue.ToString _
                                    & " AND OBJ_SBM_ID=" + hSBM_ID.Value + " AND OBJ_BSU_ID='" + Session("SBSUID") + "'" _
                                    & " AND OBJ_ID NOT IN(" + hObjCompleted.Value + ")"

        Else
            str_query = "SELECT OBJ_DESCR,OBJ_ID FROM OBJTRACK.OBJECTIVES_M WHERE OBJ_LVL_ID=" + ddlLevel.SelectedValue.ToString _
                                    & " AND OBJ_SBM_ID=" + hSBM_ID.Value + " AND OBJ_BSU_ID='" + Session("SBSUID") + "'"

        End If

        If ddlCategory.SelectedValue.ToString <> "0" Then
            str_query += "AND OBJ_OBT_ID=" + ddlCategory.SelectedValue.ToString
        End If

        str_query += " ORDER BY OBJ_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTarget.DataSource = ds
        gvTarget.DataBind()

        GetStudentTargets()
    End Sub
    Sub GetStudentObjectives()
        Dim objData As New Hashtable
        hObjCompleted.Value = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STO_OBJ_ID,STO_bCOMPLETED FROM OBJTRACK.STUDENT_OBJECTIVES_S" _
                                & " WHERE STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + " AND STO_SBM_ID=" + hSBM_ID.Value _
                                & " AND STO_STU_ID=" + hSTU_ID.Value

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlStatus As DropDownList

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                objData.Add(.Item(0).ToString, .Item(1).ToString)
                If .Item(1).ToString = "Y" Then
                    If hObjCompleted.Value <> "" Then
                        hObjCompleted.Value += ","
                    End If
                    hObjCompleted.Value += .Item(0).ToString
                End If
            End With
        Next

        For i = 0 To gvObjectives.Rows.Count - 1
            lblObjId = gvObjectives.Rows(i).FindControl("lblObjId")

            If Not objData.Item(lblObjId.Text) Is Nothing Then
                ddlStatus = gvObjectives.Rows(i).FindControl("ddlStatus")
                If Not ddlStatus.Items.FindByValue(objData.Item(lblObjId.Text)) Is Nothing Then
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(objData.Item(lblObjId.Text)).Selected = True
                End If
            End If
        Next
    End Sub

    Sub GetStudentTargets()
        Dim objData As New Hashtable

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STO_OBJ_ID,STO_bTARGET,isnull(STO_bRPTCRDSHOWTARGET,'false') FROM OBJTRACK.STUDENT_OBJECTIVES_S" _
                                & " WHERE STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + " AND STO_SBM_ID=" + hSBM_ID.Value _
                                & " AND STO_STU_ID=" + hSTU_ID.Value + " AND ISNULL(STO_bTARGET,'FALSE')='TRUE'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim lblObjId As Label
        Dim chkStatus As CheckBox
        Dim chkDisplay As CheckBox
        Dim strDisplay As String

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                objData.Add(.Item(0).ToString, .Item(2).ToString)
            End With
        Next

        For i = 0 To gvTarget.Rows.Count - 1
            lblObjId = gvTarget.Rows(i).FindControl("lblObjId")

            If Not objData.Item(lblObjId.Text) Is Nothing Then
                strDisplay = objData.Item(lblObjId.Text)
                If strDisplay.ToLower = "true" Then
                    chkDisplay = gvTarget.Rows(i).FindControl("chkDisplay")
                    chkDisplay.Checked = True
                End If
                chkStatus = gvTarget.Rows(i).FindControl("chkStatus")
                chkStatus.Checked = True
            End If
        Next
    End Sub
    Sub SaveData()
        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlStatus As DropDownList
    

        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim bCheck As Boolean

        For i = 0 To gvObjectives.Rows.Count - 1
              
            lblObjId = gvObjectives.Rows(i).FindControl("lblObjId")
            ddlStatus = gvObjectives.Rows(i).FindControl("ddlStatus")
            
            str_query = " EXEC OBJTRACK.saveSTUDENTOBJECTIVES @STO_STU_ID =" + hSTU_ID.Value + "," _
                            & " @STO_SBM_ID=" + hSBM_ID.Value + "," _
                            & " @STO_OBJ_ID=" + lblObjId.Text + "," _
                            & " @STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + "," _
                            & " @STO_bCOMPLETED='" + ddlStatus.SelectedValue + "'," _
                            & " @STO_USER='" + Session("SUSR_NAME") + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        Next





        str_query = " EXEC OBJTRACK.saveSTUDENTCURRENTLEVEL " _
                  & " @ACD_ID =" + hACD_ID.Value + "," _
                  & " @SGR_ID =" + hSGR_ID.Value + "," _
                  & " @SBM_ID =" + hSBM_ID.Value

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        Session("Popup") = "1"
    End Sub

    Sub SaveTarget()
        Dim i As Integer
        Dim lblObjId As Label
        Dim chkStatus As CheckBox


        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim bCheck As Boolean
        Dim chkDisplay As CheckBox
        Dim bDispay As Boolean

        For i = 0 To gvTarget.Rows.Count - 1

            lblObjId = gvTarget.Rows(i).FindControl("lblObjId")
            chkStatus = gvTarget.Rows(i).FindControl("chkStatus")
            chkDisplay = gvTarget.Rows(i).FindControl("chkDisplay")
            bCheck = chkStatus.Checked
            bDispay = chkDisplay.Checked
            str_query = " EXEC OBJTRACK.saveSTUDENTOBJECTIVES_TARGET @STO_STU_ID =" + hSTU_ID.Value + "," _
                            & " @STO_SBM_ID=" + hSBM_ID.Value + "," _
                            & " @STO_OBJ_ID=" + lblObjId.Text + "," _
                            & " @STO_LVL_ID=" + ddlLevel.SelectedValue.ToString + "," _
                            & " @STO_bTARGET='" + bCheck.ToString + "'," _
                            & " @STO_USER='" + Session("SUSR_NAME") + "'," _
                            & " @STO_bRPTCRDSHOWTARGET='" + bDispay.ToString + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        Next





        str_query = " EXEC OBJTRACK.saveSTUDENTCURRENTLEVEL " _
                  & " @ACD_ID =" + hACD_ID.Value + "," _
                  & " @SGR_ID =" + hSGR_ID.Value + "," _
                  & " @SBM_ID =" + hSBM_ID.Value

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Sub GetCurrentLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT isnull(SCL_CURRENTLEVEL,'') SCL_CURRENTLEVEL FROM OBJTRACK.STUDENT_CURRENTLEVEL_S WHERE" _
                                 & " SCL_STU_ID=" + hSTU_ID.Value + " AND SCL_SBM_ID=" + hSBM_ID.Value
        lblCurrentLevel.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub



  
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If tabPopup.ActiveTabIndex = 0 Then
            SaveData()
            GetCurrentLevel()
        Else
            SaveTarget()
        End If
        GridBind()
        BindObjectiveCount(True)
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        BindObjectiveCount(False)
        GridBind()
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        BindObjectiveCount(False)
        GridBind()
    End Sub
End Class
