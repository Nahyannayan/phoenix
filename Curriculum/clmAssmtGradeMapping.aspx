﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmAssmtGradeMapping.aspx.vb" Inherits="Curriculum_clmAssmtGradeMapping" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>  BrownBook Grade Mapping
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" Width="100%">
                 
                           
           <tr>
            <td align="left"  >
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblClm" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" Width="100%" >
                    
                      <tr>
                        <td align="left" >
                           <span class="field-label"> Select Academic Year</span></td>
                        
                        <td align="left" >
                             <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                            </asp:DropDownList>               
                      </td>

                           <td align="left">
                            <span class="field-label"> Grade</span></td>
                       
                        <td align="left">
                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                        </asp:DropDownList></td>


                       </tr>   
                    <tr>
                       
                        <td align="left" >
                            <span class="field-label"> Select Subject</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                     
                      <td align="center" colspan="2">
                       <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"   /></td>
                    </tr>
                    
                      <tr>
                    <td colspan="4"></td>
                    </tr>
                                           
                      
                    <tr style ="display:none" >
                        <td align="left" >
                         <span class="field-label">  Copy From  Grade</span></td>
                       
                        <td align="left" >
                        <asp:DropDownList ID="ddlCopyGrade" runat="server" AutoPostBack="true" >
                        </asp:DropDownList></td>
                        <td align="left" >
                            <span class="field-label">   Copy From Subject</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlCopySubject" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                          <td align="right" >
                       <asp:Button ID="btnCopy" runat="server" Text="Copy" CssClass="button" TabIndex="4"   /></td>
                    </tr>
                      <tr>
                    <td colspan="4" align="left" >
                    <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;">  Copy to Subjects</asp:LinkButton></td>
                    </tr>
                      <tr>
                    <td colspan="4">
                    
                          </td>
                    </tr>
                      <tr>
                    <td colspan="4"></td>
                    </tr>
                      <tr>
                      
                      
                      
                     <td align="center" colspan="4" valign="top">
                     <asp:GridView ID="gvStud" runat="server" AllowPaging="FALSE" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                       PageSize="20">
                         <RowStyle />
                     <Columns>
                   
                    
                    <asp:TemplateField HeaderText="Grade">
                    <ItemTemplate>
                    <asp:Label ID="lblGrade"  runat="server" Text='<%# Bind("RBG_GRADE") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle ></ItemStyle>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Convert to Grade">
                    <ItemTemplate>
                    <asp:TextBox ID="txtValue"   runat="server" Text='<%# Bind("RBG_VALUE") %>'></asp:TextBox>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"  ></ItemStyle>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Result">
                    <ItemTemplate>
                    <asp:DropDownList id="ddlResult"    runat="server" AutoPostBack="True" >
                                <asp:ListItem Value="Pass" >Pass</asp:ListItem>
                                <asp:ListItem Value="Fail" >Fail</asp:ListItem>
                     </asp:DropDownList>
                    </ItemTemplate>
                    <ItemStyle ></ItemStyle>
                    </asp:TemplateField>
                    

                    </Columns>  
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <AlternatingRowStyle />
                    </asp:GridView>
                    
                     </td></tr>
                    
                      <tr>
                      
                      
                      
                     <td align="center" colspan="4" valign="top">
                         </td></tr>
                    
                      <tr>
                      
                      
                      
                     <td align="center" colspan="4" valign="top">
                       <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" 
                             TabIndex="4"  />
    <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server"
    TargetControlID="lnkBtnSelectmonth"
    PopupControlID="Panel2"
    CommitProperty="value"
    Position="Bottom"
    CommitScript="" />
                          </td></tr>
                    
                    </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <input id="h_Selected_menu_8" runat="server"
                        type="hidden" value="=" />
               </td></tr>
</table>

            </div>
        </div>
       </div>

    <asp:Panel ID="Panel2" runat="server"  style =" display:none" CssClass="Visibility_none panel-cover" >
        <div style="border: 0px outset;  text-align:left " >
            <asp:UpdatePanel  runat="server" ID="up2">
                <ContentTemplate>
                 <asp:GridView ID="gvSubjects" runat="server" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                      HeaderStyle-Height="30" PageSize="20" >
                         <RowStyle  />
                     <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect"  runat="server" />
                            <asp:Label ID="lblSUB_ID"  runat="server" Visible="false"
                                    Text='<%# Bind("SBG_ID") %>' ></asp:Label>     
                        </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" ></ItemStyle>
                    </asp:TemplateField>
                    

                    <asp:TemplateField HeaderText="Subjects">
                    <ItemTemplate>
                    <asp:Label ID="lblSubject"  runat="server" 
                            Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle></ItemStyle>
                    </asp:TemplateField>
                    

                    </Columns>  
                    <SelectedRowStyle  />
                    <HeaderStyle  />
                    <AlternatingRowStyle  />
                    </asp:GridView>
                    <asp:TreeView ID="trMonths" ShowCheckBoxes="all" onclick="client_OnTreeNodeChecked();" runat="server" >
                    </asp:TreeView>
                    <div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>

</asp:Content>

