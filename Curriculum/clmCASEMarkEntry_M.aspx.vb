﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmCASEMarkEntry_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state


            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C410065") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                getACYID()
                BindGrade()
                BindSubject()
                PopulateGroup()
                BindQuestionBank()
                trSave.Visible = False
                trStudent.Visible = False
                trMarks.Visible = False
                gvQuestions.Attributes.Add("bordercolor", "#1b80b6")
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GRD_DISPLAY,GRD_ID FROM VW_GRADE_M WHERE GRD_ID  IN('04','06','08') ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                         & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH','SCIENCE','ARABIC')"
        Else
            str_query = "SELECT DISTINCT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                       & " INNER JOIN SUBJECTS_GRADE_S ON SBG_SBM_ID=SBM_ID" _
                       & " INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID" _
                       & " INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID AND SGS_TODATE IS NULL" _
                       & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH','SCIENCE','ARABIC') AND SGS_EMP_ID=" + Session("EmployeeID") _
                       & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                       & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSubject.DataSource = ds
            ddlSubject.DataTextField = "SBM_DESCR"
            ddlSubject.DataValueField = "SBM_ID"
            ddlSubject.DataBind()
    End Sub

    Sub BindStudents()
        Dim str_conn = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID,SECTION=GRM_DISPLAY+'-'+SCT_DESCR,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                        & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View',ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH " _
                        & " FROM STUDENT_M AS A INNER JOIN VW_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                        & " INNER JOIN VW_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                        & " WHERE STU_CURRSTATUS='EN' "
        Else
            str_query = "SELECT STU_ID,SECTION=GRM_DISPLAY+'-'+SCT_DESCR,STU_NO,STU_NAME," _
                        & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View',ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH " _
                        & " FROM VW_STUDENT_DETAILS_PREVYEARS" _
                        & " WHERE STU_CURRSTATUS='EN' "
        End If




        If ddlGroup.SelectedValue <> "0" Then
            str_query += " AND STU_ID IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID=" + hfSGR_ID.Value + ")"
        End If
        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID= '" + hfGRD_ID.Value + "'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvStudent.DataSource = ds
        gvStudent.DataBind()
        If gvStudent.Rows.Count > 0 Then
            gvStudent.HeaderRow.Visible = False
        End If

        Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
        hfSTU_ID.Value = lblStuId.Text
    End Sub

    Private Sub PopulateGroup()
        ddlGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M " _
                      & " INNER JOIN SUBJECTS_GRADE_S ON SGR_SBG_ID=SBG_ID" _
                      & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                      & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND SBG_SBM_ID='" + ddlSubject.SelectedValue.ToString + "'"
        Else
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                        & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID " _
                        & " INNER JOIN SUBJECTS_GRADE_S ON SGR_SBG_ID=SBG_ID" _
                        & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                        & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND " _
                        & " SBG_SBM_ID='" + ddlSubject.SelectedValue.ToString + "' AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                        & " AND SGS_TODATE IS NULL"
        End If
        str_query += " ORDER BY SGR_DESCR"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub

    Sub getACYID()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACD_ACY_ID FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        hfACY_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString
    End Sub

    Sub BindQuestionBank()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT QSM_ID,QSM_DESCR FROM CE.QUESTIONS_M  WHERE QSM_ACY_ID=" + hfACY_ID.Value _
                               & " AND QSM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND QSM_SBM_ID='" + ddlSubject.SelectedValue.ToString + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlQuestions.DataSource = ds
        ddlQuestions.DataTextField = "QSM_DESCR"
        ddlQuestions.DataValueField = "QSM_ID"
        ddlQuestions.DataBind()
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT QSD_ID,QSD_DESCR,isnull(QSD_MAXMARK,0) QSD_MAXMARK," _
                               & " ERR='Marks Should be between 0 and '+replace(convert(varchar(100),QSD_MAXMARK),'.00','') FROM  " _
                                & " CE.QUESTIONS_D WHERE QSD_QSM_ID='" + hfQSM_ID.Value + "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvQuestions.DataSource = ds
        gvQuestions.DataBind()

    End Sub

    Sub BindMarks(ByVal qsd_id As String, ByVal txtMark As TextBox)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(SSM_MARKS,0) FROM CE.STUDENT_MARKS WHERE SSM_STU_ID=" + hfSTU_ID.Value _
                                & " AND SSM_ACY_ID=" + hfACY_ID.Value + " AND SSM_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                                & " AND SSM_QSD_ID=" + qsd_id
        Dim marks As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If marks <> "0" Then
            txtMark.Text = marks
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer

        Dim lblQsdId As Label
        Dim lblMaxMark As Label
        Dim lblQuestion As Label
        Dim txtMark As TextBox

        For i = 0 To gvQuestions.Rows.Count - 1
            With gvQuestions.Rows(i)
                lblQsdId = .FindControl("lblQsdId")
                lblMaxMark = .FindControl("lblMaxMark")
                lblQuestion = .FindControl("lblQuestion")
                txtMark = .FindControl("txtMark")

                str_query = "exec CE.saveSTUDENTMARKS " _
                        & " @SSM_STU_ID=" + hfSTU_ID.Value + "," _
                        & " @SSM_BSU_ID='" + Session("sbsuid") + "'," _
                        & " @SSM_ACY_ID=" + hfACY_ID.Value + "," _
                        & " @SSM_GRD_ID='" + hfGRD_ID.Value + "'," _
                        & " @SSM_SCT_ID=" + hfSGR_ID.Value + "," _
                        & " @SSM_SBM_ID=" + hfSBM_ID.Value + "," _
                        & " @SSM_QSD_ID=" + lblQsdId.Text + "," _
                        & " @SSM_MARKS=" + Val(txtMark.Text).ToString + "," _
                        & " @SSM_MAXMARK=" + lblMaxMark.Text + "," _
                        & " @USER='" + Session("susr_name") + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End With
        Next
        lblError.Text = "Record Saved Successfully"
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        getACYID()
        BindGrade()
        PopulateGroup()
        BindQuestionBank()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        BindSubject()
        PopulateGroup()
        BindQuestionBank()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        hfSGR_ID.Value = ddlGroup.SelectedValue.ToString
        hfQSM_ID.Value = ddlQuestions.SelectedValue.ToString
        hfSBM_ID.Value = ddlSubject.SelectedValue.ToString
        getACYID()
        BindStudents()
        GridBind()
        trStudent.Visible = True
        trAcd1.Visible = False
        trAcd2.Visible = False
        trAcd3.Visible = False
        trAcd4.Visible = False
        trMarks.Visible = True
        trSave.Visible = True
    End Sub
    Protected Sub gvStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudent.PageIndexChanging
        gvStudent.PageIndex = e.NewPageIndex
        BindStudents()
        GridBind()
    End Sub
   
    Protected Sub btnCancelSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSearch.Click
        trAcd1.Visible = True
        trAcd2.Visible = True
        trAcd3.Visible = True
        trAcd4.Visible = True
        trMarks.Visible = False
        trStudent.Visible = False
        trSave.Visible = False
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        PopulateGroup()
        BindQuestionBank()
    End Sub

    Protected Sub gvQuestions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvQuestions.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblQsdId As Label
            Dim txtMark As TextBox
            lblQsdId = e.Row.FindControl("lblQsdId")
            txtMark = e.Row.FindControl("txtMark")
            BindMarks(lblQsdId.Text, txtMark)
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
        SaveData()
        If gvStudent.PageIndex <> gvStudent.PageCount - 1 Then
            gvStudent.PageIndex += 1
            BindStudents()
            If gvStudent.Rows.Count > 0 Then
                Dim lblStuId As Label = gvStudent.Rows(0).FindControl("lblStuId")
                hfSTU_ID.Value = lblStuId.Text
            End If
            GridBind()
        End If
    End Sub
End Class
