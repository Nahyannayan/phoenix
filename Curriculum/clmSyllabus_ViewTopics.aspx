<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSyllabus_ViewTopics.aspx.vb" Inherits="Curriculum_clmSyllabus_ViewTopics" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function openWin() {
            var sFeatures;
            var acdid;
            var termid;
            var gradeid;
            var groupid;
            var subjectid;
            var url;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            acdid = document.getElementById('<%=h_ACD_ID.ClientID %>').value;
            termid = document.getElementById('<%=h_TRM_ID.ClientID %>').value;
            gradeid = document.getElementById('<%=h_GRD_ID.ClientID %>').value;
            subjectid = document.getElementById('<%=h_SUBJ_ID.ClientID %>').value;

            if (gradeid == '') {
                alert('Please select Grade')
                return false;
            }
            url = "ShowSyllabus.aspx?AcdID='" + acdid + "'&TrmID='" + termid + "'&GrdID='" + gradeid + "'&SubjID='" + subjectid + "'";
            var oWnd = radopen(url, "RadWindow1");
            //result = window.showModalDialog("../Curriculum/clmPopupForm.aspx?multiselect=true&ID=SUBJECT&GRD_IDs=" + GRD_IDs+"&ACD_ID="+AcdId ,"", sFeatures)            
            <%--if (oWnd == '' || oWnd == undefined)
            { return false; }
            NameandCode = oWnd.split('||');
            document.getElementById("<%=txtSelSyllabus.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=h_SYL_ID.ClientID %>").value = NameandCode[1];--%>
        }


        function GetSyllabus() {
            // alert("Test");
            var sFeatures;
            sFeatures = "dialogWidth: 429px; ";
            sFeatures += "dialogHeight: 345px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var acdid;
            var termid;
            var gradeid;
            var groupid;
            var subjectid;
            var url;

            acdid = document.getElementById('<%=h_ACD_ID.ClientID %>').value;
        termid = document.getElementById('<%=h_TRM_ID.ClientID %>').value;
        gradeid = document.getElementById('<%=h_GRD_ID.ClientID %>').value;

        subjectid = document.getElementById('<%=h_SUBJ_ID.ClientID %>').value;

        url = "ShowSyllabus.aspx?AcdID='" + acdid + "'&TrmID='" + termid + "'&GrdID='" + gradeid + "'&SubjID='" + subjectid + "'";
        result = window.showModalDialog(url, "", sFeatures)
        if (result == '' || result == undefined)
        { return false; }
        NameandCode = result.split('||');
        document.getElementById("<%=txtSelSyllabus.ClientID %>").value = NameandCode[0];
        document.getElementById("<%=h_SYL_ID.ClientID %>").value = NameandCode[1];

    }
    function OnClientClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
            NameandCode = arg.Topic.split('||');
            document.getElementById("<%=txtSelSyllabus.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=h_SYL_ID.ClientID %>").value = NameandCode[1];
           __doPostBack('<%= txtSelSyllabus.ClientID%>', 'TextChanged');
        }
    }


    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;
        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" Width="800px" Height="600px" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="View Allocated Topics"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr id="Tr1" runat="server">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Term</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_SelectedIndexChanged"></asp:DropDownList>
                            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Grade</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left"><span class="field-label">Subject</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Group</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlgroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left"><span class="field-label">Syllabus</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtSelSyllabus" runat="server" SkinID="TextBoxNormal"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="openWin();return false;"></asp:ImageButton></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:RadioButton ID="RadPlanned" runat="server" Text="Planned" Checked="True" GroupName="1" CssClass="field-label"></asp:RadioButton></td>
                        <td align="left">
                            <asp:RadioButton ID="RadActual" runat="server" Text="Actual" GroupName="1" CssClass="field-label"></asp:RadioButton></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">From Date</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
                        <td align="left"><span class="field-label">To Date</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
                    </tr>
                    <%--<tr>
                        <td align="left" colspan="8" class="title-bg-light">
                            <asp:Label ID="Label1" runat="server" Text="Details..">
                            </asp:Label></td>
                    </tr>--%>
                    <tr>
                        <td align="center" colspan="4">&nbsp;<asp:Button ID="btnView" runat="server" CssClass="button" Text="View Details" OnClick="btnView_Click" /></td>
                    </tr>
                    <tr style="display: none">
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="left" colspan="2">&nbsp;</td>
                    </tr>
                    <tr style="display: none">
                        <td align="left" colspan="4" style="text-align: right;">
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr id="tr_Deatails" runat="server">
                        <td align="center" colspan="4" valign="top">
                            <asp:GridView ID="gvTopics" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" AllowPaging="True" OnPageIndexChanging="gvTopics_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="TopicID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTopicID" runat="server" Text='<%# Bind("ID") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Topics">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTopic" runat="server" Text='<%# Bind("Descr") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned From Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPlanStDt" runat="server" Text='<%# Bind("PLNSTDT","{0:dd/MMM/yyy}") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned To Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPlanEndDate" runat="server" Text='<%# Bind("PLNENDDT","{0:dd/MMM/yyy}") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Planned Hrs">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPlanHrs" runat="server" Text='<%# Bind("PLNHRS") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actual From Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActStDt" runat="server" Text='<%# Bind("ACTSTDT","{0:dd/MMM/yyy}") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actual To Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActEndDate" runat="server" Text='<%# Bind("ACTENDDT","{0:dd/MMM/yyy}") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actual Hrs">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActHrs" runat="server" Text='<%# Bind("ACTHRS") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>


                <asp:HiddenField ID="h_SYL_ID" runat="server" />
                <asp:HiddenField ID="h_GRD_ID" runat="server" />
                <asp:HiddenField ID="h_ACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_TRM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_SUBJ_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_GRP_ID" runat="server"></asp:HiddenField>
                <br />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                &nbsp; &nbsp; &nbsp;<ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            </div>
        </div>
    </div>
</asp:Content>

