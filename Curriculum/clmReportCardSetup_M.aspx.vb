Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmReportCardSetup_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100131") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfRSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    lblReportCard.Text = Encr_decrData.Decrypt(Request.QueryString("report").Replace(" ", "+"))
                    GridBind()
                    gvReport.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT * FROM (SELECT RSD_ID,RSD_HEADER,RSD_bDIRECTENTRY AS DIRECT,RSD_bALLSUBJECTS AS ENABLE, " _
                                 & " RSD_DISPLAYORDER,CASE WHEN RSD_bALLSUBJECTS='TRUE' THEN 1 ELSE 2 END AS HEADERORDER, CASE WHEN RSD_bALLSUBJECTS='TRUE' THEN 'Y' ELSE 'N' END AS ALLSUB FROM RPT.REPORT_SETUP_D WHERE " _
                                 & " RSD_RSM_ID=" + hfRSM_ID.Value + " AND RSD_SBG_ID IS NULL AND RSD_CSSCLASS<>'TEXTBOXMULTI' AND RSD_bDIRECTENTRY='TRUE'" _
                                 & " ) A ORDER BY HEADERORDER,RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvReport.DataSource = ds
        gvReport.DataBind()
    End Sub

    Sub SaveData()
        Dim transaction As SqlTransaction
        Dim str_query As String
        Dim i As Integer
        Dim lblRsdId As Label
        Dim chkSelect As CheckBox
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            Transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For i = 0 To gvReport.Rows.Count - 1
                    lblRsdId = gvReport.Rows(i).FindControl("lblRsdId")
                    chkSelect = gvReport.Rows(i).FindControl("chkSelect")
                    str_query = "exec RPT.updateHEADERSETTING " _
                               & lblRsdId.Text + "," _
                               & chkSelect.Checked.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
#End Region
    Protected Sub lblDefault_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRsdId As Label
        Dim lblReport As Label
        Dim chkSelect As CheckBox
        Dim lblAllSubj As Label
        lblRsdId = TryCast(sender.FindControl("lblRsdId"), Label)
        lblReport = TryCast(sender.FindControl("lblReport"), Label)
        chkSelect = TryCast(sender.FindControl("chkSelect"), CheckBox)
        lblAllSubj = TryCast(sender.FindControl("lblAllSubj"), Label)
        
        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim url As String
        url = String.Format("~\Curriculum\clmReportCardDefaults_M.aspx?MainMnu_code={0}&datamode={1}&rsmid=" + Encr_decrData.Encrypt(hfRSM_ID.Value) _
             & "&report=" + Encr_decrData.Encrypt(lblReportCard.Text) _
             & "&header=" + Encr_decrData.Encrypt(lblReport.Text) _
             & "&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
             & "&rsdid=" + Encr_decrData.Encrypt(lblRsdId.Text) _
             & "&allSubjects=" + Encr_decrData.Encrypt(lblAllSubj.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub
End Class
