Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmGroup_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
         
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                     ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    PopulateTeacher()

                    Session("dtTeacher") = SetTeacherDataTable()
                    ViewState("SelectedRow") = -1
                    If ViewState("datamode") = "add" Then

                        txtSubject.Text = ""
                        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                        PopulateGradeStream()
                        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString, "", ddlStream.SelectedValue())
                        ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                        ' PopulateGradeSubjects()

                        ' lbView.Visible = False
                        'lbAdd.Visible = False
                        mnuMaster.Visible = False
                        hfSGR_ID.Value = "0"
                        ViewState("stumode") = "add"
                        btnRemove.Visible = False
                        rdAll.Visible = False
                        rdMuslim.Visible = False
                        rdNonMuslim.Visible = False
                        BindSubjects()
                        updateSubjectURL()
                        'imgSub.Visible = True


                        txtDescr.Text = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlGrade.SelectedItem.Text.ToString + "_"
                        hfGroupName.Value = txtDescr.Text
                        GetGroupName()

                        hfDescr.Value = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlGrade.SelectedItem.Text.ToString + "_"
                        hfAddNew.Value = "add"

                    Else
                    EnableDisableControls(False)

                    Dim li As ListItem

                    li = New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("academicyear").Replace(" ", "+"))
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    ddlAcademicYear.Items.Add(li)

                    li = New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    ddlGrade.Items.Add(li)

                    li = New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("shift").Replace(" ", "+"))
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("shfid").Replace(" ", "+"))
                    ddlShift.Items.Add(li)

                    li = New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                    ddlStream.Items.Add(li)

                    'li = New ListItem
                    'li.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                    'li.Value =Encr_decrData.Decrypt(Request.QueryString("sbgid").Replace(" ", "+"))
                    'ddlSubject.Items.Add(li)

                    txtSubject.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                        'hfSBG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbgid").Replace(" ", "+"))


                    txtDescr.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    hfGROUP.Value = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    hfSGR_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sgrid").Replace(" ", "+"))
                        ViewState("stumode") = "view"

                        li = New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("sbgid").Replace(" ", "+"))

                        ddlSubjects.Items.Add(li)
                        If Session("CURRENT_ACD_ID") <> ddlAcademicYear.SelectedValue.ToString Then
                            GridBindPrevYears(gvView)
                        Else
                            GridBind(gvView)
                        End If

                        ' lbView.Visible = False
                        'lbAdd.Visible = True
                        mnuMaster.Visible = True
                        h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                        h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                        h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                        h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                        set_Menu_Img()

                        GetTeacher()

                        imgSub.Visible = False
                        txtUntisGroup.Text = Encr_decrData.Decrypt(Request.QueryString("untisgroup").Replace(" ", "+"))
                    End If

                    'If Session("multistream") = 0 And Session("multishift") = 0 Then
                    '    trshift.Visible = False
                    'End If
                End If
                gvAdd.Attributes.Add("bordercolor", "#1b80b6")
                gvView.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

            Try
                checkUnitsTimeTable()
            Catch ex As Exception

            End Try
        End If
        '        highlight_grid()
        ViewState("slno") = 0
    End Sub

    Sub checkUnitsTimeTable()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT count(GT_ID) FROM  [OASIS_TIMETABLE].[TM].[GROUP_TIMETABLE] WITH(NOLOCK) WHERE " _
                                & " GT_TYPE='GROUP' AND  GT_BSU_ID='" + Session("SBSUID") + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)


        If count = 0 Then
            trUntis.Visible = False
        Else
            trUntis.Visible = True
        End If
    End Sub

    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

 
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvOption_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"

    Sub updateSubjectURL()
        hfSubjectURL.Value = "../Curriculum/clmShowSubjects.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                          & "&grdid=" + ddlGrade.SelectedValue + "&stmid=" + ddlStream.SelectedValue.ToString
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub

    Function SetTeacherDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "EMP_NAME"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "FROMDATE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "EMP_ID"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_SCHEDULE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_ROOMNO"
        dt.Columns.Add(column)


        dt.PrimaryKey = keys
        Return dt
    End Function


    Sub SetOption()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strquery As String

        strquery = " SELECT CASE SBG_PARENT_ID WHEN 0 THEN SBG_ID ELSE SBG_PARENT_ID END FROM SUBJECTS_GRADE_S WHERE SBG_ID=" + ddlSubjects.SelectedItem.Value
        hfSBG_PARENT_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery).ToString

        'strQuery = " SELECT ISNULL(SBG_bOPTIONAL,'FALSE') FROM SUBJECTS_GRADE_S WHERE SBG_ID=" + hfSBG_ID.Value
        'IF THE OPTIONAL SUBJECTS HAVE CHILD SUBJECTS THEN CREATE PARENT AS OPTIONAL AND CHILD SUBJECTS AS COMMON

        strQuery = " SELECT ISNULL(SBG_bOPTIONAL,'FALSE') FROM SUBJECTS_GRADE_S WHERE SBG_ID=" + hfSBG_PARENT_ID.Value
        hfBOptional.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery).ToString

    End Sub
    Sub GetTeacher()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        'if the login user is a teacher then display only the groups for that teacher
        Dim empId As Integer = studClass.GetEmpId(Session("sUsr_name"))
        Dim str_query As String
       
        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')," _
                                            & " FROMDATE=SGS_FROMDATE,EMP_ID,SGS_ID,ISNULL(SGS_SCHEDULE,'') AS SGS_SCHEDULE,ISNULL(SGS_ROOMNO,'') AS SGS_ROOMNO" _
                                            & "  FROM EMPLOYEE_M AS A INNER JOIN GROUPS_TEACHER_S AS B " _
                                            & " ON A.EMP_ID=B.SGS_EMP_ID WHERE SGS_SGR_ID=" + hfSGR_ID.Value + " AND " _
                                            & " SGS_TODATE IS NULL"
        ElseIf studClass.isEmpTeacher(empId) = True Then
            str_query = "SELECT EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')," _
                                            & " FROMDATE=SGS_FROMDATE,EMP_ID,SGS_ID,ISNULL(SGS_SCHEDULE,'') AS SGS_SCHEDULE,ISNULL(SGS_ROOMNO,'') AS SGS_ROOMNO" _
                                            & " FROM EMPLOYEE_M AS A INNER JOIN GROUPS_TEACHER_S AS B " _
                                            & " ON A.EMP_ID=B.SGS_EMP_ID WHERE SGS_SGR_ID=" + hfSGR_ID.Value + " AND " _
                                            & " SGS_TODATE IS NULL AND SGS_EMP_ID=" + empId.ToString
        End If
        ' & " ISNULL(SGS_TODATE,'1900-01-01') BETWEEN '1900-01-01' AND '" + Format(Now.Date, "yyyy-MM-dd") + "'"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = SetTeacherDataTable()
        Dim dr As DataRow
        While reader.Read
            dr = dt.NewRow
            dr.Item(0) = reader.GetString(0)
            dr.Item(1) = Format(reader.GetDateTime(1), "dd/MMM/yyyy")
            dr.Item(2) = reader.GetValue(2).ToString
            dr.Item(3) = "edit"
            dr.Item(4) = reader.GetValue(3)
            dr.Item(5) = reader.GetString(4)
            dr.Item(6) = reader.GetString(5)
            dt.Rows.Add(dr)
        End While
        reader.Close()
        gvTeacher.DataSource = dt
        gvTeacher.DataBind()
        Session("dtTeacher") = dt
    End Sub
    Sub AddTeacher()
        Dim dt As DataTable = Session("dtTeacher")

        Dim dr As DataRow

        Dim keys As Object()
        ReDim keys(0)
        keys(0) = ddlTeacher.SelectedValue.ToString


        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This teacher is already added"
            Exit Sub
        End If

        dr = dt.NewRow
        dr.Item(0) = ddlTeacher.SelectedItem.Text
        dr.Item(1) = txtFrom.Text
        dr.Item(2) = ddlTeacher.SelectedValue.ToString
        dr.Item(3) = "add"
        dr.Item(4) = 0
        dr.Item(5) = txtSchedule.Text
        dr.Item(6) = txtRoomNo.Text
        dt.Rows.Add(dr)

        gvTeacher.DataSource = dt
        gvTeacher.DataBind()
        Session("dtTeacher") = dt
    End Sub


    Sub EditTeacher()
        Dim dt As DataTable = Session("dtTeacher")

        Dim dr As DataRow

        Dim index As Integer = ViewState("SelectedRow")
        With dt.Rows(index)
            If .Item(3) = "edit" Then
                .Item(3) = "update"
            End If
            .Item(5) = txtSchedule.Text
            .Item(6) = txtRoomNo.Text
        End With

        gvTeacher.DataSource = dt
        gvTeacher.DataBind()
        Session("dtTeacher") = dt
    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    'Sub PopulateGradeSubjects()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT A.SBG_ID AS SBG_ID,SUBJECT=CASE A.SBG_PARENT_ID WHEN 0 THEN A.SBG_DESCR" _
    '                             & " ELSE A.SBG_DESCR+'-'+B.SBG_SHORTCODE END FROM " _
    '                             & " SUBJECTS_GRADE_S AS A LEFT OUTER JOIN SUBJECTS_GRADE_S AS B " _
    '                             & " ON A.SBG_PARENT_ID=B.SBG_SBM_ID" _
    '                             & " AND B.SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND B.SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND B.SBG_STM_ID=" + ddlStream.SelectedValue.ToString _
    '                             & " WHERE A.SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND A.SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND A.SBG_STM_ID=" + ddlStream.SelectedValue.ToString _
    '                             & " ORDER BY SUBJECT"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlSubject.DataSource = ds
    '    ddlSubject.DataTextField = "SUBJECT"
    '    ddlSubject.DataValueField = "SBG_ID"
    '    ddlSubject.DataBind()
    'End Sub

    Public Sub PopulateGradeStream()

        'ddlStream.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT distinct stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
        '                         & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
        '                         & " grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade.SelectedValue.ToString + "' and grm_shf_id=" + ddlShift.SelectedValue.ToString
        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'ddlStream.DataSource = ds
        'ddlStream.DataTextField = "stm_descr"
        'ddlStream.DataValueField = "stm_id"
        'ddlStream.DataBind()
        'If Not ddlStream.Items.FindByValue("1") Is Nothing Then
        '    ddlStream.Items.FindByValue("1").Selected = True
        'End If
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue())
            PARAM(1) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub PopulateTeacher()
        ddlTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim empId As Integer = studClass.GetEmpId(Session("sUsr_name"))
        Dim str_query As String
        Dim empTeacher As Boolean = studClass.isEmpTeacher(empId)

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                          & "emp_id FROM employee_m WHERE EMP_ECT_ID IN(1,3,4) and emp_bsu_id='" + Session("sBsuid") + "' AND EMP_STATUS IN(1,2) order by emp_fname,emp_mname,emp_lname"
        Else
            str_query = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                    & "emp_id FROM employee_m WHERE EMP_ID=" + empId.ToString
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTeacher.DataSource = ds
        ddlTeacher.DataTextField = "emp_name"
        ddlTeacher.DataValueField = "emp_id"
        ddlTeacher.DataBind()
        If empTeacher = False Then
            Dim li As New ListItem
            li.Text = ""
            li.Value = 0
            ddlTeacher.Items.Insert(0, li)
        End If
    End Sub

    Sub GridBind(ByVal gvGrid As GridView)

        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String
        Dim options As Integer
        SetOption()
        If ViewState("stumode") = "add" Then


            If hfBOptional.Value = "True" Then
                'IF OPTIONAL SUBJECTS ARE THERE THEN STUDENTS WILL BE DISPLAYED ONLY AFTER ALL OPTIONAL SUBJECTS ARE ALLOCATED


                'SELECT MIN OPTIONS FROM GRADE_MINOPTIONSETTING.IF IT IS 0 THEN NO. OF OPTIONS WILL BE TOTAL OPTION BUCKETS GRADE

                strQuery = "SELECT GRO_OPTIONS FROM OASIS_CURRICULUM..GRADE_MINOPTIONSETTING WHERE  " _
                         & " GRO_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                         & " AND GRO_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                         & " AND GRO_STM_ID=" + ddlStream.SelectedValue.ToString
                options = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

                If options = 0 Then
                    strQuery = "SELECT COUNT(DISTINCT SGO_OPT_ID) FROM SUBJECTS_GRADE_S AS A" _
                             & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
                             & " WHERE  SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                             & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_STM_ID=" + ddlStream.SelectedValue.ToString
                    options = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
                End If

                If hfSBG_PARENT_ID.Value = ddlSubjects.SelectedItem.Value Then
                    strQuery = "SELECT STU_ID,SSD_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                                      & " SCT_DESCR,STU_SCT_ID,isnull(OPT_DESCR,'') as OPT_DESCR FROM STUDENT_M  AS A INNER JOIN" _
                                      & " STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                                      & " LEFT OUTER JOIN OPTIONS_M AS P ON B.SSD_OPT_ID=P.OPT_ID" _
                                      & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                                      & " WHERE STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                      & " AND STU_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                                      & " AND STU_SHF_ID=" + ddlShift.SelectedValue.ToString _
                                      & " AND STU_STM_ID=" + ddlStream.SelectedValue.ToString _
                                      & " AND SSD_SBG_ID=" + ddlSubjects.SelectedItem.Value _
                                      & " AND (SELECT COUNT(SSD_OPT_ID) FROM STUDENT_GROUPS_S WHERE SSD_STU_ID=A.STU_ID " _
                                      & " AND SSD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SSD_OPT_ID IS NOT NULL)>=" + options.ToString _
                                      & " AND ISNULL(SSD_SGR_ID,0) = 0 " _
                                      & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                                      & " AND STU_CURRSTATUS NOT IN('CN','TF') "
                Else
                    strQuery = "SELECT STU_ID,SSD_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                             & " SCT_DESCR,STU_SCT_ID,Isnull(OPT_DESCR,'') as OPT_DESCR FROM STUDENT_M  AS A INNER JOIN" _
                             & " STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                             & " LEFT OUTER JOIN OPTIONS_M AS P ON B.SSD_OPT_ID=P.OPT_ID" _
                             & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                             & " WHERE STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " AND STU_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                             & " AND STU_SHF_ID=" + ddlShift.SelectedValue.ToString _
                             & " AND STU_STM_ID=" + ddlStream.SelectedValue.ToString _
                             & " AND SSD_SBG_ID=" + hfSBG_PARENT_ID.Value _
                             & " AND (SELECT COUNT(SSD_OPT_ID) FROM STUDENT_GROUPS_S WHERE SSD_STU_ID=A.STU_ID " _
                             & " AND SSD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SSD_OPT_ID IS NOT NULL)>=" + options.ToString _
                             & " AND ISNULL(SSD_SGR_ID,0) = 0 " _
                             & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                             & " AND STU_CURRSTATUS NOT IN('CN','TF') " _
                             & " AND SSD_STU_ID NOT IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S WHERE SSD_STU_ID=A.STU_ID AND SSD_SGR_ID IS NOT NULL AND SSD_SBG_ID=" + ddlSubjects.SelectedItem.Value + ")"
                End If


            Else
                strQuery = "SELECT STU_ID,SSD_ID=0,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                                & " SCT_DESCR,STU_SCT_ID,OPT_DESCR='' FROM STUDENT_M  AS A " _
                                & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                                & " WHERE STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND STU_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                                & " AND STU_SHF_ID=" + ddlShift.SelectedValue.ToString _
                                & " AND STU_STM_ID=" + ddlStream.SelectedValue.ToString _
                                & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                                & " AND STU_CURRSTATUS NOT IN('CN','TF') " _
                                & " AND STU_ID NOT IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S WHERE ISNULL(SSD_SGR_ID,0)<>0 AND SSD_SBG_ID=" + ddlSubjects.SelectedItem.Value + ")"

            End If


        Else
            strQuery = "SELECT STU_ID,SSD_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                                         & " SCT_DESCR,STU_SCT_ID,Isnull(OPT_DESCR,'') as OPT_DESCR FROM STUDENT_M  AS A INNER JOIN" _
                                         & " STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                                         & " LEFT OUTER JOIN OPTIONS_M AS P ON B.SSD_OPT_ID=P.OPT_ID" _
                                         & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                                         & " WHERE SSD_SGR_ID = " + hfSGR_ID.Value
        End If


        If rdMuslim.Checked = True Then
            strQuery += " AND STU_RLG_ID='ISL'"
        End If

        If rdNonMuslim.Checked = True Then
            strQuery += " AND STU_RLG_ID<>'ISL'"
        End If

        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox

        Dim ddlgvHouse As New DropDownList
        Dim selectedHouse As String = ""

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvOption As New DropDownList

        Dim selectedSection As String = ""
        Dim selectedOption As String = ""

        Dim sectionItems As ListItemCollection
        Dim optionItems As ListItemCollection

        If gvGrid.Rows.Count > 0 Then

            ddlgvHouse = gvGrid.HeaderRow.FindControl("ddlgvHouse")

            txtSearch = gvGrid.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvGrid.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text

            ddlgvSection = gvGrid.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then

                strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text
                sectionItems = ddlgvSection.Items
            End If

            ddlgvOption = gvGrid.HeaderRow.FindControl("ddlgvOption")

            If ddlgvOption.Text <> "ALL" Then

                strFilter = strFilter + " and opt_descr='" + ddlgvOption.Text + "'"

                selectedOption = ddlgvOption.Text
                optionItems = ddlgvOption.Items
            End If




            If strFilter <> "" Then
                strQuery += strFilter
            End If
        End If

        If Session("sbsuid") = "125002" Then
            strQuery += " ORDER BY SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Else
            strQuery += " ORDER BY OPT_DESCR,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvGrid.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            'If ViewState("stumode") = "view" Then
            '    btnRemove.Visible = False
            'Else
            '    btnAllocate.Visible = False
            'End If
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGrid.DataBind()
            Dim columnCount As Integer = gvGrid.Rows(0).Cells.Count
            gvGrid.Rows(0).Cells.Clear()
            gvGrid.Rows(0).Cells.Add(New TableCell)
            gvGrid.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGrid.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            If ViewState("stumode") = "view" Then
                gvGrid.Rows(0).Cells(0).Text = "No records to view"
            Else
                gvGrid.Rows(0).Cells(0).Text = "No records to add"
            End If
        Else
            gvGrid.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvGrid.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = strName

        txtSearch = New TextBox
        txtSearch = gvGrid.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strNo



        Dim dt As DataTable = ds.Tables(0)
        If gvGrid.Rows.Count > 0 Then


            ddlgvSection = gvGrid.HeaderRow.FindControl("ddlgvSection")
            ddlgvOption = gvGrid.HeaderRow.FindControl("ddlgvOption")

            Dim dr As DataRow


            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")

            ddlgvOption.Items.Clear()
            ddlgvOption.Items.Add("ALL")

            Dim i As Integer

            If selectedSection = "" Then
                ddlgvSection.Items.Clear()
                ddlgvSection.Items.Add("ALL")


                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr
                        If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                            ddlgvSection.Items.Add(.Item(4))
                        End If
                    End With

                Next
            Else
                ddlgvSection.Items.Clear()
                For i = 0 To sectionItems.Count - 1
                    ddlgvSection.Items.Add(sectionItems.Item(i))
                Next
                ddlgvSection.Text = selectedSection
            End If

            If selectedOption = "" Then
                ddlgvOption.Items.Clear()
                ddlgvOption.Items.Add("ALL")


                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr
                        If ddlgvOption.Items.FindByText(.Item(6)) Is Nothing Then
                            ddlgvOption.Items.Add(.Item(6))
                        End If
                    End With

                Next
            Else
                ddlgvOption.Items.Clear()
                For i = 0 To optionItems.Count - 1
                    ddlgvOption.Items.Add(optionItems.Item(i))
                Next
                ddlgvOption.Text = selectedOption
            End If


        End If


        If hfBOptional.Value.ToLower = "false" Then
            gvGrid.Columns(8).Visible = False
        End If

        If Session("sbsuid") <> "125017" Then
            TB7.Rows(1).Visible = False

            gvTeacher.Columns(2).Visible = False
            gvTeacher.Columns(3).Visible = False
            gvTeacher.Columns(5).Visible = False

        End If
    End Sub

    Sub GridBindPrevYears(ByVal gvGrid As GridView)

        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String
        Dim options As Integer
        SetOption()
        If ViewState("stumode") = "add" Then


            If hfBOptional.Value = "True" Then
                'IF OPTIONAL SUBJECTS ARE THERE THEN STUDENTS WILL BE DISPLAYED ONLY AFTER ALL OPTIONAL SUBJECTS ARE ALLOCATED


                'SELECT MIN OPTIONS FROM GRADE_MINOPTIONSETTING.IF IT IS 0 THEN NO. OF OPTIONS WILL BE TOTAL OPTION BUCKETS GRADE

                strQuery = "SELECT GRO_OPTIONS FROM OASIS_CURRICULUM..GRADE_MINOPTIONSETTING WHERE  " _
                         & " GRO_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                         & " AND GRO_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                         & " AND GRO_STM_ID=" + ddlStream.SelectedValue.ToString
                options = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

                If options = 0 Then
                    strQuery = "SELECT COUNT(DISTINCT SGO_OPT_ID) FROM SUBJECTS_GRADE_S AS A" _
                             & " INNER JOIN SUBJECTGRADE_OPTIONS_S AS B ON A.SBG_ID=B.SGO_SBG_ID" _
                             & " WHERE  SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                             & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_STM_ID=" + ddlStream.SelectedValue.ToString
                    options = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
                End If

                If hfSBG_PARENT_ID.Value = ddlSubjects.SelectedItem.Value Then
                    strQuery = "SELECT STU_ID,SSD_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                                      & " SCT_DESCR,STU_SCT_ID,isnull(OPT_DESCR,'') as OPT_DESCR FROM STUDENT_M  AS A INNER JOIN" _
                                      & " STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                                      & " INNER JOIN OASIS..STUDENT_PROMO_S ON STP_STU_ID=STU_ID" _
                                      & " LEFT OUTER JOIN OPTIONS_M AS P ON B.SSD_OPT_ID=P.OPT_ID" _
                                      & " INNER JOIN SECTION_M AS C ON STP_SCT_ID=C.SCT_ID" _
                                      & " WHERE STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                      & " AND STP_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                                      & " AND STU_SHF_ID=" + ddlShift.SelectedValue.ToString _
                                      & " AND STU_STM_ID=" + ddlStream.SelectedValue.ToString _
                                      & " AND SSD_SBG_ID=" + ddlSubjects.SelectedItem.Value _
                                      & " AND (SELECT COUNT(SSD_OPT_ID) FROM STUDENT_GROUPS_S WHERE SSD_STU_ID=A.STU_ID " _
                                      & " AND SSD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SSD_OPT_ID IS NOT NULL)>=" + options.ToString _
                                      & " AND ISNULL(SSD_SGR_ID,0) = 0 " _
                                      & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                                      & " AND STU_CURRSTATUS NOT IN('CN','TF') "
                Else
                    strQuery = "SELECT STU_ID,SSD_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                             & " SCT_DESCR,STU_SCT_ID,Isnull(OPT_DESCR,'') as OPT_DESCR FROM STUDENT_M  AS A INNER JOIN" _
                             & " STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                             & " LEFT OUTER JOIN OPTIONS_M AS P ON B.SSD_OPT_ID=P.OPT_ID" _
                             & " INNER JOIN OASIS..STUDENT_PROMO_S ON STP_STU_ID=STU_ID" _
                             & " INNER JOIN SECTION_M AS C ON STP_SCT_ID=C.SCT_ID" _
                             & " WHERE STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " AND STP_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                             & " AND STU_SHF_ID=" + ddlShift.SelectedValue.ToString _
                             & " AND STU_STM_ID=" + ddlStream.SelectedValue.ToString _
                             & " AND SSD_SBG_ID=" + hfSBG_PARENT_ID.Value _
                             & " AND (SELECT COUNT(SSD_OPT_ID) FROM STUDENT_GROUPS_S WHERE SSD_STU_ID=A.STU_ID " _
                             & " AND SSD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SSD_OPT_ID IS NOT NULL)>=" + options.ToString _
                             & " AND ISNULL(SSD_SGR_ID,0) = 0 " _
                             & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                             & " AND STU_CURRSTATUS NOT IN('CN','TF') " _
                             & " AND SSD_STU_ID NOT IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S WHERE SSD_STU_ID=A.STU_ID AND SSD_SGR_ID IS NOT NULL AND SSD_SBG_ID=" + ddlSubjects.SelectedItem.Value + ")"
                End If


            Else
                strQuery = "SELECT STU_ID,SSD_ID=0,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                                & " SCT_DESCR,STU_SCT_ID,OPT_DESCR='' FROM STUDENT_M  AS A " _
                                & " INNER JOIN OASIS..STUDENT_PROMO_S ON STP_STU_ID=STU_ID" _
                                & " INNER JOIN SECTION_M AS C ON STP_SCT_ID=C.SCT_ID" _
                                & " WHERE STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND STP_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                                & " AND STU_SHF_ID=" + ddlShift.SelectedValue.ToString _
                                & " AND STU_STM_ID=" + ddlStream.SelectedValue.ToString _
                                & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                                & " AND STU_CURRSTATUS NOT IN('CN','TF') " _
                                & " AND STU_ID NOT IN(SELECT SSD_STU_ID FROM STUDENT_GROUPS_S WHERE ISNULL(SSD_SGR_ID,0)<>0 AND SSD_SBG_ID=" + ddlSubjects.SelectedItem.Value + ")"

            End If


        Else
            strQuery = "SELECT STU_ID,SSD_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                                         & " SCT_DESCR,STU_SCT_ID,Isnull(OPT_DESCR,'') as OPT_DESCR FROM STUDENT_M  AS A " _
                                         & " INNER JOIN OASIS..STUDENT_PROMO_S ON STP_STU_ID=STU_ID" _
                                         & " INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                                         & " LEFT OUTER JOIN OPTIONS_M AS P ON B.SSD_OPT_ID=P.OPT_ID" _
                                         & " INNER JOIN SECTION_M AS C ON STP_SCT_ID=C.SCT_ID" _
                                         & " WHERE SSD_SGR_ID = " + hfSGR_ID.Value _
                                         & " AND STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        End If


        If rdMuslim.Checked = True Then
            strQuery += " AND STU_RLG_ID='ISL'"
        End If

        If rdNonMuslim.Checked = True Then
            strQuery += " AND STU_RLG_ID<>'ISL'"
        End If

        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox

        Dim ddlgvHouse As New DropDownList
        Dim selectedHouse As String = ""

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvOption As New DropDownList

        Dim selectedSection As String = ""
        Dim selectedOption As String = ""

        Dim sectionItems As ListItemCollection
        Dim optionItems As ListItemCollection

        If gvGrid.Rows.Count > 0 Then

            ddlgvHouse = gvGrid.HeaderRow.FindControl("ddlgvHouse")

            txtSearch = gvGrid.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvGrid.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text

            ddlgvSection = gvGrid.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then

                strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text
                sectionItems = ddlgvSection.Items
            End If

            ddlgvOption = gvGrid.HeaderRow.FindControl("ddlgvOption")

            If ddlgvOption.Text <> "ALL" Then

                strFilter = strFilter + " and opt_descr='" + ddlgvOption.Text + "'"

                selectedOption = ddlgvOption.Text
                optionItems = ddlgvOption.Items
            End If




            If strFilter <> "" Then
                strQuery += strFilter
            End If
        End If

        If Session("sbsuid") = "125002" Then
            strQuery += " ORDER BY SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Else
            strQuery += " ORDER BY OPT_DESCR,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvGrid.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            'If ViewState("stumode") = "view" Then
            '    btnRemove.Visible = False
            'Else
            '    btnAllocate.Visible = False
            'End If
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGrid.DataBind()
            Dim columnCount As Integer = gvGrid.Rows(0).Cells.Count
            gvGrid.Rows(0).Cells.Clear()
            gvGrid.Rows(0).Cells.Add(New TableCell)
            gvGrid.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGrid.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            If ViewState("stumode") = "view" Then
                gvGrid.Rows(0).Cells(0).Text = "No records to view"
            Else
                gvGrid.Rows(0).Cells(0).Text = "No records to add"
            End If
        Else
            gvGrid.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvGrid.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = strName

        txtSearch = New TextBox
        txtSearch = gvGrid.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strNo



        Dim dt As DataTable = ds.Tables(0)
        If gvGrid.Rows.Count > 0 Then


            ddlgvSection = gvGrid.HeaderRow.FindControl("ddlgvSection")
            ddlgvOption = gvGrid.HeaderRow.FindControl("ddlgvOption")

            Dim dr As DataRow


            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")

            ddlgvOption.Items.Clear()
            ddlgvOption.Items.Add("ALL")

            Dim i As Integer

            If selectedSection = "" Then
                ddlgvSection.Items.Clear()
                ddlgvSection.Items.Add("ALL")


                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr
                        If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                            ddlgvSection.Items.Add(.Item(4))
                        End If
                    End With

                Next
            Else
                ddlgvSection.Items.Clear()
                For i = 0 To sectionItems.Count - 1
                    ddlgvSection.Items.Add(sectionItems.Item(i))
                Next
                ddlgvSection.Text = selectedSection
            End If

            If selectedOption = "" Then
                ddlgvOption.Items.Clear()
                ddlgvOption.Items.Add("ALL")


                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr
                        If ddlgvOption.Items.FindByText(.Item(6)) Is Nothing Then
                            ddlgvOption.Items.Add(.Item(6))
                        End If
                    End With

                Next
            Else
                ddlgvOption.Items.Clear()
                For i = 0 To optionItems.Count - 1
                    ddlgvOption.Items.Add(optionItems.Item(i))
                Next
                ddlgvOption.Text = selectedOption
            End If


        End If


        If hfBOptional.Value.ToLower = "false" Then
            gvGrid.Columns(8).Visible = False
        End If

        If Session("sbsuid") <> "125017" Then
            TB7.Rows(1).Visible = False

            gvTeacher.Columns(2).Visible = False
            gvTeacher.Columns(3).Visible = False
            gvTeacher.Columns(5).Visible = False

        End If
    End Sub


    Public Function getSerialNoAdd()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvAdd.PageSize * gvAdd.PageIndex)
    End Function
    Public Function getSerialNoView()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvView.PageSize * gvView.PageIndex)
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvView.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvView.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAdd.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAdd.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAdd.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAdd.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub highlight_grid()
        If ViewState("stumode") = "view" Then
            For i As Integer = 0 To gvView.Rows.Count - 1
                Dim row As GridViewRow = gvView.Rows(i)
                Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
                If isSelect Then
                    row.BackColor = Drawing.Color.FromName("#f6deb2")
                Else
                    row.BackColor = Drawing.Color.Transparent
                End If
            Next
        Else
            For i As Integer = 0 To gvAdd.Rows.Count - 1
                Dim row As GridViewRow = gvAdd.Rows(i)
                Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
                If isSelect Then
                    row.BackColor = Drawing.Color.FromName("#f6deb2")
                Else
                    row.BackColor = Drawing.Color.Transparent
                End If
            Next
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim transaction As SqlTransaction
        Dim str_query As String


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If ViewState("datamode") = "edit" Or ViewState("datamode") = "delete" Then
                    UtilityObj.InsertAuditdetails(transaction, ViewState("datamode"), "GROUPS_M", "SGR_ID", "SGR_SBG_ID", "SGR_ID=" + hfSGR_ID.Value.ToString)
                End If


                str_query = "exec saveGROUP_M  " _
                                         & hfSGR_ID.Value.ToString + "," _
                                         & "'" + Session("sbsuid") + "'," _
                                         & ddlAcademicYear.SelectedValue.ToString + "," _
                                         & "'" + ddlGrade.SelectedValue + "'," _
                                         & "'" + ddlGrade.SelectedItem.Text + "'," _
                                         & ddlShift.SelectedValue.ToString + "," _
                                         & "'" + ddlShift.SelectedItem.Text + "'," _
                                         & ddlStream.SelectedValue.ToString + "," _
                                         & "'" + ddlStream.SelectedItem.Text + "'," _
                                         & ddlSubjects.SelectedItem.Value + "," _
                                         & "'" + txtDescr.Text + "'," _
                                         & "'" + ViewState("datamode") + "'," _
                                         & "'" + txtUntisGroup.Text + "'"
                hfSGR_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                SaveTeacher(transaction)

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using

       
    End Sub
    Sub SaveTeacher(ByVal transaction As SqlTransaction)
        Dim str_query As String
        Dim dt As DataTable = Session("dtTeacher")
        Dim dr As DataRow
        For Each dr In dt.Rows
            If dr.Item(3) <> "edit" And dr.Item(3) <> "remove" Then
                str_query = "exec saveGROUPTEACHER " + dr.Item(4) + "," _
                                        & hfSGR_ID.Value.ToString + "," _
                                        & dr.Item(2) + "," _
                                        & "'" + Format(Date.Parse(dr.Item(1)), "MM/dd/yyyy") + "'," _
                                        & "'" + dr.Item(3) + "'," _
                                        & "'" + dr.Item(5) + "'," _
                                        & "'" + dr.Item(6) + "'," _
                                        & "'" + Session("sUsr_name") + "'"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
            End If
        Next
    End Sub

    Function ValidateTeacher() As Boolean
        If gvTeacher.Rows.Count = 0 Then
            If ddlTeacher.SelectedValue <> 0 And txtFrom.Text = "" Then
                lblError.Text = "Please enter data in the field from date"
                Return False
            End If
        End If
        Return True
    End Function



    Function isGroupNameExists() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(SGR_ID) FROM GROUPS_M WHERE SGR_DESCR='" + txtDescr.Text + "' AND SGR_BSU_ID='" + Session("sbsuid") + "'" _
                                & " AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Function isStudentsExits() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(SSD_ID) FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID=" + hfSGR_ID.Value.ToString
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Sub EnableDisableControls(ByVal value As Boolean)
        ddlAcademicYear.Enabled = value
        ddlSubjects.Enabled = value
        ddlGrade.Enabled = value
        ddlShift.Enabled = value
        ddlStream.Enabled = value
        txtDescr.Enabled = value
        'ddlSubject.Enabled = value
        ddlTeacher.Enabled = value
        txtFrom.Enabled = value
        btnAddNew.Enabled = value
        gvTeacher.Enabled = value
        imgFrom.Enabled = value
        txtUntisGroup.Enabled = value
    End Sub

    Sub SaveStudGroup()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim chkSelect As CheckBox
        Dim lblSsdId As Label
        Dim lblSctId As Label
        Dim lblStuId As Label
        Dim i As Integer
        SetOption()
        If ViewState("stumode") = "add" Then
            If hfBOptional.Value = "True" Then
                If hfSBG_PARENT_ID.Value = ddlSubjects.SelectedItem.Value Then
                    For i = 0 To gvAdd.Rows.Count - 1
                        chkSelect = gvAdd.Rows(i).FindControl("chkSelect")
                        If chkSelect.Checked = True Then

                            lblSsdId = gvAdd.Rows(i).FindControl("lblSsdid")
                            str_query = "exec saveSTUDENTGROUP " + hfSGR_ID.Value.ToString + "," + lblSsdId.Text + ",'add'"
                            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                        End If
                    Next
                Else

                    'if the optional  subjects has child subjects the add students to the child subject
                    For i = 0 To gvAdd.Rows.Count - 1
                        chkSelect = gvAdd.Rows(i).FindControl("chkSelect")
                        If chkSelect.Checked = True Then
                            lblSsdId = gvAdd.Rows(i).FindControl("lblSsdid")
                            lblSctId = gvAdd.Rows(i).FindControl("lblSctid")
                            lblStuId = gvAdd.Rows(i).FindControl("lblStuid")
                            str_query = "exec saveSTUDENTCOMMONSUBJECTGROUP 0," _
                                      & ddlAcademicYear.SelectedValue.ToString + "," _
                                      & "'" + ddlGrade.SelectedValue + "'," _
                                      & ddlSubjects.SelectedItem.Value + "," _
                                      & hfSGR_ID.Value.ToString + "," _
                                      & lblSctId.Text + "," _
                                      & lblStuId.Text + ",'add'"
                            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                        End If
                    Next

                End If


            Else
                For i = 0 To gvAdd.Rows.Count - 1
                    chkSelect = gvAdd.Rows(i).FindControl("chkSelect")
                    If chkSelect.Checked = True Then
                        lblSsdId = gvAdd.Rows(i).FindControl("lblSsdid")
                        lblSctId = gvAdd.Rows(i).FindControl("lblSctid")
                        lblStuId = gvAdd.Rows(i).FindControl("lblStuid")
                        str_query = "exec saveSTUDENTCOMMONSUBJECTGROUP 0," _
                                  & ddlAcademicYear.SelectedValue.ToString + "," _
                                  & "'" + ddlGrade.SelectedValue + "'," _
                                  & ddlSubjects.SelectedItem.Value + "," _
                                  & hfSGR_ID.Value.ToString + "," _
                                  & lblSctId.Text + "," _
                                  & lblStuId.Text + ",'add'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    End If
                Next
            End If
        Else
            If hfBOptional.Value = "True" Then
                For i = 0 To gvView.Rows.Count - 1
                    chkSelect = gvView.Rows(i).FindControl("chkSelect")
                    If chkSelect.Checked = True Then
                        lblSsdId = gvView.Rows(i).FindControl("lblSsdid")
                        str_query = "exec saveSTUDENTGROUP " + hfSGR_ID.Value.ToString + "," + lblSsdId.Text + ",'delete'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    End If
                Next
            Else
                For i = 0 To gvView.Rows.Count - 1
                    chkSelect = gvView.Rows(i).FindControl("chkSelect")
                    If chkSelect.Checked = True Then
                        lblSsdId = gvView.Rows(i).FindControl("lblSsdid")
                        lblSctId = gvView.Rows(i).FindControl("lblSctid")
                        lblStuId = gvView.Rows(i).FindControl("lblStuid")
                        str_query = "exec saveSTUDENTCOMMONSUBJECTGROUP " _
                                  & lblSsdId.Text + "," _
                                  & ddlAcademicYear.SelectedValue.ToString + "," _
                                  & "'" + ddlGrade.SelectedValue + "'," _
                                  & ddlSubjects.SelectedItem.Value + "," _
                                  & hfSGR_ID.Value.ToString + "," _
                                  & lblSctId.Text + "," _
                                  & lblStuId.Text + ",'delete'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    End If
                Next
            End If

        End If
    End Sub

    Function checkAssesmentScheduled() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(CAS_ID) FROM ACT.ACTIVITY_SCHEDULE WHERE CAS_SGR_ID=" + hfSGR_ID.Value
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count <> 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Function checkReportDataAdded() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(RST_ID) FROM RPT.REPORT_STUDENT_S " _
                                & " WHERE RST_SGR_ID=" + hfSGR_ID.Value
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count <> 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindTeacher(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetTeacherDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(3) <> "delete" And dt.Rows(i)(3) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvTeacher.DataSource = dtTemp
        gvTeacher.DataBind()
    End Sub

    Private Sub ResetScrollPosition()
        If Not ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "CreateResetScrollPosition") Then
            'Create the ResetScrollPosition() function
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "CreateResetScrollPosition", _
                             "function ResetScrollPosition() {" & vbCrLf & _
                             " var scrollX = document.getElementById('__SCROLLPOSITIONX');" & vbCrLf & _
                             " var scrollY = document.getElementById('__SCROLLPOSITIONY');" & vbCrLf & _
                             " if (scrollX && scrollY) {" & vbCrLf & _
                             "    scrollX.value = 0;" & vbCrLf & _
                             "    scrollY.value = 0;" & vbCrLf & _
                             " }" & vbCrLf & _
                             "}", True)

            'Add the call to the ResetScrollPosition() function
            ClientScript.RegisterStartupScript(Me.GetType(), "CallResetScrollPosition", "ResetScrollPosition();", True)
        End If
    End Sub

#End Region


    'Protected Sub lbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAdd.Click
    '    Try
    '        mvMaster.ActiveViewIndex = 1
    '        lbView.Visible = True
    '        If ViewState("stumode") = "view" Then
    '            ViewState("stumode") = "add"
    '            gvAdd.DataBind()
    '            GridBind(gvAdd)
    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub

    'Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbView.Click
    '    Try
    '        mvMaster.ActiveViewIndex = 0
    '        lblError.Text = ""
    '        lbAdd.Visible = True
    '        If ViewState("stumode") = "add" Then
    '            ViewState("stumode") = "view"
    '            btnAllocate.Visible = True
    '            btnRemove.Visible = True
    '            gvView.DataBind()
    '            GridBind(gvView)
    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            If ValidateTeacher() = True Then
                If ViewState("datamode") = "add" Then
                    mvMaster.ActiveViewIndex = 1
                    If isGroupNameExists() = False Then
                        SaveData()
                    Else
                        lblError.Text = "The group " + txtDescr.Text + " already exists for the academicyear" + ddlAcademicYear.SelectedItem.Text
                        Exit Sub
                    End If
                    GridBind(gvAdd)
                End If
                If ViewState("datamode") = "edit" Then
                    If hfGROUP.Value.Trim <> txtDescr.Text.Trim Then
                        If isGroupNameExists() = False Then
                            SaveData()
                        Else
                            lblError.Text = "The group " + txtDescr.Text + " already exists for the academicyear" + ddlAcademicYear.SelectedItem.Text
                            Exit Sub
                        End If
                    Else
                        SaveData()
                    End If
                End If
                ViewState("datamode") = "view"
                hfAddNew.Value = "add"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                EnableDisableControls(False)
                rdAll.Visible = True
                rdMuslim.Visible = True
                rdNonMuslim.Visible = True

                mnuMaster.Visible = True
                'mnuMaster.Items(0).ImageUrl = "~/Images/tabmenu/btnViewStudents1.jpg"
                'mnuMaster.Items(1).ImageUrl = "~/Images/tabmenu/btnAllocateStudents2.jpg"
          
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "delete"
            If isStudentsExits() = False Then
                SaveData()
            Else
                lblError.Text = "Cannot delete the groups for which students are allocated"
                Exit Sub
            End If
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            txtDescr.Enabled = True
            ddlTeacher.Enabled = True
            txtFrom.Enabled = True
            btnAddNew.Enabled = True
            gvTeacher.Enabled = True
            txtUntisGroup.Enabled = True
            imgFrom.Enabled = True
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllocate.Click
        Try
            ' lbView.Visible = True
            ' lbView.Width = 70
            SaveStudGroup()
            GridBind(gvAdd)
            lblError.Text = "Records added successfully"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
            'lblError1.BackColor = Drawing.Color.Wheat
        End Try
    End Sub

    Protected Sub gvAdd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAdd.PageIndexChanging
        Try
            gvAdd.PageIndex = e.NewPageIndex
            GridBind(gvAdd)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvView.PageIndexChanging
        Try
            gvView.PageIndex = e.NewPageIndex
            GridBind(gvView)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            If ViewState("datamode") = "add" Then
                PopulateGradeStream()
                ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString, "", ddlStream.SelectedValue())
                ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)

                ' PopulateGradeSubjects()
                BindSubjects()
                updateSubjectURL()
                txtDescr.Text = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlGrade.SelectedItem.Text.ToString + "_"
                hfGroupName.Value = txtDescr.Text
                GetGroupName()
                hfDescr.Value = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlGrade.SelectedItem.Text.ToString + "_"
                txtSubject.Text = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            If ViewState("datamode") = "add" Then
                ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                'PopulateGradeStream()
                ' PopulateGradeSubjects()
                BindSubjects()
                updateSubjectURL()
                txtDescr.Text = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlGrade.SelectedItem.Text.ToString + "_"
                hfGroupName.Value = txtDescr.Text
                GetGroupName()
                hfDescr.Value = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlGrade.SelectedItem.Text.ToString + "_"
                txtSubject.Text = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Try
            If ViewState("datamode") = "add" Then
                'PopulateGradeStream()
                'PopulateGradeSubjects()
                BindSubjects()
                GetGroupName()
                updateSubjectURL()
                txtSubject.Text = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

   
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Try
            If ViewState("datamode") = "add" Then
                'PopulateGradeSubjects()
                ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString, "", ddlStream.SelectedValue())
                ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue.ToString, ddlAcademicYear.SelectedValue.ToString)
                BindSubjects()
                GetGroupName()
                updateSubjectURL()
                txtSubject.Text = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            If checkAssesmentScheduled() = True Then
                lblError1.Text = "Cannot remove students from this group as assesments are already created for this group." _
                              & vbCrLf & " Note:To move students from one group to another please use the student change group option."
                'lblError1.BackColor = Drawing.Color.Wheat
                Exit Sub
            End If
            If checkReportDataAdded() = True Then
                lblError1.Text = "Cannot remove students from this group as data is already add for report processing." _
                                 & vbCrLf & " Note:To move students from one group to another please use the student change group option."
                'lblError1.BackColor = Drawing.Color.Wheat
                Exit Sub
            End If
            SaveStudGroup()
            GridBind(gvView)
            lblError.Text = "Records removed successfully"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError1.Text = "Request could not be processed"
            'lblError1.BackColor = Drawing.Color.Wheat
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try

            If ViewState("datamode") = "add" Then
                txtDescr.Text = ""
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            ElseIf ViewState("datamode") = "edit" Then
                txtDescr.Enabled = False
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            hfAddNew.Value = "add"
            If ViewState("SelectedRow") = -1 Then
                AddTeacher()
            Else
                EditTeacher()
                txtFrom.Enabled = True
                txtFrom.Text = ""
            End If
            txtSchedule.Text = ""
            txtRoomNo.Text = ""
            ViewState("SelectedRow") = -1
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTeacher_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTeacher.RowCommand
        Try

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvTeacher.Rows(index), GridViewRow)
            Dim keys As Object()
            Dim dt As DataTable
            dt = Session("dtTeacher")

            Dim lblEmpId As Label


            lblEmpId = selectedRow.FindControl("lblEmpId")

            ReDim keys(0)

            keys(0) = lblEmpId.Text



            If e.CommandName = "delete" Then
                If txtFrom.Text = "" Then
                    lblError.Text = "Please enter data in the field from date"
                    Exit Sub
                End If

                index = dt.Rows.IndexOf(dt.Rows.Find(keys))

                If dt.Rows(index).Item(3) = "add" Then
                    dt.Rows(index).Item(3) = "remove"
                Else
                    dt.Rows(index).Item(1) = txtFrom.Text
                    dt.Rows(index).Item(3) = "delete"
                End If
                Session("dtSubject") = dt
                BindTeacher(dt)
            ElseIf e.CommandName = "edit" Then
                ViewState("SelectedRow") = index
                btnAddNew.Text = "Update Teacher"
                With dt.Rows(index)
                    txtFrom.Text = Format(Date.Parse(.Item(1)), "dd/MMM/yyyy")
                    txtFrom.Enabled = False
                    txtSchedule.Text = .Item(5)
                    txtRoomNo.Text = .Item(6)
                End With
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTeacher_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTeacher.RowDeleting

    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' Response.Redirect(Request.Url.AbsoluteUri.Replace("4xCdg%2fcr4Xw%3d", Encr_decrData.Encrypt("add")))
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            url = String.Format("~\Curriculum\clmGroup_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    
   
    Protected Sub rdAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAll.CheckedChanged
        If rdAll.Checked = True Then
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        End If
    End Sub

    Protected Sub rdMuslim_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdMuslim.CheckedChanged
        If rdMuslim.Checked = True Then
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        End If
    End Sub

    Protected Sub rdNonMuslim_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdNonMuslim.CheckedChanged
        If rdNonMuslim.Checked = True Then
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        End If
    End Sub

    
    Protected Sub gvTeacher_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvTeacher.RowEditing

    End Sub

    Protected Sub mnuMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnuMaster.MenuItemClick
        Dim Iindex As Integer = Int32.Parse(e.Item.Value)
        Dim i As Integer
        'Make the selected menu item reflect the correct imageurl
        For i = 0 To mnuMaster.Items.Count - 1
            Select Case i
                Case 0
                    'If i = e.Item.Value Then
                    '    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnViewStudents2.jpg"
                    'Else
                    '    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnViewStudents1.jpg"
                    'End If
                    mvMaster.ActiveViewIndex = Iindex


                Case 1
                    'If i = e.Item.Value Then
                    '    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnAllocateStudents2.jpg"
                    'Else
                    '    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnAllocateStudents1.jpg"
                    'End If
                    mvMaster.ActiveViewIndex = Iindex

            End Select
        Next


        If mvMaster.ActiveViewIndex = 0 Then
            Try

                lblError.Text = ""
                 If ViewState("stumode") = "add" Then
                    ViewState("stumode") = "view"
                    btnAllocate.Visible = True
                    btnRemove.Visible = True
                    gvView.DataBind()
                    GridBind(gvView)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        Else
            Try
                If ViewState("stumode") = "view" Then
                    ViewState("stumode") = "add"
                    gvAdd.DataBind()
                    GridBind(gvAdd)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Sub BindSubjects()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        'Dim grade As String()
        'grade = ddlGrade.SelectedValue.Split("|")
        str_query = "SELECT SBG_ID,CASE WHEN SBG_PARENTS_SHORT<>'NA' THEN SBG_DESCR+'-'+SBG_PARENTS ELSE SBG_DESCR END AS SBG_DESCR,SBG_PARENTS, " _
                      & " OPT=CASE SBG_bOPTIONAL WHEN 'TRUE' THEN 'Yes' ELSE 'No' END,GRM_DISPLAY='',SBG_PARENTS_SHORT,SBG_GRD_ID " _
                      & " FROM SUBJECTS_GRADE_S WHERE SBG_ACD_ID = " + ddlAcademicYear.SelectedItem.Value _
                      & " AND SBG_GRD_ID='" + ddlGrade.SelectedItem.Value + "' AND SBG_STM_ID=" + ddlStream.SelectedItem.Value


        'If grade(1) <> "0" And grade(1) <> "" Then
        '    str_query += " AND SBG_STM_ID=" + grade(1)
        'End If



        If str_query <> "" Then
            str_query += " ORDER BY SBG_DESCR,SBG_PARENTS,SBG_GRD_ID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                ddlSubjects.DataSource = ds
                ddlSubjects.DataTextField = "SBG_DESCR"
                ddlSubjects.DataValueField = "SBG_ID"
                ddlSubjects.DataBind()
            End If
        End If
    End Sub

    Protected Sub ddlSubjects_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubjects.SelectedIndexChanged
        GetGroupName()
    End Sub

    Sub GetGroupName()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        str_query = "SELECT SBG_SHORTCODE FROM SUBJECTS_GRADE_S WHERE SBG_ID=" + ddlSubjects.SelectedItem.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtDescr.Text = hfGroupName.Value + ds.Tables(0).Rows(0).Item(0)
            End If
        End If
    End Sub
End Class
