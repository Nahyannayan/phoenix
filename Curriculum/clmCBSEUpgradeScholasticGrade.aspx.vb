Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Math
Partial Class Curriculum_clmCBSEUpgradeScholasticGrade
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330018") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindPrintedFor()
                    BindGrade()
                    PopulateSection()
                    tblTC.Rows(5).Visible = False
                    'tblTC.Rows(6).Visible = False


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
            gvStud.Attributes.Add("bordercolor", "#1b80b6")
        End If


    End Sub

#Region "Private Methods"



    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("Current_ACD_iD") = ddlAcademicYear.SelectedValue.ToString Then

            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                        & " STU_SCT_ID,STU_GRD_ID,GRM_DISPLAY,SCT_DESCR" _
                        & " FROM STUDENT_M  AS A" _
                        & " INNER JOIN VW_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                        & " INNER JOIN VW_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                        & " WHERE CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                        & " AND STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + hfACD_ID.Value

        Else
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                     & " STU_SCT_ID,STU_GRD_ID,GRM_DISPLAY,SCT_DESCR" _
                     & " FROM VW_STUDENT_DETAILS_PREVYEARS  AS A" _
                     & " WHERE CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
                     & " AND STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + hfACD_ID.Value
        End If

        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            Dim grade As String() = hfGRD_ID.Value.Split("|")
            str_query += " AND STU_GRD_ID= '" + grade(0) + "' AND STU_STM_ID=" + grade(1)
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox



        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ViewState("norecord") = "1"
        Else
            gvStud.DataBind()
            ViewState("norecord") = "0"
        End If



        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID,RSG_DISPLAYORDER " _
                                & " FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " INNER JOIN OASIS..STREAM_M AS D ON A.GRM_STM_ID=D.STM_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " AND GRD_ID='09'"


        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
        End If

        str_query += " ORDER BY RSG_DISPLAYORDER "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Private Sub PopulateSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + grade(0) + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)

    End Sub

    Sub BindReportCard()
        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND ISNULL(RSM_bFINALREPORT,'FALSE')='TRUE' "
        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSM_ID IN(SELECT RSG_RSM_ID FROM RPT.REPORTSETUP_GRADE_S WHERE RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|'))))"
        End If

        str_query += "ORDER BY RSM_DISPLAYORDER"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                               & " ORDER BY RPF_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Sub getHeaders()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_HEADER,RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_bDIRECTENTRY='FALSE' AND RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
                               & " ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ViewState("headersource") = ds
    End Sub

    Sub BindHeader(ByVal dlHeader As DataList)
        dlHeader.DataSource = ViewState("headersource")
        dlHeader.DataBind()
    End Sub

    Sub BindSubjects(ByVal stu_id As String, ByVal gvSubjects As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SSD_STU_ID,SBG_DESCR,SBG_ID,Sbg_Entrytype FROM SUBJECTS_GRADE_S AS A " _
                               & " INNER JOIN STUDENT_GROUPS_S AS B ON A.SBG_ID=B.SSD_SBG_ID" _
                               & " INNER JOIN RPT.REPORT_RULE_M AS D ON A.SBG_ID=D.RRM_SBG_ID" _
                               & " WHERE SSD_SGR_ID IS NOT NULL AND SSD_STU_ID='" + stu_id + "'" _
                               & " AND SSD_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                               & " AND RRM_RPF_ID=" + hfRPF_ID.Value + " AND RRM_TRM_ID=0" _
                               & " AND SBG_DESCR NOT IN('ARABIC','ISLAMIC STUDIES','MORAL SCIENCE','ISLAMIC EDUCATION'," _
                               & " 'UAE SOCIAL STUDIES')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds
        gvSubjects.DataBind()
    End Sub

    Sub BindMarks(ByVal stu_id As String, ByVal sbg_id As String, ByVal ddlTotalGrade As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim grade As String
        If Session("Current_ACD_ID") = hfACD_ID.Value Then

            str_query = "SELECT FPR_GRADING " _
               & " FROM RPT.REPORT_SETUP_D AS B LEFT OUTER JOIN RPT.FINALPROCESS_REPORT_S AS A  ON A.FPR_RSD_ID= " _
               & " B.RSD_ID AND FPR_STU_ID='" + stu_id + "' AND FPR_RPF_ID='" + hfRPF_ID.Value + "'" _
               & " AND FPR_SBG_ID='" + sbg_id + "'" _
               & " WHERE RSD_bDIRECTENTRY='FALSE' AND RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
               & " AND ISNULL(RSD_bPERFORMANCE_INDICATOR,0)=1" _
               & " ORDER BY RSD_DISPLAYORDER"

        Else

            str_query = "SELECT FPR_GRADING" _
               & " FROM RPT.REPORT_SETUP_D AS B LEFT OUTER JOIN RPT.FINALPROCESS_REPORT_PREVYEARS AS A  ON A.FPR_RSD_ID= " _
               & " B.RSD_ID AND FPR_STU_ID='" + stu_id + "' AND FPR_RPF_ID='" + hfRPF_ID.Value + "'" _
               & " AND FPR_SBG_ID='" + sbg_id + "'" _
               & " WHERE RSD_bDIRECTENTRY='FALSE' AND RSD_RSM_ID='" + hfRSM_ID.Value + "'" _
               & " AND ISNULL(RSD_bPERFORMANCE_INDICATOR,0)=1" _
               & " ORDER BY RSD_DISPLAYORDER"

        End If
        grade = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)



        If Not ddlTotalGrade.Items.FindByValue(grade) Is Nothing Then
            ddlTotalGrade.Items.FindByValue(grade).Selected = True
        End If
    End Sub

    Sub SaveData(ByVal stu_id As String, ByVal sbg_id As String, ByVal ddlTotalGrade As DropDownList, ByVal lblTotalGrade As Label)
        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim grade As String() = hfGRD_ID.Value.Split("|")
        Dim strAttendance As String = ""



        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        If ddlTotalGrade.SelectedValue <> "--" And ddlTotalGrade.SelectedValue.ToString <> lblTotalGrade.Text Then
            Dim str_query As String = "EXEC  CBSE.UPGRADESCOLASTICGRADE " _
                       & " @ACD_ID =" + ddlAcademicYear.SelectedValue.ToString + "," _
                       & " @RSM_ID =" + ddlReportCard.SelectedValue.ToString + "," _
                       & " @RPF_ID =" + ddlPrintedFor.SelectedValue.ToString + "," _
                       & " @STU_ID =" + stu_id + "," _
                       & " @SBG_ID =" + sbg_id + "," _
                       & " @GRADE =" + ddlTotalGrade.SelectedValue.ToString + "," _
                       & " @FPR_USER ='" + Session("sUsr_name") + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        End If

    End Sub

#End Region
    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Try
        Dim lblEdit As LinkButton = DirectCast(sender, LinkButton)

        Dim lblTotalGrade As Label = TryCast(sender.FindControl("lblTotalGrade"), Label)
        Dim ddlTotalGrade As DropDownList = TryCast(sender.FindControl("ddlTotalGrade"), DropDownList)



        If lblEdit.Text = "Edit" Then
            lblEdit.Text = "Update"
            ddlTotalGrade.Enabled = True
        Else
            lblEdit.Text = "Edit"
            ddlTotalGrade.Enabled = False
            Dim lblStuIdSub As Label
            Dim lblSbgId As Label
            lblStuIdSub = TryCast(sender.FindControl("lblStuIdSub"), Label)
            lblSbgId = TryCast(sender.FindControl("lblSbgId"), Label)

            SaveData(lblStuIdSub.Text, lblSbgId.Text, ddlTotalGrade, lblTotalGrade)

            lblError.Text = "Record Saved Successfully"

        End If
        ' Catch ex As Exception
        'UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        ' lblError.Text = "Request could not be processed"
        ' End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindGrade()
        PopulateSection()
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindGrade()
        PopulateSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
    End Sub

    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub



    Protected Sub gvStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand

    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gvSubjects As GridView
            gvSubjects = e.Row.FindControl("gvSubjects")
            gvSubjects.Attributes.Add("bordercolor", "#1b80b6")
            Dim lblStuId As Label

            lblStuId = e.Row.FindControl("lblStuId")
            If lblStuId.Text <> "" Then
                BindSubjects(lblStuId.Text, gvSubjects)


            End If
        End If
    End Sub
    Protected Sub gvSubjects_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStuId As Label
            Dim lblSbgId As Label

            Dim ddlTotalGrade As DropDownList
            lblSbgId = e.Row.FindControl("lblSbgId")
            lblStuId = e.Row.FindControl("lblStuIdSub")
            ddlTotalGrade = e.Row.FindControl("ddlTotalGrade")

            BindMarks(lblStuId.Text, lblSbgId.Text, ddlTotalGrade)

            Dim lblEdit As LinkButton
            lblEdit = e.Row.FindControl("lblEdit")



        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblError.Text = ""
        tblTC.Rows(5).Visible = True
        'tblTC.Rows(6).Visible = True

        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfSCT_ID.Value = ddlSection.SelectedValue
        hfSTUNO.Value = txtStuNo.Text
        hfNAME.Value = txtName.Text
        hfRSM_ID.Value = ddlReportCard.SelectedValue.ToString
        hfRPF_ID.Value = ddlPrintedFor.SelectedValue.ToString
        getHeaders()
        GridBind()
        ViewState("headersource") = Nothing
    End Sub


End Class
