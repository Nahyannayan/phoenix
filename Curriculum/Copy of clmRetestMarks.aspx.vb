Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Curriculum_clmRetestMarks
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                'Session("liUserList") = Nothing
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                ViewState("datamode") = "add"



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330180") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    ' GridBind()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)

                    BindSection()
                    tblTC.Rows(3).Visible = False
                    tblTC.Rows(4).Visible = False
                    'tblTC.Rows(5).Visible = False
                    'tblTC.Rows(6).Visible = False
                    'tblTC.Rows(7).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else
            studClass.SetChk(gvStud, Session("liUserList"))
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid

        Select Case Session("sbsuid")
            Case "121013"
                str_query += " and grm_grd_id<>'10'"
            Case "125010"
                str_query += " and grm_grd_id not in('11','12','13')"
        End Select
        str_query += " order by grd_displayorder"



        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindSection()
        Dim li As New ListItem
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()


        Dim sct_ids As String
        Dim i As Integer
        For i = 0 To ddlSection.Items.Count - 1
            If sct_ids <> "" Then
                sct_ids += ","
            End If
            sct_ids += ddlSection.Items(i).Value
        Next


        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)



    End Sub

    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String


        str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                    & " STU_DOJ,STU_SCT_ID,STU_GRD_ID,FRR_RESULT AS RESULT" _
                    & " FROM STUDENT_M  AS A" _
                    & " INNER JOIN OASIS_CURRICULUM.RPT.FINAL_RESULT_S AS B ON A.STU_ID=B.FRR_STU_ID " _
                    & " INNER JOIN STUDENT_PROMO_S AS C ON A.STU_ID=C.STP_STU_ID AND STP_ACD_ID=" + hfACD_ID.Value _
                    & " AND STU_CURRSTATUS<>'CN' AND FRR_ACD_ID=" + hfACD_ID.Value _
                    & " AND FRR_RESULT='RETEST'"
                   


        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STP_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            Dim grade As String() = hfGRD_ID.Value.Split("|")
            str_query += " AND STP_GRD_ID= '" + grade(0) + "' AND STP_STM_ID=" + grade(1)
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If


        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox



        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ViewState("norecord") = "1"
        Else
            gvStud.DataBind()
            ViewState("norecord") = "0"
        End If



    End Sub


    Sub BindMarks(ByVal stu_id As String, ByVal dlMarks As DataList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR AS SUBJECT,REPLACE(CONVERT(VARCHAR(100),FRT_MARK),'.000','') AS MARKS,FTM_GRADE AS GRADE, " _
                                & " CASE WHEN FTM_RESULT='PASS' THEN '#1B80B6' ELSE 'red' END AS MCOLOR" _
                                & " FROM SUBJECTS_GRADE_S AS A INNER JOIN RPT.FINAL_TOTALMARKS_S AS B ON " _
                                & " A.SBG_ID=B.FTM_SBG_ID " _
                                 & " LEFT OUTER JOIN RPT.FINAL_RETEST_S AS C ON A.SBG_ID=C.FRT_SBG_ID AND B.FTM_STU_ID=C.FRT_STU_ID " _
                                & " WHERE FTM_STU_ID='" + stu_id + "'" _
                                & " AND FTM_ACD_ID='" + hfACD_ID.Value + "' AND FTM_RESULT='FAIL' AND ISNULL(SBG_bRETEST,'FALSE')='TRUE'"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlMarks.DataSource = ds
        dlMarks.DataBind()

        '#1B80B6
    End Sub


    Sub SaveData(ByVal stu_id As String, ByVal dlMarks As DataList, ByVal result As String)
        Dim strMark As String = ""
        Dim strGrade As String = ""


        Dim txtMark As TextBox

        Dim lblSbgId As Label
        Dim grade As String() = hfGRD_ID.Value.Split("|")

        Dim i As Integer
        Dim str_conn As String
        Dim str_query As String

        str_conn = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        str_query = "EXEC RPT.saveRetestResult " _
                  & stu_id + "," _
                  & hfACD_ID.Value + "," _
                  & "'" + result.ToUpper + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        For i = 0 To dlMarks.Items.Count - 1
            txtMark = dlMarks.Items(i).FindControl("txtMark")

            lblSbgId = dlMarks.Items(i).FindControl("lblSbgId")
            If txtMark.Text <> "" Then

                str_query = "EXEC RPT.saveFINALRETESTMARKS " _
                                        & hfACD_ID.Value + "," _
                                        & "'" + grade(0) + "'," _
                                        & stu_id + "," _
                                        & lblSbgId.Text + "," _
                                        & txtMark.Text

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next

    End Sub

    Sub BindRetestResult(ByVal ddlResult As DropDownList, ByVal stu_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT CASE FTR_RESULT WHEN 'PASS' THEN 'Pass' WHEN 'FAIL' THEN 'Fail' WHEN 'RETEST' THEN 'Retest' ELSE isnull(FTR_RESULT,'Pass') END AS RESULT" _
                             & " FROM RPT.FINAL_RETESTRESULT_S  WHERE FTR_STU_ID='" + stu_id + "' AND FTR_ACD_ID='" + hfACD_ID.Value + "'"
        Dim result As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Not ddlResult.Items.FindByValue(result) Is Nothing Then
            ddlResult.Items.FindByValue(result).Selected = True
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        BindSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        tblTC.Rows(3).Visible = True
        tblTC.Rows(4).Visible = True
      

        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfSCT_ID.Value = ddlSection.SelectedValue
        hfSTUNO.Value = txtStuNo.Text
        hfNAME.Value = txtName.Text
        GridBind()
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStuId As Label
            Dim dlMarks As DataList
            Dim ddlResult As DropDownList
            lblStuId = e.Row.FindControl("lblStuId")
            dlMarks = e.Row.FindControl("dlMarks")
            ddlResult = e.Row.FindControl("ddlResult")
            BindMarks(lblStuId.Text, dlMarks)
            BindRetestResult(ddlResult, lblStuId.Text)
        End If
    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEdit As LinkButton = DirectCast(sender, LinkButton)

        Dim dlMarks As DataList = TryCast(sender.FindControl("dlMarks"), DataList)
        Dim lblStuId As Label = TryCast(sender.FindControl("lblStuId"), Label)
        Dim ddlResult As DropDownList = TryCast(sender.FindControl("ddlResult"), DropDownList)

        If lblEdit.Text = "Edit" Then
            dlMarks.Enabled = True
            ddlResult.Enabled = True
            lblEdit.Text = "Update"
        Else
            SaveData(lblStuId.Text, dlMarks, ddlResult.SelectedValue)
            lblEdit.Text = "Edit"
            ddlResult.Enabled = False
            dlMarks.Enabled = False
        End If
    End Sub

   
End Class
