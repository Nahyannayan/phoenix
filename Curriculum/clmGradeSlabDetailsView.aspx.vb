Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CURRICULUM

Partial Class clmGradeSlabMasterView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> CURR_CONSTANTS.MNU_GRADESLAB_DETAILS Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                hlAddNew.NavigateUrl = "clmGradeSlabDetails.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                GridBind()
                gvGradeSlab.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGradeSlab.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvGradeSlab.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            Dim txtSearch2 As New TextBox
            Dim str_search As String
            Dim str_filter_CAM_DESC As String = String.Empty
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""

            str_Filter = ""
            If gvGradeSlab.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                
                '   -- 1  txtCounterDescr
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvGradeSlab.HeaderRow.FindControl("txtCounterDescr")
                'lstrCondn2 = Trim(txtSearch.Text)
                'If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCM_DESCR", lstrCondn2)

                '   -- 2  txtUser
                'larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                txtSearch2 = gvGradeSlab.HeaderRow.FindControl("txtUser")
                'lstrCondn3 = txtSearch.Text
                'If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "USR_NAME", lstrCondn3)
            End If

            Dim str_cond As String = String.Empty
            str_cond = " GSM_BSU_ID = '" & Session("sBSUID") & "'"
            str_Sql = "SELECT DISTINCT GSM_DESC, GSM_BSU_ID, GSM_TOT_MARK, GSM_SLAB_ID FROM ACT.GRADING_SLAB_D " & _
            "INNER JOIN ACT.GRADING_SLAB_M ON ACT.GRADING_SLAB_D.GSD_GSM_SLAB_ID = ACT.GRADING_SLAB_M.GSM_SLAB_ID " & _
            " WHERE " & str_cond & str_Filter
            If txtSearch.Text <> "" Then
                If str_search = "LI" Then
                    str_filter_CAM_DESC = " AND GSM_DESC LIKE '%" & txtSearch.Text & "%'"

                ElseIf str_search = "NLI" Then
                    str_filter_CAM_DESC = "  AND  NOT GSM_DESC LIKE '%" & txtSearch.Text & "%'"

                ElseIf str_search = "SW" Then
                    str_filter_CAM_DESC = " AND GSM_DESC  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CAM_DESC = " AND GSM_DESC NOT LIKE '" & txtSearch.Text & "%'"

                ElseIf str_search = "EW" Then
                    str_filter_CAM_DESC = " AND GSM_DESC LIKE  '%" & txtSearch.Text & "'"

                ElseIf str_search = "NEW" Then
                    str_filter_CAM_DESC = " AND GSM_DESC NOT LIKE '%" & txtSearch.Text & "'"

                End If
            End If
            If txtSearch2.Text <> "" Then
                If str_search = "LI" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK LIKE '%" & txtSearch2.Text & "%'"

                ElseIf str_search = "NLI" Then
                    str_filter_CAM_DESC = "  AND  NOT GSM_TOT_MARK LIKE '%" & txtSearch2.Text & "%'"

                ElseIf str_search = "SW" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK  LIKE '" & txtSearch2.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK NOT LIKE '" & txtSearch2.Text & "%'"

                ElseIf str_search = "EW" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK LIKE  '%" & txtSearch2.Text & "'"

                ElseIf str_search = "NEW" Then
                    str_filter_CAM_DESC = " AND GSM_TOT_MARK NOT LIKE '%" & txtSearch2.Text & "'"

                End If
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_CAM_DESC)
            gvGradeSlab.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGradeSlab.DataBind()
                Dim columnCount As Integer = gvGradeSlab.Rows(0).Cells.Count

                gvGradeSlab.Rows(0).Cells.Clear()
                gvGradeSlab.Rows(0).Cells.Add(New TableCell)
                gvGradeSlab.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGradeSlab.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGradeSlab.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGradeSlab.DataBind()
            End If
            
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        Dim vGSD_GSM_SLAB_ID As String = contextKey
        Dim drReader As SqlDataReader
        Try
            Dim str_Sql As String = "SELECT GSD_MIN_MARK, GSD_MAX_MARK, GSD_DESC " & _
            " FROM ACT.GRADING_SLAB_D where GSD_GSM_SLAB_ID = " & vGSD_GSM_SLAB_ID & _
            " ORDER BY GSD_MIN_MARK DESC "

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim objConn As New SqlConnection(str_conn)
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
            If Not drReader.HasRows Then
                Return ""
            End If
            sTemp.Append("<table class='popdetails'>")
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan=3><b>Slab Details</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            sTemp.Append("<td><b>NAME</b></td>")
            sTemp.Append("<td><b>MIN. MARK</b></td>")
            sTemp.Append("<td><b>MAX. MARK</b></td>")
            sTemp.Append("</tr>")
            While (drReader.Read())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & drReader("GSD_DESC").ToString & "</td>")
                sTemp.Append("<td>" & Math.Round(CDbl(drReader("GSD_MIN_MARK")), 2) & "</td>")
                sTemp.Append("<td>" & Math.Round(CDbl(drReader("GSD_MAX_MARK")), 2) & "</td>")
                sTemp.Append("</tr>")
            End While
            '    If HttpContext.Current.Session("STUD_DET") Is Nothing Then
            '        sTemp.Append("</table>")
            '        Return sTemp.ToString()
            '    End If
            '    Dim httab As Hashtable = HttpContext.Current.Session("STUD_DET")
            '    Dim vFEE_PERF As FEEPERFORMAINVOICE = httab(STUD_ID)
            '    If Not vFEE_PERF Is Nothing Then
            '        Dim arrList As ArrayList = vFEE_PERF.STUDENT_SUBDETAILS
            '        Dim ienum As IEnumerator = arrList.GetEnumerator()
            '        While (ienum.MoveNext())
            '            Dim FEE_SUB_DET As FEEPERFORMANCEREVIEW_SUB = ienum.Current
            '            sTemp.Append("<tr>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_ID.ToString & "</td>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_FEE_DESCR & "</td>")
            '            sTemp.Append("<td>" & FEE_SUB_DET.FPD_AMOUNT.ToString & "</td>")
            '            sTemp.Append("</tr>")
            '        End While
            '    End If
        Catch

        Finally
            drReader.Close()
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function


    Protected Sub gvGradeSlab_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGradeSlab.PageIndexChanging
        gvGradeSlab.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvGradeSlab_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGradeSlab.RowDataBound
        Try
            Dim lblGSM_SLAB_ID As New Label
            lblGSM_SLAB_ID = TryCast(e.Row.FindControl("lblGSM_SLAB_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblGSM_SLAB_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "clmGradeSlabDetails.aspx?GSM_SLAB_ID=" & Encr_decrData.Encrypt(lblGSM_SLAB_ID.Text) & _
                "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub gvGradeSlab_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(e.Row.FindControl("PopupControlExtender1"), AjaxControlToolkit.PopupControlExtender)
            ' Set the BehaviorID 
            Dim behaviorID As String = String.Concat("pce", e.Row.RowIndex)
            pce.BehaviorID = behaviorID
            ' Programmatically reference the Image control 
            Dim i As LinkButton = DirectCast(e.Row.Cells(1).FindControl("lnkAmount"), LinkButton)
            ' Add the clie nt-side attributes (onmouseover & onmouseout) 
            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", behaviorID)
            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", behaviorID)
            'i.Attributes.Add("onmouseover", OnMouseOverScript)
            i.Attributes.Add("onmouseover", OnMouseOverScript)
            i.Attributes.Add("onmouseout", OnMouseOutScript)
        End If
    End Sub
End Class
