
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class Curriculum_clmReportSubjects_Add
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320004") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call callYEAR_DESCRBind()
                    If Not ddlAca_Year.SelectedIndex = -1 Then
                        PopulateReports()
                    End If
                    If Not ddlReport.SelectedIndex = -1 Then
                        PopulateGrades()
                    End If
                    PopulateSubjects()
                    PopulateRepHeader()
                    'Call CALLACTIVITY_M()
                    'Call CALLTERM_DESCRBind()
                    'Call GetEmpID()
                    'txtCAD_DESC.Attributes("onfocus") = "this.style.backgroundColor='yellow';"
                    'txtCAD_DESC.Attributes("onblur") = "this.style.backgroundColor='white';"

                    If ViewState("datamode") = "view" Then
                        h_Row.Value = "1"
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Call GETACTIVITY_D()
                        tr20.Visible = False
                        tr21.Visible = False
                        tr22.Visible = False
                        DISABLE_CONTROL()
                    ElseIf ViewState("datamode") = "add" Then
                        h_Row.Value = "2"
                        Session("REPORT_SUBJECTS") = CreateDataTable()
                        ViewState("SRNO") = 1
                        gridbind()


                        btnUpdate.Visible = False
                        btnUpdateCancel.Visible = False
                        tr20.Visible = True
                        tr21.Visible = True
                        tr22.Visible = True

                    End If

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub

    Sub DISABLE_CONTROL()
        ddlAca_Year.Enabled = False
        'ddlCAD_CAM_ID.Enabled = False
        'ddlTRM_ID.Enabled = False
        'txtCAD_DESC.Enabled = False

    End Sub
    Sub ENABLE_CONTROL()
        ddlAca_Year.Enabled = True
        'ddlCAD_CAM_ID.Enabled = True
        'ddlTRM_ID.Enabled = True
        'txtCAD_DESC.Enabled = True
    End Sub
    Sub GETACTIVITY_D()

        Using readerACTIVITY_D As SqlDataReader = ACTIVITYMASTER.GetACTIVITY_D(ViewState("viewid"))
            While readerACTIVITY_D.Read

                If Not ddlAca_Year.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_ACD_ID"))) Is Nothing Then
                    ddlAca_Year.ClearSelection()
                    ddlAca_Year.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_ACD_ID"))).Selected = True
                End If

                'If Not ddlTRM_ID.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_TRM_ID"))) Is Nothing Then
                '    ddlTRM_ID.ClearSelection()
                '    ddlTRM_ID.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_TRM_ID"))).Selected = True
                'End If


                'If Not ddlCAD_CAM_ID.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_CAM_ID"))) Is Nothing Then
                '    ddlCAD_CAM_ID.ClearSelection()
                '    ddlCAD_CAM_ID.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_CAM_ID"))).Selected = True
                'End If


                'txtCAD_DESC.Text = Convert.ToString(readerACTIVITY_D("CAD_DESC"))




            End While
        End Using
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            'Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.String"))
            'Dim ACD_DESC As New DataColumn("ACD_DESC", System.Type.GetType("System.String"))
            '' Dim GRD_DESC As New DataColumn("GRD_DESC", System.Type.GetType("System.String"))
            'Dim TRM_DESC As New DataColumn("TRM_DESC", System.Type.GetType("System.String"))
            'Dim MAIN_ACT As New DataColumn("MAIN_ACT", System.Type.GetType("System.String"))
            'Dim SUB_ACT As New DataColumn("SUB_ACT", System.Type.GetType("System.String"))
            'Dim CAD_ACD_ID As New DataColumn("CAD_ACD_ID", System.Type.GetType("System.String"))
            'Dim CAD_CAM_ID As New DataColumn("CAD_CAM_ID", System.Type.GetType("System.String"))
            ''Dim CAD_GRD_ID As New DataColumn("CAD_GRD_ID", System.Type.GetType("System.String"))
            'Dim CAD_TRM_ID As New DataColumn("CAD_TRM_ID", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            Dim RSB_ID As New DataColumn("RSB_ID", System.Type.GetType("System.String"))
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.String"))
            Dim RSB_RSM_ID As New DataColumn("RSB_RSM_ID", System.Type.GetType("System.String"))
            Dim RSB_GRD_ID As New DataColumn("RSB_GRD_ID", System.Type.GetType("System.String"))
            Dim RSB_SBG_ID As New DataColumn("RSB_SBG_ID", System.Type.GetType("System.String"))
            Dim RSB_RSD_ID As New DataColumn("RSB_RSD_ID", System.Type.GetType("System.String"))
            Dim RSB_DESCR As New DataColumn("RSB_DESCR", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(RSB_ID)
            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(RSB_RSM_ID)
            dtDt.Columns.Add(RSB_GRD_ID)
            ' dtDt.Columns.Add(GRD_DESC)
            dtDt.Columns.Add(RSB_SBG_ID)
            dtDt.Columns.Add(RSB_RSD_ID)
            dtDt.Columns.Add(RSB_DESCR)
            'dtDt.Columns.Add(CAD_ACD_ID)
            'dtDt.Columns.Add(CAD_CAM_ID)
            ' dtDt.Columns.Add(CAD_GRD_ID)
            'dtDt.Columns.Add(CAD_TRM_ID)
            dtDt.Columns.Add(STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function
    Sub gridbind()
        Try
            gvSUBJ_Detail.DataSource = Session("REPORT_SUBJECTS")
            gvSUBJ_Detail.DataBind()
            Call DisableControls()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub DisableControls()

        'For j As Integer = 0 To gvSUBJ_Detail.Rows.Count - 1

        '    Dim row As GridViewRow = gvSUBJ_Detail.Rows(j)

        '    Dim lblSTATUS As New Label

        '    lblSTATUS = DirectCast(row.FindControl("lblSTATUS"), Label)
        '    Dim Stud_ID_STATUS As String = lblSTATUS.Text
        '    If UCase(Stud_ID_STATUS) = "PENDING" Then
        '        DirectCast(row.FindControl("lblSTATUS"), Label).Visible = False
        '    Else
        '        If UCase(Stud_ID_STATUS) = "APPROVED" Then
        '            lblSTATUS.ForeColor = Drawing.Color.Green
        '        ElseIf UCase(Stud_ID_STATUS) = "NOT APPROVED" Then
        '            lblSTATUS.ForeColor = Drawing.Color.Red
        '        End If
        '        DirectCast(row.FindControl("lblEdit"), LinkButton).Visible = False
        '    End If

        'Next
    End Sub
    Sub gridbind_add()
        Try
            Dim i As Integer



            'For i = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1

            '    If Session("ACTIVITY_DETAIL").Rows(i)("STATUS") = "PENDING" Then

            '        lblError.Text = "No new leave approval can be added,since pending approval exist for the current student!!!"
            '        Exit Sub
            '    End If

            'Next



            Dim rDt As DataRow
            '
            rDt = Session("REPORT_SUBJECTS").NewRow

            rDt("SRNO") = ViewState("SRNO")
            rDt("ID") = CInt(ViewState("SRNO"))
            rDt("RSB_ID") = 0
            rDt("RSB_RSM_ID") = h_ReportId.Value
            rDt("RSB_GRD_ID") = h_GradeId.Value
            rDt("RSB_SBG_ID") = h_SubjId.Value
            rDt("RSB_RSD_ID") = h_RepHeaderid.Value
            rDt("RSB_DESCR") = txtDescription.Text
            ' rDt("GRD_DESC") = item.Text
            ' rDt("TRM_DESC") = ddlTRM_ID.SelectedItem.Text
            'rDt("MAIN_ACT") = ddlCAD_CAM_ID.SelectedItem.Text
            'rDt("SUB_ACT") = txtCAD_DESC.Text
            'rDt("CAD_ACD_ID") = ddlAca_Year.SelectedItem.Value
            'rDt("CAD_CAM_ID") = ddlCAD_CAM_ID.SelectedItem.Value
            'rDt("CAD_GRD_ID") = item.Value
            'rDt("CAD_TRM_ID") = ddlTRM_ID.SelectedItem.Value
            rDt("STATUS") = "NEW"
            Session("REPORT_SUBJECTS").Rows.Add(rDt)
            ViewState("SRNO") = ViewState("SRNO") + 1
            gridbind()
            txtDescription.Text = ""
        Catch ex As Exception
            lblError.Text = "Check Student Leave date"
        End Try
    End Sub
    Protected Sub btnAddDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlAca_Year.Enabled = False
        'ddlTRM_ID.Enabled = False

        gridbind_add()
    End Sub
    Protected Sub gvACT_Detail_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        'btnSave.Enabled = False
        'btnUpdateCancel.Visible = True
        'gvSUBJ_Detail.SelectedIndex = e.NewEditIndex
        'Dim row As GridViewRow = gvSUBJ_Detail.Rows(e.NewEditIndex)
        'gvSUBJ_Detail.Columns(5).Visible = False
        'gvSUBJ_Detail.Columns(6).Visible = False
        'Dim lblID As New Label
        'lblID = TryCast(row.FindControl("lblID"), Label)
        'Dim iEdit As Integer = 0
        'Dim iIndex As Integer = 0
        'iIndex = CInt(lblID.Text)
        ''For Each item As ListItem In chkGRD_ID.Items
        ''    If item.Selected = True Then
        ''        item.Selected = False
        ''    End If
        ''Next
        ''loop through the data table row  for the selected rowindex item in the grid view
        'For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
        '    If iIndex = Session("ACTIVITY_DETAIL").Rows(iEdit)("ID") Then
        '        'If Not ddlCAD_CAM_ID.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_CAM_ID")) Is Nothing Then
        '        '    ddlCAD_CAM_ID.ClearSelection()
        '        '    ddlCAD_CAM_ID.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_CAM_ID")).Selected = True
        '        'End If
        '        'For Each item As ListItem In chkGRD_ID.Items
        '        '    If item.Value = Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID") Then
        '        '        item.Selected = True
        '        '    End If
        '        'Next
        '        'If Not ddlGRD_ID_EDIT.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID")) Is Nothing Then
        '        '    ddlGRD_ID_EDIT.ClearSelection()
        '        '    ddlGRD_ID_EDIT.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID")).Selected = True
        '        'End If
        '        'txtCAD_DESC.Text = Session("ACTIVITY_DETAIL").Rows(iEdit)("SUB_ACT")
        '        Exit For
        '    End If
        'Next

        'btnAddDetail.Visible = False
        'btnUpdate.Visible = True
        'btnUpdateCancel.Visible = True

        'gridbind()
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        btnSave.Enabled = True
        btnUpdateCancel.Visible = False
        Dim row As GridViewRow = gvSUBJ_Detail.Rows(gvSUBJ_Detail.SelectedIndex)
        gvSUBJ_Detail.Columns(5).Visible = True
        gvSUBJ_Detail.Columns(6).Visible = True

        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        'For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
        'Next
        For iEdit = 0 To Session("REPORT_SUBJECTS").Rows.Count - 1
            If iIndex = Session("REPORT_SUBJECTS").Rows(iEdit)("ID") Then
                'Session("ACTIVITY_DETAIL").Rows(iEdit)("MAIN_ACT") = ddlCAD_CAM_ID.SelectedItem.Text
                'Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID") = ddlGRD_ID_EDIT.SelectedItem.Value
                'Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_CAM_ID") = ddlCAD_CAM_ID.SelectedItem.Value
                'Session("ACTIVITY_DETAIL").Rows(iEdit)("GRD_DESC") = ddlGRD_ID_EDIT.SelectedItem.Text
                'Session("ACTIVITY_DETAIL").Rows(iEdit)("SUB_ACT") = txtCAD_DESC.Text
                Exit For
            End If
        Next

        btnUpdate.Visible = False
        btnAddDetail.Visible = True
        gvSUBJ_Detail.SelectedIndex = -1
        gridbind()

    End Sub
    Protected Sub gvACT_Detail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)


        'Dim COND_ID As Integer = CInt(gvSUBJ_Detail.DataKeys(e.RowIndex).Value)

        'Dim i As Integer = 0
        'While i < Session("ACTIVITY_DETAIL").Rows.Count
        '    If Session("ACTIVITY_DETAIL").rows(i)("Id") = COND_ID Then
        '        Session("ACTIVITY_DETAIL").rows(i).delete()
        '    Else
        '        i = i + 1
        '    End If
        'End While
        'gridbind()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        str_err = CallTransaction(errorMessage)
        If str_err = "0" Then
            DISABLE_CONTROL()
            lblError.Text = "Record updated successfully"
        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Private Function CallTransaction(ByRef errorMessage As String) As String
        Dim STATUS As Integer
        Dim i As Integer
        Dim lblID As New Label
        Dim lblRSBID As New Label
        Dim lblRSMID As New Label
        Dim lblGRDID As New Label
        Dim lblSBGID As New Label
        Dim lblRSDID As New Label
        Dim lblDESCR As New Label
        Dim RecordSTATUS As String = String.Empty
        Dim bEdit As Boolean
        If gvSUBJ_Detail.Rows.Count > 0 Then
            'If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    'Dim ACT_XML As String = getACT_XML()
                    'If ACT_XML = "" Then
                    '    lblError.Text = "No records selected"
                    'Else
                    'STATUS = ACTIVITYMASTER.SAVEACTIVITY_D(ACT_XML, ViewState("EMP_ID"), bEdit, transaction)
                    'End If
                    Dim Params(5) As SqlClient.SqlParameter
                    Params(0) = New SqlClient.SqlParameter("@RSB_BSU_ID", Session("SBsuid"))
                    Params(1) = New SqlClient.SqlParameter("@RSB_ACD_ID", h_AcdId.Value)
                    Params(2) = New SqlClient.SqlParameter("@RSB_RSM_ID", h_ReportId.Value)
                    Params(3) = New SqlClient.SqlParameter("@RSB_GRD_ID", h_GradeId.Value)
                    Params(4) = New SqlClient.SqlParameter("@RSB_SBG_ID", h_SubjId.Value)
                    Params(5) = New SqlClient.SqlParameter("@RSB_RSD_ID", h_RepHeaderid.Value)
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[RPT].[DELETEREPORTSUBJECTS]", Params)

                    For i = 0 To gvSUBJ_Detail.Rows.Count - 1
                        lblID = TryCast(gvSUBJ_Detail.Rows(i).FindControl("lblID"), Label)
                        lblRSBID = TryCast(gvSUBJ_Detail.Rows(i).FindControl("lblRSB_ID"), Label)
                        lblRSMID = TryCast(gvSUBJ_Detail.Rows(i).FindControl("lblRSB_RSM_ID"), Label)
                        lblGRDID = TryCast(gvSUBJ_Detail.Rows(i).FindControl("lblRSB_GRD_ID"), Label)
                        lblSBGID = TryCast(gvSUBJ_Detail.Rows(i).FindControl("lblRSB_SBG_ID"), Label)
                        lblRSDID = TryCast(gvSUBJ_Detail.Rows(i).FindControl("lblRSB_RSD_ID"), Label)
                        lblDESCR = TryCast(gvSUBJ_Detail.Rows(i).FindControl("lblRSB_DESCR"), Label)
                        Dim pParms(10) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@RSB_ID", CInt(lblRSBID.Text))
                        pParms(1) = New SqlClient.SqlParameter("@RSB_BSU_ID", Session("SBsuid"))
                        pParms(2) = New SqlClient.SqlParameter("@RSB_ACD_ID", h_AcdId.Value)
                        pParms(3) = New SqlClient.SqlParameter("@RSB_RSM_ID", CInt(lblRSMID.Text))
                        pParms(4) = New SqlClient.SqlParameter("@RSB_GRD_ID", lblGRDID.Text)
                        pParms(5) = New SqlClient.SqlParameter("@RSB_SBG_ID", CInt(lblSBGID.Text))
                        pParms(6) = New SqlClient.SqlParameter("@RSB_RSD_ID", CInt(lblRSDID.Text))
                        pParms(7) = New SqlClient.SqlParameter("@RSB_DESCR", lblDESCR.Text)
                        If ViewState("datamode") = "add" Then
                            pParms(8) = New SqlClient.SqlParameter("@Edit", 0)
                            pParms(9) = New SqlClient.SqlParameter("@Delete", 0)
                        ElseIf ViewState("datamode") = "edit" Then
                            pParms(8) = New SqlClient.SqlParameter("@Edit", 1)
                            pParms(9) = New SqlClient.SqlParameter("@Delete", 0)
                        ElseIf ViewState("datamode") = "delete" Then
                            pParms(8) = New SqlClient.SqlParameter("@Edit", 1)
                            pParms(9) = New SqlClient.SqlParameter("@Delete", 1)
                        End If
                        pParms(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(10).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[RPT].[SaveREPORTSUBJECTS]", pParms)
                        Dim ReturnFlag As Integer = pParms(10).Value
                        'Return ReturnFlag
                        If ReturnFlag <> 0 Then
                            STATUS = 1
                            Exit For
                        End If
                    Next
                    If STATUS <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Error while updating records"
                        Return "1"
                    Else
                        ' STATUS = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), , Session("ACTIVITY_DETAIL").Rows(i)("ID"), "add", Page.User.Identity.Name.ToString, Me.Page)
                        If STATUS <> 0 Then
                            CallTransaction = "1"
                            errorMessage = "Could not process audit request"
                            Return "1"
                        End If
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                Catch ex As Exception
                    errorMessage = "Record could not be Updated"
                Finally
                    If CallTransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try
            End Using
            'ElseIf ViewState("datamode") = "edit" Then
            '    Dim transaction As SqlTransaction

            '    Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
            '        transaction = conn.BeginTransaction("SampleTransaction")
            '        Try
            '            bEdit = True
            '            Dim ACT_XML As String = getACT_XML_EDIT()
            '            If ACT_XML = "" Then
            '                lblError.Text = "No records selected"
            '            Else
            '                STATUS = ACTIVITYMASTER.SAVEACTIVITY_D(ACT_XML, ViewState("EMP_ID"), bEdit, transaction)
            '            End If

            '            If STATUS <> 0 Then
            '                CallTransaction = "1"
            '                errorMessage = UtilityObj.getErrorMessage(STATUS)
            '                Return "1"
            '            Else
            '                'STATUS = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), Session("ACTIVITY_DETAIL").Rows(i)("ID"), "edit", Page.User.Identity.Name.ToString, Me.Page)
            '                If STATUS <> 0 Then
            '                    CallTransaction = "1"
            '                    errorMessage = "Could not process audit request"
            '                    Return "1"
            '                End If
            '            End If
            '            ViewState("viewid") = "0"
            '            ViewState("datamode") = "none"
            '            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            '            CallTransaction = "0"
            '        Catch ex As Exception
            '            errorMessage = "Record could not be Updated"
            '        Finally
            '            If CallTransaction <> "0" Then
            '                UtilityObj.Errorlog(errorMessage)
            '                transaction.Rollback()
            '            Else
            '                errorMessage = ""
            '                transaction.Commit()
            '            End If
            '        End Try
            '    End Using
            'End If
        End If
    End Function

    Function getACT_XML_EDIT() As String
        Dim str As String = String.Empty





        Dim ID As String = ViewState("viewid")
        'Dim CAD_DESC As String = txtCAD_DESC.Text
        Dim CAD_ACD_ID As String = ddlAca_Year.SelectedValue
        ' Dim CAD_CAM_ID As String = ddlCAD_CAM_ID.SelectedValue
        'Dim CAD_TRM_ID As String = ddlTRM_ID.SelectedValue
        ' Dim CDD_GRD_ID As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_GRD_ID").ToString

        'str += String.Format("<ACTIVITY ACTID='{0}'><ACT_DETAIL ACD_ID='{1}' TRM_ID='{2}' CAD_DESC='{3}'  CAM_ID='{4}' /></ACTIVITY>", ID, CAD_ACD_ID, CAD_TRM_ID, CAD_DESC, CAD_CAM_ID)



        If str <> "" Then
            str = "<ACTIVITYS>" + str + "</ACTIVITYS>"
        End If

        Return str
    End Function
    Function getACT_XML() As String
        Dim str As String = String.Empty



        If Session("REPORT_SUBJECTS") IsNot Nothing Then

            For i As Integer = 0 To Session("REPORT_SUBJECTS").Rows.Count - 1
                Dim ID As String = Session("REPORT_SUBJECTS").Rows(i)("ID").ToString
                Dim CAD_DESC As String = Session("REPORT_SUBJECTS").Rows(i)("SUB_ACT").ToString
                Dim CAD_ACD_ID As String = Session("REPORT_SUBJECTS").Rows(i)("CAD_ACD_ID").ToString
                Dim CAD_CAM_ID As String = Session("REPORT_SUBJECTS").Rows(i)("CAD_CAM_ID").ToString
                Dim CAD_TRM_ID As String = Session("REPORT_SUBJECTS").Rows(i)("CAD_TRM_ID").ToString
                ' Dim CDD_GRD_ID As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_GRD_ID").ToString

                str += String.Format("<ACTIVITY ACTID='{0}'><ACT_DETAIL ACD_ID='{1}' TRM_ID='{2}' CAD_DESC='{3}'  CAM_ID='{4}' /></ACTIVITY>", ID, CAD_ACD_ID, CAD_TRM_ID, CAD_DESC, CAD_CAM_ID)
            Next


            If str <> "" Then
                str = "<ACTIVITYS>" + str + "</ACTIVITYS>"
            End If
        Else
            str = ""
        End If
        Return str
    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_Row.Value = "1"
        ViewState("datamode") = "edit"
        ENABLE_CONTROL()
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        tr20.Visible = False
        tr21.Visible = False
        tr22.Visible = False
        ENABLE_CONTROL()
        ddlAca_Year.Enabled = False
        'ddlTRM_ID.Enabled = False

    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ENABLE_CONTROL()
        h_Row.Value = "2"
        ViewState("datamode") = "add"
        ENABLE_CONTROL()
        tr20.Visible = True
        tr21.Visible = True
        tr22.Visible = True
        btnUpdate.Visible = False
        btnUpdateCancel.Visible = False
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            DISABLE_CONTROL()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
" FROM  vw_ACADEMICYEAR_D INNER JOIN   VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
" WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" & Session("sBsuid") & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            If Not ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                ddlAca_Year.ClearSelection()
                ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
            If Not ddlAca_Year.SelectedIndex = -1 Then
                h_AcdId.Value = ddlAca_Year.SelectedItem.Value
            End If
            'ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'Sub CALLTERM_DESCRBind()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '        Dim str_Sql As String
    '        Dim ds As New DataSet
    '        Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
    '        str_Sql = " SELECT    TRM_ID, TRM_DESCRIPTION FROM  VW_TRM_M WHERE TRM_ACD_ID= '" & ACD_ID & "' ORDER BY TRM_DESCRIPTION "
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    'ddlTRM_ID.Items.Clear()
    'ddlTRM_ID.DataSource = ds.Tables(0)
    'ddlTRM_ID.DataTextField = "TRM_DESCRIPTION"
    'ddlTRM_ID.DataValueField = "TRM_ID"
    'ddlTRM_ID.DataBind()
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub
    'Sub CALLGRD_DESCRBind_EDIT()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '        Dim str_Sql As String
    '        Dim ds As New DataSet
    '        Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
    '        str_Sql = " SELECT  VW_GRADE_BSU_M.GRM_GRD_ID AS GRM_GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY  AS GRM_DISPLAY" & _
    '       "  FROM VW_GRADE_BSU_M INNER JOIN VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID WHERE VW_GRADE_BSU_M.GRM_ACD_ID= '" & ACD_ID & "' ORDER BY  VW_GRADE_M.GRD_DISPLAYORDER"
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        ddlGRD_ID_EDIT.Items.Clear()
    '        ddlGRD_ID_EDIT.DataSource = ds.Tables(0)
    '        ddlGRD_ID_EDIT.DataTextField = "GRM_DISPLAY"
    '        ddlGRD_ID_EDIT.DataValueField = "GRM_GRD_ID"
    '        ddlGRD_ID_EDIT.DataBind()
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub
    Sub CALLACTIVITY_M()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     CAM_ID, CAM_DESC FROM ACT.ACTIVITY_M WHERE CAM_BSU_ID = '" & Session("sBSUID") & "'ORDER BY CAM_DESC"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            'ddlCAD_CAM_ID.Items.Clear()
            'ddlCAD_CAM_ID.DataSource = ds.Tables(0)
            'ddlCAD_CAM_ID.DataTextField = "CAM_DESC"
            'ddlCAD_CAM_ID.DataValueField = "CAM_ID"
            'ddlCAD_CAM_ID.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'Sub CALLGRD_DESCRBind()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '        Dim str_Sql As String
    '        Dim ds As New DataSet
    '        Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
    '        str_Sql = " SELECT  VW_GRADE_BSU_M.GRM_GRD_ID AS GRM_GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY  AS GRM_DISPLAY" & _
    '       "  FROM VW_GRADE_BSU_M INNER JOIN VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID WHERE VW_GRADE_BSU_M.GRM_ACD_ID= '" & ACD_ID & "' ORDER BY  VW_GRADE_M.GRD_DISPLAYORDER"
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        chkGRD_ID.Items.Clear()
    '        Dim row As DataRow
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            ViewState("ChangeEvent") = 1
    '            For Each row In ds.Tables(0).Rows
    '                Dim str1 As String = row("GRM_DISPLAY")
    '                Dim str2 As String = row("GRM_GRD_ID")
    '                chkGRD_ID.Items.Add(New ListItem(str1, str2))
    '            Next
    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub
    'Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'If ViewState("datamode") = "add" Then
    '    'CALLGRD_DESCRBind()
    '    '' Else
    '    'Call CALLGRD_DESCRBind_EDIT()
    '    '' End If


    '    'If ViewState("datamode") = "add" Then

    '    '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
    '    '                                 "<script language=javascript>HideRows(2);</script>")
    '    'Else
    '    '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
    '    '                                                         "<script language=javascript>HideRows(1);</script>")
    '    'End If

    'End Sub
    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub
    Protected Sub gvACT_Detail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'gvSUBJ_Detail.PageIndex = e.NewPageIndex
        'gridbind()
    End Sub
    Protected Sub btnUpdateCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'ddlCAD_CAM_ID.ClearSelection()
        'ddlCAD_CAM_ID.SelectedIndex = 0
        'txtCAD_DESC.Text = ""
        gridbind()
    End Sub
    Private Function PopulateReports()
        ddlReport.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_Sql As String = "SELECT RSM_ID,RSM_BSU_ID ,RSM_DESCR FROM RPT.REPORT_SETUP_M WHERE RSM_BSU_ID='" & Session("sBsuId") & "' " _
                   & "  AND RSM_ACD_ID=" & h_AcdId.Value & " ORDER BY RSM_DISPLAYORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlReport.DataSource = ds
        ddlReport.DataTextField = "RSM_DESCR"
        ddlReport.DataValueField = "RSM_ID"
        ddlReport.DataBind()
        If Not ddlReport.SelectedIndex = -1 Then
            h_ReportId.Value = ddlReport.SelectedItem.Value
        End If
        'If ds.Tables(0).Rows.Count >= 1 Then
        '    Dim li As New ListItem
        '    li.Text = ds.Tables(0).Rows(0).Item(0)
        '    li.Value = ds.Tables(0).Rows(0).Item(1)
        '    ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'End If
        'Return ddlReports
    End Function
    Private Function PopulateGrades()
        'ddlGrades.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_Sql As String = ""

        If h_ReportId.Value <> "" Then

            If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
                strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "'"
                str_Sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER " _
                        & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
                        & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID INNER JOIN GROUPS_TEACHER_S ON SGR_ID= SGS_SGR_ID " _
                        & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & Session("Current_ACD_ID") & "' " & strCondition & " AND VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "' " _
                        & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            Else
                str_Sql = "SELECT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY  " _
                        & " FROM RPT.REPORTSETUP_GRADE_S INNER JOIN " _
                        & " VW_GRADE_M ON RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID = VW_GRADE_M.GRD_ID " _
                        & " WHERE RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID='" & h_ReportId.Value & "' " _
                        & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            End If

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrades.DataSource = ds
        ddlGrades.DataTextField = "GRD_DISPLAY"
        ddlGrades.DataValueField = "ID"
        ddlGrades.DataBind()
        If Not ddlGrades.SelectedIndex = -1 Then
            h_GradeId.Value = ddlGrades.SelectedItem.Value
        End If
        'If ds.Tables(0).Rows.Count >= 1 Then
        '    Dim li As New ListItem
        '    li.Text = ds.Tables(0).Rows(0).Item(0)
        '    li.Value = ds.Tables(0).Rows(0).Item(1)
        '    ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'End If
        'Return ddlGrades
    End Function
    Private Function PopulateSubjects()
        'ddlsubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_Sql As String = ""

        If h_GradeId.Value <> "" Then
            strCondition += " AND SBG_GRD_ID='" & h_GradeId.Value & "'"
        End If

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then

            strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
            str_Sql = "SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                       & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                       & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                       & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & Session("Current_ACD_ID") & "'" _
                       & " " & strCondition & ""
        Else
            str_Sql = " SELECT SBG_ID ID, " & _
                                " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            str_Sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' "
            'If v_GradeID <> "" Then
            str_Sql += strCondition & " ORDER BY GRM_DISPLAY"
            'End If
        End If
        'End If

        'End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlSubj.DataSource = ds
        ddlSubj.DataTextField = "DESCR2"
        ddlSubj.DataValueField = "ID"
        ddlSubj.DataBind()
        If Not ddlSubj.SelectedIndex = -1 Then
            h_SubjId.Value = ddlSubj.SelectedItem.Value
        End If
        'If ds.Tables(0).Rows.Count >= 1 Then
        '    Dim li As New ListItem
        '    li.Text = ds.Tables(0).Rows(0).Item(0)
        '    li.Value = ds.Tables(0).Rows(0).Item(1)
        '    ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'End If
        'Return ddlSubjects
    End Function

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlAca_Year.SelectedIndex = -1 Then
            h_AcdId.Value = ddlAca_Year.SelectedValue
            PopulateReports()
            PopulateGrades()
            'PopulateSubjects(ddlSubj, ddlGrades)
        End If
    End Sub

    Protected Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlReport.SelectedIndex = -1 Then
            h_ReportId.Value = ddlReport.SelectedValue
            PopulateGrades()
            PopulateSubjects()
        End If
    End Sub

    Protected Sub ddlGrades_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlGrades.SelectedIndex = -1 Then
            h_GradeId.Value = ddlGrades.SelectedValue
            PopulateSubjects()
        End If
    End Sub


    Private Function PopulateRepHeader()
        'ddlRepHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_Sql As String = ""

        If h_SubjId.Value <> "" Then
            strCondition = strCondition & " AND RSD_SBG_ID='" & h_SubjId.Value & "'"
        End If
        If h_ReportId.Value <> "" Then
            strCondition = strCondition & " AND RSD_RSM_ID=" & h_ReportId.Value & ""
        End If

        'If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then

        'strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
        'str_Sql = "SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
        '           & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
        '           & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
        '           & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & Session("Current_ACD_ID") & "'" _
        '           & " " & strCondition & ""
        str_Sql = "select RSD_ID,RSD_HEADER from RPT.REPORT_SETUP_D D inner join RPT.REPORT_SETUP_M M on M.RSM_ID=D.RSD_RSM_ID where RSD_bDIRECTENTRY=1 and RSD_bALLSUBJECTS=0 " _
                    & " AND RSM_BSU_ID='" & Session("sBsuId") & "' AND RSM_ACD_ID ='" & Session("Current_ACD_ID") & "'"

        str_Sql += strCondition & " ORDER BY RSD_ID"
        ' Else
        'str_Sql = " SELECT SBG_ID ID, " & _
        '                    " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
        '                    " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
        '                    " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
        '                    " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
        '                    " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
        'str_Sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' "
        'If v_GradeID <> "" Then
        'str_Sql += strCondition & " ORDER BY GRM_DISPLAY"
        'End If
        'End If
        'End If

        'End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlRepHeader.DataSource = ds
        ddlRepHeader.DataTextField = "RSD_HEADER"
        ddlRepHeader.DataValueField = "RSD_ID"
        ddlRepHeader.DataBind()
        If Not ddlRepHeader.SelectedIndex = -1 Then
            h_RepHeaderid.Value = ddlRepHeader.SelectedItem.Value
        End If
        'If ds.Tables(0).Rows.Count >= 1 Then
        '    Dim li As New ListItem
        '    li.Text = ds.Tables(0).Rows(0).Item(0)
        '    li.Value = ds.Tables(0).Rows(0).Item(1)
        '    ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'End If
        'Return ddlRepHeader
    End Function

    Protected Sub ddlRepHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlRepHeader.SelectedIndex = -1 Then
            h_RepHeaderid.Value = ddlRepHeader.SelectedValue
        End If
    End Sub


    Protected Sub ddlSubj_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlSubj.SelectedIndex = -1 Then
            h_SubjId.Value = ddlSubj.SelectedValue
            PopulateRepHeader()
        End If
    End Sub

    Protected Sub gvSUBJ_Detail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim COND_ID As Integer = CInt(gvSUBJ_Detail.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0
        While i < Session("ACTIVITY_DETAIL").Rows.Count
            If Session("ACTIVITY_DETAIL").rows(i)("Id") = COND_ID Then
                Session("ACTIVITY_DETAIL").rows(i).delete()
            Else
                i = i + 1
            End If
        End While
        gridbind()
    End Sub

    Protected Sub gvSUBJ_Detail_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        btnSave.Enabled = False
        btnUpdateCancel.Visible = True
        gvSUBJ_Detail.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = gvSUBJ_Detail.Rows(e.NewEditIndex)
        gvSUBJ_Detail.Columns(5).Visible = False
        gvSUBJ_Detail.Columns(6).Visible = False
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)
        'For Each item As ListItem In chkGRD_ID.Items
        '    If item.Selected = True Then
        '        item.Selected = False
        '    End If
        'Next
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
            If iIndex = Session("ACTIVITY_DETAIL").Rows(iEdit)("ID") Then
                'If Not ddlCAD_CAM_ID.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_CAM_ID")) Is Nothing Then
                '    ddlCAD_CAM_ID.ClearSelection()
                '    ddlCAD_CAM_ID.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_CAM_ID")).Selected = True
                'End If
                'For Each item As ListItem In chkGRD_ID.Items
                '    If item.Value = Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID") Then
                '        item.Selected = True
                '    End If
                'Next
                'If Not ddlGRD_ID_EDIT.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID")) Is Nothing Then
                '    ddlGRD_ID_EDIT.ClearSelection()
                '    ddlGRD_ID_EDIT.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID")).Selected = True
                'End If
                'txtCAD_DESC.Text = Session("ACTIVITY_DETAIL").Rows(iEdit)("SUB_ACT")
                Exit For
            End If
        Next

        btnAddDetail.Visible = False
        btnUpdate.Visible = True
        btnUpdateCancel.Visible = True

        gridbind()
    End Sub

    Protected Sub gvSUBJ_Detail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvSUBJ_Detail.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
End Class

