<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmSyllabus_LessonAllocation.aspx.vb" Inherits="Curriculum_clmSyllabus_LessonAllocation"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/usrTreeView.ascx" TagName="usrTreeView" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">


        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }




        function Getemployee() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var syllabusId;
            var groupId;
            // syllabusId=document.getElementById('<%=h_SyllabusId.ClientID %>').value; 
            //alert(syllabusId);   
            //groupId=document.getElementById('<%=h_GRP_ID.ClientID %>').value;            
            groupId = "0";
            // result = window.showModalDialog("showemployee.aspx?GrpId=" + groupId + "", "", sFeatures)
            var oWnd = radopen("showemployee.aspx?GrpId=" + groupId + "", "pop_employee");
            <%--if (result != "" && result != "undefined") {
                NameandCode = result.split('||');
                document.getElementById('<%=txtempname.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=h_EMP_ID.ClientID %>').value = NameandCode[1];

            }
            return false;--%>
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Employee.split('||');
                document.getElementById('<%=txtempname.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=h_EMP_ID.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtempname.ClientID %>', 'TextChanged');

            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------
        function GetSyllabus() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var acdid;
            var termid;
            var gradeid;
            var groupid;
            var subjectid;
            var url;

            acdid = document.getElementById('<%=h_ACD_ID.ClientID %>').value;
            termid = document.getElementById('<%=h_TRM_ID.ClientID %>').value;
            gradeid = document.getElementById('<%=h_GRD_ID.ClientID %>').value.split('|');
            groupid = document.getElementById('<%=h_GRP_ID.ClientID %>').value;
            subjectid = document.getElementById('<%=h_SUBJ_ID.ClientID %>').value;

            url = "ClmPopupForm.aspx?multiselect=false&ID=SYLLABUS&AcdID=" + acdid + "&TrmID=" + termid + "&GrdID=" + gradeid[0] + "&GrpID=" + groupid + "&SubjID=" + subjectid + "";
            //result = window.showModalDialog(url, "", sFeatures)
            var oWnd = radopen(url, "pop_syllabus");
       <%--         if (result == '' || result == undefined)
            { return false; }
            NameandCode = result.split('___');
            document.getElementById("<%=txtSyllabus.ClientID %>").value = NameandCode[1];
            document.getElementById("<%=h_SyllabusId.ClientID %>").value = NameandCode[0];--%>
        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=txtSyllabus.ClientID %>").value = NameandCode[1];
                document.getElementById("<%=h_SyllabusId.ClientID %>").value = NameandCode[0];
                __doPostBack('<%= txtSyllabus.ClientID %>', 'TextChanged');
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------------
        function GetSubjects() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var syllabusId;
            syllabusId = document.getElementById('<%=h_SyllabusId.ClientID %>').value;
            //alert(syllabusId);               
            // result = window.showModalDialog("showTopics.aspx?syllabusId=" + syllabusId, "", sFeatures)
            var oWnd = radopen("showTopics.aspx?syllabusId=" + syllabusId, "pop_topic");


            <%--if (result != "" && result != "undefined") {
                NameandCode = result.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];
            }
            return false;--%>
        }
        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                alert(arg);
                NameandCode = arg.Topic.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtTopic.ClientID %>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_syllabus" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_topic" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Lesson Allocation</asp:Label>
            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr id="Tr2" runat="server">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr id="trEmp" runat="server">
                        <td align="left" width="20%"><span class="field-label">Select Employee</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtempname" runat="server"
                                Enabled="False"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/cal.gif"
                                OnClientClick="Getemployee();return false;"></asp:ImageButton>
                        </td>
                        <td align="left"  colspan="2"></td>
                    </tr>
                    <tr id="trGroup" runat="server">
                        <td colspan="4">
                            <asp:DataList ID="dlGroups" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <ItemTemplate>
                                    <asp:Label ID="lblSgrId" runat="server" Text='<% #Bind("SGR_ID")%>' Visible="false" />
                                    <asp:Button ID="btnGroup" runat="server" Text='<% #Bind("SGR_DESCR")%>'
                                        CssClass="button_menu button" OnClick="btnGroup_Click" />
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table align="center" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblGrade" runat="server" class="field-value" Text="-" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Subject</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblSubject" runat="server" class="field-value" Text="-" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Group</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:Label ID="lblGroup" runat="server" class="field-value" Text="-" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Syllabus</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSyllabus" runat="server" AutoPostBack="True"
                                            OnTextChanged="txtSyllabus_TextChanged" Enabled="False"></asp:TextBox><asp:ImageButton
                                                ID="ImageButton3" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetSyllabus();return false;"></asp:ImageButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Topic</span><asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTopic" runat="server" AutoPostBack="True" OnTextChanged="txtTopic_TextChanged"
                                            Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif"
                                            OnClientClick="GetSubjects() ;return false;"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txtTopic" Display="None" ErrorMessage="Select a Topic" ForeColor="White"
                                            ValidationGroup="add"></asp:RequiredFieldValidator></td>
                                    <td colspan="2"><span class="field-label">Copy to Groups</span>
                                        <asp:TreeView ID="tvCopyGroup" runat="server" ShowCheckBoxes="All" ExpandDepth="0"
                                            onclick="client_OnTreeNodeChecked();">
                                            <NodeStyle CssClass="treenode" />
                                        </asp:TreeView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Documents</span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <asp:DataList ID="dlDocs" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td align="center" style="height: 26px">
                                                            <asp:ImageButton ID="imgF" runat="server" OnClick="imgF_Click" CommandArgument='<%# Bind("id") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="txtfile" runat="server" Text='<%# Bind("FNAME") %>'
                                                                CommandName="View" CommandArgument='<%# Bind("id") %>' OnClick="txtfile_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Objective</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtobjective" runat="server" SkinID="MultiText_Medium"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                     <td align="left" width="20%"><span class="field-label">Subject Activities / Resources</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtResources" runat="server" SkinID="MultiText_Medium"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                              
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Planned StartDate</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPlannedStDate" runat="server" Enabled="False">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Planned EndDate</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtPlannedEndDate" runat="server" Enabled="False">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Actual Start Date</span><asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox><asp:ImageButton
                                            ID="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFromDate"
                                            Display="None" ErrorMessage="Enter Actual Start Date" ValidationGroup="add"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Actual End Date</span><asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" runat="server">
                                        </asp:TextBox><asp:ImageButton ID="imgCalendar2" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtToDate"
                                Display="None" ErrorMessage="Enter Actual End Date" ForeColor="White" ValidationGroup="add"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtFromDate"
                                            ControlToValidate="txtToDate" Display="None" ErrorMessage="End date is less than start date"
                                            ForeColor="White" Operator="GreaterThanEqual" ValidationGroup="add">
                                        </asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Lessons Taken</span><asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                    <td align="left" width="30%" ><asp:TextBox ID="txtHrs" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtHrs" Display="None"
                                        ErrorMessage="Enter Hours" ForeColor="White" ValidationGroup="add">
                                    </asp:RequiredFieldValidator></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select File to Upload</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="fp" runat="server" />
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Button ID="btnAddDocument" runat="server" Text="Upload" CssClass="button" />
                                    </td>
                                    <td align="left" width="30%" >
                                        <asp:GridView ID="grdTeachDoc" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            PageSize="5" OnPageIndexChanging="grdTeachDoc_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="id" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lb_id" runat="server" Text='<%# Bind("STD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="File Name">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:ImageButton ID="imgD" runat="server" OnClick="imgD_Click" CommandArgument='<%# Bind("STD_ID") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="txtDocName" runat="server" Text='<%# Bind("STD_FILENAME") %>'
                                                                        CommandName="View" CommandArgument='<%# Bind("STD_ID") %>' OnClick="txtDocName_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkTDelete" runat="server" CausesValidation="False" CommandName="delete"
                                                            Text="Delete"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="c2" TargetControlID="lnkTDelete" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                            runat="server">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gvLesson" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20"
                                            BorderColor="#1b80b6" BorderStyle="None">
                                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLesId" runat="server" Text='<%# Bind("LES_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("LES_DESCRIPTION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtComments" TextMode="MultiLine" SkinID="MultiText_Medium" Text='<%# Bind("LES_COMMENTS") %>'
                                                            runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSylId" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                            CausesValidation="true" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgCalendar1"
                    TargetControlID="txtFromDate" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="imgCalendar2"
                    TargetControlID="txtToDate" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_SyllabusId" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_TopicID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_EMP_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_ACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_TRM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_SYT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_Completed" runat="server"></asp:HiddenField>
                 
    <asp:HiddenField ID="h_GRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_GRP_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_SUBJ_ID" runat="server"></asp:HiddenField>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                    TargetControlID="CompareValidator1">
                </ajaxToolkit:ValidatorCalloutExtender>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
                    TargetControlID="RequiredFieldValidator1">
                </ajaxToolkit:ValidatorCalloutExtender>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server"
                    TargetControlID="RequiredFieldValidator2">
                </ajaxToolkit:ValidatorCalloutExtender>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server"
                    TargetControlID="RequiredFieldValidator2">
                </ajaxToolkit:ValidatorCalloutExtender>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server"
                    TargetControlID="RequiredFieldValidator3">
                </ajaxToolkit:ValidatorCalloutExtender>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server"
                    TargetControlID="RequiredFieldValidator4">
                </ajaxToolkit:ValidatorCalloutExtender>
            </div>
        </div>
    </div>
</asp:Content>
