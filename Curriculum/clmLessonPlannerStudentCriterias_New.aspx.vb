﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmLessonPlannerStudentCriterias
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Session("Popup") = "0"
            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100519") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                lblReportCaption.Text = Mainclass.GetMenuCaption(ViewState("MainMnu_code"))

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                '  gvStudent.Attributes.Add("bordercolor", "#1b80b6")

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindGrade()
                BindSubjects()
                BindGroup()
                BindTerm()
                btnSave.Visible = False
                ViewState("Sort") = ""
                'gvStudent.Attributes.Add("bordercolor", "#1b80b6")
                trGrid.Visible = False
                trSave.Visible = False
                h_TopicID.Value = ""
                tdhide.Visible = False
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        Else
            'reload data after savingthrough popup
            If Session("Popup") = "1" Then
                Session("Popup") = "0"
                btnView_Click(sender, e)
            End If
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Freeze Grid", "FreezeGrid();", True)

        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindTerm()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = "SELECT TRM_DESCRIPTION,TRM_ID FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
            Dim str_query As String = " SELECT TSM_ID,TSM_DESCRIPTION,TSM_BBLOCKED FROM [dbo].[term_sub_master] " _
                                    & " INNER JOIN [dbo].[term_master]  ON TSM_TRM_ID = TRM_ID WHERE TRM_BSU_ID ='" + Session("SBSUID") + "'" _
                                    & " AND TRM_ACD_ID =" + ddlAcademicYear.SelectedItem.Value + "  " _
                                    & " ORDER BY TSM_DISPLAY_ORDER,TSM_ID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlTerm.DataSource = ds
            ddlTerm.DataTextField = "TSM_DESCRIPTION"
            ddlTerm.DataValueField = "TSM_ID"
            ddlTerm.DataBind()

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                If ds.Tables(0).Rows(i)("TSM_BBLOCKED") Then
                    ddlTerm.Items(i).Attributes.Add("style", "color:red;")
                End If
            Next

            Dim str_query1 As String = "select TSM_TRM_ID from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString
            h_TRM_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query1)
        Catch ex As Exception
            lblError.Text = "Request couldn't be processed."
        End Try
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY," _
                                  & "  GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID)  GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                  & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                  & " INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID " _
                                  & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'"

            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If

            str_query += " ORDER BY GRD_DISPLAYORDER"
        Else
            str_query = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY  ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY," _
                        & "  GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID)  GRM_GRD_ID" _
                        & " ,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                        & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                        & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                        & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                        & " INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID  WHERE " _
      & "  grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString & " AND d.SGS_EMP_ID = " & Session("EmployeeId")

        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        'Dim li As New ListItem
        'li.Text = "Select"
        'li.Value = "0"
        'ddlGrade.Items.Insert(0, li)
    End Sub

    Sub BindSubjects()
        Dim grd_id() As String = ddlGrade.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  " _
                       & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND SBG_GRD_ID='" + grd_id(0) + "'" _
                       & " AND SBG_STM_ID=" + grd_id(1)
        Else
            str_query = "SELECT DISTINCT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                & " INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID" _
                & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                & " AND SBG_GRD_ID='" + grd_id(0) + "'" _
                & " AND SBG_STM_ID=" + grd_id(1) _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")


        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M  " _
                       & " WHERE SGR_SBG_ID='" + sbg_id(0) + "'"

        Else
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " AND SGR_SBG_ID='" + sbg_id(0) + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub

    Sub BindAgeBand()
        Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString)
        Dim cmd As New SqlCommand("[OASIS].dbo.[GET_GRADE_AGEBAND]", sqlCon)
        cmd.CommandType = CommandType.StoredProcedure
        sqlCon.Open()
        Using dr As SqlDataReader = cmd.ExecuteReader()
            If dr.HasRows Then
                ddlAgeband.DataSource = dr
                ddlAgeband.DataTextField = "GRD_AGEB_CATEGORY"
                ddlAgeband.DataValueField = "GRD_AGEB_ID"
                ddlAgeband.DataBind()
                Dim li As New ListItem
                li.Text = "Select"
                li.Value = "0"
                ddlAgeband.Items.Insert(0, li)
            End If
        End Using
        sqlCon.Close()
    End Sub
    Protected Function CheckAgeBand() As Boolean

        Try
            Dim bool As Boolean = False
            ' code portion to find the ageband flag 
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue, SqlDbType.BigInt)
            param(2) = Mainclass.CreateSqlParameter("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0), SqlDbType.VarChar)
            bool = SqlHelper.ExecuteScalar(str_conn, "GET_GRADE_ISAGEBAND", param)
            ' end of code portion 
            Return bool

        Catch ex As Exception
            Return False
        End Try
    End Function
    Protected Function HasSteps() As Boolean

        Try
            Dim bool As Boolean = False
            ' code portion to find the ageband flag 
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim param(2) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue, SqlDbType.BigInt)
            param(2) = Mainclass.CreateSqlParameter("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0), SqlDbType.VarChar)
            bool = SqlHelper.ExecuteScalar(str_conn, "[SYL].[HAS_STEPS]", param)
            ' end of code portion 
            Return bool

        Catch ex As Exception
            Return False
        End Try
    End Function

    Sub GridBind()
        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT DISTINCT STU_ID,[OASIS].[dbo].[fnProperCase](STU_FIRSTNAME) STU_FIRSTNAME ,[OASIS].[dbo].[fnProperCase](ISNULL(STU_LASTNAME,ISNULL(STU_MIDNAME,''))) AS STU_LASTNAME," _
                       & " STU_NO FROM STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID" _
                       & " WHERE STU_CURRSTATUS<>'CN' AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString

        If txtStudentID.Text <> "" Then
            str_query += " AND STU_NO=" + txtStudentID.Text
        End If

        If txtStudentName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtStudentName.Text + "'%"
        End If



        str_query += " ORDER BY STU_FIRSTNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dv As DataView = ds.Tables(0).DefaultView

        If ViewState("SortFilter") = "First Name" Then
            dv.Sort = "STU_FIRSTNAME"
        End If

        If ViewState("SortFilter") = "Last Name" Then
            dv.Sort = "STU_LASTNAME"
        End If

        gvStudent.DataSource = dv
        gvStudent.DataBind()
        BindDropdowns()
        GetCriterias(sbg_id(0))
        GetData(sbg_id(1), sbg_id(0))
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Freeze Grid", "FreezeGrid();", True)
    End Sub


    Sub BindDropdowns()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grd_id As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim str_obj As String = ""
        Dim str_query As String = ""

        str_query = "SELECT DAD_CODE,DAD_DESCR,DAD_COLOR_CODE,DAD_ORDER FROM  dbo.defaultaero_m " _
                            & " INNER JOIN  dbo.defaultaero_d	ON  dam_id = dad_dam_id" _
                            & " WHERE DAM_ACD_ID =" + ddlAcademicYear.SelectedValue + " and dad_type='AD' and dam_grd_ids LIKE '%" + grd_id(0) + "%'"
        str_query += " ORDER BY DAD_ORDER DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds IsNot Nothing And ds.Tables(0) IsNot Nothing Then
            Session("COLOR_CODES") = ds.Tables(0)
        End If
        For z As Integer = ds.Tables(0).Rows.Count - 1 To 0 Step -1
            str_obj += "<span style='font-weight:bold;color:" + ds.Tables(0).Rows(z).Item("DAD_COLOR_CODE") + "'>" + ds.Tables(0).Rows(z).Item("DAD_CODE") + " - " + ds.Tables(0).Rows(z).Item("DAD_DESCR") + "</span>" + " , "
        Next
        ltObjectives.Text = "<div class='higlight'>" + str_obj + "</div>"
        Dim i As Integer
        Dim j, k As Integer

        Dim objH, objId As String

        Dim ddlObjH As DropDownList
        Dim ddlObj As DropDownList

        If rdObj2.Checked = True Then
            If ds.Tables(0).Rows.Count > 25 Then
                j = 25
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = ds.Tables(0).Rows.Count
            End If
        ElseIf rdObj3.Checked = True Then
            If ds.Tables(0).Rows.Count > 50 Then
                j = 50
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = ds.Tables(0).Rows.Count
            End If
        ElseIf rdObj4.Checked = True Then
            If ds.Tables(0).Rows.Count > 75 Then
                j = 75
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = ds.Tables(0).Rows.Count
            End If
        Else
            j = 0
            If ds.Tables(0).Rows.Count > 25 Then
                k = 25
            Else
                k = ds.Tables(0).Rows.Count
            End If
        End If

        Dim p As Integer = 1
        For p = 1 To 25
            objH = "ddlObjH" + CStr(p)
            ddlObjH = gvStudent.HeaderRow.FindControl(objH)
            ddlObjH.Items.Clear()

            For i = j To k - 1


                Dim DropDownListItem As New ListItem
                DropDownListItem.Value = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Text = ds.Tables(0).Rows(i).Item("DAD_CODE")
                DropDownListItem.Attributes.Add("style", "background:" + ds.Tables(0).Rows(i).Item("DAD_COLOR_CODE") + " !important; color:#333 !important")

                '  ddlObjH.Style.Add("color", ds.Tables(0).Rows(i).Item(1))
                ddlObjH.Items.Insert(0, DropDownListItem)
                '  ddlObj.Items.Insert(CType(ds.Tables(0).Rows(i).Item(2), Integer), DropDownListItem)
            Next
            Dim DropDownListItemBlank As New ListItem
            DropDownListItemBlank.Value = "0"
            DropDownListItemBlank.Text = "-"
            ddlObjH.Items.Insert(0, DropDownListItemBlank)
        Next

        For Each gvrow As GridViewRow In gvStudent.Rows

            For p = 1 To 25
                objId = "ddlObj" + CStr(p) + "Id"
                ddlObj = gvrow.FindControl(objId)
                ddlObj.Items.Clear()
                For i = j To k - 1

                    Dim DropDownListItem As New ListItem
                    DropDownListItem.Value = ds.Tables(0).Rows(i).Item("DAD_CODE")
                    DropDownListItem.Text = ds.Tables(0).Rows(i).Item("DAD_CODE")
                    DropDownListItem.Attributes.Add("style", "background:" + ds.Tables(0).Rows(i).Item("DAD_COLOR_CODE") + " !important; color:#333 !important")
                    ddlObj.Items.Insert(0, DropDownListItem)
                Next
                Dim DropDownListItemBlank As New ListItem
                DropDownListItemBlank.Value = "0"
                DropDownListItemBlank.Text = "-"
                ddlObj.Items.Insert(0, DropDownListItemBlank)
            Next
        Next

    End Sub


    Sub GetCriterias(ByVal sbg_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        If Not ddlSteps.SelectedItem Is Nothing Then

            If ddlSteps.SelectedItem.Value <> "0" Then

                If txtTopic.Text = "" Then
                    str_query = "SELECT SYC_ID,SYC_DESCR,SYC_ENDDT,SYC_SYD_ID,isnull((SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID),'')+'-' + SYD_DESCR AS  Topic,SYC_STEP AS STEPS FROM SYL.LESSON_CRITERIA_M " _
                                        & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID " _
                                        & " INNER JOIN SYL.SYLLABUS_M  ON A.SYD_SYM_ID=SYM_ID " _
                                        & " WHERE SYC_SBG_ID=" + sbg_id _
                                        & " AND isnull(SYC_ISAGEBAND,0)= " + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0").ToString() _
                                        & " AND isnull(SYC_STEP,0)= '" + ddlSteps.SelectedItem.Value + "'" _
                                        & " AND ISNULL(SYC_bEVALUATION,0)=1 AND SYM_TRM_ID =(select TSM_TRM_ID from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString + ")"

                Else
                    str_query = "SELECT SYC_ID,SYC_DESCR,SYC_ENDDT,SYC_SYD_ID,isnull((SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID),'')+'-' + SYD_DESCR AS  Topic,SYC_STEP AS STEPS FROM SYL.LESSON_CRITERIA_M " _
                                            & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID " _
                                            & " INNER JOIN SYL.SYLLABUS_M  ON A.SYD_SYM_ID=SYM_ID " _
                                            & " WHERE SYC_SBG_ID=" + sbg_id _
                                            & " AND (SYD_ID=" + h_TopicID.Value & " OR SYD_PARENT_ID=" + h_TopicID.Value + ") AND isnull(SYC_ISAGEBAND,0)= " + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0").ToString() _
                                            & " AND isnull(SYC_STEP,0)= '" + ddlSteps.SelectedItem.Value + "'" _
                                            & " AND ISNULL(SYC_bEVALUATION,0)=1 AND SYM_TRM_ID =(select TSM_TRM_ID from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString + ")"

                End If
            Else
                If txtTopic.Text = "" Then
                    str_query = "SELECT SYC_ID,SYC_DESCR,SYC_ENDDT,SYC_SYD_ID,isnull((SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID),'')+'-' + SYD_DESCR AS  Topic,SYC_STEP AS STEPS FROM SYL.LESSON_CRITERIA_M " _
                                        & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID " _
                                        & " INNER JOIN SYL.SYLLABUS_M  ON A.SYD_SYM_ID=SYM_ID " _
                                        & " WHERE SYC_SBG_ID=" + sbg_id _
                                        & " AND isnull(SYC_ISAGEBAND,0)= " + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0").ToString() _
                                        & " AND ISNULL(SYC_bEVALUATION,0)=1 AND SYM_TRM_ID =(select TSM_TRM_ID from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString + ")"

                Else
                    str_query = "SELECT SYC_ID,SYC_DESCR,SYC_ENDDT,SYC_SYD_ID,isnull((SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID),'')+'-' + SYD_DESCR AS  Topic,SYC_STEP AS STEPS FROM SYL.LESSON_CRITERIA_M " _
                                            & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID " _
                                            & " INNER JOIN SYL.SYLLABUS_M  ON A.SYD_SYM_ID=SYM_ID " _
                                            & " WHERE SYC_SBG_ID=" + sbg_id _
                                            & " AND (SYD_ID=" + h_TopicID.Value & " OR SYD_PARENT_ID=" + h_TopicID.Value + ") AND isnull(SYC_ISAGEBAND,0)= " + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0").ToString() _
                                            & " AND ISNULL(SYC_bEVALUATION,0)=1 AND SYM_TRM_ID =(select TSM_TRM_ID from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString + ")"

                End If
            End If
        Else
            If txtTopic.Text = "" Then
                str_query = "SELECT SYC_ID,SYC_DESCR,SYC_ENDDT,SYC_SYD_ID,isnull((SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID),'')+'-' + SYD_DESCR AS  Topic,SYC_STEP AS STEPS FROM SYL.LESSON_CRITERIA_M " _
                                    & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID " _
                                    & " INNER JOIN SYL.SYLLABUS_M  ON A.SYD_SYM_ID=SYM_ID " _
                                    & " WHERE SYC_SBG_ID=" + sbg_id _
                                    & " AND isnull(SYC_ISAGEBAND,0)= " + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0").ToString() _
                                    & " AND ISNULL(SYC_bEVALUATION,0)=1 AND SYM_TRM_ID =(select TSM_TRM_ID from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString + ")"

            Else
                str_query = "SELECT SYC_ID,SYC_DESCR,SYC_ENDDT,SYC_SYD_ID,isnull((SELECT SYD_DESCR FROM SYL.SYLLABUS_D WHERE  SYD_ID = A.SYD_PARENT_ID),'')+'-' + SYD_DESCR AS  Topic,SYC_STEP AS STEPS FROM SYL.LESSON_CRITERIA_M " _
                                        & " INNER JOIN SYL.SYLLABUS_D A ON SYC_SYD_ID=SYD_ID " _
                                        & " INNER JOIN SYL.SYLLABUS_M  ON A.SYD_SYM_ID=SYM_ID " _
                                        & " WHERE SYC_SBG_ID=" + sbg_id _
                                        & " AND (SYD_ID=" + h_TopicID.Value & " OR SYD_PARENT_ID=" + h_TopicID.Value + ") AND isnull(SYC_ISAGEBAND,0)= " + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0").ToString() _
                                        & " AND ISNULL(SYC_bEVALUATION,0)=1 AND SYM_TRM_ID =(select TSM_TRM_ID from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString + ")"

            End If
        End If


        str_query += " ORDER BY SYC_ID,SYC_STEP,SYC_STARTDT,SYC_DESCR"
        Dim str_topic = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dv As DataView = New DataView(ds.Tables(0))
        Dim Topicsdt As DataTable = dv.ToTable(True, "Topic")
        For z As Integer = 0 To Topicsdt.Rows.Count - 1
            str_topic += "<span class='Topic'>" + Topicsdt.Rows(z).Item("Topic") + "<br></span> "
        Next
        ltTopics.Text = str_topic
        If Topicsdt.Rows.Count > 0 Then
            divselectedTopic.Visible = True
        End If
        Dim i As Integer
        Dim j, k As Integer

        Dim hdr, hdrId, dt, syd As String
        Dim steps, divsteps As String
        Dim lblHdr As Label
        Dim lblHdrId As Label
        Dim lblDt As Label
        Dim lblSydID As Label
        Dim lblStep As Label
        Dim dvStep As HtmlControl

        If rdObj2.Checked = True Then
            If ds.Tables(0).Rows.Count > 25 Then
                j = 25
                If ds.Tables(0).Rows.Count > 50 Then
                    k = 50
                Else
                    k = ds.Tables(0).Rows.Count
                End If

            Else
                j = 0
                k = 0
            End If
        ElseIf rdObj3.Checked = True Then
            If ds.Tables(0).Rows.Count > 50 Then
                j = 50
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = 0
            End If
        ElseIf rdObj4.Checked = True Then
            If ds.Tables(0).Rows.Count > 75 Then
                j = 75
                k = ds.Tables(0).Rows.Count
            Else
                j = 0
                k = 0
            End If
        Else
            j = 0
            If ds.Tables(0).Rows.Count > 25 Then
                k = 25
            Else
                k = ds.Tables(0).Rows.Count
            End If
        End If

        Dim p As Integer = 1
        For i = j To k - 1
            hdr = "lblObj" + CStr(p) + "H"
            hdrId = "lblObjId" + CStr(p)
            dt = "lblObjDt" + CStr(p)
            syd = "lblSydID" + CStr(p)
            steps = "lblStep" + CStr(p)
            divsteps = "divStep" + CStr(p)

            lblHdr = gvStudent.HeaderRow.FindControl(hdr)
            lblHdrId = gvStudent.HeaderRow.FindControl(hdrId)
            lblDt = gvStudent.HeaderRow.FindControl(dt)
            lblSydID = gvStudent.HeaderRow.FindControl(syd)
            lblStep = gvStudent.HeaderRow.FindControl(steps)
            dvStep = gvStudent.HeaderRow.FindControl(divsteps)

            lblHdr.Text = ds.Tables(0).Rows(i).Item(1)
            lblHdrId.Text = ds.Tables(0).Rows(i).Item(0)
            lblSydID.Text = ds.Tables(0).Rows(i).Item(3)
            If ds.Tables(0).Rows(i).Item(5).ToString() = "" Then
                dvStep.Visible = False
            Else
                dvStep.Visible = True
                lblStep.Text = ds.Tables(0).Rows(i).Item(5)
            End If

            If Not lblDt Is Nothing Then
                lblDt.Text = Format(CDate(ds.Tables(0).Rows(i).Item(2).ToString), "dd-MMM")
            End If
            lblHdr.Attributes.Add("title", ds.Tables(0).Rows(i).Item(1))

            'If ds.Tables(0).Rows(i).Item(2) = True Then
            '    lblHdr.ForeColor = Drawing.Color.Maroon
            'End If
            p += 1
        Next
    End Sub


    Sub GetData(ByVal sbm_id As String, ByVal sbg_id As String)
        Dim objData As New Hashtable
        Dim str_query As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        If txtTopic.Text <> "" Then
            str_query = "SELECT STC_STU_ID,STC_SYC_ID,STC_STATUS+'|'+CAST(STC_TSM_ID as nvarchar(20)) FROM SYL.STUDENT_LESSONCRITERIA_S" _
                               & " INNER JOIN STUDENT_GROUPS_S ON STC_STU_ID=SSD_STU_ID " _
                               & " INNER JOIN SYL.SYLLABUS_D ON SYD_ID=STC_SYD_ID " _
                               & " WHERE (SYD_ID=" + h_TopicID.Value + " OR SYD_PARENT_ID=" + h_TopicID.Value + ") AND STC_SBG_ID=" + sbg_id _
                               & " AND STC_TSM_ID='" + ddlTerm.SelectedValue.ToString + "' AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
            '    & "  AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
        Else
            str_query = "SELECT STC_STU_ID,STC_SYC_ID,STC_STATUS+'|'+CAST(STC_TSM_ID as nvarchar(20)) FROM SYL.STUDENT_LESSONCRITERIA_S" _
                                   & " INNER JOIN STUDENT_GROUPS_S ON STC_STU_ID=SSD_STU_ID " _
                                   & " INNER JOIN SYL.SYLLABUS_D ON SYD_ID=STC_SYD_ID " _
                                   & " WHERE  STC_SBG_ID=" + sbg_id _
             & " AND STC_TSM_ID='" + ddlTerm.SelectedValue.ToString + "' AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
            ' & "  AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim j As Integer

        Dim lblHdr As Label
        Dim lblHdrId As Label
        Dim lblSydId As Label

        Dim ddlObj As DropDownList
        Dim lblTsmId As Label
        Dim img_graph As ImageButton
        Dim lblFirstName As Label
        Dim lblLastName As Label
        Dim lblStuId As Label
        Dim lblStuNo As Label
        Dim dtColor As DataTable = Session("COLOR_CODES")
        Dim strLink As String
        Dim objLink As String

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                objData.Add(.Item(0).ToString + "_" + .Item(1).ToString, .Item(2).ToString)
            End With
        Next

        For i = 0 To gvStudent.Rows.Count - 1
            img_graph = gvStudent.Rows(i).FindControl("img_graph")
            lblFirstName = gvStudent.Rows(i).FindControl("lblFirstName")
            lblLastName = gvStudent.Rows(i).FindControl("lblLastName")
            lblStuId = gvStudent.Rows(i).FindControl("lblStuId")
            lblStuNo = gvStudent.Rows(i).FindControl("lblStuNo")


            strLink = "clmLessonStudentCriterias_Popup.aspx?accyear=" + ddlAcademicYear.SelectedItem.Text _
                     & "&grade=" + ddlGrade.SelectedItem.Text _
                     & "&group=" + ddlGroup.SelectedItem.Text _
                     & "&subject=" + ddlSubject.SelectedItem.Text.ToString _
                     & "&name=" + Replace(lblFirstName.Text + " " + lblLastName.Text, "'", "") _
                     & "&sbgid=" + sbg_id _
                     & "&sbmid=" + sbm_id _
                     & "&grdid=" + ddlGrade.SelectedValue.ToString _
                     & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
                     & "&stuno=" + lblStuNo.Text _
                     & "&sgr_id=" + ddlGroup.SelectedItem.Value _
                     & "&topic=" + txtTopic.Text _
                     & "&tsmid=" + ddlTerm.SelectedValue.ToString _
                     & "&topicid=" + h_TopicID.Value _
                     & "&isageband=" + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0")
            lblFirstName.Attributes.Add("onclick", "javascript:var popup = PopupBig('" + strLink + "');")
            lblLastName.Attributes.Add("onclick", "javascript:var popup = PopupBig('" + strLink + "');")

            '----------------------------------------------------------GRAPH CLICK------------------------------------------------------
            strLink = "clmLessonStudentCriterias_GRAPH.aspx?accyear=" + ddlAcademicYear.SelectedItem.Text _
                    & "&grade=" + ddlGrade.SelectedItem.Text _
                    & "&group=" + ddlGroup.SelectedItem.Text _
                    & "&subject=" + ddlSubject.SelectedItem.Text.ToString _
                    & "&name=" + Replace(lblFirstName.Text + " " + lblLastName.Text, "'", "") _
                    & "&sbgid=" + sbg_id _
                    & "&grdid=" + ddlGrade.SelectedValue.ToString _
                    & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
                    & "&stuno=" + lblStuNo.Text _
                    & "&sgr_id=" + ddlGroup.SelectedItem.Value _
                    & "&topic=" + txtTopic.Text _
                    & "&tsmid=" + ddlTerm.SelectedValue.ToString _
                    & "&topicid=" + h_TopicID.Value
            img_graph.Attributes.Add("onclick", "javascript:var popup = PopupBig('" + strLink + "');")


            For j = 0 To 24
                lblHdr = gvStudent.HeaderRow.FindControl("lblObj" + CStr(j + 1) + "H")
                lblHdrId = gvStudent.HeaderRow.FindControl("lblObjId" + CStr(j + 1))
                lblSydId = gvStudent.HeaderRow.FindControl("lblSydId" + CStr(j + 1))
                objLink = "clmLessonStudentCriterias_Popup.aspx?accyear=" + ddlAcademicYear.SelectedItem.Text _
                  & "&grade=" + ddlGrade.SelectedItem.Text _
                  & "&group=" + ddlGroup.SelectedItem.Text _
                  & "&subject=" + ddlSubject.SelectedItem.Text.ToString _
                  & "&sbgid=" + sbg_id _
                  & "&sbmid=" + sbm_id _
                  & "&grdid=" + ddlGrade.SelectedValue.ToString _
                  & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
                  & "&obj_name=" + lblHdr.Text.ToString _
                  & "&obj_id=" + lblHdrId.Text.ToString _
                  & "&sgr_id=" + ddlGroup.SelectedItem.Value _
                  & "&tsmid=" + ddlTerm.SelectedValue.ToString _
                  & "&topic=" + txtTopic.Text _
                  & "&topicid=" + h_TopicID.Value _
                  & "&isageband=" + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0")
                lblHdr.Attributes.Add("onclick", "javascript:var popup = PopupBig('" + objLink + "');")

                If lblHdrId.Text <> "" And lblHdrId.Text <> "0" Then
                    ddlObj = gvStudent.Rows(i).FindControl("ddlObj" + CStr(j + 1) + "Id")
                    lblTsmId = gvStudent.Rows(i).FindControl("lbl_TSM_ID_" + CStr(j + 1))
                    If Not objData.Item(lblStuId.Text + "_" + lblHdrId.Text) Is Nothing Then
                        '  chkObj.Checked = objData.Item(lblStuId.Text + "_" + lblHdrId.Text)
                        If Not ddlObj.Items.FindByValue(objData.Item(lblStuId.Text + "_" + lblHdrId.Text).ToString.Split("|")(0)) Is Nothing Then
                            ddlObj.ClearSelection()
                            ddlObj.Items.FindByValue(objData.Item(lblStuId.Text + "_" + lblHdrId.Text).ToString.Split("|")(0)).Selected = True

                            'FOR DISABLING THE DROPDOWN BASED ON TERM
                            'If objData.Item(lblStuId.Text + "_" + lblHdrId.Text).ToString.Split("|")(1) = ddlTerm.SelectedValue Then
                            '    ddlObj.Enabled = True
                            'Else
                            '    ddlObj.Enabled = False
                            '    ddlObj.Style.Add("border-color", "red !important;")
                            'End If


                            For cnt = 0 To dtColor.Rows.Count - 1
                                If ddlObj.SelectedItem.Value.ToString = dtColor.Rows(cnt).Item("DAD_CODE") Then
                                    ddlObj.Style.Add("background", dtColor.Rows(cnt).Item("DAD_COLOR_CODE") + " !important; color:#333 !important")
                                    ddlObj.Items.FindByValue(ddlObj.SelectedItem.Value).Attributes.Add("style", "background:" + dtColor.Rows(cnt).Item("DAD_COLOR_CODE") + " !important; color:#333 !important")

                                End If
                            Next
                        End If
                    End If
                    gvStudent.Columns(6 + j).Visible = True
                Else
                    gvStudent.Columns(6 + j).Visible = False
                End If
            Next
        Next

    End Sub

    Sub GetPrevious_Data(ByVal sbm_id As String, ByVal sbg_id As String)
        Dim objData As New Hashtable
        Dim str_query As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        If txtTopic.Text <> "" Then
            str_query = "EXEC [SYL].[GET_PREVIOUS_DATA] @SBG_ID='" + sbg_id + "', @SGR_ID='" + ddlGroup.SelectedValue.ToString + "', @Topic_ID='" + h_TopicID.Value + "'"
        Else

            str_query = "EXEC [SYL].[GET_PREVIOUS_DATA] @SBG_ID='" + sbg_id + "', @SGR_ID='" + ddlGroup.SelectedValue.ToString + "', @Topic_ID='0'"
        End If




        'str_query = "SELECT STC_STU_ID,STC_SYC_ID,STC_STATUS+'|'+CAST(STC_TSM_ID as nvarchar(20)) FROM SYL.STUDENT_LESSONCRITERIA_S" _
        '                   & " INNER JOIN STUDENT_GROUPS_S ON STC_STU_ID=SSD_STU_ID " _
        '                   & " INNER JOIN SYL.SYLLABUS_D ON SYD_ID=STC_SYD_ID " _
        '                   & " WHERE (SYD_ID=" + h_TopicID.Value + " OR SYD_PARENT_ID=" + h_TopicID.Value + ") AND STC_SBG_ID=" + sbg_id _
        '                   & " AND STC_TSM_ID='" + ddlTerm.SelectedValue.ToString + "' AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
        ''    & "  AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
        'Else
        'str_query = "SELECT STC_STU_ID,STC_SYC_ID,STC_STATUS+'|'+CAST(STC_TSM_ID as nvarchar(20)) FROM SYL.STUDENT_LESSONCRITERIA_S" _
        '                       & " INNER JOIN STUDENT_GROUPS_S ON STC_STU_ID=SSD_STU_ID " _
        '                       & " INNER JOIN SYL.SYLLABUS_D ON SYD_ID=STC_SYD_ID " _
        '                       & " WHERE  STC_SBG_ID=" + sbg_id _
        ' & " AND STC_TSM_ID='" + ddlTerm.SelectedValue.ToString + "' AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString
        '' & "  AND SSD_SGR_ID=" + ddlGroup.SelectedValue.ToString


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim j As Integer

        Dim lblHdr As Label
        Dim lblHdrId As Label
        Dim lblSydId As Label

        Dim ddlObj As DropDownList
        Dim lblTsmId As Label
        Dim img_graph As ImageButton
        Dim lblFirstName As Label
        Dim lblLastName As Label
        Dim lblStuId As Label
        Dim lblStuNo As Label
        Dim dtColor As DataTable = Session("COLOR_CODES")
        Dim strLink As String
        Dim objLink As String

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                objData.Add(.Item(0).ToString + "_" + .Item(1).ToString, .Item(2).ToString)
            End With
        Next

        For i = 0 To gvStudent.Rows.Count - 1
            img_graph = gvStudent.Rows(i).FindControl("img_graph")
            lblFirstName = gvStudent.Rows(i).FindControl("lblFirstName")
            lblLastName = gvStudent.Rows(i).FindControl("lblLastName")
            lblStuId = gvStudent.Rows(i).FindControl("lblStuId")
            lblStuNo = gvStudent.Rows(i).FindControl("lblStuNo")


            strLink = "clmLessonStudentCriterias_Popup.aspx?accyear=" + ddlAcademicYear.SelectedItem.Text _
                     & "&grade=" + ddlGrade.SelectedItem.Text _
                     & "&group=" + ddlGroup.SelectedItem.Text _
                     & "&subject=" + ddlSubject.SelectedItem.Text.ToString _
                     & "&name=" + Replace(lblFirstName.Text + " " + lblLastName.Text, "'", "") _
                     & "&sbgid=" + sbg_id _
                     & "&sbmid=" + sbm_id _
                     & "&grdid=" + ddlGrade.SelectedValue.ToString _
                     & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
                     & "&stuno=" + lblStuNo.Text _
                     & "&sgr_id=" + ddlGroup.SelectedItem.Value _
                     & "&topic=" + txtTopic.Text _
                     & "&tsmid=" + ddlTerm.SelectedValue.ToString _
                     & "&topicid=" + h_TopicID.Value _
                     & "&isageband=" + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0")
            lblFirstName.Attributes.Add("onclick", "javascript:var popup = PopupBig('" + strLink + "');")
            lblLastName.Attributes.Add("onclick", "javascript:var popup = PopupBig('" + strLink + "');")

            '----------------------------------------------------------GRAPH CLICK------------------------------------------------------
            strLink = "clmLessonStudentCriterias_GRAPH.aspx?accyear=" + ddlAcademicYear.SelectedItem.Text _
                    & "&grade=" + ddlGrade.SelectedItem.Text _
                    & "&group=" + ddlGroup.SelectedItem.Text _
                    & "&subject=" + ddlSubject.SelectedItem.Text.ToString _
                    & "&name=" + Replace(lblFirstName.Text + " " + lblLastName.Text, "'", "") _
                    & "&sbgid=" + sbg_id _
                    & "&grdid=" + ddlGrade.SelectedValue.ToString _
                    & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
                    & "&stuno=" + lblStuNo.Text _
                    & "&sgr_id=" + ddlGroup.SelectedItem.Value _
                    & "&topic=" + txtTopic.Text _
                    & "&tsmid=" + ddlTerm.SelectedValue.ToString _
                    & "&topicid=" + h_TopicID.Value
            img_graph.Attributes.Add("onclick", "javascript:var popup = PopupBig('" + strLink + "');")


            For j = 0 To 24
                lblHdr = gvStudent.HeaderRow.FindControl("lblObj" + CStr(j + 1) + "H")
                lblHdrId = gvStudent.HeaderRow.FindControl("lblObjId" + CStr(j + 1))
                lblSydId = gvStudent.HeaderRow.FindControl("lblSydId" + CStr(j + 1))
                objLink = "clmLessonStudentCriterias_Popup.aspx?accyear=" + ddlAcademicYear.SelectedItem.Text _
                  & "&grade=" + ddlGrade.SelectedItem.Text _
                  & "&group=" + ddlGroup.SelectedItem.Text _
                  & "&subject=" + ddlSubject.SelectedItem.Text.ToString _
                  & "&sbgid=" + sbg_id _
                  & "&sbmid=" + sbm_id _
                  & "&grdid=" + ddlGrade.SelectedValue.ToString _
                  & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
                  & "&obj_name=" + lblHdr.Text.ToString _
                  & "&obj_id=" + lblHdrId.Text.ToString _
                  & "&sgr_id=" + ddlGroup.SelectedItem.Value _
                  & "&tsmid=" + ddlTerm.SelectedValue.ToString _
                  & "&topic=" + txtTopic.Text _
                  & "&topicid=" + h_TopicID.Value _
                  & "&isageband=" + IIf(tdhide.Visible, ddlAgeBand.SelectedValue, "0")
                lblHdr.Attributes.Add("onclick", "javascript:var popup = PopupBig('" + objLink + "');")

                If lblHdrId.Text <> "" And lblHdrId.Text <> "0" Then
                    ddlObj = gvStudent.Rows(i).FindControl("ddlObj" + CStr(j + 1) + "Id")
                    lblTsmId = gvStudent.Rows(i).FindControl("lbl_TSM_ID_" + CStr(j + 1))
                    If Not objData.Item(lblStuId.Text + "_" + lblHdrId.Text) Is Nothing Then
                        '  chkObj.Checked = objData.Item(lblStuId.Text + "_" + lblHdrId.Text)
                        If Not ddlObj.Items.FindByValue(objData.Item(lblStuId.Text + "_" + lblHdrId.Text).ToString.Split("|")(0)) Is Nothing Then
                            ddlObj.ClearSelection()
                            ddlObj.Items.FindByValue(objData.Item(lblStuId.Text + "_" + lblHdrId.Text).ToString.Split("|")(0)).Selected = True

                            'FOR DISABLING THE DROPDOWN BASED ON TERM
                            'If objData.Item(lblStuId.Text + "_" + lblHdrId.Text).ToString.Split("|")(1) = ddlTerm.SelectedValue Then
                            '    ddlObj.Enabled = True
                            'Else
                            '    ddlObj.Enabled = False
                            '    ddlObj.Style.Add("border-color", "red !important;")
                            'End If


                            For cnt = 0 To dtColor.Rows.Count - 1
                                If ddlObj.SelectedItem.Value.ToString = dtColor.Rows(cnt).Item("DAD_CODE") Then
                                    ddlObj.Style.Add("background", dtColor.Rows(cnt).Item("DAD_COLOR_CODE") + " !important; color:#333 !important")
                                    ddlObj.Items.FindByValue(ddlObj.SelectedItem.Value).Attributes.Add("style", "background:" + dtColor.Rows(cnt).Item("DAD_COLOR_CODE") + " !important; color:#333 !important")

                                End If
                            Next
                        End If
                    End If
                    gvStudent.Columns(6 + j).Visible = True
                Else
                    gvStudent.Columns(6 + j).Visible = False
                End If
            Next
        Next

    End Sub


    Sub SaveData()
        Dim i As Integer
        Dim lblObjId As Label
        Dim ddlObj As DropDownList
        Dim lblStuId As Label
        Dim lblSydId As Label

        Dim j As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        Dim grd_id As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim strXml As String = ""

        For i = 0 To gvStudent.Rows.Count - 1
            With gvStudent.Rows(i)
                For j = 0 To 24
                    lblObjId = gvStudent.HeaderRow.FindControl("lblObjId" + CStr(j + 1))
                    lblSydId = gvStudent.HeaderRow.FindControl("lblSydId" + CStr(j + 1))
                    ddlObj = .FindControl("ddlObj" + CStr(j + 1) + "Id")
                    lblStuId = .FindControl("lblStuId")

                    If lblObjId.Text <> "" And lblObjId.Text <> "0" Then
                        strXml += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID>"
                        strXml += "<SYC_ID>" + lblObjId.Text + "</SYC_ID>"
                        strXml += "<STC_STATUS>" + ddlObj.SelectedValue + "</STC_STATUS>"
                        strXml += "<SYD_ID>" + lblSydId.Text + "</SYD_ID>"
                        strXml += "<STC_EVIDENCE_PATH></STC_EVIDENCE_PATH>"
                        strXml += "<TSM_ID>" + ddlTerm.SelectedValue + "</TSM_ID></ID>"
                    End If
                Next
            End With
        Next

        strXml = "<IDS>" + strXml + "</IDS>"
        str_query = " EXEC [SYL].[saveSTUDENTCRITERIAS_BULK_v2]" _
                & " @ACD_ID =" + ddlAcademicYear.SelectedValue.ToString + "," _
                & " @GRD_ID ='" + grd_id(0).ToString + "'," _
                & " @SBG_ID =" + sbg_id(0) + "," _
                & " @STC_XML ='" + strXml + "'," _
                & " @STC_UPDATEDATE ='" + IIf(txtDate.Text = "", Now.ToString, txtDate.Text) + "'," _
                & " @STC_USER ='" + Session("susr_id") + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        lblError.Text = "Record saved successfully"
    End Sub


#End Region

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        'If txtTopic.Text = "" Then
        '    lblError.Text = "Please select a topic"
        '    Exit Sub
        'Else
        '    lblError.Text = ""
        'End If

        ViewState("SortFilter") = ""
        '  h_LVL_ID.Value = ddlLevel.SelectedValue.ToString
        GridBind()
        If CheckTermStatus() Then
            trSave.Visible = False
            div_status.Visible = True
            lbl_BlockStatus.Text = "Entries for '" + ddlTerm.SelectedItem.Text + "' has been blocked."
            btnSave.Visible = False
            trSave.Visible = False
        Else
            div_status.Visible = False
            lbl_BlockStatus.Text = ""
            btnSave.Visible = True
            trSave.Visible = True
        End If

        trGrid.Visible = True
    End Sub

    Protected Sub lnkFirstNameH_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("SortFilter") = "First Name"
        GridBind()
    End Sub

    Protected Sub lnkLastNameH_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("SortFilter") = "Last Name"
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()

        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        h_TopicID.Value = ""
        txtTopic.Text = ""
        BindGrade()
        BindSubjects()
        BindGroup()
        BindTerm()
        trGrid.Visible = False
        trSave.Visible = False
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        h_TopicID.Value = ""
        txtTopic.Text = ""
        BindSubjects()
        BindGroup()
        trGrid.Visible = False
        trSave.Visible = False
        Dim isAgeBand As Boolean = CheckAgeBand()
        If isAgeBand Then
            BindAgeBand()
            ' ddlAgeband.Enabled = True
            tdhide.Visible = True
        Else
            tdhide.Visible = False
        End If

        If HasSteps() Then
            BindSteps(0)
            tdSteps.Visible = True
            tdStudId.Visible = False
        Else
            tdSteps.Visible = False
            tdStudId.Visible = True
        End If

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        h_TopicID.Value = ""
        txtTopic.Text = ""
        BindGroup()
        trGrid.Visible = False
        trSave.Visible = False
        If HasSteps() Then
            BindSteps(0)
        End If

    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "select TSM_TRM_ID from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString
        h_TRM_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Protected Sub btnPrev_Click(sender As Object, e As EventArgs)
        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        BindDropdowns()
        GetPrevious_Data(sbg_id(1), sbg_id(0))
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Freeze Grid", "FreezeGrid();", True)
    End Sub

    Function CheckTermStatus() As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "select TSM_BBlocked from oasis..TERM_SUB_MASTER where TSM_ID=" + ddlTerm.SelectedValue.ToString
        Dim status As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return status
    End Function

    Protected Sub ddlAgeBand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgeBand.SelectedIndexChanged

    End Sub

    Sub BindSteps(Optional ByVal TopicId As Integer = 0)
        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        ddlSteps.Items.Clear()
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim param(3) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue, SqlDbType.BigInt)
        param(1) = Mainclass.CreateSqlParameter("@GRD_ID", ddlGrade.SelectedValue.Split("|")(0), SqlDbType.VarChar)
        param(2) = Mainclass.CreateSqlParameter("@SBG_ID", sbg_id(0), SqlDbType.BigInt)
        param(3) = Mainclass.CreateSqlParameter("@SYD_ID", TopicId, SqlDbType.BigInt)
        ds = SqlHelper.ExecuteDataset(str_conn, "[SYL].[GET_STEPS]", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlSteps.DataSource = ds
            ddlSteps.DataTextField = "SYC_STEP"
            ddlSteps.DataValueField = "SYC_STEP"
            ddlSteps.DataBind()
        End If
        Dim li As New ListItem
        li.Text = "Select"
        li.Value = "0"
        ddlSteps.Items.Insert(0, li)
    End Sub


    Protected Sub txtTopic_TextChanged(sender As Object, e As EventArgs)
        BindSteps(h_TopicID.Value)
    End Sub
End Class
