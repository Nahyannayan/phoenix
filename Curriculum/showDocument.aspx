﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="showDocument.aspx.vb" Inherits="Curriculum_showDocument" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Select Documents</title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">

    <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">

    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
         </script>
</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
   

    <form id="form1" runat="server">
     <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0"> 
         <tr valign="top"  align="center" class="title">
             <td>
                            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True" PageSize="15">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sel_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                        <ItemTemplate>
                                            &nbsp;<input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            Select <br />
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="ID">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSecDescr" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblID" runat="server" Text="Description"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCode" runat="server" Width="90px"></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />
                                        </HeaderTemplate>
                                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="File Name">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblopt1" class="field-label" runat="server" CssClass="gridheader_text" Text="File Name"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtOption1" runat="server" Width="150px"></asp:TextBox>
                                            <asp:ImageButton ID="btnuid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnuid_Search_Click" />
                                        </HeaderTemplate>
                                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate  >
                                        <table  >
                                        <tr>
                                        <td align="center" > 
                                            <asp:ImageButton 
                                        ID="imgF" runat="server"   onclick="imgF_Click" CommandArgument='<%# Bind("id") %>' />
                                        </td>
                                        </tr>
                                        <tr><td >
                                            <asp:LinkButton ID="txtfile" runat="server" 
                                                Text='<%# Bind("sd_filename") %>' CommandName ="View" CommandArgument='<%# Bind("id") %>'
                                               
                                                onclick="txtfile_Click"  /> </td></tr>
                                                                                </table>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                <span class="field-label"> Select Value</span> </td>
         </tr>
                    <tr valign = "top">
                            <td>
                            <asp:CheckBox ID="chkSelAll" runat="server" CssClass="radiobutton" Text="Select All"
                                AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr> 
                        <td align="center" colspan="3" >
                         <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" />
                        
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                         <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_SelectedId" runat="server" type="hidden" value="" /></td>
                    </tr>
                </table>
         
    </form>
</body>
</html>
