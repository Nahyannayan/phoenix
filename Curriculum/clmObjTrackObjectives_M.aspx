﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmObjTrackObjectives_M.aspx.vb" Inherits="Curriculum_clmObjTrackObjectives_M"
    Title="Untitled Page" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Objectives
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="center" valign="bottom"  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                  Style="text-align:center"  ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td class="matters"  width="20%"><span class="field-label">Select Subject</span>
                                    </td>
                                    <td class="matters"  width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server"  AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                
                                    <td class="matters" align="left" width="20%"><span class="field-label">Select Level</span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters"  ><span class="field-label">Import Data From Excel</span>
                                    </td>
                                    <td class="matters" colspan="2">
                                                    <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True" />
                                        </td>
                                    <td>
                                                    <asp:Button ID="btnUpload" runat="server" CausesValidation="False" CssClass="button"
                                                        TabIndex="30" Text="Upload Excel"  />
                                        </td>
                                    
                                </tr>
                                <tr>
                                    <td class="matters" align="center" colspan="4">
                                        <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align:center">
                                        <asp:GridView ID="gvObjectives" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                             PageSize="20"  >
                                            <RowStyle CssClass="griditem"  Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="index" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblObjId" runat="server" Text='<%# Bind("OBJ_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sl.No" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtOrder" runat="server"   Text='<%# Bind("OBJ_DISPLAYORDER") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Objective">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtObjective" runat="server"  Text='<%# Bind("OBJ_DESCR") %>' Width="100%"
                                                            TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Objective" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblObjective" runat="server"  Text='<%# Bind("OBJ_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Weightage">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWeightage" Text='<%# Bind("OBJ_WEIGHTAGE") %>'  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Category">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlCategory" runat="server"  ></asp:DropDownList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sl.No" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrder" runat="server"  Text='<%# Bind("OBJ_DISPLAYORDER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Weightage" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblWeightage" Text='<%# Bind("OBJ_WEIGHTAGE") %>'  runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Mandatory">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkTarget" Checked='<%# Bind("OBJ_bTARGET") %>'   runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Mandatory" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTarget" Text='<%# Bind("OBJ_bTARGET") %>'   runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="cat" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCatId" Text='<%# Bind("OBJ_OBT_ID") %>'  runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle   CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
