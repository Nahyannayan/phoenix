<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmTargetTrackerAcrossYear.aspx.vb" Inherits="Curriculum_Reports_Aspx_clmTargetTrackerAcrossYear" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            Target Tracker Across Academic Year
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>

                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">

                            <table id="tblClm" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Previous Academic Year</span></td>

                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlPrevAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Current AcademicYear</span></td>

                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlCurrAcademicYear" runat="server" AutoPostBack="True"
                                            >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Previous
                            Grade</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPrevGrade" runat="server" AutoPostBack="true" >
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"><span class="field-label">Current Grade</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlCurrGrade" runat="server" AutoPostBack="true" Width="183px">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td align="left" width="20%"><span class="field-label">Current Section</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlCurrSection" AutoPostBack="true" runat="server"
                                           >
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>

                                    <td align="left" width="20%" ><span class="field-label">Previous Report Schedule</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPrevReportSchedule" AutoPostBack="true" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Current ReportSchedule</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlCurrReportSchedule" AutoPostBack="true"
                                            runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                   
                                    <td colspan="4" ><asp:CheckBox id="chkOption" runat="server" Text="Remove students who were present only in current grade"/></td>
                                    
                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Tracker Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnLevelReport" runat="server" CssClass="button"
                                            Text="Generate Level Tracker Report" ValidationGroup="groupM1" Visible="False" /></td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="h_MnuCode" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>

</asp:Content>

