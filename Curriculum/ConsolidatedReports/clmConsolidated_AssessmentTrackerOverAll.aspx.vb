﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_AssessmentTrackerOverall
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfRPF_IDs.Value = Encr_decrData.Decrypt(Request.QueryString("rpf_ids").Replace(" ", "+"))
        hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
        hfSBG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbgid").Replace(" ", "+"))
        hfSCT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+"))
        hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
        hfbBlank.Value = Encr_decrData.Decrypt(Request.QueryString("bBlank").Replace(" ", "+"))
        GridBind()
    End Sub

    Sub GridBind()
        Dim grade As String() = hfGRD_ID.Value.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ' str_conn = str_conn.Replace("10.0.10.15", "172.16.1.15")
        '  Dim str_query As String = "exec RPT.GETCONSOLIDATEDDATA_TRACKER_EXCEL 1060 ,'03',39184,'5525|5518|5519|5520',0,1"
        Dim str_query As String = "exec RPT.GETCONSOLIDATEDDATA_TRACKER_EXCEL_OVERALL " + hfACD_ID.Value + "," _
                                & "'" + grade(0) + "'," _
                                & hfSBG_ID.Value + "," _
                                & "'" + hfRPF_IDs.Value + "'," _
                                & hfSCT_ID.Value + "," _
                                & "'" + hfbBlank.Value + "'"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            gvSubj.DataSource = ds
            gvSubj.DataBind()
        End If
    End Sub

    Protected Sub gvSubj_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubj.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then


            Dim htHeader As New Hashtable

            Dim i, j As Integer

            Dim HeaderCell1 As New TableCell()
            Dim HeaderCell2 As New TableCell()
            Dim HeaderCell3 As New TableCell()

            Dim HeaderGridRow1 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow2 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow3 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)


            Dim reportHeader As String
            Dim Skill As String
            Dim hType As String

            Dim treportHeader As String = ""
            Dim tSkill As String = ""

            Dim scolspan As Integer = 1
            Dim hcolspan As Integer = 1

            Dim jHeader As Integer = 0
            Dim jSkill As Integer = 0

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Student ID"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Name"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Section"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Gender"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Sen"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            For i = 5 To e.Row.Cells.Count - 1
                Dim shdr As String() = e.Row.Cells(i).Text.Split("__")

                reportHeader = shdr(0)
                If reportHeader <> treportHeader And treportHeader <> "" Then
                    tSkill = "-"
                End If
                Skill = shdr(2)
                If Skill <> tSkill Then
                    If tSkill <> "" Then
                        HeaderCell2.ColumnSpan = hcolspan
                        HeaderGridRow2.Cells.Add(HeaderCell2)

                        'HeaderCell1.ColumnSpan = scolspan
                        'HeaderGridRow1.Cells.Add(HeaderCell1)
                        'HeaderCell1 = New TableCell()
                    End If

                    HeaderCell2 = New TableCell()
                    HeaderCell2.Text = Skill
                    If shdr(6) = "TOTAL" Then
                        HeaderCell2.BackColor = Drawing.Color.LightPink
                        jHeader = 0
                    ElseIf shdr(6) = "CODES" Then
                        HeaderCell2.BackColor = Drawing.Color.PaleGoldenrod
                    Else
                        Select Case jHeader
                            Case "0"
                                HeaderCell2.BackColor = Drawing.Color.LightBlue
                            Case "1"
                                HeaderCell2.BackColor = Drawing.Color.LightGreen
                            Case "2"
                                HeaderCell2.BackColor = Drawing.Color.LightGoldenrodYellow
                            Case "3"
                                HeaderCell2.BackColor = Drawing.Color.LightSteelBlue
                            Case "4"
                                HeaderCell2.BackColor = Drawing.Color.LightSlateGray
                        End Select
                        jHeader += 1
                    End If

                    'scolspan = 1
                    hcolspan = 1
                    tSkill = Skill

                    '  treportHeader = ""
                Else
                    hcolspan += 1
                End If

                If reportHeader <> treportHeader Then
                    If treportHeader <> "" Then
                        HeaderCell1.ColumnSpan = scolspan
                        HeaderGridRow1.Cells.Add(HeaderCell1)
                    End If
                    HeaderCell1 = New TableCell()
                    HeaderCell1.Text = reportHeader
                    If reportHeader.Contains("-TOTAL") = True Or reportHeader.Contains("-GRADEBAND") = True Then
                        HeaderCell1.BackColor = Drawing.Color.Pink
                    ElseIf reportHeader.Contains("-OBJECTIVES") = True Then
                        HeaderCell1.BackColor = Drawing.Color.PaleTurquoise
                    Else
                        HeaderCell1.BackColor = Drawing.Color.YellowGreen
                    End If
                    scolspan = 1
                    treportHeader = reportHeader
                Else
                    scolspan += 1
                End If

                e.Row.Cells(i).Text = shdr(4)

                e.Row.Cells(i).BackColor = Drawing.Color.LightGray

                htHeader.Add(i, shdr(4) + "|" + shdr(6) + "|" + jHeader.ToString)
            Next

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = Skill.Replace("&amp;", "&")
            HeaderCell2.ColumnSpan = hcolspan
            If reportHeader.Contains("-TOTAL") = True Or reportHeader.Contains("-GRADEBAND") = True Then
                HeaderCell2.BackColor = Drawing.Color.LightPink
            ElseIf reportHeader.Contains("-OBJECTIVES") = True Then
                HeaderCell2.BackColor = Drawing.Color.PaleGoldenrod
            End If
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = reportHeader.Replace("&amp;", "&")
            HeaderCell1.ColumnSpan = scolspan
            If reportHeader.Contains("-TOTAL") = True Or reportHeader.Contains("-GRADEBAND") = True Then
                HeaderCell1.BackColor = Drawing.Color.Pink
            ElseIf reportHeader.Contains("-OBJECTIVES") = True Then
                HeaderCell1.BackColor = Drawing.Color.PaleTurquoise
            Else
                HeaderCell1.BackColor = Drawing.Color.YellowGreen
            End If

            HeaderGridRow1.Cells.Add(HeaderCell1)

            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow2)

            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow1)

            Session("htHeader") = htHeader

        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(0).Text = "1" Then
                e.Row.BackColor = Drawing.Color.Green
                e.Row.ForeColor = Drawing.Color.White
            ElseIf e.Row.Cells(0).Text = "2" Then
                e.Row.BackColor = Drawing.Color.DarkGoldenrod
                e.Row.ForeColor = Drawing.Color.White
            ElseIf e.Row.Cells(0).Text = "3" Then
                e.Row.BackColor = Drawing.Color.Red
                e.Row.ForeColor = Drawing.Color.White
            ElseIf e.Row.Cells(0).Text = "4" Then
                e.Row.BackColor = Drawing.Color.Gray
                e.Row.ForeColor = Drawing.Color.White
            End If
            Dim i As Integer
            Dim htHeader As Hashtable = Session("htHeader")
            For i = 5 To e.Row.Cells.Count - 1
                Dim sData As String() = htHeader.Item(i).Split("|")
                If sData.Length > 1 Then
                    If sData(1) = "TOTAL" Then
                        Dim sMarks() As String = e.Row.Cells(i).Text.Split("_")

                        If sMarks.Length > 1 Then
                            e.Row.Cells(i).Text = Replace(sMarks(0), ".00", "")
                            Select Case sMarks(1)
                                Case "GREEN"
                                    e.Row.Cells(i).ForeColor = Drawing.Color.DarkGreen
                                Case "YELLOW"
                                    e.Row.Cells(i).ForeColor = Drawing.Color.DarkGoldenrod
                                Case "RED"
                                    e.Row.Cells(i).ForeColor = Drawing.Color.Red
                                Case "GREY"
                                    e.Row.Cells(i).ForeColor = Drawing.Color.Gray
                            End Select
                        End If
                        'If e.Row.Cells(i).Text <> "&nbsp;" Then
                        '    Dim maxMark As Double = CDbl(sData(0))
                        '    Dim mark As Double = CDbl(e.Row.Cells(i).Text)
                        '    Select Case Math.Round(mark * 100 / maxMark)
                        '        Case 75 To 100
                        '            e.Row.Cells(i).Text = e.Row.Cells(i).Text.Replace(".00", "") + "<font color='green'> ⋆</font>"
                        '        Case 50 To 74
                        '            e.Row.Cells(i).Text = e.Row.Cells(i).Text.Replace(".00", "") + "<font color='yellow'> ⋆</font>"
                        '        Case 25 To 49
                        '            e.Row.Cells(i).Text = e.Row.Cells(i).Text.Replace(".00", "") + "<font color='blue'> ⋆</font>"
                        '        Case 0 To 24
                        '            e.Row.Cells(i).Text = e.Row.Cells(i).Text.Replace(".00", "") + "<font color='red'> ⋆</font>"
                        '    End Select
                        'End If
                        '  e.Row.Cells(i).BackColor = Drawing.Color.LightPink
                    ElseIf sData(1) = "GRADEBAND" Then
                        Dim sGrade() As String = e.Row.Cells(i).Text.Split("_")
                        If sGrade.Length > 1 Then
                            e.Row.Cells(i).Text = sGrade(0)
                            Select Case sGrade(1)
                                Case "GREEN"
                                    e.Row.Cells(i).ForeColor = Drawing.Color.DarkGreen
                                Case "YELLOW"
                                    e.Row.Cells(i).ForeColor = Drawing.Color.DarkGoldenrod
                                Case "RED"
                                    e.Row.Cells(i).ForeColor = Drawing.Color.Red
                                Case "GREY"
                                    e.Row.Cells(i).ForeColor = Drawing.Color.DarkGray
                            End Select
                        End If
                    ElseIf sData(1) = "CODES" Then
                        If e.Row.Cells(0).Text = "00000" Then
                            Dim sCodes As String() = e.Row.Cells(i).Text.Split("|")
                            If sCodes.Length > 1 Then
                                Dim x As Integer
                                Dim y As Integer = 0
                                For x = 0 To sCodes.Length - 1
                                    gvSubj.Rows(y).Cells(i).Controls.Clear()
                                    gvSubj.Rows(y + 1).Cells(i).Controls.Clear()
                                    gvSubj.Rows(y).Cells(i).Text = sCodes(x)
                                    y += 1
                                Next
                            End If
                            e.Row.Visible = False
                        End If
                    Else
                        e.Row.Cells(i).Text = e.Row.Cells(i).Text.Replace(".00", "")
                        'Select Case sData(2)
                        '    Case "1"
                        '        e.Row.Cells(i).BackColor = Drawing.Color.LightBlue
                        '    Case "2"
                        '        e.Row.Cells(i).BackColor = Drawing.Color.LightGreen
                        '    Case "3"
                        '        e.Row.Cells(i).BackColor = Drawing.Color.LightGoldenrodYellow
                        '    Case "4"
                        '        e.Row.Cells(i).BackColor = Drawing.Color.LightSteelBlue
                        '    Case "5"
                        '        e.Row.Cells(i).BackColor = Drawing.Color.LightSlateGray
                        'End Select
                    End If
                End If
            Next
        End If
    End Sub
End Class
