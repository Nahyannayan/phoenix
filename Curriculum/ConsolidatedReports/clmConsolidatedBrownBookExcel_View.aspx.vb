Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports ResponseHelper
Partial Class Curriculum_BrownBook_clmBrownBook_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Dim url As String
        Dim MainMnu_code As String = ViewState("MainMnu_code")
        Dim Grade As String = ddlGrade.SelectedItem.Value
        Dim Section As String = ddlSection.SelectedItem.Value
        Dim strRedirect As String
        Dim grd As String() = ddlGrade.SelectedValue.Split("|")

        strRedirect = "~/Curriculum/ConsolidatedReports/clmConsolidated_ExportBrownBookExcel.aspx"
        MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
        Grade = Encr_decrData.Encrypt(Grade)
        Section = Encr_decrData.Encrypt(Section)
        
        url = String.Format("{0}?MainMnu_code={1}&Grade={2}&Section={3}&acdid={4}", strRedirect, MainMnu_code, Grade, Section, Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString))


        ' ResponseHelper.Redirect(url, "_blank", "")
        ResponseHelper.Redirect(url, "_blank", "width=1,height=1")

    End Sub

    Sub CreateSubjectImages()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                     & " WHERE SBG_GRD_ID='" + grade(0) + "' AND SBG_ACD_ID=" + ddlAcademicyear.selectedvalue.tostring _
                                     & " AND SBG_STM_ID=" + grade(1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0

        Dim imageLib As ImageCreationLibrary
        Dim stImagePath As String
        imageLib = New ImageCreationLibrary(stImagePath)

        Dim imgFilePath As String = ""
        stImagePath = System.Configuration.ConfigurationManager.AppSettings("dynamicimagepath")

        imageLib.BaseImagePath = stImagePath

        With ds.Tables(0)
            For i = 0 To .Rows.Count - 1
                imageLib.CreateVerticalTextImage(.Rows(i).Item(0), New System.Drawing.Font(System.Drawing.FontFamily.GenericSerif, 12))
            Next
        End With


    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")



                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330290") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    PopulateGrade(ddlAcademicYear.SelectedValue.ToString)
                    PopulateSection(ddlAcademicYear.SelectedValue.ToString)

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub


    Public Sub PopulateGrade(ByVal acdid As String)
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

    End Sub

    Sub PopulateSection(ByVal acdid As String)
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Session("sbsuid") = "111001" Or Session("sbsuid") = "115002" Then
            str_query = "select distinct STU_BB_SECTION_DES as SCT_ID,STU_BB_SECTION_DES as SCT_DESCR from student_m " _
                    & " where   [STU_BSU_ID]='" + Session("sbsuid") + "' and [STU_ACD_ID]='" & ddlAcademicYear.SelectedValue.ToString & "'" _
                    & " and STU_GRD_ID='" & grade(0) & "'" _
                    & " and stu_stm_id='" + grade(1) + "' order by STU_BB_SECTION_DES"

        Else
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M AS A " _
                         & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                         & " WHERE GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) _
                         & " AND GRM_ACD_ID=" + acdid _
                         & " ORDER BY SCT_DESCR"

        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub
    Sub Section_Bind()
        Using readerGrade_Section As SqlDataReader = GetGrade_Section(Session("sBsuid"), ddlAcademicYear.SelectedValue.ToString, ddlGrade.SelectedItem.Value)
            ddlSection.Items.Clear()
            ddlSection.DataSource = readerGrade_Section
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataBind()

        End Using
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "ALL"
        ddlSection.Items.Insert(0, li)
    End Sub
    Public Function GetBSU_bbsct_status() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select BSU_BB_SCT FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return IIf(IsDBNull(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)) = False, SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql), "False")

    End Function

    Public Function GetGrade_Section(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Section As String = ""

        If GetBSU_bbsct_status() = False Then
            If SHF_ID = "" Then
                sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "') order by SCT_DESCR"
            Else
                sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND [GRM_SHF_ID]='" & SHF_ID & "' ) order by SCT_DESCR"
            End If
        Else
            If SHF_ID = "" Then
                sqlGrade_Section = "select distinct STU_BB_SECTION_DES as SCT_ID,STU_BB_SECTION_DES as SCT_DESCR from student_m where [STU_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "')   order by SCT_DESCR"
            Else
                sqlGrade_Section = "select distinct STU_BB_SECTION_DES as SCT_ID,STU_BB_SECTION_DES as SCT_DESCR from student_m where [STU_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND [GRM_SHF_ID]='" & SHF_ID & "' ) order by SCT_DESCR"
            End If
        End If


        Dim command As SqlCommand = New SqlCommand(sqlGrade_Section, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function



    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection(ddlAcademicYear.SelectedValue.ToString)
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade(ddlAcademicYear.SelectedValue.ToString)
        PopulateSection(ddlAcademicYear.SelectedValue.ToString)
    End Sub
End Class
