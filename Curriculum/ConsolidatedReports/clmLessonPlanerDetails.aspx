<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmLessonPlanerDetails.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmLessonPlanerDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%">
                <tr>
                    <td align="left">

                        <CR:CrystalReportViewer ID="crv" runat="server" AutoDataBind="true" DisplayPage="true"
                            EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" ReportSourceID="rs"
                            ReuseParameterValuesOnRefresh="True" PrintMode="ActiveX" ShowAllPageIds="false"
                            BestFitPage="True" HasToggleGroupTreeButton="False" HasCrystalLogo="False" HasSearchButton="False"
                            HasDrillUpButton="False" HasZoomFactorList="False" SeparatePages="False" />
                        &nbsp;<CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                        </CR:CrystalReportSource>

                        <asp:HiddenField ID="hSYD_ID" runat="server" />
                        <asp:HiddenField ID="hYear" runat="server" />
                        <asp:HiddenField ID="hGrade" runat="server" />
                        <asp:HiddenField ID="hSubject" runat="server" />
                        <asp:HiddenField ID="hSBG_ID" runat="server" />
                        <asp:HiddenField ID="hSGR_ID" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
