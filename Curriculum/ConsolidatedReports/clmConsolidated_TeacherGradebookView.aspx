﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsolidated_TeacherGradebookView.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_TeacherGradebookView" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];

                curr_elem.checked = master_box.checked;

            }
            // master_box.checked=!master_box.checked;
        }


        function fnSelectAll_bkp(chkObj) {
            var multi = document.getElementById("<%=lstAssessment.ClientID%>");
            if (multi != null && multi.options != null) {
                if (chkObj.checked)
                    for (i = 0; i < multi.options.length; i++)
                        multi.options[i].selected = true;
                else
                    for (i = 0; i < multi.options.length; i++)
                        multi.options[i].selected = false;
            }
        }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Teacher Gradebook Report Card"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center"
                    cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" />
                        </td>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Grade</span>
                        </td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr runat="server" id="trfilter">
                        <td align="left" width="20%" valign="middle"><span class="field-label">Subject</span></td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Group</span></td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlGroup" AutoPostBack="true" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" valign="middle"><span class="field-label">Assessments</span>
                        </td>

                        <td align="left" width="30%" valign="middle" >

                            <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server"
                                Text="Select All" />
                            <br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="lstAssessment" runat="server" >
                            </asp:CheckBoxList></div>
                        </td>
                        <td colspan="2">

                        </td>
                    </tr>

                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" />

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
</asp:Content>

