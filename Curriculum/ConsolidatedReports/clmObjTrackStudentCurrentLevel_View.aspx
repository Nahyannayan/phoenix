<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmObjTrackStudentCurrentLevel_View.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmObjTrackStudentCurrentLevel_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];

                if (curr_elem.type == 'checkbox' && curr_elem.name != 'ctl00$cphMasterpage$chkTC') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        
                        <td align="left"  >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="middle"  ><span class="field-label">Grade</span></td>
                       
                        <td align="left" valign="middle"   >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
                   
                    <tr id="trSubject" runat="server">
                        <td align="left" width="20%" valign="middle" ><span class="field-label">Subject</span></td>
                        
                        <td align="left" valign="middle"  >
                            <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" CssClass="field-label" />
                            <br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="ddlSubject" runat="server" 
                               >
                            </asp:CheckBoxList></div></td>

                         <td align="left" width="20%"  valign="middle"><span class="field-label">Section</span></td>
                       
                        <td align="left"  valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" >
                            </asp:DropDownList> 
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:CheckBox ID="chkShowStudents" runat="server" Checked="True" Text="Show Students" CssClass="field-label"/>
                        </td>
                    </tr>
                  
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" 
                                Text="Generate Report" ValidationGroup="groupM1" />
                             
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfbDownload" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>

