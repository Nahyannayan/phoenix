<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsolidatedLessonPlannerView.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidatedLessonPlannerView"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnSelectAll_Subject(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {

                    if (curr_elem.id.substring(0, 43) == 'ctl00_cphMasterpage_tabPopup_HT1_lstSubject') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }


        function fnSelectAll_Subject4(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {

                    if (curr_elem.id.substring(0, 44) == 'ctl00_cphMasterpage_tabPopup_HT4_lstSubject4') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Month(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 42) == 'ctl00_cphMasterpage_tabPopup_HT1_lstMonths') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Grade(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 41) == 'ctl00_cphMasterpage_tabPopup_HT2_lstGrade') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Month2(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 43) == 'ctl00_cphMasterpage_tabPopup_HT2_lstMonths2') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Month3(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 43) == 'ctl00_cphMasterpage_tabPopup_HT3_lstMonths3') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Month4(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 43) == 'ctl00_cphMasterpage_tabPopup_HT4_lstMonths4') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Group(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 41) == 'ctl00_cphMasterpage_tabPopup_HT3_lstGroup') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }


    </script>
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            Lesson Planner Consolidated
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="0">
                    <ajaxToolkit:TabPanel ID="HT1" runat="server">
                        <ContentTemplate>
                            <table id="Table2" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0" style="border-collapse: collapse;">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Subject</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkSubject" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Subject(this);"></asp:CheckBox><br />
                                         <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstSubject" runat="server" AutoPostBack="True">
                                        </asp:CheckBoxList></div>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Months</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkMonths" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Month(this);"></asp:CheckBox><br />
                                         <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstMonths" runat="server" AutoPostBack="True">
                                        </asp:CheckBoxList></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            Lesson Plan By Grade
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="HT2" runat="server">
                        <ContentTemplate>
                            <table id="Table1" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0" style="border-collapse: collapse;">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear2" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Subject</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkGrade" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Grade(this);"></asp:CheckBox><br />
                                        <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstGrade" runat="server" AutoPostBack="True">
                                        </asp:CheckBoxList></div>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Months</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkMonth2" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Month2(this);"></asp:CheckBox><br />
                                         <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstMonths2" runat="server" AutoPostBack="True">
                                        </asp:CheckBoxList></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport2" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            Lesson Plan By Subject
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="HT3" runat="server">
                        <ContentTemplate>
                            <table id="Table3" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0" style="border-collapse: collapse;">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear3" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Teacher</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTeacher" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Groups</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkGroup" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Group(this);"></asp:CheckBox><br />
                                         <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstGroup" runat="server" AutoPostBack="True">
                                        </asp:CheckBoxList></div>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Months</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkMonth3" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Month3(this);"></asp:CheckBox><br />
                                         <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstMonths3" runat="server" AutoPostBack="True">
                                        </asp:CheckBoxList></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport3" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            Lesson Plan By Teacher
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="HT4" runat="server">
                        <ContentTemplate>
                            <table id="Table4" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear4" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade4" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Subject</span>
                                    </td>

                                    <td align="left" width="30%">

                                        <asp:CheckBox ID="chkSubject4" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Subject4(this);"></asp:CheckBox><br />
                                         <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstSubject4" runat="server">
                                        </asp:CheckBoxList></div>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Months</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:CheckBox ID="chkMonth4" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Month4(this);"></asp:CheckBox><br />
                                         <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstMonths4" runat="server" AutoPostBack="True">
                                        </asp:CheckBoxList></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport4" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            Lesson Completed Status
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>

            </div>

        </div>

    </div>
</asp:Content>
