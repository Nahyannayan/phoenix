Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_ConsolidatedReports_clmConsolidatedSheet_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330110" And _
            ViewState("MainMnu_code") <> "C330220" And ViewState("MainMnu_code") <> "C330290") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                checkEmpFormtutor()

                BindReportCard()
                BindPrintedFor()

                ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)


                Dim grade As String()
                grade = ddlGrade.SelectedValue.Split("|")

                BindSection()


            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub checkEmpFormtutor()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sctId As String = ""
        Dim grdId As String
        Dim stmId As String = ""
        hSCT_ID.Value = ""
        Dim str_query As String = "SELECT ISNULL(SCT_ID,0),ISNULL(SCT_GRD_ID,''),ISNULL(GRM_STM_ID,0) " _
                                 & " FROM SECTION_M INNER JOIN GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE " _
                                 & "  SCT_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                 & " AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            hSCT_ID.Value = reader.GetValue(0).ToString
            hGRD_ID.Value = reader.GetValue(1).ToString
            h_STM_ID.Value = reader.GetValue(2).ToString
        End While


    End Sub


    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        ElseIf hSCT_ID.Value <> "" Then
            str_query = "SELECT DISTINCT RSM_DESCR,RSM_ID,RSM_DISPLAYORDER FROM RPT.REPORT_SETUP_M AS A" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON RSM_ID=RSG_RSM_ID " _
                                & " INNER JOIN OASIS..SECTION_M AS C ON B.RSG_GRD_ID=C.SCT_GRD_ID AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'" _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        End If


        If ViewState("MainMnu_code") = "C330220" Then
            str_query += " AND RSM_bFINALREPORT=1"
            Session("PromotionSheet") = 1
        Else
            Session("PromotionSheet") = 0
        End If
        str_query += " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString _
                                & "' ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"

        If hSCT_ID.Value <> "" And Session("CurrSuperUser") <> "Y" Then
            str_query += " AND GRM_GRD_id='" + hGRD_ID.Value + "' AND GRM_STM_ID=" + h_STM_ID.Value
        End If
        str_query += " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

      Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function


 
    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            If Session("CurrSuperUser") <> "Y" And hSCT_ID.Value <> "" Then
                str_query += "AND SCT_ID=" + hSCT_ID.Value
            End If

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            'ddlSection.Items.Insert(0, li)
        End If
    End Sub

#End Region

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindSection()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindPrintedFor()
        BindSection()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
             ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
           
            If Session("sbsuid") = "125010" Then
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_TWS.aspx?MainMnu_code={0}&datamode={1}" _
                                   & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                   & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                   & "&rpf=" + Encr_decrData.Encrypt(ddlReportCard.SelectedItem.Text) _
                                   & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                                   & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                                   & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                                   & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                                   & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                   & "&term=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text.Replace(" REPORT", "")) _
                                   & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf Session("sbsuid") = "126008" Or Session("sbsuid") = "123004" Or Session("sbsuid") = "125002" Then
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_akns.aspx?MainMnu_code={0}&datamode={1}" _
                                  & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                  & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                  & "&rpf=" + Encr_decrData.Encrypt(ddlReportCard.SelectedItem.Text) _
                                  & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                                  & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                                  & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                                  & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                                  & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                  & "&term=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text.Replace(" REPORT", "")) _
                                  & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf Session("sbsuid") = "125005" Then
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_CIS.aspx?MainMnu_code={0}&datamode={1}" _
                                 & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                 & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                 & "&rpf=" + Encr_decrData.Encrypt(ddlReportCard.SelectedItem.Text) _
                                 & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                                 & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                                 & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                                 & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                                 & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                 & "&term=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text.Replace(" REPORT", "")) _
                                 & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_CBSE.aspx?MainMnu_code={0}&datamode={1}" _
                                 & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                 & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                 & "&rpf=" + Encr_decrData.Encrypt(ddlReportCard.SelectedItem.Text) _
                                 & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                                 & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                                 & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                                 & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                                 & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                 & "&term=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text.Replace(" REPORT", "")) _
                                 & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text), ViewState("MainMnu_code"), ViewState("datamode"))

            End If
            ResponseHelper.Redirect(url, "_blank", "")

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
     
             BindSection()
     
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
