﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Drawing
Partial Class Curriculum_ConsolidatedReports_clmConsolidatedLessonPlanner
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            lblBsu.Text = Session("bsu_name")


            hMonths.Value = Encr_decrData.Decrypt(Request.QueryString("months").Replace(" ", "+"))
            hACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            hType.Value = Request.QueryString("type")
            hLogin.Value = "SCHOOL"

            hYear.Value = Encr_decrData.Decrypt(Request.QueryString("year").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            If hType.Value = "Grade" Or hType.Value = "Status" Then
                hSBG_IDs.Value = Encr_decrData.Decrypt(Request.QueryString("sbgids").Replace(" ", "+"))
                hGrade.Value = Encr_decrData.Decrypt(Request.QueryString("grm").Replace(" ", "+"))
                lblTitle.Text = "Academic Year : " + hYear.Value + " Grade : " + hGrade.Value
            ElseIf hType.Value = "Subject" Then
                hGRD_IDs.Value = Encr_decrData.Decrypt(Request.QueryString("grades").Replace(" ", "+"))
                hSBM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbmid").Replace(" ", "+"))
                hSubject.Value = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                lblTitle.Text = "Academic Year : " + Encr_decrData.Decrypt(Request.QueryString("year").Replace(" ", "+")) + " Subject : " + hSubject.Value
            ElseIf hType.Value = "Teacher" Then
                hSGR_IDs.Value = Encr_decrData.Decrypt(Request.QueryString("sgrids").Replace(" ", "+"))
                hEMP_ID.Value = Encr_decrData.Decrypt(Request.QueryString("empid").Replace(" ", "+"))
                lblTitle.Text = "Academic Year : " + Encr_decrData.Decrypt(Request.QueryString("year").Replace(" ", "+")) + " Teacher : " + Encr_decrData.Decrypt(Request.QueryString("teacher").Replace(" ", "+"))
            End If

            GridBind()

        End If
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetConsolidatedSubjects(ByVal sbg_ids As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT SBG_DESCR,SBG_ID,ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR,SBG_ORDER,SBG_SHORTCODE,'0' AS OPTIONAL FROM SUBJECTS_GRADE_S " _
                    & " WHERE SBG_ID IN(" + sbg_ids + ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count
        Dim j As Integer

        Dim Subjects(,) As String
        ReDim Subjects(i - 1, 4)

        For j = 0 To i - 1
            With ds.Tables(0)
                Subjects(j, 0) = .Rows(j).Item(0).ToString
                Subjects(j, 1) = .Rows(j).Item(1).ToString
                Subjects(j, 2) = .Rows(j).Item(2).ToString
                Subjects(j, 3) = .Rows(j).Item(4).ToString
                Subjects(j, 4) = .Rows(j).Item(5).ToString
            End With
        Next
        Return Subjects
    End Function


    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        If hType.Value = "Grade" Then
            str_query = "EXEC SYL.rptCONSOLIDATEDREPORTBYGRADE " _
                        & "'" + Session("sbsuid") + "'," _
                        & hACD_ID.Value + "," _
                        & "'" + hSBG_IDs.Value + "'," _
                        & "'" + hMonths.Value + "'," _
                        & "'" + hGrade.Value + "'"
        ElseIf hType.Value = "Subject" Then
            str_query = "EXEC [SYL].[rptCONSOLIDATEDREPORTBYSUBJECT] " _
                       & "'" + Session("sbsuid") + "'," _
                       & hACD_ID.Value + "," _
                       & hSBM_ID.Value + "," _
                       & "'" + hGRD_IDs.Value + "'," _
                       & "'" + hMonths.Value + "'"
        ElseIf hType.Value = "Teacher" Then
            str_query = "EXEC [SYL].[rptCONSOLIDATEDREPORTBYTEACHER] " _
                       & "'" + Session("sbsuid") + "'," _
                       & hACD_ID.Value + "," _
                       & "'" + hSGR_IDs.Value + "'," _
                       & "'" + hMonths.Value + "'," _
                       & hEMP_ID.Value
        ElseIf hType.Value = "Status" Then
            str_query = "EXEC [SYL].[rptCONSOLIDATEDLESSONCOMPLETESTATUS] " _
                       & "'" + Session("sbsuid") + "'," _
                       & hACD_ID.Value + "," _
                       & "'" + hSBG_IDs.Value + "'," _
                       & "'" + hMonths.Value + "'," _
                       & "'" + hGrade.Value + "'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubj.DataSource = ds
        gvSubj.DataBind()
    End Sub

    Protected Sub gvSubj_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubj.RowDataBound
        e.Row.Cells(1).Visible = False
        Dim i As Integer = e.Row.Cells.Count
        Dim j As Integer
        Dim k As Integer
        Dim strLink As String
        Dim target As String
        Dim strDocs As String
        For j = 3 To i - 1
            e.Row.Cells(j).Width = 100
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Dim tb As New TextBox
                ' tb.TextMode = TextBoxMode.MultiLine
                ' tb.Text = e.Row.Cells(j).Text.Replace("|", "<br><br>")
                strLink = ""
                If hLogin.Value = "SCHOOL" Then
                    If hType.Value = "Grade" And j = 3 Then  'Events
                        e.Row.Cells(j).Text = e.Row.Cells(j).Text.Replace("|", "<br><br>")
                    Else
                        Dim strTopicMain As String() = e.Row.Cells(j).Text.Split("|")
                        For k = 0 To strTopicMain.Length - 1
                            Dim strTopic As String()
                            If hType.Value = "Status" Then
                                strTopic = strTopicMain(k).Split("@@")
                            Else
                                strTopic = strTopicMain(k).Split("__")
                            End If

                            If strTopic.Length > 1 Then
                                If strLink <> "" Then
                                    If hType.Value = "Status" Then
                                        strLink += "<br>"
                                    Else
                                        strLink += "<br><br>"
                                    End If
                                End If
                                If hType.Value = "Grade" Then

                                    target = "clmLessonPlanerDetails.aspx?type=grade&year=" + hYear.Value + "&grade=" + hGrade.Value + "&subject=" + strTopic(4) + "&syd_id=" + strTopic(2)
                                    strLink += "<a href=""" + target + """  onclick = ""window.open('" + target + "', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=800,height = 400 '); return false;"" target=""_blank"">" + strTopic(0) + "</a>"
                                    If strTopic(6) = "yes" Then
                                        strDocs = "clmLessonPlannerDocuments.aspx?topicid=" + strTopic(2) + "&acy=" + hYear.Value + "&grade=" + hGrade.Value + "&subject=" + strTopic(4) + "&topic=" + strTopic(0)
                                        strLink += "<img src='../../Images/documents.png' style='cursor:pointer' onclick = ""window.open('" + strDocs + "', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=600,height = 400 '); return false;"" />"
                                    End If
                                    'strLink += "<a href=""clmLessonPlanerDetails.aspx?type=grade&year=" + hYear.Value + "&grade=" + hGrade.Value + "&subject=" + strTopic(4) + "&syd_id=" + strTopic(2) + """ target=""_blank"">" + strTopic(0) + "</a>"
                                ElseIf hType.Value = "Subject" Then

                                    target = "clmLessonPlanerDetails.aspx?type=subject&year=" + hYear.Value + "&grade=" + hGrade.Value + "&subject=" + hSubject.Value + "&syd_id=" + strTopic(2)
                                    strLink += "<a href=""" + target + """  onclick = ""window.open('" + target + "', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=800,height = 400 '); return false;"" target=""_blank"">" + strTopic(0) + "</a>"

                                    If strTopic(4) = "yes" Then
                                        strDocs = "clmLessonPlannerDocuments.aspx?topicid=" + strTopic(2) + "&acy=" + hYear.Value + "&grade=" + hGrade.Value + "&subject=" + strTopic(4) + "&topic=" + strTopic(0)
                                        strLink += "<img src='../../Images/documents.png' style='cursor:pointer' onclick = ""window.open('" + strDocs + "', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=600,height = 400 '); return false;"" />"
                                    End If
                                    'strLink += "<a href=""clmLessonPlanerDetails.aspx?type=subject&year=" + hYear.Value + "&grade=" + hGrade.Value + "&subject=" + hSubject.Value + "&syd_id=" + strTopic(2) + """ target=""_blank"">" + strTopic(0) + "</a>"
                                ElseIf hType.Value = "Teacher" Then
                                    target = "clmLessonPlanerDetails.aspx?type=teacher&year=" + hYear.Value + "&grade=" + hGrade.Value + "&subject=" + hSubject.Value + "&syd_id=" + strTopic(2) + "&sbg_id=" + strTopic(4) + "&sgr_id=" + strTopic(6)
                                    strLink += "<a href=""" + target + """  onclick = ""window.open('" + target + "', 'myWin', 'toolbar=no, directories=no, location=no, status=yes, menubar=no, resizable=no, scrollbars=yes, width=800,height = 400 '); return false;"" target=""_blank"">" + strTopic(0) + "</a>"
                                ElseIf hType.Value = "Status" Then
                                    If strTopic(2) = "RED" Then
                                        strLink += "<font color=""red"">" + strTopic(0) + "</font>"
                                    ElseIf strTopic(2) = "GREEN" Then
                                        strLink += "<font color=""green"">" + strTopic(0) + "</font>"
                                    End If
                                End If
                            End If
                        Next
                        e.Row.Cells(j).Text = strLink
                    End If
                Else
                    e.Row.Cells(j).Text = e.Row.Cells(j).Text.Replace("|", "<br><br>")
                End If

                    ' e.Row.Cells(j).Controls.Clear()
                    'e.Row.Cells(j).Controls.Add(tb)

                    'If hType.Value = "Teacher" Then
                    ' e.Row.Cells(j).Text = "<a href=""test"">" + e.Row.Cells(j).Text.Replace("|", "</a><br><br><a href=""test"">") + "</a>"
                    'Else
                    '    e.Row.Cells(j).Text = e.Row.Cells(j).Text.Replace("|", "<br><br>")
                    'End If
                End If
        Next
    End Sub
End Class
