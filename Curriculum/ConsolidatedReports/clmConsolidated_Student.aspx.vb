Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_Student
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
            'hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))

            If Session("PromotionSheet") = 1 Then
                hfPromotionSheet.Value = 1
            Else
                hfPromotionSheet.Value = 0
            End If

            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)

            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            hfAccYear.Value = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
           
            hfTC.Value = Encr_decrData.Decrypt(Request.QueryString("includetc").Replace(" ", "+"))

          
            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights
            hfBSU_ID.Value = Session("sbsuid")
            GetACDCurrent()
            hfRecordNumber.Value = 0
            hfTableColumns.Value = 0
            hfPageNumber.Value = 0
            GetConsolidated(grd_id(0), Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+")), grd_id(1))


            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
            'End If
        End If
    End Sub
#Region "Private Methods"

    Sub GetACDCurrent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(ACD_CURRENT,'false') FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value
        hfACD_Current.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub checkReportFinal()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_bFINALREPORT,'FALSE') FROM RPT.REPORT_SETUP_M WHERE RSM_ID=" + hfRSM_ID.Value
        Dim str As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If str.ToLower = "true" Then
            hfbFinal.Value = 1
        Else
            hfbFinal.Value = 0
        End If
    End Sub



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetConsolidated(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim Subjects As String(,) = GetConsolidatedSubjects(grd_id, hfACD_ID.Value, stm_id)
        '    Dim rptHeaders As String(,) = GetReportHeaders(hfRSM_ID.Value)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        If hfPromotionSheet.Value = 1 Then
            sb.AppendLine("<HTML><HEAD><TITLE>::::PROMOTION SHEET ::::</TITLE>")
        Else
            sb.AppendLine("<HTML><HEAD><TITLE>:::: CONSOLIDATED REPORT ::::</TITLE>")
        End If

        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<BODY>")
        sb.AppendLine("</style>")
   
        sb.AppendLine(GetConsolidatedForSection(grd_id, sct_id, hfACD_ID.Value, Subjects))

        sb.AppendLine("</BODY></HTML>")
        '  Response.Write(sb.ToString())
        Dim htmlString As String = sb.ToString
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + hfGRD_ID.Value + ViewState("sctdesc") + ".xls")
        Response.ContentType = "application/vnd.ms-excel"
        Response.Write(htmlString)
        Response.End()

    End Sub

    Function GetConsolidatedSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT SBG_SHORTCODE,SBG_ID FROM SUBJECTS_GRADE_S " _
                    & " WHERE SBG_GRD_ID='" + grd_id + "' AND SBG_ACD_ID=" + acd_id _
                    & " AND SBG_STM_ID=" + stm_id _
                    & " AND SBG_bOPTIONAL=1 ORDER BY SBG_SHORTCODE"

     
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count
        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 2)

        For j = 0 To i - 1
            With ds.Tables(0)
                Subjects(j, 0) = .Rows(j).Item(0).ToString
                Subjects(j, 1) = .Rows(j).Item(1).ToString
            End With
        Next
        Return Subjects
    End Function

      Function GetSubjectsHeader(ByVal Subjects(,) As String) As String
        Dim sb As New StringBuilder
        Dim i As Integer

        Dim strSubjects As String = ""
        Dim strHeader As String = ""
        Dim strShort1 As String = ""
        Dim strShort As String = ""
        Dim imgUrl As String = ""

        Dim j As Integer

        Dim strHeader1 As String
        Dim strHeader2 As String

        hfTableColumns.Value = 0
        For i = 0 To Subjects.GetLength(0) - 1
            strSubjects += "<td width=""1%"" align=""middle"" Class=repcolDetail>" + Subjects(i, 0) + "</td>"

        Next



        sb.AppendLine("<table border=""1"" width=""1400px"" bordercolorlight=""#000000""  align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font:NORMAL NORMAL 6pt Verdana, arial, Helvetica, sans-serif"" >")

        sb.AppendLine("<tr >")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail ><center> Sr. No  </td>")
        sb.AppendLine("<td width=""50"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail ><center> Admin. No  </td>")
        sb.AppendLine("<td  width=""20%"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail ><center>Name of the Student</td>")
        sb.AppendLine("<td  width=""2%"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail ><center>Previous Class</td>")
        sb.AppendLine("<td  width=""5%"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail ><center>House</td>")
        sb.AppendLine("<td  width=""5%"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail ><center>Date of Birth</td>")
        sb.AppendLine("<td  width=""5%"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail ><center>Nationality</td>")
        If Subjects.GetLength(0) <> 0 Then
            sb.AppendLine("<td  width=""20%"" ALIGN=""middle"" colspan=" + Subjects.GetLength(0).ToString + " valign=""middle"" Class=repcolDetail ><center>Subjects Selected</td>")
        End If
        sb.AppendLine("<td  width=""20%"" ALIGN=""middle"" colspan=2 valign=""middle"" Class=repcolDetail ><center>Transport</td>")
        sb.AppendLine("<td  width=""15%"" ALIGN=""middle"" valign=""middle"" colspan=2 Class=repcolDetail ><center>Telephone</td>")
        sb.AppendLine("<td  width=""20%"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("<td  width=""20%"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("<td  width=""20%"" ALIGN=""middle"" valign=""middle""  Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("</tr>")


        sb.AppendLine("<tr>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >&nbsp;</td>")
        sb.AppendLine(strSubjects)
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >BUS NO.</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >Pickup Point</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >Residence</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >Mobile</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >Primary Contact Name</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >Primary Contact Email ID</td>")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" Class=repcolDetail >Remarks</td>")
        sb.AppendLine("</tr>")


        hfTableColumns.Value = 12 + Subjects.GetLength(0)


        Return sb.ToString
    End Function
    Public Function checkUpper(ByVal str As String) As Boolean
        If Strings.StrComp(str, Strings.UCase(str)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetConsolidatedForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,))
        Dim sb As New StringBuilder
        sb.AppendLine("<table   align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")



        sb.AppendLine(GetSubjectsHeader(Subjects))
        sb.AppendLine(GetStudentOptions(sct_id, Subjects))
        sb.AppendLine("</table>")

        'sb.AppendLine("</td></tr>")
        'sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetConsolidatedHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_BB_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table   align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=bottom>")


            sb.AppendLine("<table width=""90%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">")
            sb.AppendLine("<TR >")
            sb.AppendLine("<TD>GRADE :    " + reader("GRM_DISPLAY") + "   &nbsp;&nbsp; SECTION : " + reader("SCT_DESCR") + " </TD> </TR>")

            sb.AppendLine("</Table>")

            sb.AppendLine("</TD>")


            sb.AppendLine("<TD WIDTH=40% VALIGN=TOP>")

            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font style=""text-decoration:NONE;font:bold bold 14pt Times New Roman,Verdana, arial, Helvetica, sans-serif"">" + reader("BSU_NAME"))
            If hfPromotionSheet.Value = 1 Then
                sb.AppendLine("<BR>PROMOTION SHEET  " + reader("ACY_DESCR") + "</TD></TR>")

            Else
                sb.AppendLine("<BR>" + hfTERM.Value.ToUpper + " CONSOLIDATED SHEET  " + reader("ACY_DESCR") + "</TD></TR>")

            End If

            sb.AppendLine("</table>")

            sb.AppendLine("</TD>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=TOP ALIGN=""RIGHT"">")
            sb.AppendLine("<table width=""80%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("</table>")
            sb.AppendLine("</TD>")

            sb.AppendLine("</TR>")

            sb.AppendLine("<TR><TD colspan=2><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">&nbsp;&nbsp;&nbsp;&nbsp; Class Teacher :  " + reader("EMP_NAME") + " </TD>")
            sb.AppendLine("<TD align=right><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> Date : " + Now.Date + " </TD>")
            sb.AppendLine("</TR>")
            sb.AppendLine("</Table><BR>")

            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function

    'Sub BindMinorSubjectMarks(ByVal Subjects(,) As String)
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String
    '    If hfACD_ID.Value.ToLower = "true" Then
    '        If hfbFinal.Value = 0 Then
    '            str_query=
    '        End If
    '    End If
    'End Sub


    Function GetStudentOptions(ByVal sct_id As String, ByVal Subjects(,) As String)
        Dim strTable As String


        Dim strStudents As String = GetStudents(sct_id, Subjects)


        strTable = strStudents

        Dim id As String = ""
            Dim i As Integer

        Dim strStuId As String = ""
     
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString



        Dim str_query As String = "SELECT SBG_ID,SSD_STU_ID,SBG_SHORTCODE FROM SUBJECTS_GRADE_S AS A " _
                             & " INNER JOIN STUDENT_GROUPS_S AS B ON SBG_ID=SSD_SBG_ID" _
                             & " INNER JOIN STUDENT_M ON SSD_STU_ID=STU_ID" _
                             & " WHERE STU_SCT_ID=" + sct_id _
                             & " AND SBG_bOPTIONAL=1"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)



        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id = "*" + .Item("SSD_STU_ID").ToString + "_" + .Item("SBG_ID").ToString + "*"
                strTable = strTable.Replace(id, .Item("SBG_SHORTCODE"))
            End With
        Next
        strTable = ClearEmptyOptions(sct_id, Subjects, strTable)
        Return strTable
    End Function
    Function ClearEmptyOptions(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal strTable As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_ID FROM STUDENT_M WHERE STU_SCT_ID=" + sct_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        Dim j As Integer
        Dim id As String
        For i = 0 To ds.Tables(0).Rows.Count - 1
            For j = 0 To Subjects.GetLength(0) - 1
                id = "*" + ds.Tables(0).Rows(i).Item("STU_ID").ToString + "_" + Subjects(j, 1).ToString + "*"
                strTable = strTable.Replace(id, "&nbsp;")
            Next
        Next
        Return strTable
    End Function


   

    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
         Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()
        Dim strMain As String
        Dim strSub As String

        Dim strRedLine As String

        Dim str_query As String


        str_query = "SELECT A.STU_ID STU_ID,A.STU_FEE_ID AS FEEID,ISNULL(A.STU_PASPRTNAME,ISNULL(A.STU_FIRSTNAME,'')+' '+ISNULL(A.STU_MIDNAME,'')+' '+ISNULL(A.STU_LASTNAME,'')) AS STU_NAME," _
                 & " ISNULL(GRM_DISPLAY,'')+' '+ISNULL(SCT_DESCR,'') AS PREVCLASS, " _
                 & " CASE WHEN ISNULL(G.BNO_DESCR,'')=ISNULL(H.BNO_DESCR,'') THEN ISNULL(G.BNO_DESCR,'') ELSE ISNULL(G.BNO_DESCR,'')+'/'+ISNULL(H.BNO_DESCR,'') END BNO_DESCR," _
                 & " CASE WHEN F.STU_SBL_ID_PICKUP IS NULL THEN 'OT' ELSE ISNULL(SBL_DESCRIPTION,'')+'-'+ ISNULL(PNT_DESCRIPTION,'') END PNT_DESCRIPTION," _
                 & " CASE A.STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+" _
                 & " ISNULL(STS_FLASTNAME,'') WHEN 'M' THEN  ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+" _
                 & " ISNULL(STS_MLASTNAME,'')ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+" _
                 & " ISNULL(STS_GLASTNAME,'') END AS PARENTNAME,CASE A.STU_PRIMARYCONTACT WHEN 'F' THEN  ISNULL(STS_FRESPHONE,'')" _
                 & " WHEN 'M' THEN  ISNULL(STS_MRESPHONE,'') ELSE  ISNULL(STS_GRESPHONE,'') END AS RESPHONE," _
                 & " CASE A.STU_PRIMARYCONTACT WHEN 'F' THEN  ISNULL(STS_FMOBILE,'')" _
                 & " WHEN 'M' THEN  ISNULL(STS_MMOBILE,'') ELSE  ISNULL(STS_GMOBILE,'') END AS MOBILE," _
                 & " CASE A.STU_PRIMARYCONTACT WHEN 'F' THEN  ISNULL(STS_FEMAIL,'')" _
                 & " WHEN 'M' THEN  ISNULL(STS_MEMAIL,'') ELSE  ISNULL(STS_GEMAIL,'') END AS EMAIL,ISNULL(HOUSE_DESCRIPTION,'')  HOUSE," _
                 & " A.STU_DOB STU_DOB,CTY_NATIONALITY" _
                 & " FROM STUDENT_M AS A INNER JOIN STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID" _
                 & " LEFT OUTER JOIN STUDENT_PROMO_S AS C ON A.STU_ID=C.STP_STU_ID AND STP_ACD_ID=" + Session("PREV_ACD_ID") _
                 & " LEFT OUTER JOIN GRADE_BSU_M AS D ON C.STP_GRM_ID=D.GRM_ID " _
                 & " LEFT OUTER JOIN SECTION_M AS E ON C.STP_SCT_ID=E.SCT_ID" _
                 & " INNER JOIN OASIS_TRANSPORT..STUDENT_M AS F ON A.STU_ID=F.STU_ID " _
                 & " LEFT OUTER JOIN OASIS_TRANSPORT.TRANSPORT.VV_BUSES AS G ON F.STU_PICKUP_TRP_ID=G.TRP_ID " _
                 & " LEFT OUTER JOIN OASIS_TRANSPORT.TRANSPORT.VV_BUSES AS H ON F.STU_DROPOFF_TRP_ID=H.TRP_ID " _
                 & " LEFT OUTER JOIN TRANSPORT.VV_PICKUPOINTS_M AS J ON F.STU_PICKUP=J.PNT_ID " _
                 & " LEFT OUTER JOIN OASIS_TRANSPORT.TRANSPORT.SUBLOCATION_M AS M ON F.STU_SBL_ID_PICKUP=M.SBL_ID" _
                 & " LEFT OUTER JOIN HOUSE_M AS K ON A.STU_HOUSE_ID=K.HOUSE_ID " _
                 & " INNER JOIN COUNTRY_M AS L ON A.STU_NATIONALITY=L.CTY_ID " _
                 & " WHERE A.STU_SCT_ID = " + sct_id

        If hfTC.Value = "true" Then
            str_query += " AND A.STU_CURRSTATUS IN('EN','TC')"
        Else
            str_query += " AND A.STU_CURRSTATUS='EN'"
        End If
        str_query += " ORDER BY A.STU_PASPRTNAME,A.STU_FIRSTNAME,A.STU_MIDNAME,A.STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count


        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If



        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)

                stuNames = .Item(0).ToString.Replace("  ", " ").Split(" ")
                sb.AppendLine("<tr height=""16px"">")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td align=""left"" Class=repcolDetail><CENTER>" + .Item("FEEID") + "</td>")
          
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("PREVCLASS").ToString.ToUpper + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("HOUSE").ToString.ToUpper + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + Format(.Item("STU_DOB"), "dd/MMM/yyyy") + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("CTY_NATIONALITY").ToString.ToUpper + "</td>")
                For j = 0 To Subjects.GetLength(0) - 1
                    sb.AppendLine("<td  align=""CENTER"" ><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_" + Subjects(j, 1) + "*</font></td>")
                Next

                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("BNO_DESCR").ToString.ToUpper + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("PNT_DESCRIPTION").ToString.ToUpper + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("RESPHONE").ToString.ToUpper + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("MOBILE").ToString.ToUpper + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("PARENTNAME").ToString.ToUpper + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail>" + .Item("EMAIL").ToString.ToLower + "</td>")
                sb.AppendLine("<td  align=""left"" Class=repcolDetail></td>")
                sb.AppendLine("</tr>")


            End With
        Next





        Return sb.ToString
    End Function


   









#End Region


End Class
