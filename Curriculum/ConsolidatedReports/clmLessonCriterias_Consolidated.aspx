<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmLessonCriterias_Consolidated.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmLessonCriterias_Consolidated" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">

a {display: inline-block; max-width: 100px; vertical-align: middle;}

pre {
	white-space: pre;           /* CSS 2.0 */
	white-space: pre-wrap;      /* CSS 2.1 */
	white-space: pre-line;      /* CSS 3.0 */
	white-space: -pre-wrap;     /* Opera 4-6 */
	white-space: -o-pre-wrap;   /* Opera 7 */
	white-space: -moz-pre-wrap; /* Mozilla */
	white-space: -hp-pre-wrap;  /* HP Printers */
	word-wrap: break-word;      /* IE 5+ */
	width:100px;
	}
	
	
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblText" runat="server"  Font-Bold="true" Font-Size="Large"></asp:Label>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <asp:HiddenField ID="h_SYD_ID" runat="server" />
        <asp:HiddenField ID="h_SBG_ID" runat="server" />
    </div>
    </form>
</body>
</html>
