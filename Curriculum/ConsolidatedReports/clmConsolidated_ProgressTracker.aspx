﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmConsolidated_ProgressTracker.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_ProgressTracker" %>

<!DOCTYPE html>

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> 
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
            <div><asp:Label ID="lblText" runat="Server" ></asp:Label></div>

    <div>
      <asp:GridView ID="gvSubj" runat="server" CssClass="table table-bordered table-row" >
        <RowStyle HorizontalAlign="Center" />
        <AlternatingRowStyle />
         </asp:GridView>

         <asp:HiddenField ID="hfRSD_IDS" runat="server" /><asp:HiddenField ID="hfRecordNumber" runat="server" Visible="False" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfGRM_DISPLAY" runat="server" />
        <asp:HiddenField ID="hfSCT_ID" runat="server" />
        <asp:HiddenField ID="hfSTM_ID" runat="server" />
        <asp:HiddenField ID="hfRSM_ID" runat="server" />
        <asp:HiddenField ID="hfRPF_ID" runat="server" />
         <asp:HiddenField ID="hfRPF_PROGRESS" runat="server" />
        <asp:HiddenField ID="hfbTC" runat="server" /><asp:HiddenField ID="hfRPF_DESCR" runat="server" />
        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfSBG_IDS" runat="server" />
        <asp:HiddenField ID="hfbFinal" runat="server" />
         <asp:HiddenField ID="hfType" runat="server" />
          <asp:HiddenField ID="hbCriteria" runat="server" /><asp:HiddenField ID="hfDisplayGrade" runat="server" />
    </div>
    </form>
</body>
</html>
