Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_ConsolidatedReports_clmConsolidatedExcel_Withpivot
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hfRSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
            hfRPF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid").Replace(" ", "+"))
            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            hfGRM_DISPLAY.Value = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
            hfSBG_IDS.Value = Request.QueryString("sbg_ids")
            hfRSD_IDS.Value = Request.QueryString("rsd_ids")
            hfType.Value = Encr_decrData.Decrypt(Request.QueryString("type").Replace(" ", "+"))
            hfSCT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+"))
            hbCriteria.Value = Encr_decrData.Decrypt(Request.QueryString("bCriteria").Replace(" ", "+"))
            lblText.Text = Encr_decrData.Decrypt(Request.QueryString("strtext").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+")).Split("|")
            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)
            checkReportFinal()
            GridBind()
        End If
    End Sub
    Sub checkReportFinal()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_bFINALREPORT,'FALSE') FROM RPT.REPORT_SETUP_M WHERE RSM_ID=" + hfRSM_ID.Value
        Dim str As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If str.ToLower = "true" Then
            hfbFinal.Value = 1
        Else
            hfbFinal.Value = 0
        End If
    End Sub
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC [CBSE].[GETCONSOLIDATEDMARKS_EXCEL_PIVOT] " _
                              & hfACD_ID.Value + "," _
                              & "'" + hfGRD_ID.Value + "'," _
                              & hfRSM_ID.Value + "," _
                              & hfRPF_ID.Value + "," _
                              & hfSCT_ID.Value + "," _
                              & hfbFinal.Value + "," _
                              & "'" + hfSBG_IDS.Value + "'," _
                              & "'" + hfRSD_IDS.Value + "'," _
                              & hbCriteria.Value + "," _
                              & "'" + hfType.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            gvSubj.DataSource = ds
            gvSubj.DataBind()
        End If

        'Dim sw As New StringWriter
        'Dim hw As New HtmlTextWriter(sw)
        'gvSubj.RenderControl(hw)

        'Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", "StudentData" + Now.ToString.Replace("/", "") + ".xls"))
        'Response.ContentType = "application/ms-excel"
        'Response.Write(sw.ToString)

        'Response.End()
    End Sub

    Protected Sub gvSubj_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubj.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then




            Dim i, j As Integer

            Dim HeaderCell1 As New TableCell()
            Dim HeaderCell2 As New TableCell()
            Dim HeaderCell3 As New TableCell()

            Dim HeaderGridRow1 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow2 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow3 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            

            Dim Subject As String
            Dim Header As String
            Dim hType As String

            Dim tSubject As String = ""
            Dim tHeader As String = ""
          
            Dim scolspan As Integer = 1
            Dim hcolspan As Integer = 1
          
            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Student ID"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Name"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Section"
            HeaderGridRow1.Cells.Add(HeaderCell1)

           
            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell1.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)



            For i = 3 To e.Row.Cells.Count - 1
                Dim shdr As String() = e.Row.Cells(i).Text.Split("__")

                Subject = shdr(0)
                If Subject <> tSubject And tSubject <> "" Then
                    tHeader = "-"
                End If
                Header = shdr(2)
                If Header <> tHeader Then
                    If tHeader <> "" Then
                        HeaderCell2.ColumnSpan = hcolspan
                        HeaderGridRow2.Cells.Add(HeaderCell2)

                        'HeaderCell1.ColumnSpan = scolspan
                        'HeaderGridRow1.Cells.Add(HeaderCell1)
                        'HeaderCell1 = New TableCell()
                    End If

                    HeaderCell2 = New TableCell()
                    HeaderCell2.Text = Header
                    'scolspan = 1
                    hcolspan = 1
                    tHeader = Header
                    '  tSubject = ""
                Else
                    hcolspan += 1
                End If

                If Subject <> tSubject Then
                    If tSubject <> "" Then
                        HeaderCell1.ColumnSpan = scolspan
                        HeaderGridRow1.Cells.Add(HeaderCell1)
                    End If

                    HeaderCell1 = New TableCell()
                    HeaderCell1.Text = Subject
                    scolspan = 1
                    tSubject = Subject

                Else
                    scolspan += 1
                End If
                e.Row.Cells(i).Text = shdr(4)
            Next
            HeaderCell2 = New TableCell()
            HeaderCell2.Text = Header
            HeaderCell2.ColumnSpan = hcolspan
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = Subject
            HeaderCell1.ColumnSpan = scolspan
            HeaderGridRow1.Cells.Add(HeaderCell1)

            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow2)

            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow1)
           

        End If
    End Sub
End Class
