﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_ProgressTracker
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hfRPF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rpf_ids").Replace(" ", "+"))
            hfRPF_PROGRESS.Value = Encr_decrData.Decrypt(Request.QueryString("rpf_progress").Replace(" ", "+"))
            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            hfGRM_DISPLAY.Value = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
            hfSBG_IDS.Value = Encr_decrData.Decrypt(Request.QueryString("sbg_ids").Replace(" ", "+"))
            hfRSD_IDS.Value = Encr_decrData.Decrypt(Request.QueryString("rsd_ids").Replace(" ", "+"))
            hfType.Value = Encr_decrData.Decrypt(Request.QueryString("type").Replace(" ", "+"))
            hfSCT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+"))
            hfbTC.Value = Encr_decrData.Decrypt(Request.QueryString("bTC").Replace(" ", "+"))
            lblText.Text = Encr_decrData.Decrypt(Request.QueryString("strtext").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+")).Split("|")
            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)
            '  checkReportFinal()
            GridBind()
        End If
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec [RPT].[CONSOLIDATEDPROGRESSTRACKER] " _
                                  & "@ACD_ID=" + hfACD_ID.Value + "," _
                                  & "@GRD_ID='" + hfGRD_ID.Value + "'," _
                                  & "@RPF_IDS='" + hfRPF_ID.Value + "'," _
                                  & "@RSD_IDS='" + hfRSD_IDS.Value + "'," _
                                  & "@SBG_IDS='" + hfSBG_IDS.Value + "'," _
                                  & "@PROGRESS_RPFIDS='" + hfRPF_PROGRESS.Value + "'," _
                                  & "@SCT_ID=" + hfSCT_ID.Value + "," _
                                  & "@STU_TYPE='" + hfType.Value + "'," _
                                  & "@bINCLUDETC='" + hfbTC.Value + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            gvSubj.DataSource = ds
            gvSubj.DataBind()

            gvSubj.HeaderRow.Visible = False
        End If

    End Sub

    Protected Sub gvSubj_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubj.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then

            
            Dim HeaderCell1 As New TableCell()
            Dim HeaderCell2 As New TableCell()
            Dim HeaderCell3 As New TableCell()
          
            Dim HeaderGridRow1 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow2 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
          
            Dim Subject As String
            Dim Report As String
            Dim Header As String
            Dim hType As String

            Dim tSubject As String = ""
            Dim tReport As String = ""
            Dim tHeader As String = ""

            Dim scolspan As Integer = 1
            Dim rcolspan As Integer = 1
            Dim hcolspan As Integer = 1

            Dim bReduceCellheight As Boolean = False

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Student ID"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Name"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Section"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "DOB"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "DOJ"
            HeaderGridRow1.Cells.Add(HeaderCell1)


            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Age"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Religion"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Nationality"
            HeaderGridRow1.Cells.Add(HeaderCell1)




            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            For i = 8 To e.Row.Cells.Count - 1
                Dim shdr As String() = e.Row.Cells(i).Text.Split("__")

                If shdr.Length >= 2 Then
                    Subject = shdr(0)
                    If Subject <> tSubject And tSubject <> "" Then
                        tHeader = "-"
                        tReport = "-"
                    End If
                    If Report <> tReport And tReport <> "" Then
                        tHeader = "-"
                    End If
                    Report = shdr(2)
                    If Report <> tReport Then
                        If tReport <> "" Then
                            HeaderCell2.ColumnSpan = rcolspan
                            HeaderGridRow2.Cells.Add(HeaderCell2)
                        End If
                        HeaderCell2 = New TableCell()
                        HeaderCell2.Text = Report
                        rcolspan = 1
                        tReport = Report
                    Else
                        rcolspan += 1
                    End If

                    If Subject <> tSubject Then
                        If tSubject <> "" Then
                            HeaderCell1.ColumnSpan = scolspan
                            HeaderGridRow1.Cells.Add(HeaderCell1)
                        End If

                        HeaderCell1 = New TableCell()
                        HeaderCell1.Text = Subject
                        scolspan = 1
                        tSubject = Subject

                    Else
                        scolspan += 1
                    End If
                End If
            Next

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = Report
            HeaderCell2.ColumnSpan = rcolspan
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = Subject
            HeaderCell1.ColumnSpan = scolspan
            HeaderGridRow1.Cells.Add(HeaderCell1)

            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow2)

            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow1)
            e.Row.Cells(0).Text = ""
            e.Row.Cells(1).Text = ""
            e.Row.Cells(2).Text = ""
        End If
    End Sub


    Protected Sub gvSubj_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSubj.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim i As Integer

            For i = 1 To e.Row.Cells.Count - 1
                If i >= 8 Then
                    Dim shdr As String() = gvSubj.HeaderRow.Cells(i).Text.Split("__")
                    If shdr(4) = "PROGRESS" Or shdr(4) = "PROGRESS" Or shdr(4) = "CUMULATIVE" Then
                        Select Case e.Row.Cells(i).Text
                            Case "1"
                                e.Row.Cells(i).BackColor = Drawing.Color.LightGoldenrodYellow
                            Case "2"
                                e.Row.Cells(i).BackColor = Drawing.Color.LightGreen
                            Case "3", "4", "5", "6", "7", "8", "9", "10"
                                e.Row.Cells(i).BackColor = Drawing.Color.LightSeaGreen
                            Case "0", "-1", "-2", "-3"
                                e.Row.Cells(i).BackColor = Drawing.Color.LightSalmon
                        End Select

                    End If
                ElseIf i = 1 Then
                    e.Row.Cells(i).HorizontalAlign = HorizontalAlign.Left
                End If
            Next

        End If
    End Sub
End Class
