﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmObjTrackStudentCurrentLevel.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmObjTrackStudentCurrentLevel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
      
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="lblBsu" runat="server"
                        Text="BSU"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" class="title-bg">
                    <asp:Label ID="Label1" runat="server"
                        Text="Consolidated Student Level Tracker"></asp:Label></td>
            </tr>

            <tr>
                <td align="center">
                    <asp:Label ID="lblTitle" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvSubj" runat="server" HorizontalAlign="Center" CssClass="table table-bordered table-row">
                        <RowStyle HorizontalAlign="Center" />
                        <AlternatingRowStyle />
                    </asp:GridView>
                    <asp:HiddenField ID="hfGRD_ID" runat="server" />
                    <asp:HiddenField ID="hfSCT_ID" runat="server" />
                    <asp:HiddenField ID="hfSBM_ID" runat="server" />
                    <asp:HiddenField ID="hfACD_ID" runat="server" />
                    <asp:HiddenField ID="hfShow" runat="server" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
