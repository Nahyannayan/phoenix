Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_TWS
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
            'hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))

            If Session("PromotionSheet") = 1 Then
                hfPromotionSheet.Value = 1
            Else
                hfPromotionSheet.Value = 0
            End If

            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)

            hfRSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
            hfRPF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid").Replace(" ", "+"))
            hfSchedule.Value = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))
            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            hfAccYear.Value = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
            hfTERM.Value = Encr_decrData.Decrypt(Request.QueryString("term").Replace(" ", "+"))


            hfRPF_DESCR.Value = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))

            Dim hP As New Hashtable
            ViewState("hP") = hP

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330110") Then
            'If Not Request.UrlReferrer Is Nothing Then
            '    Response.Redirect(Request.UrlReferrer.ToString())
            'Else

            '    Response.Redirect("~\noAccess.aspx")
            'End If

            ' Else
            'calling pageright class to get the access rights


            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights
            hfBSU_ID.Value = Session("sbsuid")


            hfRecordNumber.Value = 0
            hfTableColumns.Value = 0
            hfPageNumber.Value = 0
            GetConsolidated(grd_id(0), Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+")), grd_id(1))


            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
            'End If
        End If
    End Sub
#Region "Private Methods"

    Sub SetHowParent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE') FROM " _
                               & " FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfShowParent1.Value = ds.Tables(0).Rows(0).Item(0)
        hfShowParent2.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetConsolidated(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim Subjects As String(,) = GetConsolidatedSubjects(grd_id, hfACD_ID.Value, stm_id)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        If hfPromotionSheet.Value = 1 Then
            sb.AppendLine("<HTML><HEAD><TITLE>::::PROMOTION SHEET ::::</TITLE>")
        Else
            sb.AppendLine("<HTML><HEAD><TITLE>:::: CONSOLIDATED REPORT ::::</TITLE>")
        End If

        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<Link REL=STYLESHEET HREF=""..\cssfiles\brownbook.css"" TYPE=""text/css"">")
        sb.AppendLine("<BODY>")
        sb.AppendLine("<Script language=""JavaScript"" src=""../include/com_client_func.js""></Script>")
        sb.AppendLine("<style>	body,table,td{background-color:#ffffff!important;}")
        sb.AppendLine("th{background-color:#ffffff!important;}")
        sb.AppendLine("</style>")
        If sct_id = "" Or sct_id = "0" Then
            str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                      & "' AND SCT_ACD_ID=" + hfACD_ID.Value
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetConsolidatedForSection(grd_id, .Rows(i).Item(0).ToString, hfACD_ID.Value, Subjects))
                Next
            End With
            'ltBrownBook.Text = sb.ToString
        Else
            sb.AppendLine(GetConsolidatedForSection(grd_id, sct_id, hfACD_ID.Value, Subjects))

            'ltBrownBook.Text = sb.ToString
        End If
        sb.AppendLine("</BODY></HTML>")
        '  Response.Write(sb.ToString())
        Dim htmlString As String = sb.ToString
        Dim htmlwords As String() = htmlString.Split("*")
        Dim k As Integer
        For k = 0 To htmlwords.GetLength(0) - 1
            If htmlwords(k).EndsWith("_G") = True Or htmlwords(k).EndsWith("_M") = True Or htmlwords(k).EndsWith("_HC") = True Or htmlwords(k).EndsWith("_SUBAVG") = True Or htmlwords(k).EndsWith("_ATT") = True Or htmlwords(k).EndsWith("_TOT") = True Or htmlwords(k).EndsWith("_HPOINTS") = True Then
                htmlString = htmlString.Replace("*" + htmlwords(k) + "*", "-")
            End If
        Next

        'Response.Write(sb.ToString())
        Response.Write(htmlString)


    End Sub

    Function GetConsolidatedSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_DESCR,SBG_ID,ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR,SBG_ORDER,SBG_SHORTCODE,'0' AS OPTIONAL FROM SUBJECTS_GRADE_S " _
                               & " WHERE SBG_GRD_ID='" + grd_id + "' AND SBG_ACD_ID=" + acd_id _
                               & " AND SBG_STM_ID=" + stm_id + " AND ISNULL(SBG_bOPTIONAL,0)=0 AND SBG_PARENT_ID=0"
        If hfPromotionSheet.Value = 1 Then
            str_query += " AND SBG_SHORTCODE NOT IN('LIB','PE')"
        End If
        str_query += " UNION ALL "

        str_query += "SELECT CON_DESCR AS SBG_DESCR,CON_ID AS SBG_ID,'TRUE' AS SBG_MAJOR,CON_ORDER AS SBG_ORDER,CON_SHORT AS SBG_SHORTCODE,'1' AS OPTIONAL" _
                   & " FROM CONSOLIDATED_SUBJECTGROUP_M " _
                   & "WHERE CON_ACD_ID=" + acd_id + " AND CON_GRD_ID='" + grd_id + "' AND CON_STM_ID=" + stm_id

      

        str_query = " SELECT * FROM (" + str_query + ")P ORDER BY SBG_ORDER"
       
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        str_query = "SELECT RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID=" + hfRSM_ID.Value _
                   & " AND RSD_HEADER NOT IN('Library','Physical Education')" _
                   & " AND RSD_bDIRECTENTRY=0" _
                   & " ORDER BY RSD_DISPLAYORDER"

        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count
        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 10)

        For j = 0 To i - 1
            With ds.Tables(0)
                Subjects(j, 0) = .Rows(j).Item(0).ToString
                Subjects(j, 1) = .Rows(j).Item(1).ToString
                Subjects(j, 2) = .Rows(j).Item(2).ToString
                Subjects(j, 3) = .Rows(j).Item(4).ToString


                Select Case grd_id
                    Case "07", "08", "09"
                        If ds1.Tables(0).Rows.Count > 1 Then
                            If hfPromotionSheet.Value = 1 Then
                                Subjects(j, 4) = "40%"
                                Subjects(j, 5) = "60%"
                                Subjects(j, 6) = "100"
                            Else
                                Subjects(j, 4) = "25"
                                Subjects(j, 5) = "75"
                                Subjects(j, 6) = "100"
                            End If
                        Else
                            Subjects(j, 4) = "MARK"
                            Subjects(j, 5) = ""
                            Subjects(j, 6) = ""
                        End If
                    Case "10"
                        If ds1.Tables(0).Rows.Count > 1 Then
                            If hfPromotionSheet.Value = 1 Then
                                Subjects(j, 4) = "40%"
                                Subjects(j, 5) = "60%"
                                Subjects(j, 6) = "100"
                            Else
                                Subjects(j, 4) = "20"
                                Subjects(j, 5) = "80"
                                Subjects(j, 6) = "100"
                            End If
                        Else
                            Subjects(j, 4) = "MARK"
                            Subjects(j, 5) = ""
                            Subjects(j, 6) = ""
                        End If
                    Case "11", "12", "13"
                        If hfPromotionSheet.Value = 1 Then
                            Subjects(j, 4) = "40%"
                            Subjects(j, 5) = "60%"
                            Subjects(j, 6) = "100"
                        Else
                            Subjects(j, 4) = "100"
                            Subjects(j, 5) = ""
                            Subjects(j, 6) = ""
                        End If
                    Case Else
                        Subjects(j, 4) = ""
                        Subjects(j, 5) = ""
                        Subjects(j, 6) = ""
                End Select


                Subjects(j, 7) = .Rows(j).Item(5).ToString

                Select Case grd_id
                    Case "07", "08", "09"
                        Subjects(j, 8) = ds1.Tables(0).Rows(0).Item(0)
                        If ds1.Tables(0).Rows.Count > 1 Then
                            Subjects(j, 9) = ds1.Tables(0).Rows(1).Item(0)
                            Subjects(j, 10) = ds1.Tables(0).Rows(2).Item(0)
                        End If
                    Case "10"
                        Subjects(j, 8) = ds1.Tables(0).Rows(0).Item(0)
                        If ds1.Tables(0).Rows.Count > 1 Then
                            Subjects(j, 9) = ds1.Tables(0).Rows(1).Item(0)
                            Subjects(j, 10) = ds1.Tables(0).Rows(2).Item(0)
                        End If
                    Case "11"
                        Subjects(j, 8) = ds1.Tables(0).Rows(0).Item(0)
                    Case "12", "13"
                        Subjects(j, 8) = ds1.Tables(0).Rows(0).Item(0)
                    Case Else
                        Subjects(j, 8) = ds1.Tables(0).Rows(0).Item(0)
                        'Subjects(j, 9) = ds1.Tables(0).Rows(1).Item(0)
                        'Subjects(j, 10) = ds1.Tables(0).Rows(2).Item(0)
                End Select

            End With
        Next
        Return Subjects
    End Function

    Function GetSubjectsHeader(ByVal Subjects(,) As String) As String
        Dim sb As New StringBuilder
        Dim i As Integer

        Dim strSubjects As String = ""
        Dim strHeader As String = ""
        Dim strShort1 As String = ""
        Dim strShort As String = ""
        Dim imgUrl As String = ""
        Dim colSpan As Integer = 0
        Dim colSpan1 As Integer = 0

        hfTableColumns.Value = 0
        For i = 0 To Subjects.GetLength(0) - 1
            'if major subject add one more column
            If Subjects(i, 2).ToLower = "true" Then
                colSpan = 0


                ' strSubjects += "<td width=""20"" align=""middle"" Class=repcolDetail>&nbsp;</td>"
                If hfGRD_ID.Value <> "01" And hfGRD_ID.Value <> "02" And hfGRD_ID.Value <> "03" And hfGRD_ID.Value <> "04" And hfGRD_ID.Value <> "05" And hfGRD_ID.Value And "06" Then
                    'option shortcode heading
                    If Subjects(i, 7) = 1 Then
                        strShort += "<td width=""20"" align=""middle"" Class=repcolDetail>" + Subjects(i, 3) + "</td>"
                        colSpan1 += 1
                    End If

                    'headers 25,75,100 etc
                    strHeader += "<td width=""20"" align=""middle"" Class=repcolDetail>" + Subjects(i, 4) + "</td>"
                    hfTableColumns.Value += 1
                    colSpan += 1

                End If
                If hfGRD_ID.Value <> "11" And hfGRD_ID.Value <> "12" And hfGRD_ID.Value <> "13" And hfGRD_ID.Value <> "01" And hfGRD_ID.Value <> "02" And hfGRD_ID.Value <> "03" And hfGRD_ID.Value <> "04" And hfGRD_ID.Value <> "05" And hfGRD_ID.Value And "06" Then

                    If Subjects(i, 5) <> "" Then
                        strHeader += "<td width=""20"" align=""middle"" Class=repcolDetail>" + Subjects(i, 5) + "</td>"
                        hfTableColumns.Value += 1
                        colSpan += 1
                    End If
                    If Subjects(i, 6) <> "" Then
                        strHeader += "<td width=""20"" align=""middle"" Class=repcolDetail>" + Subjects(i, 6) + "</td>"
                        hfTableColumns.Value += 1
                        colSpan += 1
                    End If
                End If
                'G
                strHeader += "<td width=""20"" align=""middle"" Class=repcolDetail>G</td>"
                hfTableColumns.Value += 1
                colSpan += 1

                strSubjects += "<td colspan=" + colSpan.ToString + " align=""middle"" valign=""bottom""  >" + Subjects(i, 0) + "</td>"
                hfTableColumns.Value += 1



            Else
                strSubjects += "<td width=""20"" align=""middle"" valign=""bottom""  rowspan=""2"" >" + Subjects(i, 3) + "</td>"
                hfTableColumns.Value += 1
            End If

        Next



        sb.AppendLine("<table border=""1"" bordercolorlight=""#000000"" WIDTH=""1400"" align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font:NORMAL NORMAL 6pt Verdana, arial, Helvetica, sans-serif"" >")

        sb.AppendLine("<tr >")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center> Sr. No  </td>")
        sb.AppendLine("<td width=""50"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center> Admin. No  </td>")
        sb.AppendLine("<td  width=400 ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center>Name of the Student</td>")

        If strShort <> "" Then
            sb.AppendLine("<td valign=""middle""  colspan=" + colSpan1.ToString + " align=""middle"" Class=repcolDetail>&nbsp;</td>")
        End If

        hfTableColumns.Value += 5
        sb.AppendLine(strSubjects)

        sb.AppendLine("<td rowspan=2 width=""20"" align=""middle"" valign=""middle""  >ATT</td>")
        sb.AppendLine("<td rowspan=2 width=""20"" align=""middle"" valign=""middle""  >Total</td>")
        sb.AppendLine("<td rowspan=2 width=""20"" align=""middle"" valign=""middle""  >House</td>")
        sb.AppendLine("<td rowspan=2 width=""20"" align=""middle"" valign=""middle""  >H.Points</td>")
        sb.AppendLine("<td rowspan=2 width=""200"" align=""middle"" valign=""middle""  >Remark</td>")

        hfTableColumns.Value += 6
        sb.AppendLine("</tr>")



        sb.AppendLine("<tr>")
        If strShort <> "" Then
            sb.Append(strShort)
        End If
        sb.AppendLine(strHeader)
        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function


    Function GetConsolidatedForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,)) As String
        Dim sb As New StringBuilder
        Dim strHeader As String = GetConsolidatedHeader(sct_id)
        ' sb.AppendLine(strHeader)
        sb.AppendLine("<table width=""1400""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

        sb.AppendLine("<tr><td algn=center>")
        sb.AppendLine(strHeader)
        sb.AppendLine("</td></tr>")

        sb.AppendLine("<tr><td algn=center>")

        sb.AppendLine(GetSubjectsHeader(Subjects))

        sb.AppendLine(GetStudentMarks(sct_id, Subjects))

        sb.AppendLine("</table>")

        sb.AppendLine("</td></tr>")
        sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetConsolidatedHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_BB_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table   align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=bottom>")


            sb.AppendLine("<table width=""90%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">")
            sb.AppendLine("<TR >")
            sb.AppendLine("<TD>GRADE :    " + reader("GRM_DISPLAY") + "   &nbsp;&nbsp; SECTION : " + reader("SCT_DESCR") + " </TD> </TR>")

            sb.AppendLine("</Table>")

            sb.AppendLine("</TD>")


            sb.AppendLine("<TD WIDTH=40% VALIGN=TOP>")

            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font style=""text-decoration:NONE;font:bold bold 14pt Times New Roman,Verdana, arial, Helvetica, sans-serif"">" + reader("BSU_NAME"))
            If hfPromotionSheet.Value = 1 Then
                sb.AppendLine("<BR>PROMOTION SHEET  " + reader("ACY_DESCR") + "</TD></TR>")

            Else
                sb.AppendLine("<BR>" + hfTERM.Value.ToUpper + " CONSOLIDATED SHEET  " + reader("ACY_DESCR") + "</TD></TR>")

            End If
         
            sb.AppendLine("</table>")

            sb.AppendLine("</TD>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=TOP ALIGN=""RIGHT"">")
            sb.AppendLine("<table width=""80%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("</table>")
            sb.AppendLine("</TD>")

            sb.AppendLine("</TR>")

            sb.AppendLine("<TR><TD colspan=2><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">&nbsp;&nbsp;&nbsp;&nbsp; Class Teacher :  " + reader("EMP_NAME") + " </TD>")
            sb.AppendLine("<TD align=right><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> Date : " + Now.Date + " </TD>")
            sb.AppendLine("</TR>")
            sb.AppendLine("</Table><BR>")

            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function

   
    Function GetStudentMarks(ByVal sct_id As String, ByVal Subjects(,) As String)
        Dim strTable As String

        Dim strFooter As String = GetConsolidatedFooter(Subjects)

        Dim strStudents As String = GetStudents(sct_id, Subjects)
        

        strTable = strStudents
        If hfLastRecord.Value = 0 Then
           
            strTable += strFooter
        End If

        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim id_M As String = ""
        Dim id_G As String = ""
        Dim i As Integer

        Dim strStuId As String = ""
        Dim strRetestSubjects As String = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        'POPULATE STUDENT MARKS


        Dim str_query As String
        If Session("Current_ACD_ID") = hfACD_ID.Value Then

            If hfPromotionSheet.Value = 1 Then
                str_query = "SELECT DISTINCT STU_ID,ISNULL(CND_CON_ID,SSD_SBG_ID),RSD_ID," _
                   & " CASE WHEN FPR_ATTENDANCE IS NULL THEN " _
                   & " CASE WHEN FPR_MARKS IS NULL THEN '&nbsp;' ELSE " _
                   & " CASE WHEN RSD_bPERFORMANCE_INDICATOR=1 THEN convert(varchar(100),convert(NUMERIC(6,0),round(FPR_MARKS,0))) ELSE convert(varchar(100),convert(NUMERIC(6,1),FPR_MARKS)) END " _
                   & " END ELSE FPR_ATTENDANCE END," _
                   & " CASE WHEN FPR_ATTENDANCE IS NULL THEN " _
                   & " ISNULL(FPR_GRADING,'&nbsp;')  ELSE FPR_ATTENDANCE END" _
                   & " ,ISNULL(RSD_bPERFORMANCE_INDICATOR,'FALSE'),ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR" _
                   & " FROM STUDENT_M AS C  " _
                   & " INNER JOIN STUDENT_GROUPS_S AS D ON C.STU_ID=D.SSD_STU_ID AND SSD_ACD_ID=STU_ACD_ID" _
                   & " INNER JOIN RPT.REPORT_RULE_M AS P ON D.SSD_SBG_ID=P.RRM_SBG_ID AND RRM_RPF_ID=" + hfRPF_ID.Value _
                   & " INNER JOIN RPT.REPORT_SETUP_D AS B ON P.RRM_RSD_ID=B.RSD_ID " _
                   & " INNER JOIN SUBJECTS_GRADE_S AS Q ON D.SSD_SBG_ID=Q.SBG_ID" _
                   & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON D.SSD_SBG_ID=E.CND_SBG_ID" _
                   & " LEFT OUTER JOIN RPT.FINALPROCESS_REPORT_S AS A ON A.FPR_STU_ID=C.STU_ID " _
                   & " AND A.FPR_RSD_ID=B.RSD_ID AND SSD_SBG_ID=FPR_SBG_ID " _
                   & " AND FPR_RPF_ID=" + hfRPF_ID.Value _
                   & " WHERE STU_SCT_ID=" + sct_id + "AND RSD_bDIRECTENTRY='FALSE'" _
                   & " AND RSD_RSM_ID=" + hfRSM_ID.Value
                ' & " AND RSD_HEADER NOT IN('Library','Physical Education')"
            Else
                str_query = "SELECT DISTINCT STU_ID,ISNULL(CND_CON_ID,SSD_SBG_ID),RSD_ID," _
                   & " CASE WHEN RST_ATTENDANCE IS NULL THEN " _
                   & " CASE WHEN RST_MARK IS NULL THEN '&nbsp;' ELSE convert(varchar(100),RST_MARK) END ELSE RST_ATTENDANCE END," _
                   & " CASE WHEN RST_ATTENDANCE IS NULL THEN " _
                   & " ISNULL(RST_GRADING,'&nbsp;')  ELSE RST_ATTENDANCE END" _
                   & " ,ISNULL(RSD_bPERFORMANCE_INDICATOR,'FALSE'),ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR" _
                   & " FROM STUDENT_M AS C  " _
                   & " INNER JOIN STUDENT_GROUPS_S AS D ON C.STU_ID=D.SSD_STU_ID AND SSD_ACD_ID=STU_ACD_ID" _
                   & " INNER JOIN RPT.REPORT_RULE_M AS P ON D.SSD_SBG_ID=P.RRM_SBG_ID AND RRM_RPF_ID=" + hfRPF_ID.Value _
                   & " INNER JOIN RPT.REPORT_SETUP_D AS B ON P.RRM_RSD_ID=B.RSD_ID " _
                   & " INNER JOIN SUBJECTS_GRADE_S AS Q ON D.SSD_SBG_ID=Q.SBG_ID" _
                   & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON D.SSD_SBG_ID=E.CND_SBG_ID" _
                   & " LEFT OUTER JOIN RPT.REPORT_STUDENT_S AS A ON A.RST_STU_ID=C.STU_ID " _
                   & " AND A.RST_RSD_ID=B.RSD_ID AND SSD_SBG_ID=RST_SBG_ID " _
                   & " AND RST_RPF_ID=" + hfRPF_ID.Value _
                   & " WHERE STU_SCT_ID=" + sct_id + "AND RSD_bDIRECTENTRY='FALSE'" _
                   & " AND RSD_RSM_ID=" + hfRSM_ID.Value
                ' & " AND RSD_HEADER NOT IN('Library','Physical Education')"
            End If
        Else
            If hfPromotionSheet.Value = 1 Then
                str_query = "SELECT DISTINCT STU_ID,ISNULL(CND_CON_ID,SSD_SBG_ID),RSD_ID," _
                   & " CASE WHEN FPR_ATTENDANCE IS NULL THEN " _
                   & " CASE WHEN FPR_MARKS IS NULL THEN '&nbsp;' ELSE " _
                   & " CASE WHEN RSD_bPERFORMANCE_INDICATOR=1 THEN convert(varchar(100),convert(NUMERIC(6,0),round(FPR_MARKS,0))) ELSE convert(varchar(100),convert(NUMERIC(6,1),FPR_MARKS)) END " _
                   & " END ELSE FPR_ATTENDANCE END," _
                   & " CASE WHEN FPR_ATTENDANCE IS NULL THEN " _
                   & " ISNULL(FPR_GRADING,'&nbsp;')  ELSE FPR_ATTENDANCE END" _
                   & " ,ISNULL(RSD_bPERFORMANCE_INDICATOR,'FALSE'),ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR" _
                   & " FROM STUDENT_M AS C  " _
                   & " INNER JOIN STUDENT_GROUPS_S AS D ON C.STU_ID=D.SSD_STU_ID AND SSD_ACD_ID=" + hfACD_ID.Value _
                   & " INNER JOIN RPT.REPORT_RULE_M AS P ON D.SSD_SBG_ID=P.RRM_SBG_ID AND RRM_RPF_ID=" + hfRPF_ID.Value _
                   & " INNER JOIN RPT.REPORT_SETUP_D AS B ON P.RRM_RSD_ID=B.RSD_ID " _
                   & " INNER JOIN SUBJECTS_GRADE_S AS Q ON D.SSD_SBG_ID=Q.SBG_ID" _
                   & " INNER JOIN OASIS..STUDENT_PROMO_S AS G ON C.STU_ID=G.STP_STU_ID" _
                   & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON D.SSD_SBG_ID=E.CND_SBG_ID" _
                   & " LEFT OUTER JOIN RPT.FINALPROCESS_REPORT_S AS A ON A.FPR_STU_ID=C.STU_ID " _
                   & " AND A.FPR_RSD_ID=B.RSD_ID AND SSD_SBG_ID=FPR_SBG_ID " _
                   & " AND FPR_RPF_ID=" + hfRPF_ID.Value _
                   & " WHERE STP_SCT_ID=" + sct_id + "AND RSD_bDIRECTENTRY='FALSE'" _
                   & " AND RSD_RSM_ID=" + hfRSM_ID.Value
                ' & " AND RSD_HEADER NOT IN('Library','Physical Education')"
            Else
                str_query = "SELECT DISTINCT STU_ID,ISNULL(CND_CON_ID,SSD_SBG_ID),RSD_ID," _
                   & " CASE WHEN RST_ATTENDANCE IS NULL THEN " _
                   & " CASE WHEN RST_MARK IS NULL THEN '&nbsp;' ELSE convert(varchar(100),RST_MARK) END ELSE RST_ATTENDANCE END," _
                   & " CASE WHEN RST_ATTENDANCE IS NULL THEN " _
                   & " ISNULL(RST_GRADING,'&nbsp;')  ELSE RST_ATTENDANCE END" _
                   & " ,ISNULL(RSD_bPERFORMANCE_INDICATOR,'FALSE'),ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR" _
                   & " FROM STUDENT_M AS C  " _
                   & " INNER JOIN STUDENT_GROUPS_S AS D ON C.STU_ID=D.SSD_STU_ID AND SSD_ACD_ID=" + hfACD_ID.Value _
                   & " INNER JOIN RPT.REPORT_RULE_M AS P ON D.SSD_SBG_ID=P.RRM_SBG_ID AND RRM_RPF_ID=" + hfRPF_ID.Value _
                   & " INNER JOIN RPT.REPORT_SETUP_D AS B ON P.RRM_RSD_ID=B.RSD_ID " _
                   & " INNER JOIN SUBJECTS_GRADE_S AS Q ON D.SSD_SBG_ID=Q.SBG_ID" _
                   & " INNER JOIN OASIS..STUDENT_PROMO_S AS G ON C.STU_ID=G.STP_STU_ID" _
                   & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON D.SSD_SBG_ID=E.CND_SBG_ID" _
                   & " LEFT OUTER JOIN RPT.REPORT_STUDENT_PREVYEARS AS A ON A.RST_STU_ID=C.STU_ID " _
                   & " AND A.RST_RSD_ID=B.RSD_ID AND SSD_SBG_ID=RST_SBG_ID " _
                   & " AND RST_RPF_ID=" + hfRPF_ID.Value _
                   & " WHERE STP_SCT_ID=" + sct_id + "AND RSD_bDIRECTENTRY='FALSE'" _
                   & " AND RSD_RSM_ID=" + hfRSM_ID.Value
                ' & " AND RSD_HEADER NOT IN('Library','Physical Education')"
            End If
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)


                id_G = "*" + .Item(0).ToString + "_" + .Item(1).ToString + "_G*"
                id_M = "*" + .Item(0).ToString + "_" + .Item(1).ToString + "_" + .Item(2).ToString + "_M*"


                strTable = strTable.Replace(id_M, .Item(3).ToString.Replace(".00", ""))

                If hfGRD_ID.Value <> "01" And hfGRD_ID.Value <> "02" And hfGRD_ID.Value <> "03" And hfGRD_ID.Value <> "04" And hfGRD_ID.Value <> "05" And hfGRD_ID.Value And "06" Then

                    If .Item(5).ToString.ToLower = "true" Or .Item(6).ToString.ToLower = "false" Then
                        strTable = strTable.Replace(id_G, .Item(4).ToString)
                    End If
                Else
                    strTable = strTable.Replace(id_G, .Item(4).ToString)
                End If

            End With
        Next




        'POPULATE TOTAL AND PERCENTAGE
        Dim percentage As Double
        If hfGRD_ID.Value <> "01" And hfGRD_ID.Value <> "02" And hfGRD_ID.Value <> "03" And hfGRD_ID.Value <> "04" And hfGRD_ID.Value <> "05" And hfGRD_ID.Value And "06" Then

            If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
                If hfPromotionSheet.Value = 1 Then
                    str_query = "SELECT STU_ID,SUM(ISNULL(FPR_MARKS,0)) FROM STUDENT_M AS A  " _
                        & " CROSS JOIN RPT.REPORT_SETUP_D AS C" _
                        & " INNER JOIN STUDENT_GROUPS_S AS E ON A.STU_ID=E.SSD_STU_ID " _
                        & " LEFT OUTER JOIN  RPT.FINALPROCESS_REPORT_S  AS B ON A.STU_ID=B.FPR_STU_ID AND " _
                        & " A.STU_ACD_ID=B.FPR_ACD_ID AND E.SSD_SBG_ID=B.FPR_SBG_ID" _
                        & " AND FPR_RPF_ID=" + hfRPF_ID.Value _
                        & " AND B.FPR_RSD_ID=C.RSD_ID " _
                        & " WHERE STU_SCT_ID=" + sct_id _
                        & " AND RSD_bPERFORMANCE_INDICATOR=1 AND RSD_RSM_ID=" + hfRSM_ID.Value _
                        & " AND RSD_HEADER NOT IN('Library','Physical Education')" _
                        & " GROUP BY STU_ID"
                Else
                    str_query = "SELECT STU_ID,SUM(ISNULL(RST_MARK,0)) FROM STUDENT_M AS A  " _
                        & " CROSS JOIN RPT.REPORT_SETUP_D AS C" _
                        & " INNER JOIN STUDENT_GROUPS_S AS E ON A.STU_ID=E.SSD_STU_ID " _
                        & " LEFT OUTER JOIN  RPT.REPORT_STUDENT_S  AS B ON A.STU_ID=B.RST_STU_ID AND " _
                        & " A.STU_ACD_ID=B.RST_ACD_ID AND E.SSD_SBG_ID=B.RST_SBG_ID" _
                        & " AND RST_RPF_ID=" + hfRPF_ID.Value _
                        & " AND B.RST_RSD_ID=C.RSD_ID " _
                        & " WHERE STU_SCT_ID=" + sct_id _
                        & " AND RSD_bPERFORMANCE_INDICATOR=1 AND RSD_RSM_ID=" + hfRSM_ID.Value _
                        & " AND RSD_HEADER NOT IN('Library','Physical Education')" _
                        & " GROUP BY STU_ID"
                End If
            Else
                If hfPromotionSheet.Value = 1 Then
                    str_query = "SELECT STU_ID,SUM(ISNULL(FPR_MARKS,0)) FROM STUDENT_M AS A  " _
                        & " CROSS JOIN RPT.REPORT_SETUP_D AS C" _
                        & " INNER JOIN STUDENT_GROUPS_S AS E ON A.STU_ID=E.SSD_STU_ID " _
                        & " INNER JOIN OASIS..STUDENT_PROMO_S AS G ON A.STU_ID=G.STP_STU_ID" _
                        & " LEFT OUTER JOIN  RPT.FINALPROCESS_REPORT_S  AS B ON A.STU_ID=B.FPR_STU_ID AND " _
                        & " G.STP_ACD_ID=B.FPR_ACD_ID AND E.SSD_SBG_ID=B.FPR_SBG_ID" _
                        & " AND FPR_RPF_ID=" + hfRPF_ID.Value _
                        & " AND B.FPR_RSD_ID=C.RSD_ID " _
                        & " WHERE STP_SCT_ID=" + sct_id _
                        & " AND RSD_bPERFORMANCE_INDICATOR=1 AND RSD_RSM_ID=" + hfRSM_ID.Value _
                        & " AND RSD_HEADER NOT IN('Library','Physical Education')" _
                        & " GROUP BY STU_ID"
                Else
                    str_query = "SELECT STU_ID,SUM(ISNULL(RST_MARK,0)) FROM STUDENT_M AS A  " _
                        & " CROSS JOIN RPT.REPORT_SETUP_D AS C" _
                        & " INNER JOIN STUDENT_GROUPS_S AS E ON A.STU_ID=E.SSD_STU_ID " _
                        & " INNER JOIN OASIS..STUDENT_PROMO_S AS G ON A.STU_ID=G.STP_STU_ID" _
                        & " LEFT OUTER JOIN  RPT.REPORT_STUDENT_S  AS B ON A.STU_ID=B.RST_STU_ID AND " _
                        & " G.STP_ACD_ID=B.RST_ACD_ID AND E.SSD_SBG_ID=B.RST_SBG_ID" _
                        & " AND RST_RPF_ID=" + hfRPF_ID.Value _
                        & " AND B.RST_RSD_ID=C.RSD_ID " _
                        & " WHERE STP_SCT_ID=" + sct_id _
                        & " AND RSD_bPERFORMANCE_INDICATOR=1 AND RSD_RSM_ID=" + hfRSM_ID.Value _
                        & " AND RSD_HEADER NOT IN('Library','Physical Education')" _
                        & " GROUP BY STU_ID"
                End If
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)



            Dim hp As String
            For i = 0 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    hp = GetHousePoints(Math.Round(CDbl(.Item(1)), 0))
                    strTable = strTable.Replace("*" + .Item(0).ToString + "_TOT*", IIf(Math.Round(CDbl(.Item(1)), 0).ToString = "0", "&nbsp;", Math.Round(CDbl(.Item(1)), 0).ToString))
                    strTable = strTable.Replace("*" + .Item(0).ToString + "_HPOINTS*", hp)
                    ViewState("hP").add(.Item(0).ToString, Val(Replace(hp, "+", "")))
                End With
            Next
        End If
        strTable = getStudentOptions(strTable, sct_id)
        strTable = GetAttendance(strTable, sct_id)
        strTable = GetGradeCount(strTable, sct_id, Subjects)
        strTable = GetHighestinClass(strTable, sct_id)
        strTable = getHouseCount(strTable, sct_id)

        Return strTable
    End Function

    Function GetHousePoints(ByVal marks As Integer)
        Dim hPoints As String = "&nbsp;"
        Select Case hfGRD_ID.Value
            Case "09"
                Select Case marks
                    Case 900 To 1000
                        hPoints = "+18"
                    Case 800 To 899
                        hPoints = "+15"
                    Case 700 To 799
                        hPoints = "+12"
                    Case 600 To 699
                        hPoints = "+9"
                    Case 500 To 599
                        hPoints = "+6"
                    Case 400 To 499
                        hPoints = "+3"
                    Case Else
                        hPoints = "0"
                End Select


            Case "07", "08", "10"
                Select Case marks
                    Case 810 To 900
                        hPoints = "+18"
                    Case 720 To 809
                        hPoints = "+15"
                    Case 630 To 719
                        hPoints = "+12"
                    Case 540 To 629
                        hPoints = "+9"
                    Case 450 To 539
                        hPoints = "+6"
                    Case 360 To 449
                        hPoints = "+3"
                    Case Else
                        hPoints = "0"
                End Select

            Case "11"
                Select Case marks
                    'Case ((marks >= 540) And (marks <= 600))
                    '    hPoints = "+18"
                    'Case ((marks >= 480) And (marks <= 539))
                    '    hPoints = "+15"
                    'Case ((marks >= 420) And (marks <= 479))
                    '    hPoints = "+12"
                    'Case ((marks >= 360) And (marks <= 419))
                    '    hPoints = "+9"
                    'Case ((marks >= 300) And (marks <= 359))
                    '    hPoints = "+6"
                    'Case ((marks >= 240) And (marks <= 299))
                    '    hPoints = "+3"
                    'Case (marks < 240)
                    '    hPoints = "0"

                    Case 540 To 600
                        hPoints = "+18"
                    Case 480 To 539
                        hPoints = "+15"
                    Case 420 To 479
                        hPoints = "+12"
                    Case 360 To 419
                        hPoints = "+9"
                    Case 300 To 359
                        hPoints = "+6"
                    Case 240 To 299
                        hPoints = "+3"
                    Case Else
                        hPoints = "0"
                End Select
        End Select

        Return hPoints.ToString
    End Function


    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ' Dim strSubjectHeader As String = GetSubjectsHeader(Subjects)
        'Dim strPageHeader As String = GetConsolidatedHeader(sct_id)
        Dim i As Integer
        Dim j As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()
        Dim strMain As String
        Dim strSub As String

        Dim strRedLine As String

        Dim str_query As String


        If Session("CURRECT_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                          & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                          & " ISNULL(STU_NO,'&nbsp') AS STU_NO, " _
                          & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                          & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                          & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                          & " STU_ID,SUBSTRING(HOUSE_DESCRIPTION,1,1) AS HOUSE FROM STUDENT_M AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                          & " LEFT OUTER JOIN OASIS..HOUSE_M AS C ON A.STU_HOUSE_ID=C.HOUSE_ID" _
                          & " WHERE STU_SCT_ID=" + sct_id + " AND STU_CURRSTATUS='EN' " _
                          & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'" _
                          & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Else
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                        & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                        & " ISNULL(STU_NO,'&nbsp') AS STU_NO, " _
                        & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                        & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                        & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                        & " STU_ID,SUBSTRING(HOUSE_DESCRIPTION,1,1) AS HOUSE FROM STUDENT_M AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                        & " INNER JOIN OASIS..STUDENT_PROMO_S AS D ON A.STU_ID=D.STP_STU_ID" _
                        & " LEFT OUTER JOIN OASIS..HOUSE_M AS C ON A.STU_HOUSE_ID=C.HOUSE_ID" _
                        & " WHERE STP_SCT_ID=" + sct_id + " AND STU_CURRSTATUS='EN' " _
                        & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'" _
                        & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count


        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)

                stuNames = .Item(0).ToString.Replace("  ", " ").Split(" ")
                sb.AppendLine("<tr height=""16px"">")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + .Item("STU_NO") + "</td>")
                If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                    sb.AppendLine("<td width=200PX align=""left"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                Else
                    sb.AppendLine("<td width=200PX align=""left"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                End If

                strMain = ""
                strSub = ""

                For j = 0 To Subjects.GetLength(0) - 1
                    If hfGRD_ID.Value <> "01" And hfGRD_ID.Value <> "02" And hfGRD_ID.Value <> "03" And hfGRD_ID.Value <> "04" And hfGRD_ID.Value <> "05" And hfGRD_ID.Value And "06" Then


                        If Subjects(j, 7) = 1 Then
                            strSub += "<td width=20 align=""CENTER"" ><font style=""font:8pt Times New roman"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + "M" + "*</font></td>"
                        End If

                        If Subjects(j, 2).ToLower = "true" Then
                            strMain += "<td width=20 align=""CENTER"" ><font style=""font:8pt Times New roman"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + Subjects(j, 8) + "_" + "M" + "*</font></td>"

                            If Subjects(j, 5) <> "" Then
                                strMain += "<td width=20 align=""CENTER"" ><font style=""font:8pt Times New roman"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + Subjects(j, 9) + "_" + "M" + "*</font></td>"
                            End If

                            If Subjects(j, 6) <> "" Then
                                strMain += "<td width=20 align=""CENTER"" ><font style=""font:8pt Times New roman"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + Subjects(j, 10) + "_" + "M" + "*</font></td>"
                            End If
                        End If
                    End If
                    strMain += "<td width=20 align=""CENTER""><font style=""font:8pt Times New roman"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + "G" + "*</font></td>"
                Next

                sb.AppendLine(strSub)
                sb.AppendLine(strMain)

                'Attendance
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_ATT*</font></td>")
                'Total Marks
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_TOT*</font></td>")

                ''House
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">" + IIf(.Item("HOUSE").ToString = "", "&nbsp;", .Item("HOUSE").ToString) + "</font></td>")


                'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_H.POINT*</font></td>")
                'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_REMARK*</font></td>")

                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_HPOINTS*</font></td>")
                sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")


                sb.AppendLine("</tr>")


            End With
        Next





        Return sb.ToString
    End Function

  
    Function GetConsolidatedFooter(ByVal subjects(,) As String) As String
        Dim sb As New StringBuilder

        sb.AppendLine(getFooter(subjects, ""))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        If hfGRD_ID.Value = "01" Or hfGRD_ID.Value = "02" Or hfGRD_ID.Value = "03" Or hfGRD_ID.Value = "04" Or hfGRD_ID.Value = "05" Or hfGRD_ID.Value Or "06" Then
            sb.AppendLine(getFooter(subjects, "A+&nbsp;"))
            sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")
        End If
        sb.AppendLine(getFooter(subjects, "A*&nbsp;"))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, "A&nbsp;"))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, "B&nbsp;"))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, "C&nbsp;"))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, "D&nbsp;"))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, "E&nbsp;"))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        If hfGRD_ID.Value <> "01" And hfGRD_ID.Value <> "02" And hfGRD_ID.Value <> "03" And hfGRD_ID.Value <> "04" And hfGRD_ID.Value <> "05" And hfGRD_ID.Value And "06" Then


            sb.AppendLine(getFooter(subjects, "F&nbsp;"))
            sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "G&nbsp;"))
            sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">ALPHA: ALPHA_COUNT</font></td>")
            sb.AppendLine("</tr>")


            sb.AppendLine(getFooter(subjects, "U&nbsp;"))
            sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">BETA:  BETA_COUNT</font></td>")
            sb.AppendLine("</tr>")
        End If
        sb.AppendLine(getFooter(subjects, "Highest in class"))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">GAMMA: GAMMA_COUNT</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, "Subject Average in Class"))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">SIGMA: SIGMA_COUNT</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, ""))
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function

    Function getFooter(ByVal subjects(,) As String, ByVal strText As String)
        Dim sb As New StringBuilder
        Dim strSub As String
        Dim strMain As String

        Dim j As Integer

        sb.AppendLine("<tr>")
        sb.AppendLine("<td HEIGHT=""16px"" align=""middle"" Class=repcolDetail><CENTER>&nbsp;</td>")
        sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>&nbsp;</td>")
        If strText = "" Then
            sb.AppendLine("<td align=""right"" Class=repcolDetail>&nbsp;</td>")
        Else
            sb.AppendLine("<td align=""right"" Class=repcolDetail>" + strText + "</td>")
        End If

     

        For j = 0 To subjects.GetLength(0) - 1

            If hfGRD_ID.Value <> "01" And hfGRD_ID.Value <> "02" And hfGRD_ID.Value <> "03" And hfGRD_ID.Value <> "04" And hfGRD_ID.Value <> "05" And hfGRD_ID.Value And "06" Then

                If subjects(j, 7) = 1 Then
                    strSub += "<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                End If

                If subjects(j, 2).ToLower = "true" Then
                    strMain += "<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>"

                    If subjects(j, 5) <> "" Then
                        strMain += "<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                    End If

                    If subjects(j, 6) <> "" Then
                        strMain += "<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                    End If
                End If
            End If
            If strText = "Highest in class" Then
                If subjects(j, 2).ToLower = "true" Then
                    strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">*count_" + subjects(j, 1) + "_HC*</font></td>"
                Else
                    strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                End If
            ElseIf strText = "Subject Average in Class" Then
                If subjects(j, 2).ToLower = "true" Then
                    strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">*count_" + subjects(j, 1) + "_SUBAVG*</font></td>"
                Else
                    strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                End If
            ElseIf strText = "" Then
                strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
            Else
                If subjects(j, 2).ToLower = "true" Then
                    strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">*count_" + subjects(j, 1) + "_" + strText + "*</font></td>"
                Else
                    strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                End If
            End If


        Next

        sb.AppendLine(strSub)
        sb.AppendLine(strMain)

        'Attendance
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")

        'Total Marks
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")

        ''Percentage
        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")


        sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")


        'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        'sb.AppendLine("</tr>")

        Return sb.ToString
    End Function

    Function getStudentOptions(ByVal strTable As String, ByVal SCT_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID,SBG_SHORTCODE,CND_CON_ID FROM " _
              & " STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID AND SSD_ACD_ID=STU_ACD_ID" _
              & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SSD_SBG_ID=C.SBG_ID" _
              & " INNER JOIN CONSOLIDATED_SUBJECTGROUP_D AS D ON C.SBG_ID=D.CND_SBG_ID" _
              & " WHERE STU_SCT_ID=" + SCT_ID
        Else
            str_query = "SELECT STU_ID,SBG_SHORTCODE,CND_CON_ID FROM " _
               & " STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID" _
               & " INNER JOIN OASIS..STUDENT_PROMO_S AS E ON A.STU_ID=E.STP_STU_ID  AND SSD_ACD_ID=STP_ACD_ID" _
               & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SSD_SBG_ID=C.SBG_ID" _
               & " INNER JOIN CONSOLIDATED_SUBJECTGROUP_D AS D ON C.SBG_ID=D.CND_SBG_ID" _
               & " WHERE STp_SCT_ID=" + SCT_ID
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id_M As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_M = "*" + .Item(0).ToString + "_" + .Item(2).ToString + "_M*"
                strTable = strTable.Replace(id_M, .Item(1).ToString)
            End With
        Next
        Return strTable
    End Function

    Function GetAttendance(ByVal strTable As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_query As String

        'If hfPromotionSheet.Value = 1 Then
        '    str_query = "SELECT STU_ID,ISNULL(ATT_VALUE,0) FROM " _
        '    & " STUDENT_M AS A LEFT OUTER JOIN RPT.FINAL_ATTENDANCE AS B ON A.STU_ID=B.ATT_STU_ID" _
        '    & " AND STU_ACD_ID=ATT_ACD_ID AND STU_SCT_ID=" + sct_id
        'Else
        '    str_query = "SELECT STU_ID,isnull(RST_COMMENTS,'&nbsp;') FROM STUDENT_M AS A " _
        '                & " CROSS JOIN  RPT.REPORT_SETUP_D AS C" _
        '                & " LEFT OUTER JOIN RPT.REPORT_STUDENT_S AS B ON A.STU_ID=B.RST_STU_ID AND RST_ACD_ID=STU_ACD_ID" _
        '                & " AND RST_RPF_ID=" + hfRPF_ID.Value _
        '                & " AND B.RST_RSD_ID=C.RSD_ID  " _
        '                & " WHERE STU_SCT_ID=" + sct_id _
        '                & " AND RSD_HEADER='Attendance' AND RSD_RSM_ID=" + hfRSM_ID.Value
        'End If

        str_query = "exec [RPT].[GETSTUD_ATT_SCTIONWISE] " _
                 & hfRPF_ID.Value + "," _
                 & hfRSM_ID.Value + "," _
                 & hfACD_ID.Value + "," _
                 & "'" + Session("sbsuid") + "'," _
                 & "'" + hfGRD_ID.Value + "'," _
                 & sct_id



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id_M As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_M = "*" + .Item(0).ToString + "_ATT*"
                strTable = strTable.Replace(id_M, .Item(2).ToString.Replace(".00", ""))
            End With
        Next

        Return strTable

    End Function

    Function GetGradeCount(ByVal strTable As String, ByVal sct_id As String, ByVal subjects(,) As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then
            If hfPromotionSheet.Value = 1 Then
                str_query = "SELECT DISTINCT STU_ID, ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,FPR_GRADING" _
                 & " FROM SUBJECTS_GRADE_S AS A " _
                 & " INNER JOIN RPT.FINALPROCESS_REPORT_S AS B ON A.SBG_ID=B.FPR_SBG_ID AND FPR_GRADING IN('A+','A','B','C','D','E','F','G','U','A*') " _
                 & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.FPR_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                 & " INNER JOIN STUDENT_M AS D ON B.FPR_STU_ID=D.STU_ID AND B.FPR_ACD_ID=D.STU_ACD_ID" _
                 & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                 & " WHERE FPR_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id _
                 & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'"

                str_query = "SELECT SBG_ID,FPR_GRADING,COUNT(FPR_GRADING) FROM(" + str_query + ")P " _
                           & " GROUP BY SBG_ID,FPR_GRADING"
            Else
                str_query = "SELECT DISTINCT STU_ID,ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,RST_GRADING" _
                    & " FROM SUBJECTS_GRADE_S AS A " _
                    & " INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.SBG_ID=B.RST_SBG_ID AND RST_GRADING IN('A+','A','B','C','D','E','F','G','U','A*') " _
                    & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                    & " INNER JOIN STUDENT_M AS D ON B.RST_STU_ID=D.STU_ID AND B.RST_ACD_ID=D.STU_ACD_ID" _
                    & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                    & " WHERE RST_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id _
                    & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'"

                str_query = "SELECT SBG_ID,RST_GRADING,COUNT(RST_GRADING) FROM(" + str_query + ")P " _
                           & " GROUP BY SBG_ID,RST_GRADING"
            End If
        Else

            If hfPromotionSheet.Value = 1 Then
                str_query = "SELECT DISTINCT STU_ID, ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,FPR_GRADING" _
                 & " FROM SUBJECTS_GRADE_S AS A " _
                 & " INNER JOIN RPT.FINALPROCESS_REPORT_S AS B ON A.SBG_ID=B.FPR_SBG_ID AND FPR_GRADING IN('A+','A','B','C','D','E','F','G','U','A*') " _
                 & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.FPR_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                 & " INNER JOIN STUDENT_M AS D ON B.FPR_STU_ID=D.STU_ID" _
                 & " INNER JOIN OASIS..STUDENT_PROMO_S AS F ON D.STU_ID=F.STP_STU_ID AND B.FPR_ACD_ID=F.STP_ACD_ID" _
                 & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                 & " WHERE FPR_RPF_ID=" + hfRPF_ID.Value + " AND STP_SCT_ID=" + sct_id _
                 & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'"

                str_query = "SELECT SBG_ID,FPR_GRADING,COUNT(FPR_GRADING) FROM(" + str_query + ")P " _
                           & " GROUP BY SBG_ID,FPR_GRADING"
            Else
                str_query = "SELECT DISTINCT STU_ID,ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,RST_GRADING" _
                    & " FROM SUBJECTS_GRADE_S AS A " _
                    & " INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B ON A.SBG_ID=B.RST_SBG_ID AND RST_GRADING IN('A+','A','B','C','D','E','F','G','U','A*') " _
                    & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                    & " INNER JOIN STUDENT_M AS D ON B.RST_STU_ID=D.STU_ID " _
                    & " INNER JOIN OASIS..STUDENT_PROMO_S AS F ON D.STU_ID=F.STP_STU_ID AND B.RST_ACD_ID=F.STP_ACD_ID" _
                    & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                    & " WHERE RST_RPF_ID=" + hfRPF_ID.Value + " AND STP_SCT_ID=" + sct_id _
                    & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'"

                str_query = "SELECT SBG_ID,RST_GRADING,COUNT(RST_GRADING) FROM(" + str_query + ")P " _
                           & " GROUP BY SBG_ID,RST_GRADING"
            End If
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id = "*count_" + .Item(0).ToString + "_" + .Item(1).ToString + "&nbsp;*"
                strTable = strTable.Replace(id, .Item(2).ToString)
            End With
        Next

        'if the grade is not there for that subject then replace with 0

        For i = 0 To subjects.GetLength(0) - 1
            id = "*count_" + subjects(i, 1) + "_A+&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_A*&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_A&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_B&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_C&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_D&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_E&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_F&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_G&nbsp;*"
            strTable = strTable.Replace(id, "0")

            id = "*count_" + subjects(i, 1) + "_U&nbsp;*"
            strTable = strTable.Replace(id, "0")
        Next

        Return strTable
    End Function


    Function GetHighestinClass(ByVal strTable As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String

        If Session("CURRENT_ACD_ID") = hfACD_ID.Value Then

            If hfPromotionSheet.Value = 1 Then
                str_query = "SELECT ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,FPR_MARKS" _
                   & " FROM SUBJECTS_GRADE_S AS A " _
                   & " INNER JOIN RPT.FINALPROCESS_REPORT_S AS B ON A.SBG_ID=B.FPR_SBG_ID " _
                   & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.FPR_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                   & " INNER JOIN STUDENT_M AS D ON B.FPR_STU_ID=D.STU_ID AND B.FPR_ACD_ID=D.STU_ACD_ID" _
                   & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                   & " WHERE FPR_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id

                str_query = "SELECT SBG_ID,MAX(isnull(FPR_MARKS,0)),AVG(isnull(FPR_MARKS,0)) FROM(" + str_query + ")P " _
                           & " GROUP BY SBG_ID"
            Else
                str_query = "SELECT ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,RST_MARK" _
                       & " FROM SUBJECTS_GRADE_S AS A " _
                       & " INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.SBG_ID=B.RST_SBG_ID " _
                       & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                       & " INNER JOIN STUDENT_M AS D ON B.RST_STU_ID=D.STU_ID AND B.RST_ACD_ID=D.STU_ACD_ID" _
                       & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                       & " WHERE RST_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id

                str_query = "SELECT SBG_ID,MAX(isnull(RST_MARK,0)),AVG(isnull(RST_MARK,0)) FROM(" + str_query + ")P " _
                           & " GROUP BY SBG_ID"
            End If
        Else

            If hfPromotionSheet.Value = 1 Then
                str_query = "SELECT ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,FPR_MARKS" _
               & " FROM SUBJECTS_GRADE_S AS A " _
               & " INNER JOIN RPT.FINALPROCESS_REPORT_S AS B ON A.SBG_ID=B.FPR_SBG_ID " _
               & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.FPR_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
               & " INNER JOIN STUDENT_M AS D ON B.FPR_STU_ID=D.STU_ID" _
               & " INNER JOIN OASIS..STUDENT_PROMO_S AS F ON D.STU_ID=F.STP_STU_ID AND B.FPR_ACD_ID=F.STP_ACD_ID" _
               & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
               & " WHERE FPR_RPF_ID=" + hfRPF_ID.Value + " AND STp_SCT_ID=" + sct_id

                str_query = "SELECT SBG_ID,MAX(isnull(FPR_MARKS,0)),AVG(isnull(FPR_MARKS,0)) FROM(" + str_query + ")P " _
                           & " GROUP BY SBG_ID"
            Else
                str_query = "SELECT ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,RST_MARK" _
               & " FROM SUBJECTS_GRADE_S AS A " _
               & " INNER JOIN RPT.REPORT_STUDENT_PREVYEARS AS B ON A.SBG_ID=B.RST_SBG_ID " _
               & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
               & " INNER JOIN STUDENT_M AS D ON B.RST_STU_ID=D.STU_ID" _
               & " INNER JOIN OASIS..STUDENT_PROMO_S AS F ON D.STU_ID=F.STP_STU_ID AND B.RST_ACD_ID=F.STP_ACD_ID" _
               & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
               & " WHERE RST_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id

                str_query = "SELECT SBG_ID,MAX(isnull(RST_MARK,0)),AVG(isnull(RST_MARK,0)) FROM(" + str_query + ")P " _
                           & " GROUP BY SBG_ID"
            End If

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id = "*count_" + .Item(0).ToString + "_HC*"
                strTable = strTable.Replace(id, .Item(1).ToString.Replace(".00", ""))
                id = "*count_" + .Item(0).ToString + "_SUBAVG*"
                strTable = strTable.Replace(id, Math.Round(.Item(2), 2).ToString.Replace(".00", ""))
            End With
        Next


        Return strTable
    End Function


    Function getHouseCount(ByVal strTable As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT HOUSE_DESCRIPTION,HOUSE_ID," _
                                & " STU_ID FROM STUDENT_M INNER JOIN HOUSE_M AS B on STU_HOUSE_ID=B.HOUSE_ID AND STU_SCT_ID=" + sct_id _
                                & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'" _
                                & " AND HOUSE_BSU_ID='" + hfBSU_ID.Value + "' AND HOUSE_DESCRIPTION<>''"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id_M As String = ""
        Dim i As Integer
        Dim alphaCount, betaCount, gammaCount, sigmaCount As Integer
        Dim alphaPoint, betaPoint, gammaPoint, sigmaPoint As Integer

        Dim hP As New Hashtable
        hP = ViewState("hP")

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                ' id_M = .Item(0).ToString.Trim.ToUpper + "_COUNT"
                'strTable = strTable.Replace(id_M, .Item(2).ToString)

                If .Item(0).ToString.Trim.ToUpper = "ALPHA" Then
                    alphaPoint += Val(hP.Item(.Item(2).ToString))
                    alphaCount += 1
                ElseIf .Item(0).ToString.Trim.ToUpper = "BETA" Then
                    betaPoint += Val(hP.Item(.Item(2).ToString))
                    betaCount += 1
                ElseIf .Item(0).ToString.Trim.ToUpper = "GAMMA" Then
                    gammaPoint += Val(hP.Item(.Item(2).ToString))
                    gammaCount += 1
                ElseIf .Item(0).ToString.Trim.ToUpper = "SIGMA" Then
                    sigmaPoint += Val(hP.Item(.Item(2).ToString))
                    sigmaCount += 1
                End If
            End With
        Next

        strTable = strTable.Replace("ALPHA_COUNT", (Math.Round(alphaPoint / IIf(alphaCount = 0, 1, alphaCount), 2)).ToString)
        strTable = strTable.Replace("BETA_COUNT", (Math.Round(betaPoint / IIf(betaCount = 0, 1, betaCount), 2)).ToString)
        strTable = strTable.Replace("GAMMA_COUNT", (Math.Round(gammaPoint / IIf(gammaCount = 0, 1, gammaCount), 2)).ToString)
        strTable = strTable.Replace("SIGMA_COUNT", (Math.Round(sigmaPoint / IIf(sigmaCount = 0, 1, sigmaCount), 2)).ToString)

        Return strTable


    End Function


#End Region


End Class
