Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM

Partial Class Curriculum_ConsolidatedReports_clmConsolidatedLevelTracker_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            hfMnu.Value = ViewState("MainMnu_code")
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400250" And ViewState("MainMnu_code") <> "C400252") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page


                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    If hfMnu.Value = "C400250" Then
                        trTC.Visible = False
                    End If
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)


                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")

                    '  If Session("CurrSuperUser") = "Y" Then
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    '  Else
                    '  ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    '  End If
                    BindSection("")

                    BindReportCard()

                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportCard()
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RPF_ID,RPF_DESCR,RPF_DISPLAYORDER FROM " _
                              & " RPT.REPORT_PRINTEDFOR_M INNER JOIN RPT.REPORT_SETUP_M ON " _
                              & " RPF_RSM_ID=RSM_ID INNER JOIN RPT.REPORTSETUP_GRADE_S ON " _
                              & " RSM_ID=RSG_RSM_ID WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                              & " AND RSG_GRD_ID='" + grade(0) + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RPF_DESCR"
        ddlReportCard.DataValueField = "RPF_ID"
        ddlReportCard.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                   & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                                   & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                                   & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                                   & " WHERE grm_acd_id=" + acdid _
                                   & " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function


    Function PopulateSubjects(ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT  SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + acd_id



        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")
            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)
        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SBG_SBM_ID SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                    & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + acd_id

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")
            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)
        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function getSubjects() As String
        Dim subs As String = ""
        Dim i As Integer

        For i = 0 To ddlSubject.Items.Count - 1
            If ddlSubject.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += ddlSubject.Items(i).Value
            End If
        Next
        Return subs

    End Function


    Sub BindSection(ByVal sctid As String)
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            If sctid <> "" Then
                str_query += " and sct_id=" + sctid
            End If
            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            If sctid = "" Then
                ddlSection.Items.Insert(0, li)
            End If
        End If
    End Sub

    Sub CallReport()
        Dim param As New Hashtable

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")
        Dim grade As String() = ddlGrade.SelectedValue.ToString.Split("|")

        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@GRD_ID", grade(0))
        param.Add("sct_id", ddlSection.SelectedValue.ToString)
        param.Add("@SBG_IDS", getSubjects)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("Grade", ddlGrade.SelectedItem.Text)
        param.Add("Section", ddlSection.SelectedItem.Text)
        param.Add("schedule", ddlReportcard.SelectedItem.Text)
        param.Add("rpf_id", ddlReportcard.Selectedvalue.tostring)
        PARAM.ADD("@bINCLUDETC", chkTC.checked.tostring)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Reports/Rpt/KHDA/rptReportLevelsDashBoard.rpt")
        End With

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            ReportLoadSelection()
            '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        End If

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
#End Region


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged

        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        ' If Session("CurrSuperUser") = "Y" Then
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ' Else
        ' ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ' End If
        BindSection("")
        BindReportCard()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If hfMnu.Value = "C400250" Then
            Dim strClass As String
            If ddlSection.SelectedValue.ToString = "0" Then
                strClass = ddlGrade.SelectedItem.Text + " - " + ddlReportCard.SelectedItem.Text.ToUpper + " (" + ddlAcademicYear.SelectedItem.Text + ")"
            Else
                strClass = ddlGrade.SelectedItem.Text + " " + ddlSection.SelectedItem.Text + " - " + ddlReportCard.SelectedItem.Text.ToUpper + " (" + ddlAcademicYear.SelectedItem.Text + ")"
            End If

            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedLevelTracker.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&grade=" + Encr_decrData.Encrypt(grade(0)) _
                               & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                               & "&subject=" + Encr_decrData.Encrypt(getSubjects) _
                               & "&class=" + Encr_decrData.Encrypt(strClass) _
                               & "&rpf=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                               & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                               , ViewState("MainMnu_code"), ViewState("datamode"))
            ResponseHelper.Redirect(url, "_blank", "")
        Else
            hfbdownload.value = 0
            callreport()
        End If
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbdownload.value = 1
        callreport()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        '  If Session("CurrSuperUser") = "Y" Or Session("formtutor") = "Y" Then
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ' Else
        ' ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        ' End If

        BindSection("")
        BindReportCard()
    End Sub




End Class
