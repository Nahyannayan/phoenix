<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmLessonPlannerDocuments.aspx.vb"
    Inherits="Curriculum_ConsolidatedReports_clmLessonPlannerDocuments" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="450px" cellpadding="5" cellspacing="0" border="1" bordercolor="#1b80b6"
                style="border-collapse: collapse;">
                <tr class="subheader_img">
                    <td colspan="6">
                        LESSON DOCUMENTS
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left">
                        Academic Year
                    </td>
                    <td class="matters">
                        :</td>
                    <td class="matters" align="left">
                        <asp:Label ID="lblAccYear" runat="server"></asp:Label></td>
                    <td class="matters" align="left">
                        Grade
                    </td>
                    <td class="matters">
                        :</td>
                    <td class="matters" align="left">
                        <asp:Label ID="lblGrade" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="matters" align="left">
                        Subject
                    </td>
                    <td class="matters">
                        :</td>
                    <td class="matters" align="left">
                        <asp:Label ID="lblSubject" runat="server"></asp:Label></td>
                    <td class="matters" align="left">
                        Topic
                    </td>
                    <td class="matters">
                        :</td>
                    <td class="matters" align="left">
                        <asp:Label ID="lblTopic" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" class="matters" style="width: 88px">
                        Documents
                    </td>
                    <td align="left" class="matters" style="width: 15px">
                        :
                    </td>
                    <td align="left" class="matters" colspan="4">
                        <asp:DataList ID="dlDocs" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td align="center" style="height: 26px">
                                            <asp:ImageButton ID="imgF" runat="server" Width="20px" OnClick="imgF_Click" CommandArgument='<%# Bind("id") %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:LinkButton ID="txtfile" runat="server" Width="70px" Text='<%# Bind("FNAME") %>'
                                                CommandName="View" CommandArgument='<%# Bind("id") %>' OnClick="txtfile_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
            <asp:HiddenField runat="server" ID="h_TopicID"></asp:HiddenField>
        </div>
    </form>
</body>
</html>
