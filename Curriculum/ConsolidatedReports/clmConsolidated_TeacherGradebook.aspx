﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmConsolidated_TeacherGradebook.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_TeacherGradebook" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css"/>
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="gvSubj" runat="server"  HorizontalAlign="Center" CssClass="table table-bordered table-row" >
        <RowStyle HorizontalAlign="Center"  />
        <AlternatingRowStyle  />
         </asp:GridView>
          <asp:HiddenField ID="hfSBG_ID" runat="server" />
    <asp:HiddenField ID="hfGRD_ID" runat="server" />
    <asp:HiddenField ID="hfACD_ID" runat="server" />
     <asp:HiddenField ID="hfSGR_ID" runat="server" />
      <asp:HiddenField ID="hfCAD_IDs" runat="server" />
          <asp:HiddenField ID="hfMarks" runat="server" />
    </div>
    </form>
</body>
</html>
