<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsSubjectGroup_M.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsSubjectGroup_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">

function moveOptionUp() 
{
var obj=document.getElementById("<%=lstRequired.ClientId%>")
	if (!hasOptions(obj)) { return; }
	for (i=0; i<obj.options.length; i++) {
		if (obj.options[i].selected) {
			if (i != 0 && !obj.options[i-1].selected) {
				swapOptions(obj,i,i-1);
				obj.options[i-1].selected = true;
				}
			}
		}
persistReqOptionsList();	
}
function hasOptions(obj) {
	if (obj!=null && obj.options!=null) { return true; }
	return false;
	}


function moveOptionDown() {
var obj=document.getElementById("<%=lstRequired.ClientId%>")
	if (!hasOptions(obj)) { return; }
	for (i=obj.options.length-1; i>=0; i--) {
		if (obj.options[i].selected) {
			if (i != (obj.options.length-1) && ! obj.options[i+1].selected) {
				swapOptions(obj,i,i+1);
				obj.options[i+1].selected = true;
				}
			}
		}
	persistReqOptionsList();
}


function removeSelectedOptions()
 { 
var from=document.getElementById("<%=lstRequired.ClientId%>")

	if (!hasOptions(from)) { return; }
	if (from.type=="select-one") {
		from.options[from.selectedIndex] = null;
		}
	else {
		for (var i=(from.options.length-1); i>=0; i--) { 
			var o=from.options[i]; 
			if (o.selected) { 
				from.options[i] = null; 
				} 
			}
		}
	from.selectedIndex = -1; 
	persistReqOptionsList();
	} 


function RemoveOption()
{

}
function swapOptions(obj,i,j) {
	var o = obj.options;
	var i_selected = o[i].selected;
	var j_selected = o[j].selected;
	var temp = new Option(o[i].text, o[i].value, o[i].defaultSelected, o[i].selected);
	var temp2= new Option(o[j].text, o[j].value, o[j].defaultSelected, o[j].selected);
	o[i] = temp2;
	o[j] = temp;
	o[i].selected = j_selected;
	o[j].selected = i_selected;
		}
		
		
	//to retain values in the trips listbox after postback the list items are stored in an input box	
function persistReqOptionsList()
{
 var listBox1 = document.getElementById("<%=lstRequired.ClientId%>");
 var optionsList = '';
 if (listBox1.options.length>0)
 {
 for (var i=0; i<listBox1.options.length; i++)
 {
  var optionText = listBox1.options[i].text;
  var optionValue = listBox1.options[i].value;
 
  if ( optionsList.length > 0 )
   optionsList += '|';
   optionsList += optionText+"$"+optionValue;
 }
}
 document.getElementById("<%=lstValuesReq.ClientId%>").value = optionsList;
 return false;
 }
 
 
 function persistNotReqOptionsList()
{
 var listBox1 = document.getElementById("<%=lstNotRequired.ClientId%>");
 var optionsList = '';
 if (listBox1.options.length>0)
 {
 for (var i=0; i<listBox1.options.length; i++)
 {
  var optionText = listBox1.options[i].text;
  var optionValue = listBox1.options[i].value;
 
  if ( optionsList.length > 0 )
   optionsList += '|';
   optionsList += optionText+"$"+optionValue;
 }
}
 document.getElementById("<%=lstValuesNotReq.ClientId%>").value = optionsList;
 return false;
 }
 
 function moveSelectedLeftToRight()
 {
 var from=document.getElementById("<%=lstNotRequired.ClientId%>");
 var to=document.getElementById("<%=lstRequired.ClientId%>");
 if (!hasOptions(from)) { return; }
	for (var i=0; i<from.options.length; i++) {
		var o = from.options[i];
		if (o.selected) {
			if (!hasOptions(to)) { var index = 0; } else { var index=to.options.length; }
			to.options[index] = new Option( o.text, o.value, false, false);
			}
		}
			// Delete them from original
	for (var i=(from.options.length-1); i>=0; i--) {
		var o = from.options[i];
		if (o.selected) {
			from.options[i] = null;
			}
		}
	persistReqOptionsList();
	persistNotReqOptionsList();
 }
 function moveSelectedRighttoLeft()
 {
 var to=document.getElementById("<%=lstNotRequired.ClientId%>");
 var from=document.getElementById("<%=lstRequired.ClientId%>");
 if (!hasOptions(from)) { return; }
	for (var i=0; i<from.options.length; i++) {
		var o = from.options[i];
		if (o.selected) {
			if (!hasOptions(to)) { var index = 0; } else { var index=to.options.length; }
			to.options[index] = new Option( o.text, o.value, false, false);
			}
		}
			// Delete them from original
	for (var i=(from.options.length-1); i>=0; i--) {
		var o = from.options[i];
		if (o.selected) {
			from.options[i] = null;
			}
		}
	persistReqOptionsList();
	persistNotReqOptionsList();
 }
 

</script>


   <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Set Consolidated Subject Groups
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">

                <tr>
                <td align="left"  >
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
         
                </td>  
                </tr>


                <tr><td align="center">
                <table id="Table1" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%" >

                <tr>
                <td align="left" width="20%">
                <asp:Label ID="lblAccText" runat="server" Text="Select Academic Year" CssClass="field-label"  ></asp:Label></td>
                
                <td align="left" width="30%">
                <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="True">
                </asp:DropDownList></td>
                <td align="left" width="20%">
                <span class="field-label">Select Grade</span></td>
                
                <td align="left" width="30%">
                <asp:DropDownList ID="ddlGrade" runat="server"   AutoPostBack="True">
                </asp:DropDownList></td>

                </tr> 
                    <tr>
                        <td align="left"   >
                            <span class="field-label">Group Name</span></td>
                        
                        <td align="left"   >
                            <asp:TextBox id="txtDesr" runat="server"></asp:TextBox></td>
                        <td align="left"   >
                            <span class="field-label">Short Name</span></td>
                        
                        <td align="left"   >
                            <asp:TextBox id="txtShort" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                <tr>
                <td align="left" class="title-bg"  colspan="4">Details</td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                <td align="center" colspan="4" >    

                <table id="Table4" runat="server" align="center" width="100%">         
                <tr>
                    <td align="left" class="title-bg-lite" width="40%">                
                Subjects</td>
                    <td width="10%"></td>
                    <td align="left" class="title-bg-lite" width="40%">
                Subject Group</td>
                    <td width="10%"></td>
                </tr>
                <tr>
                <td align="center" width="40%">
                <table id="Table3" runat="server" width="100%" >
                
                <tr>
                <td align="left" >
                <asp:ListBox ID="lstNotRequired" runat="server"   SelectionMode="Multiple"></asp:ListBox></td>
                </tr>
                </table>
                </td>
                <td width="10%">
                <input id="btnRight" type="button" value=">>" onclick="moveSelectedLeftToRight()" class="button"    /><br />
                <br />   
                <input id="btnLeft" type="button" value="<<" onclick="moveSelectedRighttoLeft()"  class="button"   />
                </td>    
                <td width="40%" >
                <table id="Table2" runat="server" width="100%">
                
                <tr>
                <td align="left" >
                <asp:ListBox ID="lstRequired" runat="server"   SelectionMode="Multiple"></asp:ListBox></td>
                </tr>
                </table>
                </td>
                <td align="center" width="10%" >
                <input id="btnUp" type="button" value="Up" onclick="moveOptionUp()" class="button"   /><br />
                <br />   
                <input id="btnDown" type="button" value="Down" onclick="moveOptionDown()" class="button"   />
                </td>

                </tr>

                </table>

                </td></tr>

                <tr>
                <td align="center" colspan="6">

                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="groupM1" CausesValidation="False" />
                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" ValidationGroup="groupM1" CausesValidation="False" />
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" ValidationGroup="groupM1" CausesValidation="False" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                Text="Cancel" UseSubmitBehavior="False" Visible="False" />
                </td>
                </tr>
                </table>
                    <asp:HiddenField id="hfCON_ID" runat="server">
                    </asp:HiddenField>
                &nbsp;&nbsp;
                <input id="lstValuesReq" name="lstValues" runat="server" type="hidden" style="left: 274px;
                 position: absolute; top: 161px; height: 10px"  />
                <input id="lstValuesNotReq" name="lstValuesNotReq" runat="server" type="hidden" style="left: 274px;
                 position: absolute; top: 161px; height: 10px"  />

                </td></tr>

                </table>

                </div>
        </div>
    </div>

</asp:Content>

