﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports GemBox.Spreadsheet

Partial Class Curriculum_ConsolidatedReports_clmConsolidated_KHDA_new_format
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")



                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330291") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    PopulateGrade(ddlAcademicYear.SelectedValue.ToString)
                    PopulateSection(ddlAcademicYear.SelectedValue.ToString)

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnView)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub PopulateGrade(ByVal acdid As String)
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

    End Sub

    Sub PopulateSection(ByVal acdid As String)
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Session("sbsuid") = "111001" Or Session("sbsuid") = "115002" Then
            str_query = "select distinct STU_BB_SECTION_DES as SCT_ID,STU_BB_SECTION_DES as SCT_DESCR from student_m " _
                    & " where   [STU_BSU_ID]='" + Session("sbsuid") + "' and [STU_ACD_ID]='" & ddlAcademicYear.SelectedValue.ToString & "'" _
                    & " and STU_GRD_ID='" & grade(0) & "'" _
                    & " and stu_stm_id='" + grade(1) + "' order by STU_BB_SECTION_DES"

        Else
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M AS A " _
                         & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                         & " WHERE GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) _
                         & " AND GRM_ACD_ID=" + acdid _
                         & " ORDER BY SCT_DESCR"

        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub
    

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection(ddlAcademicYear.SelectedValue.ToString)
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade(ddlAcademicYear.SelectedValue.ToString)
        PopulateSection(ddlAcademicYear.SelectedValue.ToString)
    End Sub

    Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        Dim connStr As String
        Dim DS As New DataSet
        Dim pParms(7) As SqlClient.SqlParameter

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        connStr = ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString

        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        pParms(1) = New SqlClient.SqlParameter("@GRD_ID", grade(0))
        pParms(2) = New SqlClient.SqlParameter("@SCT_ID", ddlSection.SelectedValue)
       
        DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "RPT.getBROWNBOOKMARKS_EXCEL_KHDA", pParms)

        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(DS.Tables(0), New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        'ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "ExportPDP.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "KHDA_New_Format.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub
   
End Class
