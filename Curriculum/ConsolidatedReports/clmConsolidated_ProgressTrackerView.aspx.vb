﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_ProgressTrackerView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code1") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400395") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights


                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)


                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")

                    If Session("CurrSuperUser") = "Y" Then
                        lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If
                    BindSection("")
                    GridBind()
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If


    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindSection(ByVal sctid As String)

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        If sctid <> "" Then
            str_query += " and sct_id=" + sctid
        End If
        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)


    End Sub


    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + acdid _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function PopulateSubjects(ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_SBM_ID NOT IN(57,58) AND SBG_ACD_ID=" + acd_id
        If ViewState("MainMnu_code") = "C300008" Then
            'str_query += " INNER JOIN ACT.ACTIVITY_SCHEDULE ON SBG_ID=CAS_SBG_ID AND SBG_PARENT_ID=0"
            str_query += "  AND SBG_PARENT_ID=0"

        End If


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                    & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "


        str_query += " WHERE SBG_ACD_ID=" + acd_id

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Sub GridBind()
        Dim grade() As String = ddlGrade.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT  RPF_ID,RPF_DESCR,RSM_ID FROM RPT.REPORT_PRINTEDFOR_M INNER JOIN " _
                                  & " RPT.REPORTSETUP_GRADE_S ON RPF_RSM_ID=RSG_RSM_ID " _
                                  & " INNER JOIN RPT.REPORT_SETUP_M ON  RPF_RSM_ID=RSM_ID " _
                                  & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND RSG_GRD_ID='" + grade(0) + "'" _
                                  & " ORDER BY RPF_REPORTORDER,RSM_DISPLAYORDER,RPF_DISPLAYORDER,RPF_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGroup.DataSource = ds
        gvGroup.DataBind()

    End Sub
#End Region


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        If Session("CurrSuperUser") = "Y" Then
            lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection("")
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If Session("CurrSuperUser") = "Y" Then
            lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection("")
        GridBind()
    End Sub

    Protected Sub btnConsolidatedReport_Click(sender As Object, e As EventArgs) Handles btnConsolidatedReport.Click
        Dim i As Integer
        Dim j As Integer
        Dim rpf_ids As String = ""
        Dim rsd_ids As String = ""
        Dim rpf_progress As String = ""

        Dim sbg_ids As String = ""
        Dim chkSelect As CheckBox
        Dim lblRpfId As Label
        Dim lstHeader As CheckBoxList
        Dim chkProgress As CheckBox

        For i = 0 To gvGroup.Rows.Count - 1
            With gvGroup.Rows(i)
                chkSelect = .FindControl("chkSelect1")
                If chkSelect.Checked = True Then
                    lblRpfId = .FindControl("lblRpfId")
                    If rpf_ids <> "" Then
                        rpf_ids += "|"
                    End If
                    rpf_ids += lblRpfId.Text
                End If

                chkProgress = .FindControl("chkProgress")
                If chkProgress.Checked = True Then
                    If rpf_progress <> "" Then
                        rpf_progress += "|"
                    End If
                    rpf_progress += lblRpfId.Text
                End If

            End With
        Next

        For i = 0 To lstSubject.Items.Count - 1
            If lstSubject.Items(i).Selected = True Then
                If sbg_ids <> "" Then
                    sbg_ids += "|"
                End If
                sbg_ids += lstSubject.Items(i).Value.ToString
            End If
        Next


        Dim strtext As String = ddlAcademicYear.SelectedItem.Text + " - " + ddlGrade.SelectedItem.Text
        Dim url As String
        url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_ProgressTracker.aspx?MainMnu_code={0}&datamode={1}" _
                            & "&rpf_ids=" + Encr_decrData.Encrypt(rpf_ids) _
                            & "&rpf_progress=" + Encr_decrData.Encrypt(rpf_progress) _
                            & "&rsd_ids=" + Encr_decrData.Encrypt(rsd_ids) _
                            & "&sbg_ids=" + Encr_decrData.Encrypt(sbg_ids) _
                            & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                            & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                            & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                            & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                            & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                            & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                            & "&bTC=" + Encr_decrData.Encrypt(IIf(chkTc.Checked = True, "1", "0")) _
                            & "&type=" + Encr_decrData.Encrypt(ddlType.SelectedItem.Text) _
                            & "&strtext=" + Encr_decrData.Encrypt(strtext), ViewState("MainMnu_code"), ViewState("datamode"))
        ResponseHelper.Redirect(url, "_blank", "")
    End Sub

    Protected Sub btnAnalysisReport_Click(sender As Object, e As EventArgs) Handles btnAnalysisReport.Click
        Dim i As Integer
        Dim j As Integer
        Dim rpf_ids As String = ""
        Dim rsd_ids As String = ""
        Dim rpf_progress As String = ""

        Dim sbg_ids As String = ""
        Dim chkSelect As CheckBox
        Dim lblRpfId As Label
        Dim lstHeader As CheckBoxList
        Dim chkProgress As CheckBox

        For i = 0 To gvGroup.Rows.Count - 1
            With gvGroup.Rows(i)
                chkSelect = .FindControl("chkSelect1")
                If chkSelect.Checked = True Then
                    lblRpfId = .FindControl("lblRpfId")
                    If rpf_ids <> "" Then
                        rpf_ids += "|"
                    End If
                    rpf_ids += lblRpfId.Text
                End If

                chkProgress = .FindControl("chkProgress")
                If chkProgress.Checked = True Then
                    If rpf_progress <> "" Then
                        rpf_progress += "|"
                    End If
                    rpf_progress += lblRpfId.Text
                End If

            End With
        Next

        For i = 0 To lstSubject.Items.Count - 1
            If lstSubject.Items(i).Selected = True Then
                If sbg_ids <> "" Then
                    sbg_ids += "|"
                End If
                sbg_ids += lstSubject.Items(i).Value.ToString
            End If
        Next


        Dim strtext As String = ddlAcademicYear.SelectedItem.Text + " - " + ddlGrade.SelectedItem.Text
        Dim url As String
        url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_AnalysisTracker.aspx?MainMnu_code={0}&datamode={1}" _
                            & "&rpf_ids=" + Encr_decrData.Encrypt(rpf_ids) _
                            & "&rpf_progress=" + Encr_decrData.Encrypt(rpf_progress) _
                            & "&rsd_ids=" + Encr_decrData.Encrypt(rsd_ids) _
                            & "&sbg_ids=" + Encr_decrData.Encrypt(sbg_ids) _
                            & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                            & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                            & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                            & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                            & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                            & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                            & "&baseline=" + Encr_decrData.Encrypt(ddlBaseLine.SelectedItem.Text) _
                            & "&bTC=" + Encr_decrData.Encrypt(IIf(chkTc.Checked = True, "1", "0")) _
                            & "&type=" + Encr_decrData.Encrypt(ddlType.SelectedItem.Text) _
                            & "&strtext=" + Encr_decrData.Encrypt(strtext), ViewState("MainMnu_code"), ViewState("datamode"))
        ResponseHelper.Redirect(url, "_blank", "")

    End Sub
End Class
