﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsolidated_KHDA_new_format.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_KHDA_new_format" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="Label1" runat="server" Text="Consolidated Report-KHDA New Format"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error"
                                EnableViewState="False" ForeColor="" HeaderText="You must enter a value in the following fields:"
                                SkinID="error" ValidationGroup="groupM1"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>

                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Section</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                        <td class="matters" colspan="2" align="center">
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" ValidationGroup="groupM1" />
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

