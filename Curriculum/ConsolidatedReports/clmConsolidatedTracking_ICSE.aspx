<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmConsolidatedTracking_ICSE.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_ICSE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:Literal ID="ltBrownBook" runat="server"></asp:Literal>
        <asp:HiddenField ID="hfSBG_IDS" runat="server" /><asp:HiddenField ID="hfRecordNumber" runat="server" Visible="False" />
        <asp:HiddenField ID="hfPageNumber" runat="server" Visible="False" />
        <asp:HiddenField ID="hfTableColumns" runat="server" Visible="False" />
        <br />
        <asp:HiddenField ID="hfLastRecord" runat="server" Value="0" Visible="False" />
        </div><asp:HiddenField ID="hfClassTeacher" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfSubmitDate" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfPassedCount" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfTotalStudents" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfDetained" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfSTM_ID" runat="server" />
        <asp:HiddenField ID="hfShowParent1" runat="server" />
        <asp:HiddenField ID="hfShowParent2" runat="server" /><asp:HiddenField ID="hfTERM" runat="server" />
        <asp:HiddenField ID="hfPromotionSheet" runat="server" />
        &nbsp;&nbsp;
        <asp:HiddenField ID="hfRPF_ID_PREV" runat="server" /><asp:HiddenField ID="hfGRD_ID_PREV" runat="server" />
        <asp:HiddenField ID="hfACD_ID_PREV" runat="server" />
        <asp:HiddenField ID="hfRSM_ID" runat="server" />
        <asp:HiddenField ID="hfRPF_ID" runat="server" /><asp:HiddenField ID="hfRPF_DESCR" runat="server" />
        <asp:HiddenField ID="hfSchedule" runat="server" /><asp:HiddenField ID="hfBSU_ID" runat="server" />
        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfAccYear" runat="server" />
        <asp:HiddenField ID="hfRed" runat="server" />
        <asp:HiddenField ID="hfBlue" runat="server" />
        <asp:HiddenField ID="hfYellow" runat="server" />
        <asp:HiddenField ID="hfGreen" runat="server" />
        
        <asp:HiddenField ID="hfA1Color" runat="server" />
        <asp:HiddenField ID="hfA2Color" runat="server" />
        <asp:HiddenField ID="hfB1Color" runat="server" />
        <asp:HiddenField ID="hfB2Color" runat="server" />
        <asp:HiddenField ID="hfC1Color" runat="server" />
        <asp:HiddenField ID="hfC2Color" runat="server" />
        <asp:HiddenField ID="hfDColor" runat="server" />
        <asp:HiddenField ID="hfE1Color" runat="server" />
        <asp:HiddenField ID="hfE2Color" runat="server" />
         <asp:HiddenField ID="hfEcolor" runat="server" />
        
        <asp:HiddenField ID="hfACD_CURRENT" runat="server" />
        <asp:HiddenField ID="hfD1Color" runat="server" />
        <asp:HiddenField ID="hfD2Color" runat="server" />
    </form>
</body>
</html>
