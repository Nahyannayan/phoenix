Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports CURRICULUM
Partial Class Curriculum_ConsolidatedReports_clmConsolidatedLessonPlannerView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300240") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                ddlAcademicYear2 = studClass.PopulateAcademicYear(ddlAcademicYear2, Session("clm"), Session("sbsuid"))
                ddlAcademicYear3 = studClass.PopulateAcademicYear(ddlAcademicYear3, Session("clm"), Session("sbsuid"))
                ddlAcademicYear4 = studClass.PopulateAcademicYear(ddlAcademicYear4, Session("clm"), Session("sbsuid"))



                PopulateGrade1(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                lstSubject = PopulateSubjects1(lstSubject)
                PopulateMonths(ddlAcademicYear.SelectedValue.ToString, lstMonths)

                PopulateSubjects2()
                PopulateGrade2()
                PopulateMonths(ddlAcademicYear2.SelectedValue.ToString, lstMonths2)

                PopulateTeacher()
                PopulateMonths(ddlAcademicYear3.SelectedValue.ToString, lstMonths3)

                PopulateGrade1(ddlGrade4, ddlAcademicYear4.SelectedValue.ToString)
                PopulateSubjects4()
                PopulateMonths(ddlAcademicYear4.SelectedValue.ToString, lstMonths4)


            End If
        End If


    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Public Function PopulateGrade1(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + acdid


        str_query += " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Sub PopulateGrade2()
        lstGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY " _
                              & " ,GRM_GRD_ID,grd_displayorder FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " INNER JOIN SUBJECTS_GRADE_S ON GRM_GRD_ID=SBG_GRD_ID AND GRM_ACD_ID=SBG_ACD_ID AND SBG_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                              & " where grm_acd_id=" + ddlAcademicYear2.SelectedValue.ToString


        str_query += " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        lstGrade.DataSource = ds
        lstGrade.DataTextField = "grm_display"
        lstGrade.DataValueField = "grm_grd_id"
        lstGrade.DataBind()
    End Sub

    Function PopulateSubjects1(ByVal lstSubjects As CheckBoxList) As CheckBoxList
        lstSubjects.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If

        str_query += " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstSubjects.DataSource = ds
        lstSubjects.DataTextField = "SBG_DESCR"
        lstSubjects.DataValueField = "SBG_ID"
        lstSubjects.DataBind()
        Return lstSubjects
    End Function

    Sub PopulateSubjects4()
        lstSubject4.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If ddlGrade4.SelectedValue <> "" Then
            grade = ddlGrade4.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If

        str_query += " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstSubject4.DataSource = ds
        lstSubject4.DataTextField = "SBG_DESCR"
        lstSubject4.DataValueField = "SBG_ID"
        lstSubject4.DataBind()

    End Sub
    Sub PopulateSubjects2()
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_SBM_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + ddlAcademicYear2.SelectedValue.ToString


        str_query += " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub


   

    Sub PopulateMonths(ByVal acdid As String, ByVal lst As CheckBoxList)
        lst.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT MNAME,SDATE FROM dbo.GETACADEMICMONTHS('" + acdid + "')"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lst.DataSource = ds
        lst.DataTextField = "MNAME"
        lst.DataValueField = "SDATE"
        lst.DataBind()
    End Sub

    Sub PopulateTeacher()
        ddlTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim empId As Integer = studClass.GetEmpId(Session("sUsr_name"))
        Dim str_query As String
        Dim empTeacher As Boolean = studClass.isEmpTeacher(empId)

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                          & "emp_id FROM employee_m WHERE EMP_ECT_ID IN(1,3,4) and emp_bsu_id='" + Session("sBsuid") + "' AND EMP_STATUS IN(1,2) order by emp_fname,emp_mname,emp_lname"
        Else
            str_query = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                    & "emp_id FROM employee_m WHERE (EMP_ID=" + empId.ToString + " OR EMP_REPORTTO_EMP_ID=" + empId.ToString + ")"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTeacher.DataSource = ds
        ddlTeacher.DataTextField = "emp_name"
        ddlTeacher.DataValueField = "emp_id"
        ddlTeacher.DataBind()
        If empTeacher = False Then
            Dim li As New ListItem
            li.Text = "--"
            li.Value = 0
            ddlTeacher.Items.Insert(0, li)
        End If
    End Sub

    Function getSubjects() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstSubject.Items.Count - 1
            If lstSubject.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstSubject.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function

    Function getSubjects4() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstSubject4.Items.Count - 1
            If lstSubject4.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstSubject4.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function

   
    Function getMonths(ByVal lst As CheckBoxList) As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lst.Items.Count - 1
            If lst.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lst.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function

    Function getGrades() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstGrade.Items.Count - 1
            If lstGrade.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstGrade.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function

    Function getGroups() As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lstGroup.Items.Count - 1
            If lstGroup.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstGroup.Items(i).Value.ToString
            End If
        Next
        Return str
    End Function


    Sub PopulateGroups()
        lstGroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M " _
                             & " INNER JOIN GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID AND SGS_TODATE IS NULL" _
                             & " WHERE SGR_ACD_ID=" + ddlAcademicYear3.SelectedValue.ToString _
                             & " AND SGS_EMP_ID=" + ddlTeacher.SelectedValue.ToString _
                             & " ORDER BY SGR_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)
        lstGroup.DataSource = ds
        lstGroup.DataTextField = "SGR_DESCR"
        lstGroup.DataValueField = "SGR_ID"
        lstGroup.DataBind()
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade1(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        lstSubject = PopulateSubjects1(lstSubject)
        PopulateMonths(ddlAcademicYear.SelectedValue.ToString, lstMonths)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        lstSubject = PopulateSubjects1(lstSubject)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim url As String
        url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedLessonPlanner.aspx?MainMnu_code={0}&datamode={1}" _
                              & "&sbgids=" + Encr_decrData.Encrypt(getSubjects) _
                              & "&months=" + Encr_decrData.Encrypt(getMonths(lstMonths)) _
                              & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                              & "&year=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                              & "&grm=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                              & "&type=Grade", ViewState("MainMnu_code"), ViewState("datamode"))
        ResponseHelper.Redirect(url, "_blank", "")

    End Sub




    Protected Sub ddlAcademicYear2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear2.SelectedIndexChanged
        PopulateSubjects2()
        PopulateGrade2()
        PopulateMonths(ddlAcademicYear2.SelectedValue.ToString, lstMonths2)
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        PopulateGrade2()
    End Sub

    Protected Sub btnGenerateReport2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport2.Click
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim url As String
        url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedLessonPlanner.aspx?MainMnu_code={0}&datamode={1}" _
                              & "&grades=" + Encr_decrData.Encrypt(getGrades) _
                              & "&months=" + Encr_decrData.Encrypt(getMonths(lstMonths2)) _
                              & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear2.SelectedValue.ToString) _
                              & "&year=" + Encr_decrData.Encrypt(ddlAcademicYear2.SelectedItem.Text) _
                              & "&subject=" + Encr_decrData.Encrypt(ddlSubject.SelectedItem.Text) _
                              & "&sbmid=" + Encr_decrData.Encrypt(ddlSubject.SelectedValue.ToString) _
                              & "&type=Subject", ViewState("MainMnu_code"), ViewState("datamode"))
        ResponseHelper.Redirect(url, "_blank", "")
    End Sub

    Protected Sub ddlTeacher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTeacher.SelectedIndexChanged
        PopulateGroups()
    End Sub

    Protected Sub btnGenerateReport3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport3.Click
        Dim url As String
        url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedLessonPlanner.aspx?MainMnu_code={0}&datamode={1}" _
                                     & "&sgrids=" + Encr_decrData.Encrypt(getGroups) _
                                     & "&months=" + Encr_decrData.Encrypt(getMonths(lstMonths3)) _
                                     & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear2.SelectedValue.ToString) _
                                     & "&year=" + Encr_decrData.Encrypt(ddlAcademicYear2.SelectedItem.Text) _
                                     & "&teacher=" + Encr_decrData.Encrypt(ddlTeacher.SelectedItem.Text) _
                                     & "&empid=" + Encr_decrData.Encrypt(ddlTeacher.SelectedValue.ToString) _
                                     & "&type=Teacher", ViewState("MainMnu_code"), ViewState("datamode"))
        ResponseHelper.Redirect(url, "_blank", "")
    End Sub

    Protected Sub ddlGrade4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade4.SelectedIndexChanged
        PopulateSubjects4()
    End Sub

    Protected Sub ddlAcademicYear4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear4.SelectedIndexChanged
        PopulateGrade1(ddlGrade4, ddlAcademicYear4.SelectedValue.ToString)
        PopulateSubjects4()
    End Sub

    Protected Sub btnGenerateReport4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport4.Click
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim url As String
        url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedLessonPlanner.aspx?MainMnu_code={0}&datamode={1}" _
                              & "&sbgids=" + Encr_decrData.Encrypt(getSubjects4) _
                              & "&months=" + Encr_decrData.Encrypt(getMonths(lstMonths4)) _
                              & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear4.SelectedValue.ToString) _
                              & "&year=" + Encr_decrData.Encrypt(ddlAcademicYear4.SelectedItem.Text) _
                              & "&grm=" + Encr_decrData.Encrypt(ddlGrade4.SelectedItem.Text) _
                              & "&type=Status", ViewState("MainMnu_code"), ViewState("datamode"))
        ResponseHelper.Redirect(url, "_blank", "")
    End Sub
End Class
