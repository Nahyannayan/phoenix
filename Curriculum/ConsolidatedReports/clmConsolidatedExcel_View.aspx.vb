Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_ConsolidatedReports_clmConsolidatedExcel_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code1") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330270" And ViewState("MainMnu_code") <> "C330271") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights


                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Try
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()

                    BindPrintedFor()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)


                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")

                    If Session("CurrSuperUser") = "Y" Then
                        lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If
                    BindSection("")


                    BindHeader()

                    ' If ViewState("MainMnu_code") <> "C330271" Then
                    'trfilter.Visible = False
                    ' End If
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        End If

    End Sub

#Region "Private methods"
    Sub checkEmpFormtutor()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sctId As String = ""
        Dim grdId As String
        Dim stmId As String = ""
        Dim str_query As String = "SELECT ISNULL(SCT_ID,0),ISNULL(SCT_GRD_ID,''),ISNULL(GRM_STM_ID,0) " _
                                 & " FROM SECTION_M INNER JOIN GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE " _
                                 & "  SCT_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                 & " AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            sctId = reader.GetValue(0).ToString
            grdId = reader.GetString(1)
            stmId = reader.GetValue(2).ToString

            If grdId <> "" Then
                If Not ddlGrade.Items.FindByValue(grdId + "|" + stmId) Is Nothing Then
                    ddlGrade.ClearSelection()
                    ddlGrade.Items.FindByValue(grdId + "|" + stmId).Selected = True
                End If
                lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
                BindSection(sctId)

            End If
        End While

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M " _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindReportCardForFormTutor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M AS A" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS B ON RSM_ID=RSG_RSM_ID " _
                                & " INNER JOIN OASIS..SECTION_M AS C ON B.RSG_GRD_ID=C.SCT_GRD_ID AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'" _
                                & " WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub


    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                                & " ORDER BY RSG_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function


    Function PopulateSubjects(ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_SBM_ID NOT IN(57,58) AND SBG_ACD_ID=" + acd_id
        If ViewState("MainMnu_code") = "C300008" Then
            'str_query += " INNER JOIN ACT.ACTIVITY_SCHEDULE ON SBG_ID=CAS_SBG_ID AND SBG_PARENT_ID=0"
            str_query += "  AND SBG_PARENT_ID=0"

        End If


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As CheckBoxList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                    & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S "


        str_query += " WHERE SBG_ACD_ID=" + acd_id

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function getSubjects() As String
        Dim subs As String = ""
        Dim i As Integer

        For i = 0 To lstSubject.Items.Count - 1
            If lstSubject.Items(i).Selected = True Then
                If subs <> "" Then
                    subs += "|"
                End If
                subs += lstSubject.Items(i).Value
            End If
        Next
        Return subs

    End Function


    Sub BindSection(ByVal sctid As String)

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        If sctid <> "" Then
            str_query += " and sct_id=" + sctid
        End If
        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        If ViewState("MainMnu_code1") = "C330271" Then
            ddlSection.Items.Insert(0, li)
        End If

        If ViewState("MainMnu_code") = "C330270" Then
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindHeader()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'" _
                                & " AND RSD_bALLSUBJECTS=1 AND RSD_SBG_ID IS NULL ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstHeader.DataSource = ds
        lstHeader.DataTextField = "RSD_HEADER"
        lstHeader.DataValueField = "RSD_ID"
        lstHeader.DataBind()
    End Sub

#End Region

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindHeader()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
            lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection("")


    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
            BindReportCard()
      
        BindPrintedFor()
        BindHeader()
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
            If Session("CurrSuperUser") = "Y" Then
            lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
        BindSection("")

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim mnucode As String = ViewState("MainMnu_code1")

        ViewState("datamode") = Encr_decrData.Encrypt("add")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

        Dim sbg_ids As String = ""
        Dim rsd_ids As String = ""
        Dim i As Integer

        For i = 0 To lstSubject.Items.Count - 1
            If lstSubject.Items(i).Selected = True Then
                If sbg_ids <> "" Then
                    sbg_ids += "|"
                End If
                sbg_ids += lstSubject.Items(i).Value.ToString
            End If
        Next

        For i = 0 To lstHeader.Items.Count - 1
            If lstHeader.Items(i).Selected = True Then
                If rsd_ids <> "" Then
                    rsd_ids += "|"
                End If
                rsd_ids += lstHeader.Items(i).Value.ToString
            End If
        Next

        Dim url As String

        If mnucode = "C330271" Then
            Dim strtext As String = ddlAcademicYear.SelectedItem.Text + " - " + ddlGrade.SelectedItem.Text + " - " + ddlPrintedFor.SelectedItem.Text
            url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedExcel_Withpivot.aspx?MainMnu_code={0}&datamode={1}" _
                                & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                & "&rpf=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text) _
                                           & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                                           & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                                           & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                                           & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                                           & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                           & "&term=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text.Replace(" REPORT", "")) _
                                           & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                                           & "&sbg_ids=" + sbg_ids _
                                           & "&bCriteria=" + Encr_decrData.Encrypt(IIf(chkCriterias.Checked = True, "1", "0")) _
                                           & "&type=" + Encr_decrData.Encrypt(ddlType.SelectedItem.Text) _
                                           & "&strtext=" + Encr_decrData.Encrypt(strtext) _
                                           & "&rsd_ids=" + rsd_ids, ViewState("MainMnu_code"), ViewState("datamode"))
            ResponseHelper.Redirect(url, "_blank", "")
        Else


            If chkCriterias.Checked = True Then
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_ExportExcelWithCriterias.aspx?MainMnu_code={0}&datamode={1}" _
                                           & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                           & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                           & "&rpf=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text) _
                                           & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                                           & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                                           & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                                           & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                                           & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                           & "&term=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text.Replace(" REPORT", "")) _
                                           & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                                           & "&sbg_ids=" + sbg_ids _
                                           & "&rsd_ids=" + rsd_ids _
                                           & "&type=" + Encr_decrData.Encrypt(ddlType.SelectedItem.Text) _
                                           & "&bData=" + chkBasedata.Checked.ToString _
                                           & "&bTC=" + chkTc.Checked.ToString, ViewState("MainMnu_code"), ViewState("datamode"))

            Else
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_ExportExcel.aspx?MainMnu_code={0}&datamode={1}" _
                                             & "&rsmid=" + Encr_decrData.Encrypt(ddlReportCard.SelectedValue.ToString) _
                                             & "&rpfid=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedValue.ToString) _
                                             & "&rpf=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text) _
                                             & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                                             & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                                             & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                                             & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                                             & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                             & "&term=" + Encr_decrData.Encrypt(ddlPrintedFor.SelectedItem.Text.Replace(" REPORT", "")) _
                                             & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                                             & "&sbg_ids=" + sbg_ids _
                                             & "&rsd_ids=" + rsd_ids _
                                             & "&type=" + Encr_decrData.Encrypt(ddlType.SelectedItem.Text) _
                                             & "&bData=" + chkBasedata.Checked.ToString _
                                             & "&bTC=" + chkTc.Checked.ToString, ViewState("MainMnu_code"), ViewState("datamode"))
            End If


            ' ResponseHelper.Redirect(url, "_blank", "")
            ResponseHelper.Redirect(url, "_blank", "width=1,height=1")
        End If

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged

        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        If Session("CurrSuperUser") = "Y" Or Session("formtutor") = "Y" Then
            lstSubject = PopulateSubjects(lstSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            lstSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), lstSubject, ddlAcademicYear.SelectedValue.ToString)
        End If

        BindSection("")

    End Sub





End Class
