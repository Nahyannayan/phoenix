﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsolidated_ProgressTrackerView.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_ProgressTrackerView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_lstSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }


        function fnSelectAll_bkp(chkObj) {
            var multi = document.getElementById("<%=lstSubject.ClientID%>");
                if (multi != null && multi.options != null) {
                    if (chkObj.checked)
                        for (i = 0; i < multi.options.length; i++)
                            multi.options[i].selected = true;
                    else
                        for (i = 0; i < multi.options.length; i++)
                            multi.options[i].selected = false;
                }
            }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center"
                    cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" />
                        </td>
                        <td colspan="2">

                        </td>
                    </tr>
                    <tr>
                        
                        <td align="left" valign="middle" width="20%" ><span class="field-label">Grade</span>
                        </td>

                        <td align="left" valign="middle"  width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" Width="172px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" valign="middle"><span class="field-label"> Section</span>
                        </td>

                        <td align="left" width="30%" valign="middle">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" Width="105px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reportcard</span>
                        </td>

                        <td align="left" width="30%" colspan="3">
                            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Records Found" HeaderStyle-Height="30" PageSize="20" ShowFooter="True"
                                >
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle Wrap="False" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect1" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RPF_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRsmId" runat="server" Text='<%# Bind("RSM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RPF_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRpfId" runat="server" Text='<%# Bind("RPF_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ReportCard">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRpf" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Track Progress">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle Wrap="False" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkProgress" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="griditem" Height="30px" />
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle CssClass="Green" />
                                <AlternatingRowStyle CssClass="griditem" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSubject" runat="server">
                        <td align="left" valign="middle" width="20%"><span class="field-label">Subject</span>
                        </td>

                        <td align="left" valign="middle" width="30%" >
                <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server"
                    Text="Select All" />
                            <br />
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="lstSubject" runat="server" 
                                >
                            </asp:CheckBoxList></div>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="Tr1" runat="server">

                        <td align="left" width="20%" valign="middle"><span class="field-label">Include TC Students</span>
                        </td>

                        <td align="left" width="30%" valign="middle">
                            <asp:CheckBox ID="chkTc" runat="server" />
                        </td>

                        <td align="left" width="20%" valign="middle"><span class="field-label">Filter By</span>
                        </td>

                        <td align="left" width="30%" valign="middle" colspan="7">
                            <asp:DropDownList ID="ddlType" runat="server" Width="199px">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" valign="middle"><span class="field-label">BaseLine Level</span>
                        </td>

                        <td align="left" width="30%" valign="middle" >
                            <asp:DropDownList ID="ddlBaseLine" runat="server" >
                                <asp:ListItem>-</asp:ListItem>
                                <asp:ListItem>1a</asp:ListItem>
                                <asp:ListItem>1b</asp:ListItem>
                                <asp:ListItem>1c</asp:ListItem>
                                <asp:ListItem>2a</asp:ListItem>
                                <asp:ListItem>2b</asp:ListItem>
                                <asp:ListItem>2c</asp:ListItem>
                                <asp:ListItem>3a</asp:ListItem>
                                <asp:ListItem>3b</asp:ListItem>
                                <asp:ListItem>3c</asp:ListItem>
                                <asp:ListItem>4a</asp:ListItem>
                                <asp:ListItem>4b</asp:ListItem>
                                <asp:ListItem>4c</asp:ListItem>
                                <asp:ListItem>5a</asp:ListItem>
                                <asp:ListItem>5b</asp:ListItem>
                                <asp:ListItem>5c</asp:ListItem>
                                <asp:ListItem>6a</asp:ListItem>
                                <asp:ListItem>6b</asp:ListItem>
                                <asp:ListItem>6c</asp:ListItem>
                                <asp:ListItem>7a</asp:ListItem>
                                <asp:ListItem>7b</asp:ListItem>
                                <asp:ListItem>7c</asp:ListItem>
                                <asp:ListItem>8a</asp:ListItem>
                                <asp:ListItem>8b</asp:ListItem>
                                <asp:ListItem>8c</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnConsolidatedReport" runat="server" CssClass="button" 
                                Text="Consolidated Progress Report" ValidationGroup="groupM1" />
                            &nbsp;
                <asp:Button ID="btnAnalysisReport" runat="server" CssClass="button"
                    Text="Analysis Report" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

