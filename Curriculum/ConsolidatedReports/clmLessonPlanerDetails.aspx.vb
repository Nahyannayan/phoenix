Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_ConsolidatedReports_clmLessonPlanerDetails
    Inherits System.Web.UI.Page



#Region "Private Methods"
    Sub LoadReports(ByVal rptClass As rptClass)
        '   Try



        Dim iRpt As New DictionaryEntry


        Dim crParameterDiscreteValue As ParameterDiscreteValue
        Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        Dim crParameterFieldLocation As ParameterFieldDefinition
        Dim crParameterValues As ParameterValues

        With rptClass

            rs.ReportDocument.Load(.reportPath)

            Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
            myConnectionInfo.ServerName = .crInstanceName
            myConnectionInfo.DatabaseName = .crDatabase
            myConnectionInfo.UserID = .crUser
            myConnectionInfo.Password = .crPassword


            SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
            SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)



            crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
            If .reportParameters.Count <> 0 Then
                For Each iRpt In .reportParameters
                    crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                    crParameterValues = crParameterFieldLocation.CurrentValues
                    crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                    crParameterDiscreteValue.Value = iRpt.Value
                    crParameterValues.Add(crParameterDiscreteValue)
                    crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                Next
            End If


            If .selectionFormula <> "" Then
                rs.ReportDocument.RecordSelectionFormula = .selectionFormula
            End If


        End With
        '  Catch ex As Exception


        '  End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next

        myReportDocument.VerifyDatabase()

    End Sub
    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)
                    SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub


    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("accYear", hYear.Value.ToString)
        param.Add("grade", hGrade.Value.ToString)
        param.Add("subject", hSubject.Value.ToString)
        param.Add("@SBG_ID", "0")
        param.Add("@SYD_ID", hSYD_ID.Value.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Reports/Rpt/LESSONPLANNER/rptLessonPlanner.rpt")
        End With



        LoadReports(rptClass)
    End Sub

    Sub CallTeacherReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("accYear", hYear.Value)
        param.Add("grade", hGrade.Value)
        param.Add("subject", hSubject.Value)
        param.Add("@SBG_ID", hSBG_ID.Value)
        param.Add("@SGR_IDS", hSGR_ID.Value)
        param.Add("@SYD_IDS", hSYD_ID.Value)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Reports/Rpt/LESSONPLANNER/rptLessonPlanByTeacher.rpt")
        End With
        Session("rptClass") = rptClass
        LoadReports(rptClass)
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hSYD_ID.Value = Request.QueryString("syd_id")
        hYear.Value = Request.QueryString("year")
        hGrade.Value = Request.QueryString("grade")
        hSubject.Value = Request.QueryString("subject")
        If Request.QueryString("type") = "teacher" Then
            hSBG_ID.Value = Request.QueryString("sbg_id")
            hSGR_ID.Value = Request.QueryString("sgr_id")
            CallTeacherReport()
        Else
            CallReport()
        End If

    End Sub
End Class
