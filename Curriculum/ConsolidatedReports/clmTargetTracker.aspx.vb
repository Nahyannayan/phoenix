Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM

Partial Class Curriculum_Reports_Aspx_clmTargetTracker
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                h_MnuCode.Value = ViewState("MainMnu_code")
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400101" And ViewState("MainMnu_code") <> "C400109" And ViewState("MainMnu_code") <> "C400119") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)

                    Dim grade As String()
                    grade = ddlGrade.SelectedValue.Split("|")
                    BindSection()
                    BindReports()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + acdid + "' order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function getReportCards() As String
        Dim str As String = ""
        Dim i As Integer

        For i = 0 To lstReportSchedule.Items.Count - 1
            If lstReportSchedule.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstReportSchedule.Items(i).Text
            End If
        Next

        Return str
    End Function

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        param.Add("grade", ddlGrade.SelectedItem.Text)


        Dim grade As String() = ddlGrade.SelectedValue.Split("|")

        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_DESCR", getReportCards())
        param.Add("@STM_ID", grade(1))
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptINTLStudentPerformanceAcrossYear.rpt")
        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'" _
                                & " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstReportSchedule.DataSource = ds
        lstReportSchedule.DataTextField = "RPF_DESCR"
        lstReportSchedule.DataValueField = "RPF_ID"
        lstReportSchedule.DataBind()
    End Sub


    Sub BindSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlGrade.SelectedValue = "" Then
            ddlSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            'ddlSection.Items.Insert(0, li)
        End If
    End Sub

  
#End Region
    
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try

            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try

            Dim grade As String()
            grade = ddlGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("add")
           
            Session("targettracker") = 1


            Dim url As String

            Dim rpf_id1 As String
            Dim rpf_id2 As String

            Dim i As Integer

            Dim j As Integer = 0


            Dim rpf As String = ""

            For i = 0 To lstReportSchedule.Items.Count - 1
                If lstReportSchedule.Items(i).Selected = True Then
                    If rpf <> "" Then
                        rpf += ","
                    End If
                    If rpf_id1 = "" Then
                        rpf_id1 = lstReportSchedule.Items(i).Value
                        rpf += lstReportSchedule.Items(i).Text
                    Else
                        rpf_id2 = lstReportSchedule.Items(i).Value
                        rpf += lstReportSchedule.Items(i).Text
                    End If
                    j += 1
                End If
            Next

           

            If j > 2 Then
                lblError.Text = "A maximum of only 2 reports can be selected"
                Exit Sub
            End If

            If rpf_id2 = "" Then
                rpf_id2 = rpf_id1
                rpf_id1 = "0"
            End If

            If Session("sbsuid") = "125010" Or Session("sbsuid") = "135010" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)

                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedTracking_TWS.aspx?MainMnu_code={0}&datamode={1}" _
                       & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                       & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                       & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                       & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                       & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                       & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                       & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                       & "&term=" + Encr_decrData.Encrypt(rpf) _
                       & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                       & "&display=tracker", ViewState("MainMnu_code"), ViewState("datamode"))

            ElseIf h_MnuCode.Value = "C400101" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)

                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_CIS.aspx?MainMnu_code={0}&datamode={1}" _
                                 & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                                 & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                                 & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                                 & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                                 & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                                 & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                                 & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                                 & "&term=" + Encr_decrData.Encrypt(rpf) _
                                 & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                                 & "&display=tracker", ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf h_MnuCode.Value = "C400119" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)

                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedTracking_ICSE.aspx?MainMnu_code={0}&datamode={1}" _
                           & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                           & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                           & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                           & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                           & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                           & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                           & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                           & "&term=" + Encr_decrData.Encrypt(rpf) _
                           & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                           & "&display=tracker", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)

                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedTracking_CBSE.aspx?MainMnu_code={0}&datamode={1}" _
                       & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                       & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                       & "&grdid=" + Encr_decrData.Encrypt(ddlGrade.SelectedValue.ToString) _
                       & "&grade=" + Encr_decrData.Encrypt(ddlGrade.SelectedItem.Text) _
                       & "&sctid=" + Encr_decrData.Encrypt(ddlSection.SelectedValue.ToString) _
                       & "&section=" + Encr_decrData.Encrypt(ddlSection.SelectedItem.Text) _
                       & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                       & "&term=" + Encr_decrData.Encrypt(rpf) _
                       & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                       & "&display=tracker", ViewState("MainMnu_code"), ViewState("datamode"))

            End If

            ResponseHelper.Redirect(url, "_blank", "")

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

End Class
