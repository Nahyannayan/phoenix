Imports System.Data.SqlClient
Imports CURRICULUM
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports system
Imports ActivityFunctions
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_ConsolidatedReports_clmLessonPlannerDocuments
    Inherits System.Web.UI.Page


    Sub bind_List()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "select SDD_DOC_ID as id,SD_FILENAME as FNAME  from syl.SYLLABUS_D_DOC inner join OASIS_DOCS.dbo.SYLLABUS_DOC " _
                                  & " on SD_ID=SDD_DOC_ID where SDD_SYD_ID =" + h_TopicID.Value




        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        dlDocs.DataSource = ds

        dlDocs.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        h_TopicID.Value = Request.QueryString("topicid")
        lblAccYear.Text = Request.QueryString("acy")
        lblGrade.Text = Request.QueryString("grade")
        lblSubject.Text = Request.QueryString("subject")
        lblTopic.Text = Request.QueryString("topic")
        bind_List()
    End Sub

    Protected Sub dlDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlDocs.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim txtfile As LinkButton = e.Item.FindControl("txtfile")
                Dim img As ImageButton = e.Item.FindControl("imgF")
                Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
                If s = ".xls" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".xlsx" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".doc" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".docx" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".pdf" Then
                    img.ImageUrl = "~/Curriculum/images/pdf.png"
                Else
                    img.ImageUrl = "~/Curriculum/images/img.png"
                End If


                '    Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                '     ScriptManager1.RegisterPostBackControl(txtfile)
                '   ScriptManager1.RegisterPostBackControl(img)


            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub
    Protected Sub imgF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If
    End Sub
    Public Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConnectionManger.GetOASIS_DOCSConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("sd_doc"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("sd_filename").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("sd_Content").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

End Class
