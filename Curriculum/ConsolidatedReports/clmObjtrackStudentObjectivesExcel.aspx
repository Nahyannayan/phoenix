<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmObjtrackStudentObjectivesExcel.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmObjtrackStudentObjectivesExcel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    
       
    <asp:Panel id="divExport" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%" id="tbExport" runat="server">
     <tr>
            <td align="center">
            <asp:Label ID="lblBsu" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt"
            Text="Label"></asp:Label></td>
     </tr>
     <tr>
            <td align="center">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="12pt"
            Text="STUDENT OBJECTIVES"></asp:Label></td>
     </tr>
     <tr><td align="center">
         <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Arial"  Font-Size="10pt" ></asp:Label></td></tr>
         <tr>
         <td>
         <asp:GridView ID="gvObjectives" AllowPaging="false" AllowSorting="false" runat="server" Font-Size="X-Small" HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" Font-Names="Arial">
        <RowStyle HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" />
        <AlternatingRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" />
         <HeaderStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" BackColor="CORNSILK"  />
    </asp:GridView>
         
         </td>
         </tr>
   
    </table>
    
 
    <br />
    
       </asp:Panel>
    
       <asp:HiddenField ID="h_SBM_ID" runat="server" />
    <asp:HiddenField ID="h_SGR_ID" runat="server" />
    <asp:HiddenField ID="h_LVL_ID" runat="server" />
    <asp:HiddenField ID="h_OBT_ID" runat="server" />
    <asp:HiddenField ID="h_filter" runat="server" />
    </form>
</body>
</html>
