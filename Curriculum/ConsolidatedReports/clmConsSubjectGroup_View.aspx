<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsSubjectGroup_View.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsSubjectGroup_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Subject Groups</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>

                        <td align="center" colspan="8">

                            <table id="Table1" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">

                                        <table id="Table2" runat="server" align="center" width="100%" cellpadding="5" cellspacing="0">

                                            <tr>
                                                <td colspan="4" align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Select AcademicYear</span></td>

                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>
                                            </tr>

                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:GridView ID="gvOption" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" BorderStyle="None">
                                                        <RowStyle CssClass="griditem" />
                                                        <EmptyDataRowStyle  />

                                                        <Columns>

                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblConId" runat="server" Text='<%# Bind("CON_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Grade">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDescr" runat="server" Text='<%# Bind("CON_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Short Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShort" runat="server" Text='<%# Bind("CON_SHORT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:ButtonField>

                                                        </Columns>

                                                        <SelectedRowStyle />
                                                        <HeaderStyle   />
                                                        <EditRowStyle  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative"  />

                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                            runat="server" type="hidden" value="=" />

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>


</asp:Content>

