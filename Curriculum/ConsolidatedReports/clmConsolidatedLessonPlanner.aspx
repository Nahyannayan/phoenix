﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmConsolidatedLessonPlanner.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidatedLessonPlanner" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/> 
    
    <title>Untitled Page</title>
</head>
<body>
    
    <form id="form1" runat="server">
        <div>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:Label ID="lblBsu" runat="server" CssClass="field-label"
                            Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="Label1" runat="server" CssClass="field-label"
                            Text="Consolidated Lesson Planner By Grade"></asp:Label></td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblTitle" runat="server" CssClass="field-label" ></asp:Label></td>
                </tr>
                <tr>
                    <td align="center">
                        <br />
                        <asp:GridView ID="gvSubj" runat="server" Font-Size="X-Small" HorizontalAlign="Center" CssClass="table table-bordered table-row">
                            <RowStyle HorizontalAlign="Center"  />
                            <AlternatingRowStyle  />
                            <HeaderStyle  />
                        </asp:GridView>
                    </td>
                </tr>
            </table>

        </div>
        <asp:HiddenField ID="hSBG_IDs" runat="server" />
        <asp:HiddenField ID="hMonths" runat="server" />
        <asp:HiddenField ID="hType" runat="server" />
        <asp:HiddenField ID="hACD_ID" runat="server" />
        <asp:HiddenField ID="hGRD_IDs" runat="server" />
        <asp:HiddenField ID="hSGR_IDs" runat="server" />
        <asp:HiddenField ID="hSBM_ID" runat="server" />
        <asp:HiddenField ID="hEMP_ID" runat="server" />
        <asp:HiddenField ID="hLogin" runat="server" />
        <asp:HiddenField ID="hYear" runat="server" />
        <asp:HiddenField ID="hGrade" runat="server" />
        <asp:HiddenField ID="hSubject" runat="server" />
    </form>
</body>
</html>
