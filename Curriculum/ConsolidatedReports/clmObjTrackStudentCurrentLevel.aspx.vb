﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_ConsolidatedReports_clmObjTrackStudentCurrentLevel
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
        hfSBM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
        hfSCT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
        hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
        hfShow.Value = Encr_decrData.Decrypt(Request.QueryString("showstudents").Replace(" ", "+"))
        lblBsu.Text = Session("bsu_name")
        lblTitle.Text = Encr_decrData.Decrypt(Request.QueryString("class").Replace(" ", "+"))
        GetData()
    End Sub

    Sub SetGridColor(ByVal row As GridViewRow)
        Dim i As Integer
        Dim level As String
        Dim hdrColumns As Hashtable = ViewState("hdrColumns")
        Dim redColor As String
        Dim grayColor As String
        Dim greenColor As String
        redColor = "#F5A6AF"
        grayColor = "#E6E6E6"
        greenColor = "#9CEFB3"
        row.Cells(0).Wrap = False
        row.Cells(0).BackColor = Drawing.Color.LightYellow
        row.Cells(1).BackColor = Drawing.Color.LightYellow
        row.Cells(0).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;font-family: 'Nunito', sans-serif;")
        row.Cells(1).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;font-family: 'Nunito', sans-serif;")
        For i = 2 To row.Cells.Count - 1
            row.Cells(i).VerticalAlign = VerticalAlign.Top
            level = hdrColumns.Item(i)
            If hfGRD_ID.Value = "01" Then
                Select Case level
                    Case "w", "w+"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + redColor)
                    Case "1a", "1b", "1c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;background:" + greenColor)
                End Select
            ElseIf hfGRD_ID.Value = "02" Then
                Select Case level
                    Case "w", "w+", "1a", "1b", "1c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + redColor)
                    Case "2b", "2c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + greenColor)
                End Select
            ElseIf hfGRD_ID.Value = "03" Then
                Select Case level
                    Case "w", "w+", "1a", "1b", "1c", "2c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + redColor)
                    Case "2b", "2a"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + greenColor)
                End Select
            ElseIf hfGRD_ID.Value = "04" Then
                Select Case level
                    Case "w", "w+", "1a", "1b", "1c", "2c", "2b", "2a"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + redColor)
                    Case "3b", "3c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + greenColor)
                End Select
            ElseIf hfGRD_ID.Value = "05" Then
                Select Case level
                    Case "w", "w+", "1a", "1b", "1c", "2c", "2b", "2a", "3c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;top;border=1;border-color:#000000;background:" + redColor)
                    Case "3b", "3a"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + greenColor)
                End Select
            ElseIf hfGRD_ID.Value = "06" Then
                Select Case level
                    Case "w", "w+", "1a", "1b", "1c", "2c", "2b", "2a", "3c", "3b", "3a"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;top;border=1;border-color:#000000;background:" + redColor)
                    Case "4a", "4b", "4c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + greenColor)
                End Select
            ElseIf hfGRD_ID.Value = "07" Then
                Select Case level
                    Case "w", "w+", "1a", "1b", "1c", "2c", "2b", "2a", "3c", "3b", "3a", "4b", "4c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;top;border=1;border-color:#000000;background:" + redColor)
                    Case "4a", "5c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + greenColor)
                End Select

            ElseIf hfGRD_ID.Value = "08" Then
                Select Case level
                    Case "w", "w+", "1a", "1b", "1c", "2c", "2b", "2a", "3c", "3b", "3a", "4b", "4c", "4a", "5c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;top;border=1;border-color:#000000;background:" + redColor)
                    Case "5a", "5b"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + greenColor)
                End Select
            ElseIf hfGRD_ID.Value = "09" Then
                Select Case level
                    Case "w", "w+", "1a", "1b", "1c", "2c", "2b", "2a", "3c", "3b", "3a", "4b", "4c", "4a", "5c", "5b"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;top;border=1;border-color:#000000;background:" + redColor)
                    Case "5a", "6c"
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + grayColor)
                    Case Else
                        row.Cells(i).Attributes.Add("Style", "font-family: 'Nunito', sans-serif;border=1;border-color:#000000;background:" + greenColor)
                End Select
            End If
        Next
    End Sub
    Sub GetData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC OBJTRACK.rptSTUDENTCURRENTLEVELESREPORT " + hfACD_ID.Value + ",'" + hfGRD_ID.Value + "','" + hfSCT_ID.Value + "','" + hfSBM_ID.Value + "','" + hfShow.Value + "'"
        Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = DS.Tables(0)
        Dim dtdes As DataTable
        dtdes = PivotTable(dt)
        gvSubj.DataSource = dtdes
        gvSubj.DataBind()
    End Sub
    Public Function PivotTable(ByVal src As DataTable) As DataTable

        Dim hdrColumns As New Hashtable

        Dim dest As New DataTable("Pivoted" + src.TableName)

        dest.Columns.Add(" ")
        dest.Columns.Add("  ")
        Dim r As DataRow
        Dim key As Integer = 2
        For Each r In src.Rows
            If dest.Columns.IndexOf(r(0).ToString()) <= 0 Then
                dest.Columns.Add(r(0).ToString())
                hdrColumns.Add(key, r(0).ToString())
                key += 1
            End If
        Next r

        ViewState("hdrColumns") = hdrColumns
        Dim i As Integer
        Dim j As Integer

        Dim dr As DataRow
        Dim rows As Integer = src.Rows.Count / (dest.Columns.Count - 2)
        Dim rowcount As Integer = 0
        For i = 0 To rows - 1
            dr = dest.NewRow
            dr.Item(0) = src.Rows(rowcount).Item(1)
            dr.Item(1) = src.Rows(rowcount).Item(2)

            For j = 2 To dest.Columns.Count - 1
                If rowcount < (src.Rows.Count) Then
                    dr.Item(j) = src.Rows(rowcount).Item(3).ToString.Replace(",", "," + vbCrLf)
                    rowcount += 1
                End If
            Next
            dest.Rows.Add(dr)
        Next i

        dest.AcceptChanges()
        Return dest
    End Function

    Protected Sub gvSubj_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubj.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Or e.Row.RowType = DataControlRowType.DataRow Then
            SetGridColor(e.Row)
        End If
    End Sub
End Class
