<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmConsolidatedExcel_Withpivot.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidatedExcel_Withpivot" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div><asp:Label ID="lblText" runat="Server" Font-Bold="true" Font-Size="Medium"></asp:Label></div>
    <div>
        
        <asp:GridView ID="gvSubj" runat="server" Font-Size="X-Small" HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" Font-Names="Arial">
        <RowStyle HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" />
        <AlternatingRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" />
         </asp:GridView>
         
           <asp:HiddenField ID="hfRowSpan" runat="server" />
        <br />
        <asp:HiddenField ID="hfRSD_IDS" runat="server" /><asp:HiddenField ID="hfRecordNumber" runat="server" Visible="False" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfGRM_DISPLAY" runat="server" />
        <asp:HiddenField ID="hfSCT_ID" runat="server" />
        <asp:HiddenField ID="hfSTM_ID" runat="server" />
        <asp:HiddenField ID="hfRSM_ID" runat="server" />
        <asp:HiddenField ID="hfRPF_ID" runat="server" /><asp:HiddenField ID="hfRPF_DESCR" runat="server" />
        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfSBG_IDS" runat="server" />
        <asp:HiddenField ID="hfbFinal" runat="server" />
         <asp:HiddenField ID="hfType" runat="server" />
          <asp:HiddenField ID="hbCriteria" runat="server" />
    </form>
</body>
</html>
