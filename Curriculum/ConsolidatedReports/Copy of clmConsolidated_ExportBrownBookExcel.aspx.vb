Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_ExportBrownBookExcel
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
            'hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))

            If Session("PromotionSheet") = 1 Then
                hfPromotionSheet.Value = 1
            Else
                hfPromotionSheet.Value = 0
            End If

            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)

            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
            ' hfAccYear.Value = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
            'hfTERM.Value = Encr_decrData.Decrypt(Request.QueryString("term").Replace(" ", "+"))
            ' hfGRM_DISPLAY.Value = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
          
            hfSCT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+"))

            '  hfRPF_DESCR.Value = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))

            Dim hP As New Hashtable
            ViewState("hP") = hP

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            'calling pageright class to get the access rights


            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights
            hfBSU_ID.Value = Session("sbsuid")
            GetACDCurrent()
            BindBrownbookDate()
            hfRecordNumber.Value = 0
            hfTableColumns.Value = 0
            hfPageNumber.Value = 0
            GetConsolidated(grd_id(0), hfSCT_ID.Value, grd_id(1))



        End If
    End Sub
#Region "Private Methods"
    Sub BindBrownbookDate()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(ACD_BROWNBOOKDATE,getdate()) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value
        hfBrownBookDate.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub
    Sub GetACDCurrent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(ACD_CURRENT,'false') FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value
        hfACD_Current.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetConsolidated(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim Subjects As String(,) = GetConsolidatedSubjects(grd_id, hfACD_ID.Value, stm_id)
        Dim rptHeaders As String(,) ' = GetReportHeaders(hfRSM_ID.Value)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        If hfPromotionSheet.Value = 1 Then
            sb.AppendLine("<HTML><HEAD><TITLE>::::PROMOTION SHEET ::::</TITLE>")
        Else
            sb.AppendLine("<HTML><HEAD><TITLE>:::: CONSOLIDATED REPORT ::::</TITLE>")
        End If

        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<BODY>")

        sb.AppendLine(GetConsolidatedForSection(grd_id, sct_id, hfACD_ID.Value, Subjects, rptHeaders))

        'ltBrownBook.Text = sb.ToString

        sb.AppendLine("</BODY></HTML>")
        'Response.Write(sb.ToString())
        Dim htmlString As String = sb.ToString
        Dim htmlwords As String() = htmlString.Split("*")
        Dim k As Integer = 0
        Dim p As Integer
        p = htmlwords.Length
        While k < p
            If htmlwords(k).EndsWith("_G") = True Or htmlwords(k).EndsWith("_M") = True Then
                htmlString = htmlString.Replace("*" + htmlwords(k) + "*", "-")
            End If
            k += 1
        End While
        '  Response.Write(htmlString)

        Response.AppendHeader("Content-Disposition", "attachment; filename=" + hfGRD_ID.Value + ViewState("sctdesc") + ".xls")
        Response.ContentType = "application/ms-excel"
        Response.Write(htmlString)
        Response.End()
    End Sub

    Function GetConsolidatedSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        If Session("SBSUID") <> "125011" Then
            str_query = "SELECT SBG_DESCR,SBG_ID,ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR,SBG_ORDER,SBG_SHORTCODE,'0' AS OPTIONAL FROM SUBJECTS_GRADE_S " _
                                   & " WHERE SBG_GRD_ID='" + grd_id + "' AND SBG_ACD_ID=" + acd_id _
                                   & " AND SBG_STM_ID=" + stm_id + "AND SBG_PARENT_ID=0"



            str_query = " SELECT * FROM (" + str_query + ")P ORDER BY SBG_DESCR"
        Else
            str_query = "SELECT SBG_DESCR,SBG_ID,ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR,SBG_ORDER,SBG_SHORTCODE,'0' AS OPTIONAL FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_GRD_ID='" + grd_id + "' AND SBG_ACD_ID=" + acd_id _
                                 & " AND SBG_STM_ID=" + stm_id
        End If



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count
        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 4)

        For j = 0 To i - 1
            With ds.Tables(0)
                Subjects(j, 0) = .Rows(j).Item(0).ToString
                Subjects(j, 1) = .Rows(j).Item(1).ToString
                Subjects(j, 2) = .Rows(j).Item(2).ToString
                Subjects(j, 3) = .Rows(j).Item(4).ToString
                Subjects(j, 4) = .Rows(j).Item(5).ToString


            End With
        Next
        Return Subjects
    End Function

      Function GetSubjectsHeader(ByVal Subjects(,) As String, ByVal rptHeaders As String(,)) As String
        Dim sb As New StringBuilder
        Dim i As Integer

        Dim strSubjects As String = ""
        Dim strHeader As String = ""
        Dim strSubHeader As String = ""
        Dim strShort1 As String = ""
        Dim strShort As String = ""
        Dim imgUrl As String = ""
        Dim colSpan As Integer = 0
        Dim colSpan1 As Integer = 0
        Dim j As Integer

        Dim imageLib As ImageCreationLibrary
        Dim imgFilePath As String = ""


        hfTableColumns.Value = 0
        For i = 0 To Subjects.GetLength(0) - 1
            'if major subject add one more column
            'If Subjects(i, 2).ToLower = "true" Then
            colSpan = 0
            'option shortcode heading
            If Subjects(i, 4) = 1 Then
                strShort += "<td width=""20px"" align=""middle"" Class=repcolDetail>" + Subjects(i, 3) + "</td>"
                colSpan1 += 1
            End If

            strSubHeader += "<td width=""5%"" align=""middle""    Class=repcolDetail>Mark</td>"
            strSubHeader += "<td width=""5%"" align=""middle""    Class=repcolDetail>Grade</td>"
            hfTableColumns.Value += 2
            colSpan += 2

            strSubjects += "<td colspan=" + colSpan.ToString + " align=""middle"" valign=""bottom""  >" + Subjects(i, 0) + "</td>"
            hfTableColumns.Value += 1
            'Else
            '    strSubjects += "<td width=""5%"" align=""middle"" valign=""bottom""  rowspan=""2"" >" + Subjects(i, 3) + "</td>"
            '    hfTableColumns.Value += 1
            'End If

        Next



        sb.AppendLine("<table border=""1""  bordercolorlight=""#000000""  align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font:NORMAL NORMAL 6pt Verdana, arial, Helvetica, sans-serif"" >")

        sb.AppendLine("<tr >")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center> Sr. No  </td>")
        sb.AppendLine("<td width=""50"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center> Admin. No  </td>")
        sb.AppendLine("<td  width=""20%"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center>Name of the Student</td>")
       
        If strShort <> "" Then
            sb.AppendLine("<td valign=""middle""  colspan=" + colSpan1.ToString + " align=""middle"" Class=repcolDetail>&nbsp;</td>")
        End If

        hfTableColumns.Value += 5
        sb.AppendLine(strSubjects)

      
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center>Result</td>")
        hfTableColumns.Value += 7

        sb.AppendLine("</tr>")


        sb.AppendLine("<tr>")
        sb.AppendLine(strSubHeader)
        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function
    Public Function checkUpper(ByVal str As String) As Boolean
        If Strings.StrComp(str, Strings.UCase(str)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetConsolidatedForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,), ByVal rptHeaders As String(,)) As String
        Dim sb As New StringBuilder
        Dim strHeader As String = GetConsolidatedHeader(sct_id)
        ' sb.AppendLine(strHeader)
        sb.AppendLine("<table   align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

        sb.AppendLine("<tr><td algn=center>")
        sb.AppendLine(strHeader)
        sb.AppendLine("</td></tr>")

        sb.AppendLine("<tr><td algn=center>")

        sb.AppendLine(GetSubjectsHeader(Subjects, rptHeaders))

        sb.AppendLine(GetStudentMarks(sct_id, Subjects, rptHeaders))

        sb.AppendLine("</table>")

        sb.AppendLine("</td></tr>")
        sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetConsolidatedHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_BB_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table   align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=bottom>")


            sb.AppendLine("<table width=""90%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">")
            sb.AppendLine("<TR >")
            sb.AppendLine("<TD>GRADE :    " + reader("GRM_DISPLAY") + "   &nbsp;&nbsp; SECTION : " + reader("SCT_DESCR") + " </TD> </TR>")

            sb.AppendLine("</Table>")

            sb.AppendLine("</TD>")


            sb.AppendLine("<TD WIDTH=40% VALIGN=TOP>")

            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font style=""text-decoration:NONE;font:bold bold 14pt Times New Roman,Verdana, arial, Helvetica, sans-serif"">" + reader("BSU_NAME"))

            sb.AppendLine("<BR>" + hfRPF_DESCR.Value.ToUpper + " (" + reader("ACY_DESCR") + ")</TD></TR>")



            sb.AppendLine("</table>")

            sb.AppendLine("</TD>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=TOP ALIGN=""RIGHT"">")
            sb.AppendLine("<table width=""80%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif"">&nbsp;</TD></TR>")
            sb.AppendLine("</table>")
            sb.AppendLine("</TD>")

            sb.AppendLine("</TR>")

            sb.AppendLine("<TR><TD colspan=2><font style=""text-decoration:NONE;font:bold bold 8Pt Verdana, arial, Helvetica, sans-serif"">&nbsp;&nbsp;&nbsp;&nbsp; Class Teacher :  " + reader("EMP_NAME") + " </TD>")
            sb.AppendLine("<TD align=right><font style=""text-decoration:NONE;font:bold bold 8pt Verdana, arial, Helvetica, sans-serif""> Date : " + Now.Date + " </TD>")
            sb.AppendLine("</TR>")
            sb.AppendLine("</Table><BR>")

            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function


    Function GetStudentMarks(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal rptHeaders(,) As String)
        Dim strTable As String

        ' Dim strFooter As String = GetConsolidatedFooter(Subjects, rptHeaders)

        Dim strStudents As String = GetStudents(sct_id, Subjects, rptHeaders)


        strTable = strStudents


        'If hfLastRecord.Value = 0 Then

        '    strTable += strFooter
        'End If

        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim id_M As String = ""
        Dim id_G As String = ""
        Dim id_Count As String = ""
        Dim id_HC As String = ""
        Dim id_SUBAVG As String = ""
        Dim i As Integer = 0

        Dim strStuId As String = ""
        Dim strRetestSubjects As String = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String


        Dim str_query As String

        str_query = "EXEC CBSE.getBROWNBOOKMARKS " _
                   & hfACD_ID.Value + "," _
                   & "'" + hfGRD_ID.Value + "'," _
                   & sct_id


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim p As Integer = ds.Tables(0).Rows.Count

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_M = "*" + .Item("FTM_STU_ID").ToString + "_" + .Item("FTM_SBG_ID").ToString + "_M*"
                id_G = "*" + .Item("FTM_STU_ID").ToString + "_" + .Item("FTM_SBG_ID").ToString + "_G*"
                ' grade = IIf(.Item("RST_GRADING") = "", "&nbsp;", .Item("RST_GRADING").ToString.Replace(".00", ""))
                strTable = strTable.Replace(id_M, IIf(.Item("FTM_MARK").ToString.Replace(".000", "") = "0", "-", .Item("FTM_MARK").ToString.Replace(".000", "")))
                strTable = strTable.Replace(id_G, IIf(.Item("FTM_GRADE").ToString = "", "-", .Item("FTM_GRADE").ToString))
              
            End With
        Next

        While i < p
            With ds.Tables(0).Rows(i)
                id_M = "*" + .Item("STU_ID").ToString + "_" + .Item("SBG_ID").ToString + "_" + .Item("RSD_ID").ToString + "_M*"
                id_G = "*" + .Item("STU_ID").ToString + "_" + .Item("SBG_ID").ToString + "_" + .Item("RSD_ID").ToString + "_G*"
                grade = IIf(.Item("RST_GRADING") = "", "&nbsp;", .Item("RST_GRADING").ToString.Replace(".00", ""))
                If .Item("RSD_TYPE") = "MARK" Then
                    strTable = strTable.Replace(id_M, grade)
                ElseIf .Item("RSD_TYPE") = "GRADE" Then
                    strTable = strTable.Replace(id_G, grade)
                Else
                    strTable = strTable.Replace(id_G, grade)
                End If
            End With
            i += 1
        End While


        'str_query = "EXEC [CBSE].[GETCONSOLIDATEDMARKS_MINORSUBJECTS] " _
        '           & hfACD_ID.Value + "," _
        '           & "'" + hfGRD_ID.Value + "'," _
        '           & hfRSM_ID.Value + "," _
        '           & hfRPF_ID.Value + "," _
        '           & sct_id + "," _
        '           & hfbFinal.Value


        '' ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        '' p() '= ds.Tables(0).Rows.Count - 1
        'i = 0

        'While i < p
        '    With ds.Tables(0).Rows(i)
        '        id_G = "*" + .Item("STU_ID").ToString + "_" + .Item("SBG_ID").ToString + "_G*"
        '        id_Count = "count_" + .Item("SBG_ID").ToString + "_" + .Item("RST_GRADING")
        '        If .Item("RSD_TYPE") = "MARK" Then
        '            strTable = strTable.Replace(id_M, .Item("RST_GRADING"))
        '        Else
        '            id_Count = "count_" + .Item("SBG_ID").ToString + "_" + .Item("RST_GRADING")


        '            strTable = strTable.Replace(id_G, .Item("RST_GRADING"))
        '            strTable = strTable.Replace(id_Count, .Item("GRADECOUNT"))
        '        End If
        '    End With
        '    i += 1
        'End While



        Return strTable
    End Function

    Function GetHousePoints(ByVal marks As Integer)
        Dim hPoints As String = "&nbsp;"
        Select Case hfGRD_ID.Value
            Case "09"
                Select Case marks
                    Case 900 To 1000
                        hPoints = "+18"
                    Case 800 To 899
                        hPoints = "+15"
                    Case 700 To 799
                        hPoints = "+12"
                    Case 600 To 699
                        hPoints = "+9"
                    Case 500 To 599
                        hPoints = "+6"
                    Case 400 To 499
                        hPoints = "+3"
                    Case Else
                        hPoints = "0"
                End Select


            Case "07", "08", "10"
                Select Case marks
                    Case 810 To 900
                        hPoints = "+18"
                    Case 720 To 809
                        hPoints = "+15"
                    Case 630 To 719
                        hPoints = "+12"
                    Case 540 To 629
                        hPoints = "+9"
                    Case 450 To 539
                        hPoints = "+6"
                    Case 360 To 449
                        hPoints = "+3"
                    Case Else
                        hPoints = "0"
                End Select

            Case "11"
                Select Case marks
                    Case marks >= 540 And marks <= 600
                        hPoints = "+18"
                    Case marks >= 480 And marks <= 539
                        hPoints = "+15"
                    Case marks >= 420 And marks <= 479
                        hPoints = "+12"
                    Case marks >= 360 And marks <= 419
                        hPoints = "+9"
                    Case marks >= 300 And marks <= 359
                        hPoints = "+6"
                    Case marks >= 240 And marks <= 299
                        hPoints = "+3"
                    Case marks < 240
                        hPoints = "0"
                End Select
        End Select

        Return hPoints.ToString
    End Function


    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String, ByVal rptHeaders As String(,)) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()
        Dim strMain As String
        Dim strSub As String

        Dim strRedLine As String

        Dim str_query As String


        If hfACD_Current.Value.ToLower = "true" Then
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                                   & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                                   & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                                   & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                                   & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                                   & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                                   & " STU_ID,CASE ISNULL(CTY_SHORT,'') WHEN 'UAE' THEN 'L' ELSE '&nbsp;' END AS NATIONALITY,ISNULL(FRR_RESULT,'PASSED') AS RESULT FROM STUDENT_M AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                                   & " INNER JOIN RPT.FINAL_RESULT_S ON STU_ID=FRR_STU_ID AND FRR_ACD_ID=STU_ACD_ID" _
                                   & " LEFT OUTER JOIN OASIS..COUNTRY_M AS D ON A.STU_NATIONALITY=D.CTY_ID" _
                                   & " WHERE STU_SCT_ID=" + sct_id + "  AND STU_CURRSTATUS<>'CN' " _
                                   & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "' AND STU_MINLIST='REGULAR'" _
                                   & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"


        Else
            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                           & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                           & " ISNULL(STU_BLUEID,'&nbsp') AS STU_BLUEID, " _
                           & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                           & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                           & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                           & " STU_ID,CASE ISNULL(CTY_SHORT,'') WHEN 'UAE' THEN 'L' ELSE '&nbsp;' END AS NATIONALITY,ISNULL(FRR_RESULT,'PASSED') AS RESULT FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                           & " INNER JOIN RPT.FINAL_RESULT_S ON STU_ID=FRR_STU_ID AND FRR_ACD_ID=STU_ACD_ID" _
                           & " LEFT OUTER JOIN OASIS..COUNTRY_M AS D ON A.STU_NATIONALITY=D.CTY_ID" _
                           & " WHERE STU_SCT_ID=" + sct_id + "  AND STU_CURRSTATUS<>'CN' " _
                           & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>'" + Format(Date.Parse(hfBrownBookDate.Value), "yyyy-MM-dd") + "' AND STU_MINLIST='REGULAR'" _
                           & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"


        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count


        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If
        Dim p As Integer = ds.Tables(0).Rows.Count
        Dim x As Integer = Subjects.GetLength(0)
        i = 0

        While i < p
            With ds.Tables(0).Rows(i)

                stuNames = .Item(0).ToString.Replace("  ", " ").Split(" ")
                sb.AppendLine("<tr height=""16px"">")
                sb.AppendLine("<td valign=top align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td valign=top align=""left"" Class=repcolDetail><CENTER>" + .Item("STU_BLUEID") + "</td>")
                If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                    sb.AppendLine("<td valign=top align=""left"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                Else
                    sb.AppendLine("<td valign=top align=""left"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                End If

                If sct_id = "0" Then
                    sb.AppendLine("<td valign=top align=""left"" Class=repcolDetail>&nbsp;" + hfGRM_DISPLAY.Value + " " + .Item("SCT_DESCR").ToString.ToUpper + "</td>")
                End If

                strMain = ""
                strSub = ""
                j = 0
                While j < x
                    If Subjects(j, 4) = 1 Then
                        strSub += "<td width=20 align=""CENTER"" ><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_" + Subjects(j, 1) + "_" + "M" + "*</font></td>"
                    End If
                    If Subjects(j, 2).ToLower = "true" Then
                        strMain += "<td width=20 valign=top align=""CENTER"" ><CENTER><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_" + Subjects(j, 1) + "_M" + "*</font></td>"
                        strMain += "<td width=20 valign=top align=""CENTER"" ><CENTER><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_" + Subjects(j, 1) + "_G" + "*</font></td>"
                      
                    Else
                        strMain += "<td width=20 valign=top align=""CENTER"" ><CENTER><font style=""font:8pt Times New roman"">-</font></td>"
                        strMain += "<td width=20 valign=top align=""CENTER"" ><CENTER><font style=""font:8pt Times New roman"">*" + .Item("STU_ID").ToString + "_" + Subjects(j, 1) + "_G*</font></td>"
                    End If
                    j += 1
                End While


                sb.AppendLine(strSub)

                sb.AppendLine(strMain)
                If .Item("RESULT").ToString = "PASS" Then
                    sb.AppendLine("<td width=20 valign=top align=""CENTER"" ><font style=""font:8pt Times New roman"">PASSED</font></td>")
                ElseIf .Item("RESULT").ToString = "FAIL" Then
                    sb.AppendLine("<td width=20 valign=top align=""CENTER"" ><font style=""font:8pt Times New roman"">DETAINED</font></td>")
                ElseIf .Item("RESULT").ToString = "RETEST" Then
                    sb.AppendLine("<td width=20 valign=top align=""CENTER"" ><font style=""font:8pt Times New roman"">RETEST</font></td>")
                End If


                sb.AppendLine("</tr>")


            End With
            i += 1
        End While





        Return sb.ToString
    End Function


    Function GetConsolidatedFooter(ByVal subjects(,) As String, ByVal rptHeaders As String(,)) As String
        Dim sb As New StringBuilder

        sb.AppendLine(getFooter(subjects, "", rptHeaders))
        sb.AppendLine("</tr>")
        If Session("sbsuid") = "125011" Then

            sb.AppendLine(getFooter(subjects, "A*&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "A&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "B&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "C&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "D&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "E&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "F&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "G&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "U&nbsp;", rptHeaders))
            sb.AppendLine("</tr>")



        ElseIf hfGRD_ID.Value = "01" Or hfGRD_ID.Value = "02" Or hfGRD_ID.Value = "03" Or hfGRD_ID.Value = "04" Then
            sb.AppendLine(getFooter(subjects, "A+&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "A&nbsp;", rptHeaders))
            ' sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "B&nbsp;", rptHeaders))
            '  sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "C&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "D&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")



        Else
            sb.AppendLine(getFooter(subjects, "A1&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "A2&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "B1&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "B2&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "C1&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "C2&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "D&nbsp;", rptHeaders))
            'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "E1&nbsp;", rptHeaders))
            ' sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")

            sb.AppendLine(getFooter(subjects, "E2&nbsp;", rptHeaders))
            ' sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
            sb.AppendLine("</tr>")
        End If

        sb.AppendLine(getFooter(subjects, "Highest in class", rptHeaders))
        ' sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, "Subject Average in Class", rptHeaders))
        ' sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        sb.AppendLine(getFooter(subjects, "", rptHeaders))
        ' sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function

    Function ClearFooter(ByVal subjects(,) As String, ByVal rptHeaders As String(,), ByVal strTable As String)
        Dim j As Integer
        Dim strCondition As String
        Dim k As Integer

        For j = 0 To subjects.GetLength(0) - 1
            For k = 0 To rptHeaders.GetLength(0) - 1
                If Session("sbsuid") = "125011" Then
                    strCondition = "count_" + subjects(j, 1) + "_HC_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_SUBAVG_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A*&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_B&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_C&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_D&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_E&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_F&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_G&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_U&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                ElseIf subjects(j, 2).ToLower = "true" Then
                    strCondition = "count_" + subjects(j, 1) + "_HC_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_SUBAVG_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A+&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_B&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_C&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_D&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A1&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A2&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_B1&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_B2&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_C1&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_C2&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_E1&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_E2&nbsp;_" + rptHeaders(k, 0)
                    strTable = strTable.Replace(strCondition, "0")
                Else
                    strCondition = "count_" + subjects(j, 1) + "_A+"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_B"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_C"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_D"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A1"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_A2"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_B1"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_B2"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_C1"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_C2"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_E1"
                    strTable = strTable.Replace(strCondition, "0")
                    strCondition = "count_" + subjects(j, 1) + "_E2"
                    strTable = strTable.Replace(strCondition, "0")
                End If
            Next
        Next
        Return strTable
    End Function

    Function getFooter(ByVal subjects(,) As String, ByVal strText As String, ByVal rptHeaders As String(,))
        Dim sb As New StringBuilder
        Dim strSub As String
        Dim strMain As String
        Dim strCondition As String
        Dim k As Integer
        Dim j As Integer

        sb.AppendLine("<tr>")
        sb.AppendLine("<td HEIGHT=""16px"" align=""middle"" Class=repcolDetail><CENTER>&nbsp;</td>")
        sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>&nbsp;</td>")
        If strText = "" Then
            sb.AppendLine("<td align=""right"" Class=repcolDetail>&nbsp;</td>")
        Else
            sb.AppendLine("<td align=""right"" Class=repcolDetail>" + strText + "</td>")
        End If



        For j = 0 To subjects.GetLength(0) - 1
            If subjects(j, 4) = 1 Then
                strSub += "<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
            End If

            'If subjects(j, 2).ToLower = "true" Then
            '    strMain += "<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>"

            '    If subjects(j, 5) <> "" Then
            '        strMain += "<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
            '    End If

            '    If subjects(j, 6) <> "" Then
            '        strMain += "<td width=""20px"" align=""CENTER"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
            '    End If
            'End If

            If subjects(j, 2).ToLower = "true" Then
                If strText = "Highest in class" Then
                    If subjects(j, 2).ToLower = "true" Then
                        strCondition = "count_" + subjects(j, 1) + "_HC"
                    Else
                        strCondition = "&nbsp;"
                    End If
                ElseIf strText = "Subject Average in Class" Then
                    If subjects(j, 2).ToLower = "true" Then
                        strCondition = "count_" + subjects(j, 1) + "_SUBAVG"
                    Else
                        strCondition = "&nbsp;"
                    End If
                ElseIf strText = "" Then
                    strCondition = "&nbsp;"
                Else
                    If subjects(j, 2).ToLower = "true" Then
                        strCondition = "count_" + subjects(j, 1) + "_" + strText
                    Else
                        strCondition = "&nbsp;"
                    End If
                End If



                For k = 0 To rptHeaders.GetLength(0) - 1
                    If rptHeaders(k, 2) = "MARK" Then
                        If strText <> "Highest in class" And strText <> "Subject Average in Class" Then
                            strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                        Else
                            If subjects(j, 4) = "1" Then
                                strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                            Else
                                strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">" + strCondition + "_" + rptHeaders(k, 0) + "</font></td>"
                            End If
                        End If

                    Else
                        If strText <> "Highest in class" And strText <> "Subject Average in Class" Then
                            If strCondition <> "&nbsp;" Then
                                strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">" + strCondition + "_" + rptHeaders(k, 0) + "</font></td>"
                            Else
                                strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                            End If
                        Else
                            strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                        End If
                    End If
                Next
            Else
                If strText <> "Highest in class" And strText <> "Subject Average in Class" And strText <> "" And strText <> "&nbsp;" Then
                    'strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">count_" + subjects(j, 1) + "_" + strText + "</font></td>"
                    strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                Else
                    strMain += "<td width=""20px"" align=""CENTER""><font style=""font:8pt Times New roman"">&nbsp;</font></td>"
                End If
            End If
        Next

        sb.AppendLine(strSub)
        sb.AppendLine(strMain)

        If hfbFinal.Value = 1 Then
            ''Attendance
            sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        End If
        ''Total Marks
        'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")

        ' ''Percentage
        'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")


        'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")


        'sb.AppendLine("<td align=""middle"" ><font style=""font:8pt Times New roman"">&nbsp;</font></td>")
        'sb.AppendLine("</tr>")

        Return sb.ToString
    End Function

    Function getStudentOptions(ByVal strTable As String, ByVal SCT_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("Current_ACD_ID") = hfACD_ID.Value Then
            str_query = "SELECT STU_ID,SBG_SHORTCODE,CND_CON_ID FROM " _
                                   & " STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID AND SSD_ACD_ID=STU_ACD_ID" _
                                   & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SSD_SBG_ID=C.SBG_ID" _
                                   & " INNER JOIN CONSOLIDATED_SUBJECTGROUP_D AS D ON C.SBG_ID=D.CND_SBG_ID" _
                                   & " WHERE STU_SCT_ID=" + SCT_ID
        Else
            str_query = "SELECT STU_ID,SBG_SHORTCODE,CND_CON_ID FROM " _
                           & " STUDENT_M AS A " _
                           & " INNER JOIN OASIS..STUDENT_PROMO_S AS E ON A.STU_ID=E.STP_STU_ID AND STP_ACD_ID=" + hfACD_ID.Value _
                           & " INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID AND                                  SSD_ACD_ID=STP_ACD_ID" _
                           & " INNER JOIN SUBJECTS_GRADE_S AS C ON B.SSD_SBG_ID=C.SBG_ID" _
                           & " INNER JOIN CONSOLIDATED_SUBJECTGROUP_D AS D ON C.SBG_ID=D.CND_SBG_ID" _
                           & " WHERE STP_SCT_ID=" + SCT_ID
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id_M As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_M = "*" + .Item(0).ToString + "_" + .Item(2).ToString + "_M*"
                strTable = strTable.Replace(id_M, .Item(1).ToString)
            End With
        Next
        Return strTable
    End Function

    Function GetAttendance(ByVal strTable As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_query As String

        ' If hfPromotionSheet.Value = 1 Then
        str_query = "SELECT STU_ID,ROUND(ISNULL(ATT_VALUE,0)*100/ISNULL(ATT_TOTALDAYS,1),1) AS PERCENTAGE FROM " _
        & " STUDENT_M AS A LEFT OUTER JOIN RPT.FINAL_ATTENDANCE AS B ON A.STU_ID=B.ATT_STU_ID" _
        & " AND STU_ACD_ID=ATT_ACD_ID WHERE STU_SCT_ID=" + sct_id
        'Else
        '    str_query = "SELECT STU_ID,isnull(RST_COMMENTS,'&nbsp;') FROM OASIS..STUDENT_M AS A " _
        '                & " CROSS JOIN  RPT.REPORT_SETUP_D AS C" _
        '                & " LEFT OUTER JOIN RPT.REPORT_STUDENT_S AS B ON A.STU_ID=B.RST_STU_ID AND RST_ACD_ID=STU_ACD_ID" _
        '                & " AND RST_RPF_ID=" + hfRPF_ID.Value _
        '                & " AND B.RST_RSD_ID=C.RSD_ID  " _
        '                & " WHERE STU_SCT_ID=" + sct_id _
        '                & " AND RSD_HEADER='Attendance' AND RSD_RSM_ID=" + hfRSM_ID.Value
        'End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id_M As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_M = "*" + .Item(0).ToString + "_ATT*"
                strTable = strTable.Replace(id_M, .Item(1).ToString)
            End With
        Next

        Return strTable

    End Function

    Function GetGradeCount(ByVal strTable As String, ByVal sct_id As String, ByVal subjects(,) As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If hfPromotionSheet.Value = 1 Then
            str_query = "SELECT DISTINCT STU_ID, ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,FPR_GRADING" _
                             & " FROM SUBJECTS_GRADE_S AS A " _
                             & " INNER JOIN RPT.FINALPROCESS_REPORT_S AS B ON A.SBG_ID=B.FPR_SBG_ID AND FPR_GRADING IN('A','B','C','D','E','F','G','U') " _
                             & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.FPR_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                             & " INNER JOIN OASIS..STUDENT_M AS D ON B.FPR_STU_ID=D.STU_ID AND B.FPR_ACD_ID=D.STU_ACD_ID" _
                             & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                             & " WHERE FPR_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id _
                             & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'"

            str_query = "SELECT SBG_ID,FPR_GRADING,COUNT(FPR_GRADING) FROM(" + str_query + ")P " _
                       & " GROUP BY SBG_ID,FPR_GRADING"
        Else
            str_query = "SELECT DISTINCT STU_ID,ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,RST_GRADING" _
                                         & " FROM SUBJECTS_GRADE_S AS A " _
                                         & " INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.SBG_ID=B.RST_SBG_ID AND RST_GRADING IN('A','B','C','D','E','F','G','U') " _
                                         & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                                         & " INNER JOIN OASIS..STUDENT_M AS D ON B.RST_STU_ID=D.STU_ID AND B.RST_ACD_ID=D.STU_ACD_ID" _
                                         & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                                         & " WHERE RST_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id _
                                         & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'"

            str_query = "SELECT SBG_ID,RST_GRADING,COUNT(RST_GRADING) FROM(" + str_query + ")P " _
                       & " GROUP BY SBG_ID,RST_GRADING"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id = "count_" + .Item(0).ToString + "_" + .Item(1).ToString
                strTable = strTable.Replace(id, .Item(2).ToString)
            End With
        Next

        'if the grade is not there for that subject then replace with 0

        For i = 0 To subjects.GetLength(0) - 1
            id = "count_" + subjects(i, 1) + "_A"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_B"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_C"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_D"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_E"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_F"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_G"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_U"
            strTable = strTable.Replace(id, "0")
        Next

        Return strTable
    End Function


    Function GetHighestinClass(ByVal strTable As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String


        If hfPromotionSheet.Value = 1 Then
            str_query = "SELECT FPR_RSD_ID AS RSD_ID,ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,FPR_MARKS" _
                               & " FROM SUBJECTS_GRADE_S AS A " _
                               & " INNER JOIN RPT.FINALPROCESS_REPORT_S AS B ON A.SBG_ID=B.FPR_SBG_ID " _
                               & " INNER JOIN OASIS..STUDENT_M AS D ON B.FPR_STU_ID=D.STU_ID AND B.FPR_ACD_ID=D.STU_ACD_ID" _
                               & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                               & " WHERE FPR_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id

            str_query = "SELECT SBG_ID,MAX(isnull(FPR_MARKS,0)),AVG(isnull(FPR_MARKS,0)) FROM(" + str_query + ")P " _
                       & " GROUP BY SBG_ID"
        Else
            str_query = "SELECT RST_RSD_ID AS RSD_ID,ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,RST_MARK" _
                       & " FROM SUBJECTS_GRADE_S AS A " _
                       & " INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.SBG_ID=B.RST_SBG_ID " _
                       & " INNER JOIN OASIS..STUDENT_M AS D ON B.RST_STU_ID=D.STU_ID AND B.RST_ACD_ID=D.STU_ACD_ID" _
                       & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                       & " WHERE RST_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id

            str_query = "SELECT SBG_ID,RSD_ID,MAX(isnull(RST_MARK,0)),AVG(isnull(RST_MARK,0)) FROM(" + str_query + ")P " _
                       & " GROUP BY SBG_ID,RSD_ID"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id = "count_" + .Item(0).ToString + "_HC_" + .Item(1).ToString
                strTable = strTable.Replace(id, .Item(2).ToString.Replace(".00", ""))
                id = "count_" + .Item(0).ToString + "_SUBAVG_" + .Item(1).ToString
                strTable = strTable.Replace(id, Math.Round(.Item(3), 2).ToString.Replace(".00", ""))
            End With
        Next


        Return strTable
    End Function


    Function getHouseCount(ByVal strTable As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT HOUSE_DESCRIPTION,HOUSE_ID," _
                                & " STU_ID FROM STUDENT_M INNER JOIN HOUSE_M AS B on STU_HOUSE_ID=B.HOUSE_ID AND STU_SCT_ID=" + sct_id _
                                & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'" _
                                & " AND HOUSE_BSU_ID='" + hfBSU_ID.Value + "' AND HOUSE_DESCRIPTION<>''"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id_M As String = ""
        Dim i As Integer
        Dim alphaCount, betaCount, gammaCount, sigmaCount As Integer
        Dim alphaPoint, betaPoint, gammaPoint, sigmaPoint As Integer

        Dim hP As New Hashtable
        hP = ViewState("hP")

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                ' id_M = .Item(0).ToString.Trim.ToUpper + "_COUNT"
                'strTable = strTable.Replace(id_M, .Item(2).ToString)

                If .Item(0).ToString.Trim.ToUpper = "ALPHA" Then
                    alphaPoint += Val(hP.Item(.Item(2).ToString))
                    alphaCount += 1
                ElseIf .Item(0).ToString.Trim.ToUpper = "BETA" Then
                    betaPoint += Val(hP.Item(.Item(2).ToString))
                    betaCount += 1
                ElseIf .Item(0).ToString.Trim.ToUpper = "GAMMA" Then
                    gammaPoint += Val(hP.Item(.Item(2).ToString))
                    gammaCount += 1
                ElseIf .Item(0).ToString.Trim.ToUpper = "SIGMA" Then
                    sigmaPoint += Val(hP.Item(.Item(2).ToString))
                    sigmaCount += 1
                End If
            End With
        Next

        strTable = strTable.Replace("ALPHA_COUNT", (Math.Round(alphaPoint / IIf(alphaCount = 0, 1, alphaCount), 2)).ToString)
        strTable = strTable.Replace("BETA_COUNT", (Math.Round(betaPoint / IIf(betaCount = 0, 1, betaCount), 2)).ToString)
        strTable = strTable.Replace("GAMMA_COUNT", (Math.Round(gammaPoint / IIf(gammaCount = 0, 1, gammaCount), 2)).ToString)
        strTable = strTable.Replace("SIGMA_COUNT", (Math.Round(sigmaPoint / IIf(sigmaCount = 0, 1, sigmaCount), 2)).ToString)

        Return strTable


    End Function


#End Region


End Class
