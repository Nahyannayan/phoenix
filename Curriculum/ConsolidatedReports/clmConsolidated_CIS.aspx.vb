Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_CIS
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
            'hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))
          
            If Session("PromotionSheet") = 1 Then
                hfPromotionSheet.Value = 1
            Else
                hfPromotionSheet.Value = 0
            End If

            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)
            ViewState("arrowkey") = "0"
            If Session("targettracker") = 1 Then
                hfRPF_ID_PREV.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid_prev").Replace(" ", "+"))
            ElseIf Session("targettracker") = 2 Then
                hfRPF_ID_PREV.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid_prev").Replace(" ", "+"))
                hfACD_ID_PREV.Value = Encr_decrData.Decrypt(Request.QueryString("acdid_prev").Replace(" ", "+"))
                Dim grdprev As String() = Encr_decrData.Decrypt(Request.QueryString("grdid_prev").Replace(" ", "+")).Split("|")
                hfGRD_ID_PREV.Value = grdprev(0)
            Else
                hfRSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
                hfSchedule.Value = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))
                hfRPF_DESCR.Value = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))
            End If
            hfRPF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid").Replace(" ", "+"))

            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))

            GetACDCurrent()


            hfAccYear.Value = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
            hfTERM.Value = Encr_decrData.Decrypt(Request.QueryString("term").Replace(" ", "+"))

            ViewState("display") = Request.QueryString("display").Replace(" ", "+")

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330110") Then
            'If Not Request.UrlReferrer Is Nothing Then
            '    Response.Redirect(Request.UrlReferrer.ToString())
            'Else

            '    Response.Redirect("~\noAccess.aspx")
            'End If

            ' Else
            'calling pageright class to get the access rights


            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights
            hfBSU_ID.Value = Session("sbsuid")
            hfBlue.Value = "#3399FF"
            hfGreen.Value = "#00CC00"
            hfYellow.Value = "#FFFF00"
            hfRed.Value = "#FF6633"



            hfRecordNumber.Value = 0
            hfTableColumns.Value = 0
            hfPageNumber.Value = 0



            GetConsolidated(grd_id(0), Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+")), grd_id(1))






            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
            'End If
        End If
    End Sub
#Region "Private Methods"


    Sub GetACDCurrent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT convert(int,ACD_CURRENT) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value
        hfACD_CURRENT.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub
    Sub SetHowParent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE') FROM " _
                               & " FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfShowParent1.Value = ds.Tables(0).Rows(0).Item(0)
        hfShowParent2.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetConsolidated(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim Subjects As String(,) = GetConsolidatedSubjects(grd_id, hfACD_ID.Value, stm_id)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
       
        sb.AppendLine("<HTML><HEAD><TITLE>:::: CONSOLIDATED REPORT ::::</TITLE>")


        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        '    sb.AppendLine("<Link REL=STYLESHEET HREF=""..\cssfiles\brownbook.css"" TYPE=""text/css"">")
        sb.AppendLine("<BODY>")
        ' sb.AppendLine("<Script language=""JavaScript"" src=""../include/com_client_func.js""></Script>")
        ' sb.AppendLine("<style>	body,table,td{background-color:#ffffff!important;}")
        ' sb.AppendLine("th{background-color:#ffffff!important;}")
        'sb.AppendLine("</style>")
        If sct_id = "" Or sct_id = "0" Then
            str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                      & "' AND SCT_ACD_ID=" + hfACD_ID.Value
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetConsolidatedForSection(grd_id, .Rows(i).Item(0).ToString, hfACD_ID.Value, Subjects))
                Next
            End With
            'ltBrownBook.Text = sb.ToString
        Else
            sb.AppendLine(GetConsolidatedForSection(grd_id, sct_id, hfACD_ID.Value, Subjects))

            'ltBrownBook.Text = sb.ToString
        End If
        sb.AppendLine("</BODY></HTML>")
        Response.Write(sb.ToString())


    End Sub

    Function GetConsolidatedSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_DESCR,SBG_ID,ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR,SBG_ORDER,SBG_SHORTCODE,'0' AS OPTIONAL FROM SUBJECTS_GRADE_S " _
                               & " WHERE SBG_GRD_ID='" + grd_id + "' AND SBG_ACD_ID=" + acd_id _
                               & " AND SBG_STM_ID=" + stm_id + " AND SBG_DESCR NOT LIKE '%BTEC%'  ORDER BY SBG_ORDER"



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count


        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 10)
        hfSubjectsKey.Value = ""
        For j = 0 To i - 1
            With ds.Tables(0)
                Subjects(j, 0) = .Rows(j).Item(0).ToString
                Subjects(j, 1) = .Rows(j).Item(1).ToString
                Subjects(j, 2) = .Rows(j).Item(2).ToString
                Subjects(j, 3) = .Rows(j).Item(4).ToString
                Subjects(j, 4) = "Cu"
                Subjects(j, 5) = .Rows(j).Item(5).ToString
                If hfSubjectsKey.Value <> "" Then
                    hfSubjectsKey.Value += ","
                End If
                hfSubjectsKey.Value += .Rows(j).Item(4).ToString + "-" + .Rows(j).Item(0).ToString + "&nbsp;"
            End With
        Next
        If hfSubjectsKey.Value <> "" Then
            hfSubjectsKey.Value = "Subject Keys : " + hfSubjectsKey.Value
        End If
        Return Subjects
    End Function

    Function GetSubjectsHeader(ByVal Subjects(,) As String) As String
        Dim sb As New StringBuilder
        Dim i As Integer

        Dim strSubjects As String = ""
        Dim strHeader As String = ""
        Dim strShort1 As String = ""
        Dim strShort As String = ""
        Dim imgUrl As String = ""
        Dim colSpan As Integer = 0
        Dim colSpan1 As Integer = 0

        hfTableColumns.Value = 0
        For i = 0 To Subjects.GetLength(0) - 1
            'if major subject add one more column



            strHeader += "<td  align=""middle"" Class=repcolDetail>Cu</td>"
            hfTableColumns.Value += 1


            strSubjects += "<td width=""30""  align=""middle"" valign=""bottom""  >" + Subjects(i, 3) + "</td>"
            hfTableColumns.Value += 1


        Next



        sb.AppendLine("<table border=""1"" bordercolorlight=""#000000""   align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font-family: sans-serif !important;"" >")

        sb.AppendLine("<tr >")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center> Sr. No  </td>")
        sb.AppendLine("<td width=""60"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center>Student ID</td>")
        sb.AppendLine("<td  width=300 ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center>Name of the Student</td>")

        If strShort <> "" Then
            sb.AppendLine("<td valign=""middle""  colspan=" + colSpan1.ToString + " align=""middle"" Class=repcolDetail>&nbsp;</td>")
        End If

        hfTableColumns.Value += 5
        sb.AppendLine(strSubjects)


        sb.AppendLine("<td colspan=4>Total Attainment</td>")




        hfTableColumns.Value += 6
        sb.AppendLine("</tr>")



        sb.AppendLine("<tr>")
        If strShort <> "" Then
            sb.Append(strShort)
        End If
        sb.AppendLine(strHeader)


        sb.AppendLine("<td BGCOLOR=" + hfBlue.Value + "  width=""40"" align=""middle"" valign=""middle""  >No. Blue</td>")
        sb.AppendLine("<td  BGCOLOR=" + hfGreen.Value + " width=""40"" align=""middle"" valign=""middle""  >No. Green</td>")
        sb.AppendLine("<td BGCOLOR=" + hfYellow.Value + " width=""40"" align=""middle"" valign=""middle""  >No. Yellow</td>")
        sb.AppendLine("<td  BGCOLOR=" + hfRed.Value + " width=""40"" align=""middle"" valign=""middle""  >No. Red</td>")


        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function


    Function GetConsolidatedForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,)) As String
        Dim sb As New StringBuilder
        Dim strHeader As String = GetConsolidatedHeader(sct_id)
        ' sb.AppendLine(strHeader)
        sb.AppendLine("<table width=""1400""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

        sb.AppendLine("<tr><td algn=center>")
        sb.AppendLine(strHeader)
        sb.AppendLine("</td></tr>")

        sb.AppendLine("<tr><td algn=center>")

        sb.AppendLine(GetSubjectsHeader(Subjects))

        sb.AppendLine(GetStudentMarks(sct_id, Subjects))

        sb.AppendLine("</table>")

        sb.AppendLine("</td></tr>")

        sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetConsolidatedHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_BB_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table   align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=bottom>")


            sb.AppendLine("<table width=""90%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font-family: sans-serif !important;"">")
            sb.AppendLine("<TR >")
            sb.AppendLine("<TD>GRADE :    " + reader("GRM_DISPLAY") + "   &nbsp;&nbsp; SECTION : " + reader("SCT_DESCR") + " </TD> </TR>")

            sb.AppendLine("</Table>")

            sb.AppendLine("</TD>")


            sb.AppendLine("<TD WIDTH=40% VALIGN=TOP>")

            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font style=""text-decoration:NONE;font-family: sans-serif !important;"">" + reader("BSU_NAME"))

            If Session("targettracker") = 1 Then
                sb.AppendLine("<BR>" + "TARGET TRACKER(" + hfTERM.Value.ToUpper.Replace("REPORT", "") + ")" + reader("ACY_DESCR") + "</TD></TR>")
            ElseIf ViewState("display") = "level" Then
                sb.AppendLine("<BR>" + "LEVEL TRACKER(" + hfTERM.Value.ToUpper.Replace("REPORT", "") + ")</TD></TR>")
            ElseIf Session("targettracker") = 2 Then
                sb.AppendLine("<BR>" + "TARGET TRACKER(" + hfTERM.Value.ToUpper.Replace("REPORT", "") + ")</TD></TR>")
            Else
                sb.AppendLine("<BR>" + hfTERM.Value.ToUpper.Replace("REPORT", "") + " SUMMARY SHEET  " + reader("ACY_DESCR") + "</TD></TR>")
            End If



            sb.AppendLine("</table>")

            sb.AppendLine("</TD>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=TOP ALIGN=""RIGHT"">")
            sb.AppendLine("<table width=""80%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;</TD></TR>")
            sb.AppendLine("</table>")
            sb.AppendLine("</TD>")

            sb.AppendLine("</TR>")

            sb.AppendLine("<TR><TD colspan=2><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;&nbsp;&nbsp;&nbsp; Class Teacher :  " + reader("EMP_NAME") + " </TD>")
            sb.AppendLine("<TD align=right><font style=""text-decoration:NONE;font-family: sans-serif !important;""> Date : " + Now.Date + " </TD>")
            sb.AppendLine("</TR>")
            sb.AppendLine("</Table><BR>")

            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function


    Function GetStudentMarks(ByVal sct_id As String, ByVal Subjects(,) As String)
        Dim strTable As String

        Dim strFooter As String = GetConsolidatedFooter(Subjects)

        Dim strStudents As String = GetStudents(sct_id, Subjects)


        strTable = strStudents
        If hfLastRecord.Value = 0 Then

            strTable += strFooter
        End If

        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim id_C As String = ""
        Dim id_G As String = ""
        Dim i As Integer

        Dim strStuId As String = ""
        Dim strRetestSubjects As String = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String
        Dim arrow As String
        'POPULATE STUDENT MARKS

        Dim levelchange As String = ""

        Dim str_query As String
        If Session("targettracker") = 1 Then
            str_query = "exec [RPT].[rptTARGETTRACKER] " _
                       & hfACD_ID.Value + "," _
                       & "'" + hfGRD_ID.Value + "'," _
                       & IIf(hfRPF_ID_PREV.Value = "0", "NULL", hfRPF_ID_PREV.Value) + "," _
                       & hfRPF_ID.Value + "," _
                       & sct_id
        ElseIf Session("targettracker") = 2 Then
            str_query = "exec [RPT].[rptTARGETTRACKERACROSSYEAR] " _
                       & hfACD_ID.Value + "," _
                       & hfACD_ID_PREV.Value + "," _
                       & "'" + hfGRD_ID.Value + "'," _
                       & "'" + hfGRD_ID_PREV.Value + "'," _
                       & IIf(hfRPF_ID_PREV.Value = "0", "NULL", hfRPF_ID_PREV.Value) + "," _
                       & hfRPF_ID.Value + "," _
                       & sct_id
        Else
            str_query = "exec [RPT].[rptSTUDENTCONSOLIDATED_CIS] " _
                             & hfACD_ID.Value + "," _
                             & "'" + hfGRD_ID.Value + "'," _
                             & hfRSM_ID.Value + "," _
                             & hfRPF_ID.Value + "," _
                             & sct_id
        End If

     


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_G = "*" + .Item(0).ToString + "_" + .Item(3).ToString + "_G*"
                id_C = "*" + .Item(0).ToString + "_" + .Item(3).ToString + "_COLOR*"
                grade = .Item("RST_COMMENTS")
                arrow = .Item("ARROW")

                If ViewState("display") = "tracker" Then
                    levelchange = .Item("LEVELCHANGE").ToString
                Else
                    levelchange = .Item("RST_PREVLEVEL") + "/" + .Item("RST_LEVEL")
                End If

                If levelchange = "0" Then
                    levelchange = ""
                End If

                'For Grades 09,10,11
                'The red  colour on the report should be to used to classify subject grades D, E, F and U.
                'The yellow colour on the report should be used to classify subject grades C
                'The green colour on the report should be used to classify subject grades B
                'The blue colour on the report should be used to classiy subject grades A and A*


                'For Grade 12,13
                'The red  colour on the report should be to used to classify subject grades  E, F and U.
                'The yellow colour on the report should be used to classify subject grades D
                'The green colour on the report should be used to classify subject grades C, B
                'The blue colour on the report should be used to classiy subject grades A and A*

                If hfGRD_ID.Value = "12" Or hfGRD_ID.Value = "13" Then
                    If grade = "A*" Or grade = "A" Or grade = "A+" Then
                        strTable = strTable.ToString.Replace(id_C, hfBlue.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_BLUE*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_BLUE*")) + 1
                    ElseIf grade = "B" Or grade = "C" Then
                        strTable = strTable.ToString.Replace(id_C, hfGreen.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_GREEN*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_GREEN*")) + 1
                    ElseIf grade = "D" Then
                        strTable = strTable.ToString.Replace(id_C, hfYellow.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_YELLOW*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_YELLOW*")) + 1
                    ElseIf grade = "E" Or grade = "F" Or grade = "G" Or grade = "U" Then
                        strTable = strTable.ToString.Replace(id_C, hfRed.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_RED*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_RED*")) + 1
                    Else
                        strTable = strTable.ToString.Replace(id_C, "#ffffff")
                    End If
                Else
                    If grade = "A*" Or grade = "A" Or grade = "A+" Then
                        strTable = strTable.ToString.Replace(id_C, hfBlue.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_BLUE*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_BLUE*")) + 1
                    ElseIf grade = "B" Then
                        strTable = strTable.ToString.Replace(id_C, hfGreen.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_GREEN*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_GREEN*")) + 1
                    ElseIf grade = "C" Then
                        strTable = strTable.ToString.Replace(id_C, hfYellow.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_YELLOW*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_YELLOW*")) + 1
                    ElseIf grade = "D" Or grade = "E" Or grade = "F" Or grade = "G" Or grade = "U" Then
                        strTable = strTable.ToString.Replace(id_C, hfRed.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_RED*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_RED*")) + 1
                    Else
                        strTable = strTable.ToString.Replace(id_C, "#ffffff")
                    End If
                End If

                If arrow = "UP" Then
                    strTable = strTable.ToString.Replace(id_G, "<SUB>" + levelchange + "</SUB>" + "↑")
                ElseIf arrow = "DOWN" Then
                    strTable = strTable.ToString.Replace(id_G, "<SUB>" + levelchange + "</SUB>" + "↓")
                ElseIf arrow = "SAME" Then
                    If ViewState("display") = "tracker" Then
                        strTable = strTable.ToString.Replace(id_G, "↔")
                    Else
                        strTable = strTable.ToString.Replace(id_G, "<SUB>" + levelchange + "</SUB>")
                    End If
                ElseIf grade = "-" And (levelchange = "" Or levelchange = "-" Or levelchange = "/" Or levelchange = "-/" Or levelchange = "/-") Then
                    strTable = strTable.ToString.Replace(id_G, "-")
                Else
                    If ViewState("display") = "tracker" Then
                        strTable = strTable.ToString.Replace(id_G, "&nbsp;")
                    Else
                        strTable = strTable.ToString.Replace(id_G, "<SUB>" + levelchange + "</SUB>")
                    End If
                End If
            End With
        Next


        str_query = "SELECT STU_ID FROM VW_STUDENT_DETAILS_PREVYEARS WHERE STU_SCT_ID=" + sct_id + " AND STU_ACD_ID=" + hfACD_ID.Value

        Dim id_T As String

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
  
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_T = "*" + .Item(0).ToString + "_BLUE*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_GREEN*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_YELLOW*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_RED*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))
            End With
        Next


        Return strTable
    End Function

    Function GetHousePoints(ByVal marks As Integer)
        Dim hPoints As String = "&nbsp;"
        Select Case hfGRD_ID.Value
            Case "09"
                Select Case marks
                    Case 900 To 1000
                        hPoints = "+18"
                    Case 800 To 899
                        hPoints = "+15"
                    Case 700 To 799
                        hPoints = "+12"
                    Case 600 To 699
                        hPoints = "+9"
                    Case 500 To 599
                        hPoints = "+6"
                    Case 400 To 499
                        hPoints = "+3"
                    Case Else
                        hPoints = "0"
                End Select


            Case "07", "08", "10"
                Select Case marks
                    Case 810 To 900
                        hPoints = "+18"
                    Case 720 To 809
                        hPoints = "+15"
                    Case 630 To 719
                        hPoints = "+12"
                    Case 540 To 629
                        hPoints = "+9"
                    Case 450 To 539
                        hPoints = "+6"
                    Case 360 To 449
                        hPoints = "+3"
                    Case Else
                        hPoints = "0"
                End Select

            Case "11"
                Select Case marks
                    Case marks >= 540 And marks <= 600
                        hPoints = "+18"
                    Case marks >= 480 And marks <= 539
                        hPoints = "+15"
                    Case marks >= 420 And marks <= 479
                        hPoints = "+12"
                    Case marks >= 360 And marks <= 419
                        hPoints = "+9"
                    Case marks >= 300 And marks <= 359
                        hPoints = "+6"
                    Case marks >= 240 And marks <= 299
                        hPoints = "+3"
                    Case marks < 240
                        hPoints = "0"
                End Select
        End Select

        Return hPoints.ToString
    End Function


    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ' Dim strSubjectHeader As String = GetSubjectsHeader(Subjects)
        'Dim strPageHeader As String = GetConsolidatedHeader(sct_id)
        Dim i As Integer
        Dim j As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()
        Dim strMain As String
        Dim strSub As String

        Dim strRedLine As String

        Dim str_query As String

        Dim hColor As New Hashtable

        If hfACD_CURRENT.Value = 1 Then


            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                          & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                          & " ISNULL(STU_NO,'&nbsp') AS STU_NO, " _
                          & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                          & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                          & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                          & " STU_ID,'' AS HOUSE FROM OASIS..STUDENT_M AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                          & " WHERE STU_SCT_ID=" + sct_id + " AND STU_CURRSTATUS='EN' " _
                          & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        Else

            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                       & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                       & " ISNULL(STU_NO,'&nbsp') AS STU_NO, " _
                       & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                       & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                       & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                       & " STU_ID,'' AS HOUSE FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                       & " WHERE STU_SCT_ID=" + sct_id + " AND STU_CURRSTATUS='EN' AND STU_ACD_ID=" + hfACD_ID.Value _
                       & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count


        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)

                stuNames = .Item(0).ToString.Replace("  ", " ").Split(" ")
                sb.AppendLine("<tr height=""16px"">")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td align=""middle""  Class=repcolDetail><CENTER>" + .Item("STU_NO") + "</td>")
                If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                    sb.AppendLine("<td width=300 align=""left"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                Else
                    sb.AppendLine("<td width=300 align=""left"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                End If

                strMain = ""
                strSub = ""

                For j = 0 To Subjects.GetLength(0) - 1
                    'If Subjects(j, 7) = 1 Then
                    '    strSub += "<td width=20 align=""CENTER"" ><font style=""font-family: sans-serif !important;"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + "M" + "*</font></td>"
                    'End If

                    'If Subjects(j, 2).ToLower = "true" Then
                    '    strMain += "<td width=20 align=""CENTER"" ><font style=""font-family: sans-serif !important;"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + Subjects(j, 8) + "_" + "M" + "*</font></td>"

                    '    If Subjects(j, 5) <> "" Then
                    '        strMain += "<td width=20 align=""CENTER"" ><font style=""font-family: sans-serif !important;"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + Subjects(j, 9) + "_" + "M" + "*</font></td>"
                    '    End If

                    '    If Subjects(j, 6) <> "" Then
                    '        strMain += "<td width=20 align=""CENTER"" ><font style=""font-family: sans-serif !important;"">*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + Subjects(j, 10) + "_" + "M" + "*</font></td>"
                    '    End If
                    'End If
                    strMain += "<td bgcolor=*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_COLOR" + "* align=""CENTER""><font >*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + "G" + "*</font></td>"
                Next

                '  sb.AppendLine(strSub)
                sb.AppendLine(strMain)


                sb.AppendLine("<td align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_BLUE*</font></td>")

                sb.AppendLine("<td align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_GREEN*</font></td>")

                sb.AppendLine("<td align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_YELLOW*</font></td>")

                sb.AppendLine("<td align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_RED*</font></td>")


                sb.AppendLine("</tr>")

                hColor.Add("*" + .Item("STU_ID").ToString + "_BLUE*", 0)
                hColor.Add("*" + .Item("STU_ID").ToString + "_GREEN*", 0)
                hColor.Add("*" + .Item("STU_ID").ToString + "_YELLOW*", 0)
                hColor.Add("*" + .Item("STU_ID").ToString + "_RED*", 0)
                ViewState("hColor") = hColor
            End With
        Next





        Return sb.ToString
    End Function


    Function GetConsolidatedFooter(ByVal subjects(,) As String) As String
        Dim sb As New StringBuilder

        sb.AppendLine("</table>")

        sb.AppendLine("<table")
        sb.AppendLine("<tr><td valign=top>")
        sb.AppendLine("<table border=""1"" bordercolorlight=""#000000""   align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font-family: sans-serif !important;"" >")
        sb.AppendLine("<tr><td colspan=2 align=center bgcolor=#CC99CC>KEY</td></tr>")
        sb.AppendLine("<tr><td align=center bgcolor=#CCCCFF>Current Grade</td><td bgcolor=#CCCCFF>Attainment</td></tr>")
        sb.AppendLine("<tr><td bgcolor=" + hfBlue.Value + " align=center>Blue</td><td bgcolor=" + hfBlue.Value + ">OutStanding</td></tr>")
        sb.AppendLine("<tr><td bgcolor=" + hfGreen.Value + " align=center>Green</td><td bgcolor=" + hfGreen.Value + ">Good</td></tr>")
        sb.AppendLine("<tr><td bgcolor=" + hfYellow.Value + " align=center>Yellow</td><td bgcolor=" + hfYellow.Value + ">Satisfactory</td></tr>")
        sb.AppendLine("<tr><td bgcolor=" + hfRed.Value + " align=center>Red</td><td bgcolor=" + hfRed.Value + ">Unsatisfactory</td></tr>")
        sb.AppendLine("</table>")

        If hfRPF_ID_PREV.Value <> "0" Then

            ' If ViewState("arrowkey") = "1" Then
            sb.AppendLine("</td><td valign=top>")
            sb.AppendLine("<table border=""1""  bordercolorlight=""#000000""   align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font-family: sans-serif !important;"" >")
            sb.AppendLine("<tr><td colspan=2 align=center  bgcolor=#CC99CC>KEY</td></tr>")
            sb.AppendLine("<tr><td align=center>Making Progress</td><td align=center>↑</td></tr>")
            sb.AppendLine("<tr><td align=center>No Change</td><td  align=center>↔</td></tr>")
            sb.AppendLine("<tr><td align=center>Declined</td><td align=center>↓</td ></tr>")
            sb.AppendLine("</table>")
        End If
        ' Else
        '  sb.AppendLine("</tr></td>")
        '  End If

        sb.AppendLine("<td valign=top><font style=""text-decoration:NONE;font-family: sans-serif !important;"">" + hfSubjectsKey.Value + "</font></td>")
        Return sb.ToString
    End Function

    

  
    Function GetGradeCount(ByVal strTable As String, ByVal sct_id As String, ByVal subjects(,) As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If hfPromotionSheet.Value = 1 Then
            str_query = "SELECT DISTINCT STU_ID, ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,FPR_GRADING" _
                             & " FROM SUBJECTS_GRADE_S AS A " _
                             & " INNER JOIN RPT.FINALPROCESS_REPORT_S AS B ON A.SBG_ID=B.FPR_SBG_ID AND FPR_GRADING IN('A','B','C','D','E','F','G','U') " _
                             & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.FPR_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                             & " INNER JOIN OASIS..STUDENT_M AS D ON B.FPR_STU_ID=D.STU_ID AND B.FPR_ACD_ID=D.STU_ACD_ID" _
                             & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                             & " WHERE FPR_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id _
                             & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'"

            str_query = "SELECT SBG_ID,FPR_GRADING,COUNT(FPR_GRADING) FROM(" + str_query + ")P " _
                       & " GROUP BY SBG_ID,FPR_GRADING"
        Else
            str_query = "SELECT DISTINCT STU_ID,ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,RST_GRADING" _
                                         & " FROM SUBJECTS_GRADE_S AS A " _
                                         & " INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.SBG_ID=B.RST_SBG_ID AND RST_GRADING IN('A','B','C','D','E','F','G','U') " _
                                         & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                                         & " INNER JOIN OASIS..STUDENT_M AS D ON B.RST_STU_ID=D.STU_ID AND B.RST_ACD_ID=D.STU_ACD_ID" _
                                         & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                                         & " WHERE RST_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id _
                                         & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN'"

            str_query = "SELECT SBG_ID,RST_GRADING,COUNT(RST_GRADING) FROM(" + str_query + ")P " _
                       & " GROUP BY SBG_ID,RST_GRADING"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id = "count_" + .Item(0).ToString + "_" + .Item(1).ToString
                strTable = strTable.Replace(id, .Item(2).ToString)
            End With
        Next

        'if the grade is not there for that subject then replace with 0

        For i = 0 To subjects.GetLength(0) - 1
            id = "count_" + subjects(i, 1) + "_A"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_B"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_C"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_D"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_E"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_F"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_G"
            strTable = strTable.Replace(id, "0")

            id = "count_" + subjects(i, 1) + "_U"
            strTable = strTable.Replace(id, "0")
        Next

        Return strTable
    End Function


    Function GetHighestinClass(ByVal strTable As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String


        If hfPromotionSheet.Value = 1 Then
            str_query = "SELECT ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,FPR_MARKS" _
                               & " FROM SUBJECTS_GRADE_S AS A " _
                               & " INNER JOIN RPT.FINALPROCESS_REPORT_S AS B ON A.SBG_ID=B.FPR_SBG_ID " _
                               & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.FPR_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                               & " INNER JOIN OASIS..STUDENT_M AS D ON B.FPR_STU_ID=D.STU_ID AND B.FPR_ACD_ID=D.STU_ACD_ID" _
                               & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                               & " WHERE FPR_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id

            str_query = "SELECT SBG_ID,MAX(isnull(FPR_MARKS,0)),AVG(isnull(FPR_MARKS,0)) FROM(" + str_query + ")P " _
                       & " GROUP BY SBG_ID"
        Else
            str_query = "SELECT ISNULL(CND_CON_ID,SBG_ID) AS SBG_ID,RST_MARK" _
                                           & " FROM SUBJECTS_GRADE_S AS A " _
                                           & " INNER JOIN RPT.REPORT_STUDENT_S AS B ON A.SBG_ID=B.RST_SBG_ID " _
                                           & " INNER JOIN RPT.REPORT_SETUP_D AS C ON B.RST_RSD_ID=RSD_ID AND RSD_bPERFORMANCE_INDICATOR=1 " _
                                           & " INNER JOIN OASIS..STUDENT_M AS D ON B.RST_STU_ID=D.STU_ID AND B.RST_ACD_ID=D.STU_ACD_ID" _
                                           & " LEFT OUTER JOIN CONSOLIDATED_SUBJECTGROUP_D AS E ON A.SBG_ID=E.CND_SBG_ID" _
                                           & " WHERE RST_RPF_ID=" + hfRPF_ID.Value + " AND STU_SCT_ID=" + sct_id

            str_query = "SELECT SBG_ID,MAX(isnull(RST_MARK,0)),AVG(isnull(RST_MARK,0)) FROM(" + str_query + ")P " _
                       & " GROUP BY SBG_ID"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id = "count_" + .Item(0).ToString + "_HC"
                strTable = strTable.Replace(id, .Item(1).ToString.Replace(".00", ""))
                id = "count_" + .Item(0).ToString + "_SUBAVG"
                strTable = strTable.Replace(id, Math.Round(.Item(2), 2).ToString.Replace(".00", ""))
            End With
        Next


        Return strTable
    End Function


    Function getHouseCount(ByVal strTable As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT HOUSE_DESCRIPTION,HOUSE_ID," _
                                & " (SELECT COUNT(STU_ID) FROM STUDENT_M WHERE STU_HOUSE_ID=B.HOUSE_ID AND STU_SCT_ID=" + sct_id _
                                & " AND ISNULL(CONVERT(DATETIME,STU_LEAVEDATE),'2100-10-10')>CONVERT(DATETIME,GETDATE()) AND STU_CURRSTATUS<>'CN')" _
                                & " FROM HOUSE_M AS B" _
                                & " WHERE  HOUSE_BSU_ID='" + hfBSU_ID.Value + "' AND HOUSE_DESCRIPTION<>''" _
                                & " GROUP BY HOUSE_ID,HOUSE_DESCRIPTION"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim id_M As String = ""
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_M = .Item(0).ToString.Trim.ToUpper + "_COUNT"
                strTable = strTable.Replace(id_M, .Item(2).ToString)
            End With
        Next

        Return strTable


    End Function


#End Region


End Class
