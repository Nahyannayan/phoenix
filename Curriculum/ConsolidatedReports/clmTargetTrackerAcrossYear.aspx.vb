Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CURRICULUM

Partial Class Curriculum_Reports_Aspx_clmTargetTrackerAcrossYear
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim currFns As New currFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                h_MnuCode.Value = ViewState("MainMnu_code")
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C400108" And ViewState("MainMnu_code") <> "C400110" And ViewState("MainMnu_code") <> "C400220" And ViewState("MainMnu_code") <> "C400129") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlPrevAcademicYear = studClass.PopulateAcademicYear(ddlPrevAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlPrevGrade = PopulateGrade(ddlPrevGrade, ddlPrevAcademicYear.SelectedValue.ToString)

                    ddlCurrAcademicYear = studClass.PopulateAcademicYear(ddlCurrAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlCurrGrade = PopulateGrade(ddlCurrGrade, ddlCurrAcademicYear.SelectedValue.ToString)

                    Dim grade As String()
                    grade = ddlPrevGrade.SelectedValue.Split("|")
                    BindSection()
                    BindReports()

                    BindCurrSection()
                    BindCurrReports()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id='" + acdid + "' order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function getReportCards() As String
        Dim str As String = ""
        Dim i As Integer

     
        str += ddlPrevReportSchedule.Items(i).Text + "|" + ddlCurrReportSchedule.SelectedItem.Text
       
        Return str
    End Function

  
    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlPrevGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE RSM_ACD_ID='" + ddlPrevAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'" _
                                & " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrevReportSchedule.DataSource = ds
        ddlPrevReportSchedule.DataTextField = "RPF_DESCR"
        ddlPrevReportSchedule.DataValueField = "RPF_ID"
        ddlPrevReportSchedule.DataBind()
    End Sub

    Sub BindCurrReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String() = ddlCurrGrade.SelectedValue.Split("|")
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE RSM_ACD_ID='" + ddlCurrAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade(0) + "'" _
                                & " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurrReportSchedule.DataSource = ds
        ddlCurrReportSchedule.DataTextField = "RPF_DESCR"
        ddlCurrReportSchedule.DataValueField = "RPF_ID"
        ddlCurrReportSchedule.DataBind()
    End Sub


    Sub BindSection()
        'Dim li As New ListItem
        'li.Text = "ALL"
        'li.Value = "0"

        'ddlPrevSection.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String
        'If ddlPrevGrade.SelectedValue = "" Then
        '    ddlPrevSection.Items.Add(li)
        'Else
        '    str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
        '               & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
        '               & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlPrevAcademicYear.SelectedValue.ToString

        '    Dim grade As String() = ddlPrevGrade.SelectedValue.Split("|")
        '    str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

        '    str_query += " ORDER BY SCT_DESCR"

        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        '    ddlPrevSection.DataSource = ds
        '    ddlPrevSection.DataTextField = "SCT_DESCR"
        '    ddlPrevSection.DataValueField = "SCT_ID"
        '    ddlPrevSection.DataBind()
        'ddlPrevSection.Items.Insert(0, li)
        'End If
    End Sub


    Sub BindCurrSection()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"

        ddlCurrSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlCurrGrade.SelectedValue = "" Then
            ddlCurrSection.Items.Add(li)
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlCurrAcademicYear.SelectedValue.ToString

            Dim grade As String() = ddlCurrGrade.SelectedValue.Split("|")
            str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"

            str_query += " ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlCurrSection.DataSource = ds
            ddlCurrSection.DataTextField = "SCT_DESCR"
            ddlCurrSection.DataValueField = "SCT_ID"
            ddlCurrSection.DataBind()
            ddlCurrSection.Items.Insert(0, li)
        End If
    End Sub



#End Region

    Protected Sub ddlPrevAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrevAcademicYear.SelectedIndexChanged
        Try

            ddlPrevGrade = PopulateGrade(ddlPrevGrade, ddlPrevAcademicYear.SelectedValue.ToString)
            Dim grade As String()
            grade = ddlPrevGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlPrevGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrevGrade.SelectedIndexChanged
        Try

            Dim grade As String()
            grade = ddlPrevGrade.SelectedValue.Split("|")
            BindSection()
            BindReports()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub




    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("add")


            Session("targettracker") = 2
            Dim display As String

            Dim url As String

            Dim rpf_id1 As String
            Dim rpf_id2 As String

            Dim rpf As String = ""

            rpf_id1 = ddlPrevReportSchedule.SelectedItem.Value
            rpf += ddlPrevReportSchedule.SelectedItem.Text + " " + ddlPrevAcademicYear.SelectedItem.Text

            rpf_id2 = ddlCurrReportSchedule.SelectedItem.Value
            rpf += "," + ddlCurrReportSchedule.SelectedItem.Text + " " + ddlCurrAcademicYear.SelectedItem.Text

            If Session("sbsuid") = "125010" Or Session("sbsuid") = "135010" Then
                If h_MnuCode.Value = "C400220" Then
                    display = "level"
                Else
                    display = "tracker"
                End If

                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)
                If ddlCurrReportSchedule.SelectedItem.Text.Contains("INTERIM") Then
                    url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_CIS.aspx?MainMnu_code={0}&datamode={1}" _
                               & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                               & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                               & "&grdid=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedValue.ToString) _
                               & "&grdid_prev=" + Encr_decrData.Encrypt(ddlPrevGrade.SelectedValue.ToString) _
                               & "&grade=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedItem.Text) _
                               & "&sctid=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedValue.ToString) _
                               & "&section=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedItem.Text) _
                               & "&acdid=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedValue.ToString) _
                               & "&acdid_prev=" + Encr_decrData.Encrypt(ddlPrevAcademicYear.SelectedValue.ToString) _
                               & "&term=" + Encr_decrData.Encrypt(rpf) _
                               & "&accyear=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedItem.Text) _
                               & "&display=" + display, ViewState("MainMnu_code"), ViewState("datamode"))
                Else
                    url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedTracking_TWS.aspx?MainMnu_code={0}&datamode={1}" _
                                    & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                                    & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                                    & "&grdid=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedValue.ToString) _
                                    & "&grdid_prev=" + Encr_decrData.Encrypt(ddlPrevGrade.SelectedValue.ToString) _
                                    & "&grade=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedItem.Text) _
                                    & "&sctid=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedValue.ToString) _
                                    & "&section=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedItem.Text) _
                                    & "&acdid=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedValue.ToString) _
                                    & "&acdid_prev=" + Encr_decrData.Encrypt(ddlPrevAcademicYear.SelectedValue.ToString) _
                                    & "&term=" + Encr_decrData.Encrypt(rpf) _
                                    & "&accyear=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedItem.Text) _
                                    & "&display=" + display, ViewState("MainMnu_code"), ViewState("datamode"))
                End If
            ElseIf h_MnuCode.Value = "C400110" Then


                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedTracking_CBSE.aspx?MainMnu_code={0}&datamode={1}" _
                                & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                                & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                                & "&grdid=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedValue.ToString) _
                                & "&grdid_prev=" + Encr_decrData.Encrypt(ddlPrevGrade.SelectedValue.ToString) _
                                & "&grade=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedItem.Text) _
                                & "&sctid=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedValue.ToString) _
                                & "&section=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedItem.Text) _
                                & "&acdid=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedValue.ToString) _
                                & "&acdid_prev=" + Encr_decrData.Encrypt(ddlPrevAcademicYear.SelectedValue.ToString) _
                                & "&term=" + Encr_decrData.Encrypt(rpf) _
                                & "&accyear=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedItem.Text) _
                                & "&opt=" + Encr_decrData.Encrypt(IIf(chkOption.Checked, "1", "0")) _
                                & "&display=0", ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf h_MnuCode.Value = "C400129" Then


                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedTracking_ICSE.aspx?MainMnu_code={0}&datamode={1}" _
                                & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                                & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                                & "&grdid=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedValue.ToString) _
                                & "&grdid_prev=" + Encr_decrData.Encrypt(ddlPrevGrade.SelectedValue.ToString) _
                                & "&grade=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedItem.Text) _
                                & "&sctid=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedValue.ToString) _
                                & "&section=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedItem.Text) _
                                & "&acdid=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedValue.ToString) _
                                & "&acdid_prev=" + Encr_decrData.Encrypt(ddlPrevAcademicYear.SelectedValue.ToString) _
                                & "&term=" + Encr_decrData.Encrypt(rpf) _
                                & "&accyear=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedItem.Text) _
                                & "&opt=" + Encr_decrData.Encrypt(IIf(chkOption.Checked, "1", "0")) _
                                & "&display=0", ViewState("MainMnu_code"), ViewState("datamode"))
            Else

                If h_MnuCode.Value = "C400220" Then
                    display = "level"
                Else
                    display = "tracker"
                End If


                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_CIS.aspx?MainMnu_code={0}&datamode={1}" _
                                 & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                                 & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                                 & "&grdid=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedValue.ToString) _
                                 & "&grdid_prev=" + Encr_decrData.Encrypt(ddlPrevGrade.SelectedValue.ToString) _
                                 & "&grade=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedItem.Text) _
                                 & "&sctid=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedValue.ToString) _
                                 & "&section=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedItem.Text) _
                                 & "&acdid=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedValue.ToString) _
                                 & "&acdid_prev=" + Encr_decrData.Encrypt(ddlPrevAcademicYear.SelectedValue.ToString) _
                                 & "&term=" + Encr_decrData.Encrypt(rpf) _
                                 & "&accyear=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedItem.Text) _
                                 & "&display=" + display, ViewState("MainMnu_code"), ViewState("datamode"))
            End If


            ResponseHelper.Redirect(url, "_blank", "")

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlCurrAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrAcademicYear.SelectedIndexChanged
        Try

            ddlCurrGrade = PopulateGrade(ddlCurrGrade, ddlCurrAcademicYear.SelectedValue.ToString)
            Dim grade As String()
            grade = ddlCurrGrade.SelectedValue.Split("|")
            BindCurrSection()
            BindCurrReports()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try


    End Sub

    Protected Sub ddlCurrGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrGrade.SelectedIndexChanged
        Try

            Dim grade As String()
            grade = ddlCurrGrade.SelectedValue.Split("|")
            BindCurrSection()
            BindCurrReports()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub btnLevelReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLevelReport.Click

        Try
            ViewState("datamode") = Encr_decrData.Encrypt("add")


            Session("targettracker") = 2


            Dim url As String

            Dim rpf_id1 As String
            Dim rpf_id2 As String

            Dim rpf As String = ""

            rpf_id1 = ddlPrevReportSchedule.SelectedItem.Value
            rpf += ddlPrevReportSchedule.SelectedItem.Text + " " + ddlPrevAcademicYear.SelectedItem.Text

            rpf_id2 = ddlCurrReportSchedule.SelectedItem.Value
            rpf += "," + ddlCurrReportSchedule.SelectedItem.Text + " " + ddlCurrAcademicYear.SelectedItem.Text

            If h_MnuCode.Value = "C400110" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidatedTracking_CBSE.aspx?MainMnu_code={0}&datamode={1}" _
                                & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                                & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                                & "&grdid=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedValue.ToString) _
                                & "&grdid_prev=" + Encr_decrData.Encrypt(ddlPrevGrade.SelectedValue.ToString) _
                                & "&grade=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedItem.Text) _
                                & "&sctid=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedValue.ToString) _
                                & "&section=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedItem.Text) _
                                & "&acdid=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedValue.ToString) _
                                & "&acdid_prev=" + Encr_decrData.Encrypt(ddlPrevAcademicYear.SelectedValue.ToString) _
                                & "&term=" + Encr_decrData.Encrypt(rpf) _
                                & "&accyear=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedItem.Text) _
                                & "&display=level", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(h_MnuCode.Value)
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsolidated_CIS.aspx?MainMnu_code={0}&datamode={1}" _
                                 & "&rpfid_prev=" + Encr_decrData.Encrypt(rpf_id1) _
                                 & "&rpfid=" + Encr_decrData.Encrypt(rpf_id2) _
                                 & "&grdid=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedValue.ToString) _
                                 & "&grdid_prev=" + Encr_decrData.Encrypt(ddlPrevGrade.SelectedValue.ToString) _
                                 & "&grade=" + Encr_decrData.Encrypt(ddlCurrGrade.SelectedItem.Text) _
                                 & "&sctid=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedValue.ToString) _
                                 & "&section=" + Encr_decrData.Encrypt(ddlCurrSection.SelectedItem.Text) _
                                 & "&acdid=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedValue.ToString) _
                                 & "&acdid_prev=" + Encr_decrData.Encrypt(ddlPrevAcademicYear.SelectedValue.ToString) _
                                 & "&term=" + Encr_decrData.Encrypt(rpf) _
                                 & "&accyear=" + Encr_decrData.Encrypt(ddlCurrAcademicYear.SelectedItem.Text) _
                                 & "&display=level", ViewState("MainMnu_code"), ViewState("datamode"))
            End If


            ResponseHelper.Redirect(url, "_blank", "")

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
