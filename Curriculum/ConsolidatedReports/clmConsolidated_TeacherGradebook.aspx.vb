﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_TeacherGradebook
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
        hfSBG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbg_id").Replace(" ", "+"))
        hfSGR_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sgr_id").Replace(" ", "+"))
        hfCAD_IDs.Value = Encr_decrData.Decrypt(Request.QueryString("cad_ids").Replace(" ", "+"))
        hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
        GridBind()
    End Sub
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String() = hfGRD_ID.Value.Split("|")
        Dim str_query As String = "ACT.getCONSOLIDATEDMARKS_TEACHERGRADEBOOK " + hfACD_ID.Value + "," _
                                & "'" + grade(0) + "'," _
                                & "'" + hfSBG_ID.Value + "'," _
                                & "'" + hfSGR_ID.Value + "'," _
                                & "'" + hfCAD_IDs.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            gvSubj.DataSource = ds
            gvSubj.DataBind()
        End If
    End Sub

    Function GetHeaders() As Hashtable
        Dim htHeader As New Hashtable
        Dim i As Integer
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "[ACT].[getCONSOLIDATEDMARKS_TEACHERGRADEBOOK_HEADERS] '" + hfCAD_IDs.Value + "','" + hfSGR_ID.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                htHeader.Add(ds.Tables(0).Rows(i).Item(0).ToString, ds.Tables(0).Rows(i).Item(1))
            Next
        End If
        Return htHeader
    End Function

    Protected Sub gvSubj_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubj.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then

            Dim lengthFunction As String
            lengthFunction += "function textHeightIncrease(txtBox,evt) {"
            '  lengthFunction += "txtBox.style.height = txtBox.scrollHeight +150+ ""px""; "
            lengthFunction += "txtBox.style.height = 200+ ""px""; "
            lengthFunction += "return true;"
            lengthFunction += "}"

            lengthFunction += "function isMaxLength1(txtBox,evt) {"
            lengthFunction += "txtBox.style.height = 20; "
            lengthFunction += "return true;"
            lengthFunction += "}"

            Dim i, j As Integer

            Dim htHeader As Hashtable = GetHeaders()


            Dim HeaderCell1 As New TableCell()
            Dim HeaderCell2 As New TableCell()
            Dim HeaderCell3 As New TableCell()
            Dim HeaderCell4 As New TableCell()

            Dim HeaderGridRow1 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow2 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow3 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderGridRow4 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)

            Dim Subject As String
            Dim Report As String
            Dim Header As String
            Dim hType As String

            Dim tSubject As String = ""
            Dim tReport As String = ""
            Dim tHeader As String = ""

            Dim scolspan As Integer = 1
            Dim rcolspan As Integer = 1
            Dim hcolspan As Integer = 1

            Dim bReduceCellheight As Boolean = False

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Student ID"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Name"
            HeaderGridRow1.Cells.Add(HeaderCell1)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = "Section"
            HeaderGridRow1.Cells.Add(HeaderCell1)


            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell2 = New TableCell()
            HeaderCell2.Text = ""
            HeaderGridRow2.Cells.Add(HeaderCell2)



            HeaderCell3 = New TableCell()
            HeaderCell3.Text = ""
            HeaderGridRow3.Cells.Add(HeaderCell3)

            HeaderCell3 = New TableCell()
            HeaderCell3.Text = ""
            HeaderGridRow3.Cells.Add(HeaderCell3)

            HeaderCell3 = New TableCell()
            HeaderCell3.Text = ""
            HeaderGridRow3.Cells.Add(HeaderCell3)


            For i = 3 To e.Row.Cells.Count - 1
                Dim shdr As String() = e.Row.Cells(i).Text.Split("__")

                If shdr.Length >= 2 Then
                    Subject = shdr(0)
                    If Subject <> tSubject And tSubject <> "" Then
                        tHeader = "-"
                        tReport = "-"
                    End If
                    If Report <> tReport And tReport <> "" Then
                        tHeader = "-"
                    End If

                    Header = htHeader.Item(shdr(4))
                    If Header <> tHeader Then
                        If tHeader <> "" Then
                            HeaderCell3.ColumnSpan = hcolspan
                            HeaderCell3.VerticalAlign = VerticalAlign.Top

                            HeaderGridRow3.Cells.Add(HeaderCell3)
                            'HeaderCell1.ColumnSpan = scolspan
                            'HeaderGridRow1.Cells.Add(HeaderCell1)
                            'HeaderCell1 = New TableCell()
                        End If

                        HeaderCell3 = New TableCell()

                        If Header.Length > 25 Then
                            bReduceCellheight = True
                            Dim txtHeader As New Label
                            'txtHeader.TextMode = TextBoxMode.MultiLine
                            txtHeader.Text = Header
                            txtHeader.Height = 20
                            txtHeader.Attributes.Add("style", "display:block; height:20; width:30;overflow:hidden;font-family: sans-serif !important;")
                            txtHeader.Attributes.Add("onkeydown", "return textHeightIncrease(this,event);")
                            txtHeader.Attributes.Add("onmousedown", "return textHeightIncrease(this,event);")
                            txtHeader.Attributes.Add("onblur", "return isMaxLength1(this,event);")
                            HeaderCell3.Controls.Add(txtHeader)
                            ClientScript.RegisterClientScriptBlock(Page.GetType(), txtHeader.ClientID, lengthFunction, True)
                            'Dim ltHeader As New Literal
                            'ltHeader.Text = "<div id=""inner-content-div"" style=""width:100%;height:200px;overflow-x: hidden;overflow-y: hidden;"">" + Header + "<div>"
                            'HeaderCell3.Controls.Add(ltHeader)

                            'Dim lblHeader As New Label
                            'lblHeader.Text = Header
                            'lblHeader.Height = 100
                            'HeaderCell3.Controls.Add(lblHeader)
                        Else
                            HeaderCell3.Text = Header
                        End If

                        HeaderCell3.VerticalAlign = VerticalAlign.Top
                        'scolspan = 1
                        hcolspan = 1
                        tHeader = Header
                        '  tSubject = ""
                    Else
                        hcolspan += 1
                    End If



                    Report = shdr(2)
                    If Report <> tReport Then
                        If tReport <> "" Then
                            HeaderCell2.ColumnSpan = rcolspan
                            HeaderGridRow2.Cells.Add(HeaderCell2)

                            'HeaderCell1.ColumnSpan = scolspan
                            'HeaderGridRow1.Cells.Add(HeaderCell1)
                            'HeaderCell1 = New TableCell()
                        End If

                        HeaderCell2 = New TableCell()
                        HeaderCell2.Text = Report
                        'scolspan = 1
                        rcolspan = 1
                        tReport = Report
                        '  tSubject = ""
                    Else
                        rcolspan += 1
                    End If

                    If Subject <> tSubject Then
                        If tSubject <> "" Then
                            HeaderCell1.ColumnSpan = scolspan
                            HeaderGridRow1.Cells.Add(HeaderCell1)
                        End If

                        HeaderCell1 = New TableCell()
                        HeaderCell1.Text = Subject
                        scolspan = 1
                        tSubject = Subject

                    Else
                        scolspan += 1
                    End If
                    If shdr(6) = "COMMENT" Then
                        e.Row.Cells(i).Text = "&nbsp;"
                    Else
                        e.Row.Cells(i).Text = shdr(6)
                    End If

                End If
            Next


            HeaderCell3 = New TableCell()
            HeaderCell3.Text = Header
            HeaderCell3.ColumnSpan = hcolspan
            HeaderCell3.VerticalAlign = VerticalAlign.Top
            If bReduceCellheight = True Then
                HeaderCell3.Height = 20
            End If
            HeaderGridRow3.Cells.Add(HeaderCell3)


            HeaderCell2 = New TableCell()
            HeaderCell2.Text = Report
            HeaderCell2.ColumnSpan = rcolspan
            HeaderGridRow2.Cells.Add(HeaderCell2)

            HeaderCell1 = New TableCell()
            HeaderCell1.Text = Subject
            HeaderCell1.ColumnSpan = scolspan
            HeaderGridRow1.Cells.Add(HeaderCell1)


            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow3)

            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow2)

            gvSubj.Controls(0).Controls.AddAt(0, HeaderGridRow1)
            e.Row.Cells(0).Text = ""
            e.Row.Cells(1).Text = ""
            e.Row.Cells(2).Text = ""
        End If
    End Sub
    Protected Sub gvSubj_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubj.RowDataBound
        Dim i As Integer
        Dim max_mark As String = "", valu As Double, marks_str As String(), marks As String(), mark As String
        If e.Row.RowType = DataControlRowType.Header Then
            For i = 0 To e.Row.Cells.Count - 1
                e.Row.Cells(i).Text = CStr(e.Row.Cells(i).Text)
                Dim str As String = e.Row.Cells(i).Text
                If str = "" Then str = "0"
                max_mark = max_mark + CStr(i) + "|" + str + "?"

            Next


            hfMarks.Value = max_mark
        End If
       

        If e.Row.RowType = DataControlRowType.DataRow Then
            marks_str = hfMarks.Value.Split("?")

            For i = 0 To e.Row.Cells.Count - 1
                If i = 0 Or i = 1 Then
                    e.Row.Cells(i).Text = CStr(e.Row.Cells(i).Text)
                    e.Row.Cells(i).HorizontalAlign = HorizontalAlign.Left


                ElseIf i > 2 Then

                    marks = marks_str(i).Split("|")
                    mark = marks(1)
                    If Session("sbsuid") = "121021" Then
                        With e.Row.Cells(i)

                            If IsNumeric(.Text) = True Then

                                valu = Math.Round((Val(.Text) / Val(mark)) * 100, 0)

                                If valu > 75 Then
                                    .BackColor = Drawing.Color.LightBlue
                                ElseIf valu > 40 And valu <= 75 Then
                                    .BackColor = Drawing.Color.Green
                                Else
                                    .BackColor = Drawing.Color.Red
                                End If



                            Else
                                Select Case .Text
                                    Case "A*", "A+", "A", "A-", "B+", "A1", "A2", "B1", "B2"
                                        .BackColor = Drawing.Color.SkyBlue
                                    Case "B", "C+", "C", "C-", "D+", "D", "C1", "C2", "D1", "D2"
                                        .BackColor = Drawing.Color.Green
                                    Case "E", "E1", "E2"
                                        .BackColor = Drawing.Color.Red
                                End Select
                            End If
                        End With
                    End If
                End If
            Next


        End If

    End Sub
   
End Class
