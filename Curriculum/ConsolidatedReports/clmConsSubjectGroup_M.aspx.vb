Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Math
Partial Class Curriculum_ConsolidatedReports_clmConsSubjectGroup_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330101") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                   
                    If ViewState("datamode") = "view" Then
                        hfCON_ID.Value = Encr_decrData.Decrypt(Request.QueryString("conid").Replace(" ", "+"))
                        BindGroupSubjects()

                        Dim li As New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                        ddlAcademicYear.Items.Add(li)

                        li = New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                        ddlGrade.Items.Add(li)

                        txtDesr.Text = Encr_decrData.Decrypt(Request.QueryString("descr").Replace(" ", "+"))
                        txtShort.Text = Encr_decrData.Decrypt(Request.QueryString("short").Replace(" ", "+"))
                        EnableDisableControls(False)
                    Else
                        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                        PopulateGrade()
                        hfCON_ID.Value = "0"
                    End If
                    BindSubjects()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub

#Region "Private Methods"

    Sub PopulateGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE WHEN STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'_'+STM_DESCR END AS GRM_DISPLAY," _
                                & "GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M AS A " _
                                & " INNER JOIN GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID" _
                                & " INNER JOIN STREAM_M AS C ON A.GRM_STM_ID=C.STM_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY GRD_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindSubjects()
        lstNotRequired.Items.Clear()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE ISNULL(SBG_bOPTIONAL,'FALSE')='TRUE'" _
                               & " AND SBG_ID NOT IN(SELECT CND_SBG_ID FROM CONSOLIDATED_SUBJECTGROUP_D)" _
                               & " AND SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_STM_ID=" + grade(1) _
                               & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstNotRequired.DataSource = ds
        lstNotRequired.DataTextField = "SBG_DESCR"
        lstNotRequired.DataValueField = "SBG_ID"
        lstNotRequired.DataBind()
    End Sub

    Sub BindGroupSubjects()
        lstRequired.Items.Clear()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                               & " INNER JOIN CONSOLIDATED_SUBJECTGROUP_D AS B ON A.SBG_ID=B.CND_SBG_ID" _
                               & " WHERE CND_CON_ID=" + hfCON_ID.Value _
                                & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstRequired.DataSource = ds
        lstRequired.DataTextField = "SBG_DESCR"
        lstRequired.DataValueField = "SBG_ID"
        lstRequired.DataBind()

        lstValuesReq.Value = ""

        Dim i As Integer
        For i = 0 To lstRequired.Items.Count - 1
            If lstValuesReq.Value <> "" Then
                lstValuesReq.Value += "|"
            End If
            lstValuesReq.Value += lstRequired.Items(i).Text + "$" + lstRequired.Items(i).Value

        Next
    End Sub


    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_query As String = " exec saveCONSOLIDATEDSUBJECTGROUP_M " _
                                & hfCON_ID.Value + "," _
                                & ddlAcademicYear.SelectedValue.ToString + "," _
                                & "'" + grade(0) + "'," _
                                & grade(1) + "," _
                                & "'" + txtDesr.Text + "'," _
                                & "'" + txtShort.Text + "'," _
                                & "'" + ViewState("datamode") + "'"


        hfCON_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Dim i As Integer
        Dim strReqs As String()
        Dim strReq As String()
        If ViewState("datamode") <> "DELETE" Then
            If lstValuesReq.Value.Length <> 0 Then
                strReqs = lstValuesReq.Value.Split("|")
                For i = 0 To strReqs.Length - 1
                    strReq = strReqs(i).Split("$")
                    str_query = " exec saveCONSOLIDATEDSUBJECTGROUP_D " _
                                & hfCON_ID.Value + "," _
                                & strReq(1)
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                Next
            End If
        End If
    End Sub

    Sub EnableDisableControls(ByVal Value As Boolean)
        txtDesr.Enabled = Value
        txtShort.Enabled = Value
        lstNotRequired.Enabled = Value
        lstRequired.Enabled = Value
    End Sub

    Sub ClearControls()
        txtDesr.Text = ""
        txtShort.Text = ""
        lstValuesNotReq.Value = ""
        lstValuesReq.Value = ""
    End Sub
#End Region


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
        PopulateGrade()
        BindSubjects()
        EnableDisableControls(True)
        ClearControls()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        lstRequired.Items.Clear()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        EnableDisableControls(True)
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        BindGroupSubjects()
        ViewState("datamode") = "view"
        EnableDisableControls(False)
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        lblError.Text = "Record saved successfully"
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateGrade()
        BindSubjects()
        lstRequired.Items.Clear()
        ClearControls()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubjects()
        lstValuesNotReq.Value = ""
        lstValuesReq.Value = ""
        lstRequired.Items.Clear()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        ViewState("datamode") = "DELETE"
        SaveData()
        lblError.Text = "Record saved successfully"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            lblError.Text = ""
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                ClearControls()
                EnableDisableControls(False)
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Else
                ViewState("datamode") = Encr_decrData.Encrypt("add")
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim url As String
                url = String.Format("~\Curriculum\ConsolidatedReports\clmConsSubjectGroup_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
