<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsolidatedBrownBookExcel_View.aspx.vb" Inherits="Curriculum_BrownBook_clmBrownBook_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Brown Book"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error"
                                EnableViewState="False" ForeColor="" HeaderText="You must enter a value in the following fields:"
                                SkinID="error" ValidationGroup="groupM1" Width="442px"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr colspan="4">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" Style="position: relative" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Grade</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters"><span class="field-label">Section</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" ValidationGroup="groupM1" />
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

