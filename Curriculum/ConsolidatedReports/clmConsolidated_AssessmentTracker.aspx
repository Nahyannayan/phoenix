﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmConsolidated_AssessmentTracker.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_AssessmentTracker" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="gvSubj" runat="server" Font-Size="X-Small" HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" Font-Names="Arial">
        <RowStyle HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" />
        <AlternatingRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" />
         </asp:GridView>
    </div>
    <asp:HiddenField ID="hfRPF_IDs" runat="server" />
    <asp:HiddenField ID="hfSBG_ID" runat="server" />
    <asp:HiddenField ID="hfGRD_ID" runat="server" />
    <asp:HiddenField ID="hfACD_ID" runat="server" />
    <asp:HiddenField ID="hfSCT_ID" runat="server" />
    <asp:HiddenField ID="hfbBlank" runat="server" />
    </form>
</body>
</html>
