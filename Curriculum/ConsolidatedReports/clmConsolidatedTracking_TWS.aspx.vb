Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Drawing
Partial Class Curriculum_ConsolidatedReports_clmConsolidated_TWS
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_InitComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim grd_id As String() = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+")).Split("|")
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
            'hfSubmitDate.Value = Encr_decrData.Decrypt(Request.QueryString("submitdate").Replace(" ", "+"))

            If Session("PromotionSheet") = 1 Then
                hfPromotionSheet.Value = 1
            Else
                hfPromotionSheet.Value = 0
            End If

            hfGRD_ID.Value = grd_id(0)
            hfSTM_ID.Value = grd_id(1)
            ViewState("arrowkey") = "0"
            If Session("targettracker") = 1 Then
                hfRPF_ID_PREV.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid_prev").Replace(" ", "+"))
            ElseIf Session("targettracker") = 2 Then
                hfRPF_ID_PREV.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid_prev").Replace(" ", "+"))
                hfACD_ID_PREV.Value = Encr_decrData.Decrypt(Request.QueryString("acdid_prev").Replace(" ", "+"))
                Dim grdprev As String() = Encr_decrData.Decrypt(Request.QueryString("grdid_prev").Replace(" ", "+")).Split("|")
                hfGRD_ID_PREV.Value = grdprev(0)
            Else
                hfRSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
                hfSchedule.Value = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))
                hfRPF_DESCR.Value = Encr_decrData.Decrypt(Request.QueryString("rpf").Replace(" ", "+"))
            End If
            hfRPF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid").Replace(" ", "+"))

            hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))

            GetACDCurrent()


            hfAccYear.Value = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
            hfTERM.Value = Encr_decrData.Decrypt(Request.QueryString("term").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330110") Then
            'If Not Request.UrlReferrer Is Nothing Then
            '    Response.Redirect(Request.UrlReferrer.ToString())
            'Else

            '    Response.Redirect("~\noAccess.aspx")
            'End If

            ' Else
            'calling pageright class to get the access rights

            ViewState("display") = Request.QueryString("display").Replace(" ", "+")
            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights
            hfBSU_ID.Value = Session("sbsuid")
            hfAColor.Value = "#3399FF"
            hfBColor.Value = "#ACE7EF"
            hfCColor.Value = "#ECC808" 'yellow
            hfDColor.Value = "#FFFF00"

            hfEColor.Value = "#16BE4F"
            hfFColor.Value = "#A1F7BE"

            hfAplusColor.Value = "#DF7DE7"


            hfGColor.Value = "#EFACAC"
            hfUColor.Value = "#FF6633" 'red



            hfRecordNumber.Value = 0
            hfTableColumns.Value = 0
            hfPageNumber.Value = 0



            GetConsolidated(grd_id(0), Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+")), grd_id(1))






            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
            'End If
        End If
    End Sub
#Region "Private Methods"


    Sub GetACDCurrent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT convert(int,ACD_CURRENT) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value
        hfACD_CURRENT.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub
    Sub SetHowParent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE') FROM " _
                               & " FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfShowParent1.Value = ds.Tables(0).Rows(0).Item(0)
        hfShowParent2.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetConsolidated(ByVal grd_id As String, ByVal sct_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim Subjects As String(,) = GetConsolidatedSubjects(grd_id, hfACD_ID.Value, stm_id)
        Dim x As Integer = Subjects.GetLength(0)
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")

        sb.AppendLine("<HTML><HEAD><TITLE>:::: CONSOLIDATED REPORT ::::</TITLE>")


        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        '    sb.AppendLine("<Link REL=STYLESHEET HREF=""..\cssfiles\brownbook.css"" TYPE=""text/css"">")
        sb.AppendLine("<BODY>")
        ' sb.AppendLine("<Script language=""JavaScript"" src=""../include/com_client_func.js""></Script>")
        ' sb.AppendLine("<style>	body,table,td{background-color:#ffffff!important;}")
        ' sb.AppendLine("th{background-color:#ffffff!important;}")
        'sb.AppendLine("</style>")
        If sct_id = "" Or sct_id = "0" Then
            str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                      & "' AND SCT_ACD_ID=" + hfACD_ID.Value
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetConsolidatedForSection(grd_id, .Rows(i).Item(0).ToString, hfACD_ID.Value, Subjects))
                Next
            End With
            'ltBrownBook.Text = sb.ToString
        Else
            sb.AppendLine(GetConsolidatedForSection(grd_id, sct_id, hfACD_ID.Value, Subjects))

            'ltBrownBook.Text = sb.ToString
        End If
        sb.AppendLine("</BODY></HTML>")
        Response.Write(sb.ToString())


    End Sub

    Function GetConsolidatedSubjects(ByVal grd_id As String, ByVal acd_id As String, ByVal stm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_DESCR,SBG_ID,ISNULL(SBG_bMAJOR,'FALSE') AS SBG_bMAJOR,SBG_ORDER,SBG_SHORTCODE,'0' AS OPTIONAL FROM SUBJECTS_GRADE_S " _
                               & " WHERE SBG_GRD_ID='" + grd_id + "' AND SBG_ACD_ID=" + acd_id _
                               & " AND SBG_STM_ID=" + stm_id + " AND SBG_bMAJOR=1 AND SBG_PARENT_ID=0  ORDER BY SBG_ORDER"



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer = 0
        i = ds.Tables(0).Rows.Count


        Dim j As Integer

        Dim Subjects(,) As String

        ReDim Subjects(i - 1, 10)

        For j = 0 To i - 1
            With ds.Tables(0)
                Subjects(j, 0) = .Rows(j).Item(0).ToString
                Subjects(j, 1) = .Rows(j).Item(1).ToString
                Subjects(j, 2) = .Rows(j).Item(2).ToString
                Subjects(j, 3) = .Rows(j).Item(4).ToString
                Subjects(j, 4) = "Cu"
                Subjects(j, 5) = .Rows(j).Item(5).ToString
            End With
        Next
        Return Subjects
    End Function

    Function GetSubjectsHeader(ByVal Subjects(,) As String) As String
        Dim sb As New StringBuilder
        Dim i As Integer

        Dim strSubjects As String = ""
        Dim strHeader As String = ""
        Dim strShort1 As String = ""
        Dim strShort As String = ""
        Dim imgUrl As String = ""
        Dim colSpan As Integer = 0
        Dim colSpan1 As Integer = 0

        hfTableColumns.Value = 0
        For i = 0 To Subjects.GetLength(0) - 1
            'if major subject add one more column



            strHeader += "<td  align=""middle"" Class=repcolDetail>G</td>"
            hfTableColumns.Value += 1


            strSubjects += "<td width=""30""  align=""middle"" valign=""bottom""  >" + Subjects(i, 3) + "</td>"
            hfTableColumns.Value += 1
        Next



        sb.AppendLine("<table border=""1"" bordercolorlight=""#000000""   align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font-family: sans-serif !important;"" >")

        sb.AppendLine("<tr >")
        sb.AppendLine("<td width=""20"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center> Sr. No  </td>")
        sb.AppendLine("<td width=""60"" ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center>Student ID</td>")
        sb.AppendLine("<td  width=300 ALIGN=""middle"" valign=""middle"" rowspan=2 Class=repcolDetail ><center>Name of the Student</td>")

        If strShort <> "" Then
            sb.AppendLine("<td valign=""middle""  colspan=" + colSpan1.ToString + " align=""middle"" Class=repcolDetail>&nbsp;</td>")
        End If

        hfTableColumns.Value += 5
        sb.AppendLine(strSubjects)


        If hfGRD_ID.Value = "01" Or hfGRD_ID.Value = "02" Or hfGRD_ID.Value = "03" Or hfGRD_ID.Value = "04" Then
            sb.AppendLine("<td colspan=5>Total Attainment</td>")
        Else
            sb.AppendLine("<td colspan=9>Total Attainment</td>")
        End If




        hfTableColumns.Value += 6
        sb.AppendLine("</tr>")



        sb.AppendLine("<tr>")
        If strShort <> "" Then
            sb.Append(strShort)
        End If
        sb.AppendLine(strHeader)

        Select Case hfGRD_ID.Value
            Case "01", "02", "03", "04", "05", "06"
                sb.AppendLine("<td BGCOLOR=" + hfAplusColor.Value + "  width=""40"" align=""middle"" valign=""middle""  >A+</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfAColor.Value + " width=""40"" align=""middle"" valign=""middle""  >A</td>")
                sb.AppendLine("<td BGCOLOR=" + hfBColor.Value + " width=""40"" align=""middle"" valign=""middle""  >B</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfCColor.Value + " width=""40"" align=""middle"" valign=""middle""  >C</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfDColor.Value + " width=""40"" align=""middle"" valign=""middle""  >D</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfEColor.Value + " width=""40"" align=""middle"" valign=""middle""  >E</td>")
            Case Else
                sb.AppendLine("<td BGCOLOR=" + hfAColor.Value + "  width=""40"" align=""middle"" valign=""middle""  >A</td>")
                sb.AppendLine("<td BGCOLOR=" + hfBColor.Value + " width=""40"" align=""middle"" valign=""middle""  >B</td>")
                sb.AppendLine("<td BGCOLOR=" + hfCColor.Value + " width=""40"" align=""middle"" valign=""middle""  >C</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfDColor.Value + " width=""40"" align=""middle"" valign=""middle""  >D</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfEColor.Value + " width=""40"" align=""middle"" valign=""middle""  >E</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfFColor.Value + " width=""40"" align=""middle"" valign=""middle""  >F</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfGColor.Value + " width=""40"" align=""middle"" valign=""middle""  >G</td>")
                sb.AppendLine("<td  BGCOLOR=" + hfUColor.Value + " width=""40"" align=""middle"" valign=""middle""  >U</td>")
        End Select
        sb.AppendLine("</tr>")

        Return sb.ToString
    End Function


    Function GetConsolidatedForSection(ByVal grd_id As String, ByVal sct_id As String, ByVal acd_id As String, ByVal Subjects As String(,)) As String
        Dim sb As New StringBuilder
        Dim strHeader As String = GetConsolidatedHeader(sct_id)
        ' sb.AppendLine(strHeader)
        sb.AppendLine("<table width=""1400""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

        sb.AppendLine("<tr><td algn=center>")
        sb.AppendLine(strHeader)
        sb.AppendLine("</td></tr>")

        sb.AppendLine("<tr><td algn=center>")

        sb.AppendLine(GetSubjectsHeader(Subjects))

        sb.AppendLine(GetStudentMarks(sct_id, Subjects))

        'sb.AppendLine("</table>")

        'sb.AppendLine("</td></tr>")
        'sb.AppendLine("</table>")

        Return sb.ToString
    End Function

    Function GetConsolidatedHeader(ByVal sct_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_NAME,BSU_BB_LOGO,ACY_DESCR,GRM_DISPLAY,SCT_DESCR," _
                               & " ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') " _
                               & " AS EMP_NAME,ACD_MOEAFFLNO FROM ACADEMICYEAR_D AS A " _
                               & " INNER JOIN BUSINESSUNIT_M AS B ON A.ACD_BSU_ID=B.BSU_ID" _
                               & " INNER JOIN GRADE_BSU_M AS C ON A.ACD_ID=C.GRM_ACD_ID" _
                               & " INNER JOIN SECTION_M AS D ON C.GRM_ID=D.SCT_GRM_ID" _
                               & " INNER JOIN ACADEMICYEAR_M AS F ON A.ACD_ACY_ID=F.ACY_ID" _
                               & " LEFT OUTER JOIN EMPLOYEE_M AS E ON D.SCT_EMP_ID=E.EMP_ID" _
                               & " WHERE SCT_ID=" + sct_id

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sb As New StringBuilder

        While reader.Read
            sb.AppendLine("<table   align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")

            sb.AppendLine("<TR>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=bottom>")


            sb.AppendLine("<table width=""90%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0 style=""text-decoration:NONE;font-family: sans-serif !important;"">")
            sb.AppendLine("<TR >")
            sb.AppendLine("<TD>GRADE :    " + reader("GRM_DISPLAY") + "   &nbsp;&nbsp; SECTION : " + reader("SCT_DESCR") + " </TD> </TR>")

            sb.AppendLine("</Table>")

            sb.AppendLine("</TD>")


            sb.AppendLine("<TD WIDTH=40% VALIGN=TOP>")

            sb.AppendLine("<table width=""100%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD align=""center"" valign=""TOP""><font style=""text-decoration:NONE;font-family: sans-serif !important;"">" + reader("BSU_NAME"))

            If Session("targettracker") = 1 Then
                sb.AppendLine("<BR>" + "TARGET TRACKER(" + hfTERM.Value.ToUpper.Replace("REPORT", "") + ")" + reader("ACY_DESCR") + "</TD></TR>")
            ElseIf ViewState("display") = "level" Then
                sb.AppendLine("<BR>" + "LEVEL TRACKER(" + hfTERM.Value.ToUpper.Replace("REPORT", "") + ")</TD></TR>")
            ElseIf Session("targettracker") = 2 Then
                sb.AppendLine("<BR>" + "TARGET TRACKER(" + hfTERM.Value.ToUpper.Replace("REPORT", "") + ")</TD></TR>")
            Else
                sb.AppendLine("<BR>" + hfTERM.Value.ToUpper.Replace("REPORT", "") + " SUMMARY SHEET  " + reader("ACY_DESCR") + "</TD></TR>")
            End If



            sb.AppendLine("</table>")

            sb.AppendLine("</TD>")

            sb.AppendLine("<TD WIDTH=30% VALIGN=TOP ALIGN=""RIGHT"">")
            sb.AppendLine("<table width=""80%""  align=""center"" border=""0"" cellspacing=""0"" cellpadding=0>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;</TD></TR>")
            sb.AppendLine("<TR><TD><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;</TD></TR>")
            sb.AppendLine("</table>")
            sb.AppendLine("</TD>")

            sb.AppendLine("</TR>")

            sb.AppendLine("<TR><TD colspan=2><font style=""text-decoration:NONE;font-family: sans-serif !important;"">&nbsp;&nbsp;&nbsp;&nbsp; Class Teacher :  " + reader("EMP_NAME") + " </TD>")
            sb.AppendLine("<TD align=right><font style=""text-decoration:NONE;font-family: sans-serif !important;""> Date : " + Now.Date + " </TD>")
            sb.AppendLine("</TR>")
            sb.AppendLine("</Table><BR>")

            hfClassTeacher.Value = reader("EMP_NAME")
        End While

        Return sb.ToString

    End Function


    Function GetStudentMarks(ByVal sct_id As String, ByVal Subjects(,) As String)
        Dim strTable As String

        Dim strFooter As String = GetConsolidatedFooter(Subjects)

        Dim strStudents As String = GetStudents(sct_id, Subjects)


        strTable = strStudents
        If hfLastRecord.Value = 0 Then

            strTable += strFooter
        End If

        Dim strMark As String = ""
        Dim strGrade As String = ""

        Dim id_C As String = ""
        Dim id_G As String = ""
        Dim i As Integer

        Dim strStuId As String = ""
        Dim strRetestSubjects As String = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim grade As String
        Dim arrow As String
        'POPULATE STUDENT MARKS

        Dim levelchange As String = ""

        Dim str_query As String
        If Session("targettracker") = 1 Then
            str_query = "exec [RPT].[rptTARGETTRACKER_cbse] " _
                       & hfACD_ID.Value + "," _
                       & "'" + hfGRD_ID.Value + "'," _
                       & IIf(hfRPF_ID_PREV.Value = "0", "NULL", hfRPF_ID_PREV.Value) + "," _
                       & hfRPF_ID.Value + "," _
                       & sct_id
        ElseIf Session("targettracker") = 2 Then
            str_query = "exec [RPT].[rptTARGETTRACKERACROSSYEAR_CBSE] " _
                       & hfACD_ID.Value + "," _
                       & hfACD_ID_PREV.Value + "," _
                       & "'" + hfGRD_ID.Value + "'," _
                       & "'" + hfGRD_ID_PREV.Value + "'," _
                       & IIf(hfRPF_ID_PREV.Value = "0", "NULL", hfRPF_ID_PREV.Value) + "," _
                       & hfRPF_ID.Value + "," _
                       & sct_id
        Else
            str_query = "exec [RPT].[rptSTUDENTCONSOLIDATED_CIS] " _
                             & hfACD_ID.Value + "," _
                             & "'" + hfGRD_ID.Value + "'," _
                             & hfRSM_ID.Value + "," _
                             & hfRPF_ID.Value + "," _
                             & sct_id
        End If




        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_G = "*" + .Item(0).ToString + "_" + .Item(3).ToString + "_G*"
                id_C = "*" + .Item(0).ToString + "_" + .Item(3).ToString + "_COLOR*"
                grade = .Item("RST_GRADING")
                arrow = .Item("ARROW")
                'levelchange = .Item("LEVELCHANGE").ToString
                If ViewState("display") = "level" Then
                    levelchange = .Item("RST_GRADING_PREV") + "/" + .Item("RST_GRADING")
                Else
                    levelchange = .Item("LEVELCHANGE").ToString
                End If

                If levelchange = "0" Then
                    levelchange = ""
                End If


                Select Case grade
                    Case "A+"
                        strTable = strTable.ToString.Replace(id_C, hfAplusColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_A+*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_A+*")) + 1
                    Case "A"
                        strTable = strTable.ToString.Replace(id_C, hfAColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_A*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_A*")) + 1
                    Case "B"
                        strTable = strTable.ToString.Replace(id_C, hfBColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_B*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_B*")) + 1
                    Case "C"
                        strTable = strTable.ToString.Replace(id_C, hfCColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_C*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_C*")) + 1
                    Case "D"
                        strTable = strTable.ToString.Replace(id_C, hfDColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_D*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_D*")) + 1
                    Case "E"
                        strTable = strTable.ToString.Replace(id_C, hfEColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_R*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_E*")) + 1
                    Case "F"
                        strTable = strTable.ToString.Replace(id_C, hfFColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_F*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_F*")) + 1
                    Case "G"
                        strTable = strTable.ToString.Replace(id_C, hfGColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_G*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_G*")) + 1
                    Case "U"
                        strTable = strTable.ToString.Replace(id_C, hfUColor.Value)
                        ViewState("hColor").item("*" + .Item(0).ToString + "_U*") = Val(ViewState("hColor").item("*" + .Item(0).ToString + "_U*")) + 1

                    Case Else
                        strTable = strTable.ToString.Replace(id_C, "#ffffff")
                End Select

                If arrow = "UP" Then
                    strTable = strTable.ToString.Replace(id_G, "<SUB>" + levelchange + "</SUB>" + "↑")
                ElseIf arrow = "DOWN" Then
                    strTable = strTable.ToString.Replace(id_G, "<SUB>" + levelchange + "</SUB>" + "↓")
                ElseIf arrow = "SAME" Then
                    If ViewState("display") = "level" Then
                        strTable = strTable.ToString.Replace(id_G, "<SUB>" + levelchange + "</SUB>")
                    Else
                        strTable = strTable.ToString.Replace(id_G, "↔")
                    End If
                ElseIf grade = "-" Then
                    strTable = strTable.ToString.Replace(id_G, "-")
                Else
                    If ViewState("display") = "level" Then
                        strTable = strTable.ToString.Replace(id_G, "<SUB>" + levelchange + "</SUB>")
                    Else
                        strTable = strTable.ToString.Replace(id_G, "&nbsp;")
                    End If

                End If
            End With
        Next


        str_query = "SELECT STU_ID FROM VW_STUDENT_DETAILS_PREVYEARS WHERE STU_SCT_ID=" + sct_id + " AND STU_ACD_ID=" + hfACD_ID.Value

        Dim id_T As String

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                id_T = "*" + .Item(0).ToString + "_A+*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_A*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_B*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_C*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_D*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_E*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_F*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_G*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

                id_T = "*" + .Item(0).ToString + "_U*"
                strTable = strTable.Replace(id_T, ViewState("hColor").item(id_T))

            End With
        Next


        Return strTable
    End Function



    Function GetStudents(ByVal sct_id As String, ByVal Subjects(,) As String) As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ' Dim strSubjectHeader As String = GetSubjectsHeader(Subjects)
        'Dim strPageHeader As String = GetConsolidatedHeader(sct_id)
        Dim i As Integer
        Dim j As Integer
        Dim sb As New StringBuilder
        Dim stuNames As String()
        Dim strMain As String
        Dim strSub As String

        Dim strRedLine As String

        Dim str_query As String

        Dim hColor As New Hashtable

        If hfACD_CURRENT.Value = 1 Then


            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                          & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                          & " ISNULL(STU_NO,'&nbsp') AS STU_NO, " _
                          & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                          & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                          & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                          & " STU_ID,'' AS HOUSE FROM OASIS..STUDENT_M AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                          & " WHERE STU_SCT_ID=" + sct_id + " AND STU_CURRSTATUS='EN' " _
                          & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        Else

            str_query = "SELECT STU_NAME=ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')" _
                       & " +' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,''), " _
                       & " ISNULL(STU_NO,'&nbsp') AS STU_NO, " _
                       & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                       & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                       & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END," _
                       & " STU_ID,'' AS HOUSE FROM VW_STUDENT_DETAILS_PREVYEARS AS A INNER JOIN OASIS..STUDENT_D AS B ON A.STU_SIBLING_ID=B.STS_STU_ID " _
                       & " WHERE STU_SCT_ID=" + sct_id + " AND STU_CURRSTATUS='EN' AND STU_ACD_ID=" + hfACD_ID.Value _
                       & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalStudents.Value += ds.Tables(0).Rows.Count


        If ds.Tables(0).Rows.Count = 0 Then
            Return ""
        End If
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                If i = 29 Then
                    sb.AppendLine("</table>")
                    sb.AppendLine("</td></tr>")
                    sb.AppendLine("</table>")

                    sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                    sb.AppendLine(GetConsolidatedHeader(sct_id))
                    sb.AppendLine(GetSubjectsHeader(Subjects))

                End If
                stuNames = .Item(0).ToString.Replace("  ", " ").Split(" ")
                sb.AppendLine("<tr height=""13px"">")
                sb.AppendLine("<td align=""middle"" Class=repcolDetail><CENTER>" + (i + 1).ToString + "</td>")
                sb.AppendLine("<td align=""middle""  Class=repcolDetail><CENTER>" + .Item("STU_NO") + "</td>")
                If (hfShowParent1.Value.ToUpper = "TRUE" And stuNames.Length = 1) Or (hfShowParent2.Value.ToUpper = "TRUE" And stuNames.Length = 2) Then
                    sb.AppendLine("<td width=300 align=""left"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "/" + .Item(3).ToString.ToUpper + "</td>")
                Else
                    sb.AppendLine("<td width=300 align=""left"" Class=repcolDetail>&nbsp;" + .Item("STU_NAME").ToString.ToUpper + "</td>")
                End If

                strMain = ""
                strSub = ""

                For j = 0 To Subjects.GetLength(0) - 1

                    strMain += "<td bgcolor=*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_COLOR" + "* align=""CENTER""><font >*" + .Item(4).ToString + "_" + Subjects(j, 1) + "_" + "G" + "*</font></td>"
                Next

                sb.AppendLine(strMain)

                Select Case hfGRD_ID.Value
                    Case "01", "02", "03", "04", "05", "06"
                        sb.AppendLine("<td bgcolor=" + hfAplusColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_A+*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfAColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_A*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfBColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_B*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfCColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_C*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfDColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_D*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfEColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_E*</font></td>")
                    Case Else
                        sb.AppendLine("<td bgcolor=" + hfAColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_A*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfBColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_B*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfCColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_C*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfDColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_D*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfEColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_E*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfFColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_F*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfGColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_G*</font></td>")
                        sb.AppendLine("<td bgcolor=" + hfUColor.Value + " align=""middle"" ><font style=""font-family: sans-serif !important;"">*" + .Item("STU_ID").ToString + "_U*</font></td>")
                End Select


                sb.AppendLine("</tr>")


                Select Case hfGRD_ID.Value
                    Case "01", "02", "03", "04", "05", "06"
                        hColor.Add("*" + .Item("STU_ID").ToString + "_A+*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_A*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_B*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_C*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_D*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_E*", 0)
                    Case Else
                        hColor.Add("*" + .Item("STU_ID").ToString + "_A*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_B*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_C*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_D*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_E*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_F*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_G*", 0)
                        hColor.Add("*" + .Item("STU_ID").ToString + "_U*", 0)
                End Select


                ViewState("hColor") = hColor
            End With
        Next


        sb.AppendLine("</table>")

        sb.AppendLine("</td></tr>")
        sb.AppendLine("</table>")


        Return sb.ToString
    End Function


    Function GetConsolidatedFooter(ByVal subjects(,) As String) As String
        Dim sb As New StringBuilder
        sb.AppendLine("</table>")

        sb.AppendLine("<table")
        sb.AppendLine("<tr><td valign=top>")

        If hfRPF_ID_PREV.Value <> "0" Then

            sb.AppendLine("</td><td valign=top>")
            sb.AppendLine("<table border=""1""  bordercolorlight=""#000000""   align=""center"" cellspacing=""0"" cellpadding=""0""   style=""text-decoration:NONE;font-family: sans-serif !important;"" >")
            sb.AppendLine("<tr><td colspan=2 align=center  bgcolor=#CC99CC>KEY</td></tr>")
            sb.AppendLine("<tr><td align=center>Making Progress</td><td align=center>↑</td></tr>")
            sb.AppendLine("<tr><td align=center>No Change</td><td  align=center>↔</td></tr>")
            sb.AppendLine("<tr><td align=center>Declined</td><td align=center>↓</td ></tr>")
            sb.AppendLine("</table>")
        End If


        Return sb.ToString
    End Function







#End Region


End Class
