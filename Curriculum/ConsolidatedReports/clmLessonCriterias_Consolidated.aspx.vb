Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Drawing
Partial Class Curriculum_ConsolidatedReports_clmLessonCriterias_Consolidated
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            h_SYD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("syd_id").Replace(" ", "+"))
            h_SBG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbg_id").Replace(" ", "+"))
            lblText.Text = Encr_decrData.Decrypt(Request.QueryString("Subject").Replace(" ", "+"))
            GridBind()
        End If
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC SYL.LESSON_CRITERIAS_CONSOLIDATED '" + h_SYD_ID.Value + "','" + h_SBG_ID.Value + "'"
        '   Dim str_query As String = "EXEC SYL.LESSON_CRITERIAS_CONSOLIDATED 11976,29793"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i, j, p As Integer
        Dim colspan As Integer = 0
        Dim strData As String = ""

        Dim str As New StringBuilder
        p = ds.Tables(0).Columns.Count
        str.AppendLine("<table border=1 bordercolor='black' cellpadding=0 cellspacing=0 style='border-collapse:collapse'>")
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 1 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    If i = 2 Or i = 4 Or i = 6 Then
                        str.AppendLine("<tr style='background:#99CCFF'>")
                    Else
                        str.AppendLine("<tr>")
                    End If

                    If i = 1 Then
                        str.AppendLine("<td rowspan=""7"" style=""-ms-transform:rotate(270deg);-webkit-transform:rotate(270deg);transform:rotate(270deg);""><b>")
                        If .Item(0).ToString = "" Then
                            str.AppendLine("&nbsp;")
                        Else
                            str.AppendLine(.Item(0).ToString)
                        End If
                        str.AppendLine("</b></td>")


                        str.AppendLine("<td rowspan=""7"" style=""-ms-transform:rotate(270deg);-webkit-transform:rotate(270deg);transform:rotate(270deg);"">")
                        If .Item(0).ToString = "" Then
                            str.AppendLine("&nbsp;")
                        Else
                            str.AppendLine(.Item(1).ToString)
                        End If

                        str.AppendLine("</td>")

                    End If

                    If i = 8 Or i = 9 Then
                        str.AppendLine("<td colspan=3>")
                        str.AppendLine("<b>" + .Item(3).ToString + "</b>")
                        str.AppendLine("</td>")
                    Else
                        str.AppendLine("<td >")
                        If .Item(3).ToString = "" Then
                            str.AppendLine("&nbsp;")
                        Else
                            str.AppendLine("<i><b>" + .Item(3).ToString + "</b></i>")
                        End If
                        str.AppendLine("</td>")

                    End If



                    strData = .Item(4).ToString
                    colspan = 1
                    If (i = 1 Or i = 3 Or i = 5) Then
                        For j = 5 To p - 1
                            If strData = .Item(j).ToString Then
                                colspan += 1
                            Else
                                str.AppendLine("<td colspan=" + colspan.ToString + ">")
                                If strData = "" Then
                                    str.AppendLine("&nbsp;")
                                Else
                                    str.AppendLine(strData)
                                End If
                                str.AppendLine("</td>")
                                strData = .Item(j).ToString
                                colspan = 1
                            End If
                        Next
                        str.AppendLine("<td colspan=" + colspan.ToString + ">")
                        If strData = "" Then
                            str.AppendLine("&nbsp;")
                        Else
                            str.AppendLine(strData)
                        End If
                        str.AppendLine("</td>")

                    Else
                        For j = 4 To p - 1
                            str.AppendLine("<td>")
                            If .Item(j).ToString = "" Then
                                str.AppendLine("&nbsp;")
                            Else
                                If i = 9 Then
                                    str.AppendLine("<a href=""" + .Item(j).ToString + """ target=""_blank"">" + .Item(j).ToString + "</a>")
                                Else
                                    str.AppendLine(.Item(j).ToString)
                                End If
                            End If
                            str.AppendLine("</td>")
                        Next
                    End If

                    str.AppendLine("</tr>")

                End With
            Next
        End If
        str.AppendLine("</table>")
        Literal1.Text = str.ToString
    End Sub

   
End Class
