<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsolidatedStudent_View.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidatedStudent_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Student Consolidated List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table id="tblrule" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%-- <tr>
                        <td valign="middle" class="subheader_img" colspan="3">
                           </td>
                    </tr>--%>
                    <tr>
                        
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>                  
                    <tr runat="server">
                       
                        <td align="left" width="20%"><span class="field-label">Section</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:CheckBox ID="chkTC" runat="server" Text="Include TC Cases"></asp:CheckBox></td>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                Text="Generate Report" ValidationGroup="groupM1" /></td>
                        
                    </tr>
                </table>



            </div>
        </div>
    </div>

</asp:Content>

