Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_ConsolidatedReports_clmObjtrackStudentObjectivesExcel
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lblBsu.Text = Session("bsu_name")
            lblTitle.Text = "Subject : " + Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+")) + " Group : " + Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
            h_SGR_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sgr_id").Replace(" ", "+"))
            h_SBM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sbm_id").Replace(" ", "+"))
            h_LVL_ID.Value = Encr_decrData.Decrypt(Request.QueryString("lvl_id").Replace(" ", "+"))
            h_OBT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("obt_id").Replace(" ", "+"))
            h_filter.Value = Encr_decrData.Decrypt(Request.QueryString("filter").Replace(" ", "+"))

            GridBind()
        End If
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET
        '     server control at run time. 

    End Sub
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec OBJTRACK.[rptOBJTRACKSTUDENTOBJECTIVES_EXCEL] " _
                                  & " '" + Session("sbsuid") + "'," _
                                  & h_SGR_ID.Value + "," _
                                  & h_SBM_ID.Value + "," _
                                  & h_LVL_ID.Value + "," _
                                  & h_OBT_ID.Value + "," _
                                  & "'" + h_filter.Value + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvObjectives.DataSource = ds
        gvObjectives.DataBind()

        'Dim frm As New HtmlForm
        '' divExport.Parent.Controls.Add(frm)
        ''  frm.Controls.Add(divExport)

        'Dim sw As New StringWriter
        'Dim hw As New HtmlTextWriter(sw)
        'divExport.RenderControl(hw)


        '' Dim tw As New TextWriter
        '' Page.CreateHtmlTextWriterFromType(hw, HtmlTextWriter)

        'Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", "StudentData.xls"))
        'Response.ContentType = "application/ms-excel"
        'Response.Write(hw.ToString)
        'Response.End()
    End Sub

    Protected Sub gvObjectives_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvObjectives.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim i As Integer
            For i = 2 To e.Row.Cells.Count - 1
                If e.Row.Cells(i).Text = "Y" Then
                    e.Row.Cells(i).ForeColor = Drawing.Color.Green
                ElseIf e.Row.Cells(i).Text = "I" Then
                    e.Row.Cells(i).ForeColor = Drawing.Color.Orange
                End If
                e.Row.Cells(i).Font.Bold = True
            Next
        End If
    End Sub
End Class
