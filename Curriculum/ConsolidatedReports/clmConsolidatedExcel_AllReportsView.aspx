﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmConsolidatedExcel_AllReportsView.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidatedExcel_AllReportsView"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_lstSubject') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }


        function fnSelectAll_bkp(chkObj) {
            var multi = document.getElementById("<%=lstSubject.ClientID%>");
            if (multi != null && multi.options != null) {
                if (chkObj.checked)
                    for (i = 0; i < multi.options.length; i++)
                    multi.options[i].selected = true;
                else
                    for (i = 0; i < multi.options.length; i++)
                    multi.options[i].selected = false;
            }
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Label ID="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <asp:Label ID="lblerror" runat="server" CssClass="error" align="left"></asp:Label>
    <table id="tblrule" runat="server" align="center" width="100%">
        
        <tr>
            <td align="left" width="15%"><span class="field-label">Academic Year</span>
            </td>
           
            <td align="left">
                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" />
            </td>
            <td align="left" valign="middle" width="15%"><span class="field-label">Grade</span>
            </td>
            
            <td align="left" valign="middle">
                <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                </asp:DropDownList>
            </td>
            <td align="left" valign="middle" width="15%"><span class="field-label">Section</span>
            </td>
            
            <td align="left" valign="middle">
                <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">Reportcard</span>
            </td>
            
            <td align="left" colspan="5">
                <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                    EmptyDataText="No Records Found" PageSize="20" ShowFooter="True"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle Wrap="False" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect1" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RPF_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblRsmId" runat="server" Text='<%# Bind("RSM_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RPF_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblRpfId" runat="server" Text='<%# Bind("RPF_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ReportCard">
                            <ItemTemplate>
                                <asp:Label ID="lblRpf" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Headers">
                            <ItemTemplate>
                                
                                <asp:CheckBoxList ID="lstHeader" class="checkbox-list" runat="server" 
                                    RepeatLayout="Flow"
                                    Width="100%">
                                </asp:CheckBoxList>
                                  
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="griditem" />
                    <RowStyle CssClass="griditem" />
                    <SelectedRowStyle CssClass="Green" />
                    <AlternatingRowStyle CssClass="griditem" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trSubject" runat="server">
            <td align="left" valign="middle"><span class="field-label">Subject</span>
            </td>
           
            <td align="left" valign="middle" colspan="2">
                
                <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server"
                    Text="Select All" />
                
                <div class="checkbox-list">
                <asp:CheckBoxList ID="lstSubject" runat="server" RepeatLayout="Flow">
                </asp:CheckBoxList>
                    </div>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="Tr1" runat="server">
            <td align="left" valign="middle"><span class="field-label">Include Subject Criterias</span>
            </td>
            
            <td align="left" valign="middle">
                <asp:CheckBox ID="chkCriterias" runat="server" />
            </td>
            <td align="left" valign="middle"><span class="field-label">Display Grades Only</span>
            </td>
           
            <td align="left" valign="middle">
                <asp:CheckBox ID="chkDisplayGrades" runat="server" />
            </td>

            <td align="left" valign="middle"><span class="field-label">Include TC Students</span>
            </td>
           
            <td align="left" valign="middle">
                <asp:CheckBox ID="chkTc" runat="server" />
            </td>
        </tr>
        <tr >
            <td align="left" valign="middle" colspan="6">
                <table border="0">
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkGender" runat="server" class="field-label" Text="Gender" /></td>
                        <td>
                            <asp:CheckBox ID="chkDOJ" runat="server" class="field-label" Text="Date of Join" /></td>
                        <td>
                            <asp:CheckBox ID="chkDOB" runat="server" class="field-label" Text="Date of Birth" /></td>
                        <td>
                            <asp:CheckBox ID="chkNationality" runat="server" class="field-label" Text="Nationality" />
                        </td>
                    </tr>
                </table>

            </td>

        </tr>
        <tr runat="server" id="trfilter">
            <td align="left" valign="middle"><span class="field-label">Filter By</span>
            </td>
            
            <td align="left" valign="middle" colspan="2">
                <asp:DropDownList ID="ddlType" runat="server">
                    <asp:ListItem>ALL</asp:ListItem>
                    <asp:ListItem>SEN</asp:ListItem>
                    <asp:ListItem>EAL</asp:ListItem>
                    <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                    <asp:ListItem>EMIRATI</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left" colspan="7" style="text-align: center">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                    Text="Generate Report" ValidationGroup="groupM1" />
                &nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfComments" runat="server"></asp:HiddenField>

                </div>
            </div>
        </div>

</asp:Content>
