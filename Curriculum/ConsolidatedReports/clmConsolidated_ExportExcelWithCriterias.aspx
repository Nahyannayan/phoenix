<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmConsolidated_ExportExcelWithCriterias.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidated_ExportExcelWithCriterias" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <asp:Literal ID="ltBrownBook" runat="server"></asp:Literal>
        <br />
        <asp:HiddenField ID="hfSBG_IDS" runat="server" />
        <asp:HiddenField ID="hfRowSpan" runat="server" />
        <br />
        <asp:HiddenField ID="hfRSD_IDS" runat="server" /><asp:HiddenField ID="hfRecordNumber" runat="server" Visible="False" />
        <asp:HiddenField ID="hfPageNumber" runat="server" Visible="False" />
        <asp:HiddenField ID="hfTableColumns" runat="server" Visible="False" />
        <br />
        <asp:HiddenField ID="hfLastRecord" runat="server" Value="0" Visible="False" />
        </div><asp:HiddenField ID="hfClassTeacher" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfSubmitDate" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfPassedCount" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfTotalStudents" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfDetained" runat="server" Value="0" Visible="False" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfGRM_DISPLAY" runat="server" />
         <asp:HiddenField ID="hfSCT_ID" runat="server" />
        <asp:HiddenField ID="hfSTM_ID" runat="server" />
        <asp:HiddenField ID="hfShowParent1" runat="server" />
        <asp:HiddenField ID="hfShowParent2" runat="server" /><asp:HiddenField ID="hfTERM" runat="server" />
        <asp:HiddenField ID="hfPromotionSheet" runat="server" />
        &nbsp;
        <asp:HiddenField ID="hfRSM_ID" runat="server" />
        <asp:HiddenField ID="hfRPF_ID" runat="server" /><asp:HiddenField ID="hfRPF_DESCR" runat="server" />
        <asp:HiddenField ID="hfSchedule" runat="server" /><asp:HiddenField ID="hfBSU_ID" runat="server" />
        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfAccYear" runat="server" />
        <asp:HiddenField ID="hfType" runat="server" />
        <asp:HiddenField ID="hfData" runat="server" />
        <asp:HiddenField ID="hfACD_Current" runat="server" 
        EnableViewState="False" />
        <br />
        <asp:HiddenField ID="hfbFinal" runat="server" EnableViewState="False" />
        <asp:HiddenField ID="hfTC" runat="server" EnableViewState="False" />
    </form>
</body>
</html>
