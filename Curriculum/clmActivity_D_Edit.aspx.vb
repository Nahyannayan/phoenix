Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class clmActivity_D_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100016") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call callYEAR_DESCRBind()
                    Call CALLACTIVITY_M()
                    Call CALLTERM_DESCRBind()
                    Call GetEmpID()
                    'txtCAD_DESC.Attributes("onfocus") = "this.style.backgroundColor='yellow';"
                    'txtCAD_DESC.Attributes("onblur") = "this.style.backgroundColor='white';"

                    If ViewState("datamode") = "view" Then
                        h_Row.Value = "1"
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Call GETACTIVITY_D()
                        tr20.Visible = False
                        tr21.Visible = False
                        tr22.Visible = False
                        DISABLE_CONTROL()
                    ElseIf ViewState("datamode") = "add" Then
                        h_Row.Value = "2"
                        Session("ACTIVITY_DETAIL") = CreateDataTable()
                        ViewState("SRNO") = 1
                        gridbind()
                        

                        btnUpdate.Visible = False
                        btnUpdateCancel.Visible = False
                        tr20.Visible = True
                        tr21.Visible = True
                        tr22.Visible = True
                       
                    End If

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub

    Sub DISABLE_CONTROL()
        ddlAca_Year.Enabled = False
        ddlCAD_CAM_ID.Enabled = False
        ddlTRM_ID.Enabled = False
        txtCAD_DESC.Enabled = False

    End Sub
    Sub ENABLE_CONTROL()
        ddlAca_Year.Enabled = True
        ddlCAD_CAM_ID.Enabled = True
        ddlTRM_ID.Enabled = True
        txtCAD_DESC.Enabled = True
    End Sub
    Sub GETACTIVITY_D()

        Using readerACTIVITY_D As SqlDataReader = ACTIVITYMASTER.GetACTIVITY_D(ViewState("viewid"))
            While readerACTIVITY_D.Read

                If Not ddlAca_Year.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_ACD_ID"))) Is Nothing Then
                    ddlAca_Year.ClearSelection()
                    ddlAca_Year.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_ACD_ID"))).Selected = True
                End If

                If Not ddlTRM_ID.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_TRM_ID"))) Is Nothing Then
                    ddlTRM_ID.ClearSelection()
                    ddlTRM_ID.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_TRM_ID"))).Selected = True
                End If


                If Not ddlCAD_CAM_ID.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_CAM_ID"))) Is Nothing Then
                    ddlCAD_CAM_ID.ClearSelection()
                    ddlCAD_CAM_ID.Items.FindByValue(Convert.ToString(readerACTIVITY_D("CAD_CAM_ID"))).Selected = True
                End If

             
                txtCAD_DESC.Text = Convert.ToString(readerACTIVITY_D("CAD_DESC"))

                chkHasAOLExam.Checked = readerACTIVITY_D("CAD_bAOL")
                chkWithoutSkills.Checked = readerACTIVITY_D("CAD_bWITHOUTSKILLS")
                chkSkills.Checked = readerACTIVITY_D("CAD_bSKILLS")

                If chkHasAOLExam.Checked = True Then
                    chkWithoutSkills.Visible = True
                End If
            End While
        End Using
    End Sub
  
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.String"))
            Dim ACD_DESC As New DataColumn("ACD_DESC", System.Type.GetType("System.String"))
            ' Dim GRD_DESC As New DataColumn("GRD_DESC", System.Type.GetType("System.String"))
            Dim TRM_DESC As New DataColumn("TRM_DESC", System.Type.GetType("System.String"))
            Dim MAIN_ACT As New DataColumn("MAIN_ACT", System.Type.GetType("System.String"))
            Dim SUB_ACT As New DataColumn("SUB_ACT", System.Type.GetType("System.String"))
            Dim CAD_ACD_ID As New DataColumn("CAD_ACD_ID", System.Type.GetType("System.String"))
            Dim CAD_CAM_ID As New DataColumn("CAD_CAM_ID", System.Type.GetType("System.String"))
            'Dim CAD_GRD_ID As New DataColumn("CAD_GRD_ID", System.Type.GetType("System.String"))
            Dim CAD_TRM_ID As New DataColumn("CAD_TRM_ID", System.Type.GetType("System.String"))
            Dim CAD_bHasAOL As New DataColumn("CAD_bAOL", System.Type.GetType("System.Boolean"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            Dim CAD_bWITHOUTSKILLS As New DataColumn("CAD_bWITHOUTSKILLS", System.Type.GetType("System.Boolean"))
            Dim CAD_bSKILLS As New DataColumn("CAD_bSKILLS", System.Type.GetType("System.Boolean"))
            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(ACD_DESC)
            ' dtDt.Columns.Add(GRD_DESC)
            dtDt.Columns.Add(TRM_DESC)
            dtDt.Columns.Add(MAIN_ACT)
            dtDt.Columns.Add(SUB_ACT)
            dtDt.Columns.Add(CAD_ACD_ID)
            dtDt.Columns.Add(CAD_CAM_ID)
            ' dtDt.Columns.Add(CAD_GRD_ID)
            dtDt.Columns.Add(CAD_TRM_ID)
            dtDt.Columns.Add(CAD_bHasAOL)
            dtDt.Columns.Add(STATUS)
            dtDt.Columns.Add(CAD_bWITHOUTSKILLS)
            dtDt.Columns.Add(CAD_bSKILLS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function
    Sub gridbind()
        Try
            gvACT_Detail.DataSource = Session("ACTIVITY_DETAIL")
            gvACT_Detail.DataBind()
            Call DisableControls()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub DisableControls()

        For j As Integer = 0 To gvACT_Detail.Rows.Count - 1

            Dim row As GridViewRow = gvACT_Detail.Rows(j)

            Dim lblSTATUS As New Label

            lblSTATUS = DirectCast(row.FindControl("lblSTATUS"), Label)
            Dim Stud_ID_STATUS As String = lblSTATUS.Text
            If UCase(Stud_ID_STATUS) = "PENDING" Then
                DirectCast(row.FindControl("lblSTATUS"), Label).Visible = False
            Else
                If UCase(Stud_ID_STATUS) = "APPROVED" Then
                    lblSTATUS.ForeColor = Drawing.Color.Green
                ElseIf UCase(Stud_ID_STATUS) = "NOT APPROVED" Then
                    lblSTATUS.ForeColor = Drawing.Color.Red
                End If
                DirectCast(row.FindControl("lblEdit"), LinkButton).Visible = False
            End If

        Next
    End Sub
    Sub gridbind_add()
        Try
            Dim i As Integer



            'For i = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1

            '    If Session("ACTIVITY_DETAIL").Rows(i)("STATUS") = "PENDING" Then

            '        lblError.Text = "No new leave approval can be added,since pending approval exist for the current student!!!"
            '        Exit Sub
            '    End If

            'Next



            Dim rDt As DataRow
            '
            rDt = Session("ACTIVITY_DETAIL").NewRow

            rDt("SRNO") = ViewState("SRNO")
            rDt("ID") = CInt(ViewState("SRNO"))
            rDt("ACD_DESC") = ddlAca_Year.SelectedItem.Text
            ' rDt("GRD_DESC") = item.Text
            rDt("TRM_DESC") = ddlTRM_ID.SelectedItem.Text
            rDt("MAIN_ACT") = ddlCAD_CAM_ID.SelectedItem.Text
            rDt("SUB_ACT") = txtCAD_DESC.Text
            rDt("CAD_ACD_ID") = ddlAca_Year.SelectedItem.Value
            rDt("CAD_CAM_ID") = ddlCAD_CAM_ID.SelectedItem.Value
            'rDt("CAD_GRD_ID") = item.Value
            rDt("CAD_TRM_ID") = ddlTRM_ID.SelectedItem.Value
            rDt("CAD_bAOL") = chkHasAOLExam.Checked
            rDt("STATUS") = "NEW"
            rDt("CAD_bWITHOUTSKILLS") = chkWithoutSkills.Checked
            rDt("CAD_bSKILLS") = chkSkills.Checked
            Session("ACTIVITY_DETAIL").Rows.Add(rDt)
            ViewState("SRNO") = ViewState("SRNO") + 1
            gridbind()
        Catch ex As Exception
            lblError.Text = "Check Student Leave date"
        End Try
    End Sub
    Protected Sub btnAddDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlAca_Year.Enabled = False
        ddlTRM_ID.Enabled = False

        gridbind_add()
    End Sub
    Protected Sub gvACT_Detail_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        btnSave.Enabled = False
        btnUpdateCancel.Visible = True
        gvACT_Detail.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = gvACT_Detail.Rows(e.NewEditIndex)
        gvACT_Detail.Columns(5).Visible = False
        gvACT_Detail.Columns(6).Visible = False
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)
        'For Each item As ListItem In chkGRD_ID.Items
        '    If item.Selected = True Then
        '        item.Selected = False
        '    End If
        'Next
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
            If iIndex = Session("ACTIVITY_DETAIL").Rows(iEdit)("ID") Then
                If Not ddlCAD_CAM_ID.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_CAM_ID")) Is Nothing Then
                    ddlCAD_CAM_ID.ClearSelection()
                    ddlCAD_CAM_ID.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_CAM_ID")).Selected = True
                End If
                'For Each item As ListItem In chkGRD_ID.Items
                '    If item.Value = Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID") Then
                '        item.Selected = True
                '    End If
                'Next
                'If Not ddlGRD_ID_EDIT.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID")) Is Nothing Then
                '    ddlGRD_ID_EDIT.ClearSelection()
                '    ddlGRD_ID_EDIT.Items.FindByValue(Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID")).Selected = True
                'End If
                txtCAD_DESC.Text = Session("ACTIVITY_DETAIL").Rows(iEdit)("SUB_ACT")
                Exit For
            End If
        Next

        btnAddDetail.Visible = False
        btnUpdate.Visible = True
        btnUpdateCancel.Visible = True

        gridbind()
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        
        btnSave.Enabled = True
        btnUpdateCancel.Visible = False
        Dim row As GridViewRow = gvACT_Detail.Rows(gvACT_Detail.SelectedIndex)
        gvACT_Detail.Columns(5).Visible = True
        gvACT_Detail.Columns(6).Visible = True

        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        'For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
        'Next
        For iEdit = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
            If iIndex = Session("ACTIVITY_DETAIL").Rows(iEdit)("ID") Then
                Session("ACTIVITY_DETAIL").Rows(iEdit)("MAIN_ACT") = ddlCAD_CAM_ID.SelectedItem.Text
                'Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_GRD_ID") = ddlGRD_ID_EDIT.SelectedItem.Value
                Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_CAM_ID") = ddlCAD_CAM_ID.SelectedItem.Value
                'Session("ACTIVITY_DETAIL").Rows(iEdit)("GRD_DESC") = ddlGRD_ID_EDIT.SelectedItem.Text
                Session("ACTIVITY_DETAIL").Rows(iEdit)("SUB_ACT") = txtCAD_DESC.Text
                Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_bAOL") = chkHasAOLExam.Checked
                Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_bWITHOUTSKILLS") = chkWithoutSkills.Checked
                Session("ACTIVITY_DETAIL").Rows(iEdit)("CAD_bSKILLS") = chkSkills.Checked
                Exit For
            End If
        Next

        btnUpdate.Visible = False
        btnAddDetail.Visible = True
        gvACT_Detail.SelectedIndex = -1
        gridbind()

    End Sub
    Protected Sub gvACT_Detail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)


        Dim COND_ID As Integer = CInt(gvACT_Detail.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0
        While i < Session("ACTIVITY_DETAIL").Rows.Count
            If Session("ACTIVITY_DETAIL").rows(i)("Id") = COND_ID Then
                Session("ACTIVITY_DETAIL").rows(i).delete()
            Else
                i = i + 1
            End If
        End While
        gridbind()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        str_err = CallTransaction(errorMessage)
        If str_err = "0" Then
            DISABLE_CONTROL()
            lblError.Text = "Record updated successfully"
        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Private Function CallTransaction(ByRef errorMessage As String) As String
        Dim STATUS As Integer
        Dim RecordSTATUS As String = String.Empty
        Dim bEdit As Boolean

        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim ACT_XML As String = getACT_XML()
                    If ACT_XML = "" Then
                        lblError.Text = "No records selected"
                    Else
                        STATUS = ACTIVITYMASTER.SAVEACTIVITY_D(ACT_XML, ViewState("EMP_ID"), bEdit, transaction)
                    End If

                    If STATUS <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Error while updating records"
                        Return "1"
                    Else
                        ' STATUS = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), , Session("ACTIVITY_DETAIL").Rows(i)("ID"), "add", Page.User.Identity.Name.ToString, Me.Page)
                        If STATUS <> 0 Then
                            CallTransaction = "1"
                            errorMessage = "Could not process audit request"
                            Return "1"
                        End If
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                Catch ex As Exception
                    errorMessage = "Record could not be Updated"
                Finally
                    If CallTransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try
            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    bEdit = True
                    Dim ACT_XML As String = getACT_XML_EDIT()
                    If ACT_XML = "" Then
                        lblError.Text = "No records selected"
                    Else
                        STATUS = ACTIVITYMASTER.SAVEACTIVITY_D(ACT_XML, ViewState("EMP_ID"), bEdit, transaction)
                    End If

                    If STATUS <> 0 Then
                        CallTransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(STATUS)
                        Return "1"
                    Else
                        'STATUS = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), Session("ACTIVITY_DETAIL").Rows(i)("ID"), "edit", Page.User.Identity.Name.ToString, Me.Page)
                        If STATUS <> 0 Then
                            CallTransaction = "1"
                            errorMessage = "Could not process audit request"
                            Return "1"
                        End If
                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                Catch ex As Exception
                    errorMessage = "Record could not be Updated"
                Finally
                    If CallTransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try
            End Using
        End If

    End Function

    Function getACT_XML_EDIT() As String
        Dim str As String = String.Empty
        Dim ID As String = ViewState("viewid")
        Dim CAD_DESC As String = txtCAD_DESC.Text
        Dim CAD_ACD_ID As String = ddlAca_Year.SelectedValue
        Dim CAD_CAM_ID As String = ddlCAD_CAM_ID.SelectedValue
        Dim CAD_TRM_ID As String = ddlTRM_ID.SelectedValue
        Dim CAD_bAOL As String = chkHasAOLExam.Checked.ToString
        Dim CAD_bWITHOUTSKILLS As String = chkWithoutSkills.Checked.ToString
        Dim CAD_bSKILLS As String = chkSkills.Checked.ToString
            ' Dim CDD_GRD_ID As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_GRD_ID").ToString

        str += String.Format("<ACTIVITY ACTID='{0}'><ACT_DETAIL ACD_ID='{1}' TRM_ID='{2}' CAD_DESC='{3}'  CAM_ID='{4}' CAD_bAOL='{5}' CAD_bWITHOUTSKILLS='{6}' CAD_bSKILLS='{7}' /></ACTIVITY>", ID, CAD_ACD_ID, CAD_TRM_ID, CAD_DESC, CAD_CAM_ID, CAD_bAOL, CAD_bWITHOUTSKILLS, CAD_bSKILLS)



            If str <> "" Then
                str = "<ACTIVITYS>" + str + "</ACTIVITYS>"
            End If
        
            Return str
    End Function
    Function getACT_XML() As String
        Dim str As String = String.Empty



        If Session("ACTIVITY_DETAIL") IsNot Nothing Then

            For i As Integer = 0 To Session("ACTIVITY_DETAIL").Rows.Count - 1
                Dim ID As String = Session("ACTIVITY_DETAIL").Rows(i)("ID").ToString
                Dim CAD_DESC As String = Session("ACTIVITY_DETAIL").Rows(i)("SUB_ACT").ToString
                Dim CAD_ACD_ID As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_ACD_ID").ToString
                Dim CAD_CAM_ID As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_CAM_ID").ToString
                Dim CAD_TRM_ID As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_TRM_ID").ToString
                Dim CAD_bAOL As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_bAOL").ToString
                Dim CAD_bWITHOUTSKILLS As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_bWITHOUTSKILLS").ToString
                Dim CAD_bSKILLS As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_bSKILLS").ToString

                ' Dim CDD_GRD_ID As String = Session("ACTIVITY_DETAIL").Rows(i)("CAD_GRD_ID").ToString

                str += String.Format("<ACTIVITY ACTID='{0}'><ACT_DETAIL ACD_ID='{1}' TRM_ID='{2}' CAD_DESC='{3}'  CAM_ID='{4}' CAD_bAOL='{5}' CAD_bWITHOUTSKILLS='{6}' CAD_bSKILLS='{7}' /></ACTIVITY>", ID, CAD_ACD_ID, CAD_TRM_ID, CAD_DESC, CAD_CAM_ID, CAD_bAOL, CAD_bWITHOUTSKILLS, CAD_bSKILLS)
            Next


            If str <> "" Then
                str = "<ACTIVITYS>" + str + "</ACTIVITYS>"
            End If
        Else
            str = ""
        End If
        Return str
    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_Row.Value = "1"
        ViewState("datamode") = "edit"
        enable_control()
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        tr20.Visible = False
        tr21.Visible = False
        tr22.Visible = False
        ENABLE_CONTROL()
        ddlAca_Year.Enabled = False
        ddlTRM_ID.Enabled = False

    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ENABLE_CONTROL()
        h_Row.Value = "2"
        ViewState("datamode") = "add"
        ENABLE_CONTROL()
        tr20.Visible = True
        tr21.Visible = True
        tr22.Visible = True
        btnUpdate.Visible = False
        btnUpdateCancel.Visible = False
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            disable_control()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
           
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     VW_ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, vw_ACADEMICYEAR_D.ACD_ID AS  ACD_ID " & _
" FROM  vw_ACADEMICYEAR_D INNER JOIN   VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID " & _
" WHERE vw_ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and (vw_ACADEMICYEAR_D.ACD_BSU_ID='" & Session("sBsuid") & "') order by vw_ACADEMICYEAR_D.ACD_ACY_ID "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            If Not ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                ddlAca_Year.ClearSelection()
                ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            End If
            'ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CALLTERM_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
            str_Sql = " SELECT    TRM_ID, TRM_DESCRIPTION FROM  VW_TRM_M WHERE TRM_ACD_ID= '" & ACD_ID & "' ORDER BY TRM_DESCRIPTION "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlTRM_ID.Items.Clear()
            ddlTRM_ID.DataSource = ds.Tables(0)
            ddlTRM_ID.DataTextField = "TRM_DESCRIPTION"
            ddlTRM_ID.DataValueField = "TRM_ID"
            ddlTRM_ID.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'Sub CALLGRD_DESCRBind_EDIT()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '        Dim str_Sql As String
    '        Dim ds As New DataSet
    '        Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
    '        str_Sql = " SELECT  VW_GRADE_BSU_M.GRM_GRD_ID AS GRM_GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY  AS GRM_DISPLAY" & _
    '       "  FROM VW_GRADE_BSU_M INNER JOIN VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID WHERE VW_GRADE_BSU_M.GRM_ACD_ID= '" & ACD_ID & "' ORDER BY  VW_GRADE_M.GRD_DISPLAYORDER"
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        ddlGRD_ID_EDIT.Items.Clear()
    '        ddlGRD_ID_EDIT.DataSource = ds.Tables(0)
    '        ddlGRD_ID_EDIT.DataTextField = "GRM_DISPLAY"
    '        ddlGRD_ID_EDIT.DataValueField = "GRM_GRD_ID"
    '        ddlGRD_ID_EDIT.DataBind()
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub
    Sub CALLACTIVITY_M()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     CAM_ID, CAM_DESC FROM ACT.ACTIVITY_M WHERE CAM_BSU_ID = '" & Session("sBSUID") & "'ORDER BY CAM_DESC"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlCAD_CAM_ID.Items.Clear()
            ddlCAD_CAM_ID.DataSource = ds.Tables(0)
            ddlCAD_CAM_ID.DataTextField = "CAM_DESC"
            ddlCAD_CAM_ID.DataValueField = "CAM_ID"
            ddlCAD_CAM_ID.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'Sub CALLGRD_DESCRBind()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '        Dim str_Sql As String
    '        Dim ds As New DataSet
    '        Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
    '        str_Sql = " SELECT  VW_GRADE_BSU_M.GRM_GRD_ID AS GRM_GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY  AS GRM_DISPLAY" & _
    '       "  FROM VW_GRADE_BSU_M INNER JOIN VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID WHERE VW_GRADE_BSU_M.GRM_ACD_ID= '" & ACD_ID & "' ORDER BY  VW_GRADE_M.GRD_DISPLAYORDER"
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        chkGRD_ID.Items.Clear()
    '        Dim row As DataRow
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            ViewState("ChangeEvent") = 1
    '            For Each row In ds.Tables(0).Rows
    '                Dim str1 As String = row("GRM_DISPLAY")
    '                Dim str2 As String = row("GRM_GRD_ID")
    '                chkGRD_ID.Items.Add(New ListItem(str1, str2))
    '            Next
    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub
    'Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'If ViewState("datamode") = "add" Then
    '    'CALLGRD_DESCRBind()
    '    '' Else
    '    'Call CALLGRD_DESCRBind_EDIT()
    '    '' End If


    '    'If ViewState("datamode") = "add" Then

    '    '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
    '    '                                 "<script language=javascript>HideRows(2);</script>")
    '    'Else
    '    '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
    '    '                                                         "<script language=javascript>HideRows(1);</script>")
    '    'End If

    'End Sub
    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub
    Protected Sub gvACT_Detail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvACT_Detail.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub btnUpdateCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlCAD_CAM_ID.ClearSelection()
        ddlCAD_CAM_ID.SelectedIndex = 0
        txtCAD_DESC.Text = ""
        gridbind()
    End Sub

   
    Protected Sub chkHasAOLExam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHasAOLExam.CheckedChanged
        If chkHasAOLExam.Checked = True Then
            chkWithoutSkills.Visible = True
        Else
            chkWithoutSkills.Visible = False
            chkWithoutSkills.Checked = False
        End If
    End Sub
End Class
