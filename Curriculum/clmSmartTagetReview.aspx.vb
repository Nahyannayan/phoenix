﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Partial Class Curriculum_clmSmartTagetReview
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300220") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))


                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))


                    BindSetup()
                    BindGrade()

                    BindSubject()
                    BindGroup()

                    gridbind()



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindSetup()
        ddlSetup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TGM_ID,TGM_DESCR  from smart.TARGET_SETUP_m " _
                                       & " WHERE TGM_ACD_ID=" + ddlAcademicYear.SelectedValue
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSetup.DataSource = ds
        ddlSetup.DataTextField = "TGM_DESCR"
        ddlSetup.DataValueField = "TGM_ID"
        ddlSetup.DataBind()
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + ddlAcademicYear.SelectedValue _
                              & " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
    End Sub
    Sub BindSubject()


        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_PARENTS+'-'+SBG_SHORTCODE END AS SBG_DESCR FROM SUBJECTS_GRADE_S "
        str_query += " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue


        If ddlGrade.SelectedValue <> "" Then


            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)


        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
     
    End Sub
    Public Sub BindGroup()
        ddlgroup.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_sql As String = ""
        Dim grade As String()


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")
            strCondition += " AND SGR_GRD_ID='" & grade(0) & "'"
        End If
        If ddlSubject.SelectedValue <> "" Then
            strCondition += " AND SGR_SBG_ID='" & ddlSubject.SelectedValue & "'"
        End If

        If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            strCondition += " AND SGS_EMP_ID=" & Session("EmployeeId") & ""
            str_sql = "SELECT  DISTINCT(SGR_ID) ID, SGR_DESCR DESCR2, SBM_DESCR DESCR1 FROM GROUPS_M " & _
              " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID INNER JOIN GROUPS_TEACHER_S GTS ON GTS.SGS_SGR_ID=GROUPS_M.SGR_ID " & _
              " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGS_TODATE IS NULL  AND SGR_ACD_ID='" + ddlAcademicYear.SelectedValue + "'"

            str_sql += strCondition
        Else
            str_sql = "SELECT SGR_ID AS ID,SGR_ID AS DESCR1,SGR_DESCR AS DESCR2 FROM dbo.GROUPS_M " _
                      & " WHERE SGR_BSU_ID='" & Session("sBsuId") & "' AND SGR_ACD_ID='" + ddlAcademicYear.SelectedValue + "'"
            'AND SGR_GRD_ID='" & strGRD_IDs & "' AND SGR_SBG_ID=" & vSBM_IDs & " ORDER BY SGR_ID "
            str_sql += strCondition & " ORDER BY SGR_DESCR"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddlgroup.DataSource = ds
        ddlgroup.DataTextField = "DESCR2"
        ddlgroup.DataValueField = "ID"
        ddlgroup.DataBind()
      
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindSetup()
        BindGrade()
        BindSubject()
        BindGroup()
        gridbind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        BindGroup()
        gridbind()
    End Sub

    Protected Sub ddlSetup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSetup.SelectedIndexChanged
        gridbind
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""
            Dim grade As String()
            Dim ds As New DataSet
            lblError.Text = ""
            grdStud.DataSource = Nothing
            grdStud.DataBind()

            grade = ddlGrade.SelectedValue.Split("|")
            str_Sql = "select  TSD_ID,STU_NO,STU_NAME ,TSD_DESCR,isnull(TSD_bAPPROVED,'false')as Approved ,isnull(TSD_bREVIEW,'false')as Review " _
                      & " from smart.TARGET_STUDENT_s inner join vw_STUDENT_DETAILS on TSD_STU_ID =STU_ID " _
                      & " inner join STUDENT_GROUPS_S  on STU_ID=SSD_STU_ID " _
                      & " where TSD_ACD_ID =" + ddlAcademicYear.SelectedValue + " and TSD_GRD_ID ='" + grade(0) + "' and TSD_SBG_ID =" + ddlSubject.SelectedValue + " and TSD_OPTION_ID =0 " _
                      & " and TSD_TGM_ID=" + ddlSetup.SelectedValue + "and SSD_SGR_ID =" + ddlgroup.SelectedValue + " order by STU_NAME "


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)




            If ds.Tables(0).Rows.Count > 0 Then
                grdStud.DataSource = ds.Tables(0)
                grdStud.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdStud.DataSource = ds.Tables(0)
                Try
                    grdStud.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdStud.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdStud.Rows(0).Cells.Clear()
                grdStud.Rows(0).Cells.Add(New TableCell)
                grdStud.Rows(0).Cells(0).ColumnSpan = columnCount
                grdStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
        gridbind()
    End Sub
    Protected Sub grdStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            grdStud.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub grdStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdStud.RowCommand
        Try


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(grdStud.Rows(index), GridViewRow)

            If e.CommandName = "Update" Then
                Dim lblid As New Label
                Dim cbA As New CheckBox
                Dim cbR As New CheckBox
                lblid = selectedRow.FindControl("AtdId")
                cbA = selectedRow.FindControl("cb1")
                cbR = selectedRow.FindControl("cb2")
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_query As String

                str_query = "exec smart.updateSMARTTARGETS " & lblid.Text & " ,'" & cbA.Checked & "','" & cbR.Checked & "'"

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                lblError.Text = "Updated..."

                gridbind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub grdStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdStud.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim chkapproved As CheckBox = DirectCast(e.Row.FindControl("cb1"), CheckBox)
            Dim chkreviewed As CheckBox = DirectCast(e.Row.FindControl("cb2"), CheckBox)

           

            If chkapproved.Text = "True" Then
                chkapproved.Checked = True
            Else
                chkapproved.Checked = False

            End If
            If chkreviewed.Text = "True" Then
                chkreviewed.Checked = True
                chkreviewed.Enabled = False
            Else
                chkreviewed.Checked = False
                chkreviewed.Enabled = True
            End If
            chkapproved.Text = ""
            chkreviewed.Text = ""
        End If


    End Sub

    Protected Sub ddlgroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlgroup.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub grdStud_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdStud.RowUpdating

    End Sub
End Class
