﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmObjTrackObjectiveClosing
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320041") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindGrade()
                GetPrevClosingDate()
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try
        End If
    End Sub


#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            If Session("sbsuid") = "125010" Then
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                     & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                     & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                     & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08','09','10') "

            Else
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                     & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                     & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                     & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08','09','10','11') "

            End If
         

            If ViewState("GRD_ACCESS") > 0 Then
                str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                         & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                         & " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
            End If

            str_query += " ORDER BY GRD_DISPLAYORDER"
        Else
            If Session("sbsuid") = "125010" Then
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                       & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                       & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_GRD_ID" _
                       & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                       & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                       & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                       & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08','09','10') " _
                       & " ORDER BY GRD_DISPLAYORDER"
            Else
                str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                       & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                       & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_GRD_ID" _
                                       & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                                       & " WHERE GRM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                       & " AND SGS_EMP_ID=" + Session("EMPLOYEEID") _
                                       & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08') " _
                                       & " ORDER BY GRD_DISPLAYORDER"
            End If
          
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
    End Sub


    Sub GetPrevClosingDate()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT isnull(MAX(SCO_CLOSINGDATE),'2100-01-01') FROM OBJTRACK.GRADE_CLOSING_S " _
                                & " WHERE SCO_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SCO_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"


        Dim closingDate As DateTime = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        hf_PrevDate.Value = Format(closingDate, "dd/MMM/yyyy")

        If hf_PrevDate.Value <> "01/Jan/2100" Then
            lblText.Text = "Prev. Closing Date : " + hf_PrevDate.Value
        End If


    End Sub
    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec OBJTRACK.saveGRADE_CLOSING " _
                                & "@BSU_ID='" + Session("sbsuid") + "'," _
                                & "@ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                & "@GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                                & "@CLOSINGDATE='" + Format(Date.Parse(txtDate.Text), "dd/MMM/yyy") + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        lblError.Text = "Record Saved Successfully"
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
    End Sub

 
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If hf_PrevDate.Value <> "01/Jan/2100" Then
                If Date.Parse(hf_PrevDate.Value) >= Date.Parse(txtDate.Text) Then
                    lblError.Text = "Please enter a date higher than the prev. closing date"
                    Exit Sub
                End If
            End If
            SaveData()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
        GetPrevClosingDate()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        GetPrevClosingDate()
    End Sub
End Class
