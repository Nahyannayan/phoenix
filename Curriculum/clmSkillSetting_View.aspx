<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSkillSetting_View.aspx.vb" Inherits="Skills_Setting_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Skills Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="8">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblAccText" runat="server" CssClass="field-label" Text="Select Academic Year"></asp:Label></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%">
                                        <asp:Label ID="Label1" runat="server" CssClass="field-label" Text="Select Grade"></asp:Label></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" CssClass="field-label" Text="Select Subject"></asp:Label></td>

                                    <td align="left" colspan="1">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="trlock" runat="server">
                                    <td align="left">
                                        <asp:Label ID="Label3" runat="server" Text="Lock Skills ? :"></asp:Label></td>

                                    <td align="left">
                                        <asp:CheckBox ID="chkLock" runat="server" Text="Check to lock the skills" Checked="true" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" class="matters">
                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <%--   <tr><td align="left">
            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
            </td></tr>--%>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvSkill" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="skl_id"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SKL_ID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSkill_id" runat="server" Text='<%# Bind("skl_id")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Skill">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSkill" runat="server" Text='<%# Bind("SKL_NAME")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Description">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("SKL_DESCR") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="UpdateRow" HeaderText="Update" Text="Update">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <HeaderStyle Wrap="False" />
                                                        <RowStyle Wrap="False" />
                                                        <SelectedRowStyle Wrap="False" />
                                                        <AlternatingRowStyle Wrap="False" />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSaveSkill" runat="server" Text="Save" OnClick="btnSaveSkill_Click" Font-Bold="True"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
