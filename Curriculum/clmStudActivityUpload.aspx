<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmStudActivityUpload.aspx.vb" Inherits="Curriculum_clmStudActivityUpload"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" EnableEventValidation="false"
    ValidateRequest="false" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

function pagereload()
{
window.location.reload();;
}
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Upload Reflection
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
                    PopupControlID="plStudAtt" CancelControlID="btnClose" OnCancelScript="cancelClick();"
                    BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll" />
                <asp:Button ID="btnShowEdit" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender ID="mdlPopUpEdit" runat="server" TargetControlID="btnShowEdit"
                    PopupControlID="divStudEdit" CancelControlID="btnCloseedit" BackgroundCssClass="modalBackground"
                    DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll" />
                <table id="Table1" width="100%" border="0">
                    <tbody>
                       
                        <tr align="left">
                            <td>
                                <asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="LabelError">
                                </asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tbl_Group" runat="server" align="center" border="0" cellpadding="0" cellspacing="0"
                    width="100%">
                    <tr>
                        <td>
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                
                                <tr>
                                    <td align="left"  width="15%"><span class="field-label" >Academic Year</span> </td>
                                 
                                    <td align="left" width="35%" >
                                        <asp:DropDownList ID="ddlAcdID" runat="server" OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>&nbsp;
                                    </td>
                                    <td colspan="2"> 

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="15%" ><span class="field-label" >Report Type</span> </td>
                                  
                                    <td align="left" width="35%" >
                                        <asp:DropDownList ID="ddlReportId" runat="server"  OnSelectedIndexChanged="ddlReportId_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"  width="15%" ><span class="field-label" >Report Schedule</span></td>
                                  
                                    <td align="left"   >
                                        <asp:DropDownList ID="ddlRptSchedule" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="15%" ><span class="field-label" >Grade</span> </td>
                               
                                    <td align="left" width="35%" >
                                        <asp:DropDownList ID="ddlGrade" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="15%" ><span class="field-label" >Section</span> </td>
                                 
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlsct_id" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr >
                                    <td align="left" width="15%" ><span class="field-label" >Upload file</span> </td>
                              
                                    <td align="left"  colspan="3"   >
                                        <iframe id="iframeFileLoad" src="CLMStud_upload_frame.aspx" scrolling="no" frameborder="0"
                                            style="text-align: center; vertical-align: middle; border-style: none; margin: 0px; width: 100%;  height: 50%;  margin-top: -2px; padding-top: -2px;"></iframe>
                                        <asp:Label ID="ltNote" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4">&nbsp;
                            <asp:Button ID="btnViewStud" runat="server" CssClass="button" Text="View Student" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvInfo" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            Height="100%" >
                                            <RowStyle />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sl.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsNo" runat="server" Text='<%# Bind("R1") %>' __designer:wfdid="w17"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# bind("STU_NO") %>' __designer:wfdid="w18"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="stu_id" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_ID" runat="server" Text='<%# Bind("STU_ID") %>' __designer:wfdid="w20"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="True" ></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:Label ID="lblSname" runat="server" Text='<%# bind("SNAME") %>'></asp:Label>
                                                        &nbsp;
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="True" ></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Image File">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgThumb" runat="server" ImageUrl='<%# bind("SFU_FILEPATH") %>'
                                                            OnClick="imgThumb_Click" width="10%" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtn" runat="server" width="10%"  ImageUrl="~/Images/editCurr.png"
                                                             OnClick="imgbtn_Click" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCT_DESCR" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle  />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <div id="plStudAtt" runat="server" >
                                <asp:Panel ID="plphoto" runat="server"  Border="1" BorderColor="Gray"   style="text-align: center; vertical-align: middle; border-style: solid !important; border-color: Gray !important; border-width: 1px !important;" BackColor="White" >
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="msg_header" style=" margin-top: 1px; vertical-align: middle; background-color: White;">
                                                <div class="msg" style="margin-top: 0px; vertical-align: middle; text-align: center;">
                                                    <span class="field-label" ><asp:Literal ID="ltHeader" runat="server"></asp:Literal></span> 
                                                    <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: -4px; vertical-align: top;">
                                                        <asp:ImageButton ID="btnClose" runat="server" ImageUrl="../Images/closeme.png" Style="margin-top: 3px; vertical-align: top; padding-bottom: 2px; float: right; margin-right:-10px;" /></span>
                                                </div>
                                            </div>
                                            <table cellpadding="0px" cellspacing="0px">
                                               <%-- <tr align="center">
                                                    <td style="height: 15px; width: 30px;"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>--%>
                                                <tr>
                                                   <%-- <td style="height: 5px; width: 5px;"></td>--%>
                                                    <td style="text-align: center; vertical-align: middle; border-style: solid; border-color: Gray; border-width: 1px;">
                                                        <asp:Image ID="imgLarge" runat="server" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"
                                                            Width="420px" Height="470px" />
                                                    </td>
                                                    <%--<td></td>--%>
                                                </tr>                                               
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                            </div>
                            <div id="divStudEdit" runat="server" style="display: none; overflow: visible; border-color: gray; border-style: solid; border-width: 1px; ">
                               
                                <asp:UpdatePanel ID="uppnlPopup" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="msg_header" style="width: 100%; margin-top: 1px; vertical-align: middle; background-color: White;">
                                            <div class="msg" style="margin-top: 0px; vertical-align: middle; text-align: center;">
                                                <span class="field-label" >
                                                    <asp:Literal ID="ltName" runat="server"></asp:Literal></span> <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: -4px; vertical-align: top;">
                                                        <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="../Images/closeme.png"
                                                            Style="margin-top: -3px; vertical-align: top; margin-right :-10px;" /></span>
                                            </div>
                                        </div>
                                        <asp:Panel ID="plStudEdit" runat="server" Width="100%" Height="100%" ScrollBars="Horizontal"
                                            Style="text-align: center; margin-top: -1px; border-color: gray; border-style: solid; border-width: 1px;" BackColor="White">
                                            <div style="float: right; display: inline; text-align: right;" width="100%" height:"100%">
                                            </div>
                                            <table cellpadding="0px" cellspacing="0px" width="100%" >
                                                <tr align="center">
                                                    <td style="text-align: center; vertical-align: middle; border-style: solid; border-color: Gray; border-width: 1px;" >
                                                        <iframe id="iframeEdit" src="CLMStud_uploadEdit_frame.aspx" scrolling="no" frameborder="0"
                                                            style="text-align: center; vertical-align: middle; border-style: none; margin: 0px; width: 1000px; height: 700px"></iframe>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                   
                            </div>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div> 

</asp:Content>
