<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="clmReport_Processing.aspx.vb" Inherits="Students_studAtt_Registration" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function showToolTip() {


            ProcessReportRule.GetXmlDocument(SucceededCallbackWithContext, FailedCallback, "XmlDocument")

        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Processing Report"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left">
                            
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>

                                </div>
                          
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcd_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcd_Year_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Term</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlTrm_id" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTrm_id_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Report card</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlRSM_ID" runat="server" OnSelectedIndexChanged="ddlRSM_ID_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%" ><span class="field-label">Printed For</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlRPF_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRPF_ID_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Column Header</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlRSD_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRSD_ID_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Grade</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlGRD_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGRD_ID_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"  width="20%"><span class="field-label">Stream</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStream_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Subject</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlSBG_ID" runat="server" OnSelectedIndexChanged="ddlSBG_ID_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left"  width="20%"><span class="field-label">Group</span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlSGR_ID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSGR_ID_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="4">
                                        <asp:LinkButton ID="lbtnDetail" runat="server">View Rule</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">


                            <asp:Button ID="btnProcess" runat="server" CausesValidation="False" CssClass="button" Text="Process" ValidationGroup="AttGroup" OnClick="btnProcess_Click" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom"></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:GridView ID="gvInfo" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" Height="100%" Width="100%" DataKeyNames="STU_ID" EmptyDataText=",,">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsNo" runat="server" Text='<%# Bind("SRNO") %>' __designer:wfdid="w7"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# bind("STU_NO") %>' __designer:wfdid="w8"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID1" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStu_ID" runat="server" Text='<%# Bind("STU_ID") %>' __designer:wfdid="w8"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" ></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lbtnSNAME" runat="server" Text='<%# BIND("SNAME") %>' __designer:wfdid="w8"></asp:LinkButton>
                                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server"
                                                PopupControlID="ActPopUp" TargetControlID="lbtnSNAME" __designer:wfdid="w9"
                                                DynamicContextKey='<%# Eval("STU_ID")& "|" & Eval("ACD_ID") & "|" & Eval("SGR_ID")& "|" & Eval("SBG_ID") %> '
                                                DynamicControlID="ActPopUp" DynamicServiceMethod="GetContent" Position="left" OffsetY="12" OffsetX="1">
                                            </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" ></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mark">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMark" runat="server" Text='<%# bind("mark") %>'  MaxLength="6"></asp:Label>
                                            <asp:TextBox ID="txtMark" runat="server" Text='<%# bind("mark") %>' Visible="false"  __designer:wfdid="w24" MaxLength="6"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbTxtmark" runat="server" __designer:wfdid="w25" TargetControlID="txtMark" ValidChars="." FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# bind("GRADE") %>'  MaxLength="255"></asp:Label>
                                            <asp:TextBox ID="txtGrade" runat="server" Text='<%# bind("GRADE") %>' Visible="false"  __designer:wfdid="w14" MaxLength="255"></asp:TextBox>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RRM_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRRM_ID" runat="server" Text='<%# bind("RRM_ID") %>' __designer:wfdid="w18"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TYPE_LEVEL" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblType_Level" runat="server" Text='<%# bind("Type_Level") %>' __designer:wfdid="w20"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom"></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnSave"
                                runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" OnClick="btnSave_Click" />
                            <asp:Button
                                    ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                                                               
                        </td>
                    </tr>
                </table>

                <div id="popup" runat="server" class="panel-cover" style="display: none; text-align: left; vertical-align: middle; width: 306px; margin-top: -2px; margin-left: -3px; margin-right: 1px;">
                    <div class="title-bg-small" style="color: #1b80b6; vertical-align: middle; padding-bottom: 2px;">Report Processing Rule For Selected Group</div>
                    <asp:Panel ID="PopupMenu" ScrollBars="Vertical" runat="server" CssClass="modalPopup1" Style="background-color: #ebeff1" Height="25%" Width="300px">
                        <asp:Literal ID="ltProcessRule" runat="server"></asp:Literal>
                    </asp:Panel>
                </div>
                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server"
                    TargetControlID="lbtnDetail"
                    PopupControlID="popup"
                    HoverCssClass="popupHover"
                    PopupPosition="Center"
                    OffsetX="-200"
                    OffsetY="20"
                    PopDelay="50" />
                
    
    <asp:Panel ID="ActPopUp" runat="server">

    </asp:Panel>

            </div>
        </div>
    </div>

</asp:Content>

