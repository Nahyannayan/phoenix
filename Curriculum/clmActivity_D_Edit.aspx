<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmActivity_D_Edit.aspx.vb" Inherits="clmActivity_D_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
  <script language="javascript" type="text/javascript">
  
	
</script>
   <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"> </i> <asp:Label ID="lblHeader" runat="server" Text="Assessment Detail"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left" >
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary><span style="font-size: 8pt; color: #800000">&nbsp;</span></div>
                </span>
            </td>
        </tr>
        
        <tr>
            <td   valign="bottom" >
                <table align="center"  cellpadding="5" cellspacing="0"
                      width="100%">
                    
                    <tr>
                        <td align="left" >
                            <span class="field-label">Academic Year</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList id="ddlAca_Year" runat="server" >
                            </asp:DropDownList></td>
                        <td align="left" >
                            <span class="field-label">Term</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList id="ddlTRM_ID" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Main Assessment</span></td>
                       
                        <td align="left"  >
                            <asp:DropDownList id="ddlCAD_CAM_ID" runat="server">
                            </asp:DropDownList></td>
                   
                        <td align="left" >
                            <span class="field-label">Sub Assessment</span></td>
                        
                        <td align="left"  >
                            <asp:TextBox id="txtCAD_DESC" runat="server" Width="289px"></asp:TextBox>
                            <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtCAD_DESC"
                                Display="Dynamic" ErrorMessage="Sub Activity required" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr >
                        <td align="left" 
                             colspan="6">
                            <asp:CheckBox ID="chkHasAOLExam" Text ="Create AOL" runat="server" class="field-label"
                                AutoPostBack="True" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkWithoutSkills" Text ="Without Skills" runat="server" class="field-label"
                                Visible="False" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkSkills" Text ="With Skills" runat="server" class="field-label"
                                Visible="True" />
                        </td>
                    </tr>
                    <tr runat="server" id="tr20">
                        <td align="right" colspan="6" style="height: 13px">
                            <asp:Button id="btnAddDetail" runat="server" CssClass="button" Text="Add Detail" OnClick="btnAddDetail_Click" ValidationGroup="AttGroup" />
                            <asp:Button id="btnUpdate" runat="server" CssClass="button" Text="Update Detail" OnClick="btnUpdate_Click" ValidationGroup="AttGroup" />
                            <asp:Button id="btnUpdateCancel" runat="server" CssClass="button" OnClick="btnUpdateCancel_Click"
                                Text="Cancel" /></td>
                    </tr>
                   <tr class="title-bg"  runat="server" id="tr21" >
                       <td align="left"  colspan="6">
                           Add Detail</td>
                    </tr>
                    <tr   runat="server" id="tr22">
                        <td align="center" colspan="6" style="height: 21px">
                <asp:GridView id="gvACT_Detail" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added" CssClass="table table-bordered table-row"
                    Width="100%" OnRowEditing="gvACT_Detail_RowEditing" AllowPaging="True" OnRowDeleting="gvACT_Detail_RowDeleting" DataKeyNames="ID" OnPageIndexChanging="gvACT_Detail_PageIndexChanging">
                    <rowstyle cssclass="griditem" height="25px" />
                    <emptydatarowstyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                    <columns>
<asp:BoundField DataField="SRNO" HeaderText="Sl.No"></asp:BoundField>
<asp:BoundField DataField="ACD_DESC" HeaderText="Acd. Year">
<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TRM_DESC" HeaderText="Term" HtmlEncode="False">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="MAIN_ACT" HeaderText="Main Activity"></asp:BoundField>
<asp:BoundField DataField="SUB_ACT" HeaderText="Sub Activity"></asp:BoundField>
<asp:BoundField DataField="CAD_bAOL" HeaderText="Create AOL Exam" />
<asp:TemplateField HeaderText="Edit" ShowHeader="False"><EditItemTemplate>
<asp:LinkButton id="LinkButton1" runat="server" Text="Update" CausesValidation="True" CommandName="Update"></asp:LinkButton> <asp:LinkButton id="LinkButton2" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel"></asp:LinkButton> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lblEdit" runat="server" Text="Edit" CausesValidation="False" CommandName="Edit"></asp:LinkButton>&nbsp; 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Delete"><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server" Text='<%# Bind("Delete") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:LinkButton id="lblDelete" runat="server" Text="Delete" CausesValidation="False" CommandName="Delete"></asp:LinkButton> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="id" Visible="False"><ItemTemplate>
<asp:Label id="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="CAD_ACD_ID" HeaderText="CAD_ACD_ID" Visible="False"></asp:BoundField>
<asp:BoundField DataField="CAD_CAM_ID" HeaderText="CAD_GAM_ID" Visible="False"></asp:BoundField>
<asp:BoundField DataField="CAD_TRM_ID" HeaderText="CAD_TRM_ID" Visible="False"></asp:BoundField>
</columns>
                    <selectedrowstyle cssclass="griditem_hilight" />
                    <headerstyle cssclass="gridheader_new" height="25px" />
                    <alternatingrowstyle cssclass="griditem_alternative" />
                </asp:GridView></td>
                    </tr>
                  
                </table>
            </td>
        </tr>
        <tr>
            <td  style="height: 9px" valign="bottom">
                <br />
                &nbsp;</td>
        </tr>
        <tr>
            <td  style="height: 10px" valign="bottom">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnAdd_Click" Text="Add" Width="50px" /><asp:Button id="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnEdit_Click" Text="Edit" Width="50px" /><asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" Width="50px" /><asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" Width="50px" /></td>
        </tr>
    </table>
    <input id="h_Row" type="hidden" runat="server" />
                </div>
            </div>
       </div>
</asp:Content>

