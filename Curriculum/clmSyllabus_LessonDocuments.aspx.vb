﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Curriculum_clmSyllabus_LessonDocuments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100546" And ViewState("MainMnu_code") <> "C300166") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    BindGrade()

                    BindSubject()



                    gridbind()
                    gvSkill.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "select distinct(GRM_GRD_ID) GRD_ID,GRM_DISPLAY,GRD_DISPLAYORDER  from GRADE_M inner join GRADE_BSU_M on grd_id=grm_grd_id " _
                              & " where GRM_BSU_ID='" + Session("sBsuid") + "' order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()

    End Sub
    Public Sub BindSubject()
        ddlSubject.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "select distinct(SBG_SBM_ID) as SBM_ID ,SBG_DESCR   from SUBJECTS_GRADE_S inner join SUBJECT_M on SBM_ID=SBG_SBM_ID  "

        str_query += " WHERE SBG_BSU_ID='" + Session("sBsuid") + "'"


        If ddlGrade.SelectedValue <> "" Then

            str_query += " AND SBG_GRD_ID='" + ddlGrade.SelectedValue + "'"


        End If
        str_query += " order by SBG_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBM_ID"
        ddlSubject.DataBind()


    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_DOCSConnectionString
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Dim strFilter As String = ""

            Dim strSearch As String = ""

            Dim txtSearch As New TextBox
            Dim optSearch As String = ""
            Dim txtSearch1 As New TextBox
            Dim optSearch1 As String = ""
            Dim strFilter1 As String = ""
            Dim str_search As String = ""



            str_Sql = "select SD_ID,SD_DESCR,SD_FILENAME,isnull(SD_bSHOW,'FALSE') as SD_bSHOW from SYLLABUS_DOC " _
                      & " WHERE SD_GRD_ID='" + ddlGrade.SelectedValue + "' AND SD_BSU_ID='" + Session("sBsuid") + "' AND SD_SBM_ID=" + ddlSubject.SelectedValue

            If gvSkill.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvSkill.HeaderRow.FindControl("txtOption")

                If txtSearch.Text <> "" Then

                    If str_search = "LI" Then
                        strFilter = " and SD_DESCR LIKE '%" & txtSearch.Text & "%'"

                    ElseIf str_search = "NLI" Then
                        strFilter = "  AND  NOT  SD_DESCR LIKE '%" & txtSearch.Text & "%'"

                    ElseIf str_search = "SW" Then
                        strFilter = " AND SD_DESCR  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        strFilter = " AND SD_DESCR NOT LIKE '" & txtSearch.Text & "%'"

                    ElseIf str_search = "EW" Then
                        strFilter = " AND SD_DESCR LIKE  '%" & txtSearch.Text & "'"

                    ElseIf str_search = "NEW" Then
                        strFilter = " AND SD_DESCR NOT LIKE '%" & txtSearch.Text & "'"

                    End If


                    optSearch = txtSearch.Text



                    If strFilter.Trim <> "" Then
                        str_Sql += " " + strFilter
                    End If
                End If



                txtSearch1 = gvSkill.HeaderRow.FindControl("txtOption1")

                If txtSearch1.Text <> "" Then

                    If str_search = "LI" Then
                        strFilter1 = " and SD_FILENAME LIKE '%" & txtSearch1.Text & "%'"

                    ElseIf str_search = "NLI" Then
                        strFilter1 = "  AND  NOT  SD_FILENAME LIKE '%" & txtSearch1.Text & "%'"

                    ElseIf str_search = "SW" Then
                        strFilter1 = " AND SD_FILENAME LIKE '" & txtSearch1.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        strFilter1 = " AND SD_FILENAME NOT LIKE '" & txtSearch1.Text & "%'"

                    ElseIf str_search = "EW" Then
                        strFilter1 = " AND SD_FILENAME LIKE  '%" & txtSearch1.Text & "'"

                    ElseIf str_search = "NEW" Then
                        strFilter1 = " AND SD_FILENAME NOT LIKE '%" & txtSearch1.Text & "'"

                    End If

                    optSearch1 = txtSearch1.Text

                    If strFilter1.Trim <> "" Then
                        str_Sql += " " + strFilter1
                    End If
                End If




            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            ' If ds.Tables(0).Rows.Count > 0 Then
            gvSkill.DataSource = ds.Tables(0)
            gvSkill.DataBind()
            'Else
            '    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            '    gvSkill.DataSource = ds.Tables(0)
            '    Try
            '        gvSkill.DataBind()
            '    Catch ex As Exception
            '    End Try
            '    Dim columnCount As Integer = gvSkill.Rows(0).Cells.Count
            '    'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
            '    gvSkill.Rows(0).Cells.Clear()
            '    gvSkill.Rows(0).Cells.Add(New TableCell)
            '    gvSkill.Rows(0).Cells(0).ColumnSpan = columnCount
            '    gvSkill.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            '    gvSkill.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            'End If
            txtSearch = New TextBox
            txtSearch = gvSkill.HeaderRow.FindControl("txtOption")
            txtSearch.Text = optSearch

            txtSearch1 = New TextBox
            txtSearch1 = gvSkill.HeaderRow.FindControl("txtOption1")
            txtSearch1.Text = optSearch1

            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        gridbind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub gvSkill_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSkill.RowCommand


        Try
            Dim strQuery As String = ""
            Dim cmd As SqlCommand
            Dim lblId As New Label
            Dim i As Integer
            Dim selectedRow As GridViewRow
            



            If e.CommandName = "View" Then
                i = Convert.ToInt32(e.CommandArgument)
                selectedRow = DirectCast(gvSkill.Rows(i), GridViewRow)


                lblId = selectedRow.FindControl("lblSksId")
                strQuery = "select sd_fileName, sd_Content, sd_doc from SYLLABUS_DOC where sd_id=" + lblId.Text

                cmd = New SqlCommand(strQuery)

                Dim dt As DataTable = GetData(cmd)

                If dt IsNot Nothing Then

                    download(dt)

                End If
            End If

            If e.CommandName = "Update" Then
                i = Convert.ToInt32(e.CommandArgument)
                selectedRow = DirectCast(gvSkill.Rows(i), GridViewRow)


                lblId = selectedRow.FindControl("lblSksId")
                Dim chk As New CheckBox
                chk = selectedRow.FindControl("cbShow")

                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                strQuery = "exec syl.UpdateSYLLABUS_DOC " & lblId.Text & " ," & chk.Checked

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                lblError.Text = "Updated..."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Public Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConnectionManger.GetOASIS_DOCSConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("sd_doc"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("sd_filename").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("sd_Content").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub gvSkill_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvSkill.RowUpdating

    End Sub

    Protected Sub gvSkill_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSkill.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtfile As LinkButton = e.Row.FindControl("txtfile")
                Dim img As ImageButton = e.Row.FindControl("imgF")
                Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
                If s = ".xls" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".xlsx" Then
                    img.ImageUrl = "~/Curriculum/images/xls.png"
                ElseIf s = ".doc" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".docx" Then
                    img.ImageUrl = "~/Curriculum/images/doc.png"
                ElseIf s = ".pdf" Then
                    img.ImageUrl = "~/Curriculum/images/pdf.png"
                Else
                    img.ImageUrl = "~/Curriculum/images/img.png"
                End If


                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(txtfile)
                ScriptManager1.RegisterPostBackControl(img)


            End If
        Catch ex As Exception
        End Try


    End Sub

    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub


    Protected Sub imgF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If
    End Sub
   

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If fp.FileName = "" Then
            lblError.Text = "Please select a File to upload"
            Exit Sub
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim filebyte As Byte() = Nothing
        Dim str_query As String = ""
        Dim FilePath As String = ""
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(str_conn)
        Try
            objConn.Open()
            cmd = New SqlCommand("SYL.SaveSYLLABUS_DOC", objConn)


            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@SD_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@SD_GRD_ID", ddlGrade.SelectedValue)
            cmd.Parameters.AddWithValue("@SD_SBM_ID", ddlSubject.SelectedValue)
            cmd.Parameters.AddWithValue("@SD_SBM_DESCR", ddlSubject.SelectedItem.Text)
            cmd.Parameters.AddWithValue("@SD_DESCR", txtDesc.Text)

            If fp.HasFile Then
                Dim p As SqlParameter

                FilePath = Server.MapPath("~/Curriculum/ReportDownloads") & "/" & fp.FileName
                fp.SaveAs(FilePath)
                filebyte = System.IO.File.ReadAllBytes(FilePath)

                p = cmd.Parameters.AddWithValue("@SD_DOC", filebyte)
                'p.Value = SqlDbType.Binary
                cmd.Parameters.AddWithValue("@SD_FILENAME", fp.FileName)
                cmd.Parameters.AddWithValue("@SD_CONTENT", fp.PostedFile.ContentType)

            End If


            cmd.ExecuteNonQuery()


            lblError.Text = "Record Saved Successfully"

            gridbind()
            File.Delete(FilePath)
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
        Finally
            cmd.Dispose()
            objConn.Close()
        End Try
    End Sub

    Protected Sub gvSkill_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSkill.RowDeleting
        Try
            gvSkill.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = gvSkill.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblSksId"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASIS_DOCSConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM SYLLABUS_DOC WHERE SD_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnempid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnuid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSkill.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvSkill.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvSkill.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvSkill.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub
End Class
