

Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports CURRICULUM
Imports ActivityFunctions

Partial Class Curriculum_clmSyllabus_ViewTopics
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            'Session("Syllabus") = SyllabusMaster.CreateDataTableSyllabus()

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320002") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'ddlAcademicYear = ActivityFunctions.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))


                    If ViewState("datamode") = "add" Then

                        FillAcd()
                        FillTerm()
                        FillGrade()
                        FillSubjects()
                        ddlgroupbind()
                        'ddlSyllabusbind()

                        
                        'ClearPartialDetails()
                        'GetDetails()

                        'txtCLM_DESC.Text = ""
                    Else
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        'FillDetails()
                    End If
                    If Not ddlAcademicYear.SelectedIndex = -1 Then
                        h_ACD_ID.Value = ddlAcademicYear.SelectedItem.Value
                    End If
                    If Not ddlTerm.SelectedIndex = -1 Then
                        h_TRM_ID.Value = ddlTerm.SelectedItem.Value
                    End If
                    If Not ddlGrade.SelectedIndex = -1 Then
                        h_GRD_ID.Value = ddlGrade.SelectedItem.Value
                    End If
                    If Not ddlSubject.SelectedIndex = -1 Then
                        h_SUBJ_ID.Value = ddlSubject.SelectedItem.Value
                    End If
                    'h_GRP_ID.Value = ddlGroup.SelectedItem.Value
                    h_SYL_ID.Value = ""

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim str_err As String = String.Empty
    '    Dim errorMessage As String = String.Empty
    '    If Page.IsValid Then

    '        'str_err = calltransaction(errorMessage)

    '        'If str_err = "0" Then
    '        '    'txtCLM_DESC.Enabled = False
    '        '    lblError.Text = "Record Saved Successfully"
    '        'Else
    '        '    lblError.Text = errorMessage
    '        'End If
    '        'SaveSyllabus()
    '    End If

    'End Sub
    'Function calltransaction(ByRef errorMessage As String) As Integer
    '    Dim bEdit As Boolean
    '    Dim CAM_ID As String = ""
    '    'Dim CLM_DESC As String = txtCLM_DESC.Text


    '    If ViewState("datamode") = "add" Then
    '        Dim transaction As SqlTransaction

    '        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
    '            transaction = conn.BeginTransaction("SampleTransaction")
    '            Try

    '                Dim status As Integer

    '                bEdit = False


    '                ' status = ACTIVITYMASTER.SAVEACTIVITY_M(CAM_ID, CLM_DESC, bEdit, transaction)


    '                If status <> 0 Then
    '                    calltransaction = "1"
    '                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
    '                    Return "1"
    '                End If
    '                '  End If
    '                ' Next
    '                ViewState("viewid") = "0"
    '                ViewState("datamode") = "none"

    '                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '                calltransaction = "0"

    '            Catch ex As Exception
    '                calltransaction = "1"
    '                errorMessage = "Error Occured While Saving."
    '            Finally
    '                If calltransaction <> "0" Then
    '                    UtilityObj.Errorlog(errorMessage)
    '                    transaction.Rollback()
    '                Else
    '                    errorMessage = ""
    '                    transaction.Commit()
    '                End If
    '            End Try

    '        End Using
    '    ElseIf ViewState("datamode") = "edit" Then
    '        Dim transaction As SqlTransaction

    '        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
    '            transaction = conn.BeginTransaction("SampleTransaction")
    '            Try
    '                Dim status As Integer


    '                CAM_ID = ViewState("viewid")
    '                bEdit = True


    '                'status = ACTIVITYMASTER.SAVEACTIVITY_M(CAM_ID, CLM_DESC, bEdit, transaction)


    '                If status <> 0 Then
    '                    calltransaction = "1"
    '                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
    '                    Return "1"
    '                End If


    '                ViewState("viewid") = "0"
    '                ViewState("datamode") = "none"

    '                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '                calltransaction = "0"

    '                ViewState("viewid") = 0
    '            Catch ex As Exception
    '                calltransaction = "1"
    '                errorMessage = "Error Occured While Saving."
    '            Finally
    '                If calltransaction <> "0" Then
    '                    UtilityObj.Errorlog(errorMessage)
    '                    transaction.Rollback()
    '                Else
    '                    errorMessage = ""
    '                    transaction.Commit()
    '                End If
    '            End Try

    '        End Using
    '    End If
    'End Function

#End Region

    

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"

                'txtCLM_DESC.Text = ""
                'txtCLM_DESC.Enabled = False
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Sub FillAcd()

        ddlAcademicYear.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        h_ACD_ID.Value = ""
        h_SYL_ID.Value = ""
        txtSelSyllabus.Text = ""

        If Not ddlAcademicYear.SelectedIndex = -1 Then
            h_ACD_ID.Value = ddlAcademicYear.SelectedItem.Value
        End If
        'ddlAcademicYear.Items.FindByValue(Session("ACY_ID")).Selected = True
    End Sub

    Sub FillTerm()

        ddlTerm.DataSource = ActivityFunctions.GetTERM_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        h_TRM_ID.Value = ""
        h_SYL_ID.Value = ""
        txtSelSyllabus.Text = ""
        If Not ddlTerm.SelectedIndex = -1 Then
            h_TRM_ID.Value = ddlTerm.SelectedItem.Value
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        h_ACD_ID.Value = ""
        h_TRM_ID.Value = ""
        h_GRD_ID.Value = ""
        h_SUBJ_ID.Value = ""
        FillTerm()
        FillGrade()
        FillSubjects()
        ddlgroupbind()

        h_ACD_ID.Value = ddlAcademicYear.SelectedItem.Value
    End Sub
    Sub FillGrade()

        'ddlGrade.DataSource = ActivityFunctions.GetGrade_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlGrade.DataSource = ACTIVITYSCHEDULE.GetGRADE(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        h_GRD_ID.Value = ""
        h_SYL_ID.Value = ""
        txtSelSyllabus.Text = ""
        If Not ddlGrade.SelectedIndex = -1 Then
            h_GRD_ID.Value = ddlGrade.SelectedItem.Value
        End If
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        h_TRM_ID.Value = ""
        h_GRD_ID.Value = ""
        h_SUBJ_ID.Value = ""
        
        If Not ddlTerm.SelectedIndex = -1 Then
            h_TRM_ID.Value = ddlTerm.SelectedItem.Value
        End If
        FillGrade()
        FillSubjects()
        ddlgroupbind()
        'GetDetails()
    End Sub
    Sub FillSubjects()

        ddlSubject.DataSource = ActivityFunctions.GetSUBJECT_ACD_YR(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value, ddlGrade.SelectedItem.Value)
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        h_SUBJ_ID.Value = ""
        h_SYL_ID.Value = ""
        txtSelSyllabus.Text = ""
        If Not ddlSubject.SelectedIndex = -1 Then
            h_SUBJ_ID.Value = ddlSubject.SelectedItem.Value
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        h_GRD_ID.Value = ""
        h_SUBJ_ID.Value = ""
        
        If Not ddlGrade.SelectedIndex = -1 Then
            h_GRD_ID.Value = ddlGrade.SelectedItem.Value
        End If
        FillSubjects()
        ddlgroupbind()
        ' ddlSyllabusbind()
    End Sub
   

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' txtSyllabus.Text = ""

        h_SUBJ_ID.Value = ""

        If Not ddlSubject.SelectedIndex = -1 Then
            h_SUBJ_ID.Value = ddlSubject.SelectedItem.Value
        End If
        ddlgroupbind()
        'ddlSyllabusbind()

    End Sub
    Sub ClearPartialDetails()
        FillGrade()
        FillSubjects()
        'txtSyllabus.Enabled = True
        'txtSyllabus.Text = ""
        ddlAcademicYear.Enabled = True
        ddlTerm.Enabled = True
        'gvSyllabus.DataSource = Nothing
        'gvSyllabus.DataBind()
        Session("gintGridLine") = 0
        Session("Syllabus") = Nothing
        h_SYL_ID.Value = 0
    End Sub
    

    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        '    Dim lblAcdId As New Label
        '    Dim lblTrmId As New Label
        '    Dim lblSubjId As New Label
        '    Dim lblSylId As New Label
        '    Dim url As String

        '    'lblCAM_ID = TryCast(sender.FindControl("lblCAM_ID"), Label)
        '    lblAcdId = TryCast(sender.FindControl("lblACDID"), Label)
        '    lblTrmId = TryCast(sender.FindControl("lblTrmID"), Label)
        '    lblSubjId = TryCast(sender.FindControl("lblSubjID"), Label)
        '    lblSylId = TryCast(sender.FindControl("lblSylId"), Label)
        '    'define the datamode to view if view is clicked
        '    ViewState("datamode") = "add"
        '    'Encrypt the data that needs to be send through Query String
        '    ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")

        '    ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        '    url = String.Format("~\Curriculum\clmSyllabus_D_AddEdit.aspx?MainMnu_code=" & ViewState("MainMnu_code") & " &datamode=" & ViewState("datamode") & "&AccId=" & Encr_decrData.Encrypt(lblAcdId.Text) & "&TermId=" & Encr_decrData.Encrypt(lblTrmId.Text) & "&SubjID=" & Encr_decrData.Encrypt(lblSubjId.Text) & "&SyllabusId=" & Encr_decrData.Encrypt(lblSylId.Text))
        '    Response.Redirect(url)
        'Catch ex As Exception
        '    lblError.Text = "Request could not be processed "
        'End Try
    End Sub

    'Sub ddlgroupbind()
    '    Dim str_sql As String = String.Empty
    '    str_sql = "SELECT SGR_ID,SGR_DESCR from GROUPS_M WHERE SGR_BSU_ID = '" & Session("sBsuid").ToString() & "'" & _
    '    " AND SGR_ACD_ID = '" & ddlAcademicYear.SelectedItem.Value & "' and SGR_GRD_ID='" & ddlGrade.SelectedItem.Value & "'" _
    '    & " and SGR_SBG_ID='" & ddlSubject.SelectedItem.Value & "'"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    '    ddlgroup.DataSource = ds
    '    ddlgroup.DataTextField = "SGR_DESCR"
    '    ddlgroup.DataValueField = "SGR_ID"
    '    ddlGroup.DataBind()
    '    h_GRP_ID.Value = ""
    '    h_SYL_ID.Value = ""
    '    If Not ddlGroup.SelectedIndex = -1 Then
    '        h_GRP_ID.Value = ddlGroup.SelectedItem.Value
    '    End If
    'End Sub
    

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'ddlSyllabusbind()
    End Sub
    Sub GetSyllabusDetails()
        Dim strSql As String
        Dim ds As DataSet
        Dim i As Integer
        Dim ldrNew As DataRow
        Try
            'Session("SyllabusDet") = SyllabusMaster.CreateDTSyllabusDetails()
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            'strSql = "SELECT     SYL.SYLLABUS_D.SYD_ID AS SydId,SYL.SYLLABUS_D.SYD_DESCR AS SYBDESC, SYL.SYLLABUS_D.SYD_PARENT_ID AS SYBPARENTID, SYLLABUS_D_1.SYD_DESCR AS SYBPARENTDESC, SYL.SYLLABUS_D.SYD_STDT AS SYBSTARTDT, " _
            '        & " SYL.SYLLABUS_D.SYD_ENDDT AS SYBENDDT, SYL.SYLLABUS_D.SYD_TOT_HRS AS SYBTOTHRS " _
            '        & " FROM SYL.SYLLABUS_D LEFT OUTER JOIN " _
            '        & " SYL.SYLLABUS_D AS SYLLABUS_D_1 ON  " _
            '        & " SYL.SYLLABUS_D.SYD_PARENT_ID = SYLLABUS_D_1.SYD_ID " _
            '        & " WHERE SYL.SYLLABUS_D.SYD_SYM_ID='" & h_SYL_ID.Value & "'"
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    For i = 0 To ds.Tables(0).Rows.Count - 1
            '        ldrNew = Session("SyllabusDet").NewRow
            '        Session("gintGridLine") = Session("gintGridLine") + 1
            '        ldrNew("ID") = Session("gintGridLine")
            '        'h_NextLine.Value = Session("gintGridLine")
            '        ldrNew("SUBID") = h_SUBJ_ID.Value
            '        ldrNew("SYBPARENTDESC") = ds.Tables(0).Rows(i)("SYBPARENTDESC")
            '        ldrNew("SYBDESC") = ds.Tables(0).Rows(i)("SYBDESC")
            '        ldrNew("SYBPARENTID") = ds.Tables(0).Rows(i)("SYBPARENTID")
            '        ldrNew("SYBSTARTDT") = Format(ds.Tables(0).Rows(i)("SYBSTARTDT"), "dd/MMM/yyyy")
            '        ldrNew("SYBENDDT") = Format(ds.Tables(0).Rows(i)("SYBENDDT"), "dd/MMM/yyyy")
            '        ldrNew("SYBTOTHRS") = ds.Tables(0).Rows(i)("SYBTOTHRS")
            '        ldrNew("SydId") = ds.Tables(0).Rows(i)("SydId")
            '        'ldrNew("SYBTOTHRS") = ds.Tables(0).Rows(i)("Subject")
            '        'ldrNew("Syllabus") = ds.Tables(0).Rows(i)("Syllabus")
            '        'ldrNew("SylId") = ds.Tables(0).Rows(i)("SylId")
            '        'ldrNew("GUID") = System.DBNull.Value
            '        Session("SyllabusDet").Rows.Add(ldrNew)
            '    Next
            '    GridBind()
            'End If
            If RadPlanned.Checked = True Then
                strSql = "(SELECT SYD_SYM_ID AS ID, SYT_STDT AS PLNSTDT, SYT_ENDDT AS PLNENDDT,SYT_HRS AS PLNHRS, " _
                            & " SYD_DESCR AS DESCR, SYD_STDT AS ACTSTDT, SYD_ENDDT AS ACTENDDT,SYD_TOT_HRS AS ACTHRS " _
                            & " FROM  SYL.SYLLABUS_D INNER JOIN " _
                            & " SYL.SYLLABUS_TEACHER_M ON SYD_ID = SYT_SYD_ID WHERE SYL.SYLLABUS_D.SYD_SYM_ID='" & h_SYL_ID.Value & "' and SYL.SYLLABUS_D.SYD_STDT>='" & CDate(txtFromDate.Text) & "' and SYL.SYLLABUS_D.SYD_ENDDT<='" & CDate(txtToDate.Text) & "' ) " _
                            & " UNION " _
                        & "(SELECT SYD_SYM_ID AS ID,  SYD_STDT AS PLNSTDT, SYD_ENDDT AS PLNENDDT, " _
                        & " SYD_TOT_HRS AS PLNHRS,SYD_DESCR AS DESCR, SYD_STDT AS ACTSTDT, SYD_ENDDT AS ACTENDDT,SYD_TOT_HRS AS ACTHRS  " _
                        & " FROM    SYL.SYLLABUS_D INNER JOIN " _
                        & " SYL.SYLLABUS_TEACHER_D ON SYD_ID = SYL_SYD_ID WHERE SYL.SYLLABUS_D.SYD_SYM_ID='" & h_SYL_ID.Value & "' and SYL.SYLLABUS_D.SYD_STDT>='" & CDate(txtFromDate.Text) & "' and SYL.SYLLABUS_D.SYD_ENDDT<='" & CDate(txtToDate.Text) & "')"
            End If
            ' SYL_ACT_STDT AS ACTSTDT, SYL_ACT_ENDDT AS ACTENDDT,SYL_HRS AS ACTHRS

            If RadActual.Checked = True Then
                strSql = "(SELECT SYD_SYM_ID AS ID, SYT_STDT AS PLNSTDT, SYT_ENDDT AS PLNENDDT,SYT_HRS AS PLNHRS, " _
                            & " SYD_DESCR AS DESCR, SYD_STDT AS ACTSTDT, SYD_ENDDT AS ACTENDDT,SYD_TOT_HRS AS ACTHRS " _
                            & " FROM  SYL.SYLLABUS_D INNER JOIN " _
                            & " SYL.SYLLABUS_TEACHER_M ON SYD_ID = SYT_SYD_ID WHERE SYL.SYLLABUS_D.SYD_SYM_ID='" & h_SYL_ID.Value & "' and SYL.SYLLABUS_TEACHER_M.SYT_STDT>='" & CDate(txtFromDate.Text) & "' and SYL.SYLLABUS_TEACHER_M.SYT_ENDDT<='" & CDate(txtToDate.Text) & "' ) " _
                            & " UNION " _
                        & "(SELECT SYD_SYM_ID AS ID,  SYD_STDT AS PLNSTDT, SYD_ENDDT AS PLNENDDT, " _
                        & " SYD_TOT_HRS AS PLNHRS,SYD_DESCR AS DESCR, SYD_STDT AS ACTSTDT, SYD_ENDDT AS ACTENDDT,SYD_TOT_HRS AS ACTHRS" _
                        & " FROM    SYL.SYLLABUS_D INNER JOIN " _
                        & " SYL.SYLLABUS_TEACHER_D ON SYD_ID = SYL_SYD_ID WHERE SYL.SYLLABUS_D.SYD_SYM_ID='" & h_SYL_ID.Value & "' and SYL.SYLLABUS_TEACHER_D.SYL_ACT_STDT>='" & CDate(txtFromDate.Text) & "' and SYL.SYLLABUS_TEACHER_D.SYL_ACT_ENDDT<='" & CDate(txtToDate.Text) & "')"
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            gvTopics.DataSource = ds
            gvTopics.DataBind()
            gvTopics.Columns(0).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Function GridBind()
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = SyllabusMaster.CreateDTSyllabusDetails()
        If Session("SyllabusDet").Rows.Count > 0 Then
            For i = 0 To Session("SyllabusDet").Rows.Count - 1
                Dim ldrTempNew As DataRow
                ldrTempNew = dtTempDtl.NewRow
                For j As Integer = 0 To Session("SyllabusDet").Columns.Count - 1
                    ldrTempNew.Item(j) = Session("SyllabusDet").Rows(i)(j)
                Next
                dtTempDtl.Rows.Add(ldrTempNew)
            Next
        End If
        gvTopics.DataSource = dtTempDtl
        gvTopics.DataBind()
        gvTopics.Columns(1).Visible = False
        gvTopics.Columns(8).Visible = False
    End Function

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(txtFromDate.Text) = False Then
            lblError.Text = "Invalid From Date"
            Exit Sub
        End If
        If IsDate(txtToDate.Text) = False Then
            lblError.Text = "Invalid To Date"
            Exit Sub
        End If
        If Convert.ToDateTime(txtFromDate.Text) > Convert.ToDateTime(txtToDate.Text) Then
            lblError.Text = "FromDate must be less than ToDate"
            Exit Sub
        End If

        GetSyllabusDetails()
    End Sub

    Protected Sub lnkAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim lblAcdId As New Label
            'Dim lblTrmId As New Label
            'Dim lblSubjId As New Label
            'Dim lblSylId As New Label
            Dim strSydId As String = String.Empty
            Dim lblSydId As New Label
            Dim url As String
            lblSydId = TryCast(sender.FindControl("lblSydId"), Label)
            If Not lblSydId Is Nothing Then
                strSydId = lblSydId.Text
            End If
            'lblCAM_ID = TryCast(sender.FindControl("lblCAM_ID"), Label)
            'lblAcdId = TryCast(sender.FindControl("lblACDID"), Label)
            'lblTrmId = TryCast(sender.FindControl("lblTrmID"), Label)
            'lblSubjId = TryCast(sender.FindControl("lblSubjID"), Label)
            'lblSylId = TryCast(sender.FindControl("lblSylId"), Label)
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Curriculum\clmSyllabus_ScheduleAllocate.aspx?MainMnu_code=" & ViewState("MainMnu_code") & " &datamode=" & ViewState("datamode") & "&AccId=" & Encr_decrData.Encrypt(h_ACD_ID.Value) & "&TermId=" & Encr_decrData.Encrypt(h_TRM_ID.Value) & "&SubjID=" & Encr_decrData.Encrypt(h_SUBJ_ID.Value) & "&SyllabusId=" & Encr_decrData.Encrypt(h_SYL_ID.Value) & "&GradeId=" & Encr_decrData.Encrypt(h_GRD_ID.Value) & "&SYDId=" & Encr_decrData.Encrypt(strSydId))
            'url = String.Format("~\Curriculum\clmSyllabus_ScheduleAllocate.aspx?MainMnu_code={0}&datamode={1}&AccId={2}&TermId={3}&SubjId={4}&SyllabusId={5}&GradeId={6}&SYDId={7}", ViewState("MainMnu_code"), ViewState("datamode"), Encr_decrData.Encrypt(h_ACD_ID.Value), Encr_decrData.Encrypt(h_TRM_ID.Value), Encr_decrData.Encrypt(h_SUBJ_ID.Value), Encr_decrData.Encrypt(h_SYL_ID.Value), Encr_decrData.Encrypt(h_GRD_ID.Value), Encr_decrData.Encrypt(strSydId))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
   
    Sub ddlgroupbind()
        Dim str_sql As String = String.Empty
        str_sql = "SELECT SGR_ID,SGR_DESCR from GROUPS_M WHERE SGR_BSU_ID = '" & Session("sBsuid").ToString() & "'" & _
        " AND SGR_ACD_ID = '" & ddlAcademicYear.SelectedValue.ToString() & "' and SGR_GRD_ID='" & ddlGrade.SelectedValue & "' and SGR_SBG_ID='" & h_SUBJ_ID.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        ddlgroup.DataSource = ds
        ddlgroup.DataTextField = "SGR_DESCR"
        ddlgroup.DataValueField = "SGR_ID"
        ddlgroup.DataBind()
        If Not ddlgroup.SelectedIndex = -1 Then
            h_GRP_ID.Value = ddlgroup.SelectedItem.Value
        End If
    End Sub
    
    Protected Sub gvTopics_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvTopics.PageIndex = e.NewPageIndex
        GetSyllabusDetails()
    End Sub
End Class

