﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Partial Class Curriculum_clmSmartTagetRelease
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300221") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))


                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    H_TEMP.Value = "0"

                    BindSetup()
                    BindGrade()
                    BindSection()
                    gridbind()



                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindSetup()
        ddlSetup.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TGM_ID,TGM_DESCR  from smart.TARGET_SETUP_m " _
                                       & " WHERE TGM_ACD_ID=" + ddlAcademicYear.SelectedValue



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSetup.DataSource = ds
        ddlSetup.DataTextField = "TGM_DESCR"
        ddlSetup.DataValueField = "TGM_ID"
        ddlSetup.DataBind()

    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " where grm_acd_id=" + ddlAcademicYear.SelectedValue _
                              & " order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
    End Sub
    Sub BindSection()


        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                   & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                   & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue


        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        str_query += " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'"


        str_query += " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        
    End Sub

    Public Sub gridbind()
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet
            lblError.Text = ""
            Dim grade As String()


            grade = ddlGrade.SelectedValue.Split("|")

            'str_Sql = "select distinct STU_SCT_ID ,SCT_DESCR,TSM_RELEASE_SDT ,TSM_RELEASE_EDT,isnull(TSM_bEXPIRE,0) as Expire  from " _
            '          & " vw_STUDENT_DETAILS  left outer join smart.TARGET_STUDENT_M  on STU_ID=TSM_STU_ID and  TSM_TGM_ID =" + ddlSetup.SelectedValue + " where " _
            '          & " SCT_DESCR <>'TEMP' and STU_ACD_ID =" + ddlAcademicYear.SelectedValue + " and STU_GRD_ID ='" + grade(0) + "' order by SCT_DESCR "

            str_Sql = "SELECT SCT_DESCR,SCT_ID STU_SCT_ID,TSM_RELEASE_SDT='01/Jan/1900',TSM_RELEASE_EDT='01/Jan/1900',Expire='FALSE' FROM VW_SECTION_M INNER JOIN VW_GRADE_BSU_M ON SCT_GRM_ID=GRM_ID " _
                 & " WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) _
                 & " AND SCT_DESCR<>'TEMP'"


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)




            If ds.Tables(0).Rows.Count > 0 Then
                grdSect.DataSource = ds.Tables(0)
                grdSect.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdSect.DataSource = ds.Tables(0)
                Try
                    grdSect.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdStud.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdSect.Rows(0).Cells.Clear()
                grdSect.Rows(0).Cells.Add(New TableCell)
                grdSect.Rows(0).Cells(0).ColumnSpan = columnCount
                grdSect.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdSect.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Public Sub gridbindSct()
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet
            lblError.Text = ""
            Dim grade As String()


            grade = ddlGrade.SelectedValue.Split("|")

            str_Sql = "select  STU_ID,STU_NO,STU_NAME,isnull(TSM_ID,0)TSM_ID,TSM_RELEASE_SDT ,TSM_RELEASE_EDT,isnull(TSM_bEXPIRE,0) as Expire  from " _
                      & " vw_STUDENT_DETAILS  left outer join smart.TARGET_STUDENT_M  on STU_ID=TSM_STU_ID and  TSM_TGM_ID =" + ddlSetup.SelectedValue + " where " _
                      & " STU_ACD_ID =" + ddlAcademicYear.SelectedValue + " and STU_GRD_ID ='" + grade(0) + "'"
            str_Sql += " and STU_SCT_ID=" + ddlsection.SelectedValue + " order by TSM_RELEASE_SDT"

           
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                grdStud.DataSource = ds.Tables(0)
                grdStud.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdStud.DataSource = ds.Tables(0)
                Try
                    grdStud.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdStud.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdStud.Rows(0).Cells.Clear()
                grdStud.Rows(0).Cells.Add(New TableCell)
                grdStud.Rows(0).Cells(0).ColumnSpan = columnCount
                grdStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub BindSectDates(ByVal sct_id As String, ByVal txtStart As TextBox, ByVal txtEnd As TextBox, ByVal chkExpire As CheckBox)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_Sql As String = "select TOP 1 TSM_RELEASE_SDT ,TSM_RELEASE_EDT  from " _
                  & " STUDENT_M  INNER JOIN smart.TARGET_STUDENT_M  on STU_ID=TSM_STU_ID and  TSM_TGM_ID =" + ddlSetup.SelectedValue + " where " _
                  & " STU_ACD_ID =" + ddlAcademicYear.SelectedValue + " and STU_SCT_ID ='" + sct_id + "' order by TSM_RELEASE_SDT "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                txtStart.Text = Format(.Item(0), "dd/MMM/yyyy").Replace("01/Jan/1900", "")
                txtEnd.Text = Format(.Item(1), "dd/MMM/yyyy").Replace("01/Jan/1900", "")
            End With
        Else
            chkExpire.Checked = False
            txtStart.Text = ""
            txtEnd.Text = ""

        End If


    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindSetup()
        BindGrade()
        BindSection()
        gridbind()
        gridbindSct()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
        gridbind()
        gridbindSct()
    End Sub
    Protected Sub grdSect_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSect.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim chk As CheckBox = DirectCast(e.Row.FindControl("cbEx"), CheckBox)
            Dim txts As TextBox = e.Row.FindControl("txts")
            Dim txte As TextBox = e.Row.FindControl("txte")
            Dim AtdId As Label = e.Row.FindControl("AtdId")

            BindSectDates(AtdId.Text, txts, txte, chk)

            If chk.Text = "True" Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
            chk.Text = ""
        End If

    End Sub
    Protected Sub grdStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdStud.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim chk As CheckBox = DirectCast(e.Row.FindControl("cb1"), CheckBox)
            If chk.Text = "True" Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
            chk.Text = ""
        End If

    End Sub

    Protected Sub ddlSetup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSetup.SelectedIndexChanged
        gridbind()
        gridbindSct()
    End Sub


    Protected Sub ddlsection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlsection.SelectedIndexChanged
        gridbindSct()
    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click

        Dim i As Integer
        Dim txtf As TextBox
        Dim txtT As TextBox
        If H_TEMP.Value = "1" Then
            For i = 0 To grdStud.Rows.Count - 1
                With grdStud.Rows(i)
                    txtf = .FindControl("txtstdt")
                    txtT = .FindControl("txtedate")

                    txtf.Text = txtFrom.Text
                    txtT.Text = txtTo.Text
                End With
            Next
        Else
            i = 0

            For i = 0 To grdSect.Rows.Count - 1
                With grdSect.Rows(i)
                    txtf = .FindControl("txts")
                    txtT = .FindControl("txte")

                    txtf.Text = txtFrom.Text
                    txtT.Text = txtTo.Text
                End With
            Next
        End If

    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        SaveData()
    End Sub
    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = ""

        Dim i As Integer
        Dim grade As String()

        grade = ddlGrade.SelectedValue.Split("|")
        Dim stId As Label
        Dim tfrm As TextBox
        Dim tto As TextBox
        Dim Cb1 As CheckBox
        Dim chkSelect1 As CheckBox

        For i = 0 To grdStud.Rows.Count - 1
            With grdStud.Rows(i)


                stId = .FindControl("stuId")
                Cb1 = .FindControl("Cb1")
                tfrm = .FindControl("txtstdt")
                tto = .FindControl("txtedate")
                chkSelect1 = .FindControl("chkSelect1")


                Dim cmd As New SqlCommand
                Dim objConn As New SqlConnection(str_conn)
                ' If chkSelect1.Checked = True Then
                Try
                    objConn.Open()
                    cmd = New SqlCommand("SMART.saveTAGET_STUDENT_M", objConn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@TSM_STU_ID", stId.Text)
                    cmd.Parameters.AddWithValue("@TSM_ACD_ID", ddlAcademicYear.SelectedValue)
                    cmd.Parameters.AddWithValue("@TSM_GRD_ID", grade(0))
                    cmd.Parameters.AddWithValue("@TSM_TGM_ID", ddlSetup.SelectedValue)
                    cmd.Parameters.AddWithValue("@TSM_RELEASE_SDT", tfrm.Text)
                    cmd.Parameters.AddWithValue("@TSM_RELEASE_EDT", tto.Text)
                    cmd.Parameters.AddWithValue("@TSM_BEXPIRE", Cb1.Checked)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    cmd.Dispose()
                    objConn.Close()
                Finally
                End Try
                '  End If
            End With
        Next
        gridbind()
        lblError.Text = "Record Saved Successfully"

    End Sub
    Protected Sub grdStud_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdStud.RowCommand
        Try


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(grdStud.Rows(index), GridViewRow)

            If e.CommandName = "Update" Then
                Dim grade As String()
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

                Dim str_query As String = ""
                grade = ddlGrade.SelectedValue.Split("|")

                Dim stId As Label
                Dim tfrm As TextBox
                Dim tto As TextBox
                Dim Cb1 As CheckBox

                stId = selectedRow.FindControl("stuId")
                Cb1 = selectedRow.FindControl("Cb1")
                tfrm = selectedRow.FindControl("txtstdt")
                tto = selectedRow.FindControl("txtedate")



                Dim cmd As New SqlCommand
                Dim objConn As New SqlConnection(str_conn)
                Try
                    objConn.Open()
                    cmd = New SqlCommand("SMART.saveTAGET_STUDENT_M", objConn)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@TSM_STU_ID", stId.Text)
                    cmd.Parameters.AddWithValue("@TSM_ACD_ID", ddlAcademicYear.SelectedValue)
                    cmd.Parameters.AddWithValue("@TSM_GRD_ID", grade(0))
                    cmd.Parameters.AddWithValue("@TSM_TGM_ID", ddlSetup.SelectedValue)
                    cmd.Parameters.AddWithValue("@TSM_RELEASE_SDT", tfrm.Text)
                    cmd.Parameters.AddWithValue("@TSM_RELEASE_EDT", tto.Text)
                    cmd.Parameters.AddWithValue("@TSM_BEXPIRE", Cb1.Checked)


                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                Finally
                    cmd.Dispose()
                    objConn.Close()
                End Try
                gridbind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub grdStud_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdStud.RowUpdating

    End Sub
    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd.Click

        'lnkEdit.CssClass = "button_menu"
        'lnkAdd.CssClass = "button_menu_click"
        mvMaster.ActiveViewIndex = 0
        vis(False)
        t4.ColSpan = "10"
        H_TEMP.Value = "0"
        gridbind()

    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEdit.Click

        'lnkEdit.CssClass = "button_menu_click"
        'lnkAdd.CssClass = "button_menu"
        mvMaster.ActiveViewIndex = 1
        vis(True)
        t4.ColSpan = "0"
        H_TEMP.Value = "1"
        gridbindSct()
    End Sub
    Sub vis(ByVal f As Boolean)
        t1.Visible = f
        't2.Visible = f
        t3.Visible = f
    End Sub

    Protected Sub grdSect_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdSect.RowUpdating

    End Sub
    Protected Sub grdSect_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSect.RowCommand
        Try


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(grdSect.Rows(index), GridViewRow)

            If e.CommandName = "Update" Then
                Dim grade As String()
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

                Dim str_query As String = ""
                grade = ddlGrade.SelectedValue.Split("|")

                Dim stId As Label
                Dim tfrm As TextBox
                Dim tto As TextBox
                Dim Cb1 As CheckBox

                stId = selectedRow.FindControl("AtdId")
                Cb1 = selectedRow.FindControl("cbEx")
                tfrm = selectedRow.FindControl("txts")
                tto = selectedRow.FindControl("txte")

                Dim cmd As New SqlCommand
                Dim objConn As New SqlConnection(str_conn)
                Try
                  
                    objConn.Open()
                    cmd = New SqlCommand("SMART.saveTAGET_STUDENT_M_ALL", objConn)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@TSM_ACD_ID", ddlAcademicYear.SelectedValue)
                    cmd.Parameters.AddWithValue("@TSM_GRD_ID", grade(0))
                    cmd.Parameters.AddWithValue("@TSM_TGM_ID", ddlSetup.SelectedValue)
                    cmd.Parameters.AddWithValue("@TSM_RELEASE_SDT", tfrm.Text)
                    cmd.Parameters.AddWithValue("@TSM_RELEASE_EDT", tto.Text)
                    cmd.Parameters.AddWithValue("@TSM_BEXPIRE", Cb1.Checked)
                    cmd.Parameters.AddWithValue("@SCT_ID", stId.Text)

                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                Finally
                    cmd.Dispose()
                    objConn.Close()
                End Try
                ' gridbind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Sub SaveAllSections()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim i As Integer
        grade = ddlGrade.SelectedValue.Split("|")

        Dim stId As Label
        Dim tfrm As TextBox
        Dim tto As TextBox
        Dim Cb1 As CheckBox
        Dim chkSelect As CheckBox
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(str_conn)
        For i = 0 To grdSect.Rows.Count - 1
            With grdSect.Rows(i)

                stId = .FindControl("AtdId")
                Cb1 = .FindControl("cbEx")
                tfrm = .FindControl("txts")
                tto = .FindControl("txte")
                chkSelect = .FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    Try
                        objConn.Open()
                        cmd = New SqlCommand("SMART.saveTAGET_STUDENT_M_ALL", objConn)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.AddWithValue("@TSM_ACD_ID", ddlAcademicYear.SelectedValue)
                        cmd.Parameters.AddWithValue("@TSM_GRD_ID", grade(0))
                        cmd.Parameters.AddWithValue("@TSM_TGM_ID", ddlSetup.SelectedValue)
                        cmd.Parameters.AddWithValue("@TSM_RELEASE_SDT", tfrm.Text)
                        cmd.Parameters.AddWithValue("@TSM_RELEASE_EDT", tto.Text)
                        cmd.Parameters.AddWithValue("@TSM_BEXPIRE", Cb1.Checked)
                        cmd.Parameters.AddWithValue("@SCT_ID", stId.Text)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                    Finally
                        cmd.Dispose()
                        objConn.Close()
                    End Try
                End If
            End With
        Next
        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub btnSecUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SaveAllSections()

    End Sub

    
End Class
