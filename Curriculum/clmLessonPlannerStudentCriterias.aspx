﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmLessonPlannerStudentCriterias.aspx.vb" Inherits="Curriculum_clmLessonPlannerStudentCriterias"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%-- <link href="../cssfiles/title.css" rel="stylesheet" />--%>

    <style type="text/css">
        /* to freeze column cells and its respecitve header*/
        .FrozenCell {
            /*background-color:#EBEBEB;*/
            position: relative;
            cursor: default;
            left: expression(document.getElementById("div_gridholder").scrollLeft-2);
        }
        /* for freezing column header*/
        .FrozenHeader {
            background-color: #EBEBEB;
            position: relative;
            cursor: default;
            top: expression(document.getElementById("div_gridholder").scrollTop-2);
            z-index: 10;
        }
            /*for the locked columns header to stay on top*/
            .FrozenHeader.locked {
                z-index: 99;
            }
            .min-Rowwidth {
                min-width:100px !important;
                vertical-align:top !important;
                max-width:200px !important;
            }
    </style>
    <script>
        function change_ddl_state(ddlThis, ddlObj) {



            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;


                if (currentid.indexOf(ddlObj) != -1) {

                    if (ddlThis.selectedIndex != -1) {


                        document.forms[0].elements[i].selectedIndex = ddlThis.selectedIndex;

                        if (document.forms[0].elements[i].selectedIndex == 0) {
                            ddlThis.style.color = "black";
                            document.forms[0].elements[i].style.color = "black";
                        }

                        if (document.forms[0].elements[i].selectedIndex == 1) {
                            ddlThis.style.color = "green";
                            document.forms[0].elements[i].style.color = "green";
                        }
                        else if (document.forms[0].elements[i].selectedIndex == 2) {
                            ddlThis.style.color = "#ffe30d";
                            document.forms[0].elements[i].style.color = "#ffe30d";
                        }
                        else if (document.forms[0].elements[i].selectedIndex == 3) {
                            ddlThis.style.color = "red";
                            document.forms[0].elements[i].style.color = "red";
                        }
                    }
                }
            }
        }

        function GetClientId(strid) {
            var count = document.forms[0].length;
            var i = 0;
            var eleName;
            for (i = 0; i < count; i++) {
                eleName = document.forms[0].elements[i].id;
                pos = eleName.indexOf(strid);
                if (pos >= 0) break;
            }
            return eleName;
        }

        function autoWidth(mySelect) {
            var maxlength = 0;

            //   var mySelect=document.getElementById(GetClientId(ddl));


            for (var i = 0; i < mySelect.options.length; i++) {
                if (mySelect[i].text.length > maxlength) {
                    maxlength = mySelect[i].text.length;
                }
            }
            mySelect.style.width = maxlength * 5;
        }

        function setWidth(mySelect) {
            //var mySelect=document.getElementById(GetClientId(ddl));
            mySelect.style.width = 35;
        }

        function setColor(ddlThis) {

            if (ddlThis.selectedIndex == 1) {
                ddlThis.style.color = "green";
            }
            else if (ddlThis.selectedIndex == 2) {
                ddlThis.style.color = "#ffe30d";
            }
            else if (ddlThis.selectedIndex == 3) {
                ddlThis.style.color = "red";
            }
            else  {
                ddlThis.style.color = "black";
            }
        }




        function GetSubjects() {

            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var trmId;
            var sbgId;
            var trmId;
            trmId = document.getElementById('<%=ddlTerm.ClientID %>').value;
            sbgId = document.getElementById('<%=ddlSubject.ClientID %>').value.split("|");
            // result = window.showModalDialog("showTopics_Term.aspx?TermId=" + trmId + "&SbgId=" + sbgId[0], "", sFeatures)

            var oWnd = radopen("showTopics_Term.aspx?TermId=" + trmId + "&SbgId=" + sbgId[0], "pop_topic");

            <%--if (result != "" && result != "undefined") {

                NameandCode = result.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];

            }
            return false;--%>
        }
        function OnClientClose3(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.Topic.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_TopicID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_topic" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Pupil Tracker
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" border="0" cellpadding="0" cellspacing="0"
                    style="vertical-align: top;" width="100%">
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lnkSearch" runat="server" Visible="false">Search</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Panel ID="pnlSearch" runat="server">
                                <table id="Table2" runat="server" cellpadding="5"
                                    cellspacing="0" style="border-collapse: collapse" width="100%">
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Academic Year          </span></td>
                                        <td width="30%">
                                            <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Grade   </span>
                                        </td>
                                        <td width="30%">
                                            <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Subject  </span></td>
                                        <td>
                                            <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left"><span class="field-label">Group   </span></td>
                                        <td>
                                            <asp:DropDownList ID="ddlGroup" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Term      </span></td>
                                        <td>
                                            <asp:DropDownList ID="ddlTerm" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left"><span class="field-label">Topic      </span></td>
                                        <td>
                                            <asp:TextBox ID="txtTopic" runat="server" AutoPostBack="True" Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetSubjects(); return false;"></asp:ImageButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="rdObj1" runat="server" Checked="True" GroupName="g1" Text="Criterias 1 to 25" CssClass="field-label" />
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:RadioButton ID="rdObj2" runat="server" GroupName="g1" Text="Criterias 26 to 50" CssClass="field-label" />
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:RadioButton ID="rdObj3" runat="server" GroupName="g1" Text="Criterias 51 to 75" CssClass="field-label" />
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:RadioButton ID="rdObj4" Visible="false" runat="server" GroupName="g1" Text="Criterias 76 to 100" CssClass="field-label" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"><span class="field-label">Student ID    </span></td>
                                        <td>
                                            <asp:TextBox ID="txtStudentID" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left"><span class="field-label">Student Name    </span></td>
                                        <td>
                                            <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnView" runat="server" CssClass="button" TabIndex="8" Text="View Students"
                                                ValidationGroup="groupM1" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%" align="center">
                    <tr id="trGrid" runat="server">
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td align="left" colspan="4">
                                        <div id="div_gridholder">
                                            <asp:GridView ID="gvStudent" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row" Width="100%" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                HeaderStyle-Height="30" PageSize="20" UseAccessibleHeader="True"
                                                BorderStyle="Solid">
                                                <RowStyle CssClass="" />
                                                <SelectedRowStyle CssClass="Green" />
                                                <%--  --%>
                                                <EditRowStyle Wrap="False" />
                                                <AlternatingRowStyle CssClass="" />
                                                <EmptyDataRowStyle />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Stuid" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="First Name">
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="lnkFirstNameH" runat="server" Text="First Name" OnClick="lnkFirstNameH_Click"
                                                                Font-Underline="false"></asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblFirstName" Font-Underline="false" runat="server"
                                                                Text='<%# Bind("STU_FIRSTNAME") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="FrozenCell" Width="150px"></ItemStyle>
                                                        <HeaderStyle CssClass="FrozenCell" Width="150px" />

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Last Name">
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="lnkLastNameH" runat="server" Text="Last Name" Font-Underline="false"
                                                                OnClick="lnkLastNameH_Click"></asp:LinkButton>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("STU_LASTNAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="FrozenCell"></ItemStyle>
                                                        <HeaderStyle CssClass="FrozenCell" Width="150px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Current Level" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCurrentLevel" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="FrozenCell"
                                                            Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Objectives Completed" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblObjCount" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="FrozenCell"
                                                            Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Stuno" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                        <HeaderStyle Wrap="true" Width="10%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>

                                                            <asp:Label ID="lblObj1H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId1" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId1" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt1" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH1" runat="server" OnChange="Javascript:change_ddl_state(this,'ddlObj1Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj1Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                        <HeaderStyle Wrap="true" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj2H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId2" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId2" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt2" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH2" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj2Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj2Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj3H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId3" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId3" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt3" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH3" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj3Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj3Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>

                                                            <asp:Label ID="lblObj4H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId4" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId4" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt4" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH4" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj4Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj4Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj5H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId5" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId5" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt5" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH5" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj5Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj5Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj6H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId6" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId6" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt6" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH6" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj6Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj6Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj7H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId7" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId7" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt7" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH7" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj7Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj7Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj8H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId8" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId8" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt8" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH8" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj8Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj8Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj9H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId9" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId9" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt9" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH9" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj9Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj9Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj10H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId10" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId10" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt10" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH10" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj10Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj10Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj11H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId11" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId11" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt11" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH11" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj11Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj11Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj12H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId12" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId12" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt12" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH12" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj12');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj12Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>

                                                            <asp:Label ID="lblObj13H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId13" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId13" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt13" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH13" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj13Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj13Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj14H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId14" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId14" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt14" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH14" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj14Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj14Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj15H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId15" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId15" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt15" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH15" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj15Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj15Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj16H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId16" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId16" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt16" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH16" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj16Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj16Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj17H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId17" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId17" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt17" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH17" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj17Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj17Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj18H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId18" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId18" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt18" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH18" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj18Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj18Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>

                                                            <asp:Label ID="lblObj19H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId19" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId19" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt19" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH19" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj19Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj19Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>

                                                            <asp:Label ID="lblObj20H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId20" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId20" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblObjDt20" runat="server"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH20" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj20Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj20Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>

                                                                        <asp:Label ID="lblObj21H" runat="server"></asp:Label>
                                                                        <asp:Label ID="lblObjId21" runat="server" Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblSydId21" runat="server" Visible="false"></asp:Label>
<br />
                                                                        <asp:DropDownList ID="ddlObjH21" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj21Id');">
                                                                            <asp:ListItem Text="--" Value="--" />
                                                                            <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                            <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                            <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                                        </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj21Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>

                                                            <asp:Label ID="lblObj22H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId22" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId22" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH22" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj22Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj22Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>

                                                            <asp:Label ID="lblObj23H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId23" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId23" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH23" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj23Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj23Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj24H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId24" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId24" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH24" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj24Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj24Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle CssClass="min-Rowwidth" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblObj25H" runat="server"></asp:Label>
                                                            <asp:Label ID="lblObjId25" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblSydId25" runat="server" Visible="false"></asp:Label>
                                                            <br />
                                                            <asp:DropDownList ID="ddlObjH25" runat="server" OnChange="javascript:change_ddl_state(this,'ddlObj25Id');">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red" />
                                                            </asp:DropDownList>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlObj25Id" runat="server" OnChange="Javascript:setColor(this);">
                                                                <asp:ListItem Text="--" Value="--" />
                                                                <asp:ListItem Text="E" Value="E" style="color: green;" title="Exceeds" />
                                                                <asp:ListItem Text="M" Value="M" style="color: #ffe30d;" title="Meets" />
                                                                <asp:ListItem Text="W" Value="W" style="color: red;" title="Working towards" />
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trSave" runat="server">
                                    <td width="200px">
                                        <span class="field-label">Completion Date : </span>
                                    </td>
                                    <td width="250px">
                                        <asp:TextBox ID="txtDate" runat="server"> </asp:TextBox><asp:ImageButton
                                            ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtDate" PopupPosition="TopRight">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td colspan="20">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_LVL_ID" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_TopicID" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
</asp:Content>
