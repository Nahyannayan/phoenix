﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Curriculum_clmAssessmentDetails_KGS
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "C100192") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    BindReportSchedule()
                    Session("CurrSuperUser") = "Y"
                    If Session("CurrSuperUser") = "Y" Then
                        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    Else
                        ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
                    End If
                    ViewState("SelectedRow") = -1
                    ShowHideGrid(False)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub

    Sub BindReportSchedule()

        Dim grades As String()
        Dim grade As String
        If ddlGrade.SelectedValue <> "" Then
            grades = ddlGrade.SelectedValue.Split("|")
            grade = grades(0)
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str As String = ""
        Dim i As Integer
        Dim str_query As String = "SELECT DISTINCT RPF_DESCR,RPF_ID,RSM_DISPLAYORDER,RPF_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A" _
                                & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID " _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON B.RSM_ID=C.RSG_RSM_ID" _
                                & " WHERE RSM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND RSG_GRD_ID='" + grade + "' AND " _
                                & " RSM_ID=" + ddlReportCard.SelectedValue + " ORDER BY RSM_DISPLAYORDER,RPF_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlRPF.DataSource = ds
        ddlRPF.DataTextField = "RPF_DESCR"
        ddlRPF.DataValueField = "RPF_ID"
        ddlRPF.DataBind()

    End Sub

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Function PopulateSubjectsByTeacher(ByVal emp_id As String, ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SGS_EMP_ID=" + emp_id _
                                 & " AND SGS_TODATE IS NULL"

        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportCard()

        ddlReportCard.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_bSKILLS=1 AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        If Session("sbsuid") = "123004" And Session("clm") = 3 Then
            str_query += " AND (RSM_DESCR LIKE '%KG1%' OR RSM_DESCR LIKE '%KG2%')"
        End If
        str_query += " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ShowHideGrid(False)
        BindReportCard()
        If ddlReportCard.Items.Count > 0 Then
            btnAddCriteria.Enabled = True
            ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
            BindReportSchedule()
            Session("CurrSuperUser") = "Y"
            If Session("CurrSuperUser") = "Y" Then
                ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            Else
                ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
            End If
        Else
            lblError.Text = "Reports are not available for the chosen Academic Year."
            btnAddCriteria.Enabled = False
        End If
        
    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        trGrid.Visible = False
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        BindReportSchedule()
        Session("CurrSuperUser") = "Y"
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ShowHideGrid(False)
        BindReportSchedule()
        Session("CurrSuperUser") = "Y"
        If Session("CurrSuperUser") = "Y" Then
            ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        Else
            ddlSubject = PopulateSubjectsByTeacher(Session("EmployeeID"), ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        End If
    End Sub

    Protected Sub btnAddCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCriteria.Click
        BindGrid()
    End Sub

    Sub ShowHideGrid(ByVal bool As Boolean)
        trGrid.Visible = bool
        trSave.Visible = bool
    End Sub

    Sub BindGrid()

        ShowHideGrid(True)
        Dim grades As String()
        Dim grade As String = ""
        If ddlGrade.SelectedValue <> "" Then
            grades = ddlGrade.SelectedValue.Split("|")
            grade = grades(0)
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        'Dim str_query As String = "SELECT SBT_ID,SBT_DESCR FROM SUBJECT_REPORT_CATEGORY WHERE " _
        '                        & " SBT_ACD_ID=" + ddlAcademicYear.SelectedValue + " AND SBT_GRD_ID='" + grade + "' AND SBT_SBG_ID='" + ddlSubject.SelectedValue + "'"

        Dim str_query As String = "SELECT DISTINCT SBT_ID,SBT_DESCR,ISNULL(SAD_CATEGORY_SUB_NAME,'') as SAD_CATEGORY_SUB_NAME FROM SUBJECT_REPORT_CATEGORY " _
                                & " INNER JOIN RPT.REPORT_SETUP_D ON SBT_SBG_ID=RSD_SBG_ID AND RSD_SBT_ID=SBT_ID " _
                                & " LEFT OUTER JOIN SUBJECT_ASSESSMENT_DETAILS ON SBT_SBG_ID=SAD_SBG_ID AND RSD_SBT_ID=SAD_SBT_ID AND SAD_RPF_ID=" + ddlRPF.SelectedValue + " " _
                                & " WHERE  SBT_ACD_ID=" + ddlAcademicYear.SelectedValue + " AND SBT_GRD_ID=" + grade + " AND SBT_SBG_ID=" + ddlSubject.SelectedValue

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvCriteria.DataSource = ds
        gvCriteria.DataBind()

    End Sub
    Dim count As Integer = 0


    Protected Sub gvCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCriteria.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If e.Row.RowIndex > 0 Then
                count = count + 1
            End If
            Dim lblCategoryID As Label = CType(e.Row.FindControl("lblCategoryID"), Label)
            Dim strCategoryID As String = lblCategoryID.Text
            Dim gvDetails As GridView = CType(e.Row.FindControl("gvDetails"), GridView)

            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT SBT_DESCR,RSD_ID,RSD_HEADER,RSD_SUB_SKILLS,ISNULL(CONVERT(VARCHAR(30),SAD_SUB_SKILLS_MAXMARKS),'') AS SAD_SUB_SKILLS_MAXMARKS" _
                                    & " FROM SUBJECT_REPORT_CATEGORY " _
                                    & " INNER JOIN RPT.REPORT_SETUP_D ON SBT_SBG_ID=RSD_SBG_ID " _
                                    & " AND RSD_SBT_ID=SBT_ID LEFT OUTER JOIN SUBJECT_ASSESSMENT_DETAILS ON SAD_RSD_ID=RSD_ID " _
                                    & " AND SAD_RPF_ID=" + ddlRPF.SelectedValue + " WHERE RSD_RSM_ID=" + ddlReportCard.SelectedValue + " AND RSD_SBT_ID='" + strCategoryID + "'" _
                                    & " AND RSD_SBG_ID='" + ddlSubject.SelectedValue + "' "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvDetails.DataSource = ds
            gvDetails.DataBind()
            Dim txtMaxMarks As TextBox = e.Row.Cells(1).FindControl("txtMaxMarks")


        End If
    End Sub

    Protected Sub ddlRPF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRPF.SelectedIndexChanged
        ShowHideGrid(False)
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        ShowHideGrid(False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim strXml As String = ""
        Try
            Dim grades As String()
            Dim grade As String = ""
            If ddlGrade.SelectedValue <> "" Then
                grades = ddlGrade.SelectedValue.Split("|")
                grade = grades(0)
            End If
            Dim i As Integer
            Dim j As Integer
            For i = 0 To gvCriteria.Rows.Count - 1

                With gvCriteria.Rows(i)

                    Dim lblCategoryID As Label = gvCriteria.Rows(i).FindControl("lblCategoryID")
                    Dim txtComments As TextBox = gvCriteria.Rows(i).FindControl("txtComments")
                    Dim gvDetails As GridView = gvCriteria.Rows(i).FindControl("gvDetails")

                    For j = 0 To gvDetails.Rows.Count - 1

                        With gvDetails.Rows(j)
                            Dim lblRSDId As Label = gvDetails.Rows(j).FindControl("lblRSDId")
                            Dim lblSkills As Label = gvDetails.Rows(j).FindControl("lblSkills")
                            Dim txtMaxMarks As TextBox = gvDetails.Rows(j).FindControl("txtMaxMarks")
                           

                            strXml += "<ID><SAD_ACD_ID>" + ddlAcademicYear.SelectedValue + "</SAD_ACD_ID>"
                            strXml += "<SAD_RSM_ID>" + ddlReportCard.SelectedValue + "</SAD_RSM_ID>"
                            strXml += "<SAD_GRD_ID>" + grade + "</SAD_GRD_ID>"
                            strXml += "<SAD_RPF_ID>" + ddlRPF.SelectedValue + "</SAD_RPF_ID>"
                            strXml += "<SAD_SBG_ID>" + ddlSubject.SelectedValue + "</SAD_SBG_ID>"
                            strXml += "<SAD_SBT_ID>" + lblCategoryID.Text + "</SAD_SBT_ID>"
                            strXml += "<SAD_RSD_ID>" + lblRSDId.Text + "</SAD_RSD_ID>"
                            strXml += "<SAD_RSD_SUB_SKILLS>" + lblSkills.Text + "</SAD_RSD_SUB_SKILLS>"
                            strXml += "<SAD_SUB_SKILLS_MAXMARKS>" + txtMaxMarks.Text + "</SAD_SUB_SKILLS_MAXMARKS>"
                            strXml += "<SAD_CATEGORY_SUB_NAME>" + txtComments.Text + "</SAD_CATEGORY_SUB_NAME></ID>"

                        End With
                    Next
                End With
            Next


            strXml = "<IDS>" + strXml + "</IDS>"
            strXml = strXml.Replace("&", "&amp;")
            
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@assessmentData", strXml)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DBO.SUBJECT_ASSESSMENT_DTL", param)
            lblError.Text = "Records saved successfully"
            BindGrid()
        Catch ex As Exception
            lblError.Text = "Records could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        
    End Sub
End Class
