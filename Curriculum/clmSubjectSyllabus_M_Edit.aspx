<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmSubjectSyllabus_M_Edit.aspx.vb" Inherits="Curriculum_clmSubjectSyllabus_M_Edit" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Subject Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                     HeaderText="You must enter a value in the following fields:" 
                    ValidationGroup="groupM1"  />            
            </td>
        </tr>
        <tr>
         <td align="left"  valign="bottom">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label></td>
        </tr>
        <tr>
            <td >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    
                    
                         <tr>
                        <td align="left" width="20%">
                         <span class="field-label">  Subject </span></td>
                  
                        <td align="left" >
                               <asp:TextBox ID="txtSubject" Enabled="false" runat="server" TabIndex="2" MaxLength="100"></asp:TextBox>
                      </td>   
                      
                       <td align="left" >
                        <span class="field-label">  Curriculum </span></td>
                       
                        <td align="left" >
                               <asp:TextBox ID="txtCurriculum" Enabled="false" runat="server" TabIndex="2" MaxLength="100"></asp:TextBox>
                      </td>  
                             
                    </tr>
                    
                    <tr>
                     
                      
                       <td align="left" >
                      <span class="field-label">  Board Code</span></td>
                      
                        <td align="left">
                         <asp:TextBox ID="txtBoard"  runat="server" TabIndex="2" MaxLength="3"></asp:TextBox>  
                      </td> 

                        <td align="left" colspan="2">&nbsp;</td>
                             
                    </tr>
                                       
                     </table>
               </td>
        </tr>
       
        <tr>
            <td  align="center">
               <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
        </tr>
        <tr>
            <td >
                &nbsp;<asp:HiddenField ID="hfSBS_ID" runat="server" />
          <asp:RequiredFieldValidator ID="rfsUBJECT" runat="server" ErrorMessage="Please enter the field Subject" ControlToValidate="txtSubject" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
          </td>
          
        </tr>
    </table>

            </div>
        </div>
    </div>
</asp:Content>

