<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmConsolidatedExcel_Upload.aspx.vb" Inherits="Curriculum_ConsolidatedReports_clmConsolidatedExcel_View" title="Untitled Page"  %>


    <asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<script language="javascript" type="text/javascript">

function fnSelectAll(master_box)
{
 var curr_elem;
 var checkbox_checked_status;
 for(var i=0; i<document.forms[0].elements.length; i++)
 {
     curr_elem = document.forms[0].elements[i];
         if (curr_elem.id.substring(0, 30) == 'ctl00_cphMasterpage_lstSubject')
    {
  curr_elem.checked = master_box.checked;
  }
 }
// master_box.checked=!master_box.checked;
}





</script>

        <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <script>
        function Popup(url) {
            $.fancybox({
                'width': '80%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
    </script>

         <style type="text/css">
        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}
.RadComboBox_Default {
    width:100% !important;
}
.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80% !important;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Label id="lblHeader" runat="server" Text="Report Card "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <asp:Label id="lblerror" runat="server" CssClass="error" ></asp:Label>
    <table class ="BlueTable" id="tblrule" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%" >
                    
                    <tr>
                        <td align="left" width="25%">
                            <span class="field-label">Academic Year</span></td>
                        
                        <td align="left" width="35%">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Report Card</span></td>
                        
                        <td align="left">
                            <asp:DropDownList id="ddlReportCard" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
        <tr>
            <td align="left">
                <span class="field-label">Report Schedule</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlPrintedFor" runat="server">
                </asp:DropDownList></td>
            <td></td>
                        <td></td>
        </tr>
        <tr>
            <td align="left">
                            <span class="field-label">Grade</span></td>
           
            <td align="left">
                <asp:DropDownList id="ddlGrade" runat="server">
                </asp:DropDownList></td>
            <td></td>
                        <td></td>
        </tr>

                    <tr   >
                        <td align="left">
                            <span class="field-label">Header</span></td>
                       
                        <td align="left">
                            <asp:CheckBoxList id="lstHeader" runat="server">
                            </asp:CheckBoxList>
                            </td>
                        <td></td>
                        <td></td>
                    </tr>
        <tr runat="server" id="trfilter">
            
            
            <td align="left">
                            <asp:DropDownList ID="ddlType" runat="server">
                                <asp:ListItem>ALL</asp:ListItem>
                                <asp:ListItem>SEN</asp:ListItem>
                                <asp:ListItem>EAL</asp:ListItem>
                                <asp:ListItem Value="GT">Gifted &amp; Talented</asp:ListItem>
                                <asp:ListItem>EMIRATI</asp:ListItem>
                            </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" width="25%">
                <span class="field-label">Upload File</span></td>
            <td >                
                    <table>
                        <tr>                            
                            <td >
                                <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True"
                                    Width="204px" />
                            </td>
                            <td>
                               
                            </td>
                        </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="text-align: center">
                <asp:Button ID="btnGenerateExcel" runat="server" CausesValidation="False" CssClass="button"
                                    TabIndex="30" Text="Generate Excel" />
                 <asp:Button ID="btnUpload" runat="server" CausesValidation="False" CssClass="button"
                                    TabIndex="30" Text="Submit"  />
            </td>

        </tr>
                </table>
    <asp:HiddenField id="hfComments" runat="server">
    </asp:HiddenField>
    

    <asp:HiddenField ID="hfbDownload" runat="server" />
    

   
        </div>
            </div>
        </div>

</asp:Content>

