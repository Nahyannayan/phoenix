﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmCASEQuestions_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state


            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C410055") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                BindAcademicYear()
                BindSkillMaster()
                BindGrade()
                BindSubject()

                If ViewState("datamode") = "edit" Then

                    hfQSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("qsmid").Replace(" ", "+"))
                    ddlSubject.ClearSelection()
                    ddlSubject.Items.FindByValue(Encr_decrData.Decrypt(Request.QueryString("sbmid").Replace(" ", "+"))).Selected = True
                    ddlSubject.Enabled = False
                    ddlAcademicYear.ClearSelection()
                    ddlAcademicYear.Items.FindByValue(Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))).Selected = True
                    ddlAcademicYear.Enabled = False
                    ddlGrade.ClearSelection()
                    ddlGrade.Items.FindByValue(Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))).Selected = True
                    ddlGrade.Enabled = False
                    txtDescription.Text = Encr_decrData.Decrypt(Request.QueryString("question").Replace(" ", "+"))
                    txtTotalMarks.Text = Encr_decrData.Decrypt(Request.QueryString("totalmarks").Replace(" ", "+"))
                    txtTotalQuestions.Text = Encr_decrData.Decrypt(Request.QueryString("totalquestion").Replace(" ", "+"))
                    BindUnit()
                    GridBind()
                    gvQuestions.Attributes.Add("bordercolor", "#1b80b6")
                Else
                    trGrid1.Visible = False
                    trGrid2.Visible = False
                    trGrid3.Visible = False
                    hfQSM_ID.Value = "0"
                    gvQuestions.Attributes.Add("bordercolor", "#1b80b6")
                End If
              
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_DESCR,ACY_ID FROM ACADEMICYEAR_M WHERE ACY_ID>=23"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACY_ID"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ACY_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                              & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("SBSUID") + "' AND ACD_CLM_ID=" + Session("CLM")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT GRD_DISPLAY,GRD_ID FROM VW_GRADE_M WHERE GRD_ID  IN('04','06','08') ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSkillMaster()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SKL_ID,SKL_DESCR FROM CE.SKILLS_M ORDER BY SKL_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ViewState("dsSkill") = ds
    End Sub

    Sub BindUnit()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBU_ID,SBU_DESCR FROM CE.SUBJECTS_UNITS_M WHERE SBU_SBM_ID=" + ddlSubject.SelectedValue.ToString _
                                 & " AND SBU_ACY_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND SBU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                 & " ORDER BY SBU_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ViewState("dsUnit") = ds
    End Sub
    Sub BindSubSkills(ByVal ddlSubSkills As DropDownList, ByVal skl_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SKS_ID,SKS_DESCR FROM CE.SKILLS_SUB WHERE SKS_SKL_ID=" + skl_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubSkills.DataSource = ds
        ddlSubSkills.DataTextField = "SKS_DESCR"
        ddlSubSkills.DataValueField = "SKS_ID"
        ddlSubSkills.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubSkills.Items.Insert(0, li)
    End Sub
    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBM_DESCR,SBM_ID FROM SUBJECT_M" _
                               & " WHERE SBM_DESCR IN('MATHEMATICS','ENGLISH','SCIENCE','ARABIC')"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBM_DESCR"
        ddlSubject.DataValueField = "SBM_ID"
        ddlSubject.DataBind()
    End Sub
    Sub SaveData()

        Dim lblDelete As Label

        Dim ddlSkill As DropDownList
        Dim ddlSubSkills As DropDownList
        Dim ddlUnit As DropDownList
        Dim lblQsdId As Label
        Dim txtQuestion As TextBox
        Dim txtMax As TextBox
        Dim sks_id As String
        Dim Descr As String = txtDescription.Text
        Dim i As Integer

        If Descr = "" Then
            Descr = ddlAcademicYear.SelectedValue.ToString + " " + ddlGrade.SelectedValue.ToString + " " + ddlSubject.SelectedValue.ToString
        End If


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC CE.saveQUESTIONS_M" _
                                & " @QSM_ID=" + hfQSM_ID.Value + "," _
                                & " @QSM_ACY_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                & " @QSM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                                & " @QSM_SBM_ID=" + ddlSubject.SelectedValue.ToString + "," _
                                & " @QSM_DESCR='" + Descr + "'," _
                                & " @QSM_TOTALMARKS=" + Val(txtTotalMarks.Text).ToString + "," _
                                & " @QSM_TOTALQUESTIONS=" + Val(txtTotalQuestions.Text).ToString + "," _
                                & " @MODE='" + ViewState("datamode") + "'"

        hfQSM_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString

        Dim mode As String

        For i = 0 To gvQuestions.Rows.Count - 1
            With gvQuestions.Rows(i)
                lblQsdId = .FindControl("lblQsdId")
                ddlSkill = .FindControl("ddlSkill")
                ddlUnit = .FindControl("ddlUnit")
                txtQuestion = .FindControl("txtQuestion")
                lblDelete = .FindControl("lblDelete")
                txtMax = .FindControl("txtMax")
                ddlSubSkills = .FindControl("ddlSubSkills")
                If ddlSubSkills.Items.Count = 0 Then
                    sks_id = "0"
                Else
                    sks_id = ddlSubSkills.SelectedValue.ToString
                End If
                If lblDelete.Text = "1" Then
                    mode = "delete"
                ElseIf lblQsdId.Text = "0" Then
                    mode = "add"
                ElseIf lblQsdId.Text <> "0" Then
                    mode = "edit"
                Else
                    mode = ""
                End If

                If mode <> "" Then
                    str_query = "exec CE.saveQUESTIONS_D" _
                          & " @QSD_ID=" + lblQsdId.Text + "," _
                          & " @QSD_QSM_ID=" + hfQSM_ID.Value + "," _
                          & " @QSD_DESCR='" + txtQuestion.Text + "'," _
                          & " @QSD_MAXMARK=" + Val(txtMax.Text).ToString + "," _
                          & " @QSD_SBU_ID=" + ddlUnit.SelectedValue.ToString + "," _
                          & " @QSD_SKL_ID=" + ddlSkill.SelectedValue.ToString + "," _
                          & " @QSD_SKS_ID=" + sks_id + "," _
                          & " @QSD_ORDER=" + (i + 1).ToString + "," _
                          & " @MODE='" + mode + "'"


                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QSD_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QSD_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QSD_SKL_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QSD_SKS_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QSD_MAXMARK"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QSD_SBU_ID"
        dt.Columns.Add(column)
        Return dt
    End Function
    Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim j As Integer = 0
        Dim i As Integer
        Dim ds As DataSet
        Dim dr As DataRow
        If hfQSM_ID.value <> "0" Then
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "SELECT QSD_ID,QSD_DESCR,isnull(QSD_SKL_ID,0),isnull(QSD_SKS_ID,0),isnull(QSD_MAXMARK,0),isnull(QSD_SBU_ID,0) FROM  " _
                                  & " CE.QUESTIONS_D WHERE QSD_QSM_ID='" + hfQSM_ID.Value + "'"


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            For i = 0 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    dr = dt.NewRow
                    dr.Item(0) = .Item(0).ToString
                    dr.Item(1) = .Item(1)
                    dr.Item(2) = "edit"
                    dr.Item(3) = i.ToString
                    dr.Item(4) = .Item(2).ToString
                    dr.Item(5) = .Item(3).ToString
                    dr.Item(6) = .Item(4).ToString.Replace(".00", "")
                    dr.Item(7) = .Item(5).ToString
                    dt.Rows.Add(dr)
                End With
            Next
            j = ds.Tables(0).Rows.Count
        End If

        If Val(txtTotalQuestions.Text) - j > 0 Then
            For i = 0 To Val(txtTotalQuestions.Text) - j - 1
                dr = dt.NewRow
                dr.Item(0) = "0"
                dr.Item(1) = "Q" + (j + i + 1).ToString
                dr.Item(2) = "add"
                dr.Item(3) = (j + i).ToString
                dr.Item(4) = "0"
                dr.Item(5) = "0"
                dr.Item(6) = ""
                dr.Item(7) = "0"
                dt.Rows.Add(dr)
            Next
        End If
        Session("dtQuestions") = dt



        gvQuestions.DataSource = dt
        gvQuestions.DataBind()

        Dim k As Integer
        k = gvQuestions.Rows.Count
        If k > 0 Then
            Dim lnkDelete As LinkButton
            lnkDelete = gvQuestions.Rows(k - 1).FindControl("lnkDelete")
            lnkDelete.Visible = True
        End If
    End Sub

#End Region

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvQuestions.Rows(Val(lblindex.Text)).Visible = False

            If Val(lblindex.Text) <> 0 Then
                Dim lnkDelete As LinkButton = gvQuestions.Rows(Val(lblindex.Text) - 1).FindControl("lnkDelete")
                lnkDelete.Visible = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlSkill_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlSkill As DropDownList = TryCast(sender.Findcontrol("ddlSkill"), DropDownList)
        Dim ddlSubSkills As DropDownList = TryCast(sender.Findcontrol("ddlSubSkills"), DropDownList)
        Dim lblSksId As Label = TryCast(sender.FindControl("lblSksId"), Label)
        If ddlSkill.SelectedValue.ToString <> "0" Then
            BindSubSkills(ddlSubSkills, ddlSkill.SelectedValue.ToString)
            If ddlSubSkills.Items.Count > 1 Then
                ddlSubSkills.Visible = True
            Else
                ddlSubSkills.Visible = False
            End If
            If lblSksId.Text <> "0" Then
                If Not ddlSubSkills.Items.FindByValue(lblSksId.Text) Is Nothing Then
                    ddlSubSkills.Items.FindByValue(lblSksId.Text).Selected = True
                End If
            End If
        Else
            ddlSubSkills.Visible = False
        End If

    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        BindUnit()
        trGrid1.Visible = True
        trGrid2.Visible = True
        trGrid3.Visible = True
        GridBind()
    End Sub

    Protected Sub gvQuestions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvQuestions.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblSklId As Label = e.Row.FindControl("lblSklId")
            Dim lblSksId As Label = e.Row.FindControl("lblSksId")
            Dim lblSbuId As Label = e.Row.FindControl("lblSbuId")
            Dim ddlSkill As DropDownList = e.Row.FindControl("ddlSkill")
            Dim ddlSubSkills As DropDownList = e.Row.FindControl("ddlSubSkills")
            Dim ddlUnit As DropDownList = e.Row.FindControl("ddlUnit")


            ddlSkill.DataSource = ViewState("dsSkill")
            ddlSkill.DataTextField = "SKL_DESCR"
            ddlSkill.DataValueField = "SKL_ID"
            ddlSkill.DataBind()

            ddlUnit.DataSource = ViewState("dsUnit")
            ddlUnit.DataTextField = "SBU_DESCR"
            ddlUnit.DataValueField = "SBU_ID"
            ddlUnit.DataBind()

            Dim li As New ListItem
            li.Text = "--"
            li.Value = "0"
            ddlSkill.Items.Insert(0, li)

            li = New ListItem
            li.Text = "--"
            li.Value = "0"
            ddlUnit.Items.Insert(0, li)

            If lblSbuId.Text <> "0" Then
                If Not ddlUnit.Items.FindByValue(lblSbuId.Text) Is Nothing Then
                    ddlUnit.Items.FindByValue(lblSbuId.Text).Selected = True
                End If
            End If

            If lblSklId.Text <> "0" Then
                If Not ddlSkill.Items.FindByValue(lblSklId.Text) Is Nothing Then
                    ddlSkill.Items.FindByValue(lblSklId.Text).Selected = True
                End If
                BindSubSkills(ddlSubSkills, lblSklId.Text)
                If ddlSubSkills.Items.Count > 1 Then
                    ddlSubSkills.Visible = True
                Else
                    ddlSubSkills.Visible = False
                End If
                If lblSksId.Text <> "0" Then
                    If Not ddlSubSkills.Items.FindByValue(lblSksId.Text) Is Nothing Then
                        ddlSubSkills.Items.FindByValue(lblSksId.Text).Selected = True
                    End If
                End If
          
            End If

        End If
    End Sub

    'Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
    '    BindUnit()
    'End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        ViewState("datamode") = "edit"
        GridBind()

    End Sub

    Protected Sub lnkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddNew.Click
        ViewState("datamode") = Encr_decrData.Encrypt("add")
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim url As String
        url = String.Format("~\Curriculum\clmCASEQuestions_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)
    End Sub
End Class
