

Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports ActivityFunctions

Partial Class Curriculum_clmSyllabus_D_AddEdit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim chk As Integer = 1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Session("SyllabusDet") = SyllabusMaster.CreateDTSyllabusDetails()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") = "C100025") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_SyllabusId.Value = Encr_decrData.Decrypt(Request.QueryString("SyllabusId").Replace(" ", "+")) '"ACDID"
                    h_ACCID.Value = Encr_decrData.Decrypt(Request.QueryString("AccId").Replace(" ", "+")) '"ACDID"
                    h_TERMID.Value = Encr_decrData.Decrypt(Request.QueryString("TermId").Replace(" ", "+")) '"TERMID"
                    h_SUBJID.Value = Encr_decrData.Decrypt(Request.QueryString("SubjID").Replace(" ", "+")) '"SUBJID"
                    ClearPartialDetails()
                    PopulateParentSyllabus()
                    GetSyllabusDetails()
                    ViewState("datamode") = "add"
                    If ViewState("datamode") = "add" Then

                        Session("gintGridLine") = 0

                    Else
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    End If
                    ' Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindNextOrder()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ' If Page.IsPostBack = False Then
        chk = 1
        SaveSyllabus()
        'End If
    End Sub
    'Function calltransaction(ByRef errorMessage As String) As Integer
    '    Dim bEdit As Boolean
    '    Dim CAM_ID As String = ""
    '    'Dim CLM_DESC As String = txtCLM_DESC.Text


    '    If ViewState("datamode") = "add" Then
    '        Dim transaction As SqlTransaction

    '        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
    '            transaction = conn.BeginTransaction("SampleTransaction")
    '            Try

    '                Dim status As Integer

    '                bEdit = False


    '                ' status = ACTIVITYMASTER.SAVEACTIVITY_M(CAM_ID, CLM_DESC, bEdit, transaction)


    '                If status <> 0 Then
    '                    calltransaction = "1"
    '                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
    '                    Return "1"
    '                End If
    '                '  End If
    '                ' Next
    '                ViewState("viewid") = "0"
    '                ViewState("datamode") = "none"

    '                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '                calltransaction = "0"

    '            Catch ex As Exception
    '                calltransaction = "1"
    '                errorMessage = "Error Occured While Saving."
    '            Finally
    '                If calltransaction <> "0" Then
    '                    UtilityObj.Errorlog(errorMessage)
    '                    transaction.Rollback()
    '                Else
    '                    errorMessage = ""
    '                    transaction.Commit()
    '                End If
    '            End Try

    '        End Using
    '    ElseIf ViewState("datamode") = "edit" Then
    '        Dim transaction As SqlTransaction

    '        Using conn As SqlConnection = ConnectionManger.GetOASIS_CURRICULUMConnection
    '            transaction = conn.BeginTransaction("SampleTransaction")
    '            Try
    '                Dim status As Integer


    '                CAM_ID = ViewState("viewid")
    '                bEdit = True


    '                'status = ACTIVITYMASTER.SAVEACTIVITY_M(CAM_ID, CLM_DESC, bEdit, transaction)


    '                If status <> 0 Then
    '                    calltransaction = "1"
    '                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
    '                    Return "1"
    '                End If


    '                ViewState("viewid") = "0"
    '                ViewState("datamode") = "none"

    '                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '                calltransaction = "0"

    '                ViewState("viewid") = 0
    '            Catch ex As Exception
    '                calltransaction = "1"
    '                errorMessage = "Error Occured While Saving."
    '            Finally
    '                If calltransaction <> "0" Then
    '                    UtilityObj.Errorlog(errorMessage)
    '                    transaction.Rollback()
    '                Else
    '                    errorMessage = ""
    '                    transaction.Commit()
    '                End If
    '            End Try

    '        End Using
    '    End If
    'End Function

#End Region

   

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try

            'ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                'Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub
    Private Sub ClearControls()
        'txtSubject.Text = ""
        'txtSubject.Text = ""
        txtSubTopic.Text = ""
        txtToDate.Text = ""
        txtFromDate.Text = ""
        txtParentTopic.Text = ""
        txtHrs.Text = ""
        txtobjective.Text = ""
        txtResource.Text = ""
        gvSyllabus.DataSource = Nothing
        gvSyllabus.DataBind()
        Session("gintGridLine") = 0
        Session("SyllabusDet") = Nothing
        h_SYD_ID.Value = 0
        h_ParentTopicID.Value = 0
        ViewState("datamode") = "add"
    End Sub

    Sub BindNextOrder()
        Dim dt As DataTable = Session("SyllabusDet")
        Dim dv As DataView = dt.DefaultView
        ' dv.Sort = "SYBORDER"
        dv.RowFilter = "SYBORDER=MAX(SYBORDER)"
        Dim i As Integer = dv.Table.Rows.Count
        If i = 0 Then
            txtOrder.Text = 1
        Else
            txtOrder.Text = Val(dv.Item(0)("SYBORDER")) + 1
        End If

    End Sub

    Sub FillAcd()
        Dim str_con As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM VW_ACADEMICYEAR_M AS A INNER JOIN VW_ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub
    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'FillSubjects()
    End Sub
    Private Function AddDetails(ByVal htFEE_DET As Hashtable) As Hashtable

    End Function

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        Dim ldrNew As DataRow
        Dim lintIndex As Integer
        If IsDate(txtFromDate.Text) = False Then
            lblError.Text = "Invalid FromDate"
            Exit Sub
        End If
        If IsDate(txtToDate.Text) = False Then
            lblError.Text = "Invalid ToDate"
            Exit Sub
        End If
        If Convert.ToDateTime(txtFromDate.Text) > Convert.ToDateTime(txtToDate.Text) Then
            lblError.Text = "From Date must be less than ToDate"
            Exit Sub
        End If
        If Trim(txtHrs.Text) = "" Then
            lblError.Text = "Specify Total Hours"
            Exit Sub
        End If
        If CheckParentTopicPeriod() = False Then
            lblError.Text = "Period must be within MainTopic period"
            Exit Sub
        End If
        If ViewState("datamode") = "add" Then
            Session("gDtlDataMode") = "Add"
            If Session("gDtlDataMode") = "Add" Then
                ldrNew = Session("SyllabusDet").NewRow
                Session("gintGridLine") = Session("gintGridLine") + 1

                ldrNew("ID") = Session("gintGridLine")
                ldrNew("SUBID") = h_SyllabusId.Value
                ldrNew("SYBDESC") = txtSubTopic.Text
                ldrNew("SYBPARENTDESC") = txtParentTopic.Text
                ldrNew("SYBPARENTID") = IIf(h_ParentTopicID.Value = "", 0, h_ParentTopicID.Value)
                ldrNew("SYBSTARTDT") = txtFromDate.Text
                ldrNew("SYBENDDT") = txtToDate.Text
                ldrNew("SYBTOTHRS") = txtHrs.Text
                ldrNew("SYBOBJECTIVE") = txtobjective.Text
                ldrNew("SYBORDER") = txtOrder.Text
                ldrNew("SydId") = 0
                ldrNew("SYBRESOURCE") = txtResource.Text
                Session("SyllabusDet").Rows.Add(ldrNew)
            End If
        End If
        If ViewState("datamode") = "edit" Then
            If Not Session("SyllabusDet") Is Nothing Then
                For lintIndex = 0 To Session("SyllabusDet").Rows.Count - 1
                    If (Session("SyllabusDet").Rows(lintIndex)("Id") = Session("gintGridLine")) Then
                        Session("SyllabusDet").Rows(lintIndex)("SYBPARENTDESC") = Trim(txtParentTopic.Text)
                        Session("SyllabusDet").Rows(lintIndex)("SYBDESC") = Trim(txtSubTopic.Text)
                        Session("SyllabusDet").Rows(lintIndex)("SYBSTARTDT") = Trim(txtFromDate.Text)
                        Session("SyllabusDet").Rows(lintIndex)("SYBENDDT") = Trim(txtToDate.Text)
                        Session("SyllabusDet").Rows(lintIndex)("SYBTOTHRS") = Trim(txtHrs.Text)
                        Session("SyllabusDet").Rows(lintIndex)("SYBOBJECTIVE") = Trim(txtobjective.Text)
                        Session("SyllabusDet").Rows(lintIndex)("SYBORDER") = Trim(txtOrder.Text)
                        Session("SyllabusDet").Rows(lintIndex)("SYBRESOURCE") = Trim(txtResource.Text)
                    End If
                Next
            End If
        End If
        GridBind()
        txtSubTopic.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        txtHrs.Text = ""
        txtobjective.Text = ""
        txtResource.Text = ""
        ViewState("datamode") = "add"
        BindNextOrder()
        'ClearPartialDetails()
    End Sub
    Private Function GridBind()
        Dim i As Integer
        Dim dtTempDtl As New DataTable
        dtTempDtl = SyllabusMaster.CreateDTSyllabusDetails()
        If Session("SyllabusDet").Rows.Count > 0 Then
            For i = 0 To Session("SyllabusDet").Rows.Count - 1
                Dim ldrTempNew As DataRow
                ldrTempNew = dtTempDtl.NewRow
                For j As Integer = 0 To Session("SyllabusDet").Columns.Count - 1
                    ldrTempNew.Item(j) = Session("SyllabusDet").Rows(i)(j)
                Next
                dtTempDtl.Rows.Add(ldrTempNew)
            Next
        End If
        gvSyllabus.DataSource = dtTempDtl
        gvSyllabus.DataBind()
        gvSyllabus.Columns(1).Visible = False
        gvSyllabus.Columns(8).Visible = False
    End Function

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Sub ClearPartialDetails()
        ClearControls()
    End Sub
    Sub SaveSyllabus()
        Dim iReturnvalue As Integer
        Dim iIndex As Integer

        'If ((ViewState("datamode") = "edit") And (h_SYD_ID.Value = "0")) Then Exit Sub

        Dim cmd As New SqlCommand
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try


            'For iIndex = 0 To Session("SyllabusDET").Rows.Count - 1
            '    If CheckDataExists(iIndex, objConn, stTrans) = False Then
            '        cmd = New SqlCommand("[SYL].[SaveSYLLABUS_D]", objConn, stTrans)
            '        cmd.CommandType = CommandType.StoredProcedure
            '        If ViewState("datamode") = "add" Then
            '            cmd.Parameters.AddWithValue("@SYD_ID", 0)
            '        End If
            '        If ViewState("datamode") = "edit" Then
            '            cmd.Parameters.AddWithValue("@SYD_ID", h_SYD_ID.Value)
            '        End If
            '        cmd.Parameters.AddWithValue("@SYD_SYM_ID", h_SyllabusId.Value)
            '        cmd.Parameters.AddWithValue("@SYD_DESCR", Session("SyllabusDet").Rows(iIndex)("SYBDESC"))
            '        cmd.Parameters.AddWithValue("@SYD_PARENT_ID", Session("SyllabusDet").Rows(iIndex)("SYBPARENTID"))
            '        cmd.Parameters.AddWithValue("@SYD_STDT", Session("SyllabusDet").Rows(iIndex)("SYBSTARTDT"))
            '        cmd.Parameters.AddWithValue("@SYD_ENDDT", Session("SyllabusDet").Rows(iIndex)("SYBENDDT"))
            '        cmd.Parameters.AddWithValue("@SYD_TOT_HRS", Session("SyllabusDet").Rows(iIndex)("SYBTOTHRS"))
            '        If ViewState("datamode") = "add" Then
            '            cmd.Parameters.AddWithValue("@bEdit", 0)
            '        End If
            '        If ViewState("datamode") = "edit" Then
            '            cmd.Parameters.AddWithValue("@bEdit", 1)
            '        End If


            'ldrNew("SUBID") = h_SyllabusId.Value
            'ldrNew("SYBDESC") = txtSubTopic.Text
            'ldrNew("SYBPARENTDESC") = txtParentTopic.Text
            'ldrNew("SYBPARENTID") = IIf(h_ParentTopicID.Value = "", 0, h_ParentTopicID.Value)
            'ldrNew("SYBSTARTDT") = txtFromDate.Text
            'ldrNew("SYBENDDT") = txtToDate.Text
            'ldrNew("SYBTOTHRS") = txtHrs.Text
            'ldrNew("SYBOBJECTIVE") = txtobjective.Text
            'ldrNew("SYBORDER") = txtOrder.Text
            'ldrNew("SydId") = 0
            'ldrNew("SYBRESOURCE") = txtResource.Text


            ''For iIndex = 0 To gvSyllabus.Rows.Count - 1
            ''    Dim lblSubTopic As New Label
            ''    Dim lblParentID As New Label
            ''    Dim lblStartDate As New Label
            ''    Dim lblEndDate As New Label
            ''    Dim lblTotalHrs As New Label
            ''    Dim lblObjective As Label
            ''    Dim lblResource As Label
            ''    Dim lblSydId As Label
            ''    Dim lblOrder As Label
            ''    lblSubTopic.Text = ""
            ''    lblParentID.Text = ""
            ''    lblStartDate.Text = ""
            ''    lblEndDate.Text = ""
            ''    lblTotalHrs.Text = ""

            ''    lblSubTopic = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblSubTopic"), Label)
            ''    lblParentID = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblParentID"), Label)
            ''    lblStartDate = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblStartDate"), Label)
            ''    lblEndDate = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblEndDate"), Label)
            ''    lblTotalHrs = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblTotalHrs"), Label)
            ''    lblSydId = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblSydId"), Label)
            ''    lblObjective = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblObjective"), Label)
            ''    lblOrder = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblOrder"), Label)
            ''    lblResource = TryCast(gvSyllabus.Rows(iIndex).FindControl("lblResource"), Label)

            'CheckDataExists(lblParentID, lblSubTopic, lblStartDate, lblEndDate, objConn, stTrans)
            cmd = New SqlCommand("[SYL].[SaveSYLLABUS_D]", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@SYD_ID", IIf(h_SYD_ID.Value = "", 0, h_SYD_ID.Value))

            cmd.Parameters.AddWithValue("@SYD_SYM_ID", h_SyllabusId.Value)
            cmd.Parameters.AddWithValue("@SYD_DESCR", txtSubTopic.Text)
            cmd.Parameters.AddWithValue("@SYD_PARENT_ID", IIf(h_ParentTopicID.Value = "", 0, h_ParentTopicID.Value))
            cmd.Parameters.AddWithValue("@SYD_STDT", txtFromDate.Text)
            cmd.Parameters.AddWithValue("@SYD_ENDDT", txtToDate.Text)
            cmd.Parameters.AddWithValue("@SYD_TOT_HRS", txtHrs.Text)
            cmd.Parameters.AddWithValue("@SYD_OBJECTIVE", txtobjective.Text)
            cmd.Parameters.AddWithValue("@SYD_ORDER", txtOrder.Text)
            cmd.Parameters.AddWithValue("@SYD_SBG_ID", h_SUBJID.Value)
            cmd.Parameters.AddWithValue("@SYD_RESOURCE", txtResource.Text)
            cmd.Parameters.AddWithValue("@DOC_IDs", h_doc_IDs.Value.Replace("___", "|"))

            If ViewState("datamode") = "add" Then
                cmd.Parameters.AddWithValue("@bEdit", 0)
            End If

            If ViewState("datamode") = "edit" Then
                cmd.Parameters.AddWithValue("@bEdit", 1)
            End If

            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
            stTrans.Commit()
            lblError.Text = "Topic's Successfully Saved"
            objConn.Close()

            h_doc_IDs.Value = ""
            grdDoc.DataSource = Nothing
            grdDoc.DataBind()
            ClearPartialDetails()
            GetSyllabusDetails()
            chk = 0
            btnSave.Text = "Save"
            ''Next
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = "Unexpected error"
            objConn.Close()
            Exit Sub
        End Try
        'If iReturnvalue <> 0 Then
        '    stTrans.Rollback()
        '    lblError.Text = "Unexpected error"
        '    objConn.Close()
        '    Exit Sub
        'End If




    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vSyllabus_M As New SyllabusMaster
        Dim strSql As String
        Dim strSubQuery As String
        Dim strSub As String
        Dim strMain As String
        Dim dsMain As DataSet
        Dim dsSub As DataSet
        Dim ds As DataSet
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            'If Not Session("SyllabusDet") Is Nothing Then
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim lblId As New Label
            Try
                lblId = TryCast(sender.parent.FindControl("lblSydId"), Label)
                If Not lblId Is Nothing Then
                    strSql = "SELECT SYD_ID FROM SYL.SYLLABUS_D WHERE SYD_PARENT_ID='" & lblId.Text & "'"
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
                    If ds.Tables(0).Rows.Count > 0 Then
                        lblError.Text = "Can't Delete,SubTopics Exists"
                        stTrans.Rollback()
                        objConn.Close()
                        Exit Sub
                    Else
                        strSub = "SELECT SYL_ID FROM SYL.SYLLABUS_TEACHER_D WHERE SYL_SYD_ID='" & lblId.Text & "'"
                        dsSub = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSub)
                        If dsSub.Tables(0).Rows.Count > 0 Then
                            lblError.Text = "Can't Delete,SubTopic Allocated"
                            stTrans.Rollback()
                            objConn.Close()
                            Exit Sub
                        End If
                        strMain = "SELECT SYT_ID FROM SYL.SYLLABUS_TEACHER_M WHERE SYT_SYD_ID='" & lblId.Text & "'"
                        dsMain = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strMain)
                        If dsMain.Tables(0).Rows.Count > 0 Then
                            lblError.Text = "Can't Delete,MainTopic Allocated"
                            stTrans.Rollback()
                            objConn.Close()
                            Exit Sub
                        End If
                        strSubQuery = "DELETE FROM SYL.SYLLABUS_D WHERE SYD_ID='" & lblId.Text & "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSubQuery)
                    End If
                End If
                stTrans.Commit()
                lblError.Text = "Successfully Deleted"
            Catch ex As Exception
                stTrans.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            Finally
                objConn.Close()
            End Try
            GetSyllabusDetails()
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
            'End If
    End Sub
    Sub PopulateParentSyllabus()
        Dim ds As New DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT     SYL.SYLLABUS_M.SYM_ACD_ID as AcdId, VW_ACADEMICYEAR_M.ACY_DESCR as AcdYear, SYL.SYLLABUS_M.SYM_TRM_ID as TrmId, VW_TRM_M.TRM_DESCRIPTION as Term, " _
                            & " SYL.SYLLABUS_M.SYM_GRD_ID as GrdId, VW_GRADE_M.GRD_DISPLAY as Grade, SYL.SYLLABUS_M.SYM_SBG_ID as SubjId, SUBJECTS_GRADE_S.SBG_DESCR as Subject, " _
                            & " SYL.SYLLABUS_M.SYM_DESCR as Syllabus,SYL.SYLLABUS_M.SYM_ID as SylId " _
                            & " FROM SYL.SYLLABUS_M INNER JOIN " _
                            & " VW_BUSINESSUNIT_M ON SYL.SYLLABUS_M.SYM_BSU_ID = VW_BUSINESSUNIT_M.BSU_ID INNER JOIN " _
                            & " vw_ACADEMICYEAR_D ON SYL.SYLLABUS_M.SYM_ACD_ID = vw_ACADEMICYEAR_D.ACD_ID INNER JOIN " _
                            & " VW_ACADEMICYEAR_M ON vw_ACADEMICYEAR_D.ACD_ACY_ID = VW_ACADEMICYEAR_M.ACY_ID INNER JOIN " _
                            & " VW_TRM_M ON SYL.SYLLABUS_M.SYM_TRM_ID = VW_TRM_M.TRM_ID INNER JOIN " _
                            & " VW_GRADE_M ON SYL.SYLLABUS_M.SYM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " _
                            & " SUBJECTS_GRADE_S ON SYL.SYLLABUS_M.SYM_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " _
                            & " WHERE (SYL.SYLLABUS_M.SYM_ACD_ID = '" & h_ACCID.Value & "') AND (SYL.SYLLABUS_M.SYM_TRM_ID = '" & h_TERMID.Value & "') AND (SYL.SYLLABUS_M.SYM_BSU_ID='" & Session("SBsuid") & "') and (SYL.SYLLABUS_M.SYM_SBG_ID='" & h_SUBJID.Value & "')"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count >= 1 Then
            txtAccYear.Text = ds.Tables(0).Rows(0)("AcdYear")
            txtTerm.Text = ds.Tables(0).Rows(0)("Term")
            txtGrade.Text = ds.Tables(0).Rows(0)("Grade")
            txtSubject.Text = ds.Tables(0).Rows(0)("Subject")
            'txtsy = ds.Tables(0).Rows(i)("Syllabus")
        End If

    End Sub

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearControls()
    End Sub

    Sub GetSyllabusDetails()
        Dim strSql As String
        Dim ds As DataSet
        Dim i As Integer
        Dim ldrNew As DataRow
        Try
            Session("SyllabusDet") = SyllabusMaster.CreateDTSyllabusDetails()
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            strSql = "SELECT     SYL.SYLLABUS_D.SYD_ID AS SydId,SYL.SYLLABUS_D.SYD_DESCR AS SYBDESC, SYL.SYLLABUS_D.SYD_PARENT_ID AS SYBPARENTID, SYLLABUS_D_1.SYD_DESCR AS SYBPARENTDESC, SYL.SYLLABUS_D.SYD_STDT AS SYBSTARTDT, " _
                    & " SYL.SYLLABUS_D.SYD_ENDDT AS SYBENDDT, SYL.SYLLABUS_D.SYD_TOT_HRS AS SYBTOTHRS, ISNULL(SYL.SYLLABUS_D.SYD_OBJECTIVE,'') SYBOBJECTIVE,ISNULL(SYL.SYLLABUS_D.SYD_ORDER,0) SYBORDER,ISNULL(SYL.SYLLABUS_D.SYD_RESOURCE,'') SYBRESOURCE " _
                    & " FROM SYL.SYLLABUS_D LEFT OUTER JOIN " _
                    & " SYL.SYLLABUS_D AS SYLLABUS_D_1 ON  " _
                    & " SYL.SYLLABUS_D.SYD_PARENT_ID = SYLLABUS_D_1.SYD_ID " _
                    & " WHERE SYL.SYLLABUS_D.SYD_SYM_ID='" & h_SyllabusId.Value & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    ldrNew = Session("SyllabusDet").NewRow
                    Session("gintGridLine") = Session("gintGridLine") + 1
                    ldrNew("ID") = Session("gintGridLine")
                    'h_NextLine.Value = Session("gintGridLine")
                    ldrNew("SUBID") = h_SUBJID.Value
                    ldrNew("SYBPARENTDESC") = ds.Tables(0).Rows(i)("SYBPARENTDESC")
                    ldrNew("SYBDESC") = ds.Tables(0).Rows(i)("SYBDESC")
                    ldrNew("SYBPARENTID") = ds.Tables(0).Rows(i)("SYBPARENTID")
                    ldrNew("SYBSTARTDT") = Format(ds.Tables(0).Rows(i)("SYBSTARTDT"), "dd/MMM/yyyy")
                    ldrNew("SYBENDDT") = Format(ds.Tables(0).Rows(i)("SYBENDDT"), "dd/MMM/yyyy")
                    ldrNew("SYBTOTHRS") = ds.Tables(0).Rows(i)("SYBTOTHRS")
                    ldrNew("SYBOBJECTIVE") = ds.Tables(0).Rows(i)("SYBOBJECTIVE")
                    ldrNew("SYBORDER") = ds.Tables(0).Rows(i)("SYBORDER")
                    ldrNew("SydId") = ds.Tables(0).Rows(i)("SydId")
                    ldrNew("SYBRESOURCE") = ds.Tables(0).Rows(i)("SYBRESOURCE")
                    'ldrNew("SYBTOTHRS") = ds.Tables(0).Rows(i)("Subject")
                    'ldrNew("Syllabus") = ds.Tables(0).Rows(i)("Syllabus")
                    'ldrNew("SylId") = ds.Tables(0).Rows(i)("SylId")
                    'ldrNew("GUID") = System.DBNull.Value
                    Session("SyllabusDet").Rows.Add(ldrNew)
                Next
                GridBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As Label
        Dim lintIndex As Integer
        lblRowId = TryCast(sender.FindControl("lblID"), Label)
        Session("gintGridLine") = lblRowId.Text
        For lintIndex = 0 To Session("SyllabusDet").Rows.Count - 1
            If (Session("SyllabusDet").Rows(lintIndex)("Id") = Session("gintGridLine")) Then
                txtParentTopic.Text = IIf(IsDBNull(Session("SyllabusDet").Rows(lintIndex)("SYBPARENTDESC")), "", Session("SyllabusDet").Rows(lintIndex)("SYBPARENTDESC"))
                txtSubTopic.Text = Trim(Session("SyllabusDet").Rows(lintIndex)("SYBDESC"))
                txtFromDate.Text = Trim(Session("SyllabusDet").Rows(lintIndex)("SYBSTARTDT"))
                txtToDate.Text = Trim(Session("SyllabusDet").Rows(lintIndex)("SYBENDDT"))
                txtHrs.Text = Trim(Session("SyllabusDet").Rows(lintIndex)("SYBTOTHRS"))
                txtobjective.Text = Trim(Session("SyllabusDet").Rows(lintIndex)("SYBOBJECTIVE"))
                txtOrder.Text = Trim(Session("SyllabusDet").Rows(lintIndex)("SYBORDER"))
                h_SYD_ID.Value = Trim(Session("SyllabusDet").Rows(lintIndex)("SydId"))
                txtResource.Text = Trim(Session("SyllabusDet").Rows(lintIndex)("SYBRESOURCE"))
                display_docs(h_SYD_ID.Value)
            End If
        Next
        btnSave.Text = "Update"
        ViewState("datamode") = "edit"
        chk = 1
    End Sub

    Sub display_docs(ByVal SYD_ID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""

            str_query = "Select distinct ST2.SDD_SYD_ID, " _
                        & " substring( " _
                        & " cast(( " _
                        & " Select '___'+cast(ST1.SDD_DOC_ID as varchar(500))   AS [text()] " _
                        & " From SYL.SYLLABUS_D_DOC ST1 " _
                        & " Where(ST1.SDD_SYD_ID = ST2.SDD_SYD_ID) " _
                        & "  ORDER BY ST1.SDD_SYD_ID " _
                        & " For XML PATH ('') " _
                        & "  )as varchar(500)), 4, 1000) [DOC_IDs] " _
                        & " From SYL.SYLLABUS_D_DOC ST2 where SDD_SYD_ID=" & SYD_ID

            If str_query <> "" Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count >= 1 Then

                    h_doc_IDs.Value = ds.Tables(0).Rows(0).Item("DOC_IDs")
                    If h_doc_IDs.Value <> "" Then
                        GridBindDocs(h_doc_IDs.Value)
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Function CheckDataExists(ByVal lblParentId As Label, ByVal lblDesc As Label, ByVal lblFrom As Label, ByVal lblTo As Label, ByVal ObjConn As SqlConnection, ByVal sttrans As SqlTransaction)
        Dim strQuery As String
        Dim dr As SqlDataReader
        Dim cmdSub As SqlCommand
        Dim iReturnvalue As Integer

        Try
            'If Not Session("SyllabusDet") Is Nothing Then
            'cmdSub = New SqlCommand("[SYL].[GETSYLLABUSDETAILDATA]", ObjConn, sttrans)
            'cmdSub.CommandType = CommandType.StoredProcedure
            'cmdSub.Parameters.AddWithValue("@SYD_SYM_ID", h_SyllabusId.Value)
            'cmdSub.Parameters.AddWithValue("@SYD_PARENT_ID", Session("SyllabusDet").Rows(iIndex)("SYBPARENTID"))
            'cmdSub.Parameters.AddWithValue("@SYD_DESCR", Session("SyllabusDet").Rows(iIndex)("SYBDESC"))
            'cmdSub.Parameters.AddWithValue("@SYD_FROMDT", Session("SyllabusDet").Rows(iIndex)("SYBSTARTDT"))
            'cmdSub.Parameters.AddWithValue("@SYD_TODT", Session("SyllabusDet").Rows(iIndex)("SYBENDDT"))
            'cmdSub.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            'cmdSub.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            'cmdSub.ExecuteNonQuery()
            'iReturnvalue = CInt(cmdSub.Parameters("@ReturnValue").Value)
            'If iReturnvalue <> 0 Then
            '    CheckDataExists = True
            'Else
            '    CheckDataExists = False
            '    ViewState("datamode") = "add"
            'End If
            'End If
            cmdSub = New SqlCommand("[SYL].[GETSYLLABUSDETAILDATA]", ObjConn, sttrans)
            cmdSub.CommandType = CommandType.StoredProcedure
            cmdSub.Parameters.AddWithValue("@SYD_SYM_ID", h_SyllabusId.Value)
            cmdSub.Parameters.AddWithValue("@SYD_PARENT_ID", lblParentId.Text)
            cmdSub.Parameters.AddWithValue("@SYD_DESCR", lblDesc.Text)
            cmdSub.Parameters.AddWithValue("@SYD_FROMDT", lblFrom.Text)
            cmdSub.Parameters.AddWithValue("@SYD_TODT", lblTo.Text)
            cmdSub.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmdSub.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmdSub.ExecuteNonQuery()
            iReturnvalue = CInt(cmdSub.Parameters("@ReturnValue").Value)
            If iReturnvalue <> 0 Then
                'CheckDataExists = True
                ViewState("datamode") = "edit"
            Else
                'CheckDataExists = False
                ViewState("datamode") = "add"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Function
    Private Function CheckParentTopicPeriod() As Boolean
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        If txtParentTopic.Text <> "" Then
            If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
                strSql = "SELECT SYD_STDT,SYD_ENDDT FROM SYL.SYLLABUS_D WHERE SYD_ID='" & h_ParentTopicID.Value & "' and " _
                          & " SYD_SYM_ID='" & h_SyllabusId.Value & "' and SYD_PARENT_ID=0"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    If (Convert.ToDateTime(txtFromDate.Text) < ds.Tables(0).Rows(0)("SYD_STDT")) Or (Convert.ToDateTime(txtToDate.Text) > ds.Tables(0).Rows(0)("SYD_ENDDT")) Then
                        CheckParentTopicPeriod = False
                    Else
                        CheckParentTopicPeriod = True
                    End If
                Else
                    CheckParentTopicPeriod = True
                End If
            End If
        Else
            CheckParentTopicPeriod = True
        End If
    End Function

    Protected Sub gvSyllabus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            gvSyllabus.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        If h_doc_IDs.Value <> "" Then
            GridBindDocs(h_doc_IDs.Value)
        End If
    End Sub

    Protected Sub grdDoc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdDoc.PageIndex = e.NewPageIndex
        GridBindDocs(h_doc_IDs.Value)
    End Sub
    Private Sub GridBindDocs(ByVal vSTU_IDs As String)
        grdDoc.DataSource = GetSelectedDoc(vSTU_IDs)
        grdDoc.DataBind()
    End Sub
    Function GetSelectedDoc(Optional ByVal vSTU_IDs As String = "")
        Dim str_sql As String = "select SD_ID as ID,SD_DESCR DESCR,SD_FILENAME FNAME from SYLLABUS_DOC"
        If vSTU_IDs <> "" Then
            str_sql += " where SD_ID  IN ('" & vSTU_IDs.Replace("___", "','") & "')"
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_DOCSConnectionString, CommandType.Text, str_sql)
    End Function

    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select sd_fileName, sd_Content, sd_doc from SYLLABUS_DOC where sd_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub
    Public Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConnectionManger.GetOASIS_DOCSConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("sd_doc"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("sd_filename").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("sd_Content").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub grdDoc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdDoc.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtfile As LinkButton = e.Row.FindControl("txtfile")
                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(txtfile)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub grdDoc_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdDoc.RowDeleting
        Try
            grdDoc.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = grdDoc.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lbstu_no"), Label)

            If lblID.Text <> "" Then
                h_doc_IDs.Value = Replace(h_doc_IDs.Value, lblID.Text, "")
                If h_doc_IDs.Value <> "" Then
                    GridBindDocs(h_doc_IDs.Value)
                End If
                If (h_SYD_ID.Value <> "") Then
                    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                    Dim str_query As String = ""

                    str_query = "DELETE FROM SYL.SYLLABUS_D_DOC where SDD_SYD_ID=" + h_SYD_ID.Value + " and SDD_DOC_ID=" & lblID.Text
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End If
            GridBindDocs(h_doc_IDs.Value)
            GridBind()
        Catch ex As Exception

        End Try
    End Sub
End Class

