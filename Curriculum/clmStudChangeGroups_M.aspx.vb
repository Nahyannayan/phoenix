Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Curriculum_clmStudChangeGroups_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100180") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    lblName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    lblStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))

                    GridBind()

                    gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub ddlCGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlCGroup As DropDownList
        Dim lblTeacher As Label

        ddlCGroup = TryCast(sender.FindControl("ddlCGroup"), DropDownList)
        lblTeacher = TryCast(sender.FindControl("lblTeacher"), Label)



        If ddlCGroup.SelectedValue <> "" Then
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT isnull(STUFF((SELECT ','+ ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') " _
                                    & " FROM EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S AS L ON K.EMP_ID=L.SGS_EMP_ID AND L.SGS_TODATE is null" _
                                    & " WHERE SGS_SGR_ID = " + ddlCGroup.SelectedValue.ToString + " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,''),'')"
            Dim teacher As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            lblTeacher.Text = "Teacher : " + teacher
        Else
            lblTeacher.Text = ""
        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR " _
                            & " ,SGR_ID,SGR_DESCR FROM " _
                            & " SUBJECTS_GRADE_S AS A " _
                            & " INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                            & " INNER JOIN STUDENT_GROUPS_S AS C ON B.SGR_ID=C.SSD_SGR_ID" _
                            & " WHERE SSD_STU_ID=" + hfSTU_ID.Value + " AND SSD_ACD_ID=" + hfACD_ID.Value _
                            & " AND SBG_GRD_ID='" + hfGRD_ID.Value + "'"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGroup.DataSource = ds
        gvGroup.DataBind()
    End Sub

 
    Sub BindChangGroups(ByVal sbgId As String, ByVal sgrId As String, ByVal ddlCGroup As DropDownList)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M WHERE SGR_SBG_ID=" + sbgId _
                                 & " AND SGR_ID<>" + sgrId
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCGroup.DataSource = ds
        ddlCGroup.DataTextField = "SGR_DESCR"
        ddlCGroup.DataValueField = "SGR_ID"
        ddlCGroup.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = ""
        ddlCGroup.Items.Insert(0, li)

    End Sub
 

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim ddlCGroup As DropDownList
        Dim lblSbgId As Label
        Dim lblSgrId As Label
        For i = 0 To gvGroup.Rows.Count - 1
            With gvGroup.Rows(i)
                ddlCGroup = .FindControl("ddlCGroup")
                lblSbgId = .FindControl("lblSbgId")
                lblSgrId = .FindControl("lblSgrId")
            End With

            If ddlCGroup.SelectedValue <> "" Then
                str_query = "exec saveSTUDCHANGEGROUP " _
                           & hfSTU_ID.Value + "," _
                           & lblSgrId.Text + "," _
                           & ddlCGroup.SelectedValue.ToString + "," _
                           & lblSbgId.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If

        Next

        lblError.Text = "Record Saved SuccessFully"

    End Sub
#End Region

        Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub


    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblSbgId As Label
            Dim lblSgrId As Label
            Dim ddlCGroup As DropDownList

            lblSbgId = e.Row.FindControl("lblSbgId")
            lblSgrId = e.Row.FindControl("lblSgrId")
            ddlCGroup = e.Row.FindControl("ddlCGroup")
            BindChangGroups(lblSbgId.Text, lblSgrId.Text, ddlCGroup)
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()

    End Sub
End Class
