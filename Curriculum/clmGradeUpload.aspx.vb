Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Telerik.Web.UI
Imports System.IO
Imports System.IO.Compression


Partial Class Curriculum_clmGradeUpload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330375") Then
                    'If 1 = 2 Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights 
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindPrintedFor()
                    BindTerm()
                    BindGrade()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If


        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID=" + RSM_ID _
                                 & " ORDER BY RPF_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub

    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "TERM FINAL"
        li.Value = "0"
        ddlTerm.Items.Add(li)
    End Sub
    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim RSM_ID As String = String.Empty
        If ddlReportCard.SelectedIndex <> -1 Then
            RSM_ID = ddlReportCard.SelectedValue.ToString
        Else
            RSM_ID = "0"
        End If


        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String



        str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,RSG_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                                & " INNER JOIN RPT.REPORTSETUP_GRADE_S AS C ON A.GRM_GRD_ID=C.RSG_GRD_ID" _
                                & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND RSG_RSM_ID=" + RSM_ID

        If ViewState("GRD_ACCESS") > 0 Then
            str_query += " AND RSG_GRD_ID IN(select DISTINCT SCT_GRD_ID FROM OASIS..SECTION_M " _
                     & " WHERE SCT_ID IN(SELECT  ID  FROM  oasisfin.dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  oasis..GRADE_SECTION_ACCESS  " _
                     & " WHERE GSA_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " and (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|')))"
        End If


        str_query += " ORDER BY RSG_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = ""
        ddlGrade.Items.Insert(0, li)

    End Sub


#End Region


    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindGrade()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindPrintedFor()
        BindTerm()
        BindGrade()
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function ValidateFiles(ByVal stu_nos As String, ByVal acd_id As String, ByVal grm_grd_id As String) As String
        Dim returnstring As String = ""
        If HttpContext.Current.Session("sUsr_name") Is Nothing Then
            returnstring = "1" '1=Session has been expired
            Return returnstring
            Exit Function
        End If

        Try
            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@STU_NO", stu_nos)
            param(1) = New SqlParameter("@ACD_ID", acd_id)
            param(2) = New SqlParameter("@GRM_GRD_ID", grm_grd_id)
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_query As String = "[dbo].[VALIDATE_STU_NO_UPLOAD]"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_query, param)
            If ds.Tables(0).Rows.Count > 0 Then
                returnstring = ds.Tables(0).Rows(0)("STU_NOS").ToString
            End If

        Catch ex As Exception
            returnstring = ex.Message
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return returnstring
    End Function

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs)
        Dim serverpath As String = WebConfigurationManager.AppSettings("STU_REPORTCARDS").ToString
        Dim filesPath As String = serverpath + "/" + Session("sBsuid") + "/" + ddlPrintedFor.SelectedValue
        Directory.CreateDirectory(filesPath)
        Dim filesNames As String = String.Empty
        If zipFileUpload.HasFile Then
            'Dim archive As ZipArchive = ZipFile.OpenRead(zipFileUpload.PostedFile.)
            Dim archive As New ZipArchive(zipFileUpload.PostedFile.InputStream)
            For Each entry As ZipArchiveEntry In archive.Entries
                If entry.Name <> "" Then
                    If System.IO.File.Exists(filesPath + "/" + entry.Name) = True Then
                        System.IO.File.Delete(filesPath + "/" + entry.Name)
                    End If
                    entry.ExtractToFile(Path.Combine(filesPath + "/", entry.Name))
                    filesNames = filesNames + "," + entry.Name.Split(".")(0)
                End If
            Next

        Else
            For Each items As UploadedFile In AsyncUpload1.UploadedFiles
                If System.IO.File.Exists(filesPath + "/" + items.FileName) = True Then
                    System.IO.File.Delete(filesPath + "/" + items.FileName)
                End If
                items.SaveAs(filesPath + "/" + items.FileName)
                filesNames = filesNames + "," + items.FileName.Split(".")(0)
            Next
        End If
        If (filesNames.Length > 1) Then
            filesNames = filesNames.Substring(1)
        End If
        SaveData(filesNames)
    End Sub

    Private Sub SaveData(ByVal stu_nos As String)
        Dim returnstring As String = ""
        Dim tran As SqlTransaction
        Using CONN As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_CURRICULUMConnectionString)
            CONN.Open()
            tran = CONN.BeginTransaction("SampleTransaction")
            Try
                Dim param(7) As SqlParameter
                param(0) = New SqlParameter("@STU_NO", stu_nos)
                param(1) = New SqlParameter("@USR_NAME", Session("sUsr_name"))
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

                param(2) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(2).Direction = ParameterDirection.ReturnValue
                param(3) = New SqlParameter("@RSM_ID", ddlReportCard.SelectedValue)
                param(4) = New SqlParameter("@RPF_ID", ddlPrintedFor.SelectedValue)
                param(5) = New SqlParameter("@TRM_ID", ddlTerm.SelectedValue)

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[dbo].[SAVE_REPORT_STUDENTS_PUBLISH]", param)
                Dim ReturnFlag As Integer = param(2).Value
                If ReturnFlag <> 0 Then
                    returnstring = "Error occured while processing info !!!"
                Else
                    ' ViewState("datamode") = "none"
                    returnstring = "0"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If returnstring = "0" Then
                    returnstring = "Data save successfully!"
                    tran.Commit()
                End If
                CONN.Close()
            End Try
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Scriptas", "alert('" + returnstring + "');", True)
        End Using
    End Sub
End Class
