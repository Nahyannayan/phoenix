<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmCommendation.aspx.vb" Inherits="Curriculum_clmCommendation" Title="Untitled Page" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <style>

        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}

.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>
    <script type="text/javascript">
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblHeader" runat="server" Text="Commendation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
                <table class="BlueTable" id="tblrule" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                   <%-- <tr>
                        <td valign="middle" class="subheader_img" colspan="16">
                            <asp:Label ID="lblHeader" runat="server" Text="Commendation"></asp:Label>
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" ><span class="field-label">Grade</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Certificate</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlType" runat="server" >
                            </asp:DropDownList>                           
                        </td>

                        <td align="left" colspan="2"> <asp:LinkButton ID="lnkCategory" runat="server" CssClass="field-label" >Create new</asp:LinkButton></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left" ><span class="field-label">Part</span>
                        </td>
                      
                        <td align="left" >
                            <asp:DropDownList ID="ddlPart" runat="server" AutoPostBack="True">
                                <asp:ListItem>Part 1</asp:ListItem>
                                <asp:ListItem>Part 2</asp:ListItem>
                                <asp:ListItem>Part 3</asp:ListItem>
                                <asp:ListItem>Part 4</asp:ListItem>
                                <asp:ListItem>Part 5</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" ><span class="field-label">Subject</span>
                        </td>
                     
                        <td align="left"  >
                          <%--  <asp:CheckBoxList ID="ddlSubject" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                >
                            </asp:CheckBoxList>--%>

                             <telerik:RadComboBox RenderMode="Lightweight" ID="ddlSubject" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"
                      Width="100%" >
                </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr id="Tr5" runat="server">
                        <td align="left" ><span class="field-label">Numbers</span>
                        </td>
                       
                        <td align="left" >
                            <asp:TextBox ID="txtNos" runat="server" />
                        </td>
                        <td align="left" ><span class="field-label">Grading</span>
                        </td>
                       
                        <td align="left" >
                            <asp:TextBox ID="txtGrading" runat="server" Style='text-transform: uppercase' />
                            <asp:Button ID="lnkType" runat="server" CssClass="button" Text="Save" />
                        </td>
                    </tr>

                    <tr id="Tr6" runat="server">
                        <td id="Td12" align="center" runat="server" colspan="4">
                            <asp:GridView ID="grdType" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="5" Width="100%">
                                <RowStyle  CssClass="griditem"/>
                                <Columns>
                                    <asp:TemplateField HeaderText="CMTID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="tyId" runat="server" Text='<%# bind("CT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Part">
                                        <ItemTemplate>
                                            <asp:Label ID="lblpart" runat="server"  Text='<%# Bind("CP_PART") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Subjects">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubject" runat="server"  Text='<%# Bind("CP_SBG_ID") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblName" runat="server" ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Certificate">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltype" runat="server" Text='<%# Bind("CT_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grading">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltype2" runat="server" Text='<%# Bind("CT_NOS") %>'> </asp:Label>
                                            <asp:Label ID="lbltype1" runat="server" Text='<%# Bind("CT_GRADE") %>'></asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:ButtonField CommandName="edit" Text="Edit" HeaderText="Edit">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                Text="Delete"></asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td runat="server" align="left"><span class="field-label">Copy to Groups</span>
                        </td>


                        <td runat="server" colspan="2">
                         <%--   <asp:TreeView ID="tvCopyGroup" runat="server" ShowCheckBoxes="All" ExpandDepth="0" onclick="client_OnTreeNodeChecked();">
                                <NodeStyle CssClass="treenode" />
                            </asp:TreeView>--%>

                                  <telerik:RadComboBox RenderMode="Lightweight" ID="ddlPrintedFor" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" AutoPostBack="true"   Width="75%">
                                  </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:Button ID="Button1" runat="server" CssClass="button" Text="Copy" /></td>

                    </tr>

                </table>
                <asp:HiddenField ID="H_CAT_ID" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="Panel1_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground"
                    PopupControlID="Panel1" TargetControlID="lnkCategory" DynamicServicePath="" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" Width="50%" CssClass="panel-cover">
                    <table width="100%" >
                        <tr >
                            <td align="left" colspan="2" >                               
                        <span class="field-label">Type Category</span>
                            </td>
                        </tr>
                        <tr id="trcatGrade" runat="server">
                            <td id="Td1" align="left" runat="server" colspan="2"></td>
                        </tr>
                        <tr>
                            <td align="left" >
                                <asp:Label ID="lblH" runat="server" Text="Type Name"  CssClass="field-label"  ></asp:Label>
                                <asp:Label ID="lbl2" runat="server"  Text="*" CssClass="text-danger font-small field-label" ></asp:Label>
                            </td>
                            <td align="left" >
                                <asp:TextBox ID="txtCategory" runat="server" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnSaveCategory" runat="server" CssClass="button" TabIndex="7" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CausesValidation="False" CssClass="button"
                                    TabIndex="8" Text="Close" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>
