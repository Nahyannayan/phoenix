﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmCASESkills_M.aspx.vb" Inherits="Curriculum_clmCASESkills_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Skills Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" valign="bottom"  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server"  width="100%">
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Select Skill</span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlSkill" runat="server" 
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave1" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gvSkill" runat="server" AllowPaging="false" AutoGenerateColumns="False" width="100%"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. 
                                 Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20" >
                                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSksId" runat="server" Text='<%# Bind("SKS_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Skill" HeaderStyle-Width="50%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSkill" runat="server"   Text='<%# Bind("SKS_DESCR") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Short Name" HeaderStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtShort" runat="server"  Text='<%# Bind("SKS_SHORTNAME") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Display Order" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtOrder" runat="server"  Text='<%# Bind("SKS_ORDER") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="index" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="10%">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

