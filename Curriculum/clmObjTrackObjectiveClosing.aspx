﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmObjTrackObjectiveClosing.aspx.vb" Inherits="Curriculum_clmObjTrackObjectiveClosing" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
             Objectives Closing
        </div>
       <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0" >
                    <tr>
                        <td align="left" valign="bottom" >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" align="center" cellpadding="10" cellspacing="0" width="100%">                               
                                <tr >
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblText" runat="server"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span> 
                                    </td>
                                  
                                    <td align="left" widt="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                     <td align="left" width="20%"><span class="field-label">Select Grade</span>
                                    </td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade"  runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>                                     
                                </tr> 
                                <tr>
                                     <td align="left" width="20%"><span class="field-label">Closing Date</span> 
                                     </td>
                                 
                                     <td align="left" width="30%">
                                        <asp:TextBox ID="txtDate" runat="server" >
                                        </asp:TextBox><asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>                             
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hf_PrevDate" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDate"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                    Display="None" ErrorMessage="Please enter the submit date" ValidationGroup="groupM1">
                </asp:RequiredFieldValidator>
                <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

