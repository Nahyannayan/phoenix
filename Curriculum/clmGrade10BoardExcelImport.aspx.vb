Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports GemBox.Spreadsheet
Partial Class Curriculum_clmGrade10BoardExcelImport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C330150") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindReportCard()
                BindPrintedFor()
                BindHeader()
                PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGenerateReport)

    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT RSM_DESCR,RSM_ID,RSM_DISPLAYORDER FROM RPT.REPORT_SETUP_M " _
        & " INNER JOIN RPT.REPORTSETUP_GRADE_S ON RSM_ID=RSG_RSM_ID AND RSG_GRD_ID='10'" _
        & "WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
        & " AND RSM_DESCR LIKE '%SUMMATIVE%'" _
        & " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT RPF_DESCR,RPF_ID FROM RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID='" + ddlReportCard.SelectedValue.ToString _
                                & "' ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlPrintedFor.DataSource = ds
        ddlPrintedFor.DataTextField = "RPF_DESCR"
        ddlPrintedFor.DataValueField = "RPF_ID"
        ddlPrintedFor.DataBind()
    End Sub
    
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                              & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                              & " INNER JOIN oasis_curriculum.RPT.REPORTSETUP_GRADE_S AS d ON A.GRM_GRD_ID=d.RSG_GRD_ID WHERE " _
                              & " grm_acd_id=" + acdid _
                              & " AND RSG_RSM_ID=" + ddlReportCard.SelectedValue.ToString _
                              & " order by grd_displayorder"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Sub BindHeader()
        ddlHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_query As String

        If rdSubject.Checked = True Then
            str_query = "SELECT RSD_HEADER,convert(VARCHAR(100),RSD_ID)+'|'+'SUBJECT' AS RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString _
                        & "' AND RSD_bDIRECTENTRY='FALSE' AND ISNULL(RSD_bFINALREPORT,'FALSE')='FALSE'" _
                        & " AND ISNULL(RSD_bALLSUBJECTS,'FALSE')='TRUE'" _
                        & "ORDER BY RSD_DISPLAYORDER"
        Else
            str_query = "SELECT RSD_HEADER,convert(VARCHAR(100),RSD_ID)+'|'+'MINORSUBJECT' AS RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString _
                    & "' AND RSD_bDIRECTENTRY='FALSE' AND ISNULL(RSD_bALLSUBJECTS,'FALSE')='FALSE'"
            '  ORDER BY RSD_DISPLAYORDER"

            str_query += " UNION ALL "

            str_query += "SELECT RSD_HEADER,convert(VARCHAR(100),RSD_ID)+'|'+'SKILL' AS RSD_ID FROM RPT.REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString _
                   & "' AND RSD_bDIRECTENTRY='TRUE' AND RSD_bHASCHILD='TRUE'"
            ' & " ORDER BY RSD_DISPLAYORDER "
        End If



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlHeader.DataSource = ds
        ddlHeader.DataTextField = "RSD_HEADER"
        ddlHeader.DataValueField = "RSD_ID"
        ddlHeader.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0|0"
        ddlHeader.Items.Insert(0, li)

    End Sub
    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SBG_GRD_ID='10' AND SBG_PARENT_ID=0 AND                                            SBG_bMAJOR=1"


        'str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dv As DataView = ds.Tables(0).DefaultView
        dv.Sort = "SBG_DESCR"
        ddlSubject.DataSource = dv
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Sub CallReport()

        Dim rsd As String() = ddlHeader.SelectedValue.Split("|")

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_query As String = "EXEC	 [dbo].[GRADE10BOARDEXCELIMPORT] " _
                                 & "@ACY_DESCR = '" + ddlAcademicYear.SelectedItem.Text + "'," _
                                 & "@ACD_ID = " + ddlAcademicYear.SelectedValue.ToString + "," _
                                 & "@SBG_ID = " + ddlSubject.SelectedValue.ToString + "," _
                                 & "@RSM_ID = " + ddlReportCard.SelectedValue.ToString + "," _
                                 & "@RSD_ID = " + rsd(0) + "," _
                                 & "@GRD_ID = '10'," _
                                 & "@RPF_ID = " + ddlPrintedFor.SelectedValue.ToString + "," _
                                 & "@RSD_HEADER = '" + ddlHeader.SelectedItem.Text + "'," _
                                 & "@RPF_DESCR = '" + ddlPrintedFor.SelectedItem.Text + "'," _
                                 & "@bTYPE = '" + rsd(1) + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dt As DataTable

        dt = ds.Tables(0)

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016


        'ws.InsertDataTable(dt, "A1", True)

        'ef.SaveXls(tempFileName)

        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ''  ws.HeadersFooters.AlignWithMargins = True
        ef.Save(tempFileName)


        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileName)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        System.IO.File.Delete(tempFileName)
    End Sub
  
#End Region

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        BindPrintedFor()
        BindHeader()

 

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        BindReportCard()
        BindPrintedFor()
        BindHeader()



    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub


    Protected Sub rdSkill_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdSkill.CheckedChanged
        If rdSkill.Checked = True Then
            BindHeader()
            trSubject.Visible = False
        End If
    End Sub

    Protected Sub rdSubject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdSubject.CheckedChanged
        If rdSubject.Checked = True Then
            BindHeader()
            trSubject.Visible = True
        End If
    End Sub
End Class
