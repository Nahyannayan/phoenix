﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Collections.Generic
Imports System.Collections
Partial Class Curriculum_clmObjtrackApptracker
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C320055") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Select Case Session("susr_name").ToString.ToLower
                        Case "dhanya", "charles", "charan"
                            If Session("sbsuid") = "125005" Then
                                hfEMP_ID.Value = "3805"
                            ElseIf Session("sbsuid") = "125010" Then
                                hfEMP_ID.Value = "21737"
                            ElseIf Session("sbsuid") = "114004" Then
                                hfEMP_ID.Value = "34548"
                            End If
                        Case Else
                            hfEMP_ID.Value = Session("EmployeeId")
                    End Select

                    'hfEMP_ID.Value = Session("EmployeeId") '"3201"
                    BindGrade()
                    BindAcy()
                    BindGroup()
                    ' trDetails.Visible = False

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            End Try

        Else
            If Session("Popup") = "1" Then
                Session("Popup") = "0"
                If Not (Session("APP_STU_ID") Is Nothing And Session("APP_STU_CURRENTLEVEL") Is Nothing) Then
                    UpdateCurrentLevel()
                    Session("APP_STU_ID") = Nothing
                    Session("APP_STU_CURRENTLEVEL") = Nothing
                End If
            End If

        End If



    End Sub

   
    Protected Sub btnGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSgrId As Label = TryCast(sender.findcontrol("lblSgrId"), Label)
        getSubject(lblSgrId.Text)
        hfSGR_ID.Value = lblSgrId.Text
        CheckAppSubject()
        BindStudents(lblSgrId.Text)
        Dim btnGroup As Button = DirectCast(sender.findcontrol("btnGroup"), Button)
        trDetails.Visible = True
        lblDetails.Text = "<font color=Purple size=2pt>Subject : " + hfSubject.Value + "&nbsp;&nbsp;&nbsp;Group : " + hfGroup.Value + "</font>"
        SetButtonBackGround(btnGroup.Text)
    End Sub

    Sub CheckAppSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(SBG_bAPP,'FALSE') FROM SUBJECTS_GRADE_S " _
                              & " INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID WHERE SGR_ID=" + hfSGR_ID.Value
        Dim bApp As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If bApp = True Then
            hfAPPSubject.Value = "1"
        Else
            hfAPPSubject.Value = "0"
        End If
    End Sub

    Sub SetButtonBackGround(ByVal group As String)
        Dim i As Integer
        Dim btnGroup As Button
        For i = 0 To dlGroups.Items.Count - 1
            btnGroup = dlGroups.Items(i).FindControl("btnGroup")
            If btnGroup.Text = group Then
                btnGroup.CssClass = "button"
            Else
                btnGroup.CssClass = "button"
            End If
        Next
    End Sub

    Function getPhoto(ByVal stu_photopath As String) As String
        Dim strImagePath As String
        If stu_photopath <> "" Then
            strImagePath = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString + stu_photopath
            'strImagePath = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto" + stu_photopath
        Else
            strImagePath = "~/Images/Photos/no_image.gif"
        End If
        Return strImagePath
        'imgStud.AlternateText = "No Image found"
    End Function

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub UpdateCurrentLevel()
        Dim i As Integer
        Dim lblStuId As Label
        Dim lblLevel As Label
        For i = 0 To dlStudents.Items.Count - 1
            lblStuId = dlStudents.Items(i).FindControl("lblStuId")
            If lblStuId.Text = Session("APP_STU_ID") Then
                lblLevel = dlStudents.Items(i).FindControl("lblLevel")
                lblLevel.Text = Session("APP_STU_CURRENTLEVEL")
            End If
        Next

    End Sub
    Sub BindAcy()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ACY_DESCR FROM VW_ACADEMICYEAR_M WHERE ACY_ID=" + Session("CURRENT_ACY_ID")
        hfACY_DESCR.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

     
    
        str_query = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM OASIS..GRADE_BSU_M AS A" _
                      & " INNER JOIN OASIS..GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                      & " INNER JOIN GROUPS_M AS C ON A.GRM_GRD_ID=C.SGR_GRD_ID AND A.GRM_ACD_ID=C.SGR_ACD_ID" _
                      & " INNER JOIN GROUPS_TEACHER_S AS D ON C.SGR_ID=D.SGS_SGR_ID" _
                      & " WHERE GRM_ACD_ID='" + Session("Current_ACD_ID") + "'" _
        & " AND SGS_EMP_ID=" + hfEMP_ID.Value _
        & " AND SGS_TODATE IS NULL" _
        & " AND GRD_ID IN('KG1','KG2','01','02','03','04','05','06','07','08') " _
        & " ORDER BY GRD_DISPLAYORDER"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
            & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
            & " AND SGR_ACD_ID='" + Session("CURRENT_ACD_ID") + "'" _
            & " AND SGS_EMP_ID=" + hfEMP_ID.Value

        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND SGR_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        End If
        str_query += "ORDER BY SGR_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlGroups.DataSource = ds
        dlGroups.DataBind()


        Dim lblSgrId As Label = dlGroups.Items(0).FindControl("lblSgrId")
        getSubject(lblSgrId.Text)
        Dim btnGroup As Button = dlGroups.Items(0).FindControl("btnGroup")
        hfSGR_ID.Value = lblSgrId.Text
        trDetails.Visible = True
        lblDetails.Text = "<font color=Purple size=2pt>Subject : " + hfSubject.Value + "&nbsp;&nbsp;&nbsp;Group : " + hfGroup.Value + "</font>"
        SetButtonBackGround(btnGroup.Text)
        CheckAppSubject()
        BindStudents(lblSgrId.Text)

    End Sub

    Sub BindStudents(ByVal sgr_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_LASTNAME,''))," _
                        & " ISNULL(STU_PHOTOPATH,'') AS STU_PHOTOPATH,ISNULL(SCL_CURRENTLEVEL,'') LEVEL,ISNULL(NCL_LVL_ID,0) LVL_ID " _
                        & " FROM STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID" _
                        & " LEFT OUTER JOIN OBJTRACK.STUDENT_CURRENTLEVEL_S ON STU_ID=SCL_STU_ID AND SCL_SBM_ID=" + hfSBM_ID.Value _
                        & " LEFT OUTER JOIN OBJTRACK.NCLEVEL_BSU_M ON STU_ACD_ID=NCL_ACD_ID AND SCL_CURRENTLEVEL=NCL_DESCR AND SCL_SBM_ID=NCL_SBM_ID" _
                        & " WHERE STU_CURRSTATUS='EN' AND SSD_SGR_ID=" + sgr_id _
                        & " ORDER BY STU_FIRSTNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlstudents.datasource = ds
        dlstudents.databind()

    End Sub

    Sub getSubject(ByVal sgr_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_SBM_ID,SBG_ID,SBG_GRD_ID,SBG_DESCR,SGR_DESCR FROM SUBJECTS_GRADE_S INNER JOIN " _
                               & " GROUPS_M ON SGR_SBG_ID=SBG_ID WHERE SGR_ID=" + sgr_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        hfSBG_ID.Value = ds.Tables(0).Rows(0).Item(1)
        hfSBM_ID.Value = ds.Tables(0).Rows(0).Item(0)
        hfGRD_ID.Value = ds.Tables(0).Rows(0).Item(2)
        hfSubject.Value = ds.Tables(0).Rows(0).Item(3)
        hfGroup.Value = ds.Tables(0).Rows(0).Item(4)
    End Sub


#End Region

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindGroup()
        dlStudents.DataBind()
    End Sub

    Protected Sub dlStudents_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlStudents.ItemDataBound
        '  If e.Item.ItemType = ListItemType.Item Then

        Dim lnkStudent As LinkButton = e.Item.FindControl("lnkStudent")
        Dim imgStud As Image = e.Item.FindControl("imgStud")
        Dim lblStuId As Label = e.Item.FindControl("lblStuId")
        Dim lblStuNo As Label = e.Item.FindControl("lblStuNo")
        Dim lvlId As Label = e.Item.FindControl("lvlId")
        Dim lblLevel As Label = e.Item.FindControl("lblLevel")
        '  If lnkStudent.Text.Contains("LINA") Then
        If hfAPPSubject.Value = "1" Then
            lblLevel.Visible = True
        Else
            lblLevel.Visible = False
        End If


        Dim strLink As String = "clmObjTrackStudentLevels_Popup.aspx?accyear=" + hfACY_DESCR.Value _
                 & "&grade=" + hfGRD_ID.Value _
                 & "&group=" + hfGroup.Value _
                 & "&subject=" + hfSubject.Value _
                 & "&name=" + lnkStudent.Text.Replace("'", " ") _
                 & "&stuno=" + lblStuNo.Text _
                 & "&sbmid=" + hfSBM_ID.Value _
                 & "&sbgid=" + hfSBG_ID.Value _
                 & "&grdid=" + hfGRD_ID.Value _
                 & "&acdid=" + Session("cURRENT_ACD_ID") _
                 & "&sgrid=" + hfSGR_ID.Value _
                 & "&clvl=" + lvlId.Text _
                 & "&page=report" _
                 & "&appsubject=" + hfAPPSubject.Value



        lnkStudent.Attributes.Add("onclick", "javascript:var popup = radopen('" + strLink + "','pop_up'); return false;")
        imgStud.Attributes.Add("onclick", "javascript:var popup = radopen('" + strLink + "','pop_up'); return false;")
        'End If
        ' End If
    End Sub

    Protected Sub dlGroups_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlGroups.ItemDataBound
        'If e.Item.ItemType = ListItemType.Item Then
        ' Dim btnGroup As Button = e.Item.FindControl("btnGroup")
        '   btnGroup.Attributes.Add("onclick", "background-image:url('../images/tabmenu/blank_click.jpg');")
        ' End If
    End Sub
End Class
