﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Curriculum_clmMentorMentee
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100855") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    ViewState("datamode") = "add"
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindTerm()

                    BindGrade(ddlMentorGrade)
                    BindSection(ddlMentorSection, ddlMentorGrade.SelectedValue.ToString)

                    BindGrade(ddlMenteeGrade)
                    BindSection(ddlMenteeSection, ddlMenteeGrade.SelectedValue.ToString)
                    updateMentorURL()
                    updateMenteeURL()
                    BindSubject()
                    rdTeacher.Checked = True
                    trStudent.Visible = False
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_aCD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " ORDER BY TRM_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
    End Sub

    Public Sub BindGrade(ByVal ddl As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT CASE WHEN GRM_STM_ID=1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END GRM_DISPLAY,GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),GRM_STM_ID) GRM_GRD_ID,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M" _
                               & " INNER JOIN VW_GRADE_M ON GRM_GRD_ID=GRD_ID INNER JOIN VW_STREAM_M ON GRM_STM_ID=STM_ID WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                               & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "GRM_DISPLAY"
        ddl.DataValueField = "GRM_GRD_ID"
        ddl.DataBind()
    End Sub

    Sub BindSection(ByVal ddl As DropDownList, ByVal GRD_ID As String)
        Dim grade As String() = GRD_ID.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SCT_DESCR,SCT_ID FROM VW_SECTION_M INNER JOIN VW_GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "SCT_DESCR"
        ddl.DataValueField = "SCT_ID"
        ddl.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddl.Items.Insert(0, li)
    End Sub

    Sub BindSubject()
        Dim grade As String() = ddlMenteeGrade.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S WHERE SBG_ACd_ID=" + ddlAcademicYear.SelectedValue.ToString _
                              & " AND SBG_GRD_ID='" + grade(0) + "' AND SBG_STM_ID=" + grade(1) _
                              & " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)
    End Sub

    Sub updateMentorURL()
        Dim grade As String() = ddlMentorGrade.SelectedValue.ToString.Split("|")
        hfMentorUrl.Value = "../Curriculum/clmShowStudents.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                          & "&grdid=" + grade(0) + "&stmid=" + grade(1) + "&sctid=" + ddlMentorSection.SelectedValue.ToString
    End Sub
    Sub updateMenteeURL()
        Dim grade As String() = ddlMenteeGrade.SelectedValue.ToString.Split("|")
        hfMenteeUrl.Value = "../Curriculum/clmShowStudents.aspx?acdid=" + ddlAcademicYear.SelectedValue.ToString _
                          & "&grdid=" + grade(0) + "&stmid=" + grade(1) + "&sctid=" + ddlMenteeSection.SelectedValue.ToString
    End Sub

    Sub SaveData(ByVal mnt_id As String, ByVal mode As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec saveSTUDENT_MENTOR_MENTEE " _
                                & " @MNT_ID=" + mnt_id + "," _
                                & " @MNT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                & " @MNT_TRM_ID=" + ddlTerm.SelectedValue.ToString + "," _
                                & " @MNT_MENTOR_EMP_ID=" + IIf(h_EMP_ID.Value = "", "NULL", h_EMP_ID.Value) + "," _
                                & " @MNT_MENTOR_STU_ID=" + IIf(hfMentor.Value = "", "NULL", hfMentor.Value) + "," _
                                & " @MNT_MENTEE_STU_ID=" + IIf(hfMentee.Value = "", "NULL", hfMentee.Value) + "," _
                                & " @MNT_SBG_ID=" + ddlSubject.SelectedValue.ToString + "," _
                                & " @MNT_REMARKS=''," _
                                & " @MODE='" + mode + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec getMENTORMENTEEDETAILS " + ddlAcademicYear.SelectedValue.ToString + "," + ddlTerm.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
       
        Dim strFilter As String = ""

        If gvMentorMentee.Rows.Count > 0 Then
            Dim dv As DataView = ds.Tables(0).DefaultView

            Dim txtMentorSearch As TextBox = gvMentorMentee.HeaderRow.FindControl("txtMentorSearch")
            If txtMentorSearch.Text <> "" Then
                strFilter += "  MENTOR_NAME LIKE '%" + txtMentorSearch.Text + "%'"
            End If

            Dim txtMentorIDSearch As TextBox = gvMentorMentee.HeaderRow.FindControl("txtMentorIDSearch")
            If txtMentorIDSearch.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += " AND "
                End If
                strFilter += "  MENTOR_ID LIKE '%" + txtMentorIDSearch.Text + "%'"
            End If

            Dim txtMentorClass As TextBox = gvMentorMentee.HeaderRow.FindControl("txtMentorClass")
            If txtMentorClass.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += " AND "
                End If
                strFilter += " MENTOR_CLASS LIKE '%" + txtMentorClass.Text + "%'"
            End If

            Dim txtMenteeSearch As TextBox = gvMentorMentee.HeaderRow.FindControl("txtMenteeSearch")
            If txtMenteeSearch.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += " AND "
                End If
                strFilter += "  MENTEE_NAME LIKE '%" + txtMenteeSearch.Text + "%'"
            End If

            Dim txtMenteeIDSearch As TextBox = gvMentorMentee.HeaderRow.FindControl("txtMenteeIDSearch")
            If txtMenteeIDSearch.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += " AND "
                End If
                strFilter += "  MENTEE_ID LIKE '%" + txtMenteeIDSearch.Text + "%'"
            End If

            Dim txtMenteeClass As TextBox = gvMentorMentee.HeaderRow.FindControl("txtMenteeClass")
            If txtMenteeClass.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += " AND "
                End If
                strFilter += " MENTEE_CLASS LIKE '%" + txtMenteeClass.Text + "%'"
            End If

            Dim txtSubjectSearch As TextBox = gvMentorMentee.HeaderRow.FindControl("txtSubjectSearch")
            If txtSubjectSearch.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += " AND "
                End If
                strFilter += " MENTEE_SUBJECT LIKE '%" + txtSubjectSearch.Text + "%'"
            End If

            If strFilter <> "" Then
                dv.RowFilter = strFilter
                gvMentorMentee.DataSource = dv
                gvMentorMentee.DataBind()

                TryCast(gvMentorMentee.HeaderRow.FindControl("txtMentorIDSearch"), TextBox).Text = txtMentorIDSearch.Text
                TryCast(gvMentorMentee.HeaderRow.FindControl("txtMentorSearch"), TextBox).Text = txtMentorSearch.Text
                 TryCast(gvMentorMentee.HeaderRow.FindControl("txtMentorClass"), TextBox).Text = txtMentorClass.Text
                TryCast(gvMentorMentee.HeaderRow.FindControl("txtMenteeSearch"), TextBox).Text = txtMenteeSearch.Text
                TryCast(gvMentorMentee.HeaderRow.FindControl("txtMenteeIDSearch"), TextBox).Text = txtMenteeIDSearch.Text
                TryCast(gvMentorMentee.HeaderRow.FindControl("txtMenteeClass"), TextBox).Text = txtMenteeClass.Text
                TryCast(gvMentorMentee.HeaderRow.FindControl("txtSubjectSearch"), TextBox).Text = txtSubjectSearch.Text
            Else
                gvMentorMentee.DataSource = ds
                gvMentorMentee.DataBind()
            End If
        Else
            gvMentorMentee.DataSource = ds
            gvMentorMentee.DataBind()
        End If
    End Sub


#End Region

    Protected Sub btnMentor_DESC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnMentor_ID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnMentor_Class_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnMentee_DESC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnMentee_ID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnMentee_Class_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblMntId As Label = TryCast(sender.findcontrol("lblMntId"), Label)
        SaveData(lblMntId.Text, "DELETE")
        GridBind()
    End Sub

    Protected Sub btSubjectSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindTerm()

        BindGrade(ddlMentorGrade)
        BindSection(ddlMentorSection, ddlMentorGrade.SelectedValue.ToString)


        BindGrade(ddlMenteeGrade)
        BindSection(ddlMenteeSection, ddlMenteeGrade.SelectedValue.ToString)
        GridBind()
    End Sub

    Protected Sub ddlMentorGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMentorGrade.SelectedIndexChanged
        BindSection(ddlMentorSection, ddlMentorGrade.SelectedValue.ToString)
        updateMentorURL()
    End Sub

    Protected Sub ddlMenteeGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMenteeGrade.SelectedIndexChanged
        BindSection(ddlMenteeSection, ddlMenteeGrade.SelectedValue.ToString)
        updateMenteeURL()
        BindSubject()
    End Sub

    Protected Sub ddlMenteeSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMenteeSection.SelectedIndexChanged
        updateMenteeURL()
    End Sub

    Protected Sub ddlMentorSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMentorSection.SelectedIndexChanged
        updateMentorURL()
    End Sub

    Protected Sub rdTeacher_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdTeacher.CheckedChanged
        trTeacher.Visible = True
        trStudent.Visible = False
        txtMentor.Text = ""
        hfMentor.Value = ""
    End Sub

    Protected Sub rdStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdStudent.CheckedChanged
        trTeacher.Visible = False
        trStudent.Visible = True
        txtempname.Text = ""
        h_EMP_ID.Value = ""
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If hfMentee.Value = "" Then
            lblError.Text = "Please select mentee"
            Exit Sub
        End If
        SaveData("0", "ADD")
        GridBind()
        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        GridBind()
    End Sub
End Class
