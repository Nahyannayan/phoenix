﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="clmCASEStudentAttendance.aspx.vb" Inherits="Curriculum_clmCASEStudentAttendance"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID%>').value;

            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }

            var oWnd = radopen("../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                //alert(NameandCode[0]);
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=btnCheck.ClientID %>").click();
    }
}


function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}



function GetSTUDENTS() {

    var sFeatures;
    sFeatures = "dialogWidth: 729px; ";
    sFeatures += "dialogHeight: 600px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID%>').value;

            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "", sFeatures);
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
    }
    else {
        return false;
    }
}

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>CASE Exam Absentees
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td>
                            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="font-weight: bold; width: 60%;" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTC" runat="server" align="center" class="BlueTableView"
                                cellpadding="7" cellspacing="0" style="width: 100%">
                                <tr id="trAcd1" runat="server">
                                    <td align="left" width="10%">
                                        <span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="10%">
                                        <span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="10%">
                                        <span class="field-label">Section</span>
                                    </td>

                                    <td align="left" width="20%">
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trAcd2" runat="server">
                                    <td align="left" width="10%">
                                        <span class="field-label">Subject</span>
                                    </td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="10%"></td>
                                    <td align="left" width="25%"></td>
                                    <td align="left" width="10%"></td>
                                    <td align="left" width="20%"></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" width="10%">
                                        <span class="field-label">Student</span>
                                    </td>

                                    <td align="left" width="25%">
                                        <asp:TextBox ID="txtStudIDs" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;"
                                            OnClick="imgStudent_Click"></asp:ImageButton>
                                       
                                    </td>
                                    <td align="left" colspan="4">
                                         <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            PageSize="5"  OnPageIndexChanging="grdStudent_PageIndexChanging" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Stud. No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle  />
                                        </asp:GridView>
                                    </td>
                                   <%-- <td align="left" width="25%"></td>
                                    <td align="left" width="10%"></td>
                                    <td align="left" width="20%"></td>--%>
                                </tr>
                                <tr>
                                    <td align="center" colspan="6">
                                        <asp:Button ID="btn" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                                <tr id="trcatGrade" runat="server">
                                    <td id="Td1" align="center" width="100%" runat="server" colspan="6">
                                        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="15" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Staff No" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("SSA_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblregno" runat="server" Text='<%# Bind("stu_no") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("stu_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltype" runat="server" Text='<%# Bind("stu_sect") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                    <ItemTemplate>

                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                            Text="Remove If not Absent"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="You are about to delete this record.Do you want to proceed?"
                                                            runat="server">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>



                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>


                            <asp:HiddenField ID="hfSGR_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfACY_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfQSM_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSBM_ID" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>


</asp:Content>
