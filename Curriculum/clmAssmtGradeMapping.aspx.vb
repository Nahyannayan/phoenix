﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Curriculum_clmAssmtGradeMapping
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                'Session("liUserList") = Nothing
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                ViewState("datamode") = "add"



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C100210") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    ' GridBind()
                    ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)

                    tblClm.Rows(2).Visible = False
                    tblClm.Rows(3).Visible = False
                    tblClm.Rows(4).Visible = False
                    tblClm.Rows(5).Visible = False
                    tblClm.Rows(6).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else

        End If
    End Sub

#Region "Private Methods"


    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim ii As Integer
        Dim lblGrade As Label
        Dim lblSBG_ID As Label
        Dim txtValue As TextBox
        Dim ddlResult As DropDownList
        Dim chkSelected As CheckBox

        Dim vDataList As GradeDetails

        Dim grade() As String = ddlGrade.SelectedValue.ToString.Split("|")
        Dim dtList As New Hashtable
        For i = 0 To gvStud.Rows.Count - 1
            vDataList = New GradeDetails
            lblGrade = gvStud.Rows(i).FindControl("lblGrade")
            txtValue = gvStud.Rows(i).FindControl("txtValue")
            ddlResult = gvStud.Rows(i).FindControl("ddlResult")

            vDataList.vGrade = lblGrade.Text
            vDataList.vValue = txtValue.Text
            vDataList.vResult = ddlResult.SelectedValue
            dtList(i) = vDataList
        Next

        Dim htSubjectList As New Hashtable
        For ii = 0 To gvSubjects.Rows.Count - 1
            chkSelected = gvSubjects.Rows(ii).FindControl("chkSelect")
            If Not chkSelected Is Nothing Then
                If chkSelected.Checked Then
                    lblSBG_ID = gvSubjects.Rows(ii).FindControl("lblSUB_ID")
                    If Not lblSBG_ID Is Nothing Then
                        htSubjectList(lblSBG_ID.Text) = lblSBG_ID.Text
                    End If
                End If
            End If
        Next
        htSubjectList(ddlSubject.SelectedValue) = ddlSubject.SelectedValue



        Dim iEnumSubjects As IDictionaryEnumerator
        iEnumSubjects = htSubjectList.GetEnumerator

        While iEnumSubjects.MoveNext
            Dim ienum As IDictionaryEnumerator
            ienum = dtList.GetEnumerator
            While ienum.MoveNext
                vDataList = ienum.Value
                If vDataList Is Nothing Then Continue While
                str_query = "EXEC saveBROWNBOOKGRADEMAPPING " _
                             & ddlAcademicYear.SelectedValue.ToString + "," _
                             & "'" + grade(0) + "'," _
                             & iEnumSubjects.Key + "," _
                             & "'" + vDataList.vGrade + "'," _
                             & "'" + vDataList.vValue + "'," _
                             & "'" + vDataList.vResult + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End While
        End While

        'For i = 0 To gvStud.Rows.Count - 1
        '    lblGrade = gvStud.Rows(i).FindControl("lblGrade")
        '    txtValue = gvStud.Rows(i).FindControl("txtValue")
        '    ddlResult = gvStud.Rows(i).FindControl("ddlResult")

        '    str_query = "EXEC saveBROWNBOOKGRADEMAPPING " _
        '                 & ddlAcademicYear.SelectedValue.ToString + "," _
        '                 & "'" + grade(0) + "'," _
        '                 & ddlSubject.SelectedValue.ToString + "," _
        '                 & "'" + lblGrade.Text + "'," _
        '                 & "'" + txtValue.Text + "'," _
        '                 & "'" + ddlResult.SelectedValue + "'"

        '    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        'Next

        'For ii = 0 To gvStud.Rows.Count - 1
        '    chkSelected = gvSubjects.Rows(ii).FindControl("chkSelect")
        '    If chkSelected Is Nothing Then
        '        If chkSelected.Checked Then
        '            lblSBG_ID = gvSubjects.Rows(ii).FindControl("lblSUB_ID")

        '            For i = 0 To gvStud.Rows.Count - 1
        '                lblGrade = gvStud.Rows(i).FindControl("lblGrade")
        '                txtValue = gvStud.Rows(i).FindControl("txtValue")
        '                ddlResult = gvStud.Rows(i).FindControl("ddlResult")

        '                str_query = "EXEC saveBROWNBOOKGRADEMAPPING " _
        '                             & ddlAcademicYear.SelectedValue.ToString + "," _
        '                             & "'" + grade(0) + "'," _
        '                             & ddlSubject.SelectedValue.ToString + "," _
        '                             & "'" + lblGrade.Text + "'," _
        '                             & "'" + txtValue.Text + "'," _
        '                             & "'" + ddlResult.SelectedValue + "'"

        '                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        '            Next
        '        End If
        '    End If
        'Next
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" _
                              & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " _
                              & " and grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & "  grm_acd_id=" + acdid + "  order by grd_displayorder"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function



    Public Function PopulateCopyGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_query As String = "SELECT distinct CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                              & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                              & " vw_grade_bsu_m as a inner join vw_grade_m as b on a.grm_grd_id=b.grd_id" _
                              & " inner join vw_stream_m as c on a.grm_stm_id=c.stm_id" _
                              & " inner join RPT.REPORT_GRADEMAPPING_BROWNBOOK as d on a.grm_grd_id=d.rbg_grd_id and a.grm_acd_id=d.rbg_acd_id" _
                              & " where grm_acd_id=" + acdid

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function


    Function PopulateSubjects(ByVal ddlSubject As DropDownList, ByVal acd_id As String)
        ddlSubject.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
        Return ddlSubject
    End Function

    Private Sub GridBindSubjects(ByVal acd_id As String)
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " WHERE SBG_ACD_ID=" + acd_id & " AND SBG_ID <> " & ddlSubject.SelectedValue


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds
        gvSubjects.DataBind()
    End Sub

    Function PopulateCopySubjects(ByVal ddl As DropDownList, ByVal acd_id As String)
        ddl.Items.Clear()
        Dim grade As String()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
                                 & " SBG_DESCR+'-'+SBG_PARENTS_SHORT END AS SBG_DESCR FROM SUBJECTS_GRADE_S " _
                                 & " AS A INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID " _
                                 & " inner join RPT.REPORT_GRADEMAPPING_BROWNBOOK AS C ON A.SBG_ID=C.RBG_SBG_ID" _
                                 & " WHERE SBG_ACD_ID=" + acd_id + " AND SBG_ID<>'" + ddlSubject.SelectedValue.ToString + "'"


        If ddlGrade.SelectedValue <> "" Then
            grade = ddlGrade.SelectedValue.Split("|")

            str_query += " AND SBG_GRD_ID='" + grade(0) + "'"
            str_query += " AND SBG_STM_ID=" + grade(1)

        End If
        str_query += " ORDER BY SBG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "SBG_DESCR"
        ddl.DataValueField = "SBG_ID"
        ddl.DataBind()
        Return ddl
    End Function


    Sub GridBind(ByVal bCopy As Boolean)
        Dim str_conn = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String


        If bCopy = False Then
            str_query = "SELECT DISTINCT CASE WHEN RBG_GRADE is NULL THEN RGP_GRADE ELSE RBG_GRADE END AS RBG_GRADE,RBG_VALUE,RBG_RESULT FROM " _
                     & " SUBJECTS_GRADE_S AS A INNER JOIN RPT.REPORT_GRADEMAPPING AS B ON A.SBG_ID=B.RGP_SBG_ID" _
                     & " LEFT OUTER JOIN RPT.REPORT_GRADEMAPPING_BROWNBOOK AS C ON A.SBG_ID=C.RBG_SBG_ID AND B.RGP_GRADE=C.RBG_GRADE" _
                     & " WHERE SBG_ID='" + ddlSubject.SelectedValue.ToString + "'"
        Else
            str_query = "SELECT DISTINCT CASE WHEN RBG_GRADE is NULL THEN RGP_GRADE ELSE RBG_GRADE END AS RBG_GRADE,RBG_VALUE,RBG_RESULT FROM " _
              & " SUBJECTS_GRADE_S AS A INNER JOIN RPT.REPORT_GRADEMAPPING AS B ON A.SBG_ID=B.RGP_SBG_ID" _
              & " LEFT OUTER JOIN RPT.REPORT_GRADEMAPPING_BROWNBOOK AS C ON A.SBG_ID=C.RBG_SBG_ID AND B.RGP_GRADE=C.RBG_GRADE" _
              & " WHERE SBG_ID='" + ddlCopySubject.SelectedValue.ToString + "'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
    End Sub


#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        GridBindSubjects(ddlAcademicYear.SelectedValue.ToString)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ddlSubject = PopulateSubjects(ddlSubject, ddlAcademicYear.SelectedValue.ToString)
        GridBindSubjects(ddlAcademicYear.SelectedValue.ToString)
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        tblClm.Rows(2).Visible = True
        tblClm.Rows(3).Visible = True
        tblClm.Rows(4).Visible = True
        tblClm.Rows(5).Visible = True
        tblClm.Rows(6).Visible = True

        ddlCopyGrade = PopulateCopyGrade(ddlCopyGrade, ddlAcademicYear.SelectedValue.ToString)
        ddlCopySubject = PopulateCopySubjects(ddlCopySubject, ddlAcademicYear.SelectedValue.ToString)
        GridBindSubjects(ddlAcademicYear.SelectedValue.ToString)
        GridBind(False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        lblError.Text = "Record Saved Successfully"
        ddlCopyGrade = PopulateCopyGrade(ddlCopyGrade, ddlAcademicYear.SelectedValue.ToString)
        ddlCopySubject = PopulateCopySubjects(ddlCopySubject, ddlAcademicYear.SelectedValue.ToString)

    End Sub

    Protected Sub ddlCopyGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCopyGrade.SelectedIndexChanged
        ddlCopySubject = PopulateCopySubjects(ddlCopySubject, ddlAcademicYear.SelectedValue.ToString)
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        GridBind(True)
    End Sub
End Class

Public Class GradeDetails
    Public vGrade As String
    Public vValue As String
    Public vResult As String
End Class
