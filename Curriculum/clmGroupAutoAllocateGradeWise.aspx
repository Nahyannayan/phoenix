<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmGroupAutoAllocateGradeWise.aspx.vb" Inherits="Curriculum_clmGroupAutoAllocateGradeWise" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
        var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=gvSubjects.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}


    function change_chk_state(chkThis)
         {
                  
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                     if (chkstate=true)
                     {
                     
                     }
                 }
              }
          }
               
            
     </script>

   <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Auto Allocate Group Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

    <table align="center" width="100%">
        <tr align="left">
            <td >
                <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                     style="text-align: center"></asp:Label></td>
        </tr>
      
                    
                    
                    
        <tr align="left">
            <td width="100%">
                <table id="tb1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="subheader_img" runat="server" visible="false">
                        <td align="center" colspan="4" valign="middle">
                            
                        </td>
                    </tr>
                      
                     <tr>
                        <td align="left" >
                           <span class="field-label">  Academic Year</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                            <td align="left" >
                        <span class="field-label"> Stream</span></td>
                        
                         <td align=left >
                         <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                         </asp:DropDownList></td>
                       
                    </tr>
         
                     <tr>
                       <td align="left" class="matters"   >
                          <span class="field-label">  Grade</span></td>
                        
                         <td align=left class="matters"><asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                         </asp:DropDownList></td>

                         <td colspan="2"> </td>
                    </tr>
                     <tr>
                     
                 <td  colspan="2" >
                    
                
                
                
                             
               </td>
                    </tr>
                   
                    
                      
                    
                   
                    <tr>
                        <td align="center" colspan="4" >
                            <asp:GridView id="gvSubjects" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-bordered table-row" Width="100%">
                        <rowstyle  />
                        <columns>
<asp:TemplateField HeaderText="Available"><EditItemTemplate>
     <asp:CheckBox ID="chkSelect" runat="server"  />
    
</EditItemTemplate>
<HeaderTemplate>
    
    Select 
    <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
    ToolTip="Click here to select/deselect all rows" />
</HeaderTemplate>
<ItemTemplate>
    <asp:CheckBox id="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox> 
    
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="sbg_id" Visible="False"><ItemTemplate>
         <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
         
</ItemTemplate>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Subjects"><EditItemTemplate>
         <asp:Label ID="Label1" runat="server" Text='<%# Eval("SBG_DESCR") %>'></asp:Label>
         
</EditItemTemplate>
<HeaderTemplate>
        
         <asp:Label ID="lblID" runat="server" EnableViewState="False"
         Text="Subject"></asp:Label>
         <asp:TextBox ID="txtSubject" runat="server" ></asp:TextBox>
         <asp:ImageButton ID="btnSubject" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
         OnClick="btnSubject_Click"  />
</HeaderTemplate>
<ItemTemplate>
         <asp:Label ID="lblSubject" runat="server"  CommandName="Select"  Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
         
</ItemTemplate>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Parent Subject" ShowHeader="False"><HeaderTemplate>
     
     <asp:Label ID="lblParent" runat="server" CssClass="gridheader_text" Text="Parent Subject"
     ></asp:Label>
        <asp:TextBox ID="txtParent" runat="server"></asp:TextBox>
        <asp:ImageButton ID="btnParent" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
       OnClick="btnParent_Click"  />
</HeaderTemplate>
<ItemTemplate>
          <asp:Label ID="lblParent" runat="server"  Text='<%# BIND("SBG_PARENTS") %>'    Width="214px"></asp:Label>
         
</ItemTemplate>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</columns>
                        <headerstyle  />
                        <alternatingrowstyle  />
                    </asp:GridView>
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Generate Groups" />
                        </td>
                    </tr>
                </table>
                <asp:XmlDataSource id="XmlSubjects" runat="server" TransformFile="~/Curriculum/subjectXSLT.xsl" EnableCaching="False" XPath="MenuItems/MenuItem"></asp:XmlDataSource>
                
            </td>
        </tr>
         <tr>
                <td align="center" valign="middle">
                    <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                        runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                            type="hidden" value="=" />
                           
                </td>
            </tr>
    </table>

            </div>
        </div>
    </div>
     
</asp:Content>

