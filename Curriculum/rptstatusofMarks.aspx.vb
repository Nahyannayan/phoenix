Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Configuration
Imports CURRICULUM
Imports ActivityFunctions
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports System.Web.Security
Partial Class Curriculum_rptstatusofMarks
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Session("StuMarks") = ReportFunctions.CreateTableStuMarks()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If


            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C3300031") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("datamode") = "add" Then

                        ddlAcdID = ActivityFunctions.PopulateAcademicYear(ddlAcdID, Session("clm"), Session("sBsuid"))

                        If ddlAcdID.Items.Count >= 1 Then
                            PopulateReports(ddlReportId, ddlAcdID)
                            If ddlReportId.SelectedValue <> "" Then
                                PopulateReportSchedule(ddlRptSchedule, ddlReportId)
                                PopulateGrades(ddlGrade, ddlReportId)
                                If ddlGrade.SelectedValue <> "" Then
                                    H_GRD_ID.Value = ddlGrade.SelectedValue
                                    BindSubject()
                                End If
                            End If
                        End If
                    Else

                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function PopulateReports(ByVal ddlReports As DropDownList, ByVal ddlACDYear As DropDownList)
        ddlReports.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_Sql As String = "SELECT RSM_ID,RSM_BSU_ID ,RSM_DESCR FROM RPT.REPORT_SETUP_M WHERE RSM_BSU_ID='" & Session("sBsuId") & "' " _
                   & "  AND RSM_ACD_ID=" & ddlACDYear.SelectedValue & " ORDER BY RSM_DISPLAYORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlReports.DataSource = ds
        ddlReports.DataTextField = "RSM_DESCR"
        ddlReports.DataValueField = "RSM_ID"
        ddlReports.DataBind()

        Return ddlReports
    End Function
    Private Function PopulateReportSchedule(ByVal ddlReportSChedule As DropDownList, ByVal ddlReportsM As DropDownList)
        ddlReportSChedule.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_Sql As String = "SELECT RPF_ID ,RPF_ID ,RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M "
        If ddlReportsM.SelectedValue <> "" Then
            str_Sql += " WHERE RPF_RSM_ID=" & ddlReportsM.SelectedValue & ""
        End If
        str_Sql += " ORDER BY RPF_DISPLAYORDER "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlReportSChedule.DataSource = ds
        ddlReportSChedule.DataTextField = "RPF_DESCR"
        ddlReportSChedule.DataValueField = "RPF_ID"
        ddlReportSChedule.DataBind()

        Return ddlReportSChedule
    End Function

    Private Function PopulateGrades(ByVal ddlGrades As DropDownList, ByVal ddlReportsM As DropDownList)
        ddlGrades.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strCondition As String = ""
        Dim str_Sql As String = ""

        If ddlReportsM.SelectedValue <> "" Then

            If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
                strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "'"
                str_Sql = "SELECT DISTINCT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY,VW_GRADE_M.GRD_DISPLAYORDER " _
                        & " FROM VW_GRADE_M INNER JOIN VW_GRADE_BSU_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID " _
                        & " INNER JOIN GROUPS_M ON SGR_GRD_ID=GRM_GRD_ID INNER JOIN GROUPS_TEACHER_S ON SGR_ID= SGS_SGR_ID " _
                        & " WHERE VW_GRADE_BSU_M.GRM_ACD_ID='" & Session("Current_ACD_ID") & "' " & strCondition & " AND VW_GRADE_BSU_M.GRM_BSU_ID='" & Session("sBsuId") & "' " _
                        & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            Else
                str_Sql = "SELECT VW_GRADE_M.GRD_ID AS ID,VW_GRADE_M.GRD_ID AS DESCR1, VW_GRADE_M.GRD_DISPLAY  " _
                        & " FROM RPT.REPORTSETUP_GRADE_S INNER JOIN " _
                        & " VW_GRADE_M ON RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID = VW_GRADE_M.GRD_ID " _
                        & " WHERE RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID='" & ddlReportsM.SelectedValue & "' " _
                        & " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            End If

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrades.DataSource = ds
        ddlGrades.DataTextField = "GRD_DISPLAY"
        ddlGrades.DataValueField = "ID"
        ddlGrades.DataBind()
        Return ddlGrades

    End Function

    Private Function BindDataGroup()

        Dim strSql As String = ""
        Dim strFilter As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Try
            Dim str_Data As String
            Dim strArr() As String

            str_Data = ddlSubject.SelectedValue
            strArr = str_Data.Split("___")
            H_SBJ_ID.Value = strArr(0)

            If strArr(0).Trim() <> "" Then
                strFilter = " AND SGR_SBG_ID=" & H_SBJ_ID.Value & " "
            End If

            strSql = "SELECT DISTINCT SBG_DESCR AS SUBJECT,SGR_DESCR AS GROUPNAME,GRD_DESCR AS GRADE,CASE WHEN SBG_bCOREEXT=0 THEN 'NORMAL' ELSE 'CORE' END AS SBJTYPE, " & _
                "CASE WHEN (SELECT DISTINCT COUNT(RST_STU_ID) FROM RPT.REPORT_STUDENT_S WHERE RST_SGR_ID=SGR_ID) <  " & _
                "(SELECT DISTINCT COUNT(SSD_STU_ID) FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID=SGR_ID) THEN 'PARTIAL' ELSE 'COMPLETED' END AS 'STATUS','View' as ViewScreen  " & _
                " FROM GROUPS_M " & _
                "INNER JOIN STUDENT_GROUPS_S ON SSD_SGR_ID=SGR_ID " & _
                "INNER JOIN dbo.SUBJECTS_GRADE_S ON SBG_ID=SGR_SBG_ID " & _
                "INNER JOIN vw_GRADE_M ON GRD_ID=SSD_GRD_ID " & _
                " WHERE SSD_GRD_ID='" & H_GRD_ID.Value & "' AND SSD_ACD_ID=" & ddlAcdID.SelectedValue & " " & strFilter

            dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            If dsStudents.Tables(0).Rows.Count >= 1 Then
                gvStudents.DataSource = dsStudents.Tables(0)
                gvStudents.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Function

    Sub BindSubject()
        ddlSubject.Items.Clear()
        Dim strSql As String = ""
        Dim strFilter As String = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Try

            strSql = " SELECT DISTINCT(CAST(SBG_ID AS VARCHAR) + '___' + CAST(ISNULL(SBG_bCOREEXT,0)AS VARCHAR)) ID, " & _
                      " CASE GRm_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS DESCR1, " & _
                      "CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 FROM SUBJECTS_GRADE_S " & _
                      " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                      " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                      " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
            strSql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' " & _
            " AND SBG_ACD_ID = " & ddlAcdID.SelectedValue & " AND SBG_GRD_ID ='" & ddlGrade.SelectedValue & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
            ddlSubject.DataSource = ds
            ddlSubject.DataTextField = "DESCR2"
            ddlSubject.DataValueField = "ID"
            ddlSubject.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim str_Data As String
        Dim strArr() As String

        str_Data = ddlSubject.SelectedValue
        strArr = str_Data.Split("___")
        H_SBJ_ID.Value = strArr(0)

    End Sub

#Region "ButtonClick's"


    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            PopulateReports(ddlReportId, ddlAcdID)
            BindSubject()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlReportId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlReportId.SelectedValue <> "" Then
                PopulateReportSchedule(ddlRptSchedule, ddlReportId)
                PopulateGrades(ddlGrade, ddlReportId)
                BindSubject()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlGrade.SelectedValue <> "" Then
                H_GRD_ID.Value = ddlGrade.SelectedValue
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlRptSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnView_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        BindDataGroup()
    End Sub

    Protected Sub imgSbjGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

#End Region

    Protected Sub gvStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvStudents.PageIndex = e.NewPageIndex
        BindDataGroup()
    End Sub
End Class
