<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="clmReportRule_View.aspx.vb" Inherits="Curriculum_clmReportRule_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />


    <script language="javascript" type="text/javascript">

        function clearSubject() {
            document.getElementById("<%=hfSBG_ID.ClientID %>").value = "";
            document.getElementById("<%=txtSubject.ClientID %>").value = "";
            return false;
        }


        function getSubject() {
            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // url='../Students/ShowAcademicInfo.aspx?id='+mode;

            url = document.getElementById("<%=hfSubjectURL.ClientID %>").value

    result = window.showModalDialog(url, "", sFeatures);

    if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('|');
    document.getElementById("<%=txtSubject.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hfSBG_ID.ClientID %>").value = NameandCode[1];



}



var color = '';
function highlight(obj) {
    var rowObject = getParentRow(obj);
    var parentTable = document.getElementById("<%=gvStud.ClientID %>");
     if (color == '') {
         color = getRowColor();
     }
     if (obj.checked) {
         rowObject.style.backgroundColor = '#f6deb2';
     }
     else {
         rowObject.style.backgroundColor = '';
         color = '';
     }
     // private method

     function getRowColor() {
         if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
         else return rowObject.style.backgroundColor;
     }
 }
 // This method returns the parent row of the object
 function getParentRow(obj) {
     do {
         obj = obj.parentElement;
     }
     while (obj.tagName != "TR")
     return obj;
 }


 function change_chk_state(chkThis) {
     var chk_state = !chkThis.checked;
     for (i = 0; i < document.forms[0].elements.length; i++) {
         var currentid = document.forms[0].elements[i].id;
         if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1 && document.forms[0].elements[i].disabled == false) {
             //if (document.forms[0].elements[i].type=='checkbox' )
             //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
             document.forms[0].elements[i].checked = chk_state;
             document.forms[0].elements[i].click();//fire the click event of the child element
         }
     }
 }








    </script>


    <script language="javascript" type="text/javascript">
        function confirm_remove() {

            if (confirm("Are you sure you want to Delete the selected rules?") == true)
                return true;
            else
                return false;

        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Report Rule Setup"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" align="center" cellpadding="0" width="100%"
                    cellspacing="0">

                    <tr>

                        <td align="left" style="height: 1px">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center" style="font-weight: bold; height: 215px" valign="top">

                            <table id="tblClm" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Term</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTerm" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>

                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Subject</span></td>

                                    <td align="left" width="30%">
                                        
                                                    <asp:TextBox ID="txtSubject" Visible="false" runat="server" Enabled="False" MaxLength="100" TabIndex="2"></asp:TextBox>
                                               
                                                    <asp:ImageButton ID="imgSub" Visible="false" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getSubject();" TabIndex="18"></asp:ImageButton>
                                                    <asp:DropDownList ID="ddlSubjects" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    <asp:LinkButton Visible="false" ID="lnkClear" runat="server" OnClientClick="return clearSubject();">(Clear)</asp:LinkButton></td>
                                           
                                    <td align="left" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="height: 16px" align="left">
                                        <asp:LinkButton ID="lnkAddNew" runat="server">Add New</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"  valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                       Select
                                                                    <br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRrmId" runat="server" Text='<%# Bind("RRM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbgId" runat="server" Text='<%# Bind("SBG_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Rule">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblRule" runat="server" Text='<%# Bind("RRM_DESCR") %>'></asp:LinkButton>


                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Report Card">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReport" runat="server" Text='<%# Bind("RSM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Report Schedule">
                                                    <HeaderTemplate>
                                                       Report Schedule<br />
                                                                    <asp:DropDownList ID="ddlgvSchedule" runat="server" AutoPostBack="True" 
                                                                        OnSelectedIndexChanged="ddlgvSchedule_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrinted" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Header">
                                                    <HeaderTemplate>
                                                       Header<br />
                                                                    <asp:DropDownList ID="ddlgvHeader" runat="server" AutoPostBack="True" 
                                                                        OnSelectedIndexChanged="ddlgvHeader_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHeader" runat="server" Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Subject">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Term">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTerm" runat="server" Text='<%# Bind("TRM_DESCRIPTION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Exam Level">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLevel" runat="server" Text='<%# Bind("RRM_TYPE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" valign="top">
                                        <asp:Button ID="btnDelete" OnClientClick="javascipt:return confirm_remove();" runat="server" Text="Delete" CssClass="button" TabIndex="4" /></td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                            <asp:HiddenField ID="hfSBG_ID" runat="server" EnableViewState="False" />
                            <asp:HiddenField ID="hfSubjectURL" runat="server" />
                            &nbsp;
                         
                        </td>
                    </tr>


                </table>


                <script type="text/javascript" lang="javascript">
                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });
                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
    </script>


            </div>
        </div>
    </div>
</asp:Content>

