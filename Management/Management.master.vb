Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Partial Class Management_Management
    Inherits System.Web.UI.MasterPage
    Dim userSuper As Boolean


    Public ReadOnly Property MenuName() As String
        Get
            Return Session("temp")
        End Get

    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("sUsr_name") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If
        Page.Title = OASISConstants.Gemstitle
        If Page.IsPostBack = False Then
            Dim ItemTypeCounter As Integer = 0
            Dim tblbUsr_id As String = Session("sUsr_id")
            Dim tblbUSuper As Boolean = Session("sBusper")

            Dim buser_id As String = Session("sBsuid")

            'if user access the page directly --will be logged back to the login page
            If tblbUsr_id = "" Then
                Response.Redirect("login.aspx")
            Else

                If Session("sUsr_name") & "" <> "" Then
                    lblFinancial.Text = "Financial Year : " + Session("F_Descr")
                    'lblFilePath.Text = Session("Menu_text")
                    If Session("Menu_text") <> "" And Session("Menu_text") <> String.Empty Then
                        Dim menu_arr() As String = Session("Menu_text").Split("|")
                        Session("temp") = menu_arr(menu_arr.Length - 1)
                    End If
                    Try
                        'if user is super admin give access to all the Business Unit 

                        Dim ds As New DataSet
                        Dim moduleDecr As New Encryption64
                        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                        Dim bunit As String = Session("sBsuid")
                        '   Dim userMod As String = moduleDecr.Decrypt(Request.QueryString("userMod").Replace(" ", "+"))
                        Dim userRol As String = Session("sroleid")

                        Dim userMod As String = Session("sModule")
                        Dim username As String = Session("sUsr_name")
                        lblUserLog.Text = username.ToUpper

                        userSuper = Session("sBusper")
                        Using Imgreader As SqlDataReader = AccessRoleUser.GetBUnitImage(bunit)
                            While Imgreader.Read
                                imgSchool.ImageUrl = Imgreader.GetString(0)

                            End While
                        End Using

                     
                    Catch ex As Exception
                        lblMainError.Text = "Your page could not processed"
                    End Try
                End If
            End If
        End If

        

    End Sub

    Private Function Pad(ByVal numberOfSpaces As Int32) As String
        Dim Spaces As String = ""

        For items As Int32 = 1 To numberOfSpaces
            Spaces &= "&nbsp;"
        Next

        Return Server.HtmlDecode(Spaces)
    End Function

    'Public Shared Property menuPath() As String
    '    Get
    '        Return lblFilePath.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblFilePath.Text = value

    '    End Set
    'End Property
     

    Public Function StringVerify(ByVal pVal As String) As Boolean
        Dim lstrLen As String
        Dim lintCounter As Integer

        lstrLen = pVal.Length
        For lintCounter = 1 To lstrLen
            Select Case Asc(Mid(pVal, lintCounter, 1))
                Case 65 To 90 'A-Z 
                    ' --- Ok
                Case 97 To 122 'a-z 
                    ' --- OK
                Case 48 To 57 ' 0123456789
                    '   --- OK
                Case Else
                    Return False
            End Select
        Next

        Return True
    End Function
    Public Function StringWithSpace(ByVal pVal As String) As Boolean
        Dim lstrLen As String
        Dim lintCounter As Integer

        lstrLen = pVal.Length
        For lintCounter = 1 To lstrLen
            If (Trim(Mid(pVal, lintCounter, 1)) <> "") Then
                Select Case Asc(Mid(pVal, lintCounter, 1))
                    Case 65 To 90 'A-Z 
                        ' --- Ok
                    Case 97 To 122 'a-z 
                        ' --- OK
                    Case 48 To 57 ' 0123456789
                        '   --- OK
                    Case Else
                        Return False
                End Select
            End If
        Next

        Return True
    End Function

    Public Function NumberVerify(ByVal pVal As String) As Boolean
        Dim lstrLen As String
        Dim lintCounter As Integer

        lstrLen = pVal.Length
        For lintCounter = 1 To lstrLen
            Select Case Asc(Mid(pVal, lintCounter, 1))
                Case 48 To 57 ' 0123456789
                    '   --- OK
                Case Else
                    Return False
            End Select
        Next

        Return True
    End Function

    Public Function GetNextDocNo(ByVal pDocType As String, ByVal pMonth As String, ByVal pYear As String) As String
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        'Dim ds As New DataSet

        'Dim str_Sql As String = "select dbo.fN_ReturnNextNo('" & pDocType & "','" & Session("sBSUId") & "','" & pMonth & "','" & pYear & "')"
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        'If ds.Tables(0).Rows.Count > 0 Then
        '    Return ds.Tables(0).Rows(0)(0) & ""
        'Else
        '    Return "--"
        'End If
        Return AccountFunctions.GetNextDocId(pDocType, Session("sBSUId"), pMonth, pYear)
    End Function

    Public Function GetDescr(ByVal pTableName As String, ByVal pFieldName As String, _
    ByVal pWhere As String, ByVal pVal As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet

        Dim str_Sql As String = "SELECT  [" & pFieldName & "] From " & pTableName & " WHERE " & pWhere & "='" & pVal & "'"


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0) & ""
        Else
            Return "--"
        End If

    End Function

    Public Function CheckPosted(ByVal pDocType As String, ByVal pDocNo As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet

        Dim str_Sql As String = "select GUID From Journal_H WHERE JHD_BSU_ID='" & Session("sBsuId") & "' AND JHD_DOCTYPE='" & pDocType & "' AND JHD_DOCNO='" & pDocNo & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Try
            If Session("FROM") Is Nothing OrElse Session("FROM") = False Then
                Select Case Session("USR_MIS").ToString
                    Case 1
                        Response.Redirect("~/Management/empManagementMain.aspx")
                End Select
            Else
                If Session("FROM") = True Then
                    Select Case Session("USR_MIS").ToString
                        Case 1
                            Response.Redirect("~/Management/empManagementMain.aspx")
                    End Select
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

End Class

