Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports UtilityObj

Partial Class BusinessUnit
    Inherits System.Web.UI.Page
    ''' 
    ''' Version              Date            Author                 Change
    ''' 1.1             1-Dec-2015          Swapna                  Adding BSu is DAX enabled session variable
    ''' 1.2             31-Oct-2017         Jacob                   Adding BSU is VAT enabled session variable
    ''' 1.3             07-Feb-2018         Jacob                   Adding filtered bsu dropdown

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Recoveryflag()
                bind_Financial_year()
                BindBusinessUnit()
                'if user access the page directly --will be logged back to the login page 
                If Session("sUsr_id") = "" Then
                    Response.Redirect("login.aspx")
                Else
                    If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                        ddlBUnit.SelectedValue = Request.Cookies("OASIS_SETTINGS")("BSUID")
                        ddFinancialYear.SelectedValue = Request.Cookies("OASIS_SETTINGS")("F_YEAR")
                    End If
                End If
            Catch ex As Exception
                lblError.Text = "Sorry ,your request could not be processed"
                UtilityObj.Errorlog(ex.Message, "Business Unit Load")
            End Try
        End If
    End Sub

    Public Sub Recoveryflag()
        Dim sql_query = "SELECT RECORD_ID FROM USER_PASSWORD_QUESTIONS WITH(NOLOCK) WHERE USER_NAME='" & Session("sUsr_name").ToString() & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then
        Else
            Hiddenrecoveryflag.Value = 1
        End If
    End Sub
    Sub bind_Financial_year()
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT FYR_ID, FYR_DESCR, bDefault FROM FINANCIALYEAR_S WITH ( NOLOCK ) ORDER BY bDefault DESC ,FYR_ID DESC"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        ddFinancialYear.DataTextField = "FYR_DESCR"
        ddFinancialYear.DataValueField = "FYR_ID"
        ddFinancialYear.DataSource = ds.Tables(0)
        ddFinancialYear.DataBind()
    End Sub

    Sub BindBusinessUnit()
        Dim ds As New DataSet
        Dim str_sql As String = "SELECT BSU_ID, BSU_NAME, BSU_NAMEwithShort FROM [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW, 0) = 1 ORDER BY BSU_NAME"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        ddlBUnit.DataSource = ds.Tables(0)
        ddlBUnit.DataTextField = "BSU_NAMEwithShort"
        ddlBUnit.DataValueField = "BSU_ID"
        ddlBUnit.DataBind()
        ddlBUnit.SelectedIndex = -1
        If Not ddlBUnit.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub imgBtnBsu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnBsu.Click
        Try
            Session("BSU_Name") = ddlBUnit.SelectedItem.Text
            Session("sBsuid") = ddlBUnit.SelectedValue
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then

                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(ddlBUnit.SelectedValue)
                    While BSUInformation.Read
                        Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_COUNTRY_ID") = Trim(Convert.ToString(BSUInformation("BSU_COUNTRY_ID")))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                        Session("BSU_IsOnDAX") = Convert.ToInt32(BSUInformation("BSU_IsOnDAX"))
                        Session("BSU_IsHROnDAX") = Convert.ToInt32(BSUInformation("BSU_IsHROnDAX"))
                        Session("BSU_bVATEnabled") = Convert.ToBoolean(BSUInformation("BSU_bVATEnabled"))
                        Dim strformat As String = "0."
                        For i As Integer = 1 To CInt(Session("BSU_ROUNDOFF"))
                            strformat = strformat & "0"
                        Next
                        Session("BSU_DataFormatString") = strformat
                    End While
                End Using
                Session("F_YEAR") = ddFinancialYear.SelectedItem.Value
                Session("F_Descr") = ddFinancialYear.SelectedItem.Text
                Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(ddFinancialYear.SelectedItem.Value)


            End If

            Dim OasisSettingsCookie As HttpCookie
            If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            Else
                OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            End If
            OasisSettingsCookie.Values("BSUID") = ddlBUnit.SelectedValue
            OasisSettingsCookie.Values("F_YEAR") = ddFinancialYear.SelectedItem.Value
            OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            Response.Cookies.Add(OasisSettingsCookie)

            Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Business Unit", "Business Unit", "Login", Page.User.Identity.Name.ToString)
            If auditFlag <> 0 Then
                Throw New ArgumentException("Unable to track your request")
            End If

            If ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                Dim url As String
                url = String.Format("~\eduShield\eduShield_Home.aspx")
                Session("sModule") = "E0"
                Response.Redirect(url)
            Else
                Response.Redirect("Modulelogin.aspx")
            End If
        Catch ex As Exception
            lblError.Text = "Unable to track your request"
            UtilityObj.Errorlog(ex.Message, "Business Unit Selection")
        End Try
    End Sub

    
End Class
