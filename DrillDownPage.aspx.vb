﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports Telerik
Imports Telerik.Web.UI
Partial Class DrillDownPage
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim userMod As String = Session("sModule")

            Dim userRol As String = Session("sroleid")
            Dim bsu As String = Session("sBsuid")
            Dim dbs As String = Session("DBS_ID")

            Session("BSU") = Session("sBsuid")
            Session("ACY") = Session("AcadYear_Descr")
            Session("CL") = Session("CLM")
            Session("GRD") = ""
            Session("SCT") = ""
            Session("STM") = ""
            Session("FTYPE") = ""

            BIND_DASHBOARD(dbs, 1)
            btnBack.Style.Add("display", "none")
        End If

    End Sub
    Private Sub BIND_DASHBOARD(ByVal dbm_id As String, ByVal type1 As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlParameter


        param(0) = New SqlParameter("@DBM_ID", dbm_id)
        param(1) = New SqlParameter("@TYPE", type1)

        Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "BIND_DASHBOARD_PAGE_drill", param)

        If dsDetails.Tables(0).Rows.Count > 0 Then
            hdnType.Value = dsDetails.Tables(0).Rows(0)("DBD_TYPE")
            hdnID.Value = dsDetails.Tables(0).Rows(0)("DBD_ID")
            hdnNext.Value = dsDetails.Tables(0).Rows(0)("DBD_NEXT_ID")
            hdParams.Value = dsDetails.Tables(0).Rows(0)("DBD_PARAMS")
            hdNextParam.Value = dsDetails.Tables(0).Rows(0)("DBD_NEXT_PARAM")
            hdnURL.Value = dsDetails.Tables(0).Rows(0)("DBD_URL")
            GetData(hdnID.Value)
        End If

    End Sub
    Public Function BIND_PARAMS(ByVal dbd_id As String) As DataSet
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlParameter


        param(0) = New SqlParameter("@DBD_ID", dbd_id)


        Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getParams_Drill", param)

        Return dsDetails



    End Function
    Public Function bindChart(ByVal id As String) As DataSet
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(15) As SqlParameter
            Dim i As Integer = 1
            Dim cnt As Integer = 0

            PARAM(0) = New SqlParameter("@ID", id)


            Dim dsParams As DataSet = BIND_PARAMS(id)

            While i <= dsParams.Tables(0).Rows.Count

                If dsParams.Tables(0).Rows(cnt)("ID") = "@BSU_ID" Then
                    PARAM(i) = New SqlParameter("@BSU_ID", Session("BSU"))
                ElseIf dsParams.Tables(0).Rows(cnt)("ID") = "@ACD_DESCR" Then
                    PARAM(i) = New SqlParameter("@ACD_DESCR", Session("ACY"))
                ElseIf dsParams.Tables(0).Rows(cnt)("ID") = "@SEL_GRADE" Then
                    PARAM(i) = New SqlParameter("@SEL_GRADE", Session("GRD"))
                ElseIf dsParams.Tables(0).Rows(cnt)("ID") = "@SEL_SECTION" Then
                    PARAM(i) = New SqlParameter("@SEL_SECTION", Session("SCT"))
                ElseIf dsParams.Tables(0).Rows(cnt)("ID") = "@SEL_STREAM" Then
                    PARAM(i) = New SqlParameter("@SEL_STREAM", Session("STM"))
                ElseIf dsParams.Tables(0).Rows(cnt)("ID") = "@SEL_CLM" Then
                    PARAM(i) = New SqlParameter("@SEL_CLM", Session("CL"))
                ElseIf dsParams.Tables(0).Rows(cnt)("ID") = "@SEL_FEETYPE" Then
                    PARAM(i) = New SqlParameter("@SEL_FEETYPE", Session("FTYPE"))
                ElseIf dsParams.Tables(0).Rows(cnt)("ID") = "@SEL_STU" Then
                    PARAM(i) = New SqlParameter("@SEL_STU", Session("STU"))
                End If

                cnt += 1

                i += 1
            End While






            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SP_OASIS_DASHBOARD_DRILL", PARAM)

            Return dsDetails


        Catch ex As Exception

        End Try
    End Function

    Sub GetData(ByVal id As String)
        Dim ds As DataSet = bindChart(id)
        If hdnType.Value = "Bar" Then
            divBar.Visible = True
            divPie.Visible = False
            divDonut.Visible = False
            divGauge.Visible = False
            divSeries.Visible = False
            divLine.Visible = False
            divStacked.Visible = False
            divRepeater.Visible = False
            divGrid.Visible = False
            divPage.Visible = False
            RadHtmlChart1.DataSource = ds.Tables(0)
            RadHtmlChart1.DataBind()
            lblBar.Text = hdnTitle.Value

        End If
        If hdnType.Value = "Pie" Then
            divBar.Visible = False
            divPie.Visible = True
            divDonut.Visible = False
            divGauge.Visible = False
            divSeries.Visible = False
            divLine.Visible = False
            divStacked.Visible = False
            divRepeater.Visible = False
            divGrid.Visible = False
            divPage.Visible = False
            RadHtmlChart2.DataSource = ds.Tables(0)
            RadHtmlChart2.DataBind()
            lblPie.Text = hdnTitle.Value

        End If
        If hdnType.Value = "Donut" Then
            divBar.Visible = False
            divPie.Visible = False
            divDonut.Visible = True
            divGauge.Visible = False
            divSeries.Visible = False
            divLine.Visible = False
            divStacked.Visible = False
            divRepeater.Visible = False
            divGrid.Visible = False
            divPage.Visible = False
            RadHtmlChart3.DataSource = ds.Tables(0)
            RadHtmlChart3.DataBind()
            lblDonut.Text = hdnTitle.Value

        End If
        If hdnType.Value = "Series" Then
            divBar.Visible = False
            divPie.Visible = False
            divDonut.Visible = False
            divGauge.Visible = False
            divSeries.Visible = True
            divLine.Visible = False
            divStacked.Visible = False
            divRepeater.Visible = False
            divGrid.Visible = False
            divPage.Visible = False
            RadHtmlChart4.DataSource = ds.Tables(0)
            RadHtmlChart4.DataBind()
            lblSeries.Text = hdnTitle.Value
            Dim dt As New DataTable
            If Not ds.Tables(0) Is Nothing Then
                dt = ds.Tables(0)
                If Not dt Is Nothing Then
                    If dt.Rows.Count > 0 Then
                        RadHtmlChart4.PlotArea.Series(0).Name = Convert.ToString(dt.Rows(0)("NAME1"))
                        RadHtmlChart4.PlotArea.Series(1).Name = Convert.ToString(dt.Rows(0)("NAME2"))
                        RadHtmlChart4.PlotArea.Series(2).Name = Convert.ToString(dt.Rows(0)("NAME3"))
                    End If
                End If
            End If

        End If
        If hdnType.Value = "Line" Then
            divBar.Visible = False
            divPie.Visible = False
            divDonut.Visible = False
            divGauge.Visible = False
            divSeries.Visible = False
            divLine.Visible = True
            divStacked.Visible = False
            divRepeater.Visible = False
            divGrid.Visible = False
            divPage.Visible = False
            RadHtmlChart5.DataSource = ds.Tables(0)
            RadHtmlChart5.DataBind()
            lblLine.Text = hdnTitle.Value

        End If
        If hdnType.Value = "Stacked" Then
            divBar.Visible = False
            divPie.Visible = False
            divDonut.Visible = False
            divGauge.Visible = False
            divSeries.Visible = False
            divLine.Visible = False
            divStacked.Visible = True
            divRepeater.Visible = False
            divGrid.Visible = False
            divPage.Visible = False
            RadHtmlChart6.DataSource = ds.Tables(0)
            RadHtmlChart6.DataBind()
            lblStacked.Text = hdnTitle.Value
            Dim dt As New DataTable
            If Not ds.Tables(0) Is Nothing Then
                dt = ds.Tables(0)
                If Not dt Is Nothing Then
                    If dt.Rows.Count > 0 Then
                        RadHtmlChart6.PlotArea.Series(0).Name = Convert.ToString(dt.Rows(0)("X1"))
                        RadHtmlChart6.PlotArea.Series(1).Name = Convert.ToString(dt.Rows(0)("X2"))

                    End If
                End If
            End If
        End If
        If hdnType.Value = "Repeater" Then
            divBar.Visible = False
            divPie.Visible = False
            divDonut.Visible = False
            divGauge.Visible = False
            divSeries.Visible = False
            divLine.Visible = False
            divStacked.Visible = False
            divRepeater.Visible = True
            divGrid.Visible = False
            divPage.Visible = False
            rptRepeater.DataSource = ds.Tables(0)
            rptRepeater.DataBind()
            lblRepeater.Text = hdnTitle.Value

        End If
        If hdnType.Value = "Grid" Then
            divBar.Visible = False
            divPie.Visible = False
            divDonut.Visible = False
            divGauge.Visible = False
            divSeries.Visible = False
            divLine.Visible = False
            divStacked.Visible = False
            divRepeater.Visible = False
            divGrid.Visible = True
            divPage.Visible = False
            RadGrid1.DataSource = ds.Tables(0)
            RadGrid1.DataBind()
            lblGrid.Text = hdnTitle.Value

        End If
        If hdnType.Value = "Gauge" Then
            divBar.Visible = False
            divPie.Visible = False
            divDonut.Visible = False
            divGauge.Visible = True
            divSeries.Visible = False
            divLine.Visible = False
            divStacked.Visible = False
            divRepeater.Visible = False
            divGrid.Visible = False
            divPage.Visible = False
            RadRadialGauge1.Pointer.Value = ds.Tables(0).Rows(0)(0).ToString
            lblGaugeVal.Text = ds.Tables(0).Rows(0)(0).ToString
            lblGauge.Text = hdnTitle.Value

        End If
        If hdnType.Value = "Page" Then
            divBar.Visible = False
            divPie.Visible = False
            divDonut.Visible = False
            divGauge.Visible = False
            divSeries.Visible = False
            divLine.Visible = False
            divStacked.Visible = False
            divRepeater.Visible = False
            divGrid.Visible = False
            divPage.Visible = True
            ' divPage.InnerHtml = hdnURL.Value
            ' divPage.InnerHtml = "<object type=''text/html'' data='" + hdnURL.Value + "' ></object>"
            ' Response.Redirect(hdnURL.Value)
            ifPage.Src = hdnURL.Value

        End If
    End Sub
 

    Protected Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click

        btnBack.Style.Add("display", "block")

        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlParameter


        param(0) = New SqlParameter("@DBM_ID", hdnNext.Value)

        Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "BIND_DASHBOARD_PAGE_drill", param)

        If dsDetails.Tables(0).Rows.Count > 0 Then
            hdnType.Value = dsDetails.Tables(0).Rows(0)("DBD_TYPE")
            hdnID.Value = dsDetails.Tables(0).Rows(0)("DBD_ID")
            hdnNext.Value = dsDetails.Tables(0).Rows(0)("DBD_NEXT_ID")
            hdParams.Value = dsDetails.Tables(0).Rows(0)("DBD_PARAMS")
            hdNextParam.Value = dsDetails.Tables(0).Rows(0)("DBD_NEXT_PARAM")
            hdnURL.Value = dsDetails.Tables(0).Rows(0)("DBD_URL")
        End If
        GetData(hdnID.Value)
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click

        Dim dbs As String = Session("DBS_ID")
        BIND_DASHBOARD(dbs, 1)
    End Sub
    Protected Sub RadAjaxManager1_AjaxRequest(sender As Object, e As AjaxRequestEventArgs)
        Dim seriesName As String = RadHtmlChart1.PlotArea.Series(0).Name

        Dim x = e.Argument

        If hdNextParam.Value = "BSU_ID" Then
            Session("BSU") = x
        ElseIf hdNextParam.Value = "ACY_ID" Then
            Session("ACY") = x
        ElseIf hdNextParam.Value = "CLM_ID" Then
            Session("CL") = x
        ElseIf hdNextParam.Value = "GRD_ID" Then
            Session("GRD") = x
        ElseIf hdNextParam.Value = "SCT_ID" Then
            Session("SCT") = x
        ElseIf hdNextParam.Value = "STM_ID" Then
            Session("STM") = x
        ElseIf hdNextParam.Value = "FTYPE" Then
            Session("FTYPE") = x
        ElseIf hdNextParam.Value = "STU" Then
            Session("STU") = x
        End If

    End Sub
End Class
