Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Telerik.Web.UI
Imports System.Collections.Generic

Partial Class rptFilter
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session.Remove("rptclass")

                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C950085") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                callYEAR_DESCRBind()
                callGrade_ACDBind()
                BindBuisnessUnit()

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Sub BindBuisnessUnit()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim strQuery As String
        If Session("sbsuid") = "999998" Then
            strQuery = "SELECT BSU_SHORT_RESULT,BSU_ID FROM BUSINESSUNIT_M" _
                      & " WHERE  ISNULL(BSU_bBOARDRESULT,0)=1  and bsu_short_result is not null"

        Else
            strQuery = "SELECT BSU_SHORT_RESULT,BSU_ID FROM BUSINESSUNIT_M" _
                         & " WHERE  ISNULL(BSU_bBOARDRESULT,0)=1 AND BSU_ID IN (SELECT USA_BSU_ID FROM USERACCESS_S WHERE USA_USR_ID='" + Session("sUsr_id").ToString + "') and bsu_short_result is not null"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        lstBSU.DataSource = ds
        lstBSU.DataTextField = "BSU_SHORT_RESULT"
        lstBSU.DataValueField = "BSU_ID"
        lstBSU.DataBind()

        'Dim i As Integer

        'With lstBSU
        '    For i = 0 To .Items.Count - 1
        '        If .Items(i).Value = Session("sBsuid") Then
        '            .Items(i).Selected = True
        '        End If
        '    Next
        'End With
        If Session("sbsuid") <> "999998" Then
            lstBSU.FindItemByValue(Session("sBsuid").ToString()).Checked = True
        End If
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub callYEAR_DESCRBind()
        Dim bUnit As String
        Dim CLM As String
        Dim ACY As String

        Try
            Dim di As ListItem
            If Session("sBsuid") = "999998" Or Session("sBsuid") = "999999" Then
                bUnit = "121013"
            Else
                bUnit = Session("sBsuid")
            End If
            '   =============== GET THE CURRICULUM  =============
            Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(bUnit)
                While BSUInformation.Read
                    Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                End While
            End Using


            Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(bUnit, Session("CLM"))
                If readerAcademic_Cutoff.HasRows = True Then
                    While readerAcademic_Cutoff.Read
                        Session("Current_ACY_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))
                        Session("Current_ACD_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ID"))
                    End While
                End If
            End Using



            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR2(bUnit, Session("CLM"), Session("Current_ACY_ID"))
                ddlAcademicYear.Items.Clear()
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using

            Using readerPrev_Next As SqlDataReader = AccessStudentClass.getNEXT_CURR_PREV(bUnit, Session("CLM"), Session("Current_ACY_ID"))
                If readerPrev_Next.HasRows = True Then
                    While readerPrev_Next.Read
                        Session("prev_ACD_ID") = Convert.ToString(readerPrev_Next("prev_ACD_ID"))
                        Session("next_ACD_ID") = Convert.ToString(readerPrev_Next("next_ACD_ID"))
                    End While
                End If
            End Using


            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("prev_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Function getAcy() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim strQuery As String = "SELECT ACD_ACY_ID FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery).ToString
    End Function

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) _
            Handles btnGenerateReport.Click


        Dim url As String
        Dim bsu_ids As String = ""
        Dim bsu_names As String = ""
        Dim i As Integer

        'For i = 0 To lstBSU.Items.Count - 1
        '    If lstBSU.Items(i).Selected = True Then
        '        If bsu_ids <> "" Then
        '            bsu_ids += "|"
        '        End If
        '        bsu_ids += lstBSU.Items(i).Value

        '        If bsu_names <> "" Then
        '            bsu_names += ","
        '        End If
        '        bsu_names += lstBSU.Items(i).Text
        '    End If
        'Next

        Dim collection As IList(Of RadComboBoxItem) = lstBSU.CheckedItems

        If (collection.Count <> 0) Then
            For Each item As RadComboBoxItem In collection
                bsu_ids += item.Value
                bsu_names += item.Text
                If bsu_ids <> "" Then
                    bsu_ids += "|"
                End If
                If bsu_names <> "" Then
                    bsu_names += ","
                End If
            Next
            bsu_ids = bsu_ids.TrimEnd("|")
            bsu_names = bsu_names.TrimEnd(",")
        Else
            If lstBSU.SelectedIndex > 0 Then
                bsu_ids = lstBSU.SelectedItem.Value
                bsu_names = lstBSU.SelectedItem.Text
            End If
        End If


        Session("htHeader") = Nothing
        Dim strRedirect As String = "~/ResAnalysis/DashBoards/ResDashBoard_Summary.aspx"
        url = String.Format("{0}?&grd_id={1}&acy_id={2}&bsu_ids={3}&bsu_names={4}", _
                            strRedirect, ddlGrade.SelectedValue.ToString, getAcy, bsu_ids, bsu_names)
        ' ResponseHelper.Redirect(url, "_blank", "")
        divDash.Visible = False
        ifDash.Src = url

    End Sub
    Public Shared Function GetGRM_GRD_ID(ByVal GRD_ID As String, ByVal CLM As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --16/JAN/2008
        'Purpose--Get DRQ_GRM_GRD_ID data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGRM_GRD_ID As String = ""

        sqlGetGRM_GRD_ID = "SELECT Distinct GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY " & _
" FROM ACADEMICYEAR_D INNER JOIN  GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & GRD_ID & "' AND GRADE_BSU_M.GRM_GRD_ID IN ('10','12') order by GRADE_BSU_M.GRM_GRD_ID desc"


        Dim command As SqlCommand = New SqlCommand(sqlGetGRM_GRD_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


End Class
