<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptResDashBoardView.aspx.vb" Inherits="rptFilter" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;

            if (mode == "cashFlow") {
                result = window.showModalDialog("../../../../Accounts/selBussinessUnit.aspx?multiSelect=false", "", sFeatures)
            }
            else {
                result = window.showModalDialog("./../../../Accounts/selBussinessUnit.aspx", "", sFeatures)
            }
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }


        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }

    </script>
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Literal ID="lblCaption" runat="server" Text="Results Dashboard"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div id="divDash" runat="server">
                    <table width="100%">
                        <tr align="left">
                            <td>
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                    ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        </tr>
                        <tr align="left">
                            <td  >
                                <table width="100%">

                                    <tr>
                                        <td align="left" class="matters" width="20%"><span class="field-label">Business unit</span></td>
                                 
                                        <td align="left" class="matters" width="30%">
                                   <%--         <asp:CheckBox ID="chkSelect" onclick="javascript:fnSelectAll(this);" runat="server" Text="Select All" /><br />
                                            <asp:CheckBoxList ID="lstBSU" runat="server" Style="vertical-align: middle; overflow: auto; text-align: left; border-right: #6a923a 1px solid; border-top: #6a923a 1px solid; border-left: #1b80b6 1px solid; border-bottom: #6a923a 1px solid;" BorderStyle="Solid" BorderWidth="1px" Width="165px" RepeatLayout="Flow">
                                            </asp:CheckBoxList>--%>
                                             <telerik:RadComboBox RenderMode="Lightweight" Width="100%" AutoPostBack="true" runat="server" ID="lstBSU" CheckBoxes="true" 
                                                 EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Business Unit(s)">  </telerik:RadComboBox>

                                        </td>
                                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                                     
                                        <td align="left" class="matters" style="text-align: left" width="30%">
                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">Grade</span></td>
                                        <td align="left" class="matters" style="text-align: left;">
                                            <asp:DropDownList ID="ddlGrade" runat="server">
                                            </asp:DropDownList></td>
                                        <td align="center" class="matters" colspan="2" style="text-align: center">
                                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                                ValidationGroup="dayBook" />&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="h_BSUID" runat="server" />
                                <asp:HiddenField ID="h_Mode" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <iframe id="ifDash" runat="server" style="width: 100%; border: none; height: 1000px;"></iframe>
            </div>
        </div>
    </div>
</asp:Content>

