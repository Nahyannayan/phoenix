﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib

Partial Class DownDefault
    Inherits System.Web.UI.Page

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Function GetDefSchool(ByVal BSU_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchoolType", pParms)
        Return reader
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Session.Timeout = 5
            HiddenPostBack.Value = 1
            Session("Data") = Nothing
            BindBsu()

            Using DefReader As SqlDataReader = GetDefSchool(Session("sBsuid"))
                While DefReader.Read
                    Session("School_Type") = Convert.ToString(DefReader("BSU_bGEMSSchool"))


                End While
            End Using


        End If

        'If HiddenPostBack.Value = 0 Then
        '    Session.Timeout = 5
        '    HiddenPostBack.Value = 1
        '    Session("Data") = Nothing
        '    BindBsu()
        'End If

    End Sub

    Public Sub BindBsu()
        Try
            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()


            Dim Sql_Query As String = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)
            ddbsu.DataSource = ds.Tables(0)
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
            Dim list As New ListItem
            list.Text = "Select a Business Unit"
            list.Value = "-1"
            ddbsu.items.insert(0, list)

            ddbsu.SelectedValue = Session("sBsuid")

            ddbsu.Enabled = False

        Catch ex As Exception



        End Try


    End Sub
    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupload.Click ' Handles btnupload.Click

        Dim Filter As String = WebConfigurationManager.AppSettings("ExtensionFilters").ToString()
        Dim dir(), d, fl(), f As String
        Dim di As DirectoryInfo
        Dim fi As FileInfo

        Dim formatarray As String() = Filter.Split(";")
        'Dim ExtractPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO"
        Dim ExtractPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString



        'Dir = Directory.GetDirectories(ExtractPath)
        'For Each d In dir
        '    di = New DirectoryInfo(d)
        '    Dim pParms(3) As SqlClient.SqlParameter
        '    pParms(0) = New SqlClient.SqlParameter("@EMP_ID", di.Name)


        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSTU_Photo_Download")
            While reader.Read
                'Dim lstrEMPNO = Convert.ToString(reader("BSU_SHORtNAME")) + "_" + Convert.ToString(reader("EMPNO")) + "_" + Convert.ToString(reader("EMPNAME"))
                Dim lstrEMPNO = Convert.ToString(reader("STU_NO"))
                Dim lstrPathDB = Convert.ToString(reader("EMD_PHOTO"))

                If lstrPathDB <> "" Then
                    'Dim ExtractPath2 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO" & "\" & di.Name
                    ' Dim ExtractPath2 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO" & "\" & di.Name
                    Dim ExtractPath2 As String = "\\172.25.26.20\oasisphotos\OASIS_HR\ApplicantPhoto & lstrPathDB"
                    Dim UploadPath As String = "\\\172.25.26.20\oasisphotos\OASIS_HR\DownloadPath\MHS\" & lstrEMPNO & ".jpg"
                    'fl = Directory.GetFiles(ExtractPath2)
                    'For Each f In fl
                    '    fi = New FileInfo(f)
                    '    'Dim ExtractPath3 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO" & "\" & di.Name & "\" & fi.Name
                    '    'Dim ExtractPath3 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO" & "\" & di.Name & "\" & fi.Name
                    Dim ExtractPath3 As String = "\\\172.25.26.20\oasisphotos\OASIS_HR\ApplicantPhoto " & lstrPathDB
                    File.Copy(ExtractPath3, UploadPath, True)
                End If

                'Next
            End While
        End Using
        'Next


    End Sub

    'Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupload.Click ' Handles btnupload.Click

    '    Dim Filter As String = WebConfigurationManager.AppSettings("ExtensionFilters").ToString()
    '    Dim dir(), d, fl(), f As String
    '    Dim di As DirectoryInfo
    '    Dim fi As FileInfo

    '    Dim formatarray As String() = Filter.Split(";")
    '    'Dim ExtractPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO"
    '    Dim ExtractPath As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO"
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString



    '    'Dir = Directory.GetDirectories(ExtractPath)
    '    'For Each d In dir
    '    '    di = New DirectoryInfo(d)
    '    '    Dim pParms(3) As SqlClient.SqlParameter
    '    '    pParms(0) = New SqlClient.SqlParameter("@EMP_ID", di.Name)


    '    Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetEMP_Photo_Download")
    '        While reader.Read
    '            Dim lstrEMPNO = Convert.ToString(reader("BSU_SHORtNAME")) + "_" + Convert.ToString(reader("EMPNO")) + "_" + Convert.ToString(reader("EMPNAME"))
    '            Dim lstrPathDB = Convert.ToString(reader("EMD_PHOTO"))

    '            If lstrPathDB <> "" Then
    '                'Dim ExtractPath2 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO" & "\" & di.Name
    '                ' Dim ExtractPath2 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO" & "\" & di.Name
    '                Dim ExtractPath2 As String = "\\172.25.26.20\oasisphotos\OASIS_HR\ApplicantPhoto & lstrPathDB"
    '                Dim UploadPath As String = "\\\172.25.26.20\oasisphotos\OASIS_HR\DownloadPath\ICT\" & lstrEMPNO & ".jpg"
    '                'fl = Directory.GetFiles(ExtractPath2)
    '                'For Each f In fl
    '                '    fi = New FileInfo(f)
    '                '    'Dim ExtractPath3 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\EMPPHOTO" & "\" & di.Name & "\" & fi.Name
    '                '    'Dim ExtractPath3 As String = "\\172.16.1.11\oasisphotos\OASIS_HR\ApplicantPhoto\999998\EMPPHOTO" & "\" & di.Name & "\" & fi.Name
    '                Dim ExtractPath3 As String = "\\\172.25.26.20\oasisphotos\OASIS_HR\ApplicantPhoto " & lstrPathDB
    '                File.Copy(ExtractPath3, UploadPath, True)
    '            End If

    '            'Next
    '        End While
    '    End Using
    '    'Next


    'End Sub


End Class
