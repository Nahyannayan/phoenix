﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"  CodeFile="DownDefault.aspx.vb" Inherits="DownDefault" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <b>
        <div align="center" class="matters">
            <img src="Images/GEMSEducationS.jpg" style="height: 92px; width: 110px" />
            <h1>
                Student Image Uploading
            </h1>
            <br />
            Business Unit :
            <asp:DropDownList ID="ddbsu" runat="server">
            </asp:DropDownList>
            &nbsp;  <span style="font-size:smaller "> </span> :
            <asp:FileUpload ID="FileUploadPhoto" runat="server" visible="false"/>
            &nbsp;
            <asp:Button ID="btnupload" runat="server" Text="Convert" CssClass="button" OnClick="btnupload_Click" Width="134px" />
            <br />
            <asp:Label ID="lblmessage" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
            <br />
            <b>
                <asp:Label ID="lblStatus" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
            </b>
            <br />
            <asp:CheckBox ID="CheckError" runat="server" AutoPostBack="True" 
                Text="Show Error List" Visible="False" />
            <br />
            <asp:GridView ID="GridUploaded" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                PageSize="15">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            File Name
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("File_Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Name
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Student_Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Image
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image1" ImageUrl='<%# Eval("Image_View") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Referenced
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Student_Reference") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Student ID
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Student_Id") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Status
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Status") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="LightGray" ForeColor="Black" />
            </asp:GridView>
        </div>
    </b>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select a Business Unit"
        ControlToValidate="ddbsu" Display="None" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Upload a Zip File (Student Image)"
        ControlToValidate="FileUploadPhoto" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
 <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUploadPhoto"
        Display="None" ErrorMessage="Please upload (*.zip) Files" SetFocusOnError="True"
        Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.zip)$"></asp:RegularExpressionValidator>--%>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
    <asp:HiddenField ID="HiddenPostBack" Value="0" runat="server" />
</asp:Content> 
