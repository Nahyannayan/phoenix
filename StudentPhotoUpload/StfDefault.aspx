﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"  CodeFile="StfDefault.aspx.vb" Inherits="_Default" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Staff Image Uploading
        </div>
        <div class="card-body">
            <div class="table-responsive">

        <div align="center">
            <img src="Images/GEMSEducationS.jpg" style="height: 92px; width: 110px" />
            <h1>
                Staff Image Uploading
            </h1>
            <table width="100%">

                <tr>
                    <td align="left" width="20%">
                        <span class="field-label">Business Unit</span>
                    </td>
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddbsu" runat="server"></asp:DropDownList>
                    </td>
                    <td align="left" width="20%">
                        <span class="field-label">Upload Zip File (*.zip) <span class="font-small"> (less than 2 mb)</span></span>
                    </td>
                    <td align="left" width="30%">
                        <asp:FileUpload ID="FileUploadPhoto" runat="server"  />
                    </td>
                    
                </tr>

                <tr>                    
                    <td align="center" colspan="4"> <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="button" OnClick="btnupload_Click" Width="134px" ValidationGroup="M1" /></td>
                    
                </tr>
            </table>
            
            
            <asp:Label ID="lblmessage" runat="server" Font-Bold="True" CssClass="text-danger"></asp:Label>
            <br />
            <b>
                <asp:Label ID="lblStatus" runat="server" Font-Bold="True" CssClass="text-danger"></asp:Label>
            </b>
            <br />
            <asp:CheckBox ID="CheckError" runat="server" AutoPostBack="True" 
                Text="Show Error List" Visible="False" />
            <br />
            <asp:GridView ID="GridUploaded" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                PageSize="15" Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            File Name
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("File_Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Name
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("Staff_Name")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Image
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image1" ImageUrl='<%# Eval("Image_View") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Referenced
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("Staff_Reference")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Student ID
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("Staff_Id")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Status
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("Status") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="LightGray" />
            </asp:GridView>
        </div>
    </div>
        </div>
    </div>

    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select a BSU."
        ControlToValidate="ddbsu" Display="None" SetFocusOnError="True"  ValidationGroup="M1"
        InitialValue="-1"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Upload a Zip File (Student Image)"
        ControlToValidate="FileUploadPhoto" Display="None" SetFocusOnError="True" ValidationGroup="M1"></asp:RequiredFieldValidator>
 <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUploadPhoto"
        Display="None" ErrorMessage="Please upload (*.zip) Files" SetFocusOnError="True"
        Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.zip)$"></asp:RegularExpressionValidator>--%>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
    <asp:HiddenField ID="HiddenPostBack" Value="0" runat="server" />
</asp:Content> 
