﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Session.Timeout = 5
            HiddenPostBack.Value = 1
            Session("Data") = Nothing
            BindBsu()
        End If

    End Sub

    Public Sub BindBsu()
        Try
            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()


            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
     CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits_Menu] " _
     & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
            'Dim Sql_Query As String = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"
            'Dim ds As DataSet
            'ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)

            ddbsu.DataSource = ds.Tables(0)
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
            Dim list As New ListItem
            list.Text = "Select a Business Unit"
            list.Value = "-1"
            ddbsu.items.insert(0, list)

            ddbsu.SelectedValue = Session("sBsuid")



        Catch ex As Exception



        End Try


    End Sub

    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles btnupload.Click
        lblmessage.Text = ""
        lblStatus.Text = ""
        Try
            Dim ExtractPath As String = WebConfigurationManager.AppSettings("ExtractPath").ToString()


            Dim DirectoryName As String = "EmpPhotoPath _" & System.Guid.NewGuid().ToString()

            If Not Directory.Exists(ExtractPath & "\" & DirectoryName) Then

                Directory.CreateDirectory(ExtractPath & "\" & DirectoryName)

                Dim FileName As String = FileUploadPhoto.PostedFile.FileName
                Dim filen As String()
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)
                If FileName.Split(".")(1) = "zip" Then
                    FileUploadPhoto.SaveAs(ExtractPath & "\" & DirectoryName & "\" + FileName)

                    Dim c As New Zip.FastZip
                    c.ExtractZip(ExtractPath & "\" & DirectoryName & "\" + FileName, ExtractPath & "\" & DirectoryName & "\", String.Empty)
                    Generate(ExtractPath & "\" & DirectoryName)

                    '' Delete
                    Directory.Delete(ExtractPath & "\" & DirectoryName, True)
                Else
                    GridUploaded.Controls.Clear()
                    lblmessage.Text = "Please upload Zip File"

                End If


            Else
                lblmessage.Text = "Please Try Again"

            End If
        Catch ex As Exception
            lblmessage.Text = "Error : " & ex.Message
        End Try


    End Sub


    Public Sub Generate(ByVal FolderPath As String)
        Session("Data") = Nothing
        CheckError.Checked = False

        Dim VirtualPath As String = WebConfigurationManager.AppSettings("VirtualPath").ToString()
        Dim Filter As String = WebConfigurationManager.AppSettings("ExtensionFilters").ToString()

        Dim formatarray As String() = Filter.Split(";")
        Dim TotalFiles As Int16 = 0
        Dim VisibleFlag As Int16 = 0
        Dim ErrorFlag As Int16 = 0

        Dim dt As New DataTable
        dt.Columns.Add("File_Name")
        dt.Columns.Add("Image_View")
        dt.Columns.Add("Staff_Reference")
        dt.Columns.Add("Staff_Id")
        dt.Columns.Add("Status")
        dt.Columns.Add("Staff_Name")
        dt.Columns.Add("Error_Flag")

        For Each FileFormat As String In formatarray

            Dim FilePath As String() = Directory.GetFiles(FolderPath, FileFormat, SearchOption.AllDirectories)


            For Each File As String In FilePath

                TotalFiles = TotalFiles + 1
                VisibleFlag = 1
                Dim filename As New FileInfo(File.ToString())

                Dim referenceno As String

                referenceno = GetData(filename.Name.Split(".")(0))

                Dim dr As DataRow = dt.NewRow()

                If referenceno.IndexOf("_") > 0 Then
                    Dim filen As String()
                    filen = File.Split("\")
                    Dim filenameDisplay As String = filen(filen.Length - 1)

                    dr.Item("File_Name") = filenameDisplay

                    dr.Item("Staff_Reference") = referenceno.Split("_")(0) & " : " & referenceno.Split("_")(2)
                    dr.Item("Staff_Id") = referenceno.Split("_")(1)
                    dr.Item("Image_View") = VirtualPath & "EMPPHOTO/" & referenceno.Split("_")(1) & "/" & "EMPPHOTO." & filenameDisplay.Split(".")(1)
                    dr.Item("Status") = CreateFile(filename.FullName, referenceno.Split("_")(1), filename.Name.Split(".")(1))
                    dr.Item("Staff_Name") = GetStaffName(referenceno.Split("_")(1))
                    dr.Item("Error_Flag") = 0
                Else

                    ErrorFlag = ErrorFlag + 1
                    Dim filen As String()
                    filen = File.Split("\")
                    Dim filenameDisplay As String = filen(filen.Length - 1)

                    dr.Item("File_Name") = filenameDisplay
                    dr.Item("Image_View") = ""
                    dr.Item("Staff_Reference") = referenceno
                    dr.Item("Staff_Id") = referenceno
                    dr.Item("Status") = "File not created.Please check the user inputs."
                    dr.Item("Staff_Name") = ""
                    dr.Item("Error_Flag") = 1
                    CheckError.Visible = True
                End If

                dt.Rows.Add(dr)



            Next


        Next
        ''Datatable in session
        Session("Data") = dt

        GridUploaded.DataSource = dt
        GridUploaded.DataBind()
        lblStatus.Text = "Total Files : " & TotalFiles & "<br>Error in Uploading : " & ErrorFlag
    End Sub

    Public Function GetStaffName(ByVal Staff_id As String) As String
        Dim returnvalue As String = ""
        Try

            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()

            Dim Sql_Query As String = "SELECT ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') EMP_NAME FROM  dbo.EMPLOYEE_M WHERE EMP_ID='" & Staff_id & "'"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = ds.Tables(0).Rows(0).Item("EMP_NAME").ToString()
            End If
        Catch ex As Exception
            returnvalue = "Error : " & ex.Message
        End Try



        Return returnvalue
    End Function


    Public Function GetData(ByVal id As String) As String

        Dim returnvalue As String = ""

        Try

            '' check for fee id for given id

            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()
            Dim Sql_Query As String = ""
            Dim ds As New DataSet


            Sql_Query = " SELECT EMP_ID FROM EMPLOYEE_M A " & _
                        " INNER JOIN dbo.EMPLOYEE_D B ON A.EMP_ID=B.EMD_EMP_ID " & _
                        " WHERE CONVERT(VARCHAR,EMPNO)='" & id & "'"

            ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)

            If ds.Tables(0).Rows.Count = 0 Then

                If ddbsu.SelectedIndex > 0 Then

                    Sql_Query = " SELECT EMP_ID FROM EMPLOYEE_M A " & _
                                " INNER JOIN dbo.EMPLOYEE_D B ON A.EMP_ID=B.EMD_EMP_ID " & _
                                " WHERE CONVERT(VARCHAR,EMP_STAFFNO)='" & id & "' AND EMP_BSU_ID='" & ddbsu.SelectedValue & "'"
                    ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)
                End If

            End If


            '' data reference fee id
            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = "Employee No_" & ds.Tables(0).Rows(0).Item("EMP_ID").ToString() & "_" & id
            Else
                returnvalue = "No Data Found. Please check the uploaded file name."
            End If
        Catch ex As Exception
            returnvalue = "Error : " & ex.Message
        End Try

        Return returnvalue

    End Function


#Region "Create"
    Public Function CreateFile(ByVal OriginalPath As String, ByVal staff_id As String, ByVal extention As String) As String
        Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()

        Dim status As String = ""
        Dim UploadPath As String = WebConfigurationManager.AppSettings("UploadPath").ToString()

        Try
            If Directory.Exists(UploadPath) Then
                Directory.CreateDirectory(UploadPath & "/EMPPHOTO/" & staff_id)

                'File.Copy(OriginalPath, UploadPath & "/EMPPHOTO/" & staff_id & "/" & "EMPPHOTO." & extention, True)


                Dim path As String = "/EMPPHOTO/" & staff_id & "/" & "EMPPHOTO." & extention


                '' Check for image size and reduce if large size and save.
                Dim FError = False
                SizeReduce(OriginalPath, UploadPath & path, FError)

                If FError = False Then
                    '' Update the database
                    Dim sql_query As String = "UPDATE dbo.EMPLOYEE_D  SET EMD_PHOTO='" & path & "' WHERE EMD_EMP_ID='" & staff_id & "'"

                    SqlHelper.ExecuteNonQuery(sql_Connection, CommandType.Text, sql_query)

                    ''Keep an audit track
                    Dim pParms(4) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@EMP_BSU_ID", ddbsu.SelectedValue)
                    pParms(1) = New SqlClient.SqlParameter("@EMD_EMP_ID", staff_id)
                    pParms(2) = New SqlClient.SqlParameter("@User", Session("EmployeeId"))

                    SqlHelper.ExecuteNonQuery(sql_Connection, CommandType.StoredProcedure, "STAFF_PHOTO_UPLOAD_STAFF", pParms)

                    status = "Successfully Uploaded"

                Else

                    status = "Error while image file size reduction."

                End If

            Else
                status = "Destination folder does not exists"
            End If
        Catch ex As Exception

            status = ex.Message

        End Try


        Return status

    End Function

#End Region

    
    Protected Sub GridUploaded_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridUploaded.PageIndexChanging
        GridUploaded.PageIndex = e.NewPageIndex
        Dim dt As New DataTable
        dt = Session("Data")
        If dt.Rows.Count > 0 Then
            BindClientGrid(dt)
        End If

    End Sub


    Public Sub BindClientGrid(ByVal dt As DataTable)


        Dim tempdt As New DataTable

        tempdt = dt
        Dim i As Integer = 0

        For i = dt.Rows.Count - 1 To 0 Step -1

            Dim dr As DataRow = tempdt.Rows(i)
            If CheckError.Checked Then
                If tempdt.Rows(i).Item("Error_Flag").ToString() = "0" Then
                    'tempdt.Rows.Remove(dr)
                    'tempdt.Rows(i).Delete()
                End If
            End If

        Next

        GridUploaded.DataSource = tempdt
        GridUploaded.DataBind()


    End Sub


    Protected Sub CheckError_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckError.CheckedChanged
        Dim dt As New DataTable
        dt = Session("Data")

        If dt.Rows.Count > 0 Then
            If CheckError.Checked Then
                BindClientGrid(dt)

            Else

                GridUploaded.DataSource = dt
                GridUploaded.DataBind()

            End If
        End If


    End Sub


    Public Sub SizeReduce(ByVal FromFilename As String, ByVal ToFileName As String, ByRef FError As Boolean)
        ''To call this function
        Try
            Dim src = FromFilename
            Dim bigImage As System.Drawing.Image = System.Drawing.Image.FromFile(src)

            Dim filedata As New System.IO.FileInfo(FromFilename)

            Dim height As Integer = (bigImage.Height)
            Dim width As Integer = (bigImage.Width)

            If filedata.Length > 50000 Then
                height = (bigImage.Height) / 2
                width = (bigImage.Width) / 2
            End If
            ImageResize(bigImage, height, width, FError, ToFileName)
            bigImage.Dispose()
        Catch ex As Exception
            FError = True
        End Try

    End Sub

    Public Shared Sub ImageResize(ByVal image As System.Drawing.Image, ByVal height As Int32, ByVal width As Int32, ByRef FError As Boolean, ByVal ToFileName As String)

        Try
            Dim bitmap As System.Drawing.Bitmap = New System.Drawing.Bitmap(width, height, image.PixelFormat)
            If bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format1bppIndexed Or _
                bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format4bppIndexed Or _
                bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format8bppIndexed Or _
                bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Undefined Or _
                bitmap.PixelFormat = Drawing.Imaging.PixelFormat.DontCare Or _
                bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppArgb1555 Or _
                bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppGrayScale Then
                Throw New NotSupportedException("Pixel format of the image is not supported.")
            End If
            Dim graphicsImage As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(bitmap)
            graphicsImage.SmoothingMode = Drawing.Drawing2D.SmoothingMode.HighQuality
            graphicsImage.InterpolationMode = Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
            graphicsImage.CompositingQuality = Drawing.Drawing2D.CompositingQuality.Invalid
            graphicsImage.DrawImage(image, 0, 0, bitmap.Width, bitmap.Height)
            bitmap.Save(ToFileName, System.Drawing.Imaging.ImageFormat.Jpeg)
            'graphicsImage.Save()
            image.Dispose()
            graphicsImage.Dispose()

        Catch ex As Exception
            FError = True
        End Try


    End Sub


End Class
