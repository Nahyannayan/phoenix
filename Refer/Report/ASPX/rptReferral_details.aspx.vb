﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Data.SqlTypes
Imports System.Data
Imports System.Data.SqlClient
Partial Class Refer_Report_ASPX_rptReferral_details
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "HD02010") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights


                getBsuTransfer_List()
                txtFromDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now.AddMonths(-1))
                txtToDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub

    Private Sub getBsuTransfer_List()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        ' Dim str_Sql As String = "select bsu_id,bsu_name from businessunit_m where  ISNULL(BSU_bREFERRAL,0)=1 order by bsu_name"
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter
        ''changed and included condiotion by nahyan on 8mar2017 - #57396
        If Session("sBsuid") = "999998" Then
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REF.GETREFERRAL_SCHOOLS")
        Else
            param(0) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GetUserBusinessUnitsForReferel", param)
        End If

        chkSch_list.DataSource = ds
        chkSch_list.DataTextField = "bsu_name"
        chkSch_list.DataValueField = "bsu_id"
        chkSch_list.DataBind()


        If Session("sBsuid") = "999998" Then
            For Each item As ListItem In chkSch_list.Items
                item.Selected = True
            Next
        Else
            For Each item As ListItem In chkSch_list.Items

                If item.Value = Session("sBsuid") Then
                    item.Selected = True
                End If
            Next
        End If
       

       

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDT.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDT.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDT.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDT.Text.Trim <> "" Then

                Dim strfDate As String = txtToDT.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDT.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDT.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            Else
                CommStr = CommStr & "<li>To Date required"

            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN THE DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            If ValidateDate() <> "-1" Then
                CallReport()
            End If

        End If
    End Sub
    Sub CallReport()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim FROMDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDT.Text) '"25/NOV/2008" '
        Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDT.Text) '"25/NOV/2008" '
        Dim bsu_ids As New StringBuilder


        For Each item As ListItem In chkSch_list.Items
            If item.Selected = True Then
                bsu_ids.Append(item.Value & "|")
            End If
        Next
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@FROMDT", FROMDT)
        param.Add("@TODT", TODT)
        If bsu_ids.ToString <> "" Then
            param.Add("@BSU_IDS", bsu_ids.ToString)
        Else
            ''addded by nahyan on 8 mar2017 #57396

            If Session("sBsuid") = "999998" Then
                param.Add("@BSU_IDS", System.DBNull.Value)
            Else
                For Each item As ListItem In chkSch_list.Items
                    bsu_ids.Append(item.Value & "|")
                Next
                param.Add("@BSU_IDS", bsu_ids.ToString)
            End If

        End If
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("FROMDT", FROMDT)
        param.Add("TODT", TODT)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("/REFER/Report/RPT/rptRef_Details.rpt")

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
