﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Refer_Report_ASPX_rptReferralHistory
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim USR_NAME As String = String.Empty
                ViewState("RFM_ID") = ""
                If Not Session("username") Is Nothing Then
                    USR_NAME = Session("username")
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'collect the url of the file to be redirected in view state
                If (ViewState("MainMnu_code") <> "HD12005") Then


                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If

                Else


                    CallReport()



                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If


    End Sub

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim bsuids As String = String.Empty
        bsuids = getBsuTransfer_List()
        param.Add("@Bsu_id", bsuids)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("/REFER/Report/RPT/rptRef_History.rpt")
          
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Function getBsuTransfer_List() As String
        Dim bsuList As String = String.Empty
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        ' Dim str_Sql As String = "select bsu_id,bsu_name from businessunit_m where  ISNULL(BSU_bREFERRAL,0)=1 order by bsu_name"
        Dim ds As DataSet
        Dim dt As DataTable
        Dim param(1) As SqlClient.SqlParameter
        ''changed and included condiotion by nahyan on 8mar2017 - #57396
        If Session("sBsuid") = "999998" Then
            bsuList = ""
        Else
            param(0) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GetUserBusinessUnitsForReferel", param)

            If (ds IsNot Nothing) Then
                If (ds.Tables(0) IsNot Nothing) Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        dt = ds.Tables(0)
                        For Each row As DataRow In dt.Rows
                            bsuList += row.Item("bsu_id") & "|"
                        Next row
                    End If
                End If
            End If
        End If

        Return bsuList
    End Function
End Class
