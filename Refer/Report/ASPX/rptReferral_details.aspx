﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptReferral_details.aspx.vb" Inherits="Refer_Report_ASPX_rptReferral_details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Referral Details"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" style="width: 100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    <td align="left" width="30%"  style="text-align: left">
                                        <asp:TextBox ID="txtFromDT" runat="server"></asp:TextBox><span style="font-size: 7pt">
                                        </span>
                                        <asp:ImageButton ID="imgFROMDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDT"
                                            CssClass="error" Display="Dynamic" ErrorMessage="From  Date required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                    <td align="left" width="30%"  style="text-align: left">
                                        <asp:TextBox ID="txtToDT" runat="server"></asp:TextBox><span style="font-size: 7pt">
                                        </span>
                                        <asp:ImageButton ID="imgtoDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtToDT"
                                            CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <br /></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">School</span>
                           <%-- <i style="display:block;color:Maroon;font-size:9px;">By default All is selected</i>--%>

                                    </td>
                                    <td align="left"  style="text-align: left" colspan="2">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="chkSch_list" runat="server" BorderStyle="None" BorderWidth="0px" Css Font-Size="11px" Font-Bold="true">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" Width="160px" /></td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CBEfdate"
                                runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFROMDate" TargetControlID="txtFromDT">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgtoDate"
                                TargetControlID="txtToDT">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

