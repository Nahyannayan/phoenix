﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections

Partial Class Referral_ReferralSchools
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim temp_ID As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            plTermCond.Visible = False

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If


            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'collect the url of the file to be redirected in view state
            If (ViewState("MainMnu_code") <> "HD01003") Then
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

            Else

                gridbind_Parent()

            End If

        End If
    End Sub
    Private Sub gridbind_Parent()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = String.Empty
            Dim STR_FILTER_CATEGORY_DES As String = String.Empty
            Dim str_filter_BName As String = String.Empty
            Dim param(3) As SqlParameter

            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[REF].[GETREFERRAL_SCHOOL_LIST]")


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'ds.Tables(0).Rows(0)(8) = True
      
                gvManageUser.DataSource = ds.Tables(0)
                gvManageUser.DataBind()
                Dim columnCount As Integer = gvManageUser.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvManageUser.Rows(0).Cells.Clear()
                gvManageUser.Rows(0).Cells.Add(New TableCell)
                gvManageUser.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUser.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUser.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUser.DataSource = ds.Tables(0)
                gvManageUser.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvManageUser_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvManageUser.RowCommand

        Dim ARG As String = e.CommandArgument

        If e.CommandName = "Ref_Par" Then
            changeStatus(ARG, "Ref_Par")

        ElseIf e.CommandName = "Ref_Pub" Then
            changeStatus(ARG, "Ref_Pub")
        ElseIf e.CommandName = "Terms" Then
            plTermCond.Visible = True
            GetEditSchTerm_Cond(ARG)
        End If

    End Sub

    Private Sub GetEditSchTerm_Cond(ByVal bsu_id As String)
        ddlSchoolEdit.Items.Clear()

        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", bsu_id)
        pParms(1) = New SqlClient.SqlParameter("@ForEdit", True)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "REF.GETSchTerms_Cond", pParms)
            If datareader.HasRows = True Then
                While datareader.Read
                    ddlSchoolEdit.Items.Add(New ListItem(datareader("BSU_name"), datareader("BSU_id")))
                    txtTermsText.Content = Convert.ToString(datareader("BSU_TERM"))
                End While
            Else
                txtTermsText.Content = ""

            End If
        End Using
        ddlSchoolEdit_SelectedIndexChanged(ddlSchoolEdit, Nothing)
    End Sub
    Protected Sub ddlSchoolEdit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSchoolEdit.SelectedIndexChanged
        getCopySchTerm_cond(ddlSchoolEdit.SelectedValue)
    End Sub


    Private Sub getCopySchTerm_cond(ByVal bsu_id As String)
        chklistSchool.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", bsu_id)
        pParms(1) = New SqlClient.SqlParameter("@ForEdit", False)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "REF.GETSchTerms_Cond", pParms)
            If datareader.HasRows = True Then
                chklistSchool.DataSource = datareader
                chklistSchool.DataTextField = "BSU_NAME"
                chklistSchool.DataValueField = "BSU_ID"
                chklistSchool.DataBind()
            End If
        End Using
    End Sub

    Sub changeStatus(ByVal BSU_ID As String, ByVal FLAG As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            pParms(1) = New SqlClient.SqlParameter("@FLAG", FLAG)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[REF].[SAVEREFERRAL_SCHOOL_LIST_STATUS]", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            If ReturnFlag <> 0 Then
                lblError.Text = "Error Occured While Saving."
            Else
                gridbind_Parent()
                lblError.Text = "Record updated Successfully !!!"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "changeStatus")
        End Try

    End Sub


    Protected Sub imgtermsClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgtermsClose.Click
        plTermCond.Visible = False
        gridbind_Parent()
    End Sub

    Protected Sub btnCloseTerm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseTerm.Click
        plTermCond.Visible = False
        gridbind_Parent()

    End Sub

    Protected Sub btnUpdateTerm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateTerm.Click
        Dim strErr As Integer
        Dim errorMessage As String = String.Empty
        strErr = CallTransaction_4TermsCond(errorMessage)
        If strErr = "0" Then


            lblPOPUPMsg.Text = "Record Saved Successfully"


        Else
            lblPOPUPMsg.Text = errorMessage
        End If
    End Sub
    Private Function CallTransaction_4TermsCond(ByRef errorMessage As String) As Integer
        Dim status As Integer

        Dim BSU_IDS As String
        For Each item As ListItem In chklistSchool.Items
            If item.Selected = True Then
                BSU_IDS += item.Value + "|"
            End If
        Next
        Dim tran As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            tran = conn.BeginTransaction("SampleTransaction")
            Try

                Dim param(4) As SqlParameter
                param(0) = New SqlParameter("@BSU_ID", ddlSchoolEdit.SelectedValue)
                param(1) = New SqlParameter("@BSU_IDS", BSU_IDS)
                param(2) = New SqlParameter("@CONTENT", txtTermsText.Content)
                param(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                param(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "REF.SAVESchTerms_Cond", param)

                status = param(3).Value
                CallTransaction_4TermsCond = 0
                If status <> 0 Then
                    CallTransaction_4TermsCond = 1
                    Return "1"
                End If

            Catch ex As Exception
                CallTransaction_4TermsCond = 1
                errorMessage = "Error Occured While Saving."
            Finally
                If CallTransaction_4TermsCond <> 0 Then
                    tran.Rollback()
                Else
                    errorMessage = ""
                    tran.Commit()
                End If
            End Try
        End Using


    End Function
End Class
