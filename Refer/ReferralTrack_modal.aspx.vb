﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class ReferralTrack_modal
    Inherits BasePage
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Dim tot_amt As String
    Dim Yes_no As String
    Dim FEE_PAY_DT As String
    Dim Fee_TYPE As String
    Dim FEE_AMT_PAID As String
    Dim CURR_STATUS As String
    Dim row_flag As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CalendarExtender1.ID = DateTime.Now.ToString()

        If Page.IsPostBack = False Then
            Try


           
                ltPay_dt.Text = "Pay Date<font color='maroon'>*</font> &nbsp;&nbsp;:"
                txtRemarks.Text = ""
                txtPay_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                ltGRemark.Text = ""
                txtRemarks.Visible = True
                btnAppr.Visible = True
                btnRej.Visible = True
                btnAppr.Enabled = True
                btnRej.Enabled = True
                trDT.Visible = False


                ViewState("RFD_ID") = Request.QueryString("RFD_ID")
                'get the menucode to confirm the user is accessing the valid page
                ViewState("STU_ID") = Request.QueryString("STU_ID")

                ViewState("Type") = Request.QueryString("TYP")
                ViewState("Status") = Request.QueryString("R_STATUS")




                If ViewState("Type") = "Reg" Then



                    If ViewState("Status") = "Approved" Then
                        btnAppr.Visible = False
                        btnRej.Visible = False
                        txtRemarks.Visible = False

                        trRM.Visible = False
                    ElseIf ViewState("Status") = "Rejected" Then
                        btnAppr.Visible = False
                        btnRej.Visible = False
                        txtRemarks.Visible = False

                        trRM.Visible = False
                    ElseIf ViewState("Status") = "Pending" Then
                        btnAppr.Visible = True
                        btnRej.Visible = True
                        txtRemarks.Visible = True
                        ltGRemark.Text = "Remarks<font color='maroon'>*</font> &nbsp;&nbsp;:"

                        trRM.Visible = True
                    End If

                ElseIf ViewState("Type") = "Acc" Then
                    If ViewState("Status") = "Approved" Then
                        btnAppr.Visible = False
                        btnRej.Visible = False
                        txtRemarks.Visible = False
                        ltGRemark.Text = ""
                        trRM.Visible = False

                    ElseIf ViewState("Status") = "Rejected" Then
                        btnAppr.Visible = False
                        btnRej.Visible = False
                        txtRemarks.Visible = False
                        ltGRemark.Text = ""
                        trRM.Visible = False

                    ElseIf ViewState("Status") = "Pending" Then
                        btnAppr.Visible = True
                        btnRej.Visible = True
                        txtRemarks.Visible = True
                        ltGRemark.Text = "Remarks<font color='maroon'>*</font> &nbsp;&nbsp;:"
                        trRM.Visible = True

                    End If
                ElseIf ViewState("Type") = "Paid" Then
                    btnAppr.Text = "Pay"
                    btnAppr.Visible = True
                    btnRej.Visible = False
                    btnAppr.Enabled = True
                    btnRej.Enabled = False
                    If ViewState("Status") = "Rejected" Then
                        btnAppr.Visible = False
                        btnRej.Visible = False
                        txtRemarks.Visible = False
                        ltGRemark.Text = ""

                        trDT.Visible = False
                        trRM.Visible = False
                    ElseIf ViewState("Status") = "Pending" Then
                        btnAppr.Visible = True
                        btnRej.Visible = False
                        txtRemarks.Visible = True
                        ltGRemark.Text = "Remarks<font color='maroon'>*</font> &nbsp;&nbsp;:"
                        trDT.Visible = True
                        trRM.Visible = True
                    Else
                        btnAppr.Visible = False
                        btnRej.Visible = False
                        txtRemarks.Visible = False
                        ltGRemark.Text = "Remarks<font color='maroon'>*</font> &nbsp;&nbsp;:"
                        trDT.Visible = False
                        trRM.Visible = False

                    End If
                End If



                bind_stuInfo()
                bind_PayInfo()
                bind_Tracking(ViewState("STU_ID"))

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub
    Sub bind_Tracking(ByVal STU_ID As String)
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[REF].[GET_STUDENTREF_SHORTLIST]", param)


            gvtrack.DataSource = ds.Tables(0)
            gvtrack.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Sub

    Sub bind_stuInfo()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@RFD_ID", ViewState("RFD_ID"))

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "REF.GETSTUDENT_REF_INFO", param)

            If datareader.HasRows = True Then

                While datareader.Read
                    ltSName.Text = Convert.ToString(datareader("SNAME"))

                    ltAtt_count.Text = "<font color='black'> " & Convert.ToString(datareader("Att_count")) & "</font>"
                    ltStu_no.Text = "<font color='black'> " & Convert.ToString(datareader("STU_NO")) & "</font>"
                    ltDOJ.Text = "<font color='black'> " & Convert.ToString(datareader("DOJ")) & "</font>"
                    ltGrade.Text = "<font color='black'> " & Convert.ToString(datareader("GRM_DISPLAY")) & "</font>"
                    ltSection.Text = "<font color='black'> " & Convert.ToString(datareader("SCT_DESCR")) & "</font>"
                    ltACY_DESCR.Text = "<font color='black'> " & Convert.ToString(datareader("ACY_DESCR")) & "</font>"
                    ltLastDT.Text = "<font color='black'> " & Convert.ToString(datareader("STU_LASTATTDATE")) & "</font>"

                    ltENQ_DT.Text = "<font color='black'> " & Convert.ToString(datareader("EQM_ENQDATE")) & "</font>"
                    ltREG_DT.Text = "<font color='black'> " & Convert.ToString(datareader("EQS_REGNDATE")) & "</font>"
                    ltRef_On.Text = "<font color='black'> " & Convert.ToString(datareader("RFS_DT")) & "</font>"



                    ltStatus.Text = "<font color='black'> " & Convert.ToString(datareader("STU_CURRSTATUS")) & "</font>"
                    ltFname.Text = "<font color='black'> " & Convert.ToString(datareader("FATHER")) & "</font>"
                    ltMname.Text = "<font color='black'> " & Convert.ToString(datareader("MOTHER")) & "</font>"
                    ltPri_cont.Text = "<font color='black'> " & Convert.ToString(datareader("STU_PRIMARYCONTACT")) & "</font>"
                    ltMobNo.Text = "<font color='black'> " & Convert.ToString(datareader("MOBILE")) & "</font>"
                    ltEmail.Text = "<font color='black'> " & Convert.ToString(datareader("EMAIL")) & "</font>"
                    ltRefBy.Text = "<font color='black'> " & Convert.ToString(datareader("REF_BY")) & "</font>"
                    ltRef_mob.Text = "<font color='black'> " & Convert.ToString(datareader("REF_MOB")) & "</font>"
                    ltRef_email.Text = "<font color='black'> " & Convert.ToString(datareader("REF_EMAIL")) & "</font>"

                    ltSib_info.Text = "<font color='black'> " & Convert.ToString(datareader("SIB_INFO")).Replace("&lt;", "<").Replace("&gt;", ">") & "</font>"
                    CURR_STATUS = Convert.ToString(datareader("CURR_STATUS"))

                    ViewState("STP_ACD_ID") = Convert.ToString(datareader("STP_ACD_ID"))
                End While
            End If
        End Using

    End Sub



    Sub bind_PayInfo()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@RFD_ID", ViewState("RFD_ID"))
            param(1) = New SqlClient.SqlParameter("@REF_STU_ID", ViewState("STU_ID"))
            param(2) = New SqlClient.SqlParameter("@ACD_ID", ViewState("STP_ACD_ID"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REF.GETREFERRAL_PAY", param)
            gvSchool.DataSource = ds
            gvSchool.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_PayInfo")
        End Try
    End Sub

    Protected Sub gvSchool_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSchool.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            tot_amt = DirectCast(e.Row.FindControl("lbltot_amt"), Label).Text
            Yes_no = DirectCast(e.Row.FindControl("lblfee_paid"), Label).Text

            Fee_TYPE = DirectCast(e.Row.FindControl("lblFee_TYPE"), Label).Text
            FEE_AMT_PAID = DirectCast(e.Row.FindControl("lblFEE_AMT_PAID"), Label).Text
            FEE_PAY_DT = DirectCast(e.Row.FindControl("lblFEE_PAY_DT"), Label).Text

            Dim grp_count As Integer = CInt(DirectCast(e.Row.FindControl("lblCount"), Label).Text)
            Dim imgLock As Image = DirectCast(e.Row.FindControl("imgLock"), Image)
            Dim grp As String = DirectCast(e.Row.FindControl("lblGRP"), Label).Text
            Dim fee_msg As String = String.Empty


            If Yes_no <> "Yes" Then
                fee_msg = "First Term Fee is pending !!!"
            End If
            divError.InnerHtml = ""
            If Yes_no <> "Yes" Or CURR_STATUS <> "" Then
                If ViewState("Type") = "Acc" Then
                    btnAppr.Enabled = False
                End If

                divmsg.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'><div width='80%' align='left'>" & CURR_STATUS & "</div><div> " & fee_msg & "</div></div>"
            Else
                divmsg.InnerHtml = ""
                btnAppr.Enabled = True
            End If


            If grp_count > 1 Then
                row_flag = True
                If grp = "group" Then
                    imgLock.Visible = True

                Else
                    imgLock.Visible = False
                End If
            Else
                imgLock.Visible = False
                row_flag = False
                ' e.Row.Cells(1).Visible = False
            End If

        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            e.Row.Cells(1).ColumnSpan = 4
            e.Row.Cells(1).HorizontalAlign = HorizontalAlign.Left


            If ViewState("Type") <> "Reg" Then
                If Fee_TYPE = "Monthly" Then
                    e.Row.Cells(1).Text = "<table  style='border-width:1px;border-collapse:collapse;border-style:groove;font-size:10px;font-family:Verdana Arial sans-serif' width='100%' cellpadding='2'    cellspacing='1' ><tr class='gridheader_pop'><td colspan='2'>Other info</td></tr><tr><td align='left' >Annual Fee Amount</td><td align='left' style='color:Black' >" & tot_amt & _
                             "</td></tr><tr><td align='left'  >First Month Fee Amount</td><td align='left' style='color:Black'> " & FEE_AMT_PAID & "</td></tr> " & _
                              "<tr><td align='left'  >First Month Fee paid</td><td align='left' style='color:Black'> " & Yes_no & "</td></tr></table>"
                Else
                    e.Row.Cells(1).Text = "<table   style='border-width:1px;border-collapse:collapse;border-style:groove;font-size:10px;font-family:Verdana Arial sans-serif' width='100%' cellpadding='2'    cellspacing='1' ><tr class='gridheader_pop'><td colspan='2'>Other info</td></tr><tr><td align='left' >Annual Fee Amount</td><td align='left' style='color:Black' >" & tot_amt & _
                              "</td></tr><tr><td align='left'  >First Term Fee Amount</td><td align='left' style='color:Black'> " & FEE_AMT_PAID & "</td></tr> " & _
                               "<tr><td align='left'  >First Term Fee paid</td><td align='left' style='color:Black'> " & Yes_no & "</td></tr></table>"
                End If

            Else

                If Fee_TYPE = "Monthly" Then
                    e.Row.Cells(1).Text = "<table   style='border-width:1px;border-collapse:collapse;border-style:groove;font-size:10px;font-family:Verdana Arial sans-serif' width='100%' cellpadding='2'    cellspacing='1' ><tr class='gridheader_pop'><td colspan='2'>Other info</td></tr><tr><td align='left' >Annual Fee Amount</td><td align='left' style='color:Black' >" & tot_amt & _
                                         "</td></tr><tr><td align='left'  >First Month Fee paid</td><td align='left' style='color:Black'> " & Yes_no & "</td></tr></table>"
                Else
                    e.Row.Cells(1).Text = "<table   style='border-width:1px;border-collapse:collapse;border-style:groove;font-size:10px;font-family:Verdana Arial sans-serif' width='100%' cellpadding='2'    cellspacing='1' ><tr class='gridheader_pop'><td colspan='2'>Other info</td></tr><tr><td align='left' >Annual Fee Amount</td><td align='left' style='color:Black' >" & tot_amt & _
                                               "</td></tr><tr><td align='left'  >First Term Fee paid</td><td align='left' style='color:Black'> " & Yes_no & "</td></tr></table>"
                End If
            End If



            e.Row.Cells(2).Visible = False
            e.Row.Cells(3).Visible = False
            e.Row.Cells(4).Visible = False
            'End If


        End If
    End Sub

    Protected Sub btnRej_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRej.Click

        If txtRemarks.Text.Replace(",", "") = "" Then
            divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Remarks cannot be left empty</div>"
            txtRemarks.Text = txtRemarks.Text.Replace(",", "")
        Else
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty


            str_err = calltransaction("Rejected", errorMessage)
            If str_err = "0" Then

                divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Record Updated  Successfully</div>"


                txtRemarks.Text = txtRemarks.Text.Replace(",", "")

                ltGRemark.Text = ""
                txtRemarks.Visible = False
                btnAppr.Visible = False
                btnRej.Visible = False
                txtPay_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                trDT.Visible = False
            Else
                txtRemarks.Text = ""
                txtRemarks.Text = txtRemarks.Text.Replace(",", "")
                txtPay_dt.Text = ""
                txtPay_dt.Text = txtPay_dt.Text.Replace(",", "")
                divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>" & errorMessage & "</div>"
            End If
        End If


    End Sub

    Protected Sub btnAppr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppr.Click
        If txtRemarks.Text.Replace(",", "") = "" Then
            divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Remarks cannot be left empty</div>"
            txtRemarks.Text = txtRemarks.Text.Replace(",", "")
        Else
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty


            str_err = calltransaction("Approved", errorMessage)
            If str_err = "0" Then

                divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Record Updated  Successfully</div>"

                txtRemarks.Text = txtRemarks.Text.Replace(",", "")
                ltGRemark.Text = ""
                txtRemarks.Visible = False
                btnAppr.Visible = False
                btnRej.Visible = False
                txtPay_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                trDT.Visible = False
            Else
                txtRemarks.Text = ""
                txtRemarks.Text = txtRemarks.Text.Replace(",", "")
                txtPay_dt.Text = ""
                txtPay_dt.Text = txtPay_dt.Text.Replace(",", "")
                divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>" & errorMessage & "</div>"
            End If
        End If



    End Sub
    Function calltransaction(ByVal flag As String, ByRef errorMessage As String) As Integer
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RFD_ID", ViewState("RFD_ID"))
                pParms(1) = New SqlClient.SqlParameter("@REF_STU_ID", ViewState("STU_ID"))
                pParms(2) = New SqlClient.SqlParameter("@Type", ViewState("Type"))
                pParms(3) = New SqlClient.SqlParameter("@Status", flag) 'flag--approved or rejected
                pParms(4) = New SqlClient.SqlParameter("@REMARKS", txtRemarks.Text.Replace(",", ""))
                pParms(5) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_id"))
                pParms(6) = New SqlClient.SqlParameter("@RFD_PAY_DT", txtPay_dt.Text)
                pParms(7) = New SqlClient.SqlParameter("@ACD_ID", ViewState("STP_ACD_ID"))
                pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[REF].[SaveREFERRAL_PAY]", pParms)
                status = pParms(8).Value


                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If


                calltransaction = "0"
            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function
End Class
