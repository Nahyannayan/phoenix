﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReferralTrack_modal.aspx.vb" Inherits="ReferralTrack_modal" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
        <ajaxtoolkit:toolkitscriptmanager id="ScriptManager1" runat="server" asyncpostbacktimeout="600">
                    </ajaxtoolkit:toolkitscriptmanager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <div style="text-align: center; margin-top: -1px;">
                    <div id="pldis" runat="server" style="display: block; overflow: visible; border-color: #1b80b6; border-style: solid; border-width: 1px; width: 915px;">
                        <div class="msg_header" style="width: 915px; margin-top: 1px; vertical-align: middle; background-color: White;">
                            <div class="msg" style="margin-top: 0px; vertical-align: middle; text-align: left;">
                                <span style="clear: left; display: inline; float: left; visibility: visible;">Process
                                Referral</span> <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: -4px; vertical-align: top;"></span>
                            </div>
                        </div>
                        <asp:Panel ID="plStudAtt" runat="server" Width="915px" Height="590px" BackColor="White"
                            BorderColor="#1b80b6" BorderStyle="Solid" BorderWidth="1px" ScrollBars="Horizontal">
                            <table>
                                <tr>
                                    <td>
                                        <table align="left" border="1" bordercolor="#1b80b6" class="BlueTable" width="915px">
                                            <tr class="subheader_img">
                                                <td align="left" colspan="6" valign="middle">
                                                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">Student Info</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Student Name</td>
                                                <td class="matters" align="center">:</td>
                                                <td align="left" class="matters">
                                                    <asp:LinkButton ID="ltSName" runat="server"></asp:LinkButton>



                                                </td>
                                                <td align="left" class="matters" style="height: 15px;">Student Id</td>
                                                <td class="matters" align="center">:</td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltStu_no" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Enquiry Date</td>
                                                <td align="center" class="matters">:</td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltENQ_DT" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left" class="matters" style="height: 15px;">Registration Date</td>
                                                <td align="center" class="matters">:</td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltREG_DT" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Grade
                                                </td>
                                                <td align="center" class="matters">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltGrade" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left" class="matters" style="height: 15px;">Section
                                                </td>
                                                <td align="center" class="matters">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltSection" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Joining Date&nbsp;</td>
                                                <td class="matters" align="center">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltDOJ" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left" class="matters" style="height: 15px;">Acad. Year
                                                </td>
                                                <td class="matters" align="center">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltACY_DESCR" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Current Status
                                                </td>
                                                <td class="matters" align="center">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltStatus" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left" class="matters" style="height: 15px;">Last Att. Date
                                                </td>
                                                <td class="matters" align="center">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltLastDT" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Attendance As On Date</td>
                                                <td class="matters" align="center">:</td>
                                                <td align="left" class="matters" colspan="4">
                                                    <asp:Literal ID="ltAtt_count" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Primary Contact
                                                </td>
                                                <td align="center" class="matters">:
                                                </td>
                                                <td align="left" class="matters" colspan="4">
                                                    <asp:Literal ID="ltPri_cont" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Father&#39;s Name
                                                </td>
                                                <td class="matters" align="center">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltFname" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left" class="matters" style="height: 15px;">Mother&#39;s Name
                                                </td>
                                                <td class="matters" align="center">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltMname" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Email Address
                                                </td>
                                                <td class="matters" align="center">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltEmail" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left" class="matters">Mobile No
                                                </td>
                                                <td class="matters" align="center">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltMobNo" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Sibling Info</td>
                                                <td align="center" class="matters">:</td>
                                                <td align="left" class="matters" colspan="4">
                                                    <asp:Literal ID="ltSib_info" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Referred By</td>
                                                <td class="matters" align="center">:</td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltRefBy" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left" class="matters">Referred On</td>
                                                <td class="matters" align="center">:</td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltRef_On" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters" style="height: 15px;">Email Address
                                                </td>
                                                <td align="center" class="matters">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltRef_email" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left" class="matters">Mobile No
                                                </td>
                                                <td align="center" class="matters">:
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:Literal ID="ltRef_mob" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters">
                                        <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False" BorderColor="#1B80B6"
                                            DataKeyNames="ID" EmptyDataText="No record available !!!" EnableModelValidation="True"
                                            Font-Names="Verdana" Font-Size="8pt" ForeColor="#1B80B6" Width="90%" ShowFooter="True">
                                            <RowStyle CssClass="griditem" Height="25px" />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                        <asp:Label ID="lbltot_amt" runat="server" Text='<%# bind("tot_amt") %>'></asp:Label>
                                                        <asp:Label ID="lblfee_paid" runat="server" Text='<%# bind("fee_paid") %>'></asp:Label>
                                                        <asp:Label ID="lblCount" runat="server" Text='<%# bind("SHOW_GRP") %>'></asp:Label>
                                                        <asp:Label ID="lblGRP" runat="server" Text='<%# bind("Grouped") %>'></asp:Label>
                                                        <asp:Label ID="lblFee_TYPE" runat="server" Text='<%# bind("Fee_TYPE") %>'></asp:Label>
                                                        <asp:Label ID="lblFEE_AMT_PAID" runat="server" Text='<%# bind("FEE_AMT_PAID") %>'></asp:Label>
                                                        <asp:Label ID="lblFEE_PAY_DT" runat="server" Text='<%# bind("FEE_PAY_DT") %>'></asp:Label>


                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgLock" runat="server" ImageUrl="~/Images/ref_image/grp.png" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reward To">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReward_To" runat="server" Text='<%# Bind("Reward_To") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="185px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRPT_DISPLAY" runat="server" Text='<%# Bind("RPT_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPAY_AMT" runat="server" Text='<%# BIND("PAY_AMT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Khaki" />
                                            <HeaderStyle CssClass="subheader_img" Height="25px" />
                                            <FooterStyle BackColor="#E4F0F7" Height="25px" Font-Names="Verdana"
                                                Font-Size="8pt" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr id="trRM" runat="server">
                                                <td class="matters">
                                                    <asp:Literal ID="ltGRemark" runat="server"></asp:Literal>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRemarks" runat="server" MaxLength="300" Width="638px" ValidationGroup="info"
                                                        EnableViewState="False"></asp:TextBox>
                                                </td>
                                            </tr>
                                           <tr id="tr1" runat="server">
                                                <td class="matters">
                                                  
                                                </td>
                                                <td align="left" class="matters" style="color: #FF0000">
                                                   Please generate the payment voucher from Financial Module.
                                                </td>
                                            </tr>
                                            <tr id="trDT" runat="server">
                                                <td class="matters">
                                                    <asp:Literal ID="ltPay_dt" runat="server"></asp:Literal>
                                                </td>
                                                <td class="matters" align="left">
                                                    <asp:TextBox ID="txtPay_dt" runat="server" Width="110px"></asp:TextBox>
                                                    &nbsp;
                                                   <%-- <img alt="Icon" src="../Images/calendar.gif" id="imgBtnPay_dt" height="16px"/>--%>
                                                    <asp:ImageButton runat="Server" ID="imgBtnPay_dt" ImageUrl="~/Images/calendar.gif"
                                                        AlternateText="Click here to display calendar" />

                                                    <ajaxtoolkit:calendarextender id="CalendarExtender1" runat="server"
                                                        format="dd/MMM/yyyy" popupbuttonid="imgBtnPay_dt"
                                                        targetcontrolid="txtPay_dt" />
                                                    <br />
                                                    <span style="font-size: 7pt" id="ddspan" runat="server">(dd/mmm/yyyy)</span>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="divError" runat="server">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnAppr" runat="server" Text="Approve" CssClass="button" Width="120px"
                                            ValidationGroup="info" /><asp:Button ID="btnRej" runat="server" Text="Reject" CssClass="button"
                                                Width="120px" ValidationGroup="info" /><asp:Button ID="btnCancelRef1" runat="server"
                                                    Text="Close" CssClass="button" Width="120px" CausesValidation="False" OnClientClick="javascript:window.close();return false;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <div id="divmsg" runat="server">
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </asp:Panel>
                    </div>
                    <ajaxtoolkit:popupcontrolextender id="popExDescr" runat="server"
                        targetcontrolid="ltSName"
                        popupcontrolid="plTrack"
                        position="left" offsetx="-30" offsety="5" />

                    <div id="plTrack" runat="server" style="display: block; overflow: visible; border-color: #1b80b6; border-style: solid; border-width: 1px; width: 800px; background-color: White;">
                        <div class="msg_header" style="width: 800px; margin-top: 1px; vertical-align: middle; background-color: White;">
                            <div class="msg" style="margin-top: 0px; vertical-align: middle; text-align: left;">
                                <span style="clear: left; display: inline; float: left; visibility: visible;">Student Tracking</span>
                            </div>
                        </div>
                        <asp:Panel ID="Panel1" runat="server" Width="800px" Height="250px" BackColor="White"
                            BorderColor="#1b80b6" BorderStyle="Solid" BorderWidth="1px" ScrollBars="Both">
                            <asp:GridView ID="gvtrack" runat="server"
                                BorderColor="#1B80B6"
                                EmptyDataText="No previous enquiry history available !!!" EnableModelValidation="True"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="#1B80B6" Width="100%">
                                <RowStyle CssClass="griditem" Height="28px" />
                                <EmptyDataRowStyle CssClass="subheader_img" Wrap="True" Height="28px" HorizontalAlign="Center" VerticalAlign="Middle" />


                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
