Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data

Partial Class Referral_create
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function IsPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "edit"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "HD01004") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    bindInfo_Type()

                    Call BindRef_Text()




                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If


    End Sub
    Private Sub bindInfo_Type()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT RT_DESCR,RT_INFO_TYPE FROM REF.REFERRAL_TEXT WITH(NOLOCK) ORDER BY RT_INFO_TYPE"
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_query)
            While datareader.Read
                ddlInfoType.Items.Add(New ListItem(datareader("RT_DESCR"), datareader("RT_INFO_TYPE")))
            End While

        End Using
    End Sub
    Private Sub BindRef_Text()
        Try

            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@INFO_TYPE", ddlInfoType.SelectedValue)

            Using readerRef_Text As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "REF.GETREF_MAIL_TEXT", param)

                If readerRef_Text.HasRows = True Then
                    While readerRef_Text.Read
                        txtOfferText.Content = Convert.ToString(readerRef_Text("TEXT_CONTENT"))

                    End While
                Else
                    txtOfferText.Content = ""
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try



    End Sub
    Protected Sub ddlInfoType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInfoType.SelectedIndexChanged
        BindRef_Text()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            Call BindRef_Text()

            lblError.Text = "Record Saved Successfully"


        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim Content_Type As String = String.Empty
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer

                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@CONTENT", txtOfferText.Content)
                pParms(1) = New SqlClient.SqlParameter("@INFO_TYPE", ddlInfoType.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "REF.SAVEREFERRAL_MAIL_TEXT", pParms)

                status = pParms(2).Value




                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If


                ViewState("datamode") = "add"


                calltransaction = "0"

                'disable_controlview

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function


End Class
