﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="referral_Link.aspx.vb" Inherits="Refer_referral_LINK" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
             <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <script type="text/javascript" language="javascript">

        function validateCheckBox() {

            var isValid = false;
            var iloop = 0
            var gridView = document.getElementById('<%= gvRef.ClientID %>');

for (var i = 1; i < gridView.rows.length; i++) {

    var inputs = gridView.rows[i].getElementsByTagName('input');

    if (inputs != null) { if (inputs[0].type == "checkbox") { if (inputs[0].checked) { iloop = iloop + 1; } } }

}

if (iloop > 1) {
    alert("Please select only one checkbox !!!");
}



}

    </script>
</head>
<body>

    <form id="form1" runat="server">

        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="">
            <tr>
                <td><span  class="field-label">Student Name</span></td>
                <td>
                    <asp:Literal ID="ltStu_name" runat="server"></asp:Literal></td>
                <td ><span  class="field-label">Grade</span></td>
                <td>
                    <asp:Literal ID="ltGradeSct" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td ><span  class="field-label">Parent Name(Primary Contact)</span></td>
                <td>
                    <asp:Literal ID="ltPrimCont" runat="server"></asp:Literal></td>
                <td ><span  class="field-label">Mobile No</span></td>
                <td>
                    <asp:Literal ID="ltMobNo" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td colspan="4" align="left" class="title-bg-lite">Search </td>
            </tr>
            <tr>
                <td ><span  class="field-label">Name(Referred By)</span></td>
                <td>
                    <asp:TextBox ID="txtName_Refd" runat="server" ></asp:TextBox>
                </td>
                <td ><span  class="field-label">Name(Referral)</span></td>
                <td>
                    <asp:TextBox ID="txtName_Referral" runat="server" ></asp:TextBox>
                </td>
            </tr>
            <tr >
                <td><span  class="field-label">Referral Mobile No</span></td>
                <td>
                    <asp:TextBox ID="txtMobNo" runat="server"></asp:TextBox>
                </td>
                <td align="right" colspan="2">

                    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search"  />
                </td>
            </tr>
            <tr>
                <td colspan="4" class="title-bg-lite">Link Referral (Must link to single referral only)</td>
            </tr>
            <tr>
                <td colspan="4" align="center" >
                    <div >
                        <asp:GridView ID="gvRef" runat="server" Width="100%" AutoGenerateColumns="False"
                            CellPadding="3" DataKeyNames="RFS_ID" PageSize="20" CssClass="table table-bordered table-row" >
                            <RowStyle HorizontalAlign="Center" CssClass="griditem"  />
                            <Columns>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# bind("bCHK") %>' Enabled='<%# bind("bCHK_Enable") %>' onclick="validateCheckBox()" />

                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" Text='<%# bind("Ref_dt") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Referred By">

                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRFM_NAME" runat="server" Text='<%# bind("RFM_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Referred To">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"  />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbtnRFS_NAME" runat="server" Text='<%# bind("RFS_NAME") %>'></asp:Label>
                                   
                                            <asp:Label ID="lblSname" runat="server" Text='<%# bind("S_NAME") %>'></asp:Label>
                                    </ItemTemplate>

                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile No">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# BIND("RFS_MOB") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email Address">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# BIND("RFS_EMAIL") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="RFD_ID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRFS_ID" runat="server" Text='<%# Bind("RFS_ID") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle  HorizontalAlign="Left" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button ID="btnSave_tran" Text="Save" runat="server" CssClass="button" />
                </td>
            </tr>

        </table>

    </form>
</body>
</html>
