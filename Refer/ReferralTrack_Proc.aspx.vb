﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO

Imports System.Text
Partial Class Referral_ReferralTrack_Proc
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
  

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'collect the url of the file to be redirected in view state
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "HD12055" And ViewState("MainMnu_code") <> "S108855" And ViewState("MainMnu_code") <> "A108865") Then


                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                Else


                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Getrights()
                    gridbind()

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub
    Private Sub Getrights()
        Dim sql_query As String = String.Empty
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        ViewState("Reg_Access") = False
        ViewState("Acc_Access") = False
        ViewState("Pay_Access") = False


        Dim para(3) As SqlClient.SqlParameter
        para(0) = New SqlClient.SqlParameter("@ROL_ID", Session("sroleid"))
        para(1) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_id"))
        para(2) = New SqlClient.SqlParameter("@bSuperUser",Session("sBusper"))

        Using dataReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "REF.GETUSER_RIGHTS", para)
            If dataReader.HasRows Then
                While dataReader.Read
                    ViewState("Reg_Access") = Convert.ToBoolean(dataReader("REG"))
                    ViewState("Acc_Access") = Convert.ToBoolean(dataReader("ACC"))
                    ViewState("Pay_Access") = Convert.ToBoolean(dataReader("PAY"))
                End While

            End If
        End Using


    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_SNAME As String = String.Empty
            Dim str_filter_GRD_SCT As String = String.Empty
            Dim str_filter_ACY_DESCR As String = String.Empty
            Dim str_filter_DOJ As String = String.Empty
            Dim str_filter_REF_BY As String = String.Empty

            Dim ds As New DataSet

            str_Sql = " SELECT distinct RFD_ID, REF_STU_ID,SNAME,DOJ,GRD_SCT,REF_BY,ACY_DESCR,STU_CURRSTATUS," & _
            " STU_DOJ, GRD_DISPLAYORDER, RFD_REG_APPR, RFD_ACC_APPR, RFD_PAYED, Reg_flag, Acc_flag, PAYED_flag " & _
  " FROM(select distinct RFD_ID, REF_STU_ID,STU_FIRSTNAME + ' '+ ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS SNAME, " & _
" REPLACE(CONVERT(VARCHAR(12),STU_DOJ,106),' ','/') AS DOJ, " & _
" GRM_DISPLAY + ' & '+ SCT_DESCR  as GRD_SCT ,case WHEN (SELECT COUNT(OLU_ID) " & _
"  FROM ONLINE.ONLINE_USERS_M WHERE OLU_NAME=RFM_USR_NAME)>0 " & _
" THEN (select top 1 upper(OLU_DISPLAYNAME) from ONLINE.ONLINE_USERS_M WITH(NOLOCK) where  OLU_NAME=RFM_USR_NAME) + '<font color=''#eb4c06''> (Parent) </font>' " & _
"  WHEN (select COUNT(EMP_ID) from dbo.EMPLOYEE_M WITH(NOLOCK) where EMP_USR_NAME =RFM_USR_NAME )>0 then (select upper(EMP_FNAME+' '+isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'')) " & _
 " from dbo.EMPLOYEE_M WITH(NOLOCK) where EMP_USR_NAME =RFM_USR_NAME )+ '<font color=''#eb4c06''> (GEMS Staff) </font>'   " & _
 "  WHEN charindex('@',RFM_USR_NAME)>0   then   ISNULL(RFM_FNAME,'')+'  '+ISNULL(RFM_LNAME,'') +'(Public/Partner)' end AS REF_BY, ACY_DESCR,STU_CURRSTATUS,STU_DOJ,GRD_DISPLAYORDER,RFD_REG_APPR,RFD_ACC_APPR,RFD_PAYED, " & _
 " CASE ISNULL(RFD_REG_APPR,'Pending') " & _
 " WHEN 'Pending' THEN 'Pending'  WHEN 'Approved' THEN 'Approved' WHEN 'Rejected' THEN 'Rejected' end as Reg_flag," & _
" CASE ISNULL(RFD_ACC_APPR,'Pending') WHEN 'Pending' THEN 'Pending'  WHEN 'Approved' THEN 'Approved' " & _
" WHEN 'Rejected' THEN 'Rejected' end as Acc_flag, CASE ISNULL(RFD_PAYED,'Pending')" & _
 " WHEN 'Pending' THEN 'Pending' WHEN 'Rewarded' THEN 'Rewarded' WHEN 'Rejected' THEN 'Rejected' end as PAYED_flag " & _
" from REF.REFERRAL_M  WITH(NOLOCK) INNER JOIN REF.REFERRAL_S WITH(NOLOCK) ON RFM_ID=RFS_RFM_ID " & _
" INNER JOIN REF.REFERRAL_D WITH(NOLOCK) ON RFS_ID=RFD_RFS_ID  INNER JOIN STUDENT_M WITH(NOLOCK) ON " & _
 " STU_ID=REF_STU_ID INNER JOIN  STUDENT_PROMO_S WITH(NOLOCK)  ON STP_STU_ID=STU_ID " & _
 "  INNER JOIN SECTION_M WITH(NOLOCK) ON STP_SCT_ID=SCT_ID AND STP_GRM_ID=SCT_GRM_ID " & _
 "  INNER JOIN GRADE_BSU_M WITH(NOLOCK) ON GRM_ID=STP_GRM_ID " & _
  "  INNER JOIN ACADEMICYEAR_D WITH(NOLOCK) ON STP_ACD_ID=ACD_ID   INNER JOIN ACADEMICYEAR_M WITH(NOLOCK) ON ACD_ACY_ID=ACY_ID " & _
  "  INNER JOIN GRADE_M WITH(NOLOCK) ON STP_GRD_ID=GRD_ID  WHERE RFD_BSU_ID='" & Session("sBsuid") & "'" & _
 "  AND STU_DOJ BETWEEN STP_FROMDATE AND ISNULL(STP_TODATE, STU_DOJ))A WHERE RFD_ID<>''"
            Dim txtSearch As New TextBox

            Dim str_search As String
            Dim str_SNAME As String = String.Empty
            Dim str_GRD_SCT As String = String.Empty
            Dim str_ACY_DESCR As String = String.Empty
            Dim str_DOJ As String = String.Empty

            Dim str_REF_BY As String = String.Empty



            If gvRef.Rows.Count > 0 Then

                txtSearch = gvRef.HeaderRow.FindControl("txtSNAME")
                str_SNAME = txtSearch.Text
                str_filter_SNAME = " AND SNAME LIKE '%" & txtSearch.Text & "%'"

                txtSearch = gvRef.HeaderRow.FindControl("txtGRD_SCT")
                str_GRD_SCT = txtSearch.Text
                str_filter_GRD_SCT = " AND GRD_SCT LIKE '%" & txtSearch.Text & "%'"

                txtSearch = gvRef.HeaderRow.FindControl("txtACY_DESCR")
                str_ACY_DESCR = txtSearch.Text
                str_filter_ACY_DESCR = " AND ACY_DESCR LIKE '%" & txtSearch.Text & "%'"

                txtSearch = gvRef.HeaderRow.FindControl("txtDOJ")
                str_DOJ = txtSearch.Text
                str_filter_DOJ = " AND DOJ LIKE '%" & txtSearch.Text & "%'"

                'txtSearch = gvRef.HeaderRow.FindControl("txtSTU_CURRSTATUS")
                'str_STU_CURRSTATUS = txtSearch.Text
                'str_filter_STU_CURRSTATUS = " AND STU_CURRSTATUS LIKE '%" & txtSearch.Text & "%'"

                txtSearch = gvRef.HeaderRow.FindControl("txtREF_BY")
                str_REF_BY = txtSearch.Text
                str_filter_REF_BY = " AND REF_BY LIKE '%" & txtSearch.Text & "%'"


            End If



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_SNAME & str_filter_GRD_SCT & str_filter_ACY_DESCR & str_filter_DOJ & str_filter_REF_BY & " ORDER BY STU_DOJ,GRD_DISPLAYORDER,REF_BY")
            If ds.Tables(0).Rows.Count > 0 Then
                gvRef.DataSource = ds.Tables(0)
                gvRef.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(7) = True
                gvRef.DataSource = ds.Tables(0)
                Try
                    gvRef.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvRef.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvRef.Rows(0).Cells.Clear()
                gvRef.Rows(0).Cells.Add(New TableCell)
                gvRef.Rows(0).Cells(0).ColumnSpan = columnCount
                gvRef.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvRef.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvRef.HeaderRow.FindControl("txtSNAME")
            txtSearch.Text = str_SNAME
            txtSearch = gvRef.HeaderRow.FindControl("txtdoj")
            txtSearch.Text = str_DOJ

            txtSearch = gvRef.HeaderRow.FindControl("txtACY_DESCR")
            txtSearch.Text = str_ACY_DESCR

            'txtSearch = gvRef.HeaderRow.FindControl("txtSTU_CURRSTATUS")
            'txtSearch.Text = str_STU_CURRSTATUS
            txtSearch = gvRef.HeaderRow.FindControl("txtGRD_SCT")
            txtSearch.Text = str_GRD_SCT

            txtSearch = gvRef.HeaderRow.FindControl("txtREF_BY")
            txtSearch.Text = str_REF_BY

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub
    Protected Sub gvRef_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRef.PageIndexChanging
        gvRef.PageIndex = e.NewPageIndex

        gridbind()
    End Sub
    Protected Sub gvRef_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRef.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbtnReg As LinkButton = DirectCast(e.Row.FindControl("lbtnReg"), LinkButton)
            Dim lbtnAcc As LinkButton = DirectCast(e.Row.FindControl("lbtnAcc"), LinkButton)
            Dim lbtnPaid As LinkButton = DirectCast(e.Row.FindControl("lbtnPaid"), LinkButton)
            Dim lblRFD_ID As Label = DirectCast(e.Row.FindControl("lblRFD_ID"), Label)
            Dim gvPay_detail As GridView = DirectCast(e.Row.FindControl("gvPay_detail"), GridView)
            Dim plInfo As Panel = DirectCast(e.Row.FindControl("plInfo"), Panel)
            Dim ltAnnual As Literal = DirectCast(e.Row.FindControl("ltAnnual"), Literal)
            Dim ltReg As Literal = DirectCast(e.Row.FindControl("ltReg"), Literal)
            Dim ltReg_Auth As Literal = DirectCast(e.Row.FindControl("ltReg_Auth"), Literal)
            Dim ltAcc As Literal = DirectCast(e.Row.FindControl("ltAcc"), Literal)
            Dim ltAcc_Auth As Literal = DirectCast(e.Row.FindControl("ltAcc_Auth"), Literal)
            Dim ltCash As Literal = DirectCast(e.Row.FindControl("ltCash"), Literal)
            Dim ltCash_Auth As Literal = DirectCast(e.Row.FindControl("ltCash_Auth"), Literal)
            Dim REF_STU_ID As Label = DirectCast(e.Row.FindControl("lblREF_STU_ID"), Label)




            lbtnReg.Attributes.Add("onclick", "javascript:Apprvdata('" & lblRFD_ID.Text & "','" & REF_STU_ID.Text & "','Reg','" & lbtnReg.Text & "'); return true;")
            lbtnAcc.Attributes.Add("onclick", "javascript:Apprvdata('" & lblRFD_ID.Text & "','" & REF_STU_ID.Text & "','Acc','" & lbtnAcc.Text & "'); return true;")
            lbtnPaid.Attributes.Add("onclick", "javascript:Apprvdata('" & lblRFD_ID.Text & "','" & REF_STU_ID.Text & "','Paid','" & lbtnPaid.Text & "'); return true;")



            If lbtnReg.Text = "Pending" Then
                If ViewState("Reg_Access") = True Then
                    lbtnReg.Enabled = True
                    lbtnAcc.Enabled = False
                    lbtnPaid.Enabled = False
                Else
                    lbtnReg.Enabled = False
                    lbtnAcc.Enabled = False
                    lbtnPaid.Enabled = False
                End If

            ElseIf lbtnReg.Text = "Approved" Then
                If ViewState("Acc_Access") = True Then

                    lbtnAcc.Enabled = True
                    lbtnPaid.Enabled = False
                Else
                    lbtnAcc.Enabled = False
                    lbtnPaid.Enabled = False
                End If


            End If
            If lbtnReg.Text = "Approved" Then
                If lbtnAcc.Text = "Pending" Then
                    If ViewState("Acc_Access") = True Then
                        lbtnAcc.Enabled = True
                        lbtnPaid.Enabled = False
                    Else
                        lbtnAcc.Enabled = False
                        lbtnPaid.Enabled = False
                    End If




                ElseIf lbtnAcc.Text = "Approved" Then
                    If (ViewState("Acc_Access") = True) And (ViewState("Pay_Access") = True) Then
                        lbtnAcc.Enabled = True
                        lbtnPaid.Enabled = True
                    ElseIf (ViewState("Acc_Access") = True) And (ViewState("Pay_Access") = False) Then
                        lbtnAcc.Enabled = True
                        lbtnPaid.Enabled = False

                    ElseIf (ViewState("Acc_Access") = False) And (ViewState("Pay_Access") = False) Then
                        lbtnAcc.Enabled = False
                        lbtnPaid.Enabled = False
                    End If



                End If
            End If
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@RFP_RFD_ID", lblRFD_ID.Text)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[REF].[GETREFERRAL_PAY_INFO]", param)
            If ds.Tables(0).Rows.Count > 0 Then
                plInfo.Visible = True
                gvPay_detail.DataSource = ds.Tables(0)
                gvPay_detail.DataBind()

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    ltAnnual.Text = "<font color='black'>" & Convert.ToString(ds.Tables(0).Rows(i)("RFD_FEE_AMOUNT")) & "</font>"
                    ltReg.Text = "<font color='black'>" & Convert.ToString(ds.Tables(0).Rows(i)("RFD_REG_REMARK")) & "</font>"
                    ltAcc.Text = "<font color='black'>" & Convert.ToString(ds.Tables(0).Rows(i)("RFD_ACC_REMARK")) & "</font>"
                    ltCash.Text = "<font color='black'>" & Convert.ToString(ds.Tables(0).Rows(i)("RFD_PAY_REMARK")) & "</font>"

                    If Convert.ToString(ds.Tables(0).Rows(i)("REG_NAME")).Trim <> "" Then
                        ltReg_Auth.Text = "Authorized By:  <font color='black'> " & Convert.ToString(ds.Tables(0).Rows(i)("REG_NAME")) & "</font>"

                    End If

                    If Convert.ToString(ds.Tables(0).Rows(i)("ACC_NAME")).Trim <> "" Then
                        ltAcc_Auth.Text = "Authorized By:  <font color='black'> " & Convert.ToString(ds.Tables(0).Rows(i)("ACC_NAME")) & "</font>"
                    End If

                    If Convert.ToString(ds.Tables(0).Rows(i)("PAYED_NAME")).Trim <> "" Then
                        ltCash_Auth.Text = "Authorized By:  <font color='black'> " & Convert.ToString(ds.Tables(0).Rows(i)("PAYED_NAME")) & "</font>"
                    End If

                    Exit For
                Next

            Else
                plInfo.Visible = False


                'Else
                '    ds.Tables(0).Clear()
                '    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                '    ''start the count from 1 no matter gridcolumn is visible or not
                '    'ds.Tables(0).Rows(0)(6) = True

                '    gvPay_detail.DataSource = ds.Tables(0)
                '    Try
                '        gvPay_detail.DataBind()
                '    Catch ex As Exception
                '    End Try
            End If



        End If

    End Sub
   

    'Protected Sub lbtnReg_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim lblRFD_ID As Label = DirectCast(sender.findcontrol("lblRFD_ID"), Label)
    '        Dim lbtnReg As LinkButton = DirectCast(sender.FindControl("lbtnReg"), LinkButton)
    '        Dim REF_STU_ID As Label = DirectCast(sender.findcontrol("lblREF_STU_ID"), Label)

    '        ViewState("RFD_ID") = lblRFD_ID.Text
    '        ViewState("STU_ID") = REF_STU_ID.Text
    '        ViewState("Type") = "Reg"
    '        ViewState("Status") = lbtnReg.Text
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub lbtnAcc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim lblRFD_ID As Label = DirectCast(sender.findcontrol("lblRFD_ID"), Label)
    '        Dim lbtnAcc As LinkButton = DirectCast(sender.FindControl("lbtnAcc"), LinkButton)
    '        Dim REF_STU_ID As Label = DirectCast(sender.findcontrol("lblREF_STU_ID"), Label)
    '        ViewState("RFD_ID") = lblRFD_ID.Text
    '        ViewState("STU_ID") = REF_STU_ID.Text
    '        ViewState("Type") = "Acc"
    '        ViewState("Status") = lbtnAcc.Text
    '        txtRemarks.Text = ""
    '        txtPay_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
    '        trDT.Visible = False
    '        ltGRemark.Text = ""
    '        txtRemarks.Visible = True
    '        btnAppr.Visible = True
    '        btnRej.Visible = True
    '        btnAppr.Enabled = True
    '        btnRej.Enabled = True
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub lbtnPaid_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim lblRFD_ID As Label = DirectCast(sender.findcontrol("lblRFD_ID"), Label)
    '        Dim REF_STU_ID As Label = DirectCast(sender.findcontrol("lblREF_STU_ID"), Label)
    '        Dim lbtnPaid As LinkButton = DirectCast(sender.FindControl("lbtnPaid"), LinkButton)
    '        ViewState("RFD_ID") = lblRFD_ID.Text
    '        ViewState("STU_ID") = REF_STU_ID.Text
    '        ViewState("Type") = "Paid"
    '        ViewState("Status") = lbtnPaid.Text
    '        txtRemarks.Text = ""
    '        txtPay_dt.Text = ""
    '        txtPay_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
    '        bind_stuInfo()
    '        bind_PayInfo()
    '        Me.mdlPopup.Show()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub btnSearchREF_BY_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGRD_SCT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchACY_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchDOJ_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

   
  
End Class

