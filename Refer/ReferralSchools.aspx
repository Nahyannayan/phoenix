﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReferralSchools.aspx.vb"
    MasterPageFile="~/mainMasterPage.master" Inherits="Referral_ReferralSchools" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
      .darkPanlvisible,
.darkPanl2,
/*.darkPanl   max-height: 800px;  { position:fixed; left:0px; top:0px; background:url(../Images/dark.png) 0 0 !important; display:none; width:100%; height:100%; z-index:10000;}*/
.darkPanl     { 
 
   position:fixed; 
   left:20%;
   top:10%;
   background:url(../Images/dark.png) 0 0 !important;
   display:none;
   z-index:10000;}
   
  /* .darkPanl     { 
 width:100% !important; height:100% !important;
   position: fixed !important;
   left:0%!important;
   top:0%!important;
   background:url(../Images/dark.png) 0 0 !important;
   display:none!important;
}
*/


.darkPanlvisible { display:block;}
}
.panelRef { background-color: #FFFFFF;  border: 3px solid #D8E5F8; 
             margin:auto auto auto auto; overflow-x: hidden; overflow-y: hidden; padding: 10px 10px 10px 10px;              
             position:absolute;margin-left: -50px; margin-top:-100px;
             }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Referral Schools
        </div>
        <div class="card-body">
            <div class="table-responsive ">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr valign="top">
                        <td align="left">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2" valign="top">
                            <asp:Panel ID="plRefSch" runat="server" Width="100%" ScrollBars="Vertical">
                                <asp:GridView ID="gvManageUser" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                    PageSize="50" Width="100%" DataKeyNames="BSU_ID">
                                    <RowStyle CssClass="griditem" Height="25px" />
                                    <Columns>
                                        <asp:BoundField DataField="r1" HeaderText="Sr.No">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:BoundField>

                                        <asp:TemplateField HeaderText="School name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSchoolH" runat="server" Text='<%# Bind("bsu_name") %>'></asp:Label>
                                                <asp:Label ID="lblBSU_ID" runat="server" Text='<%# Bind("BSU_ID") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GEMS Parent">

                                            <ItemStyle HorizontalAlign="Left"
                                                VerticalAlign="Middle" Wrap="True"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnEnq" runat="server" CausesValidation="False"
                                                    CommandArgument='<%# Bind("BSU_ID") %>' CommandName="Ref_Par"
                                                    ImageUrl='<%# Bind("BSU_bREFERRAL") %>'
                                                    OnClientClick="return confirm('Are you sure you want to update the status?'); " />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="General Public">


                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnReg" runat="server" CausesValidation="False"
                                                    CommandArgument='<%# Bind("BSU_ID") %>' CommandName="Ref_Pub"
                                                    ImageUrl='<%# Bind("BSU_bREFERRAL_PUBLIC") %>'
                                                    OnClientClick="return confirm('Are you sure you want to update the status?'); " />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit Terms & Conditions ">


                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnTerms" runat="server" CausesValidation="False" ToolTip="Click here to add/edit Terms and Conditions"
                                                    CommandArgument='<%# Bind("BSU_ID") %>' CommandName="Terms"
                                                    ImageUrl='<%# Bind("BSU_TERM") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle BackColor="Wheat" />
                                    <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </asp:Panel>

                        </td>
                    </tr>
                </table>



                <asp:Panel ID="plTermCond" runat="server" CssClass="darkPanlvisible panel-cover" Visible="false">
                    <div class="panelRef" style=" margin-top: 35px; margin-left: 10px;">
                        <div style="float: right; vertical-align: top; padding-bottom: 12px;">
                            <asp:ImageButton ID="imgtermsClose" runat="server" ToolTip="click here to close"
                                ImageUrl="~/IMAGES/closeme.png" Height="18px"  />
                        </div>
                        <div class="title-bg">
                            &nbsp;Add/Edit Terms & Condition
                        </div>
                        <div style=" display: block;">
                            <table align="left"  border="0" cellpadding="4" cellspacing="1">
                                <tr>
                                    <td align="left" >
                                        <div style="padding-top: 2px; padding-bottom: 10px;">
                                            <span class="field-label" style="background-color:white;">School Selected &nbsp;</span>
               <asp:DropDownList runat="server" ID="ddlSchoolEdit" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <telerik:RadEditor ID="txtTermsText" runat="server" EditModes="All"
                                            Height="400px" >
                                        </telerik:RadEditor>
                                    </td>
                                    <td align="left" valign="top">
                                        <div style="padding-top: 2px; padding-bottom: 10px; background-color:white;"><span class="field-label" style="background-color:white;">Copy to other school(s)</span></div>
                                        <asp:Panel ID="plSchoolList" runat="server" Height="400px" 
                                            BorderStyle="Solid" BorderWidth="1px" ScrollBars="Both" BackColor="White">
                                            <asp:CheckBoxList ID="chklistSchool" runat="server" Font-Bold="true" Font-Size="9px"></asp:CheckBoxList>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="btnUpdateTerm" runat="server" CssClass="button" Text="Update" CausesValidation="false"
                                            Width="100px" /><asp:Button ID="btnCloseTerm" runat="server" CssClass="button" Text="Close" Width="100px" CausesValidation="false" /><asp:Label ID="lblPOPUPMsg" runat="server" CssClass="error"></asp:Label>


                                    </td>
                                </tr>
                            </table>
                        </div>





                    </div>
                </asp:Panel>


            </div>
        </div>
    </div>

</asp:Content>
