﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ReferralTrack_Proc.aspx.vb" Inherits="Referral_ReferralTrack_Proc"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">




        function Apprvdata(RFD_ID, STU_ID, TYPE, RSTATUS) {


            var url;
            var NameandCode;
            url = "ReferralTrack_modal.aspx?RFD_ID=" + RFD_ID + " &STU_ID=" + STU_ID + "&TYP=" + TYPE + "&R_STATUS=" + RSTATUS;
            result = radopen(url, "pop_up");
        }
        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);


            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../Images/ref_image/minus_ref.png";
                }
                else {
                    img.src = "../Images/ref_image/minus_ref.png";
                }
                img.alt = "Close to view other details";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/ref_image/Plus_ref.png";
                }
                else {
                    img.src = "../Images/ref_image/Plus_ref.png";
                }
                img.alt = "Expand to show details";
            }

        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            location.reload();
            return true;
        }

    </script>

    <%--  
        function onCalendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10002;
        }
                
    </script>--%>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            TRACKING AND PROCESSING
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom">
                            <asp:GridView ID="gvRef" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" DataKeyNames="RFD_ID" AllowPaging="True">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:expandcollapse('div<%# Eval("RFD_ID") %>', 'one');">
                                                <img id='imgdiv<%# Eval("RFD_ID") %>' alt="Click to show/hide info" border="0"
                                                    src="../Images/ref_image/Plus_ref.png" />
                                            </a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RFD_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRFD_ID" runat="server" Text='<%# Bind("RFD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Referred By">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblRefNAMEH" runat="server" __designer:wfdid="w116"
                                                Text="Referred By"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtREF_BY" runat="server" __designer:wfdid="w117"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchREF_BY" runat="server" __designer:wfdid="w118" ImageAlign="Top"
                                                ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchREF_BY_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblREF_BY" runat="server" Text='<%# Bind("REF_BY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SNAME">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblSNAMEH" runat="server" Text="Student Name" CssClass="gridheader_text"
                                                __designer:wfdid="w116"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtSNAME" runat="server" __designer:wfdid="w117"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSNAME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" __designer:wfdid="w118" OnClick="btnSearchSNAME_Click"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNAME" runat="server" Text='<%# bind("SNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GRD_SCT">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblGRD_SCTH" runat="server" __designer:wfdid="w116" CssClass="gridheader_text"
                                                Text="Grade &amp; Sect"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtGRD_SCT" runat="server" __designer:wfdid="w117"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchGRD_SCT" runat="server" __designer:wfdid="w118" ImageAlign="Top"
                                                ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchGRD_SCT_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGRD_SCT" runat="server" Text='<%# bind("GRD_SCT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText=" ACY_DESCR">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblACY_DESCRH" runat="server" __designer:wfdid="w116" CssClass="gridheader_text"
                                                Text="Acad Year"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtACY_DESCR" runat="server" __designer:wfdid="w117"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchACY_DESCR" runat="server" __designer:wfdid="w118" ImageAlign="Top"
                                                ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchACY_DESCR_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblACY_DESCR" runat="server" Text='<%# bind("ACY_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="dateo f join">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblDOJTH" runat="server" __designer:wfdid="w97" CssClass="gridheader_text"
                                                Text="Date of Join"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtDOJ" runat="server" __designer:wfdid="w103" Width="90px"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchDOJ" runat="server" __designer:wfdid="w104" ImageAlign="Top"
                                                ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchDOJ_Click" />
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOJ" runat="server" __designer:wfdid="w106" Text='<%# Bind("DOJ") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_CURRSTATUS" runat="server" __designer:wfdid="w110" Text='<%# Bind("STU_CURRSTATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Registrars Approval">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnReg" runat="server" Text='<%# bind("Reg_flag") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accountants Approval">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnAcc" runat="server" Text='<%# bind("Acc_flag") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Status">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnPaid" runat="server" Text='<%# bind("PAYED_flag") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REF_STU_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblREF_STU_ID" runat="server" Text='<%# Bind("REF_STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr align="center">
                                                <td colspan="100%" align="center">
                                                    <div id="div<%# Eval("RFD_ID") %>" style="display: none; position: relative; left: 5px; overflow: auto; width: 100%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="gvPay_detail" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                                        DataKeyNames="RFP_ID" EmptyDataText="No record available !!!" EnableModelValidation="True"
                                                                        Width="100%">
                                                                        <RowStyle />
                                                                        <EmptyDataRowStyle BackColor="#ffffcc" HorizontalAlign="Center" Wrap="True" />
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblId_p" runat="server" Text='<%# Bind("RFP_ID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Reward To">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblReward_To_P" runat="server" Text='<%# Bind("Reward_To") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="185px" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Description">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblRPT_DISPLAY_p" runat="server" Text='<%# Bind("RPT_DISPLAY") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Amount">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblPAY_AMT_P" runat="server" Text='<%# BIND("RFP_PAY_AMT") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <SelectedRowStyle BackColor="Khaki" />
                                                                        <HeaderStyle CssClass="gridheader_pop" />
                                                                        <AlternatingRowStyle />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Panel runat="server" ID="plInfo" Width="100%" ScrollBars="None" Height="100%">
                                                                        <table width="100%">
                                                                            <thead class="gridheader_pop" style="width: 100%">
                                                                                <tr>
                                                                                    <td colspan="3">Other Details
                                                                                    </td>
                                                                                </tr>
                                                                            </thead>
                                                                            <tr style="background-color: White; height: 25px;">
                                                                                <td>Annual Fee Amount
                                                                                </td>
                                                                                <td align="left" valign="middle" colspan="2">
                                                                                    <asp:Literal ID="ltAnnual" runat="server"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="background-color: White; height: 25px;">

                                                                                <td>Registrars Remarks
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:Literal ID="ltReg" runat="server"></asp:Literal>
                                                                                </td>
                                                                                <td align="right" valign="middle">
                                                                                    <asp:Literal ID="ltReg_Auth" runat="server"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="background-color: White; height: 25px;">
                                                                                <td>Accountants Remarks
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:Literal ID="ltAcc" runat="server"></asp:Literal>
                                                                                </td>
                                                                                <td align="right" valign="middle">
                                                                                    <asp:Literal ID="ltAcc_Auth" runat="server"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="background-color: White; height: 25px;">
                                                                                <td>Cashiers Remarks
                                                                                </td>
                                                                                <td align="left" valign="middle">
                                                                                    <asp:Literal ID="ltCash" runat="server"></asp:Literal>
                                                                                </td>
                                                                                <td align="right" valign="middle">
                                                                                    <asp:Literal ID="ltCash_Auth" runat="server"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle BackColor="Wheat" />
                                <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />

                            <asp:HiddenField ID="hiddenENQIDs" runat="server" />
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>
