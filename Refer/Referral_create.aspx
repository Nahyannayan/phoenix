<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Referral_create.aspx.vb" Inherits="Referral_create" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i><asp:Literal ID="ltHeader" runat="server" Text="Referral Mail Text"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table id="Table1" border="0" width="100%">
                   
                    <tr>
                        <td   valign="bottom" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>

                <table align="center" cellpadding="5"
                    cellspacing="0" width="100%">



                    <tr>
                        <td  valign="middle" align="left" width="20%" ><span class="field-label">Content Type </span>
                        </td>
                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddlInfoType" runat="server" AutoPostBack="true">
                            </asp:DropDownList></td>
                        
                    </tr>
                    <tr>
                        <td colspan="2">
                            <telerik:RadEditor ID="txtOfferText" runat="server" EditModes="All"
                                Width="100%">
                            </telerik:RadEditor>

                        </td>
                    </tr>

                    <tr>
                        <td   valign="bottom" align="center" colspan="2">
                            <asp:Button ID="btnSave"
                                runat="server" CssClass="button" Text="Save" />
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
</asp:Content>

