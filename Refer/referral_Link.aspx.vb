﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration

Imports System.Net
Partial Class Refer_referral_LINK
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            BindGrid()
            getSTUDENT_details()
        End If


    End Sub
    Private Sub BindGrid()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = String.Empty
        Dim str_Refd As String = String.Empty
        Dim str_Referral As String = String.Empty
        Dim str_MobNo As String = String.Empty
        Dim bBTNEnable As Boolean = True
        Dim bCHK_Flag As Boolean = True
        Dim DS As New DataSet
        Dim i As Integer
        Dim charSplit As String = "|"
        Dim Links As String() = New String(2) {}
        Links(0) = 0

        If Not Session("Link_Ref") Is Nothing Then
            Links = Session("Link_Ref").ToString.Split(charSplit)
        End If


        str_query = " SELECT bCHK,CASE WHEN bCHK=0 THEN 1 ELSE 0 END AS bCHK_Enable, RFS_ID,RFM_NAME,RFS_NAME,RFS_EMAIL,RFS_MOB,Ref_dt, CASE WHEN ISNULL(S_NAME,'')<>'' THEN 'Child Name :' + S_NAME else '' end as S_NAME  " & _
 " FROM(SELECT  DISTINCT S.RFS_ID,M.RFM_FNAME AS [RFM_NAME], S.RFS_REF_FNAME + ' ' + S.RFS_REF_LNAME AS [RFS_NAME], " & _
 "S.RFS_REF_EMAIL AS [RFS_EMAIL], S.RFS_REF_MOB_NO AS [RFS_MOB], " & _
 "REPLACE(CONVERT(varchar(12),S.RFS_CREATE_DT, 106), ' ', '/') AS [Ref_dt] , " & _
 "(CASE WHEN (SELECT COUNT(RFD_ID) FROM REF.REFERRAL_D  WITH(NOLOCK) WHERE RFD_ID=D.RFD_ID AND " & _
 " (RFD_EQS_ID='" & Links(0) & "' OR REF_STU_ID='" & Links(0) & "' ))>0 THEN 1 ELSE 0 END) bCHK ," & _
" ( SELECT  EQM_APPLFIRSTNAME FROM ENQUIRY_M AS E WITH(NOLOCK) " & _
" INNER JOIN ENQUIRY_SCHOOLPRIO_S  P WITH(NOLOCK) ON P.EQS_EQM_ENQID=E.EQM_ENQID WHERE EQS_ID=D.RFD_EQS_ID AND EQS_BSU_ID='" & Session("sBsuid") & "') AS S_NAME " & _
  "FROM REF.REFERRAL_S  AS S WITH(NOLOCK) INNER JOIN  REF.REFERRAL_M  AS M WITH(NOLOCK)" & _
  "ON M.RFM_ID = S.RFS_RFM_ID INNER JOIN REF.REFERRAL_D  AS D WITH(NOLOCK)" & _
" ON D.RFD_RFS_ID=S.RFS_ID WHERE (D.RFD_BSU_ID ='" & Session("sBsuid") & "' ))A WHERE RFS_ID<>0 "


        If txtName_Refd.Text.Trim <> "" Then
            str_Refd = " AND RFM_NAME LIKE '%" & txtName_Refd.Text.Trim & "%'"
        End If
        If txtName_Referral.Text.Trim <> "" Then
            str_Referral = " AND RFS_NAME LIKE '%" & txtName_Referral.Text.Trim & "%'"
        End If

        If txtMobNo.Text.Trim <> "" Then
            str_MobNo = " AND RFS_MOB LIKE '%" & txtMobNo.Text.Trim & "%'"
        End If
        DS = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query & str_Refd & str_Referral & str_MobNo & " ORDER BY RFM_NAME")
        If DS.Tables(0).Rows.Count > 0 Then

            gvRef.DataSource = DS.Tables(0)
            gvRef.DataBind()

            For i = 0 To DS.Tables(0).Rows.Count - 1
                bCHK_Flag = DS.Tables(0).Rows(i)("bCHK")
                If bCHK_Flag = True Then
                    If bBTNEnable = True Then
                        bBTNEnable = False
                        btnSave_tran.Enabled = False
                    End If

                End If


            Next




        Else
            DS.Tables(0).Rows.Add(DS.Tables(0).NewRow())

            'start the count from 1 no matter gridcolumn is visible or not
            DS.Tables(0).Rows(0)(0) = True
            DS.Tables(0).Rows(0)(1) = True
            gvRef.DataSource = DS.Tables(0)
            Try
                gvRef.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvRef.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvRef.Rows(0).Cells.Clear()
            gvRef.Rows(0).Cells.Add(New TableCell)
            gvRef.Rows(0).Cells(0).ColumnSpan = columnCount
            gvRef.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvRef.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

        End If


    End Sub

    Private Sub getSTUDENT_details()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim charSplit As String = "|"
        Dim Links As String() = New String(2) {}

        If Not Session("Link_Ref") Is Nothing Then
            Links = Session("Link_Ref").ToString.Split(charSplit)
        End If




        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@STU_EQS_ID", Links(0))
        PARAM(1) = New SqlParameter("@INFO_TYPE", Links(1))
        PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "REF.GetStudentDetail_Ref", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    ltStu_name.Text = Convert.ToString(DATAREADER("STU_NAME"))
                    ltGradeSct.Text = Convert.ToString(DATAREADER("GRM_DISPLAY"))
                    ltMobNo.Text = Convert.ToString(DATAREADER("PARENT_MOBILE"))
                    ltPrimCont.Text = Convert.ToString(DATAREADER("PARENT_NAME"))


                End While
            End If
        End Using




    End Sub

    Protected Sub btnSave_tran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave_tran.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then

            lblError.Text = "Record updated successfully"
            BindGrid()
        Else
            lblError.Text = errorMessage
        End If



    End Sub

    Private Function calltransaction(ByRef errorMessage As String) As Integer
        Dim Status As Integer
        Dim RFS_ID As String = String.Empty
        Dim transaction As SqlTransaction
        Dim param(6) As SqlParameter
        Dim charSplit As String = "|"
        Dim Links As String() = New String(2) {}
        Dim CHK As New CheckBox
        Dim LBLRFS_ID As New Label
        Dim bChecked As Boolean = False

        If Not Session("Link_Ref") Is Nothing Then
            Links = Session("Link_Ref").ToString.Split(charSplit)
        End If
        For Each ROW As GridViewRow In gvRef.Rows
            CHK = DirectCast(ROW.FindControl("chkSelect"), CheckBox)

            If CHK.Checked = True Then
                LBLRFS_ID = DirectCast(ROW.FindControl("lblRFS_ID"), Label)
                bChecked = True
                Exit For

            End If

        Next


        If bChecked = False Then
            lblError.Text = "Please select one checkbox  to link the referral !!!"
            Exit Function

        End If



        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                param(0) = New SqlParameter("@STU_EQS_ID", Links(0))
                param(1) = New SqlParameter("@INFO_TYPE", Links(1))
                param(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                param(3) = New SqlParameter("@RFS_ID", LBLRFS_ID.Text)
                param(4) = New SqlParameter("@USR_ID", Session("sUsr_id"))
                param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                param(5).Direction = ParameterDirection.ReturnValue


                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "REF.SAVEREFERREL_LINK", param)
                Status = param(5).Value

                If Status <> 0 Then
                    calltransaction = "1"
                    errorMessage = "Could not process the request"
                    Return "1"

                End If


                calltransaction = "0"

            Catch ex As Exception
                errorMessage = "Record could not be Updated"
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using






    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub
End Class
