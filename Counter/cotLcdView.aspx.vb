Imports System.Data
Imports System.Data.SqlClient

Partial Class Counter_cotLcdView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuid.Value = Session("sbsuid")
            BindGrid()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If
    End Sub
    Public Sub BindGrid()

        Dim dt As New DataTable
        dt.Columns.Add("TokenID")
        dt.Columns.Add("CounterID")
        Dim i = 0
        For i = 0 To 10
            Dim dr As DataRow = dt.NewRow()
            dr.Item("TokenID") = ""
            dr.Item("CounterID") = ""
            dt.Rows.Add(dr)
        Next

        GridData.DataSource = dt
        GridData.DataBind()


    End Sub
End Class
