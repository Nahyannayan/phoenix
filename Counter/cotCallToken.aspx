<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cotCallToken.aspx.vb" Inherits="Counter_cotCallToken" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Token Call</title>
     <script src="Javascript/JScript.js" type="text/javascript" ></script>
     <script src="Javascript/CallToken.js" type="text/javascript" ></script>
      <script type="text/javascript" >
    
      function  call(num)
      {
    
          if (document.getElementById('HiddenSound').value == 0)
          {
          playSound(num);
          }
      }
      
      function sound()
      {
     
      document.getElementById('HiddenSound').value = 1
      
      }
      
      </script>
</head>
<body bgcolor="black" >
    <form id="form1" runat="server">
    <div>
        <asp:ImageButton ID="ImageButton1" runat="server"  OnClientClick="javascript:BindGrid();return false;"  ImageUrl="~/Images/plus.JPG" />&nbsp;&nbsp;
        <asp:ImageButton ID="ImageButton2" runat="server"  OnClientClick="javascript:sound();return false;"  ImageUrl="~/Images/minus.JPG" />&nbsp;&nbsp;
        <asp:Panel ID="Panel1" runat="server" Height="50px" Width="125px">
        <asp:HiddenField ID="HiddenBsuid" runat="server" />
        <asp:HiddenField ID="Hiddenrefreshflag" Value="0" runat="server" />
        <asp:HiddenField ID="HiddenTokenvalue" Value="TokenValue" runat="server" />
        <asp:HiddenField ID="HiddenSound" Value="0" runat="server" />
        <asp:HiddenField ID="val" Value="" runat="server" />
        </asp:Panel>
    </div>
   
    </form>
</body>
</html>
