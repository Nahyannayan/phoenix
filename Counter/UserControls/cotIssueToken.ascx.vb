Imports System.Data
Imports Microsoft.ApplicationBlocks.Data


Partial Class Counter_UserControls_cotIssueToken
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBSU_ID.Value = Session("sbsuid") '125016 '
            HiddenPrint.Value = 0
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringCounter").ConnectionString
            Dim str_query = "select * from PRINTER_PORT_MASTER where BSU_ID='" & HiddenBSU_ID.Value & "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                HiddenPrinterPortNo.Value = ds.Tables(0).Rows(0).Item("PRINTER_PORT").ToString()
                HiddenPrinterBsuName.Value = ds.Tables(0).Rows(0).Item("TOKEN_PRINT_BSU_NAME").ToString()
            End If


        End If

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        
        Talk(Button1.Text)

    End Sub

    Public Sub Talk(ByVal Text As String)
        Try
            If lblNo.Text.IndexOf("Invalid") > -1 Then
                lblNo.Text = ""
            End If
            lblNo.Text = lblNo.Text & Text

            'voice.Speak(Text)
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button2.Text)
    End Sub


    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button3.Text)
    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button4.Text)
    End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button5.Text)
    End Sub

    Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button6.Text)
    End Sub

    Protected Sub Button7_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button7.Text)
    End Sub

    Protected Sub Button8_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button8.Text)
    End Sub

    Protected Sub Button9_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button9.Text)
    End Sub

    Protected Sub Button11_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Talk(Button11.Text)
    End Sub

    
    Protected Sub Button10_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblNo.Text = ""
    End Sub

    Protected Sub Button12_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If lblNo.Text.IndexOf("Invalid") > -1 Then
            lblNo.Text = ""
        End If

        If lblNo.Text.Length > 0 Then
            lblNo.Text = lblNo.Text.Substring(0, lblNo.Text.Length - 1)
        End If
    End Sub

    Function ValidateFeeId(ByVal FeeId As String) As Boolean
        Dim Valid As Boolean = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT count(*) from  STUDENT_M where STU_FEE_ID='" & FeeId & "' and STU_BSU_ID='" & HiddenBSU_ID.Value & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_query)

        If val = 0 Then
            Valid = False
        End If

        Return Valid
    End Function

    Function ValidatePreviousIssued(ByVal Feeid As String) As Boolean
        Dim Valid As Boolean = True


        Return Valid
    End Function
    Protected Sub Button13_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If ValidateFeeId(lblNo.Text.Trim()) Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringCounter").ConnectionString

            Dim PreviousIssued = ValidatePreviousIssued(lblNo.Text.Trim())
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_FEE_ID", lblNo.Text.Trim())
            pParms(1) = New SqlClient.SqlParameter("@COUNTER_BSU_ID", HiddenBSU_ID.Value)
            Dim TokenNo = SqlHelper.ExecuteScalar(str_conn, Data.CommandType.StoredProcedure, "COUNTER_ISSUE_TOKEN", pParms)

            HiddenTokenNumber.Value = TokenNo


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, Data.CommandType.StoredProcedure, "GET_STUDENT_NAME_AND_NUMBER", pParms)

            If ds.Tables(0).Rows.Count > 0 Then
                Hiddenstudentname.Value = ds.Tables(0).Rows(0).Item("STU_NAME").ToString()
                HiddenStudentno.Value = ds.Tables(0).Rows(0).Item("STU_NO").ToString()
            End If

            lblNo.Text = ""
            HiddenPrint.Value = 1
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Print", "<script type='text/javascript'> print1(); </script>  ")

        Else
            HiddenPrint.Value = 0
            lblNo.Text = "Invalid Fee ID."
            'voice.Speak("Invalid Fee ID")
        End If


    End Sub

    

End Class
