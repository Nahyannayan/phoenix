<%@ Control Language="VB" AutoEventWireup="false" CodeFile="cotRequestProcess.ascx.vb" Inherits="Counter_UserControls_cotRequestProcess" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:TextBox ID="TxtTokenId" EnableTheming="false"  runat="server" Height="35px" Font-Bold="True" Font-Size="Medium"></asp:TextBox>
<asp:ImageButton ID="btnnext" runat="server" Height="30px" ImageUrl="~/Images/Next.gif" Width="30px" />
<asp:HiddenField ID="HiddenCounter_id" runat="server" />
<asp:HiddenField ID="HiddenCounterTypeid" runat="server" /><asp:HiddenField ID="HiddenEmpId" runat="server" />
<asp:HiddenField ID="HiddenBsu_id" runat="server" />
</ContentTemplate>
</asp:UpdatePanel>
