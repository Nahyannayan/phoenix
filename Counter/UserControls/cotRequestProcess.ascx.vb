Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Counter_UserControls_cotRequestProcess
    Inherits System.Web.UI.UserControl

    ' Delegate declaration 
    Public Delegate Sub OnButtonClick(ByVal Stu_id As String)

    ' Event declaration 
    Public Event GetFeeId As OnButtonClick


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenCounter_id.Value = "1"
            HiddenEmpId.Value = "555"
            HiddenBsu_id.Value = "121009"

        End If
    End Sub

    Protected Sub btnnext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim TokenNo = ""
        Dim Stu_id = ""
        Dim ds As DataSet

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringCounter").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@COUNTER_ID", HiddenCounter_id.Value)
        pParms(1) = New SqlClient.SqlParameter("@COUNTER_BSU_ID", HiddenBsu_id.Value)
        pParms(2) = New SqlClient.SqlParameter("@COUNTER_EMP_ID", HiddenEmpId.Value)
        ds = SqlHelper.ExecuteDataset(str_conn, Data.CommandType.StoredProcedure, "COUNTER_CALL_NEXT_TOKEN_NUMBER", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            TokenNo = ds.Tables(0).Rows(0).Item("TOKEN_NUMBER").ToString()
            Stu_id = ds.Tables(0).Rows(0).Item("STU_ID").ToString()
            TxtTokenId.Text = TokenNo
            RaiseEvent GetFeeId(Stu_id)
        Else
            TxtTokenId.Text = "No Waiting Queue"
        End If
        
    End Sub


End Class
