﻿<!--
// Preload and play audio files with event handler (MouseOver sound)
// designed by JavaScript Archive, (c)1999
// Get more free javascripts at http://jsarchive.8m.com

var aySound = new Array();
// Below: source for sound files to be preloaded
aySound[0] = "Sounds/0.mp3";
aySound[1] = "Sounds/1.mp3";
aySound[2] = "Sounds/2.mp3";
aySound[3] = "Sounds/3.mp3";
aySound[4] = "Sounds/4.mp3";
aySound[5] = "Sounds/5.mp3";
aySound[6] = "Sounds/6.mp3";
aySound[7] = "Sounds/7.mp3";
aySound[8] = "Sounds/8.mp3";
aySound[9] = "Sounds/9.mp3";
aySound[10] = "Sounds/bell.mp3";

// DO NOT edit below this line
document.write('<BGSOUND ID="auIEContainer">')
IE = (navigator.appVersion.indexOf("MSIE")!=-1 && document.all)? 1:0;
NS = (navigator.appName=="Netscape" && navigator.plugins["LiveAudio"])? 1:0;
ver4 = IE||NS? 1:0;
onload=auPreload;

function auPreload() {
if (!ver4) return;
if (NS) auEmb = new Layer(0,window);
else {
Str = "<DIV ID='auEmb' STYLE='position:absolute;'></DIV>";
document.body.insertAdjacentHTML("BeforeEnd",Str);
}
var Str = '';
for (i=0;i<aySound.length;i++)
Str += "<EMBED SRC='"+aySound[i]+"' AUTOSTART='FALSE' HIDDEN='TRUE'>"
if (IE) auEmb.innerHTML = Str;
else {
auEmb.document.open();
auEmb.document.write(Str);
auEmb.document.close();
}
auCon = IE? document.all.auIEContainer:auEmb;
auCon.control = auCtrl;
}
function auCtrl(whSound,play) {
if (IE) this.src = play? aySound[whSound]:'';
else eval("this.document.embeds[whSound]." + (play? "play()":"stop()"))
}
function playSound(whSound) { if (window.auCon) auCon.control(whSound,true); }
function stopSound(whSound) { if (window.auCon) auCon.control(whSound,false); }
//-->

