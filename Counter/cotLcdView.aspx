<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cotLcdView.aspx.vb" Inherits="Counter_cotLcdView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>LCD Display</title>
     <base target="_self" />
    <link href="cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script src="Javascript/CounterDisplay.js" type="text/javascript" ></script>
<script type="text/javascript" >
function Button1_onclick() {
window.close();
}

</script>
</head>
<body onload="BindGrid();" bgcolor="black"  >
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="GridData" runat="server" Width="100%" AutoGenerateColumns="false">
            <Columns>

                <asp:TemplateField HeaderText="Counter No.">
                    <HeaderTemplate>
                        <center>
                            Counter No.</center>
                    </HeaderTemplate>
                    <ItemTemplate>
                       <center><%#Eval("CounterID")%></center>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Token &nbsp; No.">
                    <HeaderTemplate>
                        <center>
                            Token  No.</center>
                    </HeaderTemplate>
                    <ItemTemplate>
                     <center><%#Eval("TokenID")%></center> 
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
           <HeaderStyle CssClass="subheader_img" Height="30px" Font-Size="Medium" Font-Bold="true" />
          <%--  <RowStyle Height="100px" Font-Size="100px" ForeColor="white" Font-Bold="true" HorizontalAlign="Center" Wrap="False" />--%>
          <RowStyle CssClass="Grid_LcdDisplay" HorizontalAlign="Center" Wrap="False" />
                        
        </asp:GridView>
       <iframe src="cotCallToken.aspx" frameborder="0" scrolling="no" ></iframe>

    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <input accessKey="X" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: transparent" id="Button1" type=button onclick="return Button1_onclick()" />
    </div>
    </form>
</body>
</html>
