﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Telerik.Web.UI
Partial Class PDP_V2_pdpFinal
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim TAB_MANDATORY As New Hashtable
    Dim TAB_TITLE As New Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            lbtnStep1.Attributes.Add("class", "cssStepBtnActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep2.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep3.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep4.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep5.Attributes.Add("class", "cssStepBtnInActiveDisable col-md-2 col-lg-2 col-sm-2")
            tb_ObjectivesCurrent.Visible = True     'TAB-1
            tb_CareerAspirations.Visible = False    'TAB-2
            tb_developmentGoals.Visible = False              'TAB-3
            tb_summary.Visible = False
            ViewState("EMP_ID") = ""
            ViewState("EPR_ID") = ""
            ViewState("RVW_EMP_ID") = ""

            Bind_KRA()
            Bind_Mobility()
            divWtg.Visible = False




            If Not (Request.QueryString("U") Is Nothing) Then
                ViewState("EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("U").Replace(" ", "+"))
            Else
                ViewState("EMP_ID") = "0"
            End If
            If Not (Request.QueryString("ER") Is Nothing) Then
                ViewState("EPR_ID") = Encr_decrData.Decrypt(Request.QueryString("ER").Replace(" ", "+"))
            Else
                ViewState("EPR_ID") = "0"
            End If
            If Not (Request.QueryString("MR") Is Nothing) Then
                ViewState("RVW_EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("MR").Replace(" ", "+"))
            Else
                ViewState("RVW_EMP_ID") = "0"
            End If

            'Staff
            'ViewState("EMP_ID") = "10161"
            'ViewState("EPR_ID") = "1"
            'ViewState("RVW_EMP_ID") = "10161"
            'Line Mgr
            'Session("sUsr_name") = "prem.sunder"
            'ViewState("EMP_ID") = "24322"
            'ViewState("EPR_ID") = "168"
            'ViewState("RVW_EMP_ID") = "9235"
            'Session("EmployeeId") = "9235"
            'Super User
            'ViewState("EMP_ID") = "10161"
            'ViewState("EPR_ID") = "1"
            'ViewState("RVW_EMP_ID") = "19604"


            If ((ViewState("RVW_EMP_ID") = "0") And (ViewState("EPR_ID") = "0") And (ViewState("RVW_EMP_ID") = "0")) Then

                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                lbtnRevert.Visible = False
            Else
                If (ValidateUser() = True) Then

                    BIND_EMP_OBJECTIVES()
                    BIND_CAREER_DEVEL()
                    Bind_Summary()
                    Bind_GetPDP_CompletedDate()
                    btn_SaveFinish.Visible = False
                    lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
                    lbtnStep3.Attributes.Add("class", "cssStepBtnComp")
                    lbtnStep5.Attributes.Add("class", "cssStepBtnComp")
                Else
                    btn_Next.Visible = False
                    btn_Previous.Visible = False
                    btn_SaveDraft.Visible = False
                    btn_SaveFinish.Visible = False
                    lbtnRevert.Visible = False
                End If

            End If
            'Get_Emp_BSU_TAG()

            ' If ViewState("EMP_BSU_TAG") = "G" Then
            'spMan.InnerText = "Min   5 & Max 10 objectives required"
            'Else
            spMan.InnerText = "Min   3 & Max 10 objectives required"
            ' End If
            Set_ButtonViewRights()
            ENABLE_STATUS()
            check_published()
        End If
    End Sub
    Private Sub Set_ButtonViewRights()
        final_READONLY()
        btn_Submit.Visible = False
        If ViewState("IS_EMP") = True Then  'In the case of  employee
            btn_SaveFinish.Text = "Save & Finish"
            cbeConfirmPDP.ConfirmText = "Are you sure you want to submit the GEMS Performance Development Plan form to the next reviewer?"
            btn_SaveFinish.Visible = False
            btn_Next.Text = "Next"
            btn_Next.Visible = True
            lbtnRevert.Visible = False
            status_readonly()
            If ViewState("INT_FINAL_FINISH") = 0 Then
                ViewState("INFO_TYPE") = 1 ' when staff not yet updated pdp

            ElseIf (ViewState("INT_FINAL_FINISH") = 1) Then


                status_readonly()
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
            End If
        ElseIf ViewState("IS_MGR") = True Then  'In the case of  Line Mgr
            btn_SaveFinish.Text = "Approve"
            btn_Next.Text = "Next"
            btn_SaveFinish.Visible = False
            btn_Next.Visible = True
            btn_SaveDraft.Visible = True

            If ViewState("USR_LVL_FINISH") = 1 Then
                lbtnRevert.Visible = True
            Else
                lbtnRevert.Visible = False
            End If

            cbeConfirmPDP.ConfirmText = "Are you sure you want to submit the GEMS Performance Development Plan form?"

            status_readonly()
            If ViewState("INT_FINAL_FINISH") = 0 Then
                ViewState("INFO_TYPE") = 1

            Else


                status_readonly()

                ViewState("INFO_TYPE") = 0

                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False

            End If

            ElseIf ViewState("IS_VIEW_USER") = True Then  'In the case of  super user Mgr Mgr Line
                ViewState("INFO_TYPE") = 0
                ViewState("INT_FINAL_FINISH") = 1
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                lbtnRevert.Visible = False
                MAKE_READONLY()
                ' status_readonly()

            Else 'for all the other users
                ViewState("INFO_TYPE") = 0
                ViewState("INT_FINAL_FINISH") = 1
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                lbtnRevert.Visible = False
                MAKE_READONLY()
                'status_readonly()
            End If

    End Sub

    Private Sub status_readonly()
        If ViewState("IS_MGR") = True Then
            txt_FINAL_REV1.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV2.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV3.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV4.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV5.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV6.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV7.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV8.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV9.Attributes.Add("readonly", "readonly")
            txt_FINAL_REV10.Attributes.Add("readonly", "readonly")



            'hmeRating1.Enabled = False
            'hmeRating2.Enabled = False
            'hmeRating3.Enabled = False
            'hmeRating4.Enabled = False
            'hmeRating5.Enabled = False
            'hmeRating7.Enabled = False
            'hmeRating8.Enabled = False
            'hmeRating9.Enabled = False
            'hmeRating10.Enabled = False
            'hmeRating11.Enabled = False

            'divRating1.Attributes.Add("class", "divRatingDisable")
            'divRating2.Attributes.Add("class", "divRatingDisable")
            'divRating3.Attributes.Add("class", "divRatingDisable")
            'divRating4.Attributes.Add("class", "divRatingDisable")
            'divRating5.Attributes.Add("class", "divRatingDisable")
            'divRating7.Attributes.Add("class", "divRatingDisable")
            'divRating8.Attributes.Add("class", "divRatingDisable")
            'divRating9.Attributes.Add("class", "divRatingDisable")
            'divRating10.Attributes.Add("class", "divRatingDisable")
            'divRating11.Attributes.Add("class", "divRatingDisable")


            txt_carrierInputComment.Attributes.Add("readonly", "readonly")
            txtGoalRemarks1.Attributes.Add("readonly", "readonly")
            txtGoalRemarks2.Attributes.Add("readonly", "readonly")
            txtGoalRemarks3.Attributes.Add("readonly", "readonly")
            txtGoalRemarks4.Attributes.Add("readonly", "readonly")
        ElseIf ViewState("IS_EMP") = True Then

            txt_FINAL_MGR1.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR2.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR3.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR4.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR5.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR6.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR7.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR8.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR9.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR10.Attributes.Add("readonly", "readonly")

            txt_CarrierInputMgrComment.Attributes.Add("readonly", "readonly")


            txtMgrGoalRemarks1.Attributes.Add("readonly", "readonly")
            txtMgrGoalRemarks2.Attributes.Add("readonly", "readonly")
            txtMgrGoalRemarks3.Attributes.Add("readonly", "readonly")
            txtMgrGoalRemarks4.Attributes.Add("readonly", "readonly")

            txt_mgrOverallSummary.Attributes.Add("readonly", "readonly")
            txt_EmpOverallSummary.Attributes.Add("readonly", "readonly")
            hmeOverall.Enabled = False
            divOverallRating.Attributes.Add("class", "divRatingDisable")

            



        End If
        If (ViewState("INT_FINAL_FINISH") = 1) Then
            MAKE_READONLY()
        End If
    End Sub
    Private Sub MAKE_READONLY()
        ddl_mobility.Enabled = False
        'ddlKRA_1.Attributes.Add("readonly", "readonly")
        'ddlKRA_2.Attributes.Add("readonly", "readonly")
        'ddlKRA_3.Attributes.Add("readonly", "readonly")
        'ddlKRA_4.Attributes.Add("readonly", "readonly")
        'ddlKRA_5.Attributes.Add("readonly", "readonly")
        'ddlKRA_6.Attributes.Add("readonly", "readonly")
        'ddlKRA_7.Attributes.Add("readonly", "readonly")
        'ddlKRA_8.Attributes.Add("readonly", "readonly")
        'ddlKRA_9.Attributes.Add("readonly", "readonly")
        'ddlKRA_10.Attributes.Add("readonly", "readonly")

        txt_OBJ1.Attributes.Add("readonly", "readonly")
        txt_OBJ2.Attributes.Add("readonly", "readonly")
        txt_OBJ3.Attributes.Add("readonly", "readonly")
        txt_OBJ4.Attributes.Add("readonly", "readonly")
        txt_OBJ5.Attributes.Add("readonly", "readonly")
        txt_OBJ6.Attributes.Add("readonly", "readonly")
        txt_OBJ7.Attributes.Add("readonly", "readonly")
        txt_OBJ8.Attributes.Add("readonly", "readonly")
        txt_OBJ9.Attributes.Add("readonly", "readonly")
        txt_OBJ10.Attributes.Add("readonly", "readonly")

        txt_KPI1.Attributes.Add("readonly", "readonly")
        txt_KPI2.Attributes.Add("readonly", "readonly")
        txt_KPI3.Attributes.Add("readonly", "readonly")
        txt_KPI4.Attributes.Add("readonly", "readonly")
        txt_KPI5.Attributes.Add("readonly", "readonly")
        txt_KPI6.Attributes.Add("readonly", "readonly")
        txt_KPI7.Attributes.Add("readonly", "readonly")
        txt_KPI8.Attributes.Add("readonly", "readonly")
        txt_KPI9.Attributes.Add("readonly", "readonly")
        txt_KPI10.Attributes.Add("readonly", "readonly")

        txtWTG1.Attributes.Add("readonly", "readonly")
        txtWTG2.Attributes.Add("readonly", "readonly")
        txtWTG3.Attributes.Add("readonly", "readonly")
        txtWTG4.Attributes.Add("readonly", "readonly")
        txtWTG5.Attributes.Add("readonly", "readonly")
        txtWTG6.Attributes.Add("readonly", "readonly")
        txtWTG7.Attributes.Add("readonly", "readonly")
        txtWTG8.Attributes.Add("readonly", "readonly")
        txtWTG9.Attributes.Add("readonly", "readonly")
        txtWTG10.Attributes.Add("readonly", "readonly")

       

        txt_CarrierInput.Attributes.Add("readonly", "readonly")
        txt_ProfDevelNeeds.Attributes.Add("readonly", "readonly")
       

        txt_developGoals1.Attributes.Add("readonly", "readonly")
        txt_developGoals2.Attributes.Add("readonly", "readonly")
        txt_developGoals3.Attributes.Add("readonly", "readonly")
        txt_developGoals4.Attributes.Add("readonly", "readonly")

        txt_ExpectedOutcome1.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome2.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome3.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome4.Attributes.Add("readonly", "readonly")


        txt_FINAL_REV1.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV2.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV3.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV4.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV5.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV6.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV7.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV8.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV9.Attributes.Add("readonly", "readonly")
        txt_FINAL_REV10.Attributes.Add("readonly", "readonly")



        hmeRating1.Enabled = False
        hmeRating2.Enabled = False
        hmeRating3.Enabled = False
        hmeRating4.Enabled = False
        hmeRating5.Enabled = False
        hmeRating7.Enabled = False
        hmeRating8.Enabled = False
        hmeRating9.Enabled = False
        hmeRating10.Enabled = False
        hmeRating11.Enabled = False

        divRating1.Attributes.Add("class", "divRatingDisable")
        divRating2.Attributes.Add("class", "divRatingDisable")
        divRating3.Attributes.Add("class", "divRatingDisable")
        divRating4.Attributes.Add("class", "divRatingDisable")
        divRating5.Attributes.Add("class", "divRatingDisable")
        divRating7.Attributes.Add("class", "divRatingDisable")
        divRating8.Attributes.Add("class", "divRatingDisable")
        divRating9.Attributes.Add("class", "divRatingDisable")
        divRating10.Attributes.Add("class", "divRatingDisable")
        divRating11.Attributes.Add("class", "divRatingDisable")


        txt_carrierInputComment.Attributes.Add("readonly", "readonly")
        txtGoalRemarks1.Attributes.Add("readonly", "readonly")
        txtGoalRemarks2.Attributes.Add("readonly", "readonly")
        txtGoalRemarks3.Attributes.Add("readonly", "readonly")
        txtGoalRemarks4.Attributes.Add("readonly", "readonly")

        txt_FINAL_MGR1.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR2.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR3.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR4.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR5.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR6.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR7.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR8.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR9.Attributes.Add("readonly", "readonly")
        txt_FINAL_MGR10.Attributes.Add("readonly", "readonly")

        txt_CarrierInputMgrComment.Attributes.Add("readonly", "readonly")


        txtMgrGoalRemarks1.Attributes.Add("readonly", "readonly")
        txtMgrGoalRemarks2.Attributes.Add("readonly", "readonly")
        txtMgrGoalRemarks3.Attributes.Add("readonly", "readonly")
        txtMgrGoalRemarks4.Attributes.Add("readonly", "readonly")

        txt_mgrOverallSummary.Attributes.Add("readonly", "readonly")
        txt_EmpOverallSummary.Attributes.Add("readonly", "readonly")
        hmeOverall.Enabled = False
        divOverallRating.Attributes.Add("class", "divRatingDisable")


    End Sub
    Private Sub final_READONLY()
        ddl_mobility.Enabled = False
        'ddlKRA_1.Attributes.Add("readonly", "readonly")
        'ddlKRA_2.Attributes.Add("readonly", "readonly")
        'ddlKRA_3.Attributes.Add("readonly", "readonly")
        'ddlKRA_4.Attributes.Add("readonly", "readonly")
        'ddlKRA_5.Attributes.Add("readonly", "readonly")
        'ddlKRA_6.Attributes.Add("readonly", "readonly")
        'ddlKRA_7.Attributes.Add("readonly", "readonly")
        'ddlKRA_8.Attributes.Add("readonly", "readonly")
        'ddlKRA_9.Attributes.Add("readonly", "readonly")
        'ddlKRA_10.Attributes.Add("readonly", "readonly")

        txt_OBJ1.Attributes.Add("readonly", "readonly")
        txt_OBJ2.Attributes.Add("readonly", "readonly")
        txt_OBJ3.Attributes.Add("readonly", "readonly")
        txt_OBJ4.Attributes.Add("readonly", "readonly")
        txt_OBJ5.Attributes.Add("readonly", "readonly")
        txt_OBJ6.Attributes.Add("readonly", "readonly")
        txt_OBJ7.Attributes.Add("readonly", "readonly")
        txt_OBJ8.Attributes.Add("readonly", "readonly")
        txt_OBJ9.Attributes.Add("readonly", "readonly")
        txt_OBJ10.Attributes.Add("readonly", "readonly")

        txt_KPI1.Attributes.Add("readonly", "readonly")
        txt_KPI2.Attributes.Add("readonly", "readonly")
        txt_KPI3.Attributes.Add("readonly", "readonly")
        txt_KPI4.Attributes.Add("readonly", "readonly")
        txt_KPI5.Attributes.Add("readonly", "readonly")
        txt_KPI6.Attributes.Add("readonly", "readonly")
        txt_KPI7.Attributes.Add("readonly", "readonly")
        txt_KPI8.Attributes.Add("readonly", "readonly")
        txt_KPI9.Attributes.Add("readonly", "readonly")
        txt_KPI10.Attributes.Add("readonly", "readonly")

        txtWTG1.Attributes.Add("readonly", "readonly")
        txtWTG2.Attributes.Add("readonly", "readonly")
        txtWTG3.Attributes.Add("readonly", "readonly")
        txtWTG4.Attributes.Add("readonly", "readonly")
        txtWTG5.Attributes.Add("readonly", "readonly")
        txtWTG6.Attributes.Add("readonly", "readonly")
        txtWTG7.Attributes.Add("readonly", "readonly")
        txtWTG8.Attributes.Add("readonly", "readonly")
        txtWTG9.Attributes.Add("readonly", "readonly")
        txtWTG10.Attributes.Add("readonly", "readonly")



        txt_CarrierInput.Attributes.Add("readonly", "readonly")
        txt_ProfDevelNeeds.Attributes.Add("readonly", "readonly")


        txt_developGoals1.Attributes.Add("readonly", "readonly")
        txt_developGoals2.Attributes.Add("readonly", "readonly")
        txt_developGoals3.Attributes.Add("readonly", "readonly")
        txt_developGoals4.Attributes.Add("readonly", "readonly")

        txt_ExpectedOutcome1.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome2.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome3.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome4.Attributes.Add("readonly", "readonly")


        


    End Sub
    Private Sub Bind_Mobility()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_MOBILITY_M", pParms)

        ddl_mobility.DataSource = ds
        ddl_mobility.DataValueField = "MOB_ID"
        ddl_mobility.DataTextField = "MOB_DESCR"
        ddl_mobility.DataBind()

    End Sub

    Private Sub Get_Emp_BSU_TAG()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_EMP_BSU_TAG", pParms)
        ViewState("EMP_BSU_TAG") = ds.Tables(0).Rows(0).Item("BS_TAG")

    End Sub
    Private Sub Bind_KRA()
        'Dim conn As String = ConnectionManger.GetOASIS_PDP_V2ConnectionString
        'Dim pParms(1) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@INFO_TYPE", "KRA")

        'ddlKRA_1.Items.Clear()
        'ddlKRA_2.Items.Clear()
        'ddlKRA_3.Items.Clear()
        'ddlKRA_4.Items.Clear()
        'ddlKRA_5.Items.Clear()
        'ddlKRA_6.Items.Clear()
        'ddlKRA_7.Items.Clear()
        'ddlKRA_8.Items.Clear()
        'ddlKRA_9.Items.Clear()
        'ddlKRA_10.Items.Clear()
        'ddlKRA_1.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_2.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_3.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_4.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_5.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_6.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_7.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_8.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_9.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'ddlKRA_10.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        'Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "S_PDP.BIND_MASTER_DETAILS", pParms)
        '    While datareader.Read
        '        ddlKRA_1.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_2.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_3.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_4.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_5.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_6.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_7.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_8.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_9.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '        ddlKRA_10.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
        '    End While
        'End Using

    End Sub
    Private Sub BIND_EMP_OBJECTIVES()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_V3ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PDP_N.BIND_EMP_OBJECTIVES", pParms)
            While datareader.Read
                '-----------------------------1---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "1" Then
                    'If Not ddlKRA_1.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_1.ClearSelection()
                    '    ddlKRA_1.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    ' ddlKRA_1.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ1.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI1.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    ' ddlStatus1.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV1.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch1.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV1.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR1.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating1.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))

                    spWMRating1.InnerText = ""
                    spNoRating1.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))

                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG1.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------2---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "2" Then
                    'If Not ddlKRA_2.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_2.ClearSelection()
                    '    ddlKRA_2.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    ' ddlKRA_2.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ2.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI2.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    '  ddlStatus2.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV2.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch2.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV2.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR2.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating2.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating2.InnerText = ""
                    spNoRating2.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG2.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------3---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "3" Then
                    'If Not ddlKRA_3.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_3.ClearSelection()
                    '    ddlKRA_3.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    ' ddlKRA_3.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ3.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI3.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    ' ddlStatus3.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV3.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch3.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV3.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR3.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating3.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating3.InnerText = ""
                    spNoRating3.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG3.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If


                '-----------------------------4---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "4" Then
                    'If Not ddlKRA_4.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_4.ClearSelection()
                    '    ddlKRA_4.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    '  ddlKRA_4.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ4.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI4.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    ' ddlStatus4.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV4.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch4.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV4.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR4.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating4.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating4.InnerText = ""
                    spNoRating4.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG4.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------5---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "5" Then
                    'If Not ddlKRA_5.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_5.ClearSelection()
                    '    ddlKRA_5.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    ' ddlKRA_5.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ5.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI5.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    ' ddlStatus5.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV5.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch5.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV5.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR5.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating5.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating5.InnerText = ""
                    spNoRating5.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG5.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------6---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "6" Then
                    'If Not ddlKRA_6.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_6.ClearSelection()
                    '    ddlKRA_6.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    ' ddlKRA_6.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ6.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI6.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    ' ddlStatus6.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV6.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch6.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV6.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR6.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating7.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating7.InnerText = ""
                    spNoRating7.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG6.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If


                '-----------------------------7---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "7" Then
                    'If Not ddlKRA_7.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_7.ClearSelection()
                    '    ddlKRA_7.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    ' ddlKRA_7.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ7.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI7.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    'ddlStatus7.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV7.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch7.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV7.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR7.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating8.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating8.InnerText = ""
                    spNoRating8.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG7.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If
                '-----------------------------8---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "8" Then
                    'If Not ddlKRA_8.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_8.ClearSelection()
                    '    ddlKRA_8.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    'ddlKRA_8.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ8.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI8.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    'ddlStatus8.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV8.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch8.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV8.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR8.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating9.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating9.InnerText = ""
                    spNoRating9.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG8.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------9---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "9" Then
                    'If Not ddlKRA_9.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_9.ClearSelection()
                    '    ddlKRA_9.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    ' ddlKRA_9.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ9.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI9.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    'ddlStatus9.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV9.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch9.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV9.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR9.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating10.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating10.InnerText = ""
                    spNoRating10.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG9.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------10---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "10" Then
                    'If Not ddlKRA_10.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                    '    ddlKRA_10.ClearSelection()
                    '    ddlKRA_10.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    'End If
                    ' ddlKRA_10.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ10.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI10.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    'ddlStatus10.SelectedValue = datareader("EOBJ_INTERIM_STATUS")
                    txt_REV10.InnerText = Convert.ToString(datareader("EOBJ_INTERIM_TEXT"))
                    spAch10.Attributes.Add("class", Convert.ToString(datareader("EOBJ_INTERIM_STATUS_CLASS")))
                    txt_FINAL_REV10.InnerText = Convert.ToString(datareader("EOBJ_KPI_COMMENTS"))
                    txt_FINAL_MGR10.InnerText = Convert.ToString(datareader("EOBJ_FINAL_COMMENTS"))
                    HF_Rating11.Value = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    spWMRating11.InnerText = ""
                    spNoRating11.InnerText = Convert.ToString(datareader("EOBJ_KPI_RTM_ID"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG10.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If


            End While
        End Using

    End Sub
    Private Sub BIND_CAREER_DEVEL()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_V3ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[PDP_N].[BIND_EMP_DEVELOPMENT]", pParms)
            While datareader.Read
                '-----------------------------1---------------------------
                If Convert.ToString(datareader("EDEV_ORDER")) = "1" Then
                    txt_developGoals1.InnerText = Convert.ToString(datareader("EDEV_GOAL"))
                    txt_ExpectedOutcome1.InnerText = Convert.ToString(datareader("EDEV_MEASURE"))
                    txtGoalRemarks1.InnerText = Convert.ToString(datareader("EDEV_REMARKS"))
                    txtMgrGoalRemarks1.InnerText = Convert.ToString(datareader("EDEV_MGR_REMARKS"))
                End If

                '-----------------------------2---------------------------
                If Convert.ToString(datareader("EDEV_ORDER")) = "2" Then
                    txt_developGoals2.InnerText = Convert.ToString(datareader("EDEV_GOAL"))
                    txt_ExpectedOutcome2.InnerText = Convert.ToString(datareader("EDEV_MEASURE"))
                    txtGoalRemarks2.InnerText = Convert.ToString(datareader("EDEV_REMARKS"))
                    txtMgrGoalRemarks2.InnerText = Convert.ToString(datareader("EDEV_MGR_REMARKS"))
                End If
                '-----------------------------3---------------------------
                If Convert.ToString(datareader("EDEV_ORDER")) = "3" Then
                    txt_developGoals3.InnerText = Convert.ToString(datareader("EDEV_GOAL"))
                    txt_ExpectedOutcome3.InnerText = Convert.ToString(datareader("EDEV_MEASURE"))
                    txtGoalRemarks3.InnerText = Convert.ToString(datareader("EDEV_REMARKS"))
                    txtMgrGoalRemarks3.InnerText = Convert.ToString(datareader("EDEV_MGR_REMARKS"))
                End If
                '-----------------------------4---------------------------
                If Convert.ToString(datareader("EDEV_ORDER")) = "4" Then
                    txt_developGoals4.InnerText = Convert.ToString(datareader("EDEV_GOAL"))
                    txt_ExpectedOutcome4.InnerText = Convert.ToString(datareader("EDEV_MEASURE"))
                    txtGoalRemarks4.InnerText = Convert.ToString(datareader("EDEV_REMARKS"))
                    txtMgrGoalRemarks4.InnerText = Convert.ToString(datareader("EDEV_MGR_REMARKS"))
                End If



            End While
        End Using


    End Sub
    Private Sub Bind_Summary()
        Dim msg As String = String.Empty
        Try

            Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))



            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "PDP_N.GET_EMP_FINAL_SUMMARY", pParms)
                While DATAREADER.Read
                    txt_mgrOverallSummary.InnerText = Convert.ToString(DATAREADER("EPR_FINAL_SUMMARY"))
                    txt_EmpOverallSummary.InnerText = Convert.ToString(DATAREADER("EPR_FINAL_EMP_COMMENTS"))

                    If (Convert.ToString(DATAREADER("EPR_FINAL_RTM_ID")) <> "") Then
                        SpOverallRatingWatermark.InnerText = ""
                    End If
                    HF_OverallRating.Value = Convert.ToString(DATAREADER("EPR_FINAL_RTM_ID"))
                    SpOverallRatingNo.InnerText = Convert.ToString(DATAREADER("EPR_FINAL_RTM_ID"))

                End While
            End Using



        Catch ex As Exception
            msg = ex.Message
        End Try

    End Sub
    Private Sub Bind_GetPDP_CompletedDate()
        Try


            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

                Using reader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "PDP_N.GET_PDP_FINAL_COMPLETED_DATE", pParms)
                    While reader.Read()
                        If (Convert.ToString(reader("EPL_LVL_ORDER")) = "0") Then
                            txtEmpSigned.Value = Convert.ToString(reader("EMP_NAME"))
                            txtEmpDate.Value = Convert.ToString(reader("EPL_LVL_FINISH_DATE"))
                        ElseIf (Convert.ToString(reader("EPL_LVL_ORDER")) = "1") Then
                            txtMgrSigned1.Value = Convert.ToString(reader("EMP_NAME"))
                            txtMgrDate1.Value = Convert.ToString(reader("EPL_LVL_FINISH_DATE"))
                        End If
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Function ValidateUser() As Boolean
        Try

            Dim MOBILITY_ID As String = String.Empty

            Dim bSHOW_DETAILS As Boolean
            Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
            pParms(2) = New SqlClient.SqlParameter("@REVIEWING_EMP_ID", ViewState("RVW_EMP_ID"))
            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "[PDP_N].[GET_EMP_PERF_M_FINAL_DETAIL]", pParms)

                While DATAREADER.Read
                    bSHOW_DETAILS = True


                    txt_EmpName.Value = Convert.ToString(DATAREADER("EMP_NAME"))
                    txt_role.Value = Convert.ToString(DATAREADER("DES_DESCR"))

                    txt_ManagerName.Value = Convert.ToString(DATAREADER("EMP_MANAGER_NAME"))
                    txt_bsu.Value = Convert.ToString(DATAREADER("BSU_NAME"))

                    txt_FromDate.Value = Convert.ToString(DATAREADER("PRD_START"))
                    txt_ToDate.Value = Convert.ToString(DATAREADER("PRD_END"))

                    empName.InnerHtml = Convert.ToString(DATAREADER("EMP_NAME"))
                    empDesig.InnerText = Convert.ToString(DATAREADER("DES_DESCR"))

                    txt_CarrierInput.InnerText = Convert.ToString(DATAREADER("EPR_CAREER_ASP"))
                    txt_ProfDevelNeeds.InnerText = Convert.ToString(DATAREADER("EPR_PROF_DEVEL"))
                    txt_carrierInputComment.InnerText = Convert.ToString(DATAREADER("EPR_EMP_PERF_COMMENTS"))
                    txt_CarrierInputMgrComment.InnerText = Convert.ToString(DATAREADER("EPR_MGR_PERF_COMMENTS"))

                    ViewState("IS_EMP") = Convert.ToString(DATAREADER("IS_EMP"))
                    ViewState("IS_MGR") = Convert.ToString(DATAREADER("IS_MGR"))
                    ViewState("IS_VIEW_USER") = Convert.ToString(DATAREADER("IS_VIEW_USER"))
                    ViewState("INT_FINAL_FINISH") = Convert.ToString(DATAREADER("INT_FINAL_FINISH"))
                    ViewState("FINAL_CMMT") = Convert.ToString(DATAREADER("FINAL_CMMT"))
                    ViewState("EPR_bPUBLISHED") = Convert.ToString(DATAREADER("EPR_bPUBLISHED"))
                    ViewState("USR_LVL_FINISH") = Convert.ToString(DATAREADER("USR_LVL_FINISH"))
                    MOBILITY_ID = Convert.ToString(DATAREADER("EPR_EPD_MOBILITY_ID"))

                    If (MOBILITY_ID <> "") Then
                        For Each LOC As String In MOBILITY_ID.Split("|")
                            If LOC.Trim() <> "" Then
                                For Each itm As ListItem In ddl_mobility.Items
                                    If (itm.Value = LOC) Then
                                        itm.Selected = True
                                    End If
                                Next
                            End If
                        Next
                    End If

                End While

            End Using



            Return bSHOW_DETAILS
        Catch ex As Exception
            Return False
        End Try
    End Function
    Protected Sub lbtnStep1_Click(sender As Object, e As EventArgs) Handles lbtnStep1.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        Dim StatusObjective As String = String.Empty
        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", "cssStepBtnComp")
        lbtnStep5.Attributes.Add("class", "cssStepBtnComp")
        lbtnStep1.Attributes.Add("class", "cssStepBtnActive")
        tb_ObjectivesCurrent.Visible = True    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-2
        tb_developmentGoals.Visible = False
        tb_summary.Visible = False
        'TAB-3
        'TAB-6
        btn_Previous.Visible = False
        btn_Next.Visible = True
        btn_SaveFinish.Visible = False
        btn_SaveDraft.Visible = True
        Set_ButtonViewRights()

    End Sub
    Protected Sub lbtnStep2_Click(sender As Object, e As EventArgs) Handles lbtnStep2.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = True    'TAB-2
        tb_developmentGoals.Visible = False     'TAB-3
        tb_summary.Visible = False
        Dim StatusObjective As String = String.Empty
        lbtnStep1.Attributes.Add("class", IIf(CHECK_OBJ_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", "cssStepBtnComp")
        lbtnStep2.Attributes.Add("class", "cssStepBtnActive")
        lbtnStep5.Attributes.Add("class", "cssStepBtnComp")
        btn_Next.Visible = True
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = False
        btn_SaveDraft.Visible = False
        Set_ButtonViewRights()

    End Sub
    Protected Sub lbtnStep3_Click(sender As Object, e As EventArgs) Handles lbtnStep3.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-2
        tb_developmentGoals.Visible = True     'TAB-3
        tb_summary.Visible = False
        Dim StatusObjective As String = String.Empty
        lbtnStep1.Attributes.Add("class", IIf(CHECK_OBJ_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", "cssStepBtnActive")
        lbtnStep5.Attributes.Add("class", "cssStepBtnComp")
        btn_Next.Visible = True
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = False
        btn_SaveDraft.Visible = False
        Set_ButtonViewRights()

    End Sub
    Protected Sub lbtnStep4_Click(sender As Object, e As EventArgs) Handles lbtnStep4.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
    End Sub
    Protected Sub lbtnStep5_Click(sender As Object, e As EventArgs) Handles lbtnStep5.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-2
        tb_developmentGoals.Visible = False     'TAB-3
        tb_summary.Visible = True
        Dim StatusObjective As String = String.Empty
        lbtnStep1.Attributes.Add("class", IIf(CHECK_OBJ_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", "cssStepBtnComp")
        lbtnStep5.Attributes.Add("class", "cssStepBtnActive")
        btn_Next.Visible = False
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = True
        btn_SaveDraft.Visible = False
        If ViewState("INT_FINAL_FINISH") = 1 Then
            Set_ButtonViewRights()
            Final_emp_cmmt()
        End If
    End Sub
    Protected Sub btn_Previous_Click(sender As Object, e As EventArgs) Handles btn_Previous.Click
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_V3Connection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If tb_CareerAspirations.Visible = True Then 'TAB-2
                    lbtnStep1_Click(lbtnStep1, Nothing)
                    Save_CareerAspirations(transaction)
                    txt_EmpName.Focus()

                ElseIf tb_developmentGoals.Visible = True Then 'TAB-4
                    lbtnStep2_Click(lbtnStep2, Nothing)
                    SAVE_CAREER_DEVELOPMENT(transaction)
                    txt_EmpName.Focus()
                ElseIf tb_summary.Visible = True Then 'TAB-4
                    lbtnStep3_Click(lbtnStep3, Nothing)
                    Save_OverallSummary(transaction)
                    txt_EmpName.Focus()
                    Bind_Summary()
                End If
            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then
                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using


    End Sub
    Protected Sub btn_Next_Click(sender As Object, e As EventArgs) Handles btn_Next.Click
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_V3Connection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If tb_ObjectivesCurrent.Visible = True Then 'TAB-1

                    'Dim str_MandatoryMsg As String = ""
                    ''  str_MandatoryMsg = "<div>Following fields needs to be completed before Saving the Performance Development Plan</div>"
                    'Dim i As Integer = 0
                    'Dim OBJ As String = CHECK_OBJ_COMPLETED()
                    'If ViewState("IS_EMP") = True Then
                    '    If (OBJ <> "") Then
                    '        str_MandatoryMsg = str_MandatoryMsg + "<div>" + OBJ + "</div>"
                    '        i = +1
                    '        divNote.Visible = True
                    '        divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    '        lblError.Text = str_MandatoryMsg
                    '        Exit Sub
                    '    End If
                    'End If


                    lbtnStep2_Click(lbtnStep2, Nothing)
                    Save_Objectives(transaction)
                    BIND_EMP_OBJECTIVES()
                    txt_bsu.Focus()
                ElseIf tb_CareerAspirations.Visible = True Then 'TAB-3
                    lbtnStep3_Click(lbtnStep3, Nothing)
                    Save_CareerAspirations(transaction)
                    txt_bsu.Focus()
                ElseIf tb_developmentGoals.Visible = True Then 'TAB-4
                    lbtnStep5_Click(lbtnStep5, Nothing)
                    SAVE_CAREER_DEVELOPMENT(transaction)
                    txt_bsu.Focus()
                End If
            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using



    End Sub
    Private Sub Save_Objectives(sqltran As SqlTransaction)

        Dim pParms(31) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
   
        pParms(1) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS1", txt_FINAL_REV1.InnerText.Trim())
        pParms(2) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS1", txt_FINAL_MGR1.InnerText.Trim())
        pParms(3) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID1", HF_Rating1.Value)
    
        pParms(4) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS2", txt_FINAL_REV2.InnerText.Trim())
        pParms(5) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS2", txt_FINAL_MGR2.InnerText.Trim())
        pParms(6) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID2", HF_Rating2.Value)

        pParms(7) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS3", txt_FINAL_REV3.InnerText.Trim())
        pParms(8) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS3", txt_FINAL_MGR3.InnerText.Trim())
        pParms(9) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID3", HF_Rating3.Value)

        pParms(10) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS4", txt_FINAL_REV4.InnerText.Trim())
        pParms(11) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS4", txt_FINAL_MGR4.InnerText.Trim())
        pParms(12) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID4", HF_Rating4.Value)

        pParms(13) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS5", txt_FINAL_REV5.InnerText.Trim())
        pParms(14) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS5", txt_FINAL_MGR5.InnerText.Trim())
        pParms(15) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID5", HF_Rating5.Value)

        pParms(16) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS6", txt_FINAL_REV6.InnerText.Trim())
        pParms(17) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS6", txt_FINAL_MGR6.InnerText.Trim())
        pParms(18) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID6", HF_Rating7.Value)

        pParms(19) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS7", txt_FINAL_REV7.InnerText.Trim())
        pParms(20) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS7", txt_FINAL_MGR7.InnerText.Trim())
        pParms(21) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID7", HF_Rating8.Value)

        pParms(22) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS8", txt_FINAL_REV8.InnerText.Trim())
        pParms(23) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS8", txt_FINAL_MGR8.InnerText.Trim())
        pParms(24) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID8", HF_Rating9.Value)

        pParms(25) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS9", txt_FINAL_REV9.InnerText.Trim())
        pParms(26) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS9", txt_FINAL_MGR9.InnerText.Trim())
        pParms(27) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID9", HF_Rating10.Value)

        pParms(28) = New SqlClient.SqlParameter("@EOBJ_KPI_COMMENTS10", txt_FINAL_REV10.InnerText.Trim())
        pParms(29) = New SqlClient.SqlParameter("@EOBJ_FINAL_COMMENTS10", txt_FINAL_MGR10.InnerText.Trim())
        pParms(30) = New SqlClient.SqlParameter("@EOBJ_KPI_RTM_ID10", HF_Rating11.Value)

        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.SAVE_EMP_FINAL_OBJECTIVE", pParms)

    End Sub
    Private Sub SAVE_CAREER_DEVELOPMENT(sqltran As SqlTransaction)

        Dim PARAM(10) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

        PARAM(1) = New SqlClient.SqlParameter("@EDEV_REMARKS1", txtGoalRemarks1.InnerText.Trim())
        PARAM(2) = New SqlClient.SqlParameter("@EDEV_MGR_REMARKS1", txtMgrGoalRemarks1.InnerText.Trim())

        PARAM(3) = New SqlClient.SqlParameter("@EDEV_REMARKS2", txtGoalRemarks2.InnerText.Trim())
        PARAM(4) = New SqlClient.SqlParameter("@EDEV_MGR_REMARKS2", txtMgrGoalRemarks2.InnerText.Trim())

        PARAM(5) = New SqlClient.SqlParameter("@EDEV_REMARKS3", txtGoalRemarks3.InnerText.Trim())
        PARAM(6) = New SqlClient.SqlParameter("@EDEV_MGR_REMARKS3", txtMgrGoalRemarks3.InnerText.Trim())

        PARAM(7) = New SqlClient.SqlParameter("@EDEV_REMARKS4", txtGoalRemarks4.InnerText.Trim())
        PARAM(8) = New SqlClient.SqlParameter("@EDEV_MGR_REMARKS4", txtMgrGoalRemarks4.InnerText.Trim())


        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.SAVE_EMP_FINAL_DEVELOPMENT", PARAM)

    End Sub
    Private Sub Save_CareerAspirations(SQLTRAN As SqlTransaction)
        Dim allCheckedItems As New StringBuilder()
        For Each itm As ListItem In ddl_mobility.Items
            If (itm.Selected = True) Then
                allCheckedItems = allCheckedItems.Append(itm.Value)
                allCheckedItems = allCheckedItems.Append("|")
            End If
        Next

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(1) = New SqlClient.SqlParameter("@EPR_CAREER_ASP", txt_CarrierInput.InnerText.Trim())
        pParms(2) = New SqlClient.SqlParameter("@EPR_PROF_DEVEL", txt_ProfDevelNeeds.InnerText.Trim())
        pParms(3) = New SqlClient.SqlParameter("@EPR_EPD_MOBILITY_ID", allCheckedItems.ToString.Trim())
        pParms(4) = New SqlClient.SqlParameter("@EPR_EMP_PERF_COMMENTS", txt_carrierInputComment.InnerText.Trim())
        pParms(5) = New SqlClient.SqlParameter("@EPR_MGR_PERF_COMMENTS", txt_CarrierInputMgrComment.InnerText.Trim())

        SqlHelper.ExecuteNonQuery(SQLTRAN, CommandType.StoredProcedure, "PDP_N.SAVE_EMP_CAREER_ASP", pParms)
    End Sub
    Private Sub Save_OverallSummary(sqltran As SqlTransaction)
        Dim pParms(8) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(1) = New SqlClient.SqlParameter("@EPR_FINAL_RTM_ID", HF_OverallRating.Value)
        pParms(2) = New SqlClient.SqlParameter("@EPR_FINAL_SUMMARY", txt_mgrOverallSummary.InnerText.Trim())
        pParms(3) = New SqlClient.SqlParameter("@EPR_FINAL_EMP_COMMENTS", txt_EmpOverallSummary.InnerText.Trim())
        pParms(4) = New SqlClient.SqlParameter("@EMP_ID", ViewState("RVW_EMP_ID"))
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.SAVE_SUMMARY_FINAL", pParms)

    End Sub
    Protected Sub btn_SaveDraft_Click(sender As Object, e As EventArgs) Handles btn_SaveDraft.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""

        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Save_Objectives(sqltran)
            SAVE_CAREER_DEVELOPMENT(sqltran)
            Save_CareerAspirations(sqltran)
            If ViewState("IS_MGR") = True Then
                Save_OverallSummary(sqltran)
            End If
            sqltran.Commit()
            BIND_EMP_OBJECTIVES()
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
            lblError.Text = "<div>You have successfully saved the Performance Development Plan </div>"

        Catch ex As Exception
            sqltran.Rollback()

            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Error occured while saving </div>"
        End Try





    End Sub

    Protected Sub btn_SaveFinish_Click(sender As Object, e As EventArgs) Handles btn_SaveFinish.Click

        Dim str_MandatoryMsg As String = ""
        '  str_MandatoryMsg = "<div>Following fields needs to be completed before submitting the Performance Development Plan</div>"
        Dim i As Integer = 0
        Dim OBJ As String = CHECK_OBJ_COMPLETED()
        If ViewState("IS_EMP") = True Then
            If (OBJ <> "") Then
                str_MandatoryMsg = str_MandatoryMsg + "<div>" + OBJ + "</div>"
                i = +1
            End If
        End If
        If ViewState("IS_MGR") = True Then

            If (txt_mgrOverallSummary.InnerText.Trim() = "" Or HF_OverallRating.Value = "") Then

                str_MandatoryMsg = str_MandatoryMsg + "<div>In Step 4: Overall Summary and Rating required</div>"
                i = +1
            End If
        End If

        If (i = 0) Then
            Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
            Dim con As SqlConnection = New SqlConnection(connection)
            con.Open()
            Dim sqltran As SqlTransaction
            sqltran = con.BeginTransaction("trans")
            Dim bFinished As Integer = 0

            Try

                Save_Objectives(sqltran)
                SAVE_CAREER_DEVELOPMENT(sqltran)
                Save_CareerAspirations(sqltran)

                If ViewState("IS_MGR") = True Then
                    Save_OverallSummary(sqltran)
                End If
                Dim NXTLVL_EMP_ID As String = GET_NEXTLEVEL_REVIEWER(sqltran)
                If ViewState("INFO_TYPE") = 1 Then
                    FWD_TO_NEXTLEVEL_REVIEWER(sqltran, NXTLVL_EMP_ID)
                End If

                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False


                sqltran.Commit()
                bFinished = 1
            Catch ex As Exception
                bFinished = 0
                sqltran.Rollback()
            End Try
            If (bFinished = 1) Then
                aFTER_SAVE()

                If ViewState("IS_MGR") = True Then
                    Bind_Summary()
                End If

                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                If ViewState("IS_MGR") = True Then
                    lblError.Text = "<div>You have successfully approved the Performance Development Plan </div>"
                Else
                    lblError.Text = "<div>You have successfully completed the Performance Development Plan </div>"
                End If

            Else
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoError"
                lblError.Text = "<div>Error occured while saving </div>"
            End If
        Else
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = str_MandatoryMsg

        End If
    End Sub
    Private Function GET_NEXTLEVEL_REVIEWER(sqltran As SqlTransaction) As String

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", ViewState("INFO_TYPE"))
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(3) = New SqlClient.SqlParameter("@REVIEWING_EMP_ID", ViewState("RVW_EMP_ID"))
        Dim NXTLVL_EMP_ID As String = SqlHelper.ExecuteScalar(sqltran, CommandType.StoredProcedure, "PDP_N.GET_NEXTLEVEL_REVIEWER_FINAL", pParms)
        Return NXTLVL_EMP_ID
    End Function
    Private Sub FWD_TO_NEXTLEVEL_REVIEWER(sqltran As SqlTransaction, NXTLVL_EMP_ID As String)
        Dim TR As String = String.Empty
        Try
            Dim URL As String
            Dim StaffURL As String = String.Empty
            Dim StaffUrlPage As String = "https://school.gemsoasis.com/pdp/pdpCStaff.aspx"
            Dim urlPage As String = "https://school.gemsoasis.com/pdp/pdpCStaff.aspx" ' HttpContext.Current.Request.Url.AbsoluteUri
            'StaffUrlPage = urlPage

            'If urlPage.Contains("?") Then
            '    urlPage = urlPage.Remove(urlPage.IndexOf("?"))
            '    StaffUrlPage = urlPage.Remove(urlPage.IndexOf("?"))
            'End If

            'urlPage = urlPage.Replace("ApproveRequestFromEmail.aspx", "ApproveRequestFromEmail.aspx")
            URL = urlPage & "?U=" + Encr_decrData.Encrypt(Convert.ToString(ViewState("EMP_ID"))) + "&P=" + Encr_decrData.Encrypt(Convert.ToString(ViewState("EPR_ID"))) + "&R=" + Encr_decrData.Encrypt(NXTLVL_EMP_ID)
            StaffURL = StaffUrlPage & "?U=" + Encr_decrData.Encrypt(Convert.ToString(ViewState("EMP_ID"))) + "&P=" + Encr_decrData.Encrypt(Convert.ToString(ViewState("EPR_ID"))) + "&R=" + Encr_decrData.Encrypt(ViewState("EMP_ID"))
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", ViewState("INFO_TYPE"))
            pParms(1) = New SqlClient.SqlParameter("@URL", URL)
            pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
            pParms(3) = New SqlClient.SqlParameter("@STAFF_ID", ViewState("EMP_ID"))
            pParms(4) = New SqlClient.SqlParameter("@REVIEWER_EMP_ID", ViewState("RVW_EMP_ID"))
            pParms(5) = New SqlClient.SqlParameter("@NXTLVL_EMP_ID", NXTLVL_EMP_ID)
            pParms(6) = New SqlClient.SqlParameter("@StaffURL", StaffURL)
            SqlHelper.ExecuteScalar(sqltran, CommandType.StoredProcedure, "PDP_N.FWDTO_NEXTLEVEL_REVIEWER_FINAL", pParms)

        Catch ex As Exception
            TR = ex.Message
        End Try
    End Sub
    Private Sub aFTER_SAVE()
        Dim FLAG As Boolean = ValidateUser()
        BIND_EMP_OBJECTIVES()
        BIND_CAREER_DEVEL()
        Bind_GetPDP_CompletedDate()

        Set_ButtonViewRights()
    End Sub

    Protected Sub hf1_ServerClick(sender As Object, e As EventArgs) Handles hf1.ServerClick
        spPage.InnerHtml = "Page <font color='#58B0E7'>1</font> of 2"

        hf1.Attributes.Add("class", "current")
        hf2.Attributes.Add("class", " ")

        'ddlKRA_1.Visible = True
        'ddlKRA_2.Visible = True
        'ddlKRA_3.Visible = True
        'ddlKRA_4.Visible = True
        'ddlKRA_5.Visible = True

        'ddlKRA_6.Visible = False
        'ddlKRA_7.Visible = False
        'ddlKRA_8.Visible = False
        'ddlKRA_9.Visible = False
        'ddlKRA_10.Visible = False


        txt_OBJ1.Visible = True
        txt_OBJ2.Visible = True
        txt_OBJ3.Visible = True
        txt_OBJ4.Visible = True
        txt_OBJ5.Visible = True
        txt_OBJ6.Visible = False
        txt_OBJ7.Visible = False
        txt_OBJ8.Visible = False
        txt_OBJ9.Visible = False
        txt_OBJ10.Visible = False

        txt_KPI1.Visible = True
        txt_KPI2.Visible = True
        txt_KPI3.Visible = True
        txt_KPI4.Visible = True
        txt_KPI5.Visible = True
        txt_KPI6.Visible = False
        txt_KPI7.Visible = False
        txt_KPI8.Visible = False
        txt_KPI9.Visible = False
        txt_KPI10.Visible = False


        'ddlStatus1.Visible = True
        'ddlStatus2.Visible = True
        'ddlStatus3.Visible = True
        'ddlStatus4.Visible = True
        'ddlStatus5.Visible = True
        'ddlStatus6.Visible = False
        'ddlStatus7.Visible = False
        'ddlStatus8.Visible = False
        'ddlStatus9.Visible = False
        'ddlStatus10.Visible = False

        'divStatus1.visible = True
        'divStatus2.visible = True
        'divStatus3.visible = True
        'divStatus4.visible = True
        'divStatus5.visible = True
        'divStatus6.visible = False
        'divStatus7.visible = False
        'divStatus8.visible = False
        'divStatus9.visible = False
        'divStatus10.visible = False

        txt_REV1.Visible = True
        txt_REV2.Visible = True
        txt_REV3.Visible = True
        txt_REV4.Visible = True
        txt_REV5.Visible = True
        txt_REV6.Visible = False
        txt_REV7.Visible = False
        txt_REV8.Visible = False
        txt_REV9.Visible = False
        txt_REV10.Visible = False

        txt_FINAL_REV1.Visible = True
        txt_FINAL_REV2.Visible = True
        txt_FINAL_REV3.Visible = True
        txt_FINAL_REV4.Visible = True
        txt_FINAL_REV5.Visible = True
        txt_FINAL_REV6.Visible = False
        txt_FINAL_REV7.Visible = False
        txt_FINAL_REV8.Visible = False
        txt_FINAL_REV9.Visible = False
        txt_FINAL_REV10.Visible = False

        txt_FINAL_MGR1.Visible = True
        txt_FINAL_MGR2.Visible = True
        txt_FINAL_MGR3.Visible = True
        txt_FINAL_MGR4.Visible = True
        txt_FINAL_MGR5.Visible = True
        txt_FINAL_MGR6.Visible = False
        txt_FINAL_MGR7.Visible = False
        txt_FINAL_MGR8.Visible = False
        txt_FINAL_MGR9.Visible = False
        txt_FINAL_MGR10.Visible = False

        divRating1.Style.Add("display", "block")
        divRating2.Style.Add("display", "block")
        divRating3.Style.Add("display", "block")
        divRating4.Style.Add("display", "block")
        divRating5.Style.Add("display", "block")
        divRating7.Style.Add("display", "none")
        divRating8.Style.Add("display", "none")
        divRating9.Style.Add("display", "none")
        divRating10.Style.Add("display", "none")
        divRating11.Style.Add("display", "none")

        spAch1.Visible = True
        spAch2.Visible = True
        spAch3.Visible = True
        spAch4.Visible = True
        spAch5.Visible = True
        spAch6.Visible = False
        spAch7.Visible = False
        spAch8.Visible = False
        spAch9.Visible = False
        spAch10.Visible = False

        divWTG1.Visible = True
        divWTG2.Visible = True
        divWTG3.Visible = True
        divWTG4.Visible = True
        divWTG5.Visible = True
        divWTG6.Visible = False
        divWTG7.Visible = False
        divWTG8.Visible = False
        divWTG9.Visible = False
        divWTG10.Visible = False
        divNote.Visible = False

        ENABLE_STATUS()

        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_V3Connection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
               
                    Save_Objectives(transaction)
                  
            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                    BIND_EMP_OBJECTIVES()
                End If
            End Try
        End Using

    End Sub

    Protected Sub hf2_ServerClick(sender As Object, e As EventArgs) Handles hf2.ServerClick
        spPage.InnerHtml = "Page <font color='#58B0E7'>2</font> of 2"
        hf2.Attributes.Add("class", "current")
        hf1.Attributes.Add("class", " ")



        'ddlKRA_1.Visible = False
        'ddlKRA_2.Visible = False
        'ddlKRA_3.Visible = False
        'ddlKRA_4.Visible = False
        'ddlKRA_5.Visible = False

        'ddlKRA_6.Visible = True
        'ddlKRA_7.Visible = True
        'ddlKRA_8.Visible = True
        'ddlKRA_9.Visible = True
        'ddlKRA_10.Visible = True


        txt_OBJ1.Visible = False
        txt_OBJ2.Visible = False
        txt_OBJ3.Visible = False
        txt_OBJ4.Visible = False
        txt_OBJ5.Visible = False

        txt_OBJ6.Visible = True
        txt_OBJ7.Visible = True
        txt_OBJ8.Visible = True
        txt_OBJ9.Visible = True
        txt_OBJ10.Visible = True

        txt_KPI1.Visible = False
        txt_KPI2.Visible = False
        txt_KPI3.Visible = False
        txt_KPI4.Visible = False
        txt_KPI5.Visible = False

        txt_KPI6.Visible = True
        txt_KPI7.Visible = True
        txt_KPI8.Visible = True
        txt_KPI9.Visible = True
        txt_KPI10.Visible = True



        'ddlStatus1.Visible = False
        'ddlStatus2.Visible = False
        'ddlStatus3.Visible = False
        'ddlStatus4.Visible = False
        'ddlStatus5.Visible = False
        'ddlStatus6.Visible = True
        'ddlStatus7.Visible = True
        'ddlStatus8.Visible = True
        'ddlStatus9.Visible = True
        'ddlStatus10.Visible = True

        'divStatus1.visible = False
        'divStatus2.visible = False
        'divStatus3.visible = False
        'divStatus4.visible = False
        'divStatus5.visible = False
        'divStatus6.visible = True
        'divStatus7.visible = True
        'divStatus8.visible = True
        'divStatus9.visible = True
        'divStatus10.visible = True

        txt_REV1.Visible = False
        txt_REV2.Visible = False
        txt_REV3.Visible = False
        txt_REV4.Visible = False
        txt_REV5.Visible = False
        txt_REV6.Visible = True
        txt_REV7.Visible = True
        txt_REV8.Visible = True
        txt_REV9.Visible = True
        txt_REV10.Visible = True

        txt_FINAL_REV1.Visible = False
        txt_FINAL_REV2.Visible = False
        txt_FINAL_REV3.Visible = False
        txt_FINAL_REV4.Visible = False
        txt_FINAL_REV5.Visible = False
        txt_FINAL_REV6.Visible = True
        txt_FINAL_REV7.Visible = True
        txt_FINAL_REV8.Visible = True
        txt_FINAL_REV9.Visible = True
        txt_FINAL_REV10.Visible = True

        txt_FINAL_MGR1.Visible = False
        txt_FINAL_MGR2.Visible = False
        txt_FINAL_MGR3.Visible = False
        txt_FINAL_MGR4.Visible = False
        txt_FINAL_MGR5.Visible = False
        txt_FINAL_MGR6.Visible = True
        txt_FINAL_MGR7.Visible = True
        txt_FINAL_MGR8.Visible = True
        txt_FINAL_MGR9.Visible = True
        txt_FINAL_MGR10.Visible = True

        divRating1.Style.Add("display", "none")
        divRating2.Style.Add("display", "none")
        divRating3.Style.Add("display", "none")
        divRating4.Style.Add("display", "none")
        divRating5.Style.Add("display", "none")
        divRating7.Style.Add("display", "block")
        divRating8.Style.Add("display", "block")
        divRating9.Style.Add("display", "block")
        divRating10.Style.Add("display", "block")
        divRating11.Style.Add("display", "block")

        spAch1.Visible = False
        spAch2.Visible = False
        spAch3.Visible = False
        spAch4.Visible = False
        spAch5.Visible = False
        spAch6.Visible = True
        spAch7.Visible = True
        spAch8.Visible = True
        spAch9.Visible = True
        spAch10.Visible = True

        divWTG1.Visible = False
        divWTG2.Visible = False
        divWTG3.Visible = False
        divWTG4.Visible = False
        divWTG5.Visible = False

        divWTG6.Visible = True
        divWTG7.Visible = True
        divWTG8.Visible = True
        divWTG9.Visible = True
        divWTG10.Visible = True
        divNote.Visible = False
        ENABLE_STATUS()

        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_V3Connection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Save_Objectives(transaction)

            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                    BIND_EMP_OBJECTIVES()
                End If
            End Try
        End Using
    End Sub
    Private Sub ENABLE_STATUS()
        If (txt_OBJ1.InnerText.Trim() = "" And txt_KPI1.InnerText.Trim() = "") Then

            txt_FINAL_REV1.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR1.Attributes.Add("readonly", "readonly")
            hmeRating1.Enabled = False
            divRating1.Attributes.Add("class", "divRatingDisable")

           
        End If

        If (txt_OBJ2.InnerText.Trim() = "" And txt_KPI2.InnerText.Trim() = "") Then
            txt_FINAL_REV2.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR2.Attributes.Add("readonly", "readonly")
            hmeRating2.Enabled = False
            divRating2.Attributes.Add("class", "divRatingDisable")
        End If
        If (txt_OBJ3.InnerText.Trim() = "" And txt_KPI3.InnerText.Trim() = "") Then
            txt_FINAL_REV3.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR3.Attributes.Add("readonly", "readonly")
            hmeRating3.Enabled = False
            divRating3.Attributes.Add("class", "divRatingDisable")
        End If

        If (txt_OBJ4.InnerText.Trim() = "" And txt_KPI4.InnerText.Trim() = "") Then
            txt_FINAL_REV4.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR4.Attributes.Add("readonly", "readonly")
            hmeRating4.Enabled = False
            divRating4.Attributes.Add("class", "divRatingDisable")
        End If

        If (txt_OBJ5.InnerText.Trim() = "" And txt_KPI5.InnerText.Trim() = "") Then
            txt_FINAL_REV5.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR5.Attributes.Add("readonly", "readonly")
            hmeRating5.Enabled = False
            divRating5.Attributes.Add("class", "divRatingDisable")
        End If

        If (txt_OBJ6.InnerText.Trim() = "" And txt_KPI6.InnerText.Trim() = "") Then
            txt_FINAL_REV6.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR6.Attributes.Add("readonly", "readonly")
            hmeRating7.Enabled = False
            divRating7.Attributes.Add("class", "divRatingDisable")
        End If

        If (txt_OBJ7.InnerText.Trim() = "" And txt_KPI7.InnerText.Trim() = "") Then
            txt_FINAL_REV7.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR7.Attributes.Add("readonly", "readonly")
            hmeRating8.Enabled = False
            divRating8.Attributes.Add("class", "divRatingDisable")
        End If

        If (txt_OBJ8.InnerText.Trim() = "" And txt_KPI8.InnerText.Trim() = "") Then
            txt_FINAL_REV8.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR8.Attributes.Add("readonly", "readonly")
            hmeRating9.Enabled = False
            divRating9.Attributes.Add("class", "divRatingDisable")
        End If

        If (txt_OBJ9.InnerText.Trim() = "" And txt_KPI9.InnerText.Trim() = "") Then
            txt_FINAL_REV9.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR9.Attributes.Add("readonly", "readonly")
            hmeRating10.Enabled = False
            divRating10.Attributes.Add("class", "divRatingDisable")
        End If

        If (txt_OBJ10.InnerText.Trim() = "" And txt_KPI10.InnerText.Trim() = "") Then
            txt_FINAL_REV10.Attributes.Add("readonly", "readonly")
            txt_FINAL_MGR10.Attributes.Add("readonly", "readonly")
            hmeRating11.Enabled = False
            divRating11.Attributes.Add("class", "divRatingDisable")
        End If
    End Sub
    Private Function CHECK_OBJ_COMPLETED() As String
        Dim ERRMSG As String = String.Empty

        If (txt_OBJ1.InnerText.Trim() <> "" And txt_KPI1.InnerText.Trim() <> "" And HF_Rating1.Value = "") Then

            ERRMSG = "In Step 1: Employee self rating required against objective 1"
            Return ERRMSG
            Exit Function
        End If

        If (txt_OBJ2.InnerText.Trim() <> "" And txt_KPI2.InnerText.Trim() <> "" And HF_Rating2.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 2"
            Return ERRMSG
            Exit Function
        End If
        If (txt_OBJ3.InnerText.Trim() <> "" And txt_KPI3.InnerText.Trim() <> "" And HF_Rating3.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 3"
            Return ERRMSG
            Exit Function
        End If
        If (txt_OBJ4.InnerText.Trim() <> "" And txt_KPI4.InnerText.Trim() <> "" And HF_Rating4.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 4"
            Return ERRMSG
            Exit Function
        End If
        If (txt_OBJ5.InnerText.Trim() <> "" And txt_KPI5.InnerText.Trim() <> "" And HF_Rating5.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 5"
            Return ERRMSG
            Exit Function
        End If
        If (txt_OBJ6.InnerText.Trim() <> "" And txt_KPI6.InnerText.Trim() <> "" And HF_Rating7.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 6"
            Return ERRMSG
            Exit Function
        End If
        If (txt_OBJ7.InnerText.Trim() <> "" And txt_KPI7.InnerText.Trim() <> "" And HF_Rating8.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 7"
            Return ERRMSG
            Exit Function
        End If
        If (txt_OBJ8.InnerText.Trim() <> "" And txt_KPI8.InnerText.Trim() <> "" And HF_Rating9.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 8"
            Return ERRMSG
            Exit Function
        End If
        If (txt_OBJ9.InnerText.Trim() <> "" And txt_KPI9.InnerText.Trim() <> "" And HF_Rating10.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 9"
            Return ERRMSG
            Exit Function
        End If
        If (txt_OBJ10.InnerText.Trim() <> "" And txt_KPI10.InnerText.Trim() <> "" And HF_Rating11.Value = "") Then
            ERRMSG = "In Step 1: Employee self rating required against objective 10"
            Return ERRMSG
            Exit Function
        End If

       



        Return ERRMSG

    End Function


    Private Function CHECK_DEVELOPMENT_GOALS_COMPLETED() As String
        Dim ERRMSG As String = String.Empty
        Dim i As Integer = 0
        'If (Convert.ToInt16(ViewState("GOALS_REQ")) <> 0) Then
        If (txt_developGoals1.InnerText.Trim() <> "" And txt_ExpectedOutcome1.InnerText.Trim() <> "") Then
            i = i + 1
        End If
        If (txt_developGoals2.InnerText.Trim() <> "" And txt_ExpectedOutcome2.InnerText.Trim() <> "") Then
            i = i + 1
        End If
        If (txt_developGoals3.InnerText.Trim() <> "" And txt_ExpectedOutcome3.InnerText.Trim() <> "") Then
            i = i + 1
        End If
        If (txt_developGoals4.InnerText.Trim() <> "" And txt_ExpectedOutcome4.InnerText.Trim() <> "") Then
            i = i + 1
        End If

        If i = 0 Then
            ERRMSG = "At least one  development goals required"
        End If
        Return ERRMSG
    End Function
    Private Function CHECK_CAREER_ASP_COMPLETED() As String

        Dim errmsg As String = String.Empty
        Dim i As Integer = 0
        If txt_CarrierInput.InnerText.Trim() <> "" Then
            i = i + 1
        End If

        If i = 0 Then

            errmsg = " Career Aspirations not entered"
        End If
        Return errmsg
    End Function
    Private Sub check_list()
        Dim allCheckedItems As New StringBuilder()
        Dim i As Integer = 0
        While i < ddl_mobility.Items.Count

            If ddl_mobility.Items(0).Selected Then
                ddl_mobility.Items(i).Selected = False
                ddl_mobility.Items(0).Selected = True
            End If
            i = i + 1
        End While
    End Sub

    Protected Sub spAch1_Click(sender As Object, e As EventArgs) Handles spAch1.Click
        divlogNote.Visible = False


        Dim txt As String
        txt = "'txt_OBJ1', 'txt_KPI1','spAch1', 'txt_REV1'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)

    End Sub
    Protected Sub spAch2_Click(sender As Object, e As EventArgs) Handles spAch2.Click
        divlogNote.Visible = False
       
        Dim txt As String
        txt = "'txt_OBJ2', 'txt_KPI2','spAch2', 'txt_REV2'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub spAch3_Click(sender As Object, e As EventArgs) Handles spAch3.Click
        divlogNote.Visible = False
       
        Dim txt As String
        txt = "'txt_OBJ3', 'txt_KPI3','spAch3', 'txt_REV3'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub spAch4_Click(sender As Object, e As EventArgs) Handles spAch4.Click
        divlogNote.Visible = False

        Dim txt As String
        txt = "'txt_OBJ4', 'txt_KPI4','spAch4', 'txt_REV4'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub spAch5_Click(sender As Object, e As EventArgs) Handles spAch5.Click
        divlogNote.Visible = False
        

        Dim txt As String
        txt = "'txt_OBJ5', 'txt_KPI5','spAch5', 'txt_REV5'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub spAch6_Click(sender As Object, e As EventArgs) Handles spAch6.Click
        divlogNote.Visible = False
        

        Dim txt As String
        txt = "'txt_OBJ6', 'txt_KPI6','spAch6', 'txt_REV6'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub spAch7_Click(sender As Object, e As EventArgs) Handles spAch7.Click
        divlogNote.Visible = False
       

        Dim txt As String
        txt = "'txt_OBJ7', 'txt_KPI7','spAch7', 'txt_REV7'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub spAch8_Click(sender As Object, e As EventArgs) Handles spAch8.Click
        divlogNote.Visible = False



        Dim txt As String
        txt = "'txt_OBJ8', 'txt_KPI8','spAch8', 'txt_REV8'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub spAch9_Click(sender As Object, e As EventArgs) Handles spAch9.Click
        divlogNote.Visible = False
      

        Dim txt As String
        txt = "'txt_OBJ9', 'txt_KPI9','spAch9', 'txt_REV9'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub spAch10_Click(sender As Object, e As EventArgs) Handles spAch10.Click
        divlogNote.Visible = False



        Dim txt As String
        txt = "'txt_OBJ10', 'txt_KPI10','spAch10', 'txt_REV10'"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SmartObjective", "smartObjective(" & txt & ");", True)
    End Sub
    Protected Sub lbtnRevert_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
           
            Panel_RevertBack.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_RIEVEWERS_TOREVERT", pParms)
                rbtnList_Rievewer.DataSource = ds
                rbtnList_Rievewer.DataTextField = "EMP_NAME"
                rbtnList_Rievewer.DataValueField = "EMP_ID"
                rbtnList_Rievewer.DataBind()
                rbtnList_Rievewer.Items(0).Selected = True
                'rptPDP_Reveiwer.DataSource = ds
                'rptPDP_Reveiwer.DataBind()
            End Using
        Catch ex As Exception
            Panel_RevertBack.Visible = False
           
        End Try


    End Sub
    Protected Sub btn_Revert_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Revert.Click
        '  Panel_RevertBack.Visible = False
        Dim lnkRev As LinkButton = sender.parent.FindControl("lbtnRevNew")
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim redirectUrl As String

        redirectUrl = "pdpFinal.aspx?ER=" + Encr_decrData.Encrypt(ViewState("EPR_ID")) + "&U=" + Encr_decrData.Encrypt(ViewState("EMP_ID")) + "&MR=" + Encr_decrData.Encrypt(rbtnList_Rievewer.SelectedValue)
        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If
        url = LCase(url)
        url = url.Replace("pdpdashboard.aspx", redirectUrl)
        url = url.Replace("http:", "https:")
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", rbtnList_Rievewer.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
            pParms(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks.InnerText.Trim())
            pParms(4) = New SqlClient.SqlParameter("@URL", url)
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.REVERT_BACKTO_REVIEWER", pParms)
            sqltran.Commit()
            aFTER_SAVE()
            'lblerrormsg.Text = "You have successfully saved the Performance Development Plan"
            Panel_RevertBack.Visible = False
        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub
    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Cancel.Click
        Panel_RevertBack.Visible = False
        rbtnList_Rievewer.ClearSelection()
        txt_Remarks.InnerText = ""
       
    End Sub
    Sub Final_emp_cmmt()

        If ViewState("FINAL_CMMT") = "0" And ViewState("IS_EMP") = True And ViewState("INT_FINAL_FINISH") = "1" And ViewState("EPR_bPUBLISHED") = "True" Then
            txt_EmpOverallSummary.Attributes.Remove("readonly")

            btn_Submit.Visible = True
        End If
    End Sub
    Sub check_published()
        If ViewState("IS_EMP") = "True" And ViewState("EPR_bPUBLISHED") = "False" Then
            txt_mgrOverallSummary.InnerText = ""
            SpOverallRatingNo.InnerText = ""
        End If
    End Sub
    Protected Sub btn_Submit_Click(sender As Object, e As EventArgs) Handles btn_Submit.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""

        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try

            Save_OverallSummary(sqltran)
            sqltran.Commit()
            txt_EmpOverallSummary.Attributes.Add("readonly", "readonly")
            btn_Submit.Visible = False
            ViewState("FINAL_CMMT") = "1"
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
            lblError.Text = "<div>You have successfully saved the Performance Development Plan </div>"

        Catch ex As Exception
            sqltran.Rollback()

            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Error occured while saving </div>"
        End Try
    End Sub
End Class
