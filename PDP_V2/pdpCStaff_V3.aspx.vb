﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Telerik.Web.UI

Partial Class pdpCStaff_V3
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim TAB_MANDATORY As New Hashtable
    Dim TAB_TITLE As New Hashtable

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            lbtnStep1.Attributes.Add("class", "cssStepBtnActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep2.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep3.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep4.Attributes.Add("class", "cssStepBtnInActive col-md-2 col-lg-2 col-sm-2")
            lbtnStep5.Attributes.Add("class", "cssStepBtnInActiveDisable col-md-2 col-lg-2 col-sm-2")
            tb_ObjectivesCurrent.Visible = True     'TAB-1
            tb_CareerAspirations.Visible = False    'TAB-2
            tb_developmentGoals.Visible = False              'TAB-3
            tb_competencies.Visible = False

            ViewState("EMP_ID") = ""
            ViewState("EPR_ID") = ""
            ViewState("RVW_EMP_ID") = ""

            Bind_KRA()
            Bind_Competencies()
            Bind_Mobility()
            divWtg.Visible = False




            If Not (Request.QueryString("U") Is Nothing) Then
                ViewState("EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("U").Replace(" ", "+"))
            Else
                ViewState("EMP_ID") = "0"
            End If
            If Not (Request.QueryString("ER") Is Nothing) Then
                ViewState("EPR_ID") = Encr_decrData.Decrypt(Request.QueryString("ER").Replace(" ", "+"))
            Else
                ViewState("EPR_ID") = "0"
            End If
            If Not (Request.QueryString("MR") Is Nothing) Then
                ViewState("RVW_EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("MR").Replace(" ", "+"))
            Else
                ViewState("RVW_EMP_ID") = "0"
            End If

            'Staff
            'ViewState("EMP_ID") = "10161"
            'ViewState("EPR_ID") = "1"
            'ViewState("RVW_EMP_ID") = "10161"
            'Line Mgr
            'Session("sUsr_name") = "prem.sunder"
            'ViewState("EMP_ID") = "24322"
            'ViewState("EPR_ID") = "808"
            'ViewState("RVW_EMP_ID") = "9235"
            'Session("EmployeeId") = "9235"
            'Super User
            'ViewState("EMP_ID") = "10161"
            'ViewState("EPR_ID") = "1"
            'ViewState("RVW_EMP_ID") = "19604"


            If ((ViewState("RVW_EMP_ID") = "0") And (ViewState("EPR_ID") = "0") And (ViewState("RVW_EMP_ID") = "0")) Then

                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
                lbtnRevert.Visible = False
            Else
                If (ValidateUser() = True) Then

                    BIND_EMP_OBJECTIVES()
                    BIND_CAREER_DEVEL()
                    BIND_EMP_COMPETENCIES()
                    Bind_GetPDP_CompletedDate()
                    btn_SaveFinish.Visible = False
                    lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
                    lbtnStep3.Attributes.Add("class", IIf(CHECK_DEVELOPMENT_GOALS_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
                    lbtnStep4.Attributes.Add("class", IIf(CHECK_DEVELOPMENT_GOALS_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
                Else
                    btn_Next.Visible = False
                    btn_Previous.Visible = False
                    btn_SaveDraft.Visible = False
                    btn_SaveFinish.Visible = False
                    lbtnRevert.Visible = False
                End If

            End If
            'Get_Emp_BSU_TAG()

            ' If ViewState("EMP_BSU_TAG") = "G" Then
            'spMan.InnerText = "Min   5 & Max 10 objectives required"
            'Else
            spMan.InnerText = "Min   3 & Max 10 objectives required"
            ' End If
            Set_ButtonViewRights()

        End If
    End Sub
    Private Sub Set_ButtonViewRights()

        If ViewState("IS_EMP") = True Then  'In the case of  employee
            btn_SaveFinish.Text = "Save & Finish"
            cbeConfirmPDP.ConfirmText = "Are you sure you want to submit the GEMS Performance Development Plan form to the next reviewer?"
            lbtnRevert.Visible = False

            If ViewState("INT_FINAL_FINISH") = 0 Then
                ViewState("INFO_TYPE") = 1 ' when staff not yet updated pdp
                status_readonly()
            ElseIf (ViewState("INT_FINAL_FINISH") = 1) Then

                MAKE_READONLY()
                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False
            End If
        ElseIf ViewState("IS_MGR") = True Then  'In the case of  Line Mgr
            btn_SaveFinish.Text = "Approve"

            btn_SaveDraft.Visible = True
            'lbtnRevert.Visible = True
            btn_Next.Text = "Next"
            If ViewState("USR_LVL_FINISH") = 1 Then
                lbtnRevert.Visible = True

            Else
                lbtnRevert.Visible = False
                btn_Next.Visible = False
                btn_Previous.Visible = False
            End If
            lbtnRevert.Visible = False
            cbeConfirmPDP.ConfirmText = "Are you sure you want to submit the GEMS Performance Development Plan form?"
            MAKE_READONLY()

            If ViewState("INT_FINAL_FINISH") = 0 Then
                ViewState("INFO_TYPE") = 1
                status_readonly()
            Else

                MAKE_READONLY()


                ViewState("INFO_TYPE") = 0


                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False

            End If

        ElseIf ViewState("IS_VIEW_USER") = True Then  'In the case of  super user Mgr Mgr Line
            ViewState("INFO_TYPE") = 0
            btn_Next.Visible = False
            btn_Previous.Visible = False
            btn_SaveDraft.Visible = False
            btn_SaveFinish.Visible = False
            lbtnRevert.Visible = False
            MAKE_READONLY()

        Else 'for all the other users
            ViewState("INFO_TYPE") = 0
            btn_Next.Visible = False
            btn_Previous.Visible = False
            btn_SaveDraft.Visible = False
            btn_SaveFinish.Visible = False
            lbtnRevert.Visible = False
            MAKE_READONLY()
        End If

    End Sub
    Private Sub MAKE_READONLY()
        ddl_mobility.Enabled = False
        'ddlKRA_1.Attributes.Add("readonly", "readonly")
        'ddlKRA_2.Attributes.Add("readonly", "readonly")
        'ddlKRA_3.Attributes.Add("readonly", "readonly")
        'ddlKRA_4.Attributes.Add("readonly", "readonly")
        'ddlKRA_5.Attributes.Add("readonly", "readonly")
        'ddlKRA_6.Attributes.Add("readonly", "readonly")
        'ddlKRA_7.Attributes.Add("readonly", "readonly")
        'ddlKRA_8.Attributes.Add("readonly", "readonly")
        'ddlKRA_9.Attributes.Add("readonly", "readonly")
        'ddlKRA_10.Attributes.Add("readonly", "readonly")
        ddlKRA_1.Enabled = False
        ddlKRA_2.Enabled = False
        ddlKRA_3.Enabled = False
        ddlKRA_4.Enabled = False
        ddlKRA_5.Enabled = False
        ddlKRA_6.Enabled = False
        ddlKRA_7.Enabled = False
        ddlKRA_8.Enabled = False
        ddlKRA_9.Enabled = False
        ddlKRA_10.Enabled = False
        txt_OBJ1.Attributes.Add("readonly", "readonly")
        txt_OBJ2.Attributes.Add("readonly", "readonly")
        txt_OBJ3.Attributes.Add("readonly", "readonly")
        txt_OBJ4.Attributes.Add("readonly", "readonly")
        txt_OBJ5.Attributes.Add("readonly", "readonly")
        txt_OBJ6.Attributes.Add("readonly", "readonly")
        txt_OBJ7.Attributes.Add("readonly", "readonly")
        txt_OBJ8.Attributes.Add("readonly", "readonly")
        txt_OBJ9.Attributes.Add("readonly", "readonly")
        txt_OBJ10.Attributes.Add("readonly", "readonly")

        txt_KPI1.Attributes.Add("readonly", "readonly")
        txt_KPI2.Attributes.Add("readonly", "readonly")
        txt_KPI3.Attributes.Add("readonly", "readonly")
        txt_KPI4.Attributes.Add("readonly", "readonly")
        txt_KPI5.Attributes.Add("readonly", "readonly")
        txt_KPI6.Attributes.Add("readonly", "readonly")
        txt_KPI7.Attributes.Add("readonly", "readonly")
        txt_KPI8.Attributes.Add("readonly", "readonly")
        txt_KPI9.Attributes.Add("readonly", "readonly")
        txt_KPI10.Attributes.Add("readonly", "readonly")

        txtWTG1.Attributes.Add("readonly", "readonly")
        txtWTG2.Attributes.Add("readonly", "readonly")
        txtWTG3.Attributes.Add("readonly", "readonly")
        txtWTG4.Attributes.Add("readonly", "readonly")
        txtWTG5.Attributes.Add("readonly", "readonly")
        txtWTG6.Attributes.Add("readonly", "readonly")
        txtWTG7.Attributes.Add("readonly", "readonly")
        txtWTG8.Attributes.Add("readonly", "readonly")
        txtWTG9.Attributes.Add("readonly", "readonly")
        txtWTG10.Attributes.Add("readonly", "readonly")

        txt_CarrierInput.Attributes.Add("readonly", "readonly")
        txt_ProfDevelNeeds.Attributes.Add("readonly", "readonly")

        txt_developGoals1.Attributes.Add("readonly", "readonly")
        txt_developGoals2.Attributes.Add("readonly", "readonly")
        txt_developGoals3.Attributes.Add("readonly", "readonly")
        txt_developGoals4.Attributes.Add("readonly", "readonly")

        txt_ExpectedOutcome1.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome2.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome3.Attributes.Add("readonly", "readonly")
        txt_ExpectedOutcome4.Attributes.Add("readonly", "readonly")

        ddlCOM_1.Enabled = False
        ddlCOM_2.Enabled = False
        ddlCOM_3.Enabled = False
        ddlCOM_4.Enabled = False
        ddlCOM_5.Enabled = False
        ddlCOM_6.Enabled = False
        ddlCOM_7.Enabled = False
        ddlCOM_8.Enabled = False
        ddlCOM_9.Enabled = False
        ddlCOM_10.Enabled = False

        txt_COM1.Attributes.Add("readonly", "readonly")
        txt_COM2.Attributes.Add("readonly", "readonly")
        txt_COM3.Attributes.Add("readonly", "readonly")
        txt_COM4.Attributes.Add("readonly", "readonly")
        txt_COM5.Attributes.Add("readonly", "readonly")
        txt_COM6.Attributes.Add("readonly", "readonly")
        txt_COM7.Attributes.Add("readonly", "readonly")
        txt_COM8.Attributes.Add("readonly", "readonly")
        txt_COM9.Attributes.Add("readonly", "readonly")
        txt_COM10.Attributes.Add("readonly", "readonly")

        txt_COM_MGR1.Attributes.Add("readonly", "readonly")
        txt_COM_MGR2.Attributes.Add("readonly", "readonly")
        txt_COM_MGR3.Attributes.Add("readonly", "readonly")
        txt_COM_MGR4.Attributes.Add("readonly", "readonly")
        txt_COM_MGR5.Attributes.Add("readonly", "readonly")
        txt_COM_MGR6.Attributes.Add("readonly", "readonly")
        txt_COM_MGR7.Attributes.Add("readonly", "readonly")
        txt_COM_MGR8.Attributes.Add("readonly", "readonly")
        txt_COM_MGR9.Attributes.Add("readonly", "readonly")
        txt_COM_MGR10.Attributes.Add("readonly", "readonly")

    End Sub
    Private Sub status_readonly()
        If ViewState("IS_MGR") = True Then
            If ddlCOM_1.SelectedItem.Value <> 0 Then
                txt_COM_MGR1.Attributes.Remove("readonly")
            Else
                txt_COM_MGR1.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_2.SelectedItem.Value <> 0 Then
                txt_COM_MGR2.Attributes.Remove("readonly")
            Else
                txt_COM_MGR2.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_3.SelectedItem.Value <> 0 Then
                txt_COM_MGR3.Attributes.Remove("readonly")
            Else
                txt_COM_MGR3.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_4.SelectedItem.Value <> 0 Then
                txt_COM_MGR4.Attributes.Remove("readonly")
            Else
                txt_COM_MGR4.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_5.SelectedItem.Value <> 0 Then
                txt_COM_MGR5.Attributes.Remove("readonly")
            Else
                txt_COM_MGR5.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_6.SelectedItem.Value <> 0 Then
                txt_COM_MGR6.Attributes.Remove("readonly")
            Else
                txt_COM_MGR6.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_7.SelectedItem.Value <> 0 Then
                txt_COM_MGR7.Attributes.Remove("readonly")
            Else
                txt_COM_MGR7.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_8.SelectedItem.Value <> 0 Then
                txt_COM_MGR8.Attributes.Remove("readonly")
            Else
                txt_COM_MGR8.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_9.SelectedItem.Value <> 0 Then
                txt_COM_MGR9.Attributes.Remove("readonly")
            Else
                txt_COM_MGR9.Attributes.Add("readonly", "readonly")
            End If

            If ddlCOM_10.SelectedItem.Value <> 0 Then
                txt_COM_MGR10.Attributes.Remove("readonly")
            Else
                txt_COM_MGR10.Attributes.Add("readonly", "readonly")
            End If
            'If ddlCOM_3 Then
            '    txt_COM_MGR3.Attributes.Remove("readonly")
            '    txt_COM_MGR4.Attributes.Remove("readonly")
            '    txt_COM_MGR5.Attributes.Remove("readonly")
            '    txt_COM_MGR6.Attributes.Remove("readonly")
            '    txt_COM_MGR7.Attributes.Remove("readonly")
            '    txt_COM_MGR8.Attributes.Remove("readonly")
            '    txt_COM_MGR9.Attributes.Remove("readonly")
            '    txt_COM_MGR10.Attributes.Remove("readonly")
        Else
            txt_COM_MGR1.Attributes.Add("readonly", "readonly")
            txt_COM_MGR2.Attributes.Add("readonly", "readonly")
            txt_COM_MGR3.Attributes.Add("readonly", "readonly")
            txt_COM_MGR4.Attributes.Add("readonly", "readonly")
            txt_COM_MGR5.Attributes.Add("readonly", "readonly")
            txt_COM_MGR6.Attributes.Add("readonly", "readonly")
            txt_COM_MGR7.Attributes.Add("readonly", "readonly")
            txt_COM_MGR8.Attributes.Add("readonly", "readonly")
            txt_COM_MGR9.Attributes.Add("readonly", "readonly")
            txt_COM_MGR10.Attributes.Add("readonly", "readonly")
        End If
    End Sub
    Private Sub Bind_Mobility()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_MOBILITY_M", pParms)

        ddl_mobility.DataSource = ds
        ddl_mobility.DataValueField = "MOB_ID"
        ddl_mobility.DataTextField = "MOB_DESCR"
        ddl_mobility.DataBind()

    End Sub

    Private Sub Get_Emp_BSU_TAG()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_EMP_BSU_TAG", pParms)
        ViewState("EMP_BSU_TAG") = ds.Tables(0).Rows(0).Item("BS_TAG")

    End Sub
    Private Sub Bind_KRA()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_V3ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@INFO_TYPE", "KRA")
        pParms(1) = New SqlClient.SqlParameter("@PDPY_ID", Session("PDP_YEAR"))

        ddlKRA_1.Items.Clear()
        ddlKRA_2.Items.Clear()
        ddlKRA_3.Items.Clear()
        ddlKRA_4.Items.Clear()
        ddlKRA_5.Items.Clear()
        ddlKRA_6.Items.Clear()
        ddlKRA_7.Items.Clear()
        ddlKRA_8.Items.Clear()
        ddlKRA_9.Items.Clear()
        ddlKRA_10.Items.Clear()
        ddlKRA_1.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_2.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_3.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_4.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_5.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_6.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_7.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_8.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_9.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        ddlKRA_10.Items.Add(New ListItem("Select Your  Key Result Area ", "0"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PDP_N.BIND_MASTER_DETAILS", pParms)
            While datareader.Read
                ddlKRA_1.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_2.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_3.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_4.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_5.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_6.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_7.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_8.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_9.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
                ddlKRA_10.Items.Add(New ListItem(Convert.ToString(datareader("KRA_HDR_DESCR")), Convert.ToString(datareader("KRA_ID"))))
            End While
        End Using

    End Sub
    Private Sub Bind_Competencies()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_V3ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@INFO_TYPE", "COMP")
        pParms(1) = New SqlClient.SqlParameter("@PDPY_ID", Session("PDP_YEAR"))

        ddlCOM_1.Items.Clear()
        ddlCOM_2.Items.Clear()
        ddlCOM_3.Items.Clear()
        ddlCOM_4.Items.Clear()
        ddlCOM_5.Items.Clear()
        ddlCOM_6.Items.Clear()
        ddlCOM_7.Items.Clear()
        ddlCOM_8.Items.Clear()
        ddlCOM_9.Items.Clear()
        ddlCOM_10.Items.Clear()
        ddlCOM_1.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_2.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_3.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_4.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_5.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_6.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_7.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_8.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_9.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        ddlCOM_10.Items.Add(New ListItem("Select Your  Competencies ", "0"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PDP_N.BIND_MASTER_DETAILS", pParms)
            While datareader.Read

                ddlCOM_1.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_2.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_3.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_4.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_5.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_6.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_7.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_8.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_9.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))
                ddlCOM_10.Items.Add(New ListItem(Convert.ToString(datareader("COM_DESCR")), Convert.ToString(datareader("COM_ID"))))

               
            End While
        End Using

    End Sub
    Private Sub BIND_EMP_OBJECTIVES()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_V3ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PDP_N.BIND_EMP_OBJECTIVES", pParms)
            While datareader.Read
                '-----------------------------1---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "1" Then
                    If Not ddlKRA_1.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_1.ClearSelection()
                        ddlKRA_1.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    ' ddlKRA_1.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ1.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI1.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG1.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------2---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "2" Then
                    If Not ddlKRA_2.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_2.ClearSelection()
                        ddlKRA_2.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    ' ddlKRA_2.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ2.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI2.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG2.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------3---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "3" Then
                    If Not ddlKRA_3.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_3.ClearSelection()
                        ddlKRA_3.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    ' ddlKRA_3.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ3.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI3.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG3.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If


                '-----------------------------4---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "4" Then
                    If Not ddlKRA_4.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_4.ClearSelection()
                        ddlKRA_4.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    '  ddlKRA_4.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ4.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI4.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG4.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------5---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "5" Then
                    If Not ddlKRA_5.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_5.ClearSelection()
                        ddlKRA_5.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    ' ddlKRA_5.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ5.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI5.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG5.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------6---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "6" Then
                    If Not ddlKRA_6.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_6.ClearSelection()
                        ddlKRA_6.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    ' ddlKRA_6.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ6.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI6.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG6.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If


                '-----------------------------7---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "7" Then
                    If Not ddlKRA_7.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_7.ClearSelection()
                        ddlKRA_7.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    ' ddlKRA_7.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ7.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI7.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG7.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If
                '-----------------------------8---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "8" Then
                    If Not ddlKRA_8.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_8.ClearSelection()
                        ddlKRA_8.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    'ddlKRA_8.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ8.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI8.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG8.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------9---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "9" Then
                    If Not ddlKRA_9.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_9.ClearSelection()
                        ddlKRA_9.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    ' ddlKRA_9.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ9.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI9.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG9.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If

                '-----------------------------10---------------------------
                If Convert.ToString(datareader("EOBJ_ORDER_ID")) = "10" Then
                    If Not ddlKRA_10.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))) Is Nothing Then
                        ddlKRA_10.ClearSelection()
                        ddlKRA_10.Items.FindByValue(Convert.ToString(datareader("EOBJ_KRA_ID"))).Selected = True
                    End If
                    ' ddlKRA_10.InnerText = Convert.ToString(datareader("EOBJ_KRA_ID"))
                    txt_OBJ10.InnerText = Convert.ToString(datareader("EOBJ_TEXT"))
                    txt_KPI10.InnerText = Convert.ToString(datareader("EOBJ_KPI_TEXT"))
                    If Convert.ToString(datareader("EOBJ_WEIGTAGE")) <> "" Then
                        txtWTG10.Value = Convert.ToString(datareader("EOBJ_WEIGTAGE"))
                    End If
                End If


            End While
        End Using

    End Sub
    Private Sub BIND_EMP_COMPETENCIES()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_V3ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "PDP_N.BIND_EMP_COMPETENCIES", pParms)
            While datareader.Read
                '-----------------------------1---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "1" Then
                    If Not ddlCOM_1.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_1.ClearSelection()
                        ddlCOM_1.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM1.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR1.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------2---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "2" Then
                    If Not ddlCOM_2.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_2.ClearSelection()
                        ddlCOM_2.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM2.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR2.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------3---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "3" Then
                    If Not ddlCOM_3.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_3.ClearSelection()
                        ddlCOM_3.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM3.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR3.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If


                '-----------------------------4---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "4" Then
                    If Not ddlCOM_4.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_4.ClearSelection()
                        ddlCOM_4.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM4.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR4.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------5---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "5" Then
                    If Not ddlCOM_5.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_5.ClearSelection()
                        ddlCOM_5.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM5.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR5.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------6---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "6" Then
                    If Not ddlCOM_6.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_6.ClearSelection()
                        ddlCOM_6.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM6.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR6.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If


                '-----------------------------7---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "7" Then
                    If Not ddlCOM_7.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_7.ClearSelection()
                        ddlCOM_7.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM7.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR7.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If
                '-----------------------------8---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "8" Then
                    If Not ddlCOM_8.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_8.ClearSelection()
                        ddlCOM_8.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM8.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR8.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------9---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "9" Then
                    If Not ddlCOM_9.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_9.ClearSelection()
                        ddlCOM_9.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM9.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR9.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If

                '-----------------------------10---------------------------
                If Convert.ToString(datareader("COMP_ORDER_ID")) = "10" Then
                    If Not ddlCOM_10.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))) Is Nothing Then
                        ddlCOM_10.ClearSelection()
                        ddlCOM_10.Items.FindByValue(Convert.ToString(datareader("COMP_COM_ID"))).Selected = True
                    End If

                    txt_COM10.InnerText = Convert.ToString(datareader("COMP_EMP_CMNT"))
                    txt_COM_MGR10.InnerText = Convert.ToString(datareader("COMP_MGR_CMNT"))

                End If


            End While
        End Using

    End Sub
    Private Sub BIND_CAREER_DEVEL()
        Dim conn As String = ConnectionManger.GetOASIS_PDP_V3ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[PDP_N].[BIND_EMP_DEVELOPMENT]", pParms)
            While datareader.Read
                '-----------------------------1---------------------------
                If Convert.ToString(datareader("EDEV_ORDER")) = "1" Then
                    txt_developGoals1.InnerText = Convert.ToString(datareader("EDEV_GOAL"))
                    txt_ExpectedOutcome1.InnerText = Convert.ToString(datareader("EDEV_MEASURE"))
                End If

                '-----------------------------2---------------------------
                If Convert.ToString(datareader("EDEV_ORDER")) = "2" Then
                    txt_developGoals2.InnerText = Convert.ToString(datareader("EDEV_GOAL"))
                    txt_ExpectedOutcome2.InnerText = Convert.ToString(datareader("EDEV_MEASURE"))
                End If
                '-----------------------------3---------------------------
                If Convert.ToString(datareader("EDEV_ORDER")) = "3" Then
                    txt_developGoals3.InnerText = Convert.ToString(datareader("EDEV_GOAL"))
                    txt_ExpectedOutcome3.InnerText = Convert.ToString(datareader("EDEV_MEASURE"))
                End If
                '-----------------------------4---------------------------
                If Convert.ToString(datareader("EDEV_ORDER")) = "4" Then
                    txt_developGoals4.InnerText = Convert.ToString(datareader("EDEV_GOAL"))
                    txt_ExpectedOutcome4.InnerText = Convert.ToString(datareader("EDEV_MEASURE"))
                End If



            End While
        End Using


    End Sub
    Private Sub Bind_GetPDP_CompletedDate()
        Try


            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

                Using reader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "PDP_N.GET_PDP_COMPLETED_DATE", pParms)
                    While reader.Read()
                        If (Convert.ToString(reader("EPL_LVL_ORDER")) = "0") Then
                            txtEmpSigned.Value = Convert.ToString(reader("EMP_NAME"))
                            txtEmpDate.Value = Convert.ToString(reader("EPL_LVL_FINISH_DATE"))
                        ElseIf (Convert.ToString(reader("EPL_LVL_ORDER")) = "1") Then
                            txtMgrSigned1.Value = Convert.ToString(reader("EMP_NAME"))
                            txtMgrDate1.Value = Convert.ToString(reader("EPL_LVL_FINISH_DATE"))
                        End If
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Function ValidateUser() As Boolean
        Try

            Dim MOBILITY_ID As String = String.Empty

            Dim bSHOW_DETAILS As Boolean
            Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
            pParms(2) = New SqlClient.SqlParameter("@REVIEWING_EMP_ID", ViewState("RVW_EMP_ID"))
            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "[PDP_N].[GET_EMP_PERF_M_DETAIL]", pParms)

                While DATAREADER.Read
                    bSHOW_DETAILS = True


                    txt_EmpName.Value = Convert.ToString(DATAREADER("EMP_NAME"))
                    txt_role.Value = Convert.ToString(DATAREADER("DES_DESCR"))

                    spStdte.InnerText = Convert.ToString(DATAREADER("PDPY_DESC"))
                    spstdte1.InnerText = Convert.ToString(DATAREADER("PDPY_DESC"))

                    txt_ManagerName.Value = Convert.ToString(DATAREADER("EMP_MANAGER_NAME"))
                    txt_bsu.Value = Convert.ToString(DATAREADER("BSU_NAME"))

                    txt_FromDate.Value = Convert.ToString(DATAREADER("PRD_START"))
                    txt_ToDate.Value = Convert.ToString(DATAREADER("PRD_END"))

                    empName.InnerHtml = Convert.ToString(DATAREADER("EMP_NAME"))
                    empDesig.InnerText = Convert.ToString(DATAREADER("DES_DESCR"))

                    txt_CarrierInput.InnerText = Convert.ToString(DATAREADER("EPR_CAREER_ASP"))
                    txt_ProfDevelNeeds.InnerText = Convert.ToString(DATAREADER("EPR_PROF_DEVEL"))

                    ViewState("IS_EMP") = Convert.ToString(DATAREADER("IS_EMP"))
                    ViewState("IS_MGR") = Convert.ToString(DATAREADER("IS_MGR"))
                    ViewState("IS_VIEW_USER") = Convert.ToString(DATAREADER("IS_VIEW_USER"))
                    ViewState("INT_FINAL_FINISH") = Convert.ToString(DATAREADER("INT_FINAL_FINISH"))
                    ViewState("USR_LVL_FINISH") = Convert.ToString(DATAREADER("USR_LVL_FINISH"))
                    MOBILITY_ID = Convert.ToString(DATAREADER("EPR_EPD_MOBILITY_ID"))

                    If (MOBILITY_ID <> "") Then
                        For Each LOC As String In MOBILITY_ID.Split("|")
                            If LOC.Trim() <> "" Then
                                For Each itm As ListItem In ddl_mobility.Items
                                    If (itm.Value = LOC) Then
                                        itm.Selected = True
                                    End If
                                Next
                            End If
                        Next
                    End If

                End While

            End Using



            Return bSHOW_DETAILS
        Catch ex As Exception
            Return False
        End Try
    End Function
    Protected Sub lbtnStep1_Click(sender As Object, e As EventArgs) Handles lbtnStep1.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        Dim StatusObjective As String = String.Empty
        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", IIf(CHECK_DEVELOPMENT_GOALS_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep4.Attributes.Add("class", IIf(CHECK_DEVELOPMENT_GOALS_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep1.Attributes.Add("class", "cssStepBtnActive")
        tb_ObjectivesCurrent.Visible = True    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-2
        tb_developmentGoals.Visible = False              'TAB-3
        tb_competencies.Visible = False
        'TAB-6
        btn_Previous.Visible = False
        btn_Next.Visible = True
        btn_SaveFinish.Visible = False
        Set_ButtonViewRights()
        lbtnRevert.Visible = False
    End Sub
    Protected Sub lbtnStep2_Click(sender As Object, e As EventArgs) Handles lbtnStep2.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = True    'TAB-2
        tb_developmentGoals.Visible = False     'TAB-3
        tb_competencies.Visible = False
        Dim StatusObjective As String = String.Empty
        lbtnStep1.Attributes.Add("class", IIf(CHECK_OBJ_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", IIf(CHECK_DEVELOPMENT_GOALS_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep4.Attributes.Add("class", IIf(CHECK_DEVELOPMENT_GOALS_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep2.Attributes.Add("class", "cssStepBtnActive")
        btn_Next.Visible = True
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = False

        Set_ButtonViewRights()
        lbtnRevert.Visible = False
    End Sub
    Protected Sub lbtnStep3_Click(sender As Object, e As EventArgs) Handles lbtnStep3.Click

        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-2
        tb_developmentGoals.Visible = True     'TAB-3
        tb_competencies.Visible = False
        Dim StatusObjective As String = String.Empty
        lbtnStep1.Attributes.Add("class", IIf(CHECK_OBJ_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep4.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", "cssStepBtnActive")
        btn_Next.Visible = False
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = True

        Set_ButtonViewRights()
        If ((ViewState("USR_LVL_FINISH") = 1) And (ViewState("IS_MGR") = True)) Then
            lbtnRevert.Visible = True

        Else
            lbtnRevert.Visible = False
        End If
    End Sub
    Protected Sub lbtnStep4_Click(sender As Object, e As EventArgs) Handles lbtnStep4.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
        tb_ObjectivesCurrent.Visible = False    'TAB-1
        tb_CareerAspirations.Visible = False    'TAB-2
        tb_developmentGoals.Visible = False     'TAB-3
        tb_competencies.Visible = True
        Dim StatusObjective As String = String.Empty
        lbtnStep1.Attributes.Add("class", IIf(CHECK_OBJ_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep2.Attributes.Add("class", IIf(CHECK_CAREER_ASP_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep3.Attributes.Add("class", IIf(CHECK_DEVELOPMENT_GOALS_COMPLETED() = "", "cssStepBtnComp", "cssStepBtnInActive"))
        lbtnStep4.Attributes.Add("class", "cssStepBtnActive")
       btn_Next.Visible = True
        btn_Previous.Visible = True
        btn_SaveFinish.Visible = False

        Set_ButtonViewRights()
        lbtnRevert.Visible = False
    End Sub

    Protected Sub lbtnStep5_Click(sender As Object, e As EventArgs) Handles lbtnStep5.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""
    End Sub
    Protected Sub btn_Previous_Click(sender As Object, e As EventArgs) Handles btn_Previous.Click
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_V3Connection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If tb_CareerAspirations.Visible = True Then 'TAB-2
                    lbtnStep4_Click(lbtnStep4, Nothing)
                    Save_CareerAspirations(transaction)
                ElseIf tb_developmentGoals.Visible = True Then 'TAB-4
                    lbtnStep2_Click(lbtnStep2, Nothing)
                    SAVE_CAREER_DEVELOPMENT(transaction)
                ElseIf tb_competencies.Visible = True Then 'TAB-4
                    lbtnStep1_Click(lbtnStep1, Nothing)
                    Save_COMPETENCIES(transaction)
                End If
            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then
                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using


    End Sub
    Protected Sub btn_Next_Click(sender As Object, e As EventArgs) Handles btn_Next.Click
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_V3Connection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If tb_ObjectivesCurrent.Visible = True Then 'TAB-1
                    lbtnStep4_Click(lbtnStep4, Nothing)
                    Save_Objectives(transaction)
                ElseIf tb_CareerAspirations.Visible = True Then 'TAB-3
                    lbtnStep3_Click(lbtnStep3, Nothing)
                    Save_CareerAspirations(transaction)
                ElseIf tb_competencies.Visible = True Then
                    lbtnStep2_Click(lbtnStep2, Nothing)
                    Save_COMPETENCIES(transaction)
                End If
            Catch ex As Exception
                ERRORMSG = "-1"
            Finally
                If ERRORMSG = "-1" Then

                    transaction.Rollback()
                Else
                    transaction.Commit()
                End If
            End Try
        End Using



    End Sub
    Private Sub Save_Objectives(sqltran As SqlTransaction)

        Dim pParms(45) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

        pParms(1) = New SqlClient.SqlParameter("@EOBJ_KRA_ID1", ddlKRA_1.SelectedValue) 'ddlKRA_1.InnerText.Trim())
        pParms(2) = New SqlClient.SqlParameter("@EOBJ_TEXT1", txt_OBJ1.InnerText.Trim())
        pParms(3) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT1", txt_KPI1.InnerText.Trim())
        pParms(4) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE1", txtWTG1.Value)

        pParms(5) = New SqlClient.SqlParameter("@EOBJ_KRA_ID2", ddlKRA_2.SelectedValue)
        pParms(6) = New SqlClient.SqlParameter("@EOBJ_TEXT2", txt_OBJ2.InnerText.Trim())
        pParms(7) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT2", txt_KPI2.InnerText.Trim())
        pParms(8) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE2", txtWTG2.Value)


        pParms(9) = New SqlClient.SqlParameter("@EOBJ_KRA_ID3", ddlKRA_3.SelectedValue)
        pParms(10) = New SqlClient.SqlParameter("@EOBJ_TEXT3", txt_OBJ3.InnerText.Trim())
        pParms(11) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT3", txt_KPI3.InnerText.Trim())
        pParms(12) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE3", txtWTG3.Value)


        pParms(13) = New SqlClient.SqlParameter("@EOBJ_KRA_ID4", ddlKRA_4.SelectedValue)
        pParms(14) = New SqlClient.SqlParameter("@EOBJ_TEXT4", txt_OBJ4.InnerText.Trim())
        pParms(15) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT4", txt_KPI4.InnerText.Trim())
        pParms(16) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE4", txtWTG4.Value)

        pParms(17) = New SqlClient.SqlParameter("@EOBJ_KRA_ID5", ddlKRA_5.SelectedValue)
        pParms(18) = New SqlClient.SqlParameter("@EOBJ_TEXT5", txt_OBJ5.InnerText.Trim())
        pParms(19) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT5", txt_KPI5.InnerText.Trim())
        pParms(20) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE5", txtWTG5.Value)



        pParms(21) = New SqlClient.SqlParameter("@EOBJ_KRA_ID6", ddlKRA_6.SelectedValue)
        pParms(22) = New SqlClient.SqlParameter("@EOBJ_TEXT6", txt_OBJ6.InnerText.Trim())
        pParms(23) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT6", txt_KPI6.InnerText.Trim())
        pParms(24) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE6", txtWTG6.Value)


        pParms(25) = New SqlClient.SqlParameter("@EOBJ_KRA_ID7", ddlKRA_7.SelectedValue)
        pParms(26) = New SqlClient.SqlParameter("@EOBJ_TEXT7", txt_OBJ7.InnerText.Trim())
        pParms(27) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT7", txt_KPI7.InnerText.Trim())
        pParms(28) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE7", txtWTG7.Value)

        pParms(29) = New SqlClient.SqlParameter("@EOBJ_KRA_ID8", ddlKRA_8.SelectedValue)
        pParms(30) = New SqlClient.SqlParameter("@EOBJ_TEXT8", txt_OBJ8.InnerText.Trim())
        pParms(31) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT8", txt_KPI8.InnerText.Trim())
        pParms(32) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE8", txtWTG8.Value)

        pParms(33) = New SqlClient.SqlParameter("@EOBJ_KRA_ID9", ddlKRA_9.SelectedValue)
        pParms(34) = New SqlClient.SqlParameter("@EOBJ_TEXT9", txt_OBJ9.InnerText.Trim())
        pParms(35) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT9", txt_KPI9.InnerText.Trim())
        pParms(36) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE9", txtWTG9.Value)


        pParms(37) = New SqlClient.SqlParameter("@EOBJ_KRA_ID10", ddlKRA_10.SelectedValue)
        pParms(38) = New SqlClient.SqlParameter("@EOBJ_TEXT10", txt_OBJ10.InnerText.Trim())
        pParms(39) = New SqlClient.SqlParameter("@EOBJ_KPI_TEXT10", txt_KPI10.InnerText.Trim())
        pParms(40) = New SqlClient.SqlParameter("@EOBJ_WEIGTAGE10", txtWTG10.Value)



        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.SAVE_EMP_OBJECTIVE", pParms)

    End Sub
    Private Sub Save_COMPETENCIES(sqltran As SqlTransaction)

        Dim pParms(32) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

        pParms(1) = New SqlClient.SqlParameter("@COMP_COM_ID1", ddlCOM_1.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@COMP_EMP_CMNT1", txt_COM1.InnerText.Trim())
        pParms(3) = New SqlClient.SqlParameter("@COMP_MGR_CMNT1", txt_COM_MGR1.InnerText.Trim())


        pParms(4) = New SqlClient.SqlParameter("@COMP_COM_ID2", ddlCOM_2.SelectedValue)
        pParms(5) = New SqlClient.SqlParameter("@COMP_EMP_CMNT2", txt_COM2.InnerText.Trim())
        pParms(6) = New SqlClient.SqlParameter("@COMP_MGR_CMNT2", txt_COM_MGR2.InnerText.Trim())


        pParms(7) = New SqlClient.SqlParameter("@COMP_COM_ID3", ddlCOM_3.SelectedValue)
        pParms(8) = New SqlClient.SqlParameter("@COMP_EMP_CMNT3", txt_COM3.InnerText.Trim())
        pParms(9) = New SqlClient.SqlParameter("@COMP_MGR_CMNT3", txt_COM_MGR3.InnerText.Trim())


        pParms(10) = New SqlClient.SqlParameter("@COMP_COM_ID4", ddlCOM_4.SelectedValue)
        pParms(11) = New SqlClient.SqlParameter("@COMP_EMP_CMNT4", txt_COM4.InnerText.Trim())
        pParms(12) = New SqlClient.SqlParameter("@COMP_MGR_CMNT4", txt_COM_MGR4.InnerText.Trim())

        pParms(13) = New SqlClient.SqlParameter("@COMP_COM_ID5", ddlCOM_5.SelectedValue)
        pParms(14) = New SqlClient.SqlParameter("@COMP_EMP_CMNT5", txt_COM5.InnerText.Trim())
        pParms(15) = New SqlClient.SqlParameter("@COMP_MGR_CMNT5", txt_COM_MGR5.InnerText.Trim())


        pParms(16) = New SqlClient.SqlParameter("@COMP_COM_ID6", ddlCOM_6.SelectedValue)
        pParms(17) = New SqlClient.SqlParameter("@COMP_EMP_CMNT6", txt_COM6.InnerText.Trim())
        pParms(18) = New SqlClient.SqlParameter("@COMP_MGR_CMNT6", txt_COM_MGR6.InnerText.Trim())


        pParms(19) = New SqlClient.SqlParameter("@COMP_COM_ID7", ddlCOM_7.SelectedValue)
        pParms(20) = New SqlClient.SqlParameter("@COMP_EMP_CMNT7", txt_COM7.InnerText.Trim())
        pParms(21) = New SqlClient.SqlParameter("@COMP_MGR_CMNT7", txt_COM_MGR7.InnerText.Trim())

        pParms(22) = New SqlClient.SqlParameter("@COMP_COM_ID8", ddlCOM_8.SelectedValue)
        pParms(23) = New SqlClient.SqlParameter("@COMP_EMP_CMNT8", txt_COM8.InnerText.Trim())
        pParms(24) = New SqlClient.SqlParameter("@COMP_MGR_CMNT8", txt_COM_MGR8.InnerText.Trim())

        pParms(25) = New SqlClient.SqlParameter("@COMP_COM_ID9", ddlCOM_9.SelectedValue)
        pParms(26) = New SqlClient.SqlParameter("@COMP_EMP_CMNT9", txt_COM9.InnerText.Trim())
        pParms(27) = New SqlClient.SqlParameter("@COMP_MGR_CMNT9", txt_COM_MGR9.InnerText.Trim())


        pParms(28) = New SqlClient.SqlParameter("@COMP_COM_ID10", ddlCOM_10.SelectedValue)
        pParms(29) = New SqlClient.SqlParameter("@COMP_EMP_CMNT10", txt_COM10.InnerText.Trim())
        pParms(30) = New SqlClient.SqlParameter("@COMP_MGR_CMNT10", txt_COM_MGR10.InnerText.Trim())



        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.SAVE_EMP_COMPETENCIES", pParms)

    End Sub
    Private Sub SAVE_CAREER_DEVELOPMENT(sqltran As SqlTransaction)

        Dim PARAM(10) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))

        PARAM(1) = New SqlClient.SqlParameter("@EDEV_GOAL1", txt_developGoals1.InnerText.Trim())
        PARAM(2) = New SqlClient.SqlParameter("@EDEV_MEASURE1", txt_ExpectedOutcome1.InnerText.Trim())

        PARAM(3) = New SqlClient.SqlParameter("@EDEV_GOAL2", txt_developGoals2.InnerText.Trim())
        PARAM(4) = New SqlClient.SqlParameter("@EDEV_MEASURE2", txt_ExpectedOutcome2.InnerText.Trim())


        PARAM(5) = New SqlClient.SqlParameter("@EDEV_GOAL3", txt_developGoals3.InnerText.Trim())
        PARAM(6) = New SqlClient.SqlParameter("@EDEV_MEASURE3", txt_ExpectedOutcome3.InnerText.Trim())

        PARAM(7) = New SqlClient.SqlParameter("@EDEV_GOAL4", txt_developGoals4.InnerText.Trim())
        PARAM(8) = New SqlClient.SqlParameter("@EDEV_MEASURE4", txt_ExpectedOutcome4.InnerText.Trim())


        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.SAVE_EMP_DEVELOPMENT", PARAM)

    End Sub
    Private Sub Save_CareerAspirations(SQLTRAN As SqlTransaction)
        Dim allCheckedItems As New StringBuilder()
        For Each itm As ListItem In ddl_mobility.Items
            If (itm.Selected = True) Then
                allCheckedItems = allCheckedItems.Append(itm.Value)
                allCheckedItems = allCheckedItems.Append("|")
            End If
        Next

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(1) = New SqlClient.SqlParameter("@EPR_CAREER_ASP", txt_CarrierInput.InnerText.Trim())
        pParms(2) = New SqlClient.SqlParameter("@EPR_PROF_DEVEL", txt_ProfDevelNeeds.InnerText.Trim())
        pParms(3) = New SqlClient.SqlParameter("@EPR_EPD_MOBILITY_ID", allCheckedItems.ToString.Trim())

        SqlHelper.ExecuteNonQuery(SQLTRAN, CommandType.StoredProcedure, "PDP_N.SAVE_EMP_CAREER_ASP", pParms)
    End Sub
    Protected Sub btn_SaveDraft_Click(sender As Object, e As EventArgs) Handles btn_SaveDraft.Click
        divNote.Visible = False
        divNote.Attributes("class") = ""
        lblError.Text = ""

        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Save_Objectives(sqltran)
            SAVE_CAREER_DEVELOPMENT(sqltran)
            Save_CareerAspirations(sqltran)
            Save_COMPETENCIES(sqltran)
            sqltran.Commit()
            ' Bind_Objective()
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
            lblError.Text = "<div>You have successfully saved the Performance Development Plan </div>"

        Catch ex As Exception
            sqltran.Rollback()

            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Error occured while saving </div>"
        End Try





    End Sub

    Protected Sub btn_SaveFinish_Click(sender As Object, e As EventArgs) Handles btn_SaveFinish.Click

        Dim str_MandatoryMsg As String = ""
        str_MandatoryMsg = "<div>Following fields need to be completed before submitting the Performance Development Plan</div>"
        Dim i As Integer = 0
        Dim OBJ As String = CHECK_OBJ_COMPLETED()
        If ViewState("IS_EMP") = True Then
            If (OBJ <> "") Then
                str_MandatoryMsg = str_MandatoryMsg + "<div>" + OBJ + "</div>"
                i = +1
            End If
        End If


        If (i = 0) Then
            Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
            Dim con As SqlConnection = New SqlConnection(connection)
            con.Open()
            Dim sqltran As SqlTransaction
            sqltran = con.BeginTransaction("trans")
            Dim bFinished As Integer = 0

            Try

                Save_Objectives(sqltran)
                SAVE_CAREER_DEVELOPMENT(sqltran)
                Save_CareerAspirations(sqltran)
                Save_COMPETENCIES(sqltran)

                Dim NXTLVL_EMP_ID As String = GET_NEXTLEVEL_REVIEWER(sqltran)
                If ViewState("INFO_TYPE") = 1 Then
                    FWD_TO_NEXTLEVEL_REVIEWER(sqltran, NXTLVL_EMP_ID)
                End If

                btn_Next.Visible = False
                btn_Previous.Visible = False
                btn_SaveDraft.Visible = False
                btn_SaveFinish.Visible = False


                sqltran.Commit()
                bFinished = 1
            Catch ex As Exception
                bFinished = 0
                sqltran.Rollback()
            End Try
            If (bFinished = 1) Then
                aFTER_SAVE()

                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                If ViewState("IS_MGR") = True Then
                    lblError.Text = "<div>You have successfully approved the Performance Development Plan </div>"
                Else
                    lblError.Text = "<div>You have successfully completed the Performance Development Plan </div>"
                End If
            Else
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoError"
                lblError.Text = "<div>Error occured while saving </div>"
            End If
        Else
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = str_MandatoryMsg

        End If
    End Sub
    Private Function GET_NEXTLEVEL_REVIEWER(sqltran As SqlTransaction) As String

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", ViewState("INFO_TYPE"))
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
        pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
        pParms(3) = New SqlClient.SqlParameter("@REVIEWING_EMP_ID", ViewState("RVW_EMP_ID"))
        Dim NXTLVL_EMP_ID As String = SqlHelper.ExecuteScalar(sqltran, CommandType.StoredProcedure, "PDP_N.GET_NEXTLEVEL_REVIEWER", pParms)
        Return NXTLVL_EMP_ID
    End Function
    Private Sub FWD_TO_NEXTLEVEL_REVIEWER(sqltran As SqlTransaction, NXTLVL_EMP_ID As String)
        Dim TR As String = String.Empty
        Try
            Dim URL As String
            Dim StaffURL As String = String.Empty
            Dim StaffUrlPage As String = "https://school.gemsoasis.com/pdp/pdpCStaff.aspx"
            Dim urlPage As String = "https://school.gemsoasis.com/pdp/pdpCStaff.aspx" ' HttpContext.Current.Request.Url.AbsoluteUri
            'StaffUrlPage = urlPage

            'If urlPage.Contains("?") Then
            '    urlPage = urlPage.Remove(urlPage.IndexOf("?"))
            '    StaffUrlPage = urlPage.Remove(urlPage.IndexOf("?"))
            'End If

            'urlPage = urlPage.Replace("ApproveRequestFromEmail.aspx", "ApproveRequestFromEmail.aspx")
            URL = urlPage & "?U=" + Encr_decrData.Encrypt(Convert.ToString(ViewState("EMP_ID"))) + "&P=" + Encr_decrData.Encrypt(Convert.ToString(ViewState("EPR_ID"))) + "&R=" + Encr_decrData.Encrypt(NXTLVL_EMP_ID)
            StaffURL = StaffUrlPage & "?U=" + Encr_decrData.Encrypt(Convert.ToString(ViewState("EMP_ID"))) + "&P=" + Encr_decrData.Encrypt(Convert.ToString(ViewState("EPR_ID"))) + "&R=" + Encr_decrData.Encrypt(ViewState("EMP_ID"))
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", ViewState("INFO_TYPE"))
            pParms(1) = New SqlClient.SqlParameter("@URL", URL)
            pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
            pParms(3) = New SqlClient.SqlParameter("@STAFF_ID", ViewState("EMP_ID"))
            pParms(4) = New SqlClient.SqlParameter("@REVIEWER_EMP_ID", ViewState("RVW_EMP_ID"))
            pParms(5) = New SqlClient.SqlParameter("@NXTLVL_EMP_ID", NXTLVL_EMP_ID)
            pParms(6) = New SqlClient.SqlParameter("@StaffURL", StaffURL)
            SqlHelper.ExecuteScalar(sqltran, CommandType.StoredProcedure, "PDP_N.FWDTO_NEXTLEVEL_REVIEWER", pParms)

        Catch ex As Exception
            TR = ex.Message
        End Try
    End Sub
    Private Sub aFTER_SAVE()
        Dim FLAG As Boolean = ValidateUser()
        BIND_EMP_OBJECTIVES()
        BIND_EMP_COMPETENCIES()
        BIND_CAREER_DEVEL()
        Bind_GetPDP_CompletedDate()
        Set_ButtonViewRights()
    End Sub

    Protected Sub hf1_ServerClick(sender As Object, e As EventArgs) Handles hf1.ServerClick
        spPage.InnerHtml = "Page <font color='#58B0E7'>1</font> of 2"

        hf1.Attributes.Add("class", "current")
        hf2.Attributes.Add("class", " ")

        ddlKRA_1.Visible = True
        ddlKRA_2.Visible = True
        ddlKRA_3.Visible = True
        ddlKRA_4.Visible = True
        ddlKRA_5.Visible = True

        ddlKRA_6.Visible = False
        ddlKRA_7.Visible = False
        ddlKRA_8.Visible = False
        ddlKRA_9.Visible = False
        ddlKRA_10.Visible = False


        txt_OBJ1.Visible = True
        txt_OBJ2.Visible = True
        txt_OBJ3.Visible = True
        txt_OBJ4.Visible = True
        txt_OBJ5.Visible = True
        txt_OBJ6.Visible = False
        txt_OBJ7.Visible = False
        txt_OBJ8.Visible = False
        txt_OBJ9.Visible = False
        txt_OBJ10.Visible = False

        txt_KPI1.Visible = True
        txt_KPI2.Visible = True
        txt_KPI3.Visible = True
        txt_KPI4.Visible = True
        txt_KPI5.Visible = True
        txt_KPI6.Visible = False
        txt_KPI7.Visible = False
        txt_KPI8.Visible = False
        txt_KPI9.Visible = False
        txt_KPI10.Visible = False

        divWTG1.Visible = True
        divWTG2.Visible = True
        divWTG3.Visible = True
        divWTG4.Visible = True
        divWTG5.Visible = True
        divWTG6.Visible = False
        divWTG7.Visible = False
        divWTG8.Visible = False
        divWTG9.Visible = False
        divWTG10.Visible = False
        divNote.Visible = False
    End Sub

    Protected Sub hf2_ServerClick(sender As Object, e As EventArgs) Handles hf2.ServerClick
        spPage.InnerHtml = "Page <font color='#58B0E7'>2</font> of 2"
        hf2.Attributes.Add("class", "current")
        hf1.Attributes.Add("class", " ")



        ddlKRA_1.Visible = False
        ddlKRA_2.Visible = False
        ddlKRA_3.Visible = False
        ddlKRA_4.Visible = False
        ddlKRA_5.Visible = False

        ddlKRA_6.Visible = True
        ddlKRA_7.Visible = True
        ddlKRA_8.Visible = True
        ddlKRA_9.Visible = True
        ddlKRA_10.Visible = True


        txt_OBJ1.Visible = False
        txt_OBJ2.Visible = False
        txt_OBJ3.Visible = False
        txt_OBJ4.Visible = False
        txt_OBJ5.Visible = False

        txt_OBJ6.Visible = True
        txt_OBJ7.Visible = True
        txt_OBJ8.Visible = True
        txt_OBJ9.Visible = True
        txt_OBJ10.Visible = True

        txt_KPI1.Visible = False
        txt_KPI2.Visible = False
        txt_KPI3.Visible = False
        txt_KPI4.Visible = False
        txt_KPI5.Visible = False

        txt_KPI6.Visible = True
        txt_KPI7.Visible = True
        txt_KPI8.Visible = True
        txt_KPI9.Visible = True
        txt_KPI10.Visible = True

        divWTG1.Visible = False
        divWTG2.Visible = False
        divWTG3.Visible = False
        divWTG4.Visible = False
        divWTG5.Visible = False

        divWTG6.Visible = True
        divWTG7.Visible = True
        divWTG8.Visible = True
        divWTG9.Visible = True
        divWTG10.Visible = True
        divNote.Visible = False
    End Sub

    Private Function CHECK_OBJ_COMPLETED() As String
        Dim ERRMSG As String = String.Empty
        Dim WTG As Decimal
        Dim I As Integer = 0
        If (txt_OBJ1.InnerText.Trim() <> "" And txt_KPI1.InnerText.Trim() <> "" And ddlKRA_1.SelectedValue <> "0") Then
            I = I + 1
        End If

        If (txt_OBJ2.InnerText.Trim() <> "" And txt_KPI2.InnerText.Trim() <> "" And ddlKRA_2.SelectedValue <> "0") Then
            I = I + 1
        End If
        If (txt_OBJ3.InnerText.Trim() <> "" And txt_KPI3.InnerText.Trim() <> "" And ddlKRA_3.SelectedValue <> "0") Then
            I = I + 1
        End If

        If (txt_OBJ4.InnerText.Trim() <> "" And txt_KPI4.InnerText.Trim() <> "" And ddlKRA_4.SelectedValue <> "0") Then
            I = I + 1
        End If

        If (txt_OBJ5.InnerText.Trim() <> "" And txt_KPI5.InnerText.Trim() <> "" And ddlKRA_5.SelectedValue <> "0") Then
            I = I + 1
        End If

        If (txt_OBJ6.InnerText.Trim() <> "" And txt_KPI6.InnerText.Trim() <> "" And ddlKRA_6.SelectedValue <> "0") Then
            I = I + 1
        End If

        If (txt_OBJ7.InnerText.Trim() <> "" And txt_KPI7.InnerText.Trim() <> "" And ddlKRA_7.SelectedValue <> "0") Then
            I = I + 1
        End If

        If (txt_OBJ8.InnerText.Trim() <> "" And txt_KPI8.InnerText.Trim() <> "" And ddlKRA_8.SelectedValue <> "0") Then
            I = I + 1
        End If

        If (txt_OBJ9.InnerText.Trim() <> "" And txt_KPI9.InnerText.Trim() <> "" And ddlKRA_9.SelectedValue <> "0") Then
            I = I + 1
        End If

        If (txt_OBJ10.InnerText.Trim() <> "" And txt_KPI10.InnerText.Trim() <> "" And ddlKRA_10.SelectedValue <> "0") Then
            I = I + 1
        End If

        If txtWTG1.Value <> "" Then
            WTG += CDec(txtWTG1.Value)
        End If
        If txtWTG2.Value <> "" Then
            WTG += CDec(txtWTG2.Value)
        End If
        If txtWTG3.Value <> "" Then
            WTG += CDec(txtWTG3.Value)
        End If
        If txtWTG4.Value <> "" Then
            WTG += CDec(txtWTG4.Value)
        End If
        If txtWTG5.Value <> "" Then
            WTG += CDec(txtWTG5.Value)
        End If
        If txtWTG6.Value <> "" Then
            WTG += CDec(txtWTG6.Value)
        End If
        If txtWTG7.Value <> "" Then
            WTG += CDec(txtWTG7.Value)
        End If
        If txtWTG8.Value <> "" Then
            WTG += CDec(txtWTG8.Value)
        End If
        If txtWTG9.Value <> "" Then
            WTG += CDec(txtWTG9.Value)
        End If
        If txtWTG10.Value <> "" Then
            WTG += CDec(txtWTG10.Value)
        End If

        'If ViewState("EMP_BSU_TAG") = "G" Then
        '    If I < 5 Then


        '        ERRMSG = "At least 5 SMART Objective required "

        '    End If
        'Else
        If I < 3 Then


            ERRMSG = "At least 3 SMART Objectives required.Please enter the Objective and KPI for a selected Key Result Area "

        End If
        ' End If


        'If ((txt_OBJ1.InnerText.Trim() = "" And txt_KPI1.InnerText.Trim() <> "") Or (txt_OBJ1.InnerText.Trim() <> "" And txt_KPI1.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        'If ((txt_OBJ2.InnerText.Trim() = "" And txt_KPI2.InnerText.Trim() <> "") Or (txt_OBJ2.InnerText.Trim() <> "" And txt_KPI2.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If
        'If ((txt_OBJ3.InnerText.Trim() = "" And txt_KPI3.InnerText.Trim() <> "") Or (txt_OBJ3.InnerText.Trim() <> "" And txt_KPI3.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        'If ((txt_OBJ4.InnerText.Trim() = "" And txt_KPI4.InnerText.Trim() <> "") Or (txt_OBJ4.InnerText.Trim() <> "" And txt_KPI4.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        'If ((txt_OBJ5.InnerText.Trim() = "" And txt_KPI5.InnerText.Trim() <> "") Or (txt_OBJ5.InnerText.Trim() <> "" And txt_KPI5.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        'If ((txt_OBJ6.InnerText.Trim() = "" And txt_KPI6.InnerText.Trim() <> "") Or (txt_OBJ6.InnerText.Trim() <> "" And txt_KPI6.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        'If ((txt_OBJ7.InnerText.Trim() = "" And txt_KPI7.InnerText.Trim() <> "") Or (txt_OBJ7.InnerText.Trim() <> "" And txt_KPI7.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        'If ((txt_OBJ8.InnerText.Trim() = "" And txt_KPI8.InnerText.Trim() <> "") Or (txt_OBJ8.InnerText.Trim() <> "" And txt_KPI8.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        'If (ddlKRA_9.SelectedValue = "0" And (txt_OBJ9.InnerText.Trim() = "" And txt_KPI9.InnerText.Trim() <> "") Or (txt_OBJ9.InnerText.Trim() <> "" And txt_KPI9.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        'If ((txt_OBJ10.InnerText.Trim() = "" And txt_KPI10.InnerText.Trim() <> "") Or (txt_OBJ10.InnerText.Trim() <> "" And txt_KPI10.InnerText.Trim() = "")) Then
        '    ERRMSG = "KRA or Objective or KPI field is empty , please check."
        '    Return ERRMSG
        '    Exit Function
        'End If

        If (ddlKRA_1.SelectedValue = "0" And txt_OBJ1.InnerText.Trim() <> "" And txt_KPI1.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_2.SelectedValue = "0" And txt_OBJ2.InnerText.Trim() <> "" And txt_KPI2.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_3.SelectedValue = "0" And txt_OBJ3.InnerText.Trim() <> "" And txt_KPI3.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_4.SelectedValue = "0" And txt_OBJ4.InnerText.Trim() <> "" And txt_KPI4.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_5.SelectedValue = "0" And txt_OBJ5.InnerText.Trim() <> "" And txt_KPI5.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_6.SelectedValue = "0" And txt_OBJ6.InnerText.Trim() <> "" And txt_KPI6.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_7.SelectedValue = "0" And txt_OBJ7.InnerText.Trim() <> "" And txt_KPI7.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_8.SelectedValue = "0" And txt_OBJ8.InnerText.Trim() <> "" And txt_KPI8.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_9.SelectedValue = "0" And txt_OBJ9.InnerText.Trim() <> "" And txt_KPI9.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_10.SelectedValue = "0" And txt_OBJ10.InnerText.Trim() <> "" And txt_KPI10.InnerText.Trim() <> "") Then
            ERRMSG = "KRA is not selected , please check."
            Return ERRMSG
            Exit Function
        End If

        If (ddlKRA_1.SelectedValue <> "0" And (txt_OBJ1.InnerText.Trim() = "" Or txt_KPI1.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_2.SelectedValue <> "0" And (txt_OBJ2.InnerText.Trim() = "" Or txt_KPI2.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_3.SelectedValue <> "0" And (txt_OBJ3.InnerText.Trim() = "" Or txt_KPI3.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_4.SelectedValue <> "0" And (txt_OBJ4.InnerText.Trim() = "" Or txt_KPI4.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_5.SelectedValue <> "0" And (txt_OBJ5.InnerText.Trim() = "" Or txt_KPI5.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_6.SelectedValue <> "0" And (txt_OBJ6.InnerText.Trim() = "" Or txt_KPI6.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_7.SelectedValue <> "0" And (txt_OBJ7.InnerText.Trim() = "" Or txt_KPI7.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_8.SelectedValue <> "0" And (txt_OBJ8.InnerText.Trim() = "" Or txt_KPI8.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_9.SelectedValue <> "0" And (txt_OBJ9.InnerText.Trim() = "" Or txt_KPI9.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If
        If (ddlKRA_10.SelectedValue <> "0" And (txt_OBJ10.InnerText.Trim() = "" Or txt_KPI10.InnerText.Trim() = "")) Then
            ERRMSG = "Objective or KPI field is empty , please check."
            Return ERRMSG
            Exit Function
        End If

       If ((ddlCOM_1.SelectedValue <> "0" And txt_COM1.InnerText.Trim() = "") Or (ddlCOM_1.SelectedValue = "0" And txt_COM1.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function

        End If
        If ((ddlCOM_2.SelectedValue <> "0" And txt_COM2.InnerText.Trim() = "") Or (ddlCOM_2.SelectedValue = "0" And txt_COM2.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If
        If ((ddlCOM_3.SelectedValue <> "0" And txt_COM3.InnerText.Trim() = "") Or (ddlCOM_3.SelectedValue = "0" And txt_COM3.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If
        If ((ddlCOM_4.SelectedValue <> "0" And txt_COM4.InnerText.Trim() = "") Or (ddlCOM_4.SelectedValue = "0" And txt_COM4.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If
        If ((ddlCOM_5.SelectedValue <> "0" And txt_COM5.InnerText.Trim() = "") Or (ddlCOM_5.SelectedValue = "0" And txt_COM5.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If
        If ((ddlCOM_6.SelectedValue <> "0" And txt_COM6.InnerText.Trim() = "") Or (ddlCOM_6.SelectedValue = "0" And txt_COM6.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If
        If ((ddlCOM_7.SelectedValue <> "0" And txt_COM7.InnerText.Trim() = "") Or (ddlCOM_7.SelectedValue = "0" And txt_COM7.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If
        If ((ddlCOM_8.SelectedValue <> "0" And txt_COM8.InnerText.Trim() = "") Or (ddlCOM_8.SelectedValue = "0" And txt_COM8.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If
        If ((ddlCOM_9.SelectedValue <> "0" And txt_COM9.InnerText.Trim() = "") Or (ddlCOM_9.SelectedValue = "0" And txt_COM9.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If
        If ((ddlCOM_10.SelectedValue <> "0" And txt_COM10.InnerText.Trim() = "") Or (ddlCOM_10.SelectedValue = "0" And txt_COM10.InnerText.Trim() <> "")) Then
            ERRMSG = "Competencies or Employee comments field is empty on Competencies Page , please check."
            Return ERRMSG
            Exit Function
        End If

        Return ERRMSG

    End Function


    Private Function CHECK_DEVELOPMENT_GOALS_COMPLETED() As String
        Dim ERRMSG As String = String.Empty
        Dim i As Integer = 0
        'If (Convert.ToInt16(ViewState("GOALS_REQ")) <> 0) Then
        If (txt_developGoals1.InnerText.Trim() <> "" And txt_ExpectedOutcome1.InnerText.Trim() <> "") Then
            i = i + 1
        End If
        If (txt_developGoals2.InnerText.Trim() <> "" And txt_ExpectedOutcome2.InnerText.Trim() <> "") Then
            i = i + 1
        End If
        If (txt_developGoals3.InnerText.Trim() <> "" And txt_ExpectedOutcome3.InnerText.Trim() <> "") Then
            i = i + 1
        End If
        If (txt_developGoals4.InnerText.Trim() <> "" And txt_ExpectedOutcome4.InnerText.Trim() <> "") Then
            i = i + 1
        End If

        If i = 0 Then
            ERRMSG = "At least one  development goals required"
        End If
        Return ERRMSG
    End Function
    Private Function CHECK_CAREER_ASP_COMPLETED() As String

        Dim errmsg As String = String.Empty
        Dim i As Integer = 0
        If txt_CarrierInput.InnerText.Trim() <> "" Then
            i = i + 1
        End If

        If i = 0 Then

            errmsg = " Career Aspirations not entered"
        End If
        Return errmsg
    End Function
    Private Sub check_list()
        Dim allCheckedItems As New StringBuilder()
        Dim i As Integer = 0
        While i < ddl_mobility.Items.Count

            If ddl_mobility.Items(0).Selected Then
                ddl_mobility.Items(i).Selected = False
                ddl_mobility.Items(0).Selected = True
            End If
            i = i + 1
        End While
    End Sub

    Protected Sub lbtnRevert_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Panel_RevertBack.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_RIEVEWERS_TOREVERT", pParms)
                rbtnList_Rievewer.DataSource = ds
                rbtnList_Rievewer.DataTextField = "EMP_NAME"
                rbtnList_Rievewer.DataValueField = "EMP_ID"
                rbtnList_Rievewer.DataBind()
                rbtnList_Rievewer.Items(0).Selected = True
                'rptPDP_Reveiwer.DataSource = ds
                'rptPDP_Reveiwer.DataBind()
            End Using
        Catch ex As Exception
            Panel_RevertBack.Visible = False

        End Try


    End Sub
    Protected Sub btn_Revert_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Revert.Click
        '  Panel_RevertBack.Visible = False
        divNote.Visible = False
        Dim lnkRev As LinkButton = sender.parent.FindControl("lbtnRevNew")
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim redirectUrl As String

        redirectUrl = "pdpCStaff_V2.aspx?ER=" + Encr_decrData.Encrypt(ViewState("EPR_ID")) + "&U=" + Encr_decrData.Encrypt(ViewState("EMP_ID")) + "&MR=" + Encr_decrData.Encrypt(rbtnList_Rievewer.SelectedValue)
        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If
        url = LCase(url)
        url = url.Replace("pdpdashboard.aspx", redirectUrl)
        url = url.Replace("http:", "https:")
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", rbtnList_Rievewer.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("EPR_ID"))
            pParms(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks.InnerText.Trim())
            pParms(4) = New SqlClient.SqlParameter("@URL", url)
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.REVERT_BACKTO_REVIEWER", pParms)
            sqltran.Commit()

            aFTER_SAVE()

            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
            lblError.Text = "<div>You have successfully reverted the Performance Development Plan </div>"

            Panel_RevertBack.Visible = False
        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub
    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Cancel.Click
        Panel_RevertBack.Visible = False
        rbtnList_Rievewer.ClearSelection()
        txt_Remarks.InnerText = ""

    End Sub

    Protected Sub A1_ServerClick(sender As Object, e As EventArgs) Handles A1.ServerClick
        Span2.InnerHtml = "Page <font color='#58B0E7'>1</font> of 2"

        A1.Attributes.Add("class", "current")
        A2.Attributes.Add("class", " ")

        ddlCOM_1.Visible = True
        ddlCOM_2.Visible = True
        ddlCOM_3.Visible = True
        ddlCOM_4.Visible = True
        ddlCOM_5.Visible = True

        ddlCOM_6.Visible = False
        ddlCOM_7.Visible = False
        ddlCOM_8.Visible = False
        ddlCOM_9.Visible = False
        ddlCOM_10.Visible = False


        txt_COM1.Visible = True
        txt_COM2.Visible = True
        txt_COM3.Visible = True
        txt_COM4.Visible = True
        txt_COM5.Visible = True

        txt_COM6.Visible = False
        txt_COM7.Visible = False
        txt_COM8.Visible = False
        txt_COM9.Visible = False
        txt_COM10.Visible = False

        txt_COM_MGR1.Visible = True
        txt_COM_MGR2.Visible = True
        txt_COM_MGR3.Visible = True
        txt_COM_MGR4.Visible = True
        txt_COM_MGR5.Visible = True

        txt_COM_MGR6.Visible = False
        txt_COM_MGR7.Visible = False
        txt_COM_MGR8.Visible = False
        txt_COM_MGR9.Visible = False
        txt_COM_MGR10.Visible = False

        
        divNote.Visible = False
    End Sub

    Protected Sub A2_ServerClick(sender As Object, e As EventArgs) Handles A2.ServerClick
        Span2.InnerHtml = "Page <font color='#58B0E7'>2</font> of 2"
        A2.Attributes.Add("class", "current")
        A1.Attributes.Add("class", " ")

        ddlCOM_1.Visible = False
        ddlCOM_2.Visible = False
        ddlCOM_3.Visible = False
        ddlCOM_4.Visible = False
        ddlCOM_5.Visible = False

        ddlCOM_6.Visible = True
        ddlCOM_7.Visible = True
        ddlCOM_8.Visible = True
        ddlCOM_9.Visible = True
        ddlCOM_10.Visible = True


        txt_COM1.Visible = False
        txt_COM2.Visible = False
        txt_COM3.Visible = False
        txt_COM4.Visible = False
        txt_COM5.Visible = False

        txt_COM6.Visible = True
        txt_COM7.Visible = True
        txt_COM8.Visible = True
        txt_COM9.Visible = True
        txt_COM10.Visible = True

        txt_COM_MGR1.Visible = False
        txt_COM_MGR2.Visible = False
        txt_COM_MGR3.Visible = False
        txt_COM_MGR4.Visible = False
        txt_COM_MGR5.Visible = False

        txt_COM_MGR6.Visible = True
        txt_COM_MGR7.Visible = True
        txt_COM_MGR8.Visible = True
        txt_COM_MGR9.Visible = True
        txt_COM_MGR10.Visible = True

        divNote.Visible = False
    End Sub
End Class
