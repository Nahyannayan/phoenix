﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Configuration
Imports Telerik.Web.UI
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class pdpDashboard
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'Session("sUsr_name") = "manoj"
        'Session("sUsr_name") = "prem.sunder"
        'Session("sUsr_name") = "ROBBIE"
        'Session("sUsr_name") = "anne.tice"
        'Session("sUsr_name") = "lijo"
        btnPublish.Visible = False
        GETCURRENT_YEAR_nEW()
        If Session("sUsr_name") <> "" Then
            If Page.IsPostBack = False Then
                'Session("sUsr_name") = "prem.sunder"
                Bind_Department()
                Bind_Levels()
                Bind_Level1()
                Bind_Level2()


                GETCURRENT_YEAR()

                'gridbind()

                ' gridbind()
            End If
        Else
            Response.Redirect("../login.aspx")
        End If
        Dim ToolkitScriptManager1 As AjaxControlToolkit.ToolkitScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me.Page)
        ToolkitScriptManager1.RegisterPostBackControl(btnExport)
        ToolkitScriptManager1.RegisterPostBackControl(btnPDF_D)
    End Sub
    Private Sub Bind_Department()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDPConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        '   pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP.GET_DEPARTEMENTS", pParms)

        ddlDept.DataSource = ds
        ddlDept.DataValueField = "DPT_ID"
        ddlDept.DataTextField = "DPT_DESCR"
        ddlDept.DataBind()
        ddlDept.Items.Insert(0, New ListItem("-- ALL --", "0"))
    End Sub
    Private Sub Bind_BSU()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        pParms(1) = New SqlClient.SqlParameter("@pdpy_id", ViewState("Year"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_businessunit", pParms)

        ddlBsu.DataSource = ds
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataBind()
        ddlBsu.Items.Insert(0, New ListItem("-- ALL --", "0"))
    End Sub
    Private Sub Bind_TYPE()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        pParms(1) = New SqlClient.SqlParameter("@pdpy_id", ViewState("Year"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_emp_types", pParms)

        ddlType.DataSource = ds
        ddlType.DataValueField = "DESIG"
        ddlType.DataTextField = "DESIG"
        ddlType.DataBind()
        ddlType.Items.Insert(0, New ListItem("-- ALL --", "0"))

        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        pParms(1) = New SqlClient.SqlParameter("@pdpy_id", ViewState("Year"))
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_emp_type_usr", pParms)
        If ds1.Tables(0).Rows(0).Item("EPR_DESIGNATION") <> "" Then
            ddlType.ClearSelection()
            ddlType.Items.FindByText(ds1.Tables(0).Rows(0).Item("EPR_DESIGNATION").ToString()).Selected = True
        End If
    End Sub
    Private Sub Bind_Levels()
        ddlPending.Items.Insert(0, New ListItem("-- Select --", ""))
        ddlPending.Items.Insert(1, New ListItem("Employee", "0"))
        ddlPending.Items.Insert(2, New ListItem("Reviewer 1", "1"))
        ddlPending.Items.Insert(3, New ListItem("Reviewer 2", "2"))
        ddlPending.Items.Insert(4, New ListItem("Reviewer 3", "3"))
    End Sub

    Private Sub Bind_Level1()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_EMPLOYEE_CORP", pParms)

        ddl_Level1.DataSource = ds
        ddl_Level1.DataValueField = "EMP_ID"
        ddl_Level1.DataTextField = "EMP_NAME"
        ddl_Level1.DataBind()
        ddl_Level1.Items.Insert(0, New ListItem("-- NOT APPLICABLE --", "0"))
    End Sub

    Private Sub Bind_Level2()
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_EMPLOYEE_CORP", pParms)

        ddl_Level2.DataSource = ds
        ddl_Level2.DataValueField = "EMP_ID"
        ddl_Level2.DataTextField = "EMP_NAME"
        ddl_Level2.DataBind()
        ddl_Level2.Items.Insert(0, New ListItem("-- NOT APPLICABLE --", "0"))
    End Sub
    'Private Sub Bind_Level3()
    '    Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
    '    Dim pParms(2) As SqlClient.SqlParameter
    '    pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_EMPLOYEE_CORP", pParms)

    '    ddl_Level3.DataSource = ds
    '    ddl_Level3.DataValueField = "EMP_ID"
    '    ddl_Level3.DataTextField = "EMP_NAME"
    '    ddl_Level3.DataBind()
    '    ddl_Level3.Items.Insert(0, New ListItem("-- NOT APPLICABLE --", "0"))
    'End Sub

    Private Sub gridbind()
        cbePublish.ConfirmText = "Are you sure you want to Publish the Rating to  " & ddlDept.SelectedItem.Text & " employees ?"
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString

        'Dim param(2) As SqlParameter
        'param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        'param(1) = New SqlParameter("@emp_id", 16450)
        Dim DS As New DataSet
        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        pParms(1) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_EmpName.Text.Trim() = "", DBNull.Value, txt_EmpName.Text.Trim()))
        pParms(2) = New SqlClient.SqlParameter("@EMP_NO", IIf(txt_EmpNo.Text.Trim() = "", DBNull.Value, txt_EmpNo.Text.Trim()))
        pParms(3) = New SqlClient.SqlParameter("@DPT_ID", IIf(ddlDept.SelectedValue = "0", DBNull.Value, ddlDept.SelectedValue))
        pParms(4) = New SqlClient.SqlParameter("@PENDINGUNDER_LVL_ID", IIf(ddlPending.SelectedValue = "", "-1", ddlPending.SelectedValue))
        pParms(5) = New SqlClient.SqlParameter("@PDPY_ID", ViewState("Year"))
        pParms(6) = New SqlClient.SqlParameter("@BSU_ID", IIf(ddlBsu.SelectedValue = "0", DBNull.Value, ddlBsu.SelectedValue))
        pParms(7) = New SqlClient.SqlParameter("@TYPE_ID", IIf(ddlType.SelectedValue = "0", DBNull.Value, ddlType.SelectedValue))

        DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[PDP_N].[GET_EMP_PDP_COMPLETIONDETAILS]", pParms)
        rptPdp.DataSource = DS.Tables(0)
        rptPdp.DataBind()
        DS.Clear()
    End Sub
    Public Sub ExportExcel(dt As DataTable)
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '  ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "ExportJobs.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "ExportPDPfil.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click


        gridbind()

    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
        Dim connStr As String
        Dim DS As New DataSet
        Dim pParms(9) As SqlClient.SqlParameter


        connStr = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString

        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        pParms(1) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_EmpName.Text.Trim() = "", DBNull.Value, txt_EmpName.Text.Trim()))
        pParms(2) = New SqlClient.SqlParameter("@EMP_NO", IIf(txt_EmpNo.Text.Trim() = "", DBNull.Value, txt_EmpNo.Text.Trim()))
        pParms(3) = New SqlClient.SqlParameter("@DPT_ID", IIf(ddlDept.SelectedValue = "0", DBNull.Value, ddlDept.SelectedValue))
        pParms(4) = New SqlClient.SqlParameter("@PENDINGUNDER_LVL_ID", IIf(ddlPending.SelectedValue = "", "-1", ddlPending.SelectedValue))
        pParms(5) = New SqlClient.SqlParameter("@PDPY_ID", ViewState("Year"))
        pParms(6) = New SqlClient.SqlParameter("@BSU_ID", IIf(ddlBsu.SelectedValue = "0", DBNull.Value, ddlBsu.SelectedValue))
        pParms(7) = New SqlClient.SqlParameter("@TYPE_ID", IIf(ddlType.SelectedValue = "0", DBNull.Value, ddlType.SelectedValue))

        DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[PDP_N].EXPORT_EMP_PDP_COMPLETIONDETAILS", pParms)

        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(DS.Tables(0), New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        'ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "ExportPDP.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "ExportPDP.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub
    Protected Sub lbtnRev_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
        Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
        Dim HF_USR_EMP_ID As HiddenField = sender.parent.FindControl("HF_USR_EMP_ID")

        Dim redirectUrl As String

        If ViewState("Year") = 7 Then
            redirectUrl = "pdpCStaff_V3.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)
        ElseIf ViewState("Year") = 6 Then
            redirectUrl = "pdpCStaff_V2.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)
        Else
            redirectUrl = "pdpCStaff.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)

        End If


        'redirectUrl = "pdpFinalStaff.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)

        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If
        url = LCase(url)
        url = url.Replace("pdpdashboard.aspx", redirectUrl)
        url = url.Replace("http:", "https:")
        Dim sb As New System.Text.StringBuilder("")
        sb.Append(Convert.ToString("popUpDetails('" + url + "');"))
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", sb.ToString(), True)
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "reportshow", sb.ToString())
    End Sub

    Protected Sub lbtnSetLevels_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EPR_ID") = HF_EPR_ID.Value
            ViewState("HF_EMP_ID") = ""
            ViewState("HF_EMP_ID") = HF_EMP_ID.Value
            Panel_SetLevel.Visible = True

            BIND_VIEW_LEVEL(HF_EPR_ID.Value)

            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString)
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)

                Dim ds As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "PDP_N.GET_RIEVEWERS_LEVEL", pParms)
                While ds.Read
                    lbl_EMPNAME.Text = Convert.ToString(ds("EMPNAME"))
                    ddl_Level1.ClearSelection()
                    ddl_Level2.ClearSelection()
                    '  ddl_Level3.ClearSelection()

                    If Not ddl_Level1.Items.FindByValue(ds("LVL1")) Is Nothing Then
                        ddl_Level1.Items.FindByValue(ds("LVL1")).Selected = True
                    Else
                        ddl_Level1.Items.FindByValue("0").Selected = True
                    End If

                    If Not ddl_Level2.Items.FindByValue(ds("LVL2")) Is Nothing Then
                        ddl_Level2.Items.FindByValue(ds("LVL2")).Selected = True
                    Else
                        ddl_Level2.Items.FindByValue("0").Selected = True
                    End If

                    'If Not ddl_Level3.Items.FindByValue(ds("LVL3")) Is Nothing Then
                    '    ddl_Level3.Items.FindByValue(ds("LVL3")).Selected = True
                    'Else

                    '    ddl_Level3.Items.FindByValue("0").Selected = True
                    'End If



                End While

            End Using
        Catch ex As Exception
            Panel_RevertBack.Visible = False
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EMP_ID") = ""
        End Try

    End Sub
    Sub BIND_VIEW_LEVEL(ByVal ID As String)
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString

        Dim DS As New DataSet
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EPR_ID", ID)

        DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[PDP_N].[GET_RIEVEWER_VIEW_LEVEL]", pParms)
        gvVieLevel.DataSource = DS.Tables(0)
        gvVieLevel.DataBind()
        DS.Clear()
    End Sub
    Protected Sub lbtnRevert_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EPR_ID") = HF_EPR_ID.Value
            ViewState("HF_EMP_ID") = ""
            ViewState("HF_EMP_ID") = HF_EMP_ID.Value
            Panel_RevertBack.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_RIEVEWERS_TOREVERT", pParms)
                rbtnList_Rievewer.DataSource = ds
                rbtnList_Rievewer.DataTextField = "EMP_NAME"
                rbtnList_Rievewer.DataValueField = "EMP_ID"
                rbtnList_Rievewer.DataBind()
                rbtnList_Rievewer.Items(0).Selected = True
                'rptPDP_Reveiwer.DataSource = ds
                'rptPDP_Reveiwer.DataBind()
            End Using
        Catch ex As Exception
            Panel_RevertBack.Visible = False
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EMP_ID") = ""
        End Try


    End Sub
    Protected Sub lbtnRevertPublish_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")

            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EPR_ID") = HF_EPR_ID.Value
            ViewState("HF_EMP_ID") = ""
            ViewState("HF_EMP_ID") = HF_EMP_ID.Value


            Panel_PublishRevertBack.Visible = True
            Using connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString)
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(1) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "PDP_N.GET_RIEVEWERS_TOREVERT_PUBLISHED", pParms)
                rbtnList_Rievewer_Publish.DataSource = ds
                rbtnList_Rievewer_Publish.DataTextField = "EMP_NAME"
                rbtnList_Rievewer_Publish.DataValueField = "EMP_ID"
                rbtnList_Rievewer_Publish.DataBind()
                rbtnList_Rievewer_Publish.Items(0).Selected = True
                'rptPDP_Reveiwer.DataSource = ds
                'rptPDP_Reveiwer.DataBind()
            End Using
        Catch ex As Exception
            Panel_PublishRevertBack.Visible = False
            ViewState("HF_EPR_ID") = ""
            ViewState("HF_EMP_ID") = ""

        End Try


    End Sub
    Protected Sub lbtnFinal_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            Dim HF_USR_EMP_ID As HiddenField = sender.parent.FindControl("HF_USR_EMP_ID")

            Dim redirectUrl As String
            If ViewState("Year") = 7 Then
                redirectUrl = "pdpFinal_V2.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)

            Else
                redirectUrl = "pdpFinal.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)

            End If
            

            'redirectUrl = "pdpFinalStaff.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)

            Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
            If url.Contains("?") Then
                url = url.Remove(url.IndexOf("?"))
            End If
            url = LCase(url)
            url = url.Replace("pdpdashboard.aspx", redirectUrl)
            url = url.Replace("http:", "https:")
            Dim sb As New System.Text.StringBuilder("")
            sb.Append(Convert.ToString("popUpDetails('" + url + "');"))
            ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", sb.ToString(), True)
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub lbtnInterim_Click(sender As Object, e As EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
            Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
            Dim HF_USR_EMP_ID As HiddenField = sender.parent.FindControl("HF_USR_EMP_ID")
            Dim HF_INTERIM_EDIT As HiddenField = sender.parent.FindControl("HF_INTERIM_EDIT")

            Dim redirectUrl As String

            If ViewState("Year") = 6 Then
                If HF_INTERIM_EDIT.Value = "1" Then
                    redirectUrl = "pdpInterim_Edit.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)
                Else
                    redirectUrl = "pdpInterim_V2.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)
                End If
            ElseIf ViewState("Year") = 7 Then
                If HF_INTERIM_EDIT.Value = "1" Then
                    redirectUrl = "pdpInterim_Edit_v2.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)
                Else
                    redirectUrl = "pdpInterim_V3.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)
                End If
            Else
                redirectUrl = "pdpInterim.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)
            End If

            'redirectUrl = "pdpFinalStaff.aspx?ER=" + Encr_decrData.Encrypt(HF_EPR_ID.Value) + "&U=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&MR=" + Encr_decrData.Encrypt(HF_USR_EMP_ID.Value)

            Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
            If url.Contains("?") Then
                url = url.Remove(url.IndexOf("?"))
            End If
            url = LCase(url)
            url = url.Replace("pdpdashboard.aspx", redirectUrl)
            url = url.Replace("http:", "https:")
            Dim sb As New System.Text.StringBuilder("")
            sb.Append(Convert.ToString("popUpDetails('" + url + "');"))
            ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", sb.ToString(), True)
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub btn_Revert_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Revert.Click
        '  Panel_RevertBack.Visible = False
        Dim lnkRev As LinkButton = sender.parent.FindControl("lbtnRevNew")


        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim redirectUrl As String

        redirectUrl = "pdpCStaff.aspx?ER=" + Encr_decrData.Encrypt(ViewState("HF_EPR_ID")) + "&U=" + Encr_decrData.Encrypt(ViewState("HF_EMP_ID")) + "&MR=" + Encr_decrData.Encrypt(rbtnList_Rievewer.SelectedValue)
        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If
        url = LCase(url)
        url = url.Replace("pdpdashboard.aspx", redirectUrl)
        url = url.Replace("http:", "https:")
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", rbtnList_Rievewer.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
            pParms(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks.InnerText.Trim())
            pParms(4) = New SqlClient.SqlParameter("@URL", url)


            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.REVERT_BACKTO_REVIEWER", pParms)



            sqltran.Commit()
            gridbind()
            'lblerrormsg.Text = "You have successfully saved the Performance Development Plan"
            Panel_RevertBack.Visible = False
        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub

    Protected Sub btn_SaveLevel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_SaveLevel.Click
        '  Panel_RevertBack.Visible = False
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")


        Try
            Dim pparms(5) As SqlClient.SqlParameter
            pparms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pparms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
            pparms(2) = New SqlClient.SqlParameter("@REV1", ddl_Level1.SelectedItem.Value)
            pparms(3) = New SqlClient.SqlParameter("@REV2", ddl_Level2.SelectedItem.Value)
            pparms(4) = New SqlClient.SqlParameter("@REV3", "")

            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.SAVE_SET_LEVELS", pparms)
            sqltran.Commit()
            gridbind()
            'lblerrormsg.Text = "You have successfully saved the Performance Development Plan"
            'Panel_SetLevel.Visible = False
        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Cancel.Click
        Panel_RevertBack.Visible = False

        rbtnList_Rievewer.ClearSelection()
        txt_Remarks.InnerText = ""
        ViewState("HF_EPR_ID") = ""
        ViewState("HF_EMP_ID") = ""
    End Sub

    Protected Sub btn_SaveLevel_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_SaveLevel_Cancel.Click
        Panel_SetLevel.Visible = False

        ViewState("HF_EPR_ID") = ""
        ViewState("HF_EMP_ID") = ""
    End Sub

    Protected Sub rptPdp_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptPdp.ItemDataBound
        ' Execute the following logic for Items and Alternating Items.
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim EmpStatus_CSS As HtmlGenericControl = e.Item.FindControl("EmpStatus_CSS")
            Dim EmpStatus_Value As HtmlGenericControl = e.Item.FindControl("EmpStatus_Value")
            Dim LEVEL1_COLOR As HtmlGenericControl = e.Item.FindControl("LEVEL1_COLOR")
            Dim LEVEL1_CHECKBOX As HtmlGenericControl = e.Item.FindControl("LEVEL1_CHECKBOX")
            Dim spSummary As HtmlGenericControl = e.Item.FindControl("spSummary")
            Dim spRating As HtmlGenericControl = e.Item.FindControl("spRating")

            Dim HF_EPR_ID As HiddenField = e.Item.FindControl("HF_EPR_ID")

            Dim hfSuper As HiddenField = e.Item.FindControl("hfSuper")
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@YEAR", ViewState("Year"))
            pParms(1) = New SqlClient.SqlParameter("@USR_EMP_ID", Session("EmployeeId"))
            pParms(2) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)
            ' Dim LVL_FINISH As String = "0"
            Dim NXT_USR_LVL_FINISH As String = "0"
            Dim USR_LVL_FINISH As String = "0"
            Dim PRV_USR_LVL_FINISH As String = "0"
            Dim INTERIM_COMP As String = "0"
            Dim bENABLESETLEVELS As Boolean
            Dim bENABLE_REVERTBACK_BUTTON As Boolean
            Dim MAIN_LVL As String = "0"
            Dim Interim_Css As String = ""
            Dim EPR_bPUBLISHED As String = "0"
            Dim bSHOW_MGR As String = "0"
            Dim REVERT_RATING_SHOW As String = "0"
            Dim PUBLISH_RATING_SHOW As String = "0"
            Dim PUBLISH_BUTTON_SHOW As String = "0"
            Dim INTERIM_EDIT_BUTTON_SHOW As String = "0"

            Using reader As SqlDataReader = SqlHelper.ExecuteReader(connStr, CommandType.StoredProcedure, "PDP_N.GET_PDP_LEVEL", pParms)
                While reader.Read()


                    MAIN_LVL = Convert.ToString(reader("LVL"))

                    EmpStatus_CSS.Attributes.Add("class", Convert.ToString(reader("EmpStatus_CSS")))
                    EmpStatus_Value.InnerHtml = Convert.ToString(reader("EmpStatus_Value"))


                    LEVEL1_COLOR.Attributes.Add("class", Convert.ToString(reader("LEVEL1_COLOR")))
                    LEVEL1_CHECKBOX.Attributes.Add("class", Convert.ToString(reader("LEVEL1_CHECKBOX")))

                    bENABLESETLEVELS = Convert.ToBoolean(reader("bENABLESETLEVELS"))
                    bENABLE_REVERTBACK_BUTTON = Convert.ToBoolean(reader("bENABLE_REVERTBACK_BUTTON"))
                    Interim_Css = Convert.ToString(reader("Interim_Css"))
                    EPR_bPUBLISHED = Convert.ToString(reader("EPR_bPUBLISHED"))
                    bSHOW_MGR = Convert.ToString(reader("bSHOW_MGR"))
                    REVERT_RATING_SHOW = Convert.ToString(reader("REVERT_RATING_SHOW"))
                    PUBLISH_RATING_SHOW = Convert.ToString(reader("PUBLISH_RATING_SHOW"))
                    PUBLISH_BUTTON_SHOW = Convert.ToString(reader("PUBLISH_BUTTON_SHOW"))
                    INTERIM_EDIT_BUTTON_SHOW = Convert.ToString(reader("INTERIM_EDIT_BUTTON"))
                End While
            End Using


            If PUBLISH_BUTTON_SHOW = "1" Then
                btnPublish.Visible = True
            End If


            If EPR_bPUBLISHED = "0" And bSHOW_MGR = "0" Then
                spSummary.InnerHtml = ""
                spRating.InnerHtml = ""
            End If


            If bENABLE_REVERTBACK_BUTTON = "1" Then

                CType(e.Item.FindControl("lbtnRevert"), LinkButton).Visible = True
            Else
                CType(e.Item.FindControl("lbtnRevert"), LinkButton).Visible = False
            End If



            If (bENABLESETLEVELS = "1") Then
                CType(e.Item.FindControl("lbtnSetLevels"), LinkButton).Visible = True
            Else
                CType(e.Item.FindControl("lbtnSetLevels"), LinkButton).Visible = False
            End If

            If (MAIN_LVL = "1") Then
                CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = True
                CType(e.Item.FindControl("lbtnInterim"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnFinal"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnRevPublish"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnPublish"), LinkButton).Visible = False
            End If

            If (MAIN_LVL = "2") Then
                CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnInterim"), LinkButton).Visible = True
                CType(e.Item.FindControl("lbtnInterim"), LinkButton).CssClass = Interim_Css
                CType(e.Item.FindControl("lbtnFinal"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnRevPublish"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnPublish"), LinkButton).Visible = False
            End If
            If (MAIN_LVL = "3") Then
                CType(e.Item.FindControl("lbtnRev"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnInterim"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnFinal"), LinkButton).Visible = True
                CType(e.Item.FindControl("lbtnRevPublish"), LinkButton).Visible = False
                CType(e.Item.FindControl("lbtnPublish"), LinkButton).Visible = False
            End If

            If PUBLISH_RATING_SHOW = "1" Then
                CType(e.Item.FindControl("lbtnPublish"), LinkButton).Visible = True
            End If
            If REVERT_RATING_SHOW = "1" Then
                CType(e.Item.FindControl("lbtnRevPublish"), LinkButton).Visible = True
            End If
            If INTERIM_EDIT_BUTTON_SHOW = "1" Then

                CType(e.Item.FindControl("lbtnInterim_Edit"), LinkButton).Visible = True
            Else
                CType(e.Item.FindControl("lbtnInterim_Edit"), LinkButton).Visible = False
            End If


            Dim lbtnPdf As LinkButton = e.Item.FindControl("btnDownloadPdf")
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbtnPdf)


        End If
    End Sub
    Protected Sub lnk_home_Click(sender As Object, e As EventArgs) Handles lnk_home.Click
        Response.Redirect("/ESSDashboard.aspx")
    End Sub
    Protected Sub btnDownloadPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
        Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
        hdnEpr.Value = HF_EPR_ID.Value
        ViewState("HF_EMP_ID") = HF_EMP_ID.Value
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", "buttonDownClick();", True)

    End Sub
    Protected Sub lbtnInterim_Edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_EMP_ID As HiddenField = sender.parent.FindControl("HF_EMP_ID")
        Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")
        Dim HF_USR_EMP_ID As HiddenField = sender.parent.FindControl("HF_USR_EMP_ID")

        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")



        Try
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", HF_EPR_ID.Value)



            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.GET_INTERIM_EDIT", pParms)

            sqltran.Commit()
            gridbind()

        Catch ex As Exception
            sqltran.Rollback()
        End Try



    End Sub
    Protected Sub btnPDF_D_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPDF_D.Click
        If hdnEpr.Value <> "" Then
            Try
                Dim EPR_ID As String = hdnEpr.Value
                Dim param As New Hashtable
                param.Add("@emp_id", ViewState("HF_EMP_ID"))
                param.Add("@pdpy_id", ViewState("Year"))
                param.Add("@usr_emp_id", Session("EmployeeId"))

                Dim rptClass As New rptClass
                With rptClass
                    .crDatabase = "oasis_pdp_v3"
                    .reportParameters = param
                    .reportPath = Server.MapPath("Reports/rptPDPDetailReport_New.rpt")
                End With



                Dim rptDownload As New ReportDownload
                rptDownload.LoadReports(rptClass, rs1)
                rptDownload = Nothing
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub lbtn_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim hf As HiddenField = sender.parent.FindControl("h_ID")
            Dim lnk As LinkButton = sender.parent.FindControl("lbtn")

            ViewState("Year") = hf.Value
            Session("PDP_YEAR") = hf.Value
            Session("PDP_YEAR_ord") = lnk.CommandArgument
            BIND_YEAR(lnk.CommandArgument)
            Bind_BSU()
            Bind_TYPE()
            gridbind()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub GETCURRENT_YEAR()
        Try




            Dim conn As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
            Dim DS As New DataSet
            Dim param(1) As SqlParameter

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "PDP_N.GET_CURRENT_YEAR", param)
            If DS.Tables(0).Rows.Count >= 1 Then
                ViewState("Year") = DS.Tables(0).Rows(0).Item("id")
                Session("PDP_YEAR") = DS.Tables(0).Rows(0).Item("id")
                Session("PDP_YEAR_ord") = DS.Tables(0).Rows(0).Item("ord")
                BIND_YEAR(DS.Tables(0).Rows(0).Item("ord"))
                Bind_BSU()
                Bind_TYPE()
                gridbind()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub GETCURRENT_YEAR_nEW()
        Try





            BIND_YEAR(Session("PDP_YEAR_ord"))

            ' gridbind()
            ' lbtn_Click(Nothing, Nothing)

        Catch ex As Exception

        End Try
    End Sub
    Private Sub BIND_YEAR(ByVal i As Integer)
        Try




            Dim conn As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
            Dim DS As New DataSet
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@id", i)
            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "PDP_N.BIND_YEAR", param)
            rptYear.DataSource = DS.Tables(0)
            rptYear.DataBind()

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub btn_Revert_Publish_Click(sender As Object, e As EventArgs) Handles btn_Revert_Publish.Click
        Dim lnkRev As LinkButton = sender.parent.FindControl("lbtnRevNew")


        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim redirectUrl As String

        redirectUrl = "pdpCStaff.aspx?ER=" + Encr_decrData.Encrypt(ViewState("HF_EPR_ID")) + "&U=" + Encr_decrData.Encrypt(ViewState("HF_EMP_ID")) + "&MR=" + Encr_decrData.Encrypt(rbtnList_Rievewer.SelectedValue)
        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        If url.Contains("?") Then
            url = url.Remove(url.IndexOf("?"))
        End If
        url = LCase(url)
        url = url.Replace("pdpdashboard.aspx", redirectUrl)
        url = url.Replace("http:", "https:")
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@REV_TO_EMP_ID", rbtnList_Rievewer_Publish.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
            pParms(3) = New SqlClient.SqlParameter("@REMARKS", txt_Remarks_Publish.InnerText.Trim())
            pParms(4) = New SqlClient.SqlParameter("@URL", url)


            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.REVERT_RATING", pParms)

            sqltran.Commit()
            gridbind()
            'lblerrormsg.Text = "You have successfully saved the Performance Development Plan"
            Panel_PublishRevertBack.Visible = False
        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub

    Protected Sub btn_Cancel_Publish_Click(sender As Object, e As EventArgs) Handles btn_Cancel_Publish.Click
        Panel_PublishRevertBack.Visible = False
        rbtnList_Rievewer_Publish.ClearSelection()
        txt_Remarks_Publish.InnerText = ""
        ViewState("HF_EPR_ID") = ""
        ViewState("HF_EMP_ID") = ""
    End Sub
    Private Sub PUBLISH_RATING(ByRef errorMessage As String, ByVal XML_OBJ As String, ByVal status As String, ByVal transaction As SqlTransaction)
        Dim ReturnFlag As Integer
        Dim PARAM(4) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))

        PARAM(1) = New SqlClient.SqlParameter("@STR_XML", "<RT_M>" + XML_OBJ + "</RT_M>")

        PARAM(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        PARAM(2).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "PDP_N.PUBLISH_RATING", PARAM)
        ReturnFlag = PARAM(2).Value
        If ReturnFlag <> 0 Then
            errorMessage = "-1"
        End If


    End Sub

    Protected Sub lbtnPublish_Click(sender As Object, e As EventArgs)
        Dim XML_INTRIM As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_V3Connection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                Dim STR_XML As New StringBuilder


                Dim HF_EPR_ID As HiddenField = sender.parent.FindControl("HF_EPR_ID")


                STR_XML.Append(String.Format("<RT EPR_ID='{0}' STATUS='{1}' />", HF_EPR_ID.Value, 1))

                XML_INTRIM = STR_XML.ToString()

                PUBLISH_RATING(ERRORMSG, XML_INTRIM, "", transaction)
                If ERRORMSG <> "-1" Then
                    transaction.Commit()
                    gridbind()

                End If

            Catch ex As Exception
                ERRORMSG = "-1"
                transaction.Rollback()





            End Try
        End Using

    End Sub
    Private Function CHECK_GET_RATING(ByRef errormsg As String) As String
        Dim STR_XML As New StringBuilder
        Dim hfEPR_ID As HiddenField
        Dim lbtnPublish As LinkButton

        For Each objItem As RepeaterItem In rptPdp.Items
            hfEPR_ID = DirectCast(objItem.FindControl("HF_EPR_ID"), HiddenField)
            lbtnPublish = DirectCast(objItem.FindControl("lbtnPublish"), LinkButton)


            If lbtnPublish.Visible = "True" Then
                STR_XML.Append(String.Format("<RT EPR_ID='{0}' STATUS='{1}' />", hfEPR_ID.Value, 1))
            End If
        Next


        Return STR_XML.ToString()
    End Function

    Protected Sub btnPublish_Click(sender As Object, e As EventArgs) Handles btnPublish.Click


        Dim XML_INTRIM As String = String.Empty
        Dim transaction As SqlTransaction
        Dim ERRORMSG As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASIS_PDP_V3Connection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try



                XML_INTRIM = CHECK_GET_RATING(ERRORMSG)

                PUBLISH_RATING(ERRORMSG, XML_INTRIM, "", transaction)
                If ERRORMSG <> "-1" Then
                    transaction.Commit()
                    gridbind()

                End If

            Catch ex As Exception
                ERRORMSG = "-1"
                transaction.Rollback()





            End Try
        End Using
    End Sub

    Protected Sub gvVieLevel_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvVieLevel.RowCommand
        If e.CommandName = "Delete" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvVieLevel.Rows(index), GridViewRow)

            Dim lblEPL_ID As Label

            lblEPL_ID = selectedRow.FindControl("lblEPL_ID")

            del_view_lvl(lblEPL_ID.Text)

        End If
    End Sub

    Sub del_view_lvl(ByVal id As String)
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")

        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@EPL_ID", id)


            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.DEL_RIEVEWER_VIEW_LEVEL", pParms)

            sqltran.Commit()
            BIND_VIEW_LEVEL(ViewState("HF_EPR_ID"))

        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub
    Sub ADD_view_lvl(ByVal id As String)
        Dim connection As String = ConfigurationManager.ConnectionStrings("OASIS_PDP_V3ConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")

        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
            pParms(1) = New SqlClient.SqlParameter("@EPR_ID", ViewState("HF_EPR_ID"))
            pParms(2) = New SqlClient.SqlParameter("@REV1", id)

            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "PDP_N.SAVE_SET_VIEW_LEVELS", pParms)

            sqltran.Commit()
            BIND_VIEW_LEVEL(ViewState("HF_EPR_ID"))

        Catch ex As Exception
            sqltran.Rollback()
        End Try
    End Sub
    Protected Sub gvVieLevel_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvVieLevel.RowDeleting

    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If ddl_Level2.SelectedValue <> "0" Then
            ADD_view_lvl(ddl_Level2.SelectedValue)
        End If
    End Sub
End Class

