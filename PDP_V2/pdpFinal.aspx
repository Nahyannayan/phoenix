﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="pdpFinal.aspx.vb" Inherits="PDP_V2_pdpFinal" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>GEMS Performance Development Plan</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
  
     <link href="../PDP_v2/Styles/pdpGStaff.css?v=1" rel="stylesheet" media="all" />
    <link href="../PDP_v2/Styles/bootstrap.css?id=ver2.4" rel="stylesheet" />

    <script type="text/javascript">
        window.onbeforeunload = function (e) {


            if (document.getElementById('<%= hd_keys.ClientID%>').value != 0) {
                  e = e || window.event;


                  if (e) {
                      e.returnValue = '(Changes you made may not be saved)';
                  }


                  return '(Changes you made may not be saved)';
              }
          };
        function ShowTabs(tab) {
            if (tab == 1) {
                $("#tb_ObjectivesCurrent").show();
                $("#tb_SmartObjective").hide();
            }
            else if (tab == 2) {
                $("#tb_ObjectivesCurrent").hide();
                $("#tb_SmartObjective").show();

            }
            return false;
        }

        function showHide() {

            $header = $(".header");
            $content = $(".contentPanel");
            $ColExpId = $("#ColExpCss");

            $content.slideToggle(500, function () {

                if ($content.is(":visible") == true) {

                    $ColExpId.removeClass("colClass").addClass("ExpClass");
                }
                else {

                    $ColExpId.removeClass("ExpClass").addClass("colClass");
                }

            });

        }




        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


        function check_values() {
            var ch = document.getElementById('<%=ddl_mobility.ClientID %>');
               var checkbox = ch.getElementsByTagName("input");

               for (var i = 0; i < checkbox.length; i++) {
                   if (checkbox[0].checked) {
                       checkbox[i].checked = false;
                       checkbox[0].checked = true;
                   }
               }
           }

        function process(e) {
            var code = document.getElementById('<%= hd_keys.ClientID%>').value;
             code = code + 1;
             document.getElementById('<%= hd_keys.ClientID%>').value = code;
  }

  function reset_val(e) {
      document.getElementById('<%= hd_keys.ClientID%>').value = 0;


        }
        function SetRating1(val) {
            //alert(val);
            //alert(spWMRating1.innerHTML);
            //alert(spNoRating1.innerText);
            //alert(spNoRating1);
            var x = document.getElementById('<%= HF_Rating1.ClientID%>');
    document.getElementById('<%= HF_Rating1.ClientID%>').value = val;
    x.value = val;
    spNoRating1.innerHTML = val;
    spWMRating1.innerHTML = '';
    $("#PopupMenu1").hide();
}
function SetRating2(val) {
    var x = document.getElementById('<%= HF_Rating2.ClientID%>');
             x.value = val;
             spNoRating2.innerHTML = val;
             spWMRating2.innerHTML = '';
             $("#PopupMenu2").hide();
         }
         function SetRating3(val) {
             var x = document.getElementById('<%= HF_Rating3.ClientID%>');
            x.value = val;
            spNoRating3.innerHTML = val;
            spWMRating3.innerHTML = '';
            $("#PopupMenu3").hide();
        }
        function SetRating4(val) {
            var x = document.getElementById('<%= HF_Rating4.ClientID%>');
            x.value = val;
            spNoRating4.innerHTML = val;
            spWMRating4.innerHTML = '';
            $("#PopupMenu4").hide();
        }
        function SetRating5(val) {
            var x = document.getElementById('<%= HF_Rating5.ClientID%>');
            x.value = val;
            spNoRating5.innerHTML = val;
            spWMRating5.innerHTML = '';
            $("#PopupMenu5").hide();
        }
        function SetRating6(val) {

            var x = document.getElementById('<%= HF_OverallRating.ClientID%>');
           var PopId = document.getElementById('<%= popupMenu6.ClientID%>');
           x.value = val;
           SpOverallRatingNo.innerHTML = val;
           SpOverallRatingWatermark.innerHTML = '';
           PopId.style.visibility = 'hidden';
           $("#PopupMenu6").hide();
       }
        function SetRating7(val) {
            var x = document.getElementById('<%= HF_Rating7.ClientID%>');
             x.value = val;
             spNoRating7.innerHTML = val;
             spWMRating7.innerHTML = '';
             $("#PopupMenu7").hide();
         }
        function SetRating8(val) {
            var x = document.getElementById('<%= HF_Rating8.ClientID%>');
            x.value = val;
            spNoRating8.innerHTML = val;
            spWMRating8.innerHTML = '';
            $("#PopupMenu8").hide();
        }
        function SetRating9(val) {
            var x = document.getElementById('<%= HF_Rating9.ClientID%>');
            x.value = val;
            spNoRating9.innerHTML = val;
            spWMRating9.innerHTML = '';
            $("#PopupMenu9").hide();
        }
        function SetRating10(val) {
            var x = document.getElementById('<%= HF_Rating10.ClientID%>');
            x.value = val;
            spNoRating10.innerHTML = val;
            spWMRating10.innerHTML = '';
            $("#PopupMenu10").hide();
        }
        function SetRating11(val) {
            var x = document.getElementById('<%= HF_Rating11.ClientID%>');
            x.value = val;
            spNoRating11.innerHTML = val;
            spWMRating11.innerHTML = '';
            $("#PopupMenu11").hide();
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
         <asp:HiddenField ID="hd_keys" runat="server" Value="0" />
        <div class="col-sm-12 col-md-12 col-lg-12">
            <img src="Images/GEMS-pdp-banner_2014_15.png" class="img-responsive" />
        </div>

        <section class="col-sm-12 col-md-12 col-lg-12">

            <div class="header" onclick="showHide();">

                <span id="ColExpCss" class="ExpClass"></span><span id="empName" runat="server" enableviewstate="true"></span>
                <span id="empDesig" runat="server" enableviewstate="true"></span>
                <span id="spRev" runat="server" class="PlanningPhaseCss">Final Phase</span>

            </div>
            <div class="contentPanel panelHeader panel-body">
                <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">Name:</span>
                        <input type="text" id="txt_EmpName" runat="server" class="form-control" readonly="true" />

                    </div>
                </div>
                <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">Designation:</span>
                        <input type="text" id="txt_role" runat="server" class="form-control" readonly="true" />
                    </div>
                </div>
                <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">Business:</span>
                        <input type="text" id="txt_bsu" runat="server" class="form-control" readonly="true" style="font-size: 11px;" />
                    </div>
                </div>
                <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">Reviewing Manager:</span>
                        <input type="text" id="txt_ManagerName" runat="server" class="form-control" readonly="true" />

                    </div>
                </div>
                <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">Review Period From:</span>
                        <input type="text" id="txt_FromDate" runat="server" class="form-control" readonly="true" />
                    </div>
                </div>
                <div class="form-group col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">Review Period To:</span>

                        <input type="text" id="txt_ToDate" runat="server" class="form-control" readonly="true" />
                    </div>
                </div>
            </div>


        </section>


        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <section class="col-md-10 col-lg-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
                    <div class="row">
                        <asp:LinkButton ID="lbtnStep1" runat="server" CssClass="">
                 <div class="cssStepNo" >1</div>
                 <div class="cssStepContainer">           
                           <div class="cssStep">STEP ONE</div>   <div class="cssStepInfo" id="">SMART Objectives (Sep 2016-Aug 2017)</div>     
                 </div>
                    
                
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbtnStep2" runat="server" CssClass="">
                 <div class="cssStepNo" >2</div>
                 <div class="cssStepContainer">           
                           <div class="cssStep">STEP TWO</div>   <div class="cssStepInfo">Career Aspirations</div>     
                 </div>
                    
                
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbtnStep3" runat="server" CssClass="">
                 <div class="cssStepNo" >3</div>
                 <div class="cssStepContainer">           
                           <div class="cssStep">STEP THREE</div>   <div class="cssStepInfo">Development Goals</div>     
                 </div>
                    
                
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbtnStep4" runat="server" CssClass="" Enabled="false" Visible="false">
                 <div class="cssStepNo" >4</div>
                 <div class="cssStepContainer">           
                           <div class="cssStep">STEP FOUR</div>   <div class="cssStepInfo"> &nbsp;</div>     
                 </div>
                    
                
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbtnStep5" runat="server" CssClass="" >
                 <div class="cssStepNo" >4</div>
                 <div class="cssStepContainer">           
                           <div class="cssStep">STEP FOUR</div>   <div class="cssStepInfo">Final Review</div>     
                 </div>
                    
                
                        </asp:LinkButton>

                    </div>
                </section>

                <section class="col-md-12 col-lg-12 col-sm-12 colContainerBox">

                    <div id="divNote" runat="server" title="Click on the message box to drag it up and down" visible="false" clientidmode="Static">
                        <span class="msgInfoclose"></span>

                        <asp:Label ID="lblError" runat="server" EnableViewState="false"></asp:Label>

                    </div>

                    <div runat="server" id="tb_ObjectivesCurrent" class="table-responsive" style="width: 100%;" clientidmode="Static">

                        <div class="wellCustom  col-md-12 col-lg-12 col-sm-12 wellBorder">
                            <font color="#F77825">* SMART</font>- <font color="#F77825" size="3px">S</font>pecific, <font color="#F77825" size="3px">M</font>easurable,
         <font color="#F77825" size="3px">A</font>ligned, <font color="#F77825" size="3px">R</font>ealistic and <font color="#F77825" size="3px">T</font>imely
                             
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="smallBox ontarget"></div> &nbsp; On Target &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="smallBox GoalChange"></div> &nbsp; Goal Change &nbsp;&nbsp;&nbsp;
                            <div class="smallBox AtRisk"></div> &nbsp; At Risk 
                   
                  
      <span class="wellMandatory" id="spMan" runat="server"></span>
                        </div>

                      

                        <section class="col-md-2 col-lg-2 col-sm-2">
                            <div class="panel panel-success">
                                <div class="panel-heading" runat="server">
                                    Objective
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                    <div class="form-group">


                                        <div class="form-group">
                                            <textarea class="form-control" rows="4" id="txt_OBJ1" runat="server" placeholder="1. Enter your objective" draggable="false"
                                                style="min-height: 65px !important;"
                                                enableviewstate="true"></textarea>
                                            <textarea class="form-control" rows="4" id="txt_OBJ6" runat="server" placeholder="6. Enter your objective" draggable="false" style="min-height: 65px !important;"
                                                enableviewstate="true" visible="false"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="4" id="txt_OBJ2" runat="server" placeholder="2. Enter your objective" draggable="false" style="min-height: 65px !important;"
                                                enableviewstate="true"></textarea>
                                            <textarea class="form-control" rows="4" id="txt_OBJ7" runat="server" placeholder="7. Enter your objective" draggable="false" style="min-height: 65px !important;"
                                                enableviewstate="true" visible="false"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="4" id="txt_OBJ3" runat="server" placeholder="3. Enter your objective" draggable="false" style="min-height: 65px !important;"
                                                enableviewstate="true"></textarea>
                                            <textarea class="form-control" rows="4" id="txt_OBJ8" runat="server" placeholder="8. Enter your objective" draggable="false" style="min-height: 65px !important;"
                                                enableviewstate="true" visible="false"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="4" id="txt_OBJ4" runat="server" placeholder="4. Enter your objective" draggable="false"
                                                style="min-height: 65px !important;"
                                                enableviewstate="true"></textarea>
                                            <textarea class="form-control" rows="4" id="txt_OBJ9" runat="server" placeholder="9. Enter your objective" draggable="false" style="min-height: 65px !important;"
                                                enableviewstate="true" visible="false"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="4" id="txt_OBJ5" runat="server" placeholder="5. Enter your objective" draggable="false" style="min-height: 65px !important;"
                                                enableviewstate="true"></textarea>
                                            <textarea class="form-control" rows="4" id="txt_OBJ10" runat="server" placeholder="10. Enter your objective" draggable="false" style="min-height: 65px !important;"
                                                enableviewstate="true" visible="false"></textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>




                        <section class="col-md-3 col-lg-3 col-sm-3">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Key Performance Indicator(Measures) 
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>

                                    <div class="form-group">
                                        <div class="input-group"><asp:linkbutton class="input-group-addon ontarget" id="spAch1" runat="server"><img src="Images/popout.gif"></asp:linkbutton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="1. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI1" runat="server"></textarea>
                                             <asp:LinkButton class="input-group-addon AtRisk" id="spAch6" runat="server"  visible="false"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="6. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI6" runat="server" visible="false"></textarea>
                                    </div>
                                        </div>


                                    <div class="form-group">
                                        <div class="input-group"><asp:LinkButton class="input-group-addon ontarget" id="spAch2" runat="server"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="2. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI2" runat="server"></textarea>
                                             <asp:LinkButton class="input-group-addon ontarget" id="spAch7" runat="server" visible="false"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="7. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI7" runat="server" visible="false"></textarea>
</div>
                                    </div>
                                    <div class="form-group">
                                         <div class="input-group"><asp:LinkButton class="input-group-addon ontarget" id="spAch3" runat="server"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="3. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI3" runat="server"></textarea>
                                              <asp:LinkButton class="input-group-addon ontarget" id="spAch8" runat="server" visible="false"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="8. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI8" runat="server" visible="false"></textarea>
</div>
                                    </div>
                                    <div class="form-group">
                                         <div class="input-group"><asp:LinkButton class="input-group-addon ontarget" id="spAch4" runat="server"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="4. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI4" runat="server"></textarea>
                                              <asp:LinkButton class="input-group-addon ontarget" id="spAch9" runat="server" visible="false"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="9. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI9" runat="server" visible="false"></textarea>
                                    </div>
                                        </div>
                                    <div class="form-group">
                                         <div class="input-group"><asp:LinkButton class="input-group-addon ontarget" id="spAch5" runat="server"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="5. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI5" runat="server"></textarea>
                                              <asp:LinkButton class="input-group-addon ontarget" id="spAch10" runat="server" visible="false"><img src="Images/popout.gif"></asp:LinkButton>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="10. Enter  Key Performance Indicator" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_KPI10" runat="server" visible="false"></textarea>
                                    </div>
                                        </div>
                                </div>
                            </div>
                        </section>
                        
                      <%-- Interim Review Comment--%>
                            <div class="panel panel-success" style="display:none;">
                                <div class="panel-heading">
                                    Interim Review Comment
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>

                                    <div class="form-group"> 
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="1. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV1" runat="server"></textarea>
                                       
                                       
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="6. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV6" runat="server" visible="false"></textarea>
                                   
                                        </div>


                                    <div class="form-group">
                                        
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="2. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV2" runat="server"></textarea>
                                           
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="7. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV7" runat="server" visible="false"></textarea>
</div>
                                    
                                    <div class="form-group">
                                       
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="3. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV3" runat="server"></textarea>
                                           
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="8. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV8" runat="server" visible="false"></textarea>
                                            </div>
                                    
                                    <div class="form-group">
                                       
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="4. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV4" runat="server"></textarea>
                                           
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="9. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV9" runat="server" visible="false"></textarea>
                                    </div>
                                        
                                    <div class="form-group">
                                       
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="5. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV5" runat="server"></textarea>
                                           
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="10. Enter  Interim Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_REV10" runat="server" visible="false"></textarea>
                                    </div>
                                        
                                </div>
                            </div>
                       

                         <section class="col-md-3 col-lg-3 col-sm-3">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                   Final Review Comment
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>

                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="1. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV1" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="6. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV6" runat="server" visible="false" onkeypress="process(event, this)"></textarea>
                                    </div>


                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="2. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV2" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="7. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV7" runat="server" visible="false" onkeypress="process(event, this)"></textarea>

                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="3. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV3" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="8. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV8" runat="server" visible="false" onkeypress="process(event, this)"></textarea>

                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="4. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV4" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="9. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV9" runat="server" visible="false" onkeypress="process(event, this)"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="5. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV5" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="10. Enter  Final Review Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_REV10" runat="server" visible="false" onkeypress="process(event, this)"></textarea>
                                    </div>
                                </div>
                            </div>
                        </section>
                           <section class="col-md-3 col-lg-3 col-sm-3">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                   Line Manager Comment
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>

                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="1. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR1" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="6. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR6" runat="server" visible="false" onkeypress="process(event, this)"></textarea>
                                    </div>


                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="2. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR2" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="7. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR7" runat="server" visible="false" onkeypress="process(event, this)"></textarea>

                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="3. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR3" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="8. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR8" runat="server" visible="false" onkeypress="process(event, this)"></textarea>

                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="4. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR4" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="9. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR9" runat="server" visible="false" onkeypress="process(event, this)"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="5. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR5" runat="server" onkeypress="process(event, this)"></textarea>
                                        <textarea class="form-control" rows="4" draggable="false" placeholder="10. Enter  Line Manager Comment" style="min-height: 65px !important;" enableviewstate="true"
                                            id="txt_FINAL_MGR10" runat="server" visible="false" onkeypress="process(event, this)"></textarea>
                                    </div>
                                </div>
                            </div>
                        </section>

                         <section class="col-md-1 col-lg-1 col-sm-1">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                              Rating
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                 <div class="form-group">
                                    <div id="divRating1" runat="server" style="margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating1" runat="server">1.Rating</span><div class="NoRating" id="spNoRating1" visible="True" runat="server"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating1" runat="server" />
                                    </div>
 <div id="divRating7" runat="server" style="display:none; margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating7" runat="server">6.Rating</span><div class="NoRating" id="spNoRating7" visible="True" runat="server"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating7" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="divRating2" runat="server" style="margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating2" runat="server">2.Rating</span><div class="NoRating" id="spNoRating2" runat="server" visible="True"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating2" runat="server" />
                                    </div>
                                     <div id="divRating8" runat="server" style="display:none; margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating8" runat="server">7.Rating</span><div class="NoRating" id="spNoRating8" visible="True" runat="server"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating8" runat="server" />
                                    </div>
                                </div>
                               <div class="form-group">
                                    <div id="divRating3" runat="server" style="margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating3" runat="server">3.Rating</span><div class="NoRating" id="spNoRating3" runat="server" visible="True"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating3" runat="server" />
                                    </div>
                                    <div id="divRating9" runat="server" style="display:none; margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating9" runat="server">8.Rating</span><div class="NoRating" id="spNoRating9" visible="True" runat="server"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating9" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="divRating4" runat="server" style="margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating4" runat="server" visible="true">4.Rating</span><div class="NoRating" id="spNoRating4" runat="server"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating4" runat="server" />
                                    </div>
                                    <div id="divRating10" runat="server" style="display:none; margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating10" runat="server">9.Rating</span><div class="NoRating" id="spNoRating10" visible="True" runat="server"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating10" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="divRating5" runat="server" style="margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating5" runat="server">5.Rating</span><div class="NoRating" id="spNoRating5" runat="server" visible="True"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating5" runat="server" />
                                    </div>
                                     <div id="divRating11" runat="server" style="display:none; margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="spWMRating11" runat="server">10.Rating</span><div class="NoRating" id="spNoRating11" visible="True" runat="server"></div>
                                        <textarea class="form-control" rows="3" draggable="false" style="padding-top: 20px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_Rating11" runat="server" />
                                    </div>
                                </div>



                            </div>
                        </div>
                    </section>

                        <div class="panel panel-success" id="divWtg" runat="server">
                            <div class="panel-heading">
                                Weightage
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>

                                <div class="form-group">
                                    <div class="input-group" id="divWTG1" runat="server" visible="true">
                                        <input type="text" id="txtWTG1" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <div class="input-group" id="divWTG6" runat="server" visible="false">
                                        <input type="text" id="txtWTG6" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <textarea class="form-control" rows="2" draggable="false" style="min-height: 52px !important; visibility: hidden;" enableviewstate="true"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="input-group" id="divWTG2" runat="server" visible="true">
                                        <input type="text" id="txtWTG2" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />

                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <div class="input-group" id="divWTG7" runat="server" visible="false">
                                        <input type="text" id="txtWTG7" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <textarea class="form-control" rows="2" draggable="false" style="min-height: 52px !important; visibility: hidden;" enableviewstate="true"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="input-group" id="divWTG3" runat="server" visible="true">
                                        <input type="text" id="txtWTG3" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <div class="input-group" id="divWTG8" runat="server" visible="false">
                                        <input type="text" id="txtWTG8" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <textarea class="form-control" rows="2" draggable="false" style="min-height: 52px !important; visibility: hidden;" enableviewstate="true"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="input-group" id="divWTG4" runat="server" visible="true">

                                        <input type="text" id="txtWTG4" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <div class="input-group" id="divWTG9" runat="server" visible="false">

                                        <input type="text" id="txtWTG9" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <textarea class="form-control" rows="2" draggable="false" style="min-height: 52px !important; visibility: hidden;" enableviewstate="true"></textarea>
                                </div>


                                <div class="form-group">
                                    <div class="input-group" id="divWTG5" runat="server" visible="true">
                                        <input type="text" id="txtWTG5" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <div class="input-group" id="divWTG10" runat="server" visible="false">
                                        <input type="text" id="txtWTG10" runat="server" class="form-control_Inter" clientidmode="Static"
                                            maxlength="3" onkeypress="return isNumberKey(event)" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <textarea class="form-control" rows="2" draggable="false" style="min-height: 52px !important; visibility: hidden;" enableviewstate="true"></textarea>
                                </div>

                            </div>
                        </div>
                        <%--     </section>--%>



                        <div class="wellCustomPage col-md-12 col-lg-12 col-sm-12 wellBorder">
                            <span class="PageNo" id="spPage" runat="server">Page <font color="#58B0E7">1</font>of 2</span><div class="page">
                                <ul class="pagination pagination1 pagination2">
                                    <li><a href="#" class="current" id="hf1" runat="server">1</a></li>
                                    <li><a href="#" id="hf2" runat="server">2</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div runat="server" id="tb_CareerAspirations">
                        <%--              <div class="wellCustom  col-md-12 col-lg-12 col-sm-12 wellBorder" > 
        <font color="#F77825">GEMS Education Values - To be rated in the future    </font> <div class="RatingBox">Rating Box</div>
    </div>--%>

                        <section class="col-md-5 col-lg-5 col-sm-5">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Career Aspirations
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="8" id="txt_CarrierInput" runat="server" placeholder="Enter your Career Aspirations" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true"></textarea>
                                    </div>
                                      <div class="form-group">
                                    <textarea class="form-control" rows="8" id="txt_carrierInputComment" runat="server" placeholder="Career Aspirations comments" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"></textarea>
                                </div>
                                </div>
                            </div>
                        </section>

                        <section class="col-md-5 col-lg-5 col-sm-5">
                            <div class="panel   panel-success">
                                <div class="panel-heading">
                                    Timeframe
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="8" id="txt_ProfDevelNeeds" runat="server" placeholder="Mention the time frame" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true"></textarea>
                                    </div>
                                        <div class="form-group">
                                    <textarea class="form-control" rows="8" id="txt_CarrierInputMgrComment" runat="server" placeholder="Manager Comments" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true" onkeypress="process(event, this)"></textarea>
                                </div>
                                </div>
                            </div>
                        </section>

                        <section class="col-md-2 col-lg-2 col-sm-2">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Mobility
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                    <div class="form-group">
                                        <div class="divCheckListBoxcss">
                                            <asp:CheckBoxList ID="ddl_mobility" runat="server" onClick="check_values();"
                                                RepeatDirection="Vertical" RepeatLayout="Flow" CssClass="checklistcss">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>




                    </div>




                    <div runat="server" id="tb_developmentGoals">
                        <div class="wellCustom  col-md-12 col-lg-12 col-sm-12 wellBorder">
                            <font color="#F77825" size="3px">70%</font>&nbsp;on the job, <font color="#F77825" size="3px">20%</font>&nbsp;coaching/mentoring & <font color="#F77825" size="3px">10%</font>&nbsp;instruction based learning
                        </div>

                        <section class="col-md-3 col-lg-3 col-sm-3">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Development Goals <span class="headerdateWhite">(Sep 2015-Aug 2016)</span>
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txt_developGoals1" runat="server" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true" placeholder="1. Enter your Development Goal"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txt_developGoals2" runat="server" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true" placeholder="2. Enter your Development Goal"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txt_developGoals3" runat="server" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true" placeholder="3. Enter your Development Goal"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txt_developGoals4" runat="server" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true" placeholder="4. Enter your Development Goal"></textarea>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="col-md-5 col-lg-5 col-sm-5">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    Measure of Achievement/Expected Outcome <span class="headerdateWhite">(Sep 2016-Aug 2017)</span>
                                </div>
                                <div class="panel-body">
                                    <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txt_ExpectedOutcome1" runat="server" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true" placeholder="1. Enter Measure of Achievement/Expected Outcome"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txt_ExpectedOutcome2" runat="server" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true" placeholder="2. Enter Measure of Achievement/Expected Outcome"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txt_ExpectedOutcome3" runat="server" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true" placeholder="3. Enter Measure of Achievement/Expected Outcome"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" id="txt_ExpectedOutcome4" runat="server" draggable="false" style="min-height: 50px !important;"
                                            enableviewstate="true" placeholder="4. Enter Measure of Achievement/Expected Outcome"></textarea>
                                    </div>
                                </div>
                            </div>
                        </section>

                         <section class="col-md-2 col-lg-2 col-sm-2">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                               Employee Remarks
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="txtGoalRemarks1" runat="server" placeholder="1. Enter Employee remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"  onkeypress="process(event, this)"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="txtGoalRemarks2" runat="server" placeholder="2. Enter Employee remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"  onkeypress="process(event, this)"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="txtGoalRemarks3" runat="server" placeholder="3. Enter Employee remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"  onkeypress="process(event, this)"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="txtGoalRemarks4" runat="server" placeholder="4. Enter Employee remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true" onkeypress="process(event, this)"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>
                        <section class="col-md-2 col-lg-2 col-sm-2">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                               Line Manager Remarks
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="txtMgrGoalRemarks1" runat="server" placeholder="1. Enter  Manager remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"  onkeypress="process(event, this)"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="txtMgrGoalRemarks2" runat="server" placeholder="2. Enter  Manager remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"  onkeypress="process(event, this)"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="txtMgrGoalRemarks3" runat="server" placeholder="3. Enter  Manager remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"  onkeypress="process(event, this)"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="txtMgrGoalRemarks4" runat="server" placeholder="4. Enter  Manager remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true" onkeypress="process(event, this)"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>
                    </div>
                      <div runat="server" id="tb_summary">
                    <div class="wellCustom  col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                        &nbsp;
                    </div>


                    <section class="col-md-offset-1 col-lg-offset-1 col-sm-offset-1 col-md-4 col-lg-4 col-sm-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Overall Summary
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="8" id="txt_mgrOverallSummary" runat="server" placeholder="To be entered by Line Managers" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true" onkeypress="process(event, this)"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>
             
                    <section class="col-md-2 col-lg-2 col-sm-2 ">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Overall Rating 
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                <div class="form-group">
                                    <div id="divOverallRating" runat="server" style="margin: 0px; border: 1px solid #ccc; -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); text-align: left; font-style: normal;">
                                        <span class="RatingWatermark" id="SpOverallRatingWatermark" runat="server">Select Overall Rating</span>
                                        <div class="NoRatingBig" id="SpOverallRatingNo" runat="server" visible="True"></div>
                                        <textarea class="form-control" rows="7" draggable="false" style="padding-top: 19px; visibility: hidden;" enableviewstate="true"></textarea>
                                        <asp:HiddenField ID="HF_OverallRating" runat="server" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                     <section class="col-md-4 col-lg-4 col-sm-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                              Employee  Overall Summary
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 10px; width: 100%; height: 10px;"></div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="8" id="txt_EmpOverallSummary" runat="server" placeholder="To be entered by Employee after Line Manager completes the rating" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true" readonly="readonly" onkeypress="process(event, this)"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>

                    <div class="row"></div>
                    <section>
                        <div class="row">
                            <div class="form-groupcol-md-offset-1 col-lg-offset-1 col-sm-offset-1 col-md-5 col-lg-5 col-sm-5">

                                <div class="input-group">
                                    <span class="input-group-addon label-primary">Employee Signed:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                     <input type="text" id="txtEmpSigned" runat="server" class="form-control" readonly="true"/>
                                  
                                </div>
                            </div>
                            <div class="form-group col-md-2 col-lg-2 col-sm-2">
                                <div class="input-group">
                                    <span class="input-group-addon label-primary">Date:</span>
                                   
                                     <input type="text" id="txtEmpDate" runat="server" class="form-control" readonly="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-offset-1 col-lg-offset-1 col-sm-offset-1 col-md-5 col-lg-5 col-sm-5">

                                <div class="input-group">
                                    <span class="input-group-addon label-primary">Manager Signed:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
  <input type="text" id="txtMgrSigned1" runat="server" class="form-control" readonly="true"/>
                                

                                </div>


                            </div>
                            <div class="form-group col-md-2 col-lg-2 col-sm-2">
                                <div class="input-group">
                                    <span class="input-group-addon label-primary">Date:</span>
                                 
                                     <input type="text" id="txtMgrDate1" runat="server" class="form-control" readonly="true"/>
                                </div>
                            </div>
                        </div>
                       


                    </section>

                </div>


                </section>






                <div class="form-group col-md-12 col-lg-12 col-sm-12">
                    <span id="divMandatory" runat="server" class="MandatoryCss" enableviewstate="false"></span>
                    <div class="pull-right">
                        <asp:LinkButton ID="btn_Previous" runat="server" CssClass="buttonNext" Width="133px" Height="27px" Visible="false" OnClientClick="reset_val(event, this)"> Previous
                        </asp:LinkButton>
                        <asp:LinkButton ID="btn_SaveDraft" runat="server" CssClass="buttonDraft" Width="133px" Height="27px" OnClientClick="reset_val(event, this)"> Save As Draft
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbtnRevert" runat="server" CssClass="buttonRevert" Height="27px" OnClick="lbtnRevert_Click"
                                                   
                                                    Text="Revert Back" ></asp:LinkButton>
                        
                        <asp:LinkButton ID="btn_Next" runat="server" CssClass="buttonNext" Width="133px" Height="27px" OnClientClick="reset_val(event, this)"> Save & Next
                        </asp:LinkButton>
                           
                        <asp:LinkButton ID="btn_SaveFinish" runat="server" CssClass="buttonFinish" Width="133px" Height="27px" Visible="false" OnClientClick="reset_val(event, this)"> Save & Finish
                        </asp:LinkButton>
                         <asp:LinkButton ID="btn_Submit" runat="server" CssClass="buttonFinish" Width="133px" Height="27px" Visible="false" OnClientClick="reset_val(event, this)"> Submit
                        </asp:LinkButton>
                        <ajaxToolkit:ConfirmButtonExtender ID="cbeConfirmPDP" runat="server"
                            TargetControlID="btn_SaveFinish"
                            ConfirmText="Are you sure you want to submit the GEMS Performance Development Plan form to the next reviewer?" />



                    </div>

                </div>
                <section class="col-md-12 col-lg-12 col-sm-12">
                    <div class="footerStyle">
                        <center> <span  class="input-group"><img src="favicon.ico" style="height:15px;width:15px; vertical-align:top;" ></img> &nbsp; Powered by GEMS OASIS &nbsp; <img src="favicon.ico" style="height:15px;width:15px; vertical-align:top;"></img></span></center>
                    </div>
                </section>

                <asp:HiddenField ID="hfCurrTabid" runat="Server" />

                <div id="Panel_RevertBack" runat="server" class="darkPanelM" visible="false">
                    <div class="darkPanelMTop">
                        <div class="holderInner" style="overflow:scroll" >

                            <table style="width: 100%; height: 100%;">
                                <tr>
                                    <td valign="top" style="padding-left:20px;" >
                                        <asp:Label ID="lblerrormsg" runat="server" EnableViewState="false" Text="Select  Employee">
                                        </asp:Label>
                                        
                                    </td>
                                    </tr>
                                <tr>
                                    <td valign="top" style="padding-left:20px;"> 
                                        <asp:RadioButtonList ID="rbtnList_Rievewer" runat="server">
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="rfv_Rievewer" runat="server" ErrorMessage="Select the Rievewer" ControlToValidate="rbtnList_Rievewer" ValidationGroup="SaveRevertBack"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>                                    
                                    <td width="80%"> <textarea class="form-control" rows="3" id="txt_Remarks" runat="server" placeholder="Remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"></textarea>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Remarks Required" ControlToValidate="txt_Remarks" ValidationGroup="SaveRevertBack"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btn_Revert" runat="server" Text="Revert" CssClass="buttonSave" Height="26px" Width="80px" ValidationGroup="SaveRevertBack"  CausesValidation="true" />
                                         <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server"
                            TargetControlID="btn_Revert" 
                            ConfirmText="Are you sure you want to revert the PDP form to the selected employee?" />
                                          <asp:Button ID="btn_Cancel" runat="server" Text="Cancel"  Height="26px" Width="80px" ToolTip="Click here to cancel and close"  CssClass="buttonCancel" />
                                   <asp:Repeater ID="rptPDP_Reveiwer" runat="server">
                            <HeaderTemplate>
                                <table style="width: 100%;" border="0" class="table">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="tbldbImgCss">
                                        <asp:HiddenField ID="hfID" runat="server" Value='<%# Eval("EMP_ID")%>' />
                                        <img src='<%# Eval("EMD_PHOTO")%>' class="img-thumbnail img-responsive" alt='<%# Eval("EMP_NAME")%>' style="min-height: 120px; height: 120px; width: 110px; min-width: 110px;" />                                                                     
                                    </td>
                                    <td class="tbldbContentcss">
                                        <div class="divDbHeadercss">
                                            <span class="spDbEnamecss"><%# Eval("EMP_NAME")%></span>
                                            <span class="spDbDesign"><%# Eval("DES_DESCR")%></span><span class="spDbDept"><%# Eval("DPT_DESCR")%></span>
                                           
                                        </div>                                                                     
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater> 
<asp:HiddenField ID="hdnEpr" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="darkPanelFooter">
                            <span class="TitlePl">Revert Back</span>
                        </div>
                    </div>
                </div>
                <div id="myModal1" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" onclick="HideModal()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">Confirmation</h4>
                            </div>
                            <div class="modal-body">

                                <section class="col-md-12 col-lg-12 col-sm-12">
                                </section>
                            </div>

                            <div class="modal-footer">
                                <asp:Button ID="btnSaveChanges" runat="server"
                                    CssClass="buttonFinish" Text="Save changes" Width="136px" Height="27px" />
                                <div id="divlogNote" runat="server" class="divinfoInner">
                                    <div id="divErrorLog" runat="server"></div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>

  <div id="myModal" class="modal fade" data-backdrop="static" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" >
                    <button type="button" class="close" data-dismiss="modal" onclick="HideModal()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Interim Review Comment</h4>
                </div>
                <div class="modal-body">
                   <section class="col-md-3 col-lg-3 col-sm-3">    
                   
                        <div class="panel panel-info">
                            <div class="panel-heading">
                               Objective
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 5px; width: 100%; height: 5px;"></div>
                                <div class="form-group"><textarea class="form-control" rows="6" draggable="false" style="min-height: 65px !important;"  enableviewstate="true"
                                             id="txt_rev_obj" runat="server" readonly="readonly" ClientIDMode="Static"></textarea>
                                    </div>
                                   
                                </div></div></section>
                      <section class="col-md-4 col-lg-4 col-sm-4">    
                   
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Key Performance Indicator(Measures) 
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 5px; width: 100%; height: 5px;"></div>
                                <div class="form-group"><textarea class="form-control" rows="6" draggable="false" style="min-height: 65px !important;"  enableviewstate="true"
                                             id="txt_rev_KPI" readonly="readonly" runat="server" ClientIDMode="Static"></textarea>
                                    </div>
                                   
                                </div></div></section>
                     <section class="col-md-2 col-lg-2 col-sm-2">    
                   
                        <div class="panel panel-info">
                            <div class="panel-heading">
                               Interim review Status 
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 5px; width: 100%; height: 5px;"></div>
                                <div class="form-group"><textarea class="form-control" rows="6" draggable="false" style="min-height: 65px !important;"  enableviewstate="true"
                                             id="txt_rev_status" readonly="readonly" runat="server" ClientIDMode="Static"></textarea>
                                    </div>
                                   
                                </div></div></section>
                    <section class="col-md-3 col-lg-3 col-sm-3">    
                    
                        <div class="panel panel-success">
                            <div class="panel-heading">
                               Comments
                            </div>
                            <div class="panel-body">
                                <div style="padding-top: 5px; width: 100%; height: 5px;"></div>
                                <div class="form-group"><textarea class="form-control" rows="6" draggable="false" style="min-height: 65px !important;"  enableviewstate="true"
                                             id="txt_rev_cmt" readonly="readonly" runat="server" ClientIDMode="Static"></textarea>
                                    </div>
                                   
                                </div></div></section>
</div>
                
                    <div class="modal-footer"> 
                   

                </div>

        
    </div>
            </div></div>
                  <ajaxToolkit:HoverMenuExtender ID="hmeRating1" runat="Server"
                    TargetControlID="divRating1"
                    PopupControlID="PopupMenu1"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
                <ajaxToolkit:HoverMenuExtender ID="hmeRating2" runat="Server"
                    TargetControlID="divRating2"
                    PopupControlID="PopupMenu2"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
                <ajaxToolkit:HoverMenuExtender ID="hmeRating3" runat="Server"
                    TargetControlID="divRating3"
                    PopupControlID="PopupMenu3"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
                <ajaxToolkit:HoverMenuExtender ID="hmeRating4" runat="Server"
                    TargetControlID="divRating4"
                    PopupControlID="PopupMenu4"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
                <ajaxToolkit:HoverMenuExtender ID="hmeRating5" runat="Server"
                    TargetControlID="divRating5"
                    PopupControlID="PopupMenu5"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
                <ajaxToolkit:HoverMenuExtender ID="hmeRating7" runat="Server"
                    TargetControlID="divRating7"
                    PopupControlID="PopupMenu7"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
                 <ajaxToolkit:HoverMenuExtender ID="hmeRating8" runat="Server"
                    TargetControlID="divRating8"
                    PopupControlID="PopupMenu8"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
                 <ajaxToolkit:HoverMenuExtender ID="hmeRating9" runat="Server"
                    TargetControlID="divRating9"
                    PopupControlID="PopupMenu9"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
 <ajaxToolkit:HoverMenuExtender ID="hmeRating10" runat="Server"
                    TargetControlID="divRating10"
                    PopupControlID="PopupMenu10"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />
 <ajaxToolkit:HoverMenuExtender ID="hmeRating11" runat="Server"
                    TargetControlID="divRating11"
                    PopupControlID="PopupMenu11"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="-48"
                    OffsetY="25"
                    PopDelay="50" />

             <ajaxToolkit:HoverMenuExtender ID="hmeOverall" runat="Server"
                    TargetControlID="divOverallRating"
                    PopupControlID="PopupMenu6"
                    PopupPosition="Center" HoverCssClass="HoverCSS"
                    OffsetX="28"
                    OffsetY="25"
                    PopDelay="50" />

                <asp:Panel ID="PopupMenu1" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating1('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating1('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating1('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating1('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating1('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating1('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>

                <asp:Panel ID="PopupMenu2" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating2('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating2('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating2('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating2('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating2('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating2('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>

                <asp:Panel ID="PopupMenu3" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating3('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating3('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating3('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating3('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating3('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating3('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>

                <asp:Panel ID="PopupMenu4" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating4('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating4('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating4('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating4('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating4('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating4('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>

                <asp:Panel ID="PopupMenu5" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating5('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating5('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating5('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating5('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating5('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating5('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>

                <asp:Panel ID="popupMenu6" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating6('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating6('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating6('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating6('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating6('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating6('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>

                 <asp:Panel ID="popupMenu7" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating7('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating7('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating7('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating7('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating7('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating7('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>
                  <asp:Panel ID="popupMenu8" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating8('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating8('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating8('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating8('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating8('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating8('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>
                  <asp:Panel ID="popupMenu9" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating9('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating9('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating9('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating9('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating9('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating9('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>
                  <asp:Panel ID="popupMenu10" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating10('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating10('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating10('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating10('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating10('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating10('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>
                  <asp:Panel ID="popupMenu11" CssClass="PanelCSS" Width="130" runat="server">
                    <a class="LinkRatingcss" onclick="SetRating11('TN');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStarEmpty"></span></td>
                                <td style="text-align: right;"><span class="step">TN</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating11('1');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">1</span></td>
                            </tr>
                        </table>
                    </a>
                    <a class="LinkRatingcss" onclick="SetRating11('2');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">2</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating11('3');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">3</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating11('4');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">4</span></td>
                            </tr>
                        </table>
                    </a>

                    <a class="LinkRatingcss" onclick="SetRating11('5');">
                        <table class="table-borderless">
                            <tr>
                                <td style="text-align: left;"><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span><span class="spanStar"></span></td>
                                <td style="text-align: right;"><span class="step">5</span></></td>
                            </tr>
                        </table>
                    </a>
                </asp:Panel>

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progBgFilter_Show" runat="server"></div>
                <div id="processMessage" class="progMsg_Show">
                    <img alt="Loading..." src="Images/Loading.gif" /><br />
                    <br />
                    Loading Please Wait...
                </div>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" TargetControlID="progressBackgroundFilter"
                    VerticalSide="Middle" HorizontalSide="Center" VerticalOffset="10" ScrollEffectDuration=".1"
                    HorizontalOffset="10">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </form>

    <script type="text/javascript" src="../PDP/Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../PDP/Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="../PDP/Scripts/bootstrap.min.js"></script>
    <script>

        function smartObjective(obj, kpi,st, cmt) {
         
           
            var obj_txt = $("#" + obj).val();
           var kpi_txt = $("#" + kpi).val();
           var st_txt = $("#" + st).attr("class");
           var cmt_txt = $("#" + cmt).val();
           var temp_st_txt = st_txt.substring(18, st_txt.length);
           
           if (temp_st_txt == "ontarget")
           {
               temp_st_txt = "On Target";
           }
           else if (temp_st_txt == "GoalChange")
           {
               temp_st_txt = "Goal Change";
           }
           else if (temp_st_txt == "AtRisk") {
               temp_st_txt = "At Risk";
           }
            $("#txt_rev_obj").val(obj_txt);
            $("#txt_rev_KPI").val(kpi_txt);
            $("#txt_rev_status").val(temp_st_txt);
            $("#txt_rev_cmt").val(cmt_txt);
            
            $("#myModal").modal({
                backdrop: true,
                keyboard: true
            });
        }

        function showModal() {
            $("#myModal").modal('show');

        }
        function showModal_confirm() {

            $("#modalConfirm").modal({
                backdrop: true,
                keyboard: true
            });
        }

        function HideModal() {
            $("#myModal").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }

        $(document).ready(function () {


            $(".msgInfoBox").draggable({ axis: "y" });

            $(".msgInfoclose").click(function (e) {
                $(this).parent().fadeTo(300, 0, function () {
                    $(this).remove();
                });
                e.preventDefault();
            });


            if ($('#hfCurrTabid').length > 0) {

                var CurrTabId = $('#hfCurrTabid').val();
                $('.glyphicon').removeClass("glyphicon-minus").addClass("glyphicon-plus")
                $('.acc_container').hide();
                $("#" + CurrTabId).addClass('active').next().show();

                $('.glyphicon', "#" + CurrTabId).removeClass("glyphicon-plus").addClass("glyphicon-minus");
            }


            ////Hide/close all containers
            // $('.acc_Header:first').addClass('active').next().show();

            $('.acc_Header').click(function (e) {
                var maxHeight = 0;
                var cpHeight = 0;
                var minusHeight = 30;
                var ids = '';
                $content = $(".contentPanel");
                tab_id = e.target.id;

                if ($(this).next().is(':hidden')) { //If immediate next container is closed...
                    $('.glyphicon').removeClass("glyphicon-minus").addClass("glyphicon-plus")
                    $('.acc_Header').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
                    $(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
                    $('.glyphicon', this).removeClass("glyphicon-plus").addClass("glyphicon-minus");
                    $('#hfCurrTabid').val(tab_id);

                    if ($content.is(":visible") == true) {

                        cpHeight = $content.outerHeight();
                        minusHeight = 115;
                    }


                    $(".acc_Header").each(function () {
                        ids = this.id;
                        maxHeight += $(this).outerHeight();

                        if (ids == tab_id) {
                            return false;
                        }

                    });


                    $('html, body').animate({
                        scrollTop: ($('#tb_ObjectivesCurrent').offset().top + maxHeight + cpHeight - minusHeight)
                    }, 1000);



                }
                e.preventDefault(); //Prevent the browser jump to the link anchor
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {

            $(".msgInfoBox").draggable({ axis: "y" });

            $(".msgInfoclose").click(function (e) {
                $(this).parent().fadeTo(300, 0, function () {

                    $(this).remove();
                });
                e.preventDefault();
            });

            if ($('#hfCurrTabid').length > 0) {
                var CurrTabId = $('#hfCurrTabid').val();
                $('.glyphicon').removeClass("glyphicon-minus").addClass("glyphicon-plus")
                $('.acc_container').hide();
                $("#" + CurrTabId).addClass('active').next().show();

                $('.glyphicon', "#" + CurrTabId).removeClass("glyphicon-plus").addClass("glyphicon-minus");

            }


            //$('.acc_container').hide(); //Hide/close all containers
            //$('.acc_Header:first').addClass('active').next().show();

            $('.acc_Header').click(function (e) {
                var maxHeight = 0;
                var cpHeight = 0;
                var minusHeight = 30;
                var ids = '';
                $content = $(".contentPanel");
                tab_id = e.target.id;

                if ($(this).next().is(':hidden')) { //If immediate next container is closed...
                    $('.glyphicon').removeClass("glyphicon-minus").addClass("glyphicon-plus")
                    $('.acc_Header').removeClass('active').next().slideUp(); //Remove all "active" state and slide up the immediate next container
                    $(this).toggleClass('active').next().slideDown(); //Add "active" state to clicked trigger and slide down the immediate next container
                    $('.glyphicon', this).removeClass("glyphicon-plus").addClass("glyphicon-minus");
                    $('#hfCurrTabid').val(tab_id);

                    if ($content.is(":visible") == true) {

                        cpHeight = $content.outerHeight();
                        minusHeight = 115;
                    }



                    $(".acc_Header").each(function () {
                        ids = this.id;
                        maxHeight += $(this).outerHeight();
                        if (ids == tab_id) {
                            return false;
                        }

                    });
                    $('html, body').animate({
                        scrollTop: ($('#tb_ObjectivesCurrent').offset().top + maxHeight + cpHeight - minusHeight)
                    }, 1000);


                }
                e.preventDefault(); //Prevent the browser jump to the link anchor

            });
        });


    </script>
</body>
</html>
