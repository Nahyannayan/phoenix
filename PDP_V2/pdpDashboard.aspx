﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="pdpDashboard.aspx.vb" Inherits="pdpDashboard" EnableTheming="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GEMS Performance Development Plan</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
    <%--<link href="Styles/Tabstyle.css" rel="stylesheet" />
    <link href="Styles/PDP.css" rel="stylesheet" />
    <link href="Styles/bootstrap.css" rel="stylesheet" />--%>
    
    <%--<link href="Styles/Tabstyle.css?id=<%: DateTime.Now.Ticks %>" rel="stylesheet" />
    <link href="Styles/PDP.css?id=<%: DateTime.Now.Ticks %>" rel="stylesheet" />
    <link href="Styles/bootstrap.css?id=<%: DateTime.Now.Ticks %>" rel="stylesheet" />--%>
       <link href="Styles/Tabstyle.css?id=v3" rel="stylesheet" />
    <link href="Styles/PDP.css?id=v4" rel="stylesheet" />
   <%-- <link href="Styles/style.css" rel="stylesheet"  />--%>
    <link href="Styles/bootstrap.css" rel="stylesheet" />
    <script type="text/javascript">
        function showHide() {
            $header = $(".header");
            $content = $(".contentPanel");
            $ColExpId = $("#ColExpCss");
            $content.slideToggle(500, function () {
                if ($content.is(":visible") == true) {
                    $ColExpId.removeClass("colClass").addClass("ExpClass");
                }
                else {
                    $ColExpId.removeClass("ExpClass").addClass("colClass");
                }
            });
        }
        var tempObj = '';
        function popUpDetails(url) {
            try {
                if (tempObj && !tempObj.closed) {
                    tempObj.focus();
                }
                else {
                    window.open(url);
                }
            }
            catch (e) { }

            return false;
        }

        function buttonDownClick() {

            var cbutton = document.getElementById("<%= btnPDF_D.ClientID %>");
            cbutton.click();
        }

   
    </script>
</head>
<body  >
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
        <div class="col-md-12 col-lg-12 col-sm-12">
            <img src="Images/GEMS-pdp-banner_2015_16.png" class="img-responsive" />
        </div>
<div style="display:none;">
<asp:Button ID="btnPDF_D" runat="server" Text="btnS"  /></div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <section class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
 <asp:linkbutton id="lnk_home" runat="server" class="btn btn-xs btn-default pull-right" ><span class="glyphicon glyphicon-backward"></span>&nbsp;Back To Home Page</asp:linkbutton>
                    <div class="header" onclick="showHide();">

                        <span id="ColExpCss" class="ExpClass"></span>
                        <span style="font-size: 16px; padding-left: 6px; color: #19a9e5;">Filter By</span>
                    </div>
                   
                    <div class="contentPanel">
                        <div class="form-group col-md-6 col-lg-6 col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">Staff Name:</span>

                                <asp:TextBox ID="txt_EmpName" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-sm-6">

                            <div class="input-group">
                                <span class="input-group-addon">Staff No:</span>

                                <asp:TextBox ID="txt_EmpNo" runat="server" class="form-control"></asp:TextBox>

                            </div>
                        </div>
                         <div class="form-group col-md-6 col-lg-6 col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">Business Unit:</span>
                                <asp:DropDownList ID="ddlBsu" runat="server" class="form-control input-sm"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">PDP Category:</span>

                                <asp:DropDownList ID="ddlType" runat="server" class="form-control input-sm"></asp:DropDownList>

                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">Department:</span>
                                <asp:DropDownList ID="ddlDept" runat="server" class="form-control input-sm"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-6 col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">Pending Level:</span>

                                <asp:DropDownList ID="ddlPending" runat="server" class="form-control input-sm"></asp:DropDownList>

                            </div>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12">

                            <div class="pull-right">
                                <asp:Button ID="btnSearch" runat="server" CssClass="buttonSearch" Text="Search" Width="128px" Height="24px" />
                                     <asp:Button ID="btnExport" runat="server" CssClass="buttonExport" Text="Export" Width="128px" Height="24px" />
                                 <asp:Button ID="btnPublish" runat="server" CssClass="buttonPub" Text="Publish Rating" Width="128px" Height="24px" Visible="false" />
                                 <ajaxToolkit:ConfirmButtonExtender ID="cbePublish" runat="server"
                            TargetControlID="btnPublish" 
                            ConfirmText="Are you sure you want to Publish the Rating to the selected employee?" />
                            </div>
                        </div>

                    </div>
                </section>
                <section class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                    <div class="table-responsive divDashboardContainer" >
                  
                  <div style="float:right;">
                  
                  
                  <asp:Repeater ID="rptYear" runat="server">
                   <ItemTemplate >
                <asp:LinkButton ID="lbtn" runat="server" CssClass='<%# Eval("class")%>' CommandArgument='<%# Eval("ORD")%>' OnClick ="lbtn_Click" >
                 <span class="cssYear" > <asp:Label  id="lblyear" runat="server" Text='<%# Eval("PDPY_NAME")%>' ></asp:Label> </span>
                 <div class="cssStepContainer"><asp:Label id="lbl" runat="server" Text='<%# Eval("PDPY_DESC")%>' ></asp:Label></div>
                    
                 </asp:LinkButton>
                       <asp:HiddenField ID="h_ID" runat="server" value='<%# Eval("ID")%>'/>
                       <asp:HiddenField ID="H_ACTIVE" runat="server" value='<%# Eval("PDPY_bLIVE")%>'/>
</ItemTemplate>
</asp:Repeater>
                  
</div>


           
            
                        <asp:Repeater ID="rptPdp" runat="server">
                            <HeaderTemplate>
                                <table style="width: 100%;" border="0" class="table">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="tbldbImgCss">
                                        <asp:HiddenField ID="hfID" runat="server" Value='<%# Eval("EMP_ID")%>' />
                                        <asp:HiddenField ID="hfSuper" runat="server" Value='<%# Eval("bSUPER")%>' />
                                        <asp:HiddenField ID="hf_published" runat="server" Value='<%# Eval("EPR_BPUBLISHED")%>' />
                                        <img src='<%# Eval("EMD_PHOTO")%>' class="img-thumbnail img-responsive" alt='<%# Eval("EMP_NAME")%>' style="min-height: 120px; height: 120px; width: 110px; min-width: 110px;" />
                                        <div id="EmpStatus_CSS" runat="server" class='<%# Eval("EmpStatus_CSS")%>'><span id="EmpStatus_Value" runat="server" ><%# Eval("EmpStatus_Value")%></span></div>
                                    </td>
                                    <td class="tbldbContentcss">
                                        <div class="divDbHeadercss">
                                            <span class="spDbEnamecss"><%# Eval("EMP_NAME")%></span>
                                            <span class="spDbDesign"><%# Eval("DES_DESCR")%></span>&nbsp;&nbsp;&nbsp;<img src='<%# Eval("EPR_PUBLISHED_IMG")%>' style="width:80px; height:20px;"><span class="spDbDept"><%# Eval("DPT_DESCR")%></span></div>
                                        <div class="divDbContentHoldercss">
                                            <span class="spdbOverallCss">Overall Summary</span>
                                            <div class="divDbContentcss">
                                              <span id="spSummary" runat="server">  <%# Eval("EPR_SUMMARY")%></span>
                                            </div>
                                            <div class="divDbContentRatingcss"><span id="spRating" runat="server"><%# Eval("EPR_RTM_ID")%></span></div>
                                        </div>
                                        <div class="divDbEmpLevelcss">
                                            <span id="LEVEL1_COLOR" runat="server" class='<%# Eval("LEVEL1_COLOR")%>'><span  id="LEVEL1_CHECKBOX" runat="server" class='<%# Eval("LEVEL1_CHECKBOX")%>'></span>
                                                <%# Eval("EMP_REVLEVEL_1_NAME")%></span>

                                            <span class='<%# Eval("LEVEL2_COLOR")%>'><span class='<%# Eval("LEVEL2_CHECKBOX")%>'></span>
                                                <%# Eval("EMP_REVLEVEL_2_NAME")%></span>
                                            <span class='<%# Eval("LEVEL3_COLOR")%>'><span class='<%# Eval("LEVEL3_CHECKBOX")%>'></span>
                                                <%# Eval("EMP_REVLEVEL_3_NAME")%></span>
                                            <span class="spdbEditRev">
                                            
                                               <asp:LinkButton ID="lbtnSetLevels" runat="server" CssClass="buttonRevert" Height="23px" OnClick="lbtnSetLevels_Click"
                                                   
                                                    Text="Set Levels"></asp:LinkButton>
                                                 <asp:LinkButton ID="lbtnRevPublish" runat="server" CssClass="buttonRevertRating" Height="23px" OnClick="lbtnRevertPublish_Click"
                                                   
                                                    Text="Revert to Line Manager"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnPublish" runat="server" CssClass="buttonPublish" Height="23px" OnClick="lbtnPublish_Click"
                                                   
                                                    Text="Publish Rating"></asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server"
                            TargetControlID="lbtnPublish" 
                            ConfirmText="Are you sure you want to Publish the Rating to the selected employee?" />
                                                <asp:LinkButton ID="lbtnRevert" runat="server" CssClass="buttonRevert" Height="23px" OnClick="lbtnRevert_Click"
                                                   
                                                    Text="Revert Back"></asp:LinkButton>
                                                <asp:LinkButton ID="lbtnRev" runat="server" CssClass="buttonreview" Height="23px" OnClick="lbtnRev_Click"
                                                        Text="Review Performance & Goals"></asp:LinkButton>
                                                 <asp:LinkButton ID="lbtnFinal" runat="server"  Height="23px" CssClass="buttonFinal"
                                                     OnClick="lbtnFinal_Click" Text="Final Review"></asp:LinkButton>  
                                                 <asp:LinkButton ID="lbtnInterim" runat="server"  Height="23px" 
                                                     OnClick="lbtnInterim_Click" 

                                                                                                             Text="Interim Review" ></asp:LinkButton>
                                                 <asp:LinkButton ID="lbtnInterim_Edit" runat="server"  Height="23px" 
                                                     OnClick="lbtnInterim_Edit_Click" 

                                                                                                             Text="Edit Interim" CssClass="buttonFinal"></asp:LinkButton>
                                                                                                               <asp:LinkButton ID="btnDownloadPdf" runat="server" Text="Download & Print" Height="23px" CssClass="buttonPdf"
                                                      CommandArgument='<%# Bind("EPR_ID") %>' OnClick="btnDownloadPdf_Click"></asp:LinkButton>

                                                <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Eval("EMP_ID")%>' />
                                                <asp:HiddenField ID="HF_EPR_ID" runat="server" Value='<%# Eval("EPR_ID")%>' />
                                                <asp:HiddenField ID="HF_USR_EMP_ID" runat="server" Value='<%# Eval("USR_EMP_ID")%>' />
                                                 <asp:HiddenField ID="HF_INTERIM_EDIT" runat="server" Value='<%# Eval("EPR_bEDIT_INTERIM")%>' />
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    
                  
                    </div>
                </section>
                
                <div id="Panel_PublishRevertBack" runat="server" class="darkPanelM" visible="false">
                    <div class="darkPanelMTop">
                        <div class="holderInner" style="overflow:scroll" >

                            <table style="width: 100%; height: 100%;">
                                <tr>
                                    <td valign="top" style="padding-left:20px;" >
                                        <asp:Label ID="Label1" runat="server" EnableViewState="false" Text="Select  Employee">
                                        </asp:Label>
                                        
                                    </td>
                                    </tr>
                                <tr>
                                    <td valign="top" style="padding-left:20px;"> 
                                        <asp:RadioButtonList ID="rbtnList_Rievewer_Publish" runat="server">
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Select the Rievewer" ControlToValidate="rbtnList_Rievewer_Publish" ValidationGroup="SaveRevertBack"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>                                    
                                    <td width="80%"> <textarea class="form-control" rows="3" id="txt_Remarks_Publish" runat="server" placeholder="Remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"></textarea>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Remarks Required" ControlToValidate="txt_Remarks_Publish" ValidationGroup="SaveRevertBack"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btn_Revert_Publish" runat="server" Text="Revert" CssClass="buttonSave" Height="26px" Width="80px" ValidationGroup="SaveRevertBack"  CausesValidation="true" />
                                         <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server"
                            TargetControlID="btn_Revert_Publish" 
                            ConfirmText="Are you sure you want to revert the PDP form to the selected employee?" />
                                          <asp:Button ID="btn_Cancel_Publish" runat="server" Text="Cancel"  Height="26px" Width="80px" ToolTip="Click here to cancel and close"  CssClass="buttonCancel" />
                                    
<asp:HiddenField ID="hdnEpr_Publish" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="darkPanelFooter">
                            <span class="TitlePl">Revert Back</span>
                        </div>
                    </div>
                </div>


                <div id="Panel_RevertBack" runat="server" class="darkPanelM" visible="false">
                    <div class="darkPanelMTop">
                        <div class="holderInner" style="overflow:scroll" >

                            <table style="width: 100%; height: 100%;">
                                <tr>
                                    <td valign="top" style="padding-left:20px;" >
                                        <asp:Label ID="lblerrormsg" runat="server" EnableViewState="false" Text="Select  Employee">
                                        </asp:Label>
                                        
                                    </td>
                                    </tr>
                                <tr>
                                    <td valign="top" style="padding-left:20px;"> 
                                        <asp:RadioButtonList ID="rbtnList_Rievewer" runat="server">
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="rfv_Rievewer" runat="server" ErrorMessage="Select the Rievewer" ControlToValidate="rbtnList_Rievewer" ValidationGroup="SaveRevertBack"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>                                    
                                    <td width="80%"> <textarea class="form-control" rows="3" id="txt_Remarks" runat="server" placeholder="Remarks" draggable="false" style="min-height: 50px !important;"
                                        enableviewstate="true"></textarea>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Remarks Required" ControlToValidate="txt_Remarks" ValidationGroup="SaveRevertBack"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btn_Revert" runat="server" Text="Revert" CssClass="buttonSave" Height="26px" Width="80px" ValidationGroup="SaveRevertBack"  CausesValidation="true" />
                                         <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server"
                            TargetControlID="btn_Revert" 
                            ConfirmText="Are you sure you want to revert the PDP form to the selected employee?" />
                                          <asp:Button ID="btn_Cancel" runat="server" Text="Cancel"  Height="26px" Width="80px" ToolTip="Click here to cancel and close"  CssClass="buttonCancel" />
                                   <asp:Repeater ID="rptPDP_Reveiwer" runat="server">
                            <HeaderTemplate>
                                <table style="width: 100%;" border="0" class="table">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="tbldbImgCss">
                                        <asp:HiddenField ID="hfID" runat="server" Value='<%# Eval("EMP_ID")%>' />
                                        <img src='<%# Eval("EMD_PHOTO")%>' class="img-thumbnail img-responsive" alt='<%# Eval("EMP_NAME")%>' style="min-height: 120px; height: 120px; width: 110px; min-width: 110px;" />                                                                     
                                    </td>
                                    <td class="tbldbContentcss">
                                        <div class="divDbHeadercss">
                                            <span class="spDbEnamecss"><%# Eval("EMP_NAME")%></span>
                                            <span class="spDbDesign"><%# Eval("DES_DESCR")%></span><span class="spDbDept"><%# Eval("DPT_DESCR")%></span>
                                           
                                        </div>                                                                     
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater> 
<asp:HiddenField ID="hdnEpr" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="darkPanelFooter">
                            <span class="TitlePl">Revert Back</span>
                        </div>
                    </div>
                </div>
                
                
                
                <div id="Panel_SetLevel" runat="server" class="darkPanelM" visible="false">
                     
                    <div class="darkPanelMTop">
                        <div class="holderInner" style="overflow:scroll" >
                           
                            <table border=1 style="width: 100%; height: 100%;">
                                <tr><td><div class="darkPanelFooter">
                            <span class="TitlePl">Set Level</span>
                        </div></td></tr>
                            <tr>
                                    <td align="left">
                             <asp:Label ID="lbl_EMPNAME" runat="server" EnableViewState="false" Text="">
                                        </asp:Label>
                                        
                                  </td></tr> 
                                  
                                    <tr>
                                    <td align="left">     
                                <div class="form-group col-md-6 col-lg-6 col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">Line Manager:</span>
                                <asp:DropDownList ID="ddl_Level1" runat="server" class="form-control input-sm"></asp:DropDownList>
                            </div>
                        </div>
                                        <asp:Button ID="btn_SaveLevel" runat="server" Text="Update" CssClass="buttonSave" Height="26px" Width="80px"  />
                                         <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                            TargetControlID="btn_Revert" 
                            ConfirmText="Are you sure you want to change the PDP Levels for the selected employee?" />
                         </td></tr> 
                        <tr><td>
                                         <div class="darkPanelFooter">
                            <span class="TitlePl">Set View Level</span>
                        </div></td></tr>
                          <tr>
                                    <td align="left">
                         <div class="form-group col-md-6 col-lg-6 col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">View Level:</span>
                                <asp:DropDownList ID="ddl_Level2" runat="server" class="form-control input-sm"></asp:DropDownList>
                               

                            </div>
                        </div>
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="buttonAdd" Height="26px" Width="80px"  />
                         </td>

                          </tr> 
                        <tr><td align="center">
                           <asp:GridView ID="gvVieLevel" runat="server"  AutoGenerateColumns="False"  GridLines="None" >
                                <RowStyle CssClass="form-control row"  />
                                <Columns>
                                    <asp:TemplateField  Visible="False">
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblEPL_ID" runat="server" Text='<%# Bind("EPL_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField >
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblslno" runat="server" Text='<%# Bind("SLNO")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField >
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmp" runat="server" Text='<%# Bind("EMP_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:ButtonField CommandName="Delete" ImageUrl="~/PDP_V2/Images/cross.gif" ButtonType="Image" >
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:ButtonField>
                                    </Columns>
                               </asp:GridView>
                            </td></tr>
                         
                        
                               
                              <tr>
                                    <td align="left">
                                        
                                          <asp:Button ID="btn_SaveLevel_Cancel" runat="server" Text="Close"  Height="26px" Width="80px" ToolTip="Click here to cancel and close"  CssClass="buttonCancel" />
                             
   </td></tr> 
                                   
                            </table>
                        </div>
                        
                    </div>
                </div>
                
               
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progBgFilter_Show" runat="server"></div>
                <div id="processMessage" class="progMsg_Show">
                    <img alt="Loading..." src="Images/Loading.gif" /><br />
                    <br />
                    Loading Please Wait...
                </div>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" TargetControlID="progressBackgroundFilter"
                    VerticalSide="Middle" HorizontalSide="Center" VerticalOffset="10" ScrollEffectDuration=".1"
                    HorizontalOffset="10">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>

<cr:crystalreportsource id="rs1" runat="server" cacheduration="1">
    </cr:crystalreportsource>

    </form>
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="Scripts/bootstrap.min.js"></script>
</body>
</html>
